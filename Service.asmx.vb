﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

Imports bym_dll
Imports System.IO


' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://bym.net.tr/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class Service
    Inherits System.Web.Services.WebService

    Private connstr As String = ConfigurationManager.ConnectionStrings("connstr").ConnectionString
    Private builder As New System.Data.SqlClient.SqlConnectionStringBuilder(connstr)
    Private initialcatalog As String = builder.InitialCatalog




    Private Function StringToDatatable(ByVal Veri As String) As DataTable
        Dim dt As New DataTable("Tablo")
        Dim incomingdata As String = Veri

        If incomingdata = "" Then Return New DataTable

        Dim stringRead As New StringReader(incomingdata) 'servisten stringi al
        Dim glist As New List(Of String) 'string array oluştur

        Dim strTabloFields As String = stringRead.ReadLine() 'ilk satırı oku
        Dim arr As Array = strTabloFields.Split(Chr(9)) 'ilk satırı parçala

        glist.AddRange(arr) 'array i generic arraya göm
        glist.RemoveAt(arr.Length - 1)

        For i As Integer = 0 To glist.Count - 1
            dt.Columns.Add(glist(i))
        Next

        While stringRead.Peek <> -1

            Dim strTabloDatalar As String = stringRead.ReadLine()
            arr = strTabloDatalar.Split(Chr(9))
            glist.Clear()
            glist.AddRange(arr)
            glist.RemoveAt(arr.Length - 1)

            dt.Rows.Add(dt.NewRow())


            For i As Integer = 0 To glist.Count - 1
                dt.Rows(dt.Rows.Count - 1)(dt.Columns(i).ColumnName) = glist(i)
            Next

        End While

        Return dt
    End Function
    Private Function StringOlustur(ByVal dt As DataTable) As String
        Dim data As String = ""

        Dim stringBuilder As New StringBuilder()
        data = ""
        For Y = 0 To dt.Columns.Count - 1
            data = data & dt.Columns(Y).ColumnName & Chr(179)
        Next
        stringBuilder.AppendLine(data)
        For X = 0 To dt.Rows.Count - 1
            data = ""
            For Y = 0 To dt.Columns.Count - 1
                data = data & NULA(dt, X, (dt.Columns(Y).ColumnName)) & Chr(9)
            Next
            stringBuilder.AppendLine(data)
        Next
        Return stringBuilder.ToString()

    End Function


    ''' <summary>
    ''' SQL WEB KOMUT
    ''' </summary>
    ''' <param name="sq">İŞLENECEK SQL</param>
    ''' <param name="TabloName">SQL'IN ÇALIŞACAĞI TABLO</param>
    ''' <returns></returns>
    <WebMethod()>
    Public Function SQL_WEB_Komut(ByVal sq As String, ByVal TabloName As String) As String

        Dim ST As New STOK_BILGI

        B_SQL = connstr
        B_PRJ = "PDA_TRANS"
        SQL_TUR = "2005"

        Dim DT As New DataTable
        Dim S As String = ""
        Dim L As String = ""
        Dim LS As String = ""
        Dim X As Integer = 0
        Dim Y As Integer = 0
        Dim Data As String = ""
        Dim KOMUT As String = ""

        Dim SYOL As String = ""
        Dim SON As String = ""

        Dim SONUC As String = ""

        sq = Trim(sq)

        sq = sq.Replace("@tarih_saat@", TTAR.TR2UK(TTAR.TARIH) & " " & Now.ToShortTimeString)
        sq = sq.Replace("@tarih@", TTAR.TR2UK(TTAR.TARIH))
        sq = sq.Replace("@saat@", Now.ToShortTimeString)

        L = sq
        KOMUT = sq

        If InStr(L, " ") > 0 Then

            KOMUT = Trim(Mid(L, 1, InStr(L, " ")))
            KOMUT = LCase(KOMUT)
            KOMUT = KOMUT.Replace("ınsert", "insert")

        End If

        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()

        L = L.Replace("tbl_001_01_", KUL.tper)
        L = L.Replace("tbl_001_", KUL.tfrm)

        'INSERT INTO 
        'SELECT SCOPE_IDENTITY() AS DEGER 
        If InStr(L, "SCOPE_IDENTITY") > 0 Then
            KOMUT = "select"
        End If



        If KOMUT = "select" Then
            sq = Trim(L)
            DT = SQL_TABLO_2005(B_SQL, sq, TabloName, False, False, False, False)
            Return StringOlustur(DT)
        End If

        If KOMUT = "insert" Or KOMUT = "update" Or KOMUT = "delete" Then
            sq = Trim(L)
            If SQL_KOS(sq, False) = True Then
                Return "True"
            Else
                Return "False"
            End If
        End If

        '------------------------------------------------------------------------------------------------------------------------------------------------------------
        '------------------------------------------------------------------------------------------------------------------------------------------------------------
        '-- Versiyon Bilgisi 
        '------------------------------------------------------------------------------------------------------------------------------------------------------------
        '------------------------------------------------------------------------------------------------------------------------------------------------------------
        If KOMUT = "@versiyon" Then
            DT = SQL_TABLO_2005(B_SQL, "SELECT '1.1.7' As versiyon", TabloName, False, False, False, False)
            Return StringOlustur(DT)
        End If
        '------------------------------------------------------------------------------------------------------------------------------------------------------------
        '------------------------------------------------------------------------------------------------------------------------------------------------------------



        If KOMUT = "@sevk" Then
            Dim Firma As String
            Dim GelenVeri(4) As String
            GelenVeri = L.Split(" ")


            If GelenVeri(4) <> 0 Then
                Firma = New String("0", 3 - GelenVeri(4).ToString.Length) & GelenVeri(4)
                KUL.tfrm = "tbl_" & Firma & "_"
                KUL.tper = "tbl_" & Firma & "_" & "01" & "_"

            Else
                KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
                KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
            End If
            Call BARKOD_TURLERI()


            sq = Trim(L)
            SONUC = Sevk_Islem(sq)
            KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
            KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
            Return SONUC
        End If

        'SQ = "@TRANS " & Val(Label6.Tag)
        If KOMUT = "@trans" Then
            Dim Firma As String
            Dim GelenVeri(2) As String
            GelenVeri = L.Split(" ")


            If GelenVeri(2) <> 0 Then
                Firma = New String("0", 3 - GelenVeri(2).ToString.Length) & GelenVeri(2)
                KUL.tfrm = "tbl_" & Firma & "_"
                KUL.tper = "tbl_" & Firma & "_" & "01" & "_"

            Else
                KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
                KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
            End If
            Call BARKOD_TURLERI()

            sq = Trim(L)
            SONUC = Trans_Islem(sq)

            KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
            KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
            Return SONUC
        End If

        'SQ = "@stok 001abc"
        If KOMUT = "@stok" Then

            Dim Firma As String = ""
            Dim GelenVeri(2) As String
            GelenVeri = L.Split("|")


            If GelenVeri(2) <> 0 Then
                Firma = New String("0", 3 - GelenVeri(2).ToString.Length) & GelenVeri(2)
                KUL.tfrm = "tbl_" & Firma & "_"
                KUL.tper = "tbl_" & Firma & "_" & "01" & "_"

            Else
                KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
                KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
            End If

            Call BARKOD_TURLERI()

            sq = GelenVeri(1)
            If ST.BUL(sq) = False Then
                KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
                KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
                Return "0" & "|" & "xx" & "|" & "yy"
            Else
                SONUC = ST.SKART.S00_id
                SONUC = SONUC & "|" & ST.SKART.S01_kod
                SONUC = SONUC & "|" & ST.SKART.S02_adi
                Select Case ST.SKART.S99_Nerede_Buldun
                    Case STOK_BILGI.Bul_Tur.Stok_1BrmBarkod
                        SONUC = SONUC & "|" & ST.SKART.S03_1birim
                        SONUC = SONUC & "|" & ST.SKART.S03_1brmcar
                    Case STOK_BILGI.Bul_Tur.Stok_2BrmBarkod
                        SONUC = SONUC & "|" & ST.SKART.S03_2birim
                        SONUC = SONUC & "|" & ST.SKART.S03_2brmcar
                    Case Else
                        SONUC = SONUC & "|" & ST.SKART.S03_birim
                        SONUC = SONUC & "|" & "1"
                End Select
                SONUC = SONUC & "|" & ST.SKART.S05_afiyat
                SONUC = SONUC & "|" & ST.SKART.S06_sfiyat
                SONUC = SONUC & "|" & ST.SKART.S03_birim
                SONUC = SONUC & "|" & ST.SKART.S07_alkdvor
                SONUC = SONUC & "|" & ST.SKART.S08_sakdvor
                SONUC = SONUC & "|" & ST.SKART.S100_BarMik
                SONUC = SONUC & "|" & ST.SKART.S98_Carpan
                SONUC = SONUC & "|" & ST.SKART.S101_asorti_id
                SONUC = SONUC & "|" & ST.SKART.S43_a_reftip
                KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
                KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
                Return SONUC
            End If
        End If

        If KOMUT = "@stokfiy" Then
            Dim GelenVeri(7) As String
            Dim Firma As String = ""
            GelenVeri = sq.Split("|")


            If GelenVeri(7) <> 0 Then
                Firma = New String("0", 3 - GelenVeri(7).ToString.Length) & GelenVeri(7)
                KUL.tfrm = "tbl_" & Firma & "_"
                KUL.tper = "tbl_" & Firma & "_" & "01" & "_"

            Else
                KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
                KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
            End If


            Dim XF As New X_Fiyat_Bilgi
            Dim FIY As Double = 0
            Call BARKOD_TURLERI()



            If ST.BUL(GelenVeri(1)) = False Then
                KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
                KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
                Return "0" & "|" & "xx" & "|" & "yy"
            Else

                XF.Fil_AL_SAT = GelenVeri(2)
                XF.Fil_Tur = GelenVeri(3)
                XF.Fil_F_List_Id = 0
                XF.Fil_Cari_Id = GelenVeri(4)
                XF.Fil_Cari_Kod = VT_BILGI(KUL.tfrm & "cari", "a_id", "a_kod", GelenVeri(4))
                XF.Fil_Sube_Id = KUL.ID_SUBE
                XF.Fil_Isl_Tarih = GelenVeri(5)
                XF.Fil_Kul_Kod = GelenVeri(6)
                XF.Fil_STB = ST
                XF.X_Fiyat_Getir()
                FIY = XF.RET_Fiyat

                SONUC = ST.SKART.S00_id
                SONUC = SONUC & "|" & ST.SKART.S01_kod
                SONUC = SONUC & "|" & ST.SKART.S02_adi
                Select Case ST.SKART.S99_Nerede_Buldun
                    Case STOK_BILGI.Bul_Tur.Stok_1BrmBarkod
                        SONUC = SONUC & "|" & ST.SKART.S03_1birim
                        SONUC = SONUC & "|" & ST.SKART.S03_1brmcar
                    Case STOK_BILGI.Bul_Tur.Stok_2BrmBarkod
                        SONUC = SONUC & "|" & ST.SKART.S03_2birim
                        SONUC = SONUC & "|" & ST.SKART.S03_2brmcar
                    Case Else
                        SONUC = SONUC & "|" & ST.SKART.S03_birim
                        SONUC = SONUC & "|" & "1"
                End Select
                SONUC = SONUC & "|" & FIY
                SONUC = SONUC & "|" & FIY
                SONUC = SONUC & "|" & ST.SKART.S03_birim
                SONUC = SONUC & "|" & ST.SKART.S07_alkdvor
                SONUC = SONUC & "|" & ST.SKART.S08_sakdvor
                SONUC = SONUC & "|" & ST.SKART.S100_BarMik
                SONUC = SONUC & "|" & ST.SKART.S98_Carpan
                SONUC = SONUC & "|" & ST.SKART.S101_asorti_id
                SONUC = SONUC & "|" & ST.SKART.S43_a_reftip
                SONUC = SONUC & "|" & XF.RET_DovizID
                SONUC = SONUC & "|" & XF.RET_DovizKur
                SONUC = SONUC & "|" & ST.SKART.S22_selo
                KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
                KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
                Return SONUC
            End If
        End If

        If KOMUT = "@stoksecfiy" Then
            Dim GelenVeri(4) As String
            Dim Firma As String = ""
            Dim FIY As Double = 0
            Call BARKOD_TURLERI()

            GelenVeri = sq.Split("|")
            If GelenVeri(4) <> 0 Then
                Firma = New String("0", 3 - GelenVeri(4).ToString.Length) & GelenVeri(4)
                KUL.tfrm = "tbl_" & Firma & "_"
                KUL.tper = "tbl_" & Firma & "_" & "01" & "_"

            Else
                KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
                KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
            End If

            Dim XF As New X_Fiyat_Bilgi

            If ST.BUL(GelenVeri(1)) = False Then
                Return "0" & "|" & "xx" & "|" & "yy"
            Else

                FIY = FIYAT_BUL(GelenVeri(2), GelenVeri(1), "", GelenVeri(3), True)

                SONUC = ST.SKART.S00_id
                SONUC = SONUC & "|" & ST.SKART.S01_kod
                SONUC = SONUC & "|" & ST.SKART.S02_adi
                Select Case ST_KART.SKART.S99_Nerede_Buldun
                    Case STOK_BILGI.Bul_Tur.Stok_1BrmBarkod
                        SONUC = SONUC & "|" & ST.SKART.S03_1birim
                        SONUC = SONUC & "|" & ST.SKART.S03_1brmcar
                    Case STOK_BILGI.Bul_Tur.Stok_2BrmBarkod
                        SONUC = SONUC & "|" & ST.SKART.S03_2birim
                        SONUC = SONUC & "|" & ST.SKART.S03_2brmcar
                    Case Else
                        SONUC = SONUC & "|" & ST.SKART.S03_birim
                        SONUC = SONUC & "|" & "1"
                End Select
                SONUC = SONUC & "|" & FIY
                SONUC = SONUC & "|" & FIY
                SONUC = SONUC & "|" & ST.SKART.S03_birim
                SONUC = SONUC & "|" & ST.SKART.S07_alkdvor
                SONUC = SONUC & "|" & ST.SKART.S08_sakdvor
                SONUC = SONUC & "|" & ST.SKART.S100_BarMik
                SONUC = SONUC & "|" & ST.SKART.S98_Carpan
                SONUC = SONUC & "|" & ST.SKART.S101_asorti_id
                SONUC = SONUC & "|" & ST.SKART.S43_a_reftip
                SONUC = SONUC & "|" & ST.SKART.S22_selo
                KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
                KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
                Return SONUC
            End If
        End If

        If KOMUT = "@fark" Then
            Dim GelenVeri(2) As String
            GelenVeri = sq.Split(" ")
            DepoTransFarkIslem(Val(GelenVeri(1)), Val(GelenVeri(2)))

        End If
        If KOMUT = "@farksil" Then
            Dim GelenVeri(2) As String
            GelenVeri = sq.Split(" ")
            DepoTransFarkSil(Val(GelenVeri(1)), Val(GelenVeri(2)))

        End If

        If KOMUT = "@deposifirla" Then
            Dim GelenVeri(3) As String
            Dim Sonuc1 As String = ""
            GelenVeri = sq.Split(" ")
            Sonuc1 = DepoSifirla(Val(GelenVeri(1)), Val(GelenVeri(2)), GelenVeri(3))
            Return Sonuc1
        End If

        If KOMUT = "ankapat" Then
            '--------------------------------------------------------------------------------
            'Adisyonlu Nakita Kasadan Kapat
            '--------------------------------------------------------------------------------
            'data = "HATA"
            '"ANKAPAT PER=" & PERSON_ID & " MASA=" & Val(Label5.Tag) & " PDA=" & "PDA1"
            sq = Trim(L)
            Try
                Call Masa_Kapat("ANKAPAT", sq)
                Return "True"
            Catch ex As Exception
                Return "False"
            End Try
        End If


        Dim P1 As Integer = 0
        Dim P2 As Integer = 0
        Dim P3 As Integer = 0
        Dim P4 As Integer = 0
        Dim P5 As Integer = 0
        Dim P6 As Integer = 0
        Dim P7 As Integer = 0
        Dim P8 As Integer = 0
        Dim P9 As Integer = 0
        Dim PER_ID As Long = 0
        Dim USR_ID As Long = 0
        Dim PER_AD As String = ""
        Dim MAS_ID As Long = 0
        Dim KID As Double = 0
        Dim KTUT As Double = 0
        Dim BID As Long = 0
        Dim BTUT As Long = 0
        Dim SQ1 As String = ""
        Dim SQ2 As String = ""
        Dim SQ3 As String = ""
        Dim Tur As String = ""
        Dim DEP_ID As Long = 0
        Dim KAS_ID As Long = 0
        Dim BNK_ID As Long = 0
        Dim PDA As String = ""
        Dim ADSNO As Long = 0
        Dim OTUTAR As Double = 0
        Dim TTUTAR As Double = 0

        ' DEĞİŞKENLER (EMRE)
        Dim MASA_ID As Long = 0
        Dim USER_ID As Long = 0
        Dim ADISYON_ID As Long = 0
        Dim VARDIYA_ID As Long = 0
        Dim KASA_ID As Long = 0
        Dim KASA_TUTAR As Double = 0
        Dim BANKA_ID As Long = 0
        Dim BANKA_TUTAR As Double = 0

        If KOMUT = "ykapat" Then

            sq = Trim(L)
            Tur = UCase(KOMUT)
            If Tur = "YKAPAT" Then

                P1 = InStr(sq, "PER=")
                P2 = InStr(sq, "MASA=")
                P3 = InStr(sq, "PDA=")
                P4 = InStr(sq, "KID=")
                P5 = InStr(sq, "KTUTAR=")
                P6 = InStr(sq, "BID=")
                P7 = InStr(sq, "BTUT=")
                P8 = InStr(sq, "ADSNO=")
                P9 = InStr(sq, "VARID=")

                USR_ID = Val(Trim(Mid(sq, P1 + 4, P2 - (P1 + 4))))
                MAS_ID = Val(Trim(Mid(sq, P2 + 5, P3 - (P2 + 5))))
                PDA = Val(Trim(Mid(sq, P3 + 4, P4 - (P3 + 4))))
                KID = Val(Trim(Mid(sq, P4 + 4, P5 - (P4 + 4))))
                KTUT = Val(Trim(Mid(sq, P5 + 7, P6 - (P5 + 7))))
                BID = Val(Trim(Mid(sq, P6 + 4, P7 - (P6 + 4))))
                BTUT = Val(Trim(Mid(sq, P7 + 5, P8 - (P7 + 5))))

                ADSNO = Trim(Mid(sq, P8 + 6, P9 - (P8 + 6)))


                ' DEĞİŞKENLER (EMRE)
                USER_ID = Val(Trim(Mid(sq, P1 + 4, P2 - (P1 + 4))))
                MASA_ID = Val(Trim(Mid(sq, P2 + 5, P3 - (P2 + 5))))
                ADISYON_ID = Trim(Mid(sq, P8 + 6, P9 - (P8 + 6)))
                VARDIYA_ID = Trim(Mid(sq, P9 + 6, Len(sq)))
                KASA_ID = Val(Trim(Mid(sq, P4 + 4, P5 - (P4 + 4))))
                KASA_TUTAR = Val(Trim(Mid(sq, P5 + 7, P6 - (P5 + 7))))
                BANKA_ID = Val(Trim(Mid(sq, P6 + 4, P7 - (P6 + 4))))
                BANKA_TUTAR = Val(Trim(Mid(sq, P7 + 5, P8 - (P7 + 5))))
            End If
            Try
                If Not KASA_TUTAR = 0 Then
                    Call MEN_TAHSILAT(MASA_ID, ADISYON_ID, "KASA", KASA_ID, KASA_TUTAR)
                End If
                If Not BANKA_TUTAR = 0 Then
                    Call MEN_TAHSILAT(MASA_ID, ADISYON_ID, "BANKA", BANKA_ID, BANKA_TUTAR)
                End If

                ' Adisyon Tutarı ile Yapılan Ödemeler Aynı ise Masa Kapat İşlemi Devreye Girsin.
                Dim tahsilatSql As String = ""
                Dim adisyonSql As String = ""
                Dim TAHSILAT As Double = 0
                Dim ADISYON_TUTAR As Double = 0

                tahsilatSql = "SELECT ISNULL(SUM(a_tutar), 0) AS tahsilat"
                tahsilatSql = tahsilatSql & " FROM " & KUL.tfrm & "men_masa_tahsil"
                tahsilatSql = tahsilatSql & " WHERE (a_masid = " & MASA_ID & ") AND (a_master_id = " & ADISYON_ID & ")"
                DT = SQL_TABLO(tahsilatSql, "T", False, False, False)
                TAHSILAT = NULD(DT, 0, "tahsilat")

                adisyonSql = "SELECT ISNULL(SUM(a_tut), 0) AS adisyonTutar"
                adisyonSql = adisyonSql & " FROM " & KUL.tfrm & "men_adisyon"
                adisyonSql = adisyonSql & " WHERE (a_mid = " & MASA_ID & ") AND (a_master_id = " & ADISYON_ID & ")"
                DT = SQL_TABLO(adisyonSql, "T", False, False, False)
                ADISYON_TUTAR = NULD(DT, 0, "adisyonTutar")

                If TAHSILAT = ADISYON_TUTAR Then

                    Call Masa_Kapat("YKAPAT", sq)

                End If

                Return "True"
            Catch ex As Exception
                Return "False"
            End Try


        End If
        If KOMUT = "akkapat" Then
            '--------------------------------------------------------------------------------
            'Adisyonlu Kredi Bankadan Kapat
            '--------------------------------------------------------------------------------
            'data = "HATA"
            '"AKKAPAT PER=" & PERSON_ID & " MASA=" & Val(Label5.Tag) & " PDA=" & "PDA1"
            sq = Trim(L)
            Try
                Call Masa_Kapat("AKKAPAT", sq)
                Return "True"
            Catch ex As Exception
                Return "False"
            End Try

        End If

        If KOMUT = "nkapat" Then
            '--------------------------------------------------------------------------------
            'Nakita Kasadan Kapat
            '--------------------------------------------------------------------------------
            'data = "HATA"
            '"NKAPAT PER=" & PERSON_ID & " MASA=" & Val(Label5.Tag)

            sq = Trim(L)
            Try
                Call Masa_Kapat("NKAPAT", sq)
                Return "True"
            Catch ex As Exception
                Return "False"
            End Try

        End If


        If KOMUT = "kkapat" Then
            '--------------------------------------------------------------------------------
            'Kredi Bankadan Kapat
            '--------------------------------------------------------------------------------
            'data = "HATA"
            '"KKAPAT PER=" & PERSON_ID & " MASA=" & Val(Label5.Tag)
            sq = Trim(L)
            Try
                Call Masa_Kapat("KKAPAT", sq)
                Return "True"
            Catch ex As Exception
                Return "False"
            End Try
        End If


        If KOMUT = "adısyon" Or KOMUT = "adisyon" Then
            '"ADISYON MASA=" & Val(Label5.Tag)
            '"ADISYON MASA=" & Val(Label5.Tag) & " PDA=" & PDA_AD & " GARSON=" & PERSON_ID
            sq = Trim(L)
            Try
                Call Masa_Adisyon(sq)
                Return "True"
            Catch ex As Exception
                Return "False"
            End Try
        End If


        If KOMUT = "mtası" Or KOMUT = "mtasi" Then
            '"MTASI M1=" & 1 & " M2=" & 2
            sq = Trim(L)
            Try
                Call Masa_Tasi(sq)
                Return "True"
            Catch ex As Exception
                Return "False"
            End Try
        End If



        If KOMUT = "adsekle" Then
            '"MTASI M1=" & 1 & " M2=" & 2
            sq = Trim(L)
            Try
                Call ADS_EKLE(sq)
                Return "True"
            Catch ex As Exception
                Return "False"
            End Try
        End If



        If KOMUT = "adsguncelle" Or KOMUT = "adsgüncelle" Then
            '"MTASI M1=" & 1 & " M2=" & 2
            sq = Trim(L)
            Try
                Call ADS_GUNCELLE(sq)
                Return "True"
            Catch ex As Exception
                Return "False"
            End Try
        End If


        If KOMUT = "masayaısle" Or KOMUT = "masayaisle" Then
            'MASAYAISLE MASA=" & Val(Label5.Tag) & " PDA=" & PDA_AD & " ADS_NO=" & PERSON_ID
            sq = Trim(L)
            Try
                Call Masaya_Isle(sq)
                Return "True"
            Catch ex As Exception
                Return "False"
            End Try
        End If


        If KOMUT = "masadansıl" Or KOMUT = "masadansil" Then
            'sq = "MASADANSIL "
            'sq = sq & " MASA=" & Val(Label10.Text)
            'sq = sq & " SIRA=" & Val(Label11.Text)
            'sq = sq & " URUN=" & Val(Label5.Tag)
            'sq = sq & " PDA=" & PDA_AD
            'sq = sq & " GARSON=" & PERSON_ID
            'sq = sq & " MIKTAR=" & Miktar
            'sq = sq & " ADS_NO=" & Val(L_AdsNo.Text)
            'sq = sq & " NEDEN=" & ComboBox1.SelectedValue


            sq = Trim(L)
            Try
                Call Masaya_Sil(sq)
                Return "True"
            Catch ex As Exception
                Return "False"
            End Try
        End If


        Return "True"

    End Function


    <WebMethod()>
    Public Function Web_Asorti_Ekle(ByVal Asorti_ID As Long, ByVal Fat_ID As Long, ByVal Fat_Tur As Integer, ByVal Fat_Sira As Integer, ByVal Fat_Miktar As Double, ByVal FirmaID As Integer) As Boolean


        If Fat_Tur = 33 Or Fat_Tur = 34 Then
            If Asorti_Ekle_Siparis(Asorti_ID, Fat_ID, Fat_Tur, Fat_Sira, Fat_Miktar, FirmaID) = True Then
                Return True
            Else
                Return False
            End If
        Else
            If Asorti_Ekle_Fatura(Asorti_ID, Fat_ID, Fat_Tur, Fat_Sira, Fat_Miktar, FirmaID) = True Then
                Return True
            Else
                Return False
            End If

        End If



    End Function
    Private Function Asorti_Ekle_Fatura(ByVal Asorti_ID As Long, ByVal Fat_ID As Long, ByVal Fat_Tur As Integer, ByVal Fat_Sira As Integer, ByVal Fat_Miktar As Double, ByVal FirmaID As Integer) As Boolean
        Dim SQ As String = ""
        Dim DT As New DataTable
        Dim X As Long = 0
        Dim Y As Long = 0
        Dim Firma As String

        B_SQL = connstr
        B_PRJ = "PDA_TRANS"
        SQL_TUR = "2005"


        If FirmaID <> 0 Then
            Firma = New String("0", 3 - FirmaID.ToString.Length) & FirmaID
            KUL.tfrm = "tbl_" & Firma & "_"
            KUL.tper = "tbl_" & Firma & "_" & "01" & "_"

        Else
            KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
            KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        End If

        Dim FT As New X_Fatura
        Dim KL As New X_Fatura.Veri_Detay

        Dim M As Double = 0



        If FT.Fatura_Bul(Fat_Tur, Fat_ID) = True Then
            For X = 0 To FT.Fatura_Satirlari.Count - 1
                If FT.Fatura_Satirlari(X).Satir_Sirasi = Fat_Sira Then
                    Y = X
                    Exit For
                End If
            Next




            SQ = ""
            SQ = "SELECT  stok_asortidet.a_mas_id, stok_asortidet.a_stok_id, stok_asortidet.a_miktar, stok.a_kod, stok.a_adi, stok.a_birim, stok.a_alkdvor, stok.a_sakdvor, stok.a_aisk, stok.a_sisk, stok.a_afiyat, stok.a_sfiyat"
            SQ = SQ & " FROM "
            SQ = SQ & " " & KUL.tfrm & "stok_asortidet AS stok_asortidet INNER JOIN"
            SQ = SQ & " " & KUL.tfrm & "stok AS stok ON stok_asortidet.a_stok_id = stok.a_id"
            SQ = SQ & " WHERE (stok_asortidet.a_mas_id = " & Asorti_ID & ")"
            DT = SQL_TABLO(SQ, "T", False, False, False)
            For X = 0 To DT.Rows.Count - 1

                KL = New bym_dll.X_Fatura.Veri_Detay

                'KL.Depo_Id = 1
                KL.Depo_Id = FT.Fatura_Baslik.Depo_Id

                KL.Fatura_Cikti = "E"
                KL.Fatura_Fiyat = FT.Fatura_Satirlari(Y).Fatura_Fiyat ' NULD(DT, X, "a_sfiyat")
                KL.Masref_Merkezi = 0
                KL.Masref_Yeri = 0
                KL.Satir_Aciklamasi = NULA(DT, X, "a_adi")
                KL.Satir_Doviz_Id = 0
                KL.Satir_Doviz_Kuru = 1
                KL.Satir_Durum_Tarihi = TTAR.TARIH
                KL.Satir_Durumu = ""
                '18.10.2012
                'KL.Satir_Genel_İskonto_Orani =0
                KL.Satir_Genel_İskonto_Orani = 0
                KL.Satir_Genel_İskonto_Tutari = 0
                KL.Satir_Iskonto_1_Oran = FT.Fatura_Satirlari(Y).Satir_Iskonto_1_Oran
                KL.Satir_Iskonto_2_Oran = FT.Fatura_Satirlari(Y).Satir_Iskonto_2_Oran
                KL.Satir_Iskonto_3_Oran = FT.Fatura_Satirlari(Y).Satir_Iskonto_3_Oran
                KL.Satir_Iskonto_4_Oran = FT.Fatura_Satirlari(Y).Satir_Iskonto_4_Oran
                KL.Satir_Iskonto_5_Oran = FT.Fatura_Satirlari(Y).Satir_Iskonto_5_Oran
                KL.Satir_Iskonto_6_Oran = FT.Fatura_Satirlari(Y).Satir_Iskonto_6_Oran
                KL.Satir_Islem_Birim_Fiyati = FT.Fatura_Satirlari(Y).Satir_Islem_Birim_Fiyati
                KL.Satir_Islem_Birim_Miktari = NULD(DT, X, "a_miktar") * Fat_Miktar 'FT.Fatura_Satirlari(Y).Satir_Islem_Birim_Miktari
                KL.Satir_Islem_Birimi = FT.Fatura_Satirlari(Y).Satir_Islem_Birimi
                KL.Satir_Kdv_Orani = FT.Fatura_Satirlari(Y).Satir_Kdv_Orani
                KL.Satir_Ozel_Iletisim_Vergi_Orani = 0
                KL.Satir_Ozel_Tuketim_Vergisi_Orani = 0
                KL.Satir_Personel_Id = 1
                KL.Stok_Id = NULN(DT, X, "a_stok_id")

                KL.Satir_Asorti_Id = Asorti_ID
                KL.Satir_Asorti_Miktari = Fat_Miktar
                KL.Satir_Bagli_Satir_Id = Fat_Sira


                'Evrak Sipariş Bağlantısını Yansıt.
                '-------------------------------------------------
                KL.Siparis_Turu = 0
                KL.Siparis_Id = 0
                KL.Siparis_Sira = 0
                '-------------------------------------------------
                'FT.Fatura_Satirlari.Add(KL)
                FT.Fatura_Satir_Ekle(KL)
                M = M + (NULD(DT, X, "a_miktar") * Fat_Miktar)

            Next

            FT.Fatura_Satirlari(Y).Satir_Islem_Birim_Miktari = M
            FT.Fatura_Satirlari(Y).Satir_Asorti_Id = Asorti_ID
            FT.Fatura_Satirlari(Y).Satir_Asorti_Miktari = Fat_Miktar
            FT.Fatura_Satirlari(Y).Satir_Bagli_Satir_Id = 0
            FT.Fatura_Satir_Hesapla(Y)


            FT.Fatura_Guncelle()
            KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
            KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        Else
            KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
            KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
            Return False
        End If

        Return True
    End Function
    Private Function Asorti_Ekle_Siparis(ByVal Asorti_ID As Long, ByVal Fat_ID As Long, ByVal Fat_Tur As Integer, ByVal Fat_Sira As Integer, ByVal Fat_Miktar As Double, ByVal FirmaID As Integer) As Boolean

        Dim SQ As String = ""
        Dim DT As New DataTable
        Dim DT2 As New DataTable
        Dim DT3 As New DataTable

        Dim EBS As Long = 0

        Dim X As Long = 0
        Dim Y As Long = 0
        Dim Firma As String = ""
        Dim M As Double = 0


        B_SQL = connstr
        B_PRJ = "PDA_TRANS"
        SQL_TUR = "2005"

        If FirmaID <> 0 Then
            Firma = New String("0", 3 - FirmaID.ToString.Length) & FirmaID
            KUL.tfrm = "tbl_" & Firma & "_"
            KUL.tper = "tbl_" & Firma & "_" & "01" & "_"

        Else
            KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
            KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        End If


        SQ = ""
        SQ = "SELECT "
        SQ = SQ & " a_tur, a_id, a_sira, a_tarih, a_teslim, a_cari_id, a_stok_id, a_ambar_id, "
        SQ = SQ & " a_mik, a_kapamik, a_brmfiy, a_kdv, a_kdvtut, a_isk, a_isktut, a_tutar, a_indirim, "
        SQ = SQ & " a_gsisko, a_gsiskt, a_ack, a_tel, a_atar, a_durum, a_sbrm, a_sbrmcrp, a_sbrmmik, a_sbrmfiy, "
        SQ = SQ & " a_did, a_dtut, a_dkur ,a_bloke, a_kesin, a_recete_id, a_perid, a_refsira, a_asorti_id, a_asorti_mik "
        SQ = SQ & " FROM " & KUL.tper & "sipdet"
        SQ = SQ & " WHERE (a_tur = " & Fat_Tur & ") AND (a_id = " & Fat_ID & ")"
        DT2 = SQL_TABLO(SQ, "T", False, False, False)


        SQ = ""
        SQ = "SELECT  stok_asortidet.a_mas_id, stok_asortidet.a_stok_id, stok_asortidet.a_miktar, stok.a_kod, stok.a_adi, stok.a_birim, stok.a_alkdvor, stok.a_sakdvor, stok.a_aisk, stok.a_sisk, stok.a_afiyat, stok.a_sfiyat"
        SQ = SQ & " FROM "
        SQ = SQ & " " & KUL.tfrm & "stok_asortidet AS stok_asortidet INNER JOIN"
        SQ = SQ & " " & KUL.tfrm & "stok AS stok ON stok_asortidet.a_stok_id = stok.a_id"
        SQ = SQ & " WHERE (stok_asortidet.a_mas_id = " & Asorti_ID & ")"
        DT = SQL_TABLO(SQ, "T", False, False, False)
        For X = 0 To DT.Rows.Count - 1


            SQ = ""
            SQ = "SELECT MAX(a_sira) AS EBID"
            SQ = SQ & " FROM " & KUL.tper & "sipdet"
            SQ = SQ & " WHERE (a_tur = " & Fat_Tur & ") AND (a_id = " & Fat_ID & ")"
            DT3 = SQL_TABLO(SQ, "Y", False, False, False)
            EBS = NULN(DT3, 0, "EBID") + 1


            SQ = ""
            SQ = "INSERT INTO " & KUL.tper & "sipdet"
            SQ = SQ & " (a_tur, a_id, a_sira, a_tarih, a_teslim, a_cari_id, a_stok_id, a_ambar_id, "
            SQ = SQ & " a_mik, a_kapamik, a_brmfiy, a_kdv, a_kdvtut, a_isk, a_isktut, a_tutar, a_indirim, "
            SQ = SQ & " a_gsisko, a_gsiskt, a_ack, a_tel, a_atar, a_durum, a_sbrm, a_sbrmcrp, a_sbrmmik, a_sbrmfiy, "
            SQ = SQ & " a_did, a_dtut, a_dkur ,a_bloke, a_kesin, a_recete_id, a_perid, a_refsira, a_asorti_id, a_asorti_mik ) "
            SQ = SQ & " Select "
            SQ = SQ & " " & Fat_Tur & " as a_tur,"
            SQ = SQ & " " & Fat_ID & " as a_id,"
            SQ = SQ & " " & EBS & " as a_sira,"
            SQ = SQ & " N'" & TTAR.TR2UK(NULA(DT2, 0, "a_tarih")) & "' as a_tarih,"
            SQ = SQ & " N'" & TTAR.TR2UK(NULA(DT2, 0, "a_teslim")) & "' as a_teslim,"
            SQ = SQ & " " & NULN(DT2, 0, "a_cari_id") & " as a_cari_id,"
            SQ = SQ & " " & NULN(DT, X, "a_stok_id") & " as a_stok_id,"
            SQ = SQ & " " & NULN(DT2, 0, "a_ambar_id") & " as a_ambar_id,"

            SQ = SQ & " " & NULD(DT, X, "a_miktar") * Fat_Miktar & " as a_mik,"

            SQ = SQ & " " & 0 & " as a_kapamik,"
            SQ = SQ & " " & NULD(DT2, 0, "a_brmfiy") & " as a_brmfiy,"
            SQ = SQ & " " & NULD(DT2, 0, "a_kdv") & " as a_kdv,"
            SQ = SQ & " " & NULD(DT2, 0, "a_kdvtut") & " as a_kdvtut,"
            SQ = SQ & " " & NULD(DT2, 0, "a_isk") & " as a_isk,"
            SQ = SQ & " " & NULD(DT2, 0, "a_isktut") & " as a_isktut,"
            SQ = SQ & " " & NULD(DT2, 0, "a_tutar") & " as a_tutar,"
            SQ = SQ & " " & NULD(DT2, 0, "a_indirim") & " as a_indirim,"
            SQ = SQ & " " & NULD(DT2, 0, "a_gsisko") & " as a_gsisko,"
            SQ = SQ & " " & NULD(DT2, 0, "a_gsiskt") & " as a_gsiskt,"

            SQ = SQ & " N'" & NULA(DT, X, "a_adi") & "' as a_ack,"

            SQ = SQ & " N'" & "" & "' as a_tel,"
            SQ = SQ & "  " & TTAR.Deger_Oku(NULA(DT2, 0, "a_atar")) & " as a_atar ,"

            SQ = SQ & " N'" & "" & "' as a_durum,"

            SQ = SQ & " N'" & NULA(DT2, 0, "a_sbrm") & "' as a_sbrm,"
            SQ = SQ & " " & NULD(DT2, 0, "a_sbrmcrp") & " as a_sbrmcrp,"
            SQ = SQ & " " & NULD(DT2, 0, "a_sbrmmik") & " as a_sbrmmik,"
            SQ = SQ & " " & NULD(DT2, 0, "a_sbrmfiy") & " as a_sbrmfiy,"
            SQ = SQ & " " & NULN(DT2, 0, "a_did") & " as a_did,"
            SQ = SQ & " " & NULD(DT2, 0, "a_dtut") & " as a_dtut,"
            SQ = SQ & " " & NULD(DT2, 0, "a_dkur") & " as a_dkur,"

            SQ = SQ & " 0 as a_bloke,"
            SQ = SQ & " 0 as a_kesin,"
            SQ = SQ & " 0 as a_recete_id,"
            SQ = SQ & " " & NULD(DT2, 0, "a_perid") & " as a_perid, "

            SQ = SQ & " " & Fat_Sira & " as a_refsira,"
            SQ = SQ & " " & Asorti_ID & " as a_asorti_id,"
            SQ = SQ & " " & Fat_Miktar & " as a_asorti_mik"

            If SQL_KOS(SQ) = True Then
            End If

            M = M + NULD(DT, X, "a_miktar") * Fat_Miktar

        Next

        SQ = "UPDATE " & KUL.tper & "sipdet"
        SQ = SQ & " SET "
        SQ = SQ & "   a_refsira    = NULL"
        SQ = SQ & " , a_asorti_id  = " & Asorti_ID & ""
        SQ = SQ & " , a_asorti_mik = " & Fat_Miktar
        SQ = SQ & " , a_sbrmmik    = " & M & " "
        SQ = SQ & " , a_mik        = " & M & " "
        SQ = SQ & " ,a_tutar       = " & NULD(DT2, 0, "a_brmfiy") * M & " "
        SQ = SQ & " WHERE (a_tur = " & Fat_Tur & ") AND (a_id = " & Fat_ID & ") AND (a_sira = " & Fat_Sira & ")"
        If SQL_KOS(SQ) = True Then
        End If

        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
    End Function
    Private Function Sevk_Islem(ByVal K As String) As String

        Dim SQ As String = ""
        Dim DT As New DataTable
        Dim ST As New STOK_BILGI

        Dim S As String = ""
        Dim L() As String

        'SQ = "@SEVK " & Val(Label6.Tag) & " " & TextBox1.Text & " " & Val(TextBox3.Text)
        L = K.Split(" ")

        'MKUTU(L(0) + " " + L(1) + " " + L(2) + " " + L(3), "T", "Bilgi")

        Dim ID As Long = 0
        Dim BR As String = ""
        Dim MK As Double = 0
        Dim SI As Long = 0

        ID = Val(L(1))
        BR = L(2)
        MK = Val(L(3))
        If MK = 0 Then MK = 1

        If ST.BUL(BR) = False Then
            S = "Barkod Kaydı Bulunamadı."
            Return S
        End If

        SI = ST.SKART.S00_id

        If ST.SKART.S100_BarMik <> 0 Then
            MK = ST.SKART.S100_BarMik
        End If


        SQ = "SELECT a_id, a_sira, a_stok_id, a_miktar, a_kapamik, a_miktar - a_kapamik AS Kalan, a_sbrmmik, a_sbrmcrp, a_pdasevk"
        SQ = SQ & " FROM " & KUL.tper & "depistdet"
        SQ = SQ & " WHERE (a_id = " & ID & ") AND (a_stok_id = " & SI & ")"
        DT = SQL_TABLO(B_SQL, SQ, "T", False, False, False)
        If DT.Rows.Count = 0 Then
            S = "Ürün Yüklemede Yok."
            Return S
        End If






        Select Case ST.SKART.S99_Nerede_Buldun

            Case STOK_BILGI.Bul_Tur.Stok_1BrmBarkod
                'FIY = FIY * ST_KART.SKART.S25_a_1brmcar
                'TGrid1.DGV.Item("sbrmmik", SAT).Value = Miktar
                'TGrid1.DGV.Item("sabirim", SAT).Value = ST_KART.SKART.S24_a_1birim
                'TGrid1.DGV.Item("sbrmfiy", SAT).Value = FIY
                'TGrid1.DGV.Item("sbrmcarp", SAT).Value = ST_KART.SKART.S25_a_1brmcar

            Case STOK_BILGI.Bul_Tur.Stok_2BrmBarkod
                'FIY = FIY * ST_KART.SKART.S30_a_2brmcar
                'TGrid1.DGV.Item("sbrmmik", SAT).Value = Miktar
                'TGrid1.DGV.Item("sabirim", SAT).Value = ST_KART.SKART.S29_a_2birim
                'TGrid1.DGV.Item("sbrmfiy", SAT).Value = FIY
                'TGrid1.DGV.Item("sbrmcarp", SAT).Value = ST_KART.SKART.S30_a_2brmcar

            Case Else
                'TGrid1.DGV.Item("sbrmmik", SAT).Value = Miktar
                'TGrid1.DGV.Item("sabirim", SAT).Value = ST_KART.SKART.S03_birim
                'TGrid1.DGV.Item("sbrmfiy", SAT).Value = NULD(TGrid1.DGV, SAT, "sfiyat")
                'TGrid1.DGV.Item("sbrmcarp", SAT).Value = 1
        End Select





        SQ = ""
        SQ = "UPDATE " & KUL.tper & "depistdet"
        SQ = SQ & " SET "
        SQ = SQ & " a_pdasevk = " & NULD(DT, 0, "a_pdasevk") + MK
        SQ = SQ & " WHERE "
        SQ = SQ & " a_id = " & ID & ""
        SQ = SQ & " AND a_sira = " & NULN(DT, 0, "a_sira") & ""
        SQL_KOS(B_SQL, SQ, False)


        If NULD(DT, 0, "Kalan") - (MK + NULD(DT, 0, "a_pdasevk")) < 0 Then
            S = "Fazla Yükleme. K:" & NULD(DT, 0, "Kalan") - NULD(DT, 0, "a_pdasevk")
        Else
            S = "Yükleme Kayda Alındı."
        End If

        Return S



    End Function

    Private Function Trans_Islem(ByVal K As String) As String

        Dim SQ As String = ""
        Dim DT As New DataTable
        Dim DET As New DataTable
        Dim ST As New STOK_BILGI


        Dim S As String = ""
        Dim L() As String

        'SQ = "@TRANS " & Val(Label6.Tag)
        L = K.Split(" ")

        Dim FID As Long = 0
        FID = Val(L(1))




        Dim T As Double = 0
        Dim X As Integer = 0
        Dim A As Integer = 0
        Dim B As Integer = 0

        Dim ID As Integer = 0
        Dim C_DEPO As Long = 0
        Dim G_DEPO As Long = 0

        'a_pdasevk

        SQ = ""
        SQ = "SELECT mas.a_id, mas.a_cdepo_id, mas.a_gdepo_id, det.a_sira, det.a_stok_id, det.a_miktar, det.a_kapamik, det.a_hesmik, det.a_sbrm, det.a_sbrmcrp, det.a_sbrmmik, det.a_fiyat, det.a_pdasevk"
        SQ = SQ & " FROM "
        SQ = SQ & " " & KUL.tper & "depistmas AS mas INNER JOIN"
        SQ = SQ & " " & KUL.tper & "depistdet AS det ON mas.a_id = det.a_id"
        SQ = SQ & " WHERE (mas.a_id = " & FID & ") and (det.a_pdasevk > 0)"
        DET = SQL_TABLO(B_SQL, SQ, "T", False, False, False)

        If DET.Rows.Count = 0 Then
            S = "Boş Transfer Kaydedilemez."
            Return S
        End If


        C_DEPO = NULN(DET, 0, "a_cdepo_id")
        G_DEPO = NULN(DET, 0, "a_gdepo_id")


        ID = TSAY.MAX_FIS_DEPO(49, 1)

        SQ = ""
        SQ = "INSERT INTO " & KUL.tper & "stkhmas"
        SQ = SQ & " (a_gc, a_tur, a_id, a_kod, a_bno, a_ack, a_tarih, a_cari_id, a_ambar_id, a_oplan_id, a_isktop, a_kdvtop, a_tuttop, a_cuser, a_cdate, a_ctime, a_muser, a_mdate, a_mtime, a_bloke, a_kesin) "
        SQ = SQ & " Select "
        SQ = SQ & " 'C' as a_gc,"
        SQ = SQ & " " & 49 & " as a_tur,"
        SQ = SQ & " " & ID & " as a_id,"
        SQ = SQ & " N'" & "ISTEK-" & ID & "' as a_kod,"
        SQ = SQ & " N'" & "@ISTEK@" & "' as a_bno,"
        SQ = SQ & " N'" & "" & "' as a_ack,"
        SQ = SQ & " N'" & TTAR.TR2UK(TTAR.TARIH) & "' as a_tarih,"
        SQ = SQ & " 0 as a_cari_id,"
        'Çıkış Ambarı
        SQ = SQ & " " & C_DEPO & " as a_ambar_id,"
        'Giriş Ambarı
        SQ = SQ & " " & G_DEPO & " as a_oplan_id,"
        SQ = SQ & " 0 as a_isktop,"
        SQ = SQ & " 0 as a_kdvtop,"
        SQ = SQ & " 0 as a_tuttop,"
        SQ = SQ & " " & Val(KUL.KOD) & " as a_cuser,"
        SQ = SQ & " N'" & TTAR.TR2UK(Date.Now.ToShortDateString) & "' as a_cdate,"
        SQ = SQ & " N'" & Date.Now.ToShortTimeString & "' as a_ctime,"
        SQ = SQ & " " & Val(KUL.KOD) & " as a_muser,"
        SQ = SQ & " N'" & TTAR.TR2UK(Date.Now.ToShortDateString) & "' as a_mdate,"
        SQ = SQ & " N'" & Date.Now.ToShortTimeString & "' as a_mtime,"
        SQ = SQ & " 0 as a_bloke,"
        SQ = SQ & " 0 as a_kesin;"
        SQL_KOS(B_SQL, SQ, False)




        For X = 0 To DET.Rows.Count - 1

            If NULD(DET, X, "a_pdasevk") > 0 Then

                SQ = "Update " & KUL.tper & "depistdet"
                SQ = SQ & " SET  "
                SQ = SQ & " a_pdasevk = 0 ,"
                SQ = SQ & " a_kapamik = a_kapamik + " & NULD(DET, X, "a_pdasevk") & ""
                SQ = SQ & " WHERE (a_id = " & FID & ") and (a_sira = " & NULD(DET, X, "a_sira") & ")"
                SQL_KOS(B_SQL, SQ, False)


                SQ = ""
                SQ = "INSERT INTO " & KUL.tper & "stkhdet"
                SQ = SQ & " (a_gc, a_tur, a_id, a_sira, a_tarih, a_cari_id, a_stok_id, a_ambar_id, a_mik, a_brmfiy, a_kdv, a_kdvtut, a_isk, a_isktut, a_tutar, a_ack, a_bloke, a_kesin) "
                SQ = SQ & " Select "
                SQ = SQ & " 'C' as a_gc,"
                SQ = SQ & " " & 49 & " as a_tur,"
                SQ = SQ & " " & ID & " as a_id,"
                SQ = SQ & " " & X & " as a_sira,"
                SQ = SQ & " N'" & TTAR.TR2UK(TTAR.TARIH) & "' as a_tarih,"
                SQ = SQ & " 0 as a_cari_id,"
                SQ = SQ & " " & NULN(DET, X, "a_stok_id") & " as a_stok_id,"
                SQ = SQ & " " & C_DEPO & " as a_ambar_id,"
                SQ = SQ & " " & NULD(DET, X, "a_pdasevk") & " as a_mik,"
                SQ = SQ & " " & NULD(DET, X, "a_fiyat") & " as a_brmfiy,"
                SQ = SQ & " 0 as a_kdv,"
                SQ = SQ & " 0 as a_kdvtut,"
                SQ = SQ & " " & 0 & " as a_isk,"
                SQ = SQ & " " & 0 & " as a_isktut,"
                SQ = SQ & " " & NULD(DET, X, "a_pdasevk") * NULD(DET, X, "a_fiyat") & " as a_tutar,"
                SQ = SQ & " N'" & "" & "' as a_ack,"
                SQ = SQ & " 0 as a_bloke,"
                SQ = SQ & " 0 as a_kesin;"

                If SQL_KOS(B_SQL, SQ, False) = True Then
                    BG.depo_stok_miktar(C_DEPO, NULN(DET, X, "a_stok_id"), NULD(DET, X, "a_pdasevk"), "C+")
                    SQ = "Update " & KUL.tper & "stkhmas"
                    SQ = SQ & " SET  a_tuttop = a_tuttop + " & NULD(DET, X, "a_pdasevk") * NULD(DET, X, "a_fiyat") & ""
                    SQ = SQ & " WHERE   (a_tur = 49) AND (a_id = " & ID & ")"
                    SQL_KOS(B_SQL, SQ, False)
                End If


                SQ = ""
                SQ = "INSERT INTO " & KUL.tper & "stkhdet"
                SQ = SQ & " (a_gc, a_tur, a_id, a_sira, a_tarih, a_cari_id, a_stok_id, a_ambar_id, a_mik, a_brmfiy, a_kdv, a_kdvtut, a_isk, a_isktut, a_tutar, a_ack, a_bloke, a_kesin) "
                SQ = SQ & " Select "
                SQ = SQ & " 'G' as a_gc,"
                SQ = SQ & " " & 50 & " as a_tur,"
                SQ = SQ & " " & ID & " as a_id,"
                SQ = SQ & " " & X & " as a_sira,"
                SQ = SQ & " N'" & TTAR.TR2UK(TTAR.TARIH) & "' as a_tarih,"
                SQ = SQ & " 0 as a_cari_id,"
                SQ = SQ & " " & NULN(DET, X, "a_stok_id") & " as a_stok_id,"
                SQ = SQ & " " & G_DEPO & " as a_ambar_id,"
                SQ = SQ & " " & NULD(DET, X, "a_pdasevk") & " as a_mik,"
                SQ = SQ & " " & NULD(DET, X, "a_fiyat") & " as a_brmfiy,"
                SQ = SQ & " 0 as a_kdv,"
                SQ = SQ & " 0 as a_kdvtut,"
                SQ = SQ & " " & 0 & " as a_isk,"
                SQ = SQ & " " & 0 & " as a_isktut,"
                SQ = SQ & " " & NULD(DET, X, "a_pdasevk") * NULD(DET, X, "a_fiyat") & " as a_tutar,"
                SQ = SQ & " N'" & "" & "' as a_ack,"
                SQ = SQ & " 0 as a_bloke,"
                SQ = SQ & " 0 as a_kesin;"
                If SQL_KOS(B_SQL, SQ, False) = True Then
                    BG.depo_stok_miktar(G_DEPO, NULN(DET, X, "a_stok_id"), NULD(DET, X, "a_pdasevk"), "G+")
                End If
            End If
        Next

        SQ = ""
        SQ = "INSERT INTO " & KUL.tper & "depistbag ( a_dif_tur, a_dif_id, a_dtf_tur, a_dtf_id )"
        SQ = SQ & " VALUES "
        SQ = SQ & " ( "
        SQ = SQ & " 89, "
        SQ = SQ & " " & FID & ", "
        SQ = SQ & " 49, "
        SQ = SQ & " " & ID & " "
        SQ = SQ & " ) "
        If SQL_KOS(B_SQL, SQ, False) = True Then
        End If

        SQ = ""
        SQ = "Update " & KUL.tper & "depistmas"
        SQ = SQ & " SET "
        SQ = SQ & " a_yononay = 1 "
        SQ = SQ & " WHERE  a_id = " & FID & ""
        If SQL_KOS(B_SQL, SQ, False) = True Then
        End If


        'Call SORU("Bilgi", Sozlukde_Bul("İşlem Nosu : ", L_LISAN) & "ISTEK-" & ID & Sozlukde_Bul(" Tarihi : ", L_LISAN) & TbTarih7.TextBox1.Text & Sozlukde_Bul(" olan depo transfer fişi oluşturuldu.", L_LISAN), "T")



        S = "Transfer Fişi Kaydedildi."
        Return S



    End Function
    Private Function DepoTransFarkIslem(ByVal IDD As Long, ByVal FirmaID As Integer) As Boolean

        Dim SQ As String
        Dim DT As New DataTable
        Dim DT2 As New DataTable
        Dim X As Integer = 0
        Dim L As Integer = 0
        Dim ID As Long
        Dim GDepo As Integer = 0
        Dim CDepo As Integer = 0


        Dim Firma As String = ""
        If FirmaID <> 0 Then
            Firma = New String("0", 3 - FirmaID.ToString.Length) & FirmaID
            KUL.tfrm = "tbl_" & Firma & "_"
            KUL.tper = "tbl_" & Firma & "_" & "01" & "_"

        Else
            KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
            KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        End If

        Dim DTT As New X_DepoTransfer
        Dim KL As New X_DepoTransfer.Belge_Detay


        SQ = ""
        SQ = "SELECT  MAS.a_ambar_id, MAS.a_oplan_id"
        SQ = SQ & " FROM "
        SQ = SQ & " " & KUL.tper & "stkhmas AS MAS INNER JOIN"
        SQ = SQ & " " & KUL.tfrm & "ambar AS CDEP ON MAS.a_ambar_id = CDEP.a_id INNER JOIN"
        SQ = SQ & " " & KUL.tfrm & "ambar AS GDEP ON MAS.a_oplan_id = GDEP.a_id"
        SQ = SQ & " WHERE (MAS.a_tur = 49) AND (MAS.a_id = " & IDD & ")"
        DT = SQL_TABLO(SQ, "T", False, False, False)

        GDepo = NULN(DT, 0, "a_oplan_id")
        CDepo = NULN(DT, 0, "a_ambar_id")

        SQ = ""
        SQ = "SELECT det.a_tur, det.a_id, det.a_sid, stk.a_kod, stk.a_adi, det.a_fismik, det.a_konmik, det.a_frkmik,"
        SQ = SQ & " " & GDepo & " as a_gdid, " & CDepo & " as a_cdid, det.a_49_Id, stk.a_afiyat"
        SQ = SQ & " FROM "
        SQ = SQ & " " & KUL.tfrm & "pdakabuldet AS det INNER JOIN"
        SQ = SQ & " " & KUL.tfrm & "stok AS stk ON det.a_sid = stk.a_id"
        SQ = SQ & " WHERE (det.a_tur = 49) AND (det.a_id = " & IDD & ") AND (det.a_fismik < det.a_konmik)"
        DT = SQL_TABLO(SQ, "T", False, False, False)


        DTT.Belge_Baslik_Temizle()
        DTT.Belge_Satirlari_Temizle()
        ID = 0

        DTT.Belge_Baslik.Evrak_Turu = 49
        'DT.Belge_Baslik.Giris_Cikis = "C"
        DTT.Belge_Baslik.Acik_Kapali = "A"
        DTT.Belge_Baslik.Aciklaması = ""
        DTT.Belge_Baslik.Adres_Id = 0
        DTT.Belge_Baslik.Belge_Nosu = ""
        DTT.Belge_Baslik.Cari_Id = 0
        DTT.Belge_Baslik.Cikti_Turu = "E"
        DTT.Belge_Baslik.Cikis_Depo_Id = NULN(DT, 0, "a_cdid")
        DTT.Belge_Baslik.Giris_Depo_Id = NULN(DT, 0, "a_gdid")
        DTT.Belge_Baslik.Doviz_Id = 0
        DTT.Belge_Baslik.Doviz_Kuru = 1
        DTT.Belge_Baslik.Fatura_Baglantisi = 0
        DTT.Belge_Baslik.Fatura_Cari_Adi = ""
        DTT.Belge_Baslik.Fatura_Cari_AdresSat1 = ""
        DTT.Belge_Baslik.Fatura_Cari_AdresSat2 = ""
        DTT.Belge_Baslik.Fatura_Cari_VergiDairesi = ""
        DTT.Belge_Baslik.Fatura_Cari_VergiNo = ""
        DTT.Belge_Baslik.Fatura_Grup_Turu = 0
        DTT.Belge_Baslik.Fiyat_Listesi_Id = 0
        DTT.Belge_Baslik.Genel_Iskontosu = 0
        DTT.Belge_Baslik.Irsaliye_Nosu = ""
        DTT.Belge_Baslik.Irsaliye_Tarihi = TTAR.TARIH
        DTT.Belge_Baslik.Islem_Nosu = ""
        DTT.Belge_Baslik.Islem_Yeri = "PDATRANS"
        DTT.Belge_Baslik.Islem_Yeri_Id = 0
        DTT.Belge_Baslik.Islem_Yeri_Tur = 49
        DTT.Belge_Baslik.Kapama_Taksit_Sayisi = 0
        DTT.Belge_Baslik.Kapama_Turu = 1
        DTT.Belge_Baslik.Kapama_Yeri = 0
        DTT.Belge_Baslik.Kdv_Tevkifat_Orani = 0
        DTT.Belge_Baslik.Komisyon_Tutari = 0
        DTT.Belge_Baslik.Ozel_Iletisim_Vergi_Orani = 0
        DTT.Belge_Baslik.Komisyon_Tutari = 0
        DTT.Belge_Baslik.Personel_Id = 0
        DTT.Belge_Baslik.Puan_Kapama_Taksit_Sayisi = 0
        DTT.Belge_Baslik.Puan_Kapama_Turu = 0
        DTT.Belge_Baslik.Puan_Kapama_Yeri = 0
        DTT.Belge_Baslik.Siparis_Nosu = ""
        DTT.Belge_Baslik.Sube_Id = 0
        DTT.Belge_Baslik.Tarihi = TTAR.TARIH
        DTT.Belge_Baslik.Vade_Tarihi = TTAR.TARIH
        DTT.Belge_Baslik.Vardiya_Id = 0



        L = 0



        For X = 0 To DT.Rows.Count - 1

            If NULN(DT, X, "a_49_Id") = 0 Then

                KL = New X_DepoTransfer.Belge_Detay


                KL.Depo_Id = NULN(DT, 0, "a_cdid")
                KL.Fatura_Cikti = "E"
                KL.Fatura_Fiyat = NULD(DT, X, "a_afiyat")
                KL.Masref_Merkezi = 0
                KL.Masref_Yeri = 0
                KL.Satir_Aciklamasi = NULA(DT, X, "a_adi")
                KL.Satir_Doviz_Id = 0
                KL.Satir_Doviz_Kuru = 1
                KL.Satir_Durum_Tarihi = TTAR.TARIH
                KL.Satir_Durumu = ""
                KL.Satir_Genel_İskonto_Orani = 0
                KL.Satir_Genel_İskonto_Tutari = 0
                KL.Satir_Iskonto_1_Oran = 0
                KL.Satir_Iskonto_2_Oran = 0
                KL.Satir_Iskonto_3_Oran = 0
                KL.Satir_Iskonto_4_Oran = 0
                KL.Satir_Iskonto_5_Oran = 0
                KL.Satir_Iskonto_6_Oran = 0

                KL.Satir_Islem_Birim_Fiyati = NULD(DT, X, "a_afiyat")
                If NULD(DT, X, "a_frkmik") < 0 Then
                    KL.Satir_Islem_Birim_Miktari = NULD(DT, X, "a_frkmik") * -1
                Else
                    KL.Satir_Islem_Birim_Miktari = NULD(DT, X, "a_frkmik")
                End If
                KL.Satir_Islem_Birimi = ""

                KL.Satir_Kdv_Orani = 0
                KL.Satir_Ozel_Iletisim_Vergi_Orani = 0
                KL.Satir_Ozel_Tuketim_Vergisi_Orani = 0
                KL.Satir_Personel_Id = 1
                KL.Stok_Id = NULN(DT, X, "a_sid")
                DTT.Belge_Satirlari.Add(KL)
                L = L + 1
            End If
        Next

        If L <> 0 Then
            ID = DTT.Belge_Ekle()

            If ID = 0 Then
                MKUTU("Hata : " & DTT.Hata_text, "T", "Uyarı")
                Exit Function
            Else
                For X = 0 To DT.Rows.Count - 1
                    If NULN(DT, X, "a_49_Id") = 0 Then
                        SQ = "UPDATE  " & KUL.tfrm & "pdakabuldet"
                        SQ = SQ & " SET  a_49_Id = " & ID & " "
                        SQ = SQ & " WHERE  "
                        SQ = SQ & " a_tur = " & NULN(DT, X, "a_tur") & " "
                        SQ = SQ & " AND a_id = " & NULN(DT, X, "a_id") & " "
                        SQ = SQ & " AND a_sid = " & NULN(DT, X, "a_sid") & ""
                        SQL_KOS(SQ, False)
                    End If
                Next
            End If

        End If


        '--------------------------------------------------------------------------------------------

        SQ = ""
        SQ = "SELECT det.a_tur, det.a_id, det.a_sid, stk.a_kod, stk.a_adi, det.a_fismik, det.a_konmik, det.a_frkmik,"
        SQ = SQ & " " & GDepo & " as a_gdid, " & CDepo & " as a_cdid, det.a_49_Id, stk.a_afiyat"
        SQ = SQ & " FROM "
        SQ = SQ & " " & KUL.tfrm & "pdakabuldet AS det INNER JOIN"
        SQ = SQ & " " & KUL.tfrm & "stok AS stk ON det.a_sid = stk.a_id"
        SQ = SQ & " WHERE (det.a_tur = 49) AND (det.a_id = " & IDD & ") AND (det.a_fismik > det.a_konmik)"
        DT = SQL_TABLO(SQ, "T", False, False, False)

        '--------------------------------------------------------------------------------------------
        'Eksik Olanları Eksilten Ters Transferleri Kes
        '--------------------------------------------------------------------------------------------

        DTT.Belge_Baslik_Temizle()
        DTT.Belge_Satirlari_Temizle()
        ID = 0

        DTT.Belge_Baslik.Evrak_Turu = 49
        DTT.Belge_Baslik.Acik_Kapali = "A"
        DTT.Belge_Baslik.Aciklaması = ""
        DTT.Belge_Baslik.Adres_Id = 0
        DTT.Belge_Baslik.Belge_Nosu = ""
        DTT.Belge_Baslik.Cari_Id = 0
        DTT.Belge_Baslik.Cikti_Turu = "E"
        DTT.Belge_Baslik.Cikis_Depo_Id = NULN(DT, 0, "a_gdid")
        DTT.Belge_Baslik.Giris_Depo_Id = NULN(DT, 0, "a_cdid")
        DTT.Belge_Baslik.Doviz_Id = 0
        DTT.Belge_Baslik.Doviz_Kuru = 1
        DTT.Belge_Baslik.Fatura_Baglantisi = 0
        DTT.Belge_Baslik.Fatura_Cari_Adi = ""
        DTT.Belge_Baslik.Fatura_Cari_AdresSat1 = ""
        DTT.Belge_Baslik.Fatura_Cari_AdresSat2 = ""
        DTT.Belge_Baslik.Fatura_Cari_VergiDairesi = ""
        DTT.Belge_Baslik.Fatura_Cari_VergiNo = ""
        DTT.Belge_Baslik.Fatura_Grup_Turu = 0
        DTT.Belge_Baslik.Fiyat_Listesi_Id = 0
        DTT.Belge_Baslik.Genel_Iskontosu = 0
        DTT.Belge_Baslik.Irsaliye_Nosu = ""
        DTT.Belge_Baslik.Irsaliye_Tarihi = TTAR.TARIH
        DTT.Belge_Baslik.Islem_Nosu = ""
        DTT.Belge_Baslik.Islem_Yeri = "PDATRANS"
        DTT.Belge_Baslik.Islem_Yeri_Id = 0
        DTT.Belge_Baslik.Islem_Yeri_Tur = 49
        DTT.Belge_Baslik.Kapama_Taksit_Sayisi = 0
        DTT.Belge_Baslik.Kapama_Turu = 1
        DTT.Belge_Baslik.Kapama_Yeri = 0
        DTT.Belge_Baslik.Kdv_Tevkifat_Orani = 0
        DTT.Belge_Baslik.Komisyon_Tutari = 0
        DTT.Belge_Baslik.Ozel_Iletisim_Vergi_Orani = 0
        DTT.Belge_Baslik.Komisyon_Tutari = 0
        DTT.Belge_Baslik.Personel_Id = 0
        DTT.Belge_Baslik.Puan_Kapama_Taksit_Sayisi = 0
        DTT.Belge_Baslik.Puan_Kapama_Turu = 0
        DTT.Belge_Baslik.Puan_Kapama_Yeri = 0
        DTT.Belge_Baslik.Siparis_Nosu = ""
        DTT.Belge_Baslik.Sube_Id = 0
        DTT.Belge_Baslik.Tarihi = TTAR.TARIH
        DTT.Belge_Baslik.Vade_Tarihi = TTAR.TARIH
        DTT.Belge_Baslik.Vardiya_Id = 0



        L = 0

        For X = 0 To DT.Rows.Count - 1

            If NULN(DT, X, "a_49_Id") = 0 Then

                KL = New X_DepoTransfer.Belge_Detay

                KL.Depo_Id = NULN(DT, 0, "a_gdid")
                KL.Fatura_Cikti = "E"
                KL.Fatura_Fiyat = NULD(DT, X, "a_afiyat")
                KL.Masref_Merkezi = 0
                KL.Masref_Yeri = 0
                KL.Satir_Aciklamasi = NULA(DT, X, "a_adi")
                KL.Satir_Doviz_Id = 0
                KL.Satir_Doviz_Kuru = 1
                KL.Satir_Durum_Tarihi = TTAR.TARIH
                KL.Satir_Durumu = ""
                KL.Satir_Genel_İskonto_Orani = 0
                KL.Satir_Genel_İskonto_Tutari = 0
                KL.Satir_Iskonto_1_Oran = 0
                KL.Satir_Iskonto_2_Oran = 0
                KL.Satir_Iskonto_3_Oran = 0
                KL.Satir_Iskonto_4_Oran = 0
                KL.Satir_Iskonto_5_Oran = 0
                KL.Satir_Iskonto_6_Oran = 0

                KL.Satir_Islem_Birim_Fiyati = NULD(DT, X, "a_afiyat")
                If NULD(DT, X, "a_frkmik") < 0 Then
                    KL.Satir_Islem_Birim_Miktari = NULD(DT, X, "a_frkmik") * -1
                Else
                    KL.Satir_Islem_Birim_Miktari = NULD(DT, X, "a_frkmik")
                End If
                KL.Satir_Islem_Birimi = ""

                KL.Satir_Kdv_Orani = 0
                KL.Satir_Ozel_Iletisim_Vergi_Orani = 0
                KL.Satir_Ozel_Tuketim_Vergisi_Orani = 0
                KL.Satir_Personel_Id = 1
                KL.Stok_Id = NULN(DT, X, "a_sid")
                DTT.Belge_Satirlari.Add(KL)
                L = L + 1
            End If
        Next

        If L <> 0 Then
            ID = DTT.Belge_Ekle()

            If ID = 0 Then
                MKUTU("Hata : " & DTT.Hata_text, "T", "Uyarı")
                Exit Function
            Else
                For X = 0 To DT.Rows.Count - 1
                    If NULN(DT, X, "a_49_Id") = 0 Then
                        SQ = "UPDATE  " & KUL.tfrm & "pdakabuldet"
                        SQ = SQ & " SET  a_49_Id = " & ID & " "
                        SQ = SQ & " WHERE  "
                        SQ = SQ & " a_tur = " & NULN(DT, X, "a_tur") & " "
                        SQ = SQ & " AND a_id = " & NULN(DT, X, "a_id") & " "
                        SQ = SQ & " AND a_sid = " & NULN(DT, X, "a_sid") & ""
                        SQL_KOS(SQ, False)
                    End If
                Next
            End If


        End If

        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        Return True

    End Function
    Private Function DepoTransFarkSil(ByVal IDD As Long, ByVal FirmaID As Integer) As Boolean

        Dim SQ As String
        Dim DT As New DataTable


        Dim Firma As String = ""
        If FirmaID <> 0 Then
            Firma = New String("0", 3 - FirmaID.ToString.Length) & FirmaID
            KUL.tfrm = "tbl_" & Firma & "_"
            KUL.tper = "tbl_" & Firma & "_" & "01" & "_"

        Else
            KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
            KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        End If

        SQ = ""
        SQ = "SELECT det.a_tur, det.a_id, det.a_sid, stk.a_kod, stk.a_adi, det.a_fismik, det.a_konmik, det.a_frkmik,"
        SQ = SQ & " det.a_49_Id, stk.a_afiyat"
        SQ = SQ & " FROM "
        SQ = SQ & " " & KUL.tfrm & "pdakabuldet AS det INNER JOIN"
        SQ = SQ & " " & KUL.tfrm & "stok AS stk ON det.a_sid = stk.a_id"
        SQ = SQ & " WHERE (det.a_tur = 49) AND (det.a_id = " & IDD & ") "
        DT = SQL_TABLO(SQ, "T", False, False, False)


        Dim DTT As New X_DepoTransfer

        For x = 0 To DT.Rows.Count - 1
            If NULN(DT, x, "a_49_Id") <> 0 Then
                Call DTT.Belge_Sil(NULN(DT, x, "a_49_Id"))


                SQ = "UPDATE  " & KUL.tfrm & "pdakabuldet"
                SQ = SQ & " SET  a_49_Id = " & 0 & " "
                SQ = SQ & " WHERE  "
                SQ = SQ & " a_tur = " & NULN(DT, x, "a_tur") & " "
                SQ = SQ & " AND a_id = " & NULN(DT, x, "a_id") & " "
                SQ = SQ & " AND a_sid = " & NULN(DT, x, "a_sid") & ""
                SQL_KOS(SQ, False)

            End If

        Next



        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        Return True

    End Function

    Private Function DepoSifirla(ByVal DepoID As Integer, ByVal FirmaID As Integer, ByVal IslemTur As String) As String
        Dim SQ As String
        Dim DT1 As New DataTable
        Dim DT As New DataTable
        Dim SID As Long
        Dim DRO As New DataTable
        Dim GMIK As Double
        Dim CMIK As Double
        Dim KALAN As Double
        Dim Fiyat As Double
        Dim X As Integer = 1
        Dim Firma As String = ""
        Dim Sonuc As String = ""

        If FirmaID <> 0 Then
            Firma = New String("0", 3 - FirmaID.ToString.Length) & FirmaID
            KUL.tfrm = "tbl_" & Firma & "_"
            KUL.tper = "tbl_" & Firma & "_" & "01" & "_"

        Else
            KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
            KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        End If





        SQ = ""
        SQ = "SELECT a_id, a_kod, a_adi, a_giren, a_cikan, a_kalan"
        SQ = SQ & " FROM " & KUL.tfrm & "stok "
        SQ = SQ & " WHERE a_selo = 0"
        DT1 = SQL_TABLO(SQ, "T", False, False, False)

        If DT1.Rows.Count = 0 Then
            Sonuc = "Stok Bulunamadı"
            Return Sonuc
        End If


        SQ = ""
        SQ = "SELECT MAX(a_id) AS EB"
        SQ = SQ & " FROM " & KUL.tper & "stsaymas"
        DT = SQL_TABLO(SQ, "T", False, False, False)

        SQ = ""
        SQ = "INSERT INTO " & KUL.tper & "stsaymas"
        SQ = SQ & " (a_id, a_bno, a_depo_id, a_ack, a_tarih, a_tutar, a_kesin, a_cuser, a_cdate, a_ctime, a_muser, a_mdate, a_mtime, a_gfisid, a_cfisid)"
        SQ = SQ & " VALUES ("
        SQ = SQ & " " & Val(NULN(DT, 0, "EB")) + 1 & ", "
        SQ = SQ & " N'PDA-" & (NULN(DT, 0, "EB") + 1) & "', "
        SQ = SQ & " " & DepoID & ", "
        SQ = SQ & " N'PDA Depo Sıfırlama', "
        SQ = SQ & " N'" & TTAR.TR2UK(TTAR.TARIH) & "', "
        SQ = SQ & " 0, "
        SQ = SQ & " 0, "
        SQ = SQ & " " & Val(KUL.KOD) & ", "
        SQ = SQ & " N'" & TTAR.TR2UK(TTAR.TARIH) & "', "
        SQ = SQ & " N'" & Date.Now.ToShortTimeString & "', "
        SQ = SQ & " " & Val(KUL.KOD) & ", "
        SQ = SQ & " N'" & TTAR.TR2UK(TTAR.TARIH) & "', "
        SQ = SQ & " N'" & Date.Now.ToShortTimeString & "', "
        SQ = SQ & " -1, "
        SQ = SQ & " -1 "
        SQ = SQ & " )"
        If SQL_KOS(SQ, True) = True Then

            For Y = 0 To DT1.Rows.Count - 1

                SID = NULN(DT1, Y, "a_id")

                SQ = ""
                SQ = "SELECT  SUM(a_mik) AS GTOP"
                SQ = SQ & " FROM " & KUL.tper & "stkhdet"
                SQ = SQ & " WHERE (a_tarih <= N'" & TTAR.TR2UK(TTAR.TARIH) & "') AND (a_gc = N'G') AND (a_stok_id = " & SID & ") AND (a_ambar_id = " & DepoID & ")"
                DRO = SQL_TABLO(SQ, "T", False, False, False)
                GMIK = NULD(DRO, 0, "GTOP")

                SQ = ""
                SQ = "SELECT  SUM(a_mik) AS CTOP"
                SQ = SQ & " FROM " & KUL.tper & "stkhdet"
                SQ = SQ & " WHERE (a_tarih <= N'" & TTAR.TR2UK(TTAR.TARIH) & "') AND (a_gc = N'C') AND (a_stok_id = " & SID & ") AND (a_ambar_id = " & DepoID & ")"
                DRO = SQL_TABLO(SQ, "T", False, False, False)
                CMIK = NULD(DRO, 0, "CTOP")

                KALAN = GMIK - CMIK



                Select Case IslemTur
                    Case XL_Tumu
                        If KALAN <> 0 Then
                            Fiyat = Val(VT_BILGI(KUL.tfrm & "stok", "a_id", "a_afiyat", SID))
                            SQ = ""
                            SQ = "INSERT INTO " & KUL.tper & "stsaydet"
                            SQ = SQ & " (a_id, a_sira, a_depo_id, a_stok_id, a_tarih, a_mevcut, a_sayilan, a_miktar, a_mikdur, a_fiyat, a_tutar, a_kesin, a_cuser, a_cdate, a_ctime)"
                            SQ = SQ & " VALUES ("
                            SQ = SQ & " " & Val(NULN(DT, 0, "EB")) + 1 & ","
                            SQ = SQ & " " & X & ","
                            SQ = SQ & " " & DepoID & ","
                            SQ = SQ & " " & SID & ","
                            SQ = SQ & " N'" & TTAR.TR2UK(TTAR.TARIH) & "',"
                            SQ = SQ & " " & KALAN & ","
                            SQ = SQ & " " & 0 & ","
                            SQ = SQ & " " & 0 - KALAN & ","
                            SQ = SQ & " 'E',"
                            SQ = SQ & " " & Fiyat & ","
                            SQ = SQ & " " & Fiyat * KALAN & ","
                            SQ = SQ & " 0,"
                            SQ = SQ & " " & Val(KUL.KOD) & ","
                            SQ = SQ & " N'" & TTAR.TR2UK(TTAR.TARIH) & "',"
                            SQ = SQ & " N'" & Date.Now.ToShortTimeString & "'"
                            SQ = SQ & " )"
                            SQL_KOS(SQ, False)
                            X = X + 1
                        End If

                    Case Sozlukde_Bul("Negatif", L_LISAN)

                        If KALAN < 0 Then
                            Fiyat = Val(VT_BILGI(KUL.tfrm & "stok", "a_id", "a_afiyat", SID))
                            SQ = ""
                            SQ = "INSERT INTO " & KUL.tper & "stsaydet"
                            SQ = SQ & " (a_id, a_sira, a_depo_id, a_stok_id, a_tarih, a_mevcut, a_sayilan, a_miktar, a_mikdur, a_fiyat, a_tutar, a_kesin, a_cuser, a_cdate, a_ctime)"
                            SQ = SQ & " VALUES ("
                            SQ = SQ & " " & Val(NULN(DT, 0, "EB")) + 1 & ","
                            SQ = SQ & " " & X & ","
                            SQ = SQ & " " & DepoID & ","
                            SQ = SQ & " " & SID & ","
                            SQ = SQ & " N'" & TTAR.TR2UK(TTAR.TARIH) & "',"
                            SQ = SQ & " " & KALAN & ","
                            SQ = SQ & " " & 0 & ","
                            SQ = SQ & " " & 0 - KALAN & ","
                            SQ = SQ & " 'E',"
                            SQ = SQ & " " & Fiyat & ","
                            SQ = SQ & " " & Fiyat * KALAN & ","
                            SQ = SQ & " 0,"
                            SQ = SQ & " " & Val(KUL.KOD) & ","
                            SQ = SQ & " N'" & TTAR.TR2UK(TTAR.TARIH) & "',"
                            SQ = SQ & " N'" & Date.Now.ToShortTimeString & "'"
                            SQ = SQ & " )"
                            SQL_KOS(SQ, False)
                            X = X + 1
                        End If



                    Case Sozlukde_Bul("Pozitif", L_LISAN)

                        If KALAN > 0 Then
                            Fiyat = Val(VT_BILGI(KUL.tfrm & "stok", "a_id", "a_afiyat", SID))
                            SQ = ""
                            SQ = "INSERT INTO " & KUL.tper & "stsaydet"
                            SQ = SQ & " (a_id, a_sira, a_depo_id, a_stok_id, a_tarih, a_mevcut, a_sayilan, a_miktar, a_mikdur, a_fiyat, a_tutar, a_kesin, a_cuser, a_cdate, a_ctime)"
                            SQ = SQ & " VALUES ("
                            SQ = SQ & " " & Val(NULN(DT, 0, "EB")) + 1 & ","
                            SQ = SQ & " " & X & ","
                            SQ = SQ & " " & DepoID & ","
                            SQ = SQ & " " & SID & ","
                            SQ = SQ & " N'" & TTAR.TR2UK(TTAR.TARIH) & "',"
                            SQ = SQ & " " & KALAN & ","
                            SQ = SQ & " " & 0 & ","
                            SQ = SQ & " " & 0 - KALAN & ","
                            SQ = SQ & " 'E',"
                            SQ = SQ & " " & Fiyat & ","
                            SQ = SQ & " " & Fiyat * KALAN & ","
                            SQ = SQ & " 0,"
                            SQ = SQ & " " & Val(KUL.KOD) & ","
                            SQ = SQ & " N'" & TTAR.TR2UK(TTAR.TARIH) & "',"
                            SQ = SQ & " N'" & Date.Now.ToShortTimeString & "'"
                            SQ = SQ & " )"
                            SQL_KOS(SQ, False)
                            X = X + 1
                        End If

                    Case Else

                        If KALAN <> 0 Then
                            Fiyat = Val(VT_BILGI(KUL.tfrm & "stok", "a_id", "a_afiyat", SID))
                            SQ = ""
                            SQ = "INSERT INTO " & KUL.tper & "stsaydet"
                            SQ = SQ & " (a_id, a_sira, a_depo_id, a_stok_id, a_tarih, a_mevcut, a_sayilan, a_miktar, a_mikdur, a_fiyat, a_tutar, a_kesin, a_cuser, a_cdate, a_ctime)"
                            SQ = SQ & " VALUES ("
                            SQ = SQ & " " & Val(NULN(DT, 0, "EB")) + 1 & ","
                            SQ = SQ & " " & X & ","
                            SQ = SQ & " " & DepoID & ","
                            SQ = SQ & " " & SID & ","
                            SQ = SQ & " N'" & TTAR.TR2UK(TTAR.TARIH) & "',"
                            SQ = SQ & " " & KALAN & ","
                            SQ = SQ & " " & 0 & ","
                            SQ = SQ & " " & 0 - KALAN & ","
                            SQ = SQ & " 'E',"
                            SQ = SQ & " " & Fiyat & ","
                            SQ = SQ & " " & Fiyat * KALAN & ","
                            SQ = SQ & " 0,"
                            SQ = SQ & " " & Val(KUL.KOD) & ","
                            SQ = SQ & " N'" & TTAR.TR2UK(TTAR.TARIH) & "',"
                            SQ = SQ & " N'" & Date.Now.ToShortTimeString & "'"
                            SQ = SQ & " )"
                            SQL_KOS(SQ, False)
                            X = X + 1
                        End If

                End Select



            Next

            Dim GID As Long
            Dim CID As Long
            'Giriş Fişini Ekle
            GID = Trim(Str(Fis_Ekle("G", (Val(NULN(DT, 0, "EB")) + 1), DepoID)))

            'Çıkış Fişini Ekle
            CID = Trim(Str(Fis_Ekle("C", (Val(NULN(DT, 0, "EB")) + 1), DepoID)))

            SQ = ""
            SQ = "Update " & KUL.tper & "stsaymas"
            SQ = SQ & " SET "
            SQ = SQ & " a_gfisid = " & GID & ","
            SQ = SQ & " a_cfisid = " & CID & ","
            SQ = SQ & " a_kesin = " & 1 & ""
            SQ = SQ & " WHERE(a_id = " & Val(NULN(DT, 0, "EB")) + 1 & ")"
            If SQL_KOS(SQ, False) = True Then
                Sonuc = "Stok Sayım Fişi Eklendi"
                Return Sonuc
            Else
                Sonuc = "Stok Sayım Fişi Onaylanamadı"
                Return Sonuc
            End If



        Else
            Sonuc = "Stok Sayım Fişi Kesilemedi"
            Return Sonuc
        End If

    End Function
    Public Function Fis_Ekle(ByVal GC As String, ByVal FID As Long, ByVal DepoId As Integer) As Long

        Dim SQ As String = ""
        Dim DT As New DataTable

        Dim MIKTAR As Double = 0
        Dim TUTAR As Double = 0

        Dim Tur As Long = 0
        Dim IDD As Long = 0
        Dim X As Long = 0


        If GC = "G" Then
            'Sayım Girişi
            Tur = 8
            SQ = ""
            SQ = "SELECT " & KUL.tper & "stsaydet.a_id, " & KUL.tper & "stsaydet.a_sira, " & KUL.tper & "stsaydet.a_stok_id, "
            SQ = SQ & " " & KUL.tper & "stsaydet.a_mevcut, " & KUL.tper & "stsaydet.a_sayilan, " & KUL.tper & "stsaydet.a_miktar, " & KUL.tper & "stsaydet.a_fiyat, "
            SQ = SQ & " " & KUL.tper & "stsaydet.a_tutar"
            SQ = SQ & " FROM " & KUL.tper & "stsaydet"
            SQ = SQ & " WHERE " & KUL.tper & "stsaydet.a_id = " & FID & " AND " & KUL.tper & "stsaydet.a_miktar > 0"
            SQ = SQ & " ORDER BY " & KUL.tper & "stsaydet.a_sira DESC"
            DT = SQL_TABLO(SQ, "TABLO", False, False, False)
            If DT.Rows.Count <= 0 Then
                Fis_Ekle = -1
                Exit Function
            End If
            IDD = TSAY.MAX_FIS_SG(Tur, 1)
            Fis_Ekle = IDD
        End If


        If GC = "C" Then
            'Sayım Çıkışı
            Tur = 4
            SQ = ""
            SQ = "SELECT " & KUL.tper & "stsaydet.a_id, " & KUL.tper & "stsaydet.a_sira, " & KUL.tper & "stsaydet.a_stok_id, "
            SQ = SQ & " " & KUL.tper & "stsaydet.a_mevcut, " & KUL.tper & "stsaydet.a_sayilan, " & KUL.tper & "stsaydet.a_miktar, " & KUL.tper & "stsaydet.a_fiyat, "
            SQ = SQ & " " & KUL.tper & "stsaydet.a_tutar"
            SQ = SQ & " FROM " & KUL.tper & "stsaydet"
            SQ = SQ & " WHERE " & KUL.tper & "stsaydet.a_id = " & FID & " AND " & KUL.tper & "stsaydet.a_miktar < 0"
            SQ = SQ & " ORDER BY " & KUL.tper & "stsaydet.a_sira DESC"
            DT = SQL_TABLO(SQ, "TABLO", False, False, False)
            If DT.Rows.Count <= 0 Then
                Fis_Ekle = -1
                Exit Function
            End If
            IDD = TSAY.MAX_FIS_SC(Tur, 1)
            Fis_Ekle = IDD
        End If

        SQ = ""
        SQ = "INSERT INTO " & KUL.tper & "stkhmas"
        SQ = SQ & " (a_gc, a_tur, a_id, a_kod, a_bno, a_ack, a_tarih, a_cari_id, a_ambar_id, a_isktop, a_kdvtop, a_tuttop, a_cuser, a_cdate, a_ctime, a_muser, a_mdate, a_mtime, a_bloke, a_kesin) "
        SQ = SQ & " Select "
        SQ = SQ & " N'" & GC & "' as a_gc,"
        SQ = SQ & " " & Tur & " as a_tur,"
        SQ = SQ & " " & IDD & " as a_id,"
        SQ = SQ & " N'" & IDD & "' as a_kod,"
        SQ = SQ & " 'S-" & IDD & "' as a_bno,"
        SQ = SQ & " 'Stok Sayım Düzeltme Fişi' as a_ack,"
        SQ = SQ & " N'" & TTAR.TR2UK(TTAR.TARIH) & "' as a_tarih,"
        SQ = SQ & " '' as a_cari_id,"
        SQ = SQ & " " & DepoId & " as a_ambar_id,"
        SQ = SQ & " 0 as a_isktop,"
        SQ = SQ & " 0 as a_kdvtop,"
        SQ = SQ & " 0 as a_tuttop,"
        SQ = SQ & " " & KUL.KOD & " as a_cuser,"
        SQ = SQ & " N'" & TTAR.TR2UK(Date.Now.ToShortDateString) & "' as a_cdate,"
        SQ = SQ & " N'" & Date.Now.ToShortTimeString & "' as a_ctime,"
        SQ = SQ & " " & KUL.KOD & " as a_muser,"
        SQ = SQ & " N'" & TTAR.TR2UK(Date.Now.ToShortDateString) & "' as a_mdate,"
        SQ = SQ & " N'" & Date.Now.ToShortTimeString & "' as a_mtime,"
        SQ = SQ & " 0 as a_bloke,"
        SQ = SQ & " 1 as a_kesin;"
        If SQL_KOS(SQ) = True Then

            TUTAR = 0

            For X = 0 To DT.Rows.Count - 1

                If GC = "C" Then
                    MIKTAR = NULD(DT, X, "a_miktar") * -1
                End If

                If GC = "G" Then
                    MIKTAR = NULD(DT, X, "a_miktar")
                End If

                SQ = ""
                SQ = "INSERT INTO " & KUL.tper & "stkhdet"
                SQ = SQ & " (a_gc, a_tur, a_id, a_sira, a_tarih, a_cari_id, a_stok_id, a_ambar_id, a_mik, a_brmfiy, a_kdv, a_kdvtut, a_isk, a_isktut, a_tutar, a_ack, a_bloke, a_kesin) "
                SQ = SQ & " Select "
                SQ = SQ & " N'" & GC & "' as a_gc,"
                SQ = SQ & " " & Tur & " as a_tur,"
                SQ = SQ & " " & IDD & " as a_id,"
                SQ = SQ & " " & X + 1 & " as a_sira,"
                SQ = SQ & " N'" & TTAR.TR2UK(TTAR.TARIH) & "' as a_tarih,"
                SQ = SQ & " 0 as a_cari_id,"
                SQ = SQ & " " & NULN(DT, X, "a_stok_id") & " as a_stok_id,"
                SQ = SQ & " " & DepoId & " as a_ambar_id,"
                SQ = SQ & " " & MIKTAR & " as a_mik,"
                SQ = SQ & " " & NULD(DT, X, "a_fiyat") & " as a_brmfiy,"
                SQ = SQ & " 0 as a_kdv,"
                SQ = SQ & " 0 as a_kdvtut,"
                SQ = SQ & " 0 as a_isk,"
                SQ = SQ & " 0 as a_isktut,"
                SQ = SQ & " " & MIKTAR * NULD(DT, X, "a_fiyat") & "  as a_tutar,"
                SQ = SQ & " N'" & "" & "' as a_ack,"
                SQ = SQ & " 0 as a_bloke,"
                SQ = SQ & " 0 as a_kesin;"
                If SQL_KOS(SQ) = True Then
                    BG.stok_miktar(NULN(DT, X, "a_stok_id"), MIKTAR, GC & "+")
                    BG.depo_stok_miktar(DepoId, NULN(DT, X, "a_stok_id"), MIKTAR, GC & "+")
                    TUTAR = TUTAR + (MIKTAR * NULD(DT, X, "a_fiyat"))
                End If
            Next

            SQ = ""
            SQ = "UPDATE " & KUL.tper & "stkhmas"
            SQ = SQ & " SET a_tuttop = " & TUTAR & ""
            SQ = SQ & " WHERE (a_gc = N'" & GC & "') AND (a_tur = " & Tur & ") AND (a_id = " & IDD & ")"
            Call SQL_KOS(SQ, False)

        End If



    End Function
    <WebMethod()>
    Public Function WEB_Fatura_Onayla(ByVal Fat_ID As Long, ByVal Fat_Tur As Integer, ByVal FirmaID As Integer) As String

        Dim Sonuc As String = ""

        If Fat_Tur = 33 Or Fat_Tur = 34 Then
            Sonuc = Siparis_Onayla(Fat_ID, Fat_Tur, FirmaID)
        Else
            Sonuc = Fatura_Onayla(Fat_ID, Fat_Tur, FirmaID)
        End If

        Return Sonuc
    End Function
    Private Function Siparis_Onayla(ByVal Fat_ID As Long, ByVal Fat_Tur As Integer, ByVal FirmaID As Integer) As String
        Dim SQ As String = ""
        Dim DT As New DataTable
        Dim X As Long = 0
        Dim Y As Long = 0
        Dim Firma As String = ""

        B_SQL = connstr
        B_PRJ = "PDA_TRANS"
        SQL_TUR = "2005"

        If FirmaID <> 0 Then
            Firma = New String("0", 3 - FirmaID.ToString.Length) & FirmaID
            KUL.tfrm = "tbl_" & Firma & "_"
            KUL.tper = "tbl_" & Firma & "_" & "01" & "_"

        Else
            KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
            KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        End If



        Dim FT As New X_Siparis
        Dim KL As New X_Siparis.Veri_SipDet


        If FT.Siparis_Bul(Fat_Tur, Fat_ID) = True Then
            FT.Siparis_Guncelle()
            FT.Siparis_Baslik.Uyari_Kontrol = 1
            If FT.Siparis_Onayla() = True Then
                KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
                KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
                Return "Sipariş Onaylandı"
            Else
                KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
                KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
                Return FT.Hata_text
            End If
        Else
            KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
            KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
            Return "Sipariş Bulunamadı"
        End If

        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        Return "Sipariş Bulunamadı"

    End Function
    Private Function Fatura_Onayla(ByVal Fat_ID As Long, ByVal Fat_Tur As Integer, ByVal FirmaID As Integer) As String
        Dim SQ As String = ""
        Dim DT As New DataTable
        Dim X As Long = 0
        Dim Y As Long = 0
        Dim Firma As String = ""

        B_SQL = connstr
        B_PRJ = "PDA_TRANS"
        SQL_TUR = "2005"

        If FirmaID <> 0 Then
            Firma = New String("0", 3 - FirmaID.ToString.Length) & FirmaID
            KUL.tfrm = "tbl_" & Firma & "_"
            KUL.tper = "tbl_" & Firma & "_" & "01" & "_"

        Else
            KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
            KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        End If

        Dim FT As New X_Fatura
        Dim KL As New X_Fatura.Veri_Detay


        If FT.Fatura_Bul(Fat_Tur, Fat_ID) = True Then
            FT.Fatura_Guncelle()
            FT.Fatura_Baslik.Uyari_Kontrol = 1
            If FT.Fatura_Onayla() = True Then
                KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
                KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
                Return "Fatura Onaylandı"
            Else
                KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
                KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
                Return FT.Hata_text
            End If
        Else
            KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
            KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
            Return "Fatura Bulunamadı"
        End If

        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        Return "Fatura Bulunamadı"

    End Function

    ''' <summary>
    ''' DÖVİZ KUR GETİR
    ''' </summary>
    ''' <param name="Doviz_ID">DÖVİZ ID</param>
    ''' <param name="Evrak_Tarih">TARİH</param>
    ''' <returns></returns>
    <WebMethod()>
    Public Function Doviz_Kur_Getir(ByVal Doviz_ID As Integer, ByVal Evrak_Tarih As String) As Double
        Dim Sonuc As Double = 0
        B_SQL = connstr
        B_PRJ = "PDA_TRANS"
        SQL_TUR = "2005"

        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()

        Sonuc = doviz_kuru(Doviz_ID, Evrak_Tarih)
        Return Sonuc
    End Function
    <WebMethod()>
    Public Function Genel_Iskonto(ByVal XTur As String, ByVal Fat_Tur As Integer, ByVal Fat_ID As Long, ByVal Isk_Tur As Integer, ByVal Isk_Tutar As Double, ByVal FirmaID As Integer) As Boolean
        Dim Sonuc As Double = 0
        B_SQL = connstr
        B_PRJ = "PDA_TRANS"
        SQL_TUR = "2005"





        Dim X As Integer
        Dim TOP As Double
        Dim KDVTOP As Double = 0
        Dim ISKTOP As Double = 0

        Dim GISK As Double
        Dim SISK As Double
        Dim SYAG As Double = 0
        Dim SYIS As Double
        Dim Firma As String = ""
        Dim DTTablo As New DataTable
        Dim Tur As String = ""

        Dim SQ As String
        Dim DT As New DataTable



        GISK = Isk_Tutar

        If FirmaID <> 0 Then
            Firma = New String("0", 3 - FirmaID.ToString.Length) & FirmaID
            KUL.tfrm = "tbl_" & Firma & "_"
            KUL.tper = "tbl_" & Firma & "_" & "01" & "_"

        Else
            KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
            KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        End If

        SQ = ""
        SQ = "SELECT a_tur "
        SQ = SQ & " FROM " & KUL.tper & "maxid"
        SQ = SQ & " WHERE a_id=" & Fat_Tur
        DTTablo = SQL_TABLO(SQ, "T", False, False, False)

        Tur = NULA(DTTablo, 0, "a_tur")

        If Isk_Tur = 4 Then
            'Genel İsk kaldır
            SQ = ""
            SQ = "Update "
            Select Case Tur
                Case "SGFAT", "SGIRS"
                    SQ = SQ & " " & KUL.tper & "stkgdet"
                Case "SCFAT", "SCIRS"
                    SQ = SQ & " " & KUL.tper & "stkcdet"
            End Select
            SQ = SQ & " SET "
            SQ = SQ & " a_gsisko =" & 0
            SQ = SQ & " WHERE a_tur = " & Fat_Tur & ""
            SQ = SQ & " AND a_id = " & Fat_ID & ""
            Call SQL_KOS(SQ, False)

            KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
            KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
            Return True
        End If



        SQ = " SELECT Master.a_sira, (Master.a_brmfiy * Master.a_mik) as a_tutar, Master.a_kdvtut, Master.a_isktut, Stok.a_reftip "
        Select Case Tur
            Case "SGFAT"
                SQ = SQ & " FROM " & KUL.tper & "stkgdet"
            Case "SCFAT"
                SQ = SQ & " FROM " & KUL.tper & "stkcdet"
        End Select
        SQ = SQ & " AS Master INNER JOIN "
        SQ = SQ & " " & KUL.tfrm & "stok as Stok ON Master.a_stok_id = Stok.a_id"
        SQ = SQ & " WHERE Master.a_fattur=" & Fat_Tur & " AND Master.a_fatid=" & Fat_ID
        SQ = SQ & " AND Master.a_refsira IS NULL "
        DT = SQL_TABLO(SQ, "T", False, False, False)

        If DT.Rows.Count = 0 Then
            Return False
        End If


        For X = 0 To DT.Rows.Count - 1
            'Satır Toplamlarının Toplamı
            TOP = TOP + NULD(DT, X, "a_tutar")
            KDVTOP = KDVTOP + NULD(DT, X, "a_kdvtut")
            ISKTOP = ISKTOP + NULD(DT, X, "a_isktut")
        Next X


        For X = 0 To DT.Rows.Count - 1
            If TOP > 0 Then

                Select Case Isk_Tur
                    Case 1
                        SISK = (GISK * 100) / ((TOP + KDVTOP) - (ISKTOP))
                    Case 2
                        SISK = (GISK * 100) / ((TOP) - (ISKTOP))
                    Case 3
                        SISK = GISK

                End Select

                If SISK <> 0 Then
                    SYIS = SISK
                    'a_gsisko
                    SQ = ""
                    SQ = "Update "
                    Select Case Tur
                        Case "SGFAT"
                            SQ = SQ & " " & KUL.tper & "stkgdet"
                        Case "SCFAT"
                            SQ = SQ & " " & KUL.tper & "stkcdet"
                    End Select
                    SQ = SQ & " SET "
                    SQ = SQ & " a_gsisko =" & SYIS
                    SQ = SQ & " WHERE a_tur = " & Fat_Tur & ""
                    SQ = SQ & " AND a_id = " & Fat_ID & ""
                    SQ = SQ & " AND a_sira = " & NULN(DT, X, "a_sira") & ""
                    If SQL_KOS(SQ, False) = True Then
                        If NULA(DT, X, "a_reftur") = "A" Then
                            GenelIsk_Tali_Satir_Guncelle(Fat_Tur, Fat_ID, NULN(DT, X, "a_sira"), Tur)
                        End If
                    End If
                End If
            End If
        Next


        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        Return True
    End Function
    Private Sub GenelIsk_Tali_Satir_Guncelle(ByVal FTUR As Integer, ByVal FID As Long, ByVal RowHandle As Integer, ByVal Tur As String)

        Dim SQ As String = ""
        Dim DT As New DataTable
        Dim LN As New DataTable
        Dim X As Integer = 0


        Dim BMIK As Double = 0
        Dim AMIK As Double = 0
        Dim MCAR As Double = 0

        Dim FIY As Double = 0
        Dim ISK As Double = 0
        Dim KDV As Double = 0
        Dim TUT As Double = 0
        Dim DTUT As Double = 0
        Dim KBS As Integer = 2
        Dim GISK As Double = 0
        Dim OTV As Double = 0
        Dim OIV As Double = 0

        'XGrid1.X_Kolon_Ekle("T", 22, "İrsaliye Türü", "itur", 20, 5, "0", "", 4, False, False, Color.WhiteSmoke, Color.Blue, DataGridViewColumnSortMode.NotSortable, F)
        'XGrid1.X_Kolon_Ekle("T", 23, "İrsaliye Nosu", "ino", 20, 5, "0", "", 4, False, False, Color.WhiteSmoke, Color.Blue, DataGridViewColumnSortMode.NotSortable, F)

        SQ = ""
        SQ = "SELECT   a_tur, a_id, a_sira, a_tarih, a_cari_id, a_stok_id, a_ambar_id, a_mik, a_brmfiy, a_kdv, a_kdvtut, a_isk, a_isk1, a_isk2, a_isktut, a_otv, a_otvtut, a_tutar, "
        SQ = SQ & " a_ack, a_durum, a_atar, a_perid, a_bloke, a_kesin, a_did, a_dtut, a_dkur, a_indirim, a_masraf, a_isk3, a_isk4, a_isk5, a_masmer, a_masyer, a_fatfiyat, a_fateh,"
        SQ = SQ & " a_fattur, a_fatid, a_sbrm, a_sbrmcrp, a_sbrmmik, a_sbrmfiy, a_oivy, a_oivt, a_gsiskt, a_gsisko, a_refsira"
        Select Case Tur
            Case "SGFAT"
                SQ = SQ & " " & KUL.tper & "stkgdet"
            Case "SCFAT"
                SQ = SQ & " " & KUL.tper & "stkcdet"
        End Select
        SQ = SQ & " WHERE  (a_tur = " & FTUR & ") AND (a_id = " & FID & ") AND (a_sira = " & RowHandle & ")"
        LN = SQL_TABLO(SQ, "T", False, False, False)


        SQ = ""
        SQ = "SELECT   a_tur, a_id, a_sira, a_tarih, a_cari_id, a_stok_id, a_ambar_id, a_mik, a_brmfiy, a_kdv, a_kdvtut, a_isk, a_isk1, a_isk2, a_isktut, a_otv, a_otvtut, a_tutar, "
        SQ = SQ & " a_ack, a_durum, a_atar, a_perid, a_bloke, a_kesin, a_did, a_dtut, a_dkur, a_indirim, a_masraf, a_isk3, a_isk4, a_isk5, a_masmer, a_masyer, a_fatfiyat, a_fateh,"
        SQ = SQ & " a_fattur, a_fatid, a_sbrm, a_sbrmcrp, a_sbrmmik, a_sbrmfiy, a_oivy, a_oivt, a_gsiskt, a_gsisko, a_refsira"
        Select Case Tur
            Case "SGFAT"
                SQ = SQ & " " & KUL.tper & "stkgdet"
            Case "SCFAT"
                SQ = SQ & " " & KUL.tper & "stkcdet"
        End Select
        SQ = SQ & " WHERE  (a_tur = " & FTUR & ") AND (a_id = " & FID & ") AND (a_refsira = " & RowHandle & ")"
        DT = SQL_TABLO(SQ, "T", False, False, False)
        For X = 0 To DT.Rows.Count - 1

            AMIK = NULD(DT, X, "a_sbrmmik") * NULD(LN, 0, "a_sbrmcrp")

            GISK = kurus((NULD(LN, 0, "a_gsiskt") / NULD(LN, 0, "a_mik")) * AMIK, KBS)


            SQ = ""
            SQ = "UPDATE "
            Select Case Tur
                Case "SGFAT"
                    SQ = SQ & " " & KUL.tper & "stkgdet"
                Case "SCFAT"
                    SQ = SQ & " " & KUL.tper & "stkcdet"
            End Select
            SQ = SQ & " SET "
            SQ = SQ & " a_gsisko  = " & NULD(LN, 0, "a_gsisko") & ", "
            SQ = SQ & " a_gsiskt  = " & GISK & ""
            SQ = SQ & " WHERE  (a_tur = " & FTUR & ") AND (a_id = " & FID & ") AND (a_sira = " & NULN(DT, X, "a_sira") & ")"
            SQL_KOS(SQ, False)
        Next

    End Sub
    <WebMethod()>
    Public Function Banka_Dekont_Ekle(ByVal Tur As Integer, ByVal BankaID As Integer, ByVal CariID As Integer, ByVal Tutar As Double, ByVal ETarih As String, ByVal Ack As String, ByVal CekimTur As Integer, ByVal Taksit As Integer, ByVal Firma As Integer) As Long
        Dim BN_ID As Long
        Dim FirAd As String
        B_SQL = connstr
        B_PRJ = "PDA_TRANS"
        SQL_TUR = "2005"
        Dim SQ As String = ""
        Dim DT As New DataTable


        If Firma <> 0 Then
            FirAd = New String("0", 3 - Firma.ToString.Length) & Firma
            KUL.tfrm = "tbl_" & FirAd & "_"
            KUL.tper = "tbl_" & FirAd & "_" & "01" & "_"

        Else
            KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
            KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        End If




        Dim BD As New XBanka_Dekontlari
        BD.V00_id = 0
        BD.V00_tur = Tur
        BD.V00_TurAdi = VT_BILGI(KUL.tper & "maxid", "a_id", "a_adi", Tur)
        BD.V01_Banka_Id = BankaID
        BD.V01_Cari_Id = CariID
        BD.V01_Kasa_Id = 0
        BD.V01_VBanka_Id = 0
        BD.V04_RefId = CariID
        BD.V05_TahsilTarihi = ETarih
        BD.V05_Tarihi = ETarih
        BD.V05_VadeTarihi = ETarih
        BD.V06_Aciklama = Ack
        BD.V07_Tutar = Tutar
        BD.V08_IslemYeri = "BANKA"
        BD.V09_YerTur = 0
        BD.V10_YerId = 0
        BD.V16_Cekim_Tur = CekimTur
        If Tur = 65 Then
            Select Case CekimTur
                Case 0
                    BD.V16_Cekim_Turu = "KREDİ KARTI TEK"
                    BD.V17_Cekim_Taksit = 1
                Case 1
                    BD.V16_Cekim_Turu = "KREDİ KARTI TAKSİT"
                    BD.V17_Cekim_Taksit = Taksit
                Case 2
                    BD.V16_Cekim_Turu = "DİGER BANKA KARTLARI"
                    BD.V17_Cekim_Taksit = 1
            End Select
            BD.V18_Bloke = 0
        Else
            BD.V16_Cekim_Turu = "KREDİ KARTI TEK"
            BD.V17_Cekim_Taksit = 1
            BD.V18_Bloke = 1
        End If
        BD.V19_VardiyaId = 0
        BD.V20_HesapPlanID = 0
        BD.X_Mesaj = False
        BN_ID = BD.Dekont_Ekle(Tur)

        If Tur = 65 Then
            If BN_ID <> 0 Then
                SQ = ""
                SQ = "SELECT a_tur,a_id"
                SQ = SQ & " FROM " & KUL.tper & "nakitgc"
                SQ = SQ & " WHERE a_tur=65 and a_kkt_mas=" & BN_ID
                DT = SQL_TABLO(SQ, "T", False, False, False)

                If DT.Rows.Count <> 0 Then
                    BN_ID = NULN(DT, 0, "a_id")
                End If

            End If
        End If

        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        Return BN_ID


    End Function
    <WebMethod()>
    Public Function Banka_Dekont_Sil(ByVal Tur As Integer, ByVal IDD As Integer, ByVal Firma As Integer) As String
        Dim FirAd As String
        B_SQL = connstr
        B_PRJ = "PDA_TRANS"
        SQL_TUR = "2005"

        If Firma <> 0 Then
            FirAd = New String("0", 3 - Firma.ToString.Length) & Firma
            KUL.tfrm = "tbl_" & FirAd & "_"
            KUL.tper = "tbl_" & FirAd & "_" & "01" & "_"

        Else
            KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
            KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        End If

        Dim BD As New XBanka_Dekontlari

        'If BD.Dekont_Bul(Tur, IDD) = False Then
        '    KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        '    KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        '    Return "Dekont kaydı Bulunamadı"
        'End If

        If BD.Dekont_Sil(Tur, IDD) = False Then
            KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
            KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
            Return "Dekont Kaydı Silinemedi"
        End If



        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        Return "Dekont Silindi"


    End Function
    <WebMethod()>
    Public Function Banka_Dekont_Bul(ByVal Tur As Integer, ByVal IDD As Integer, ByVal Firma As Integer) As String
        Dim FirAd As String
        Dim SQ As String
        Dim DT As New DataTable
        Dim DT2 As New DataTable


        B_SQL = connstr
        B_PRJ = "PDA_TRANS"
        SQL_TUR = "2005"


        If Firma <> 0 Then
            FirAd = New String("0", 3 - Firma.ToString.Length) & Firma
            KUL.tfrm = "tbl_" & FirAd & "_"
            KUL.tper = "tbl_" & FirAd & "_" & "01" & "_"

        Else
            KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
            KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        End If



        SQ = ""
        SQ = "SELECT a_banka_id, a_id, a_bloke, a_islemno, a_belgeno, a_ref_id, a_tarih, a_ack, a_tur, a_mebla, a_yer, a_yerid, a_did, a_dtut, a_dkur, a_masmer, a_masyer, a_cuser, a_cdate, a_ctime, a_muser, a_mdate, a_mtime, a_sube_id"
        SQ = SQ & " , a_tektal, a_taksay, a_hplan_id, a_vtarih, a_ttarih, a_kkt_mas, a_kkt_komy, 0 as a_duzelt, '' as a_banka_adi, '' as a_cari_adi"
        SQ = SQ & " FROM " & KUL.tper & "nakitgc"
        SQ = SQ & " WHERE a_tur=" & Tur & " AND a_id=" & IDD & ""
        DT = SQL_TABLO(B_SQL, SQ, "T", False, False, False)


        If NULN(DT, 0, "a_tur") = 65 Then
            If NULN(DT, 0, "a_kkt_mas") <> 0 Then

                SQ = "SELECT a_tur, a_id, a_bloke, a_kkt_mas, a_kkt_komy"
                SQ = SQ & " FROM " & KUL.tper & "nakitgc"
                SQ = SQ & " WHERE (a_tur = 65) AND (a_kkt_mas = " & NULN(DT, 0, "a_kkt_mas") & ") AND (a_bloke = 0)"
                DT2 = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                If DT2.Rows.Count <> 0 Then
                    DT.Rows(0).Item("a_duzelt") = 1
                End If

                Dim CD As New XCari.XCari_Hesap_Dekontlari
                If CD.Dekont_Bul(Islem_Turu.a023_Cari_Hesap_Alacak_Dekontu, NULN(DT, 0, "a_kkt_mas")) = True Then

                    DT.Rows(0).Item("a_ack") = CD.V06_Aciklama
                    DT.Rows(0).Item("a_mebla") = CD.V07_Tutar
                End If
            End If
        Else
            If NULN(DT, 0, "a_bloke") = 0 Then
                DT.Rows(0).Item("a_duzelt") = 1
            End If
        End If

        DT.Rows(0).Item("a_banka_adi") = VT_BILGI(KUL.tfrm & "banka", "a_id", "a_adi", NULN(DT, 0, "a_banka_id"))
        DT.Rows(0).Item("a_cari_adi") = VT_BILGI(KUL.tfrm & "cari", "a_id", "a_adi", NULN(DT, 0, "a_ref_id"))

        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        Return StringOlustur(DT)


    End Function
    <WebMethod()>
    Public Function Siparis_Sevk(ByVal SipID As Integer, ByVal FatTur As Integer, ByVal Stoklar As String, ByVal Firma As Integer) As Boolean


        Dim DT As New DataTable
        Dim SQ As String = ""
        Dim DT2 As New DataTable
        Dim ID As Long = 0
        Dim M As Integer = 0
        Dim StokIDler As String = ""

        Dim FirmaID As String = ""

        B_SQL = connstr
        B_PRJ = "PDA_TRANS"
        SQL_TUR = "2005"

        If Firma <> 0 Then
            FirmaID = New String("0", 3 - Firma.ToString.Length) & Firma
            KUL.tfrm = "tbl_" & FirmaID & "_"
            KUL.tper = "tbl_" & FirmaID & "_" & "01" & "_"

        Else
            KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
            KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        End If


        'Stoklar = "a_tur	a_id	a_sira	a_stok_id	a_stok_kod	a_stok_adi	a_mik	" & vbCrLf
        'Stoklar = Stoklar & "33	3	1	291	alm hrd	ALÜMİNYUM HURDA TALAŞ	10	" & vbCrLf
        'Stoklar = Stoklar & "0	0	0	291	alm hrd	ALÜMİNYUM HURDA TALAŞ	5	" & vbCrLf
        'Stoklar = Stoklar & "33	3	2	241	alm kmp pnl	ALÜMİNYUM KOMPOZİT PANEL	20	" & vbCrLf
        'Stoklar = Stoklar & "33	3	4	3	alm Lev 5083	ALÜMİNYUM LEVHA 5083	20	" & vbCrLf
        'Stoklar = Stoklar & "0	0	0	225	FY FR	FİYAT FARKI	20" & vbCrLf

        DT = StringToDatatable(Stoklar)

        If DT.Rows.Count = 0 Then
            Return False

        Else

            For x = 0 To DT.Rows.Count - 1

                If NULN(DT, x, "a_tur") = 33 Then
                    StokIDler = StokIDler & NULN(DT, x, "a_stok_id")
                    StokIDler = StokIDler & ","
                End If
            Next

            StokIDler = StokIDler.Substring(0, StokIDler.Length - 1)

            SQ = ""
            SQ = "SELECT "
            SQ = SQ & " sipmas.a_kod, "
            SQ = SQ & " sipmas.a_bno, "
            SQ = SQ & " sipmas.a_ack, "
            SQ = SQ & " sipmas.a_tarih, "
            SQ = SQ & " sipmas.a_cyansit, "
            SQ = SQ & " sipmas.a_cambar_id, "
            SQ = SQ & " sipdet.a_ambar_id, "
            SQ = SQ & " sipmas.a_sube_id, "
            SQ = SQ & " sipdet.a_tur, "
            SQ = SQ & " sipdet.a_id, "
            SQ = SQ & " sipdet.a_sira, "
            SQ = SQ & " sipdet.a_sirano, "
            SQ = SQ & " sipdet.a_cari_id, "
            SQ = SQ & " sipdet.a_stok_id, "
            SQ = SQ & " sipdet.a_mik, "
            SQ = SQ & " sipdet.a_kapamik, "
            SQ = SQ & " sipdet.a_mik , sipdet.a_kapamik , "
            SQ = SQ & " 0.00 AS a_sevk, "
            SQ = SQ & " sipdet.a_brmfiy, "
            SQ = SQ & " sipdet.a_tutar, "
            SQ = SQ & " sipdet.a_teslim, "
            SQ = SQ & " sipdet.a_durum, "
            SQ = SQ & " sipdet.a_atar, "
            SQ = SQ & " sipdet.a_isk ,"

            SQ = SQ & " sipdet.a_isk1 ,"
            SQ = SQ & " sipdet.a_isk2 ,"
            SQ = SQ & " sipdet.a_isk3 ,"
            SQ = SQ & " sipdet.a_isk4 ,"
            SQ = SQ & " sipdet.a_isk5 ,"

            SQ = SQ & " STOK.a_sakdvor, "
            SQ = SQ & " STOK.a_grup1, "
            SQ = SQ & " STOK.a_grup2, "
            SQ = SQ & " STOK.a_grup3, "
            SQ = SQ & " STOK.a_grup4, "
            SQ = SQ & " STOK.a_grup5, "
            SQ = SQ & " STOK.a_grup6, "
            SQ = SQ & " STOK.a_marka, "
            SQ = SQ & " STOK.a_model, "

            SQ = SQ & " stok.a_kod AS s_kod, "
            SQ = SQ & " stok.a_adi AS s_adi, "
            SQ = SQ & " stok.a_birim, "
            SQ = SQ & " stok.a_barkod, "
            SQ = SQ & " stok.a_reftip,"
            SQ = SQ & " stok.a_kalan AS a_skalan,"

            SQ = SQ & " cari.a_kod AS c_kod,"
            SQ = SQ & " cari.a_adi AS c_adi,"
            SQ = SQ & " sipdet.a_gsisko,"
            SQ = SQ & " sipdet.a_refsira,"
            SQ = SQ & " sipdet.a_did , "
            SQ = SQ & " sipdet.a_dtut , "
            SQ = SQ & " sipdet.a_dkur  ,"
            SQ = SQ & " sipdet.a_sbrm"

            SQ = SQ & " FROM "
            SQ = SQ & " " & KUL.tper & "sipdet AS sipdet INNER JOIN"
            SQ = SQ & " " & KUL.tper & "sipmas AS sipmas ON sipdet.a_tur = sipmas.a_tur AND sipdet.a_id = sipmas.a_id INNER JOIN"
            SQ = SQ & " " & KUL.tfrm & "stok AS stok ON sipdet.a_stok_id = stok.a_id  INNER JOIN"
            SQ = SQ & " " & KUL.tfrm & "cari AS cari ON sipmas.a_cari_id = cari.a_id "

            SQ = SQ & " WHERE (sipdet.a_tur = 33)  "
            SQ = SQ & " AND (sipmas.a_id = " & SipID & ")"
            SQ = SQ & " AND sipdet.a_stok_id IN(" & StokIDler & ")"
            SQ = SQ & " ORDER BY sipdet.a_tarih ,sipmas.a_bno, sipdet.a_sira"
            DT2 = SQL_TABLO(SQ, "T", False, True, False)

            If DT2.Rows.Count = 0 Then
                KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
                KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
                Return False
            End If

            For X = 0 To DT2.Rows.Count - 1
                For Y = 0 To DT.Rows.Count - 1
                    If NULN(DT2, X, "a_stok_id") = NULN(DT, Y, "a_stok_id") Then
                        DT2.Rows(X)("a_sevk") = NULD(DT, Y, "a_mik")
                    End If
                Next
            Next
            For Y = 0 To DT.Rows.Count - 1
                If NULN(DT, Y, "a_tur") = 0 Then
                    DT2.Rows.Add()

                    DT2.Rows(DT2.Rows.Count - 1).Item("a_ambar_id") = NULN(DT2, 0, "a_ambar_id")
                    DT2.Rows(DT2.Rows.Count - 1).Item("a_brmfiy") = FIYAT_BUL(FatTur, NULA(DT, Y, "a_stok_kod"), NULN(DT2, 0, "a_cari_id"), TTAR.TARIH, True)
                    DT2.Rows(DT2.Rows.Count - 1).Item("s_adi") = NULA(DT, Y, "a_stok_adi")
                    DT2.Rows(DT2.Rows.Count - 1).Item("a_did") = 0
                    DT2.Rows(DT2.Rows.Count - 1).Item("a_dkur") = 1
                    DT2.Rows(DT2.Rows.Count - 1).Item("a_gsisko") = 0
                    DT2.Rows(DT2.Rows.Count - 1).Item("a_isk") = 0
                    DT2.Rows(DT2.Rows.Count - 1).Item("a_isk1") = 0
                    DT2.Rows(DT2.Rows.Count - 1).Item("a_isk2") = 0
                    DT2.Rows(DT2.Rows.Count - 1).Item("a_isk3") = 0
                    DT2.Rows(DT2.Rows.Count - 1).Item("a_isk4") = 0
                    DT2.Rows(DT2.Rows.Count - 1).Item("a_isk5") = 0
                    DT2.Rows(DT2.Rows.Count - 1).Item("a_sevk") = NULD(DT, Y, "a_mik")
                    DT2.Rows(DT2.Rows.Count - 1).Item("a_sakdvor") = VT_BILGI_D(KUL.tfrm & "stok", "a_id", "a_sakdvor", NULN(DT, Y, "a_stok_id"))
                    DT2.Rows(DT2.Rows.Count - 1).Item("a_tur") = 0
                    DT2.Rows(DT2.Rows.Count - 1).Item("a_id") = 0
                    DT2.Rows(DT2.Rows.Count - 1).Item("a_sira") = 0
                    DT2.Rows(DT2.Rows.Count - 1).Item("a_refsira") = 0
                    DT2.Rows(DT2.Rows.Count - 1).Item("a_stok_id") = NULN(DT, Y, "a_stok_id")
                    DT2.Rows(DT2.Rows.Count - 1).Item("a_sbrm") = VT_BILGI(KUL.tfrm & "stok", "a_id", "a_birim", NULN(DT, Y, "a_stok_id"))
                End If
            Next

        End If



        Dim FT As New X_Fatura
        Dim KL As New X_Fatura.Veri_Detay

        FT.Fatura_Baslik.Fatura_Turu = FatTur
        FT.Fatura_Baslik.Acik_Kapali = "A"
        FT.Fatura_Baslik.Aciklaması = "PDA - Sipariş Sevk"
        FT.Fatura_Baslik.Adres_Id = 0
        FT.Fatura_Baslik.Belge_Nosu = ""
        FT.Fatura_Baslik.Cari_Id = NULN(DT2, 0, "a_cari_id")
        FT.Fatura_Baslik.Cikti_Turu = "E"
        FT.Fatura_Baslik.Depo_Id = NULN(DT2, 0, "a_ambar_id")
        FT.Fatura_Baslik.Depo_Id_Ters = 0
        FT.Fatura_Baslik.Doviz_Id = 0
        FT.Fatura_Baslik.Doviz_Kuru = 1
        FT.Fatura_Baslik.Fatura_Baglantisi = 0
        FT.Fatura_Baslik.Fatura_Grup_Turu = 0
        FT.Fatura_Baslik.Fiyat_Listesi_Id = 0
        FT.Fatura_Baslik.Genel_Iskontosu = 0
        FT.Fatura_Baslik.Irsaliye_Nosu = ""
        FT.Fatura_Baslik.Irsaliye_Tarihi = TTAR.TARIH
        FT.Fatura_Baslik.Islem_Nosu = ""
        FT.Fatura_Baslik.Islem_Yeri = "FATURA"
        FT.Fatura_Baslik.Islem_Yeri_Id = 0
        FT.Fatura_Baslik.Islem_Yeri_Tur = FatTur
        FT.Fatura_Baslik.Kapama_Taksit_Sayisi = 0
        FT.Fatura_Baslik.Kapama_Turu = 1
        FT.Fatura_Baslik.Kapama_Yeri = 0
        FT.Fatura_Baslik.Kdv_Tevkifat_Orani = 0
        FT.Fatura_Baslik.Komisyon_Tutari = 0
        FT.Fatura_Baslik.Ozel_Iletisim_Vergi_Orani = 0
        FT.Fatura_Baslik.Komisyon_Tutari = 0
        FT.Fatura_Baslik.Personel_Id = 0
        FT.Fatura_Baslik.Puan_Kapama_Taksit_Sayisi = 0
        FT.Fatura_Baslik.Puan_Kapama_Turu = 0
        FT.Fatura_Baslik.Puan_Kapama_Yeri = 0
        FT.Fatura_Baslik.Siparis_Nosu = ""
        FT.Fatura_Baslik.Sube_Id = NULN(DT2, 0, "a_sube_id")
        FT.Fatura_Baslik.Tarihi = TTAR.TARIH
        FT.Fatura_Baslik.Vade_Tarihi = TTAR.TARIH
        FT.Fatura_Baslik.Vardiya_Id = 0



        For X = 0 To DT2.Rows.Count - 1



            KL = New X_Fatura.Veri_Detay

            'KL.Depo_Id = 1
            KL.Depo_Id = NULN(DT2, 0, "a_ambar_id")
            KL.Fatura_Cikti = "E"
            KL.Fatura_Fiyat = NULD(DT2, X, "a_brmfiy")
            KL.Masref_Merkezi = 0
            KL.Masref_Yeri = 0
            KL.Satir_Aciklamasi = NULA(DT2, X, "s_adi")
            KL.Satir_Doviz_Id = NULN(DT2, X, "a_did")
            KL.Satir_Doviz_Kuru = NULD(DT2, X, "a_dkur")
            KL.Satir_Durum_Tarihi = TTAR.TARIH
            KL.Satir_Durumu = ""
            '18.10.2012
            'KL.Satir_Genel_İskonto_Orani =0
            KL.Satir_Genel_İskonto_Orani = NULD(DT2, X, "a_gsisko")
            KL.Satir_Genel_İskonto_Tutari = 0
            KL.Satir_Iskonto_1_Oran = NULD(DT2, X, "a_isk")
            KL.Satir_Iskonto_2_Oran = NULD(DT2, X, "a_isk1")
            KL.Satir_Iskonto_3_Oran = NULD(DT2, X, "a_isk2")
            KL.Satir_Iskonto_4_Oran = NULD(DT2, X, "a_isk3")
            KL.Satir_Iskonto_5_Oran = NULD(DT2, X, "a_isk4")
            KL.Satir_Iskonto_6_Oran = NULD(DT2, X, "a_isk5")
            KL.Satir_Islem_Birim_Fiyati = NULD(DT2, X, "a_brmfiy")
            KL.Satir_Islem_Birim_Miktari = NULD(DT2, X, "a_sevk")
            KL.Satir_Islem_Birimi = NULA(DT2, X, "a_sbrm")
            KL.Satir_Kdv_Orani = NULD(DT2, X, "a_sakdvor")
            KL.Satir_Ozel_Iletisim_Vergi_Orani = 0
            KL.Satir_Ozel_Tuketim_Vergisi_Orani = 0
            KL.Satir_Personel_Id = 1
            KL.Stok_Id = NULN(DT2, X, "a_stok_id")

            'Evrak Sipariş Bağlantısını Yansıt.
            '-------------------------------------------------
            KL.Siparis_Turu = NULN(DT2, X, "a_tur")
            KL.Siparis_Id = NULN(DT2, X, "a_id")
            KL.Siparis_Sira = NULN(DT2, X, "a_sira")
            '-------------------------------------------------
            'FT.Fatura_Satirlari.Add(KL)
            KL.Satir_Sira_No = NULN(DT2, X, "a_sirano")
            KL.Satir_Bagli_Satir_Id = NULN(DT2, X, "a_refsira")
            FT.Fatura_Satir_Ekle(KL)
            M = M + 1

        Next


        If M > 0 Then

            ID = FT.Fatura_Ekle()
            If ID = 0 Then
                KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
                KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
                Return False
            End If
        Else
            KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
            KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
            Return False
        End If

        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        Return True


    End Function

    Public Class Men_Masa
        Public TableId As Integer
        Public TableCode As String
        Public TableName As String
        Public BranchID As Integer
        Public BranchName As String
        Public PlaceID As Integer
        Public PlaceName As String
        Public Chairs As Integer
        Public IsAvailable As Boolean
        '  Public FreeChairs As Integer
    End Class

    <WebMethod()>
    Public Function SetRsv(ByVal TableId As Integer, ByVal GuestNum As Integer, ByVal CustNam As String, ByVal RsvDate As String, ByVal RsvTime As String, ByVal BranchID As Integer, ByVal Memo As String) As Boolean
        Dim DT As New DataTable
        Dim SQ As String = ""


        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()


        SQ = ""
        SQ = "INSERT INTO " & KUL.tfrm & "men_rzv"
        SQ = SQ & " (a_masa, a_kisisay, a_cariid, a_musteri, a_cuser, a_cdate, a_ctime, a_muser, a_mdate, a_mtime, "
        SQ = SQ & " a_rzvtarih, a_rzvsaat, a_rzvsure, a_sube, a_aciklama, a_durum ) "

        SQ = SQ & " Select "
        SQ = SQ & " " & TableId & " As a_masa,"
        SQ = SQ & " " & GuestNum & " As a_kisisay,"
        SQ = SQ & " 0 As a_cariid,"
        SQ = SQ & " '" & CustNam & "' As a_musteri,"
        SQ = SQ & " " & Val(KUL.KOD) & " as a_cuser,"
        SQ = SQ & " N'" & TTAR.TR2UK(Date.Now.ToShortDateString) & "' as a_cdate,"
        SQ = SQ & " N'" & Date.Now.ToShortTimeString & "' as a_ctime,"
        SQ = SQ & " " & Val(KUL.KOD) & " as a_muser,"
        SQ = SQ & " N'" & TTAR.TR2UK(Date.Now.ToShortDateString) & "' as a_mdate,"
        SQ = SQ & " N'" & Date.Now.ToShortTimeString & "' as a_mtime,"
        SQ = SQ & " N'" & TTAR.TR2UK(RsvDate) & "' as a_rzvtarih,"
        SQ = SQ & " N'" & RsvTime & "' as a_rzvsaat,"
        SQ = SQ & " 0 As a_rzvsure,"
        SQ = SQ & " " & BranchID & " As a_sube,"
        SQ = SQ & " '" & Memo & "' As a_aciklama,"
        SQ = SQ & " 1 As a_durum"


        SetRsv = SQL_KOS(SQ, False)




    End Function
    <WebMethod()>
    Public Function GetTablesStatus(ByVal Tarih As String, ByVal Saat As String) As Men_Masa()
        Dim DT As New DataTable
        Dim SQ As String = ""
        Dim Durum As Boolean

        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()

        '  B_SQL = "Data Source=.;Integrated Security=False;Initial Catalog=ALTINDERE;User ID=sa;Password="


        SQ = " Select masa.a_id as MasaID, masa.a_kod as MasaKod,masa.a_adi as MasaAdi,masa.a_sandalye as Sandalye, masa.a_yerid as MekanID, yer.a_adi as MekanAdi, "
        SQ = SQ & " Case When masa.a_durum='BOŞ' AND " 'TALEP EDİLEN SAATİN 1.5 SAAT ÖNÜNE VE ARKASINA REZERV KAYDI YOKSA MASA MÜSAİTTİR.
        SQ = SQ & " (Select Top 1 a_id from tbl_001_men_rzv as rzv where rzv.a_rzvtarih='" & TTAR.TR2UK(Tarih) & "' AND "
        SQ = SQ & " rzv.a_masa=masa.a_id And a_durum=1 AND (a_rzvsaat between DATEADD(MINUTE,-80,'" & Saat & "' ) AND DATEADD(MINUTE,80, '" & Saat & "' ) )) IS NULL THEN 1 " '(a_rzvsaat between '" & Saat & "' AND '18:00')) IS NULL "

        SQ = SQ & " When  masa.a_durum='DOLU' AND "  'MASADA OTURAN VARSA, TALEP EDİLEN SAAT ADİSYON AÇILIŞINDAN 1.5 SAAT SONRA İSE VE BAŞKA BİR REZERV KAYDI DA YOKSA MASA MÜSAİTTİR.
        SQ = SQ & " (SELECT TOP 1 ABS(DATEDIFF(MINUTE, '" & Saat & "' ,CONVERT(NVARCHAR(50),a_saat,108))) FROM  tbl_001_men_adisyon WHERE a_mid=masa.a_id ) > 80 AND "
        SQ = SQ & " (Select Top 1 a_id from tbl_001_men_rzv as rzv where rzv.a_rzvtarih='" & TTAR.TR2UK(Tarih) & "' AND "
        SQ = SQ & " rzv.a_masa=masa.a_id And a_durum=1 AND (a_rzvsaat between DATEADD(MINUTE,-80,'" & Saat & "' ) AND DATEADD(MINUTE,80, '" & Saat & "' ) )) IS NULL "
        SQ = SQ & " THEN 1"
        SQ = SQ & " Else 0 End As MusaitMi ,"
        SQ = SQ & " masa.a_sube As SubeID,  sube.a_adi As SubeAdi"
        SQ = SQ & " From tbl_001_men_masa as masa "
        SQ = SQ & " inner join tbl_001_men_masayer as yer on yer.a_id=masa.a_yerid "
        SQ = SQ & " inner join tbl_001_sube as sube on sube.a_id=masa.a_sube "

        SQ = SQ.Replace("tbl_001_01_", KUL.tper)
        SQ = SQ.Replace("tbl_001_", KUL.tfrm)

        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)

        Dim MS(DT.Rows.Count - 1) As Men_Masa



        For X = 0 To DT.Rows.Count - 1
            If NULN(DT, X, "MusaitMi") = 0 Then
                Durum = False
            Else
                Durum = True

            End If

            MS(X) = New Men_Masa
            MS(X).TableId = NULN(DT, X, "MasaID")
            MS(X).TableCode = NULA(DT, X, "MasaKod")
            MS(X).TableName = NULA(DT, X, "MasaAdi")
            MS(X).Chairs = NULN(DT, X, "Sandalye")
            MS(X).IsAvailable = Durum
            '  MS(X).FreeChairs = NULN(DT, X, "BosSandalye")
            MS(X).BranchID = NULN(DT, X, "SubeID")
            MS(X).PlaceID = NULN(DT, X, "MekanID")
            MS(X).BranchName = NULA(DT, X, "SubeAdi")
            MS(X).PlaceName = NULA(DT, X, "MekanAdi")

            ' If MS(X).FreeChairs < 0 Then MS(X).FreeChairs = 0

        Next

        Return MS

    End Function




    Public Class WS_Stok
        Public ID As Integer
        Public KOD As String
        Public AD As String
        Public BARKOD As String
        Public SFIYAT As Double
        Public OFIYAT1 As Double
        Public OFIYAT2 As Double
        Public OFIYAT3 As Double
        Public OFIYAT4 As Double
        Public OFIYAT5 As Double
        Public OFIYAT6 As Double
        Public DIPFIYAT As Double
        Public KALAN As Integer
        Public REFTIP As String
        Public REFID As Integer
        Public RENKID As Integer
        Public RENK As String
        Public BEDENID As Integer
        Public BEDEN As String
        Public OKOD As String
        Public AADI As String
        Public MARKA As String
        Public MODEL As String
        Public ACIKLAMA As String
        Public BILGI1 As String
        Public BILGI2 As String
        Public BILGI3 As String

        Public GRUP1 As String
        Public GRUP2 As String
        Public GRUP3 As String
        Public GRUP4 As String
        Public GRUP5 As String
        Public GRUP6 As String
        Public GRUP7 As String



    End Class
    <WebMethod()>
    Public Function GetStockList() As WS_Stok()


        Dim SQ As String
        Dim DT As New DataTable


        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        'B_SQL = connstr ' "Data Source=212.156.150.210;Integrated Security=False;Initial Catalog=ANTEX;User ID=bym;Password=0296"

        SQ = " "
        SQ = "SELECT "
        SQ = SQ & " ANTEX.a_id AS a_id, ANTEX.a_adi AS a_adi, ANTEX.a_kod AS a_kod, ANTEX.a_barkod AS a_barkod, ANTEX.a_sfiyat AS a_sfiyat,ANTEX.a_ofiyat1 AS a_ofiyat1,
	                ANTEX.a_ofiyat2 AS a_ofiyat2, ANTEX.a_ofiyat3 AS a_ofiyat3, ANTEX.a_ofiyat4 AS a_ofiyat4, ANTEX.a_ofiyat5 AS a_ofiyat5, ANTEX.a_ofiyat6 AS a_ofiyat6, SUM(ANTEXDEG.a_kalan) AS a_kalan, ANTEX.a_reftip AS a_reftip, ANTEX.a_grup1 AS a_grup1, 
	                ANTEX.a_grup2 AS a_grup2, ANTEX.a_grup3 AS a_grup3, ANTEX.a_grup4 AS a_grup4, ANTEX.a_grup5 AS a_grup5, ANTEX.a_grup6 AS a_grup6, ANTEX.a_okod AS a_okod,
	                ANTEX.a_aadi, ANTEX.a_marka AS a_marka,ANTEX.a_model AS a_model, ANTEX.a_acik AS a_acik, ANTEX.a_bilgi1 AS a_bilgi1, ANTEX.a_bilgi2 AS a_bilgi2, 
	                ANTEX.a_bilgi3 AS a_bilgi3 "
        SQ = SQ & " FROM tbl_009_stok AS ANTEX "
        SQ = SQ & " LEFT JOIN tbl_009_stokdeg AS ANTEXDEG ON ANTEXDEG.a_id = ANTEX.a_id "
        SQ = SQ & " LEFT JOIN [AYHANLTD].[DBO].[tbl_003_stok] AS AYHANLTD on ANTEX.a_kod=AYHANLTD.a_kod and ANTEX.a_barkod=AYHANLTD.a_barkod "
        SQ = SQ & " LEFT JOIN [AYHANLTD].[DBO].[tbl_003_stokdeg] as AYHANDEG ON AYHANLTD.a_id=AYHANDEG.a_id "
        SQ = SQ & " WHERE ANTEX.a_webyay = 1 AND ANTEX.a_reftip <> 'T' AND ANTEXDEG.a_ambid IN (13,24) AND ANTEXDEG.a_kalan > 0 OR  (AYHANLTD.a_webyay = 1 AND AYHANLTD.a_reftip <> 'T' AND AYHANDEG.a_ambid = 21 AND AYHANDEG.a_kalan > 0 )"
        SQ = SQ & " GROUP BY ANTEX.a_id, ANTEX.a_adi, ANTEX.a_barkod, ANTEX.a_kod, ANTEX.a_sfiyat, ANTEX.a_ofiyat1, ANTEX.a_ofiyat2, ANTEX.a_ofiyat3,ANTEX.a_ofiyat4, ANTEX.a_ofiyat5, ANTEX.a_ofiyat6, ANTEX.a_reftip, 
	                ANTEX.a_grup1, ANTEX.a_grup2, ANTEX.a_grup3, ANTEX.a_grup4, ANTEX.a_grup5, ANTEX.a_grup6, ANTEX.a_okod, ANTEX.a_aadi, ANTEX.a_marka, ANTEX.a_model, 
	                ANTEX.a_acik, ANTEX.a_bilgi1, ANTEX.a_bilgi2, ANTEX.a_bilgi3 "
        SQ = SQ & " ORDER BY ANTEX.a_kod "
        'SQ = SQ.Replace("tbl_001_01_", KUL.tper)
        'SQ = SQ.Replace("tbl_001_", KUL.tfrm)

        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)

        Dim STK(DT.Rows.Count - 1) As WS_Stok


        For X = 0 To DT.Rows.Count - 1

            STK(X) = New WS_Stok
            STK(X).ID = NULN(DT, X, "a_id")
            STK(X).AD = NULA(DT, X, "a_adi")
            STK(X).KOD = NULA(DT, X, "a_kod")
            STK(X).BARKOD = NULA(DT, X, "a_barkod")
            STK(X).SFIYAT = NULD(DT, X, "a_sfiyat")
            STK(X).OFIYAT1 = NULD(DT, X, "a_ofiyat1")
            STK(X).OFIYAT2 = NULD(DT, X, "a_ofiyat2")
            STK(X).OFIYAT3 = NULD(DT, X, "a_ofiyat3")
            STK(X).OFIYAT4 = NULD(DT, X, "a_ofiyat4")
            STK(X).OFIYAT5 = NULD(DT, X, "a_ofiyat5")
            STK(X).OFIYAT6 = NULD(DT, X, "a_ofiyat6")
            STK(X).DIPFIYAT = NULD(DT, X, "a_dipfiyat")
            STK(X).KALAN = NULD(DT, X, "a_kalan")
            'STK(X).REFID = NULN(DT, X, "a_refstokid")
            STK(X).REFTIP = NULA(DT, X, "a_reftip")
            'STK(X).RENKID = NULN(DT, X, "a_renkid")
            'STK(X).RENK = NULA(DT, X, "a_renkadi")
            'STK(X).BEDENID = NULN(DT, X, "a_bedenid")
            'STK(X).BEDEN = NULA(DT, X, "a_beden")
            STK(X).GRUP1 = NULA(DT, X, "a_grup1")
            STK(X).GRUP2 = NULA(DT, X, "a_grup2")
            STK(X).GRUP3 = NULA(DT, X, "a_grup3")
            STK(X).GRUP4 = NULA(DT, X, "a_grup4")
            STK(X).GRUP5 = NULA(DT, X, "a_grup5")
            STK(X).GRUP6 = NULA(DT, X, "a_grup6")
            STK(X).OKOD = NULA(DT, X, "a_okod")
            STK(X).AADI = NULA(DT, X, "a_aadi")
            STK(X).MARKA = NULA(DT, X, "a_marka")
            STK(X).MODEL = NULA(DT, X, "a_model")
            STK(X).ACIKLAMA = NULA(DT, X, "a_acik")
            STK(X).BILGI1 = NULA(DT, X, "a_bilgi1")
            STK(X).BILGI2 = NULA(DT, X, "a_bilgi2")
            STK(X).BILGI3 = NULA(DT, X, "a_bilgi3")
            'STK(X).GRUP7 = NULA(DT, X, "a_grup7")

        Next

        Return STK


    End Function

    <WebMethod()>
    Public Function GetChildStock(S_CODE As String) As WS_Stok()


        Dim SQ As String
        Dim DT As New DataTable


        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        ' B_SQL = "Data Source=212.156.150.210;Integrated Security=False;Initial Catalog=ANTEX;User ID=bym;Password=0296"

        SQ = " "
        SQ = "SELECT  a_id, a_adi, a_kod, a_barkod, a_sfiyat, a_ofiyat1, a_ofiyat2, a_ofiyat3, a_ofiyat4, a_ofiyat5, a_ofiyat6, a_dipfiyat, SUM(a_kalan) as a_kalan, a_reftip, a_refstokid, a_renkid, a_renkadi, a_bedenid, a_beden, a_grup1,"
        SQ = SQ & " a_grup2, a_grup3, a_grup4, a_grup5, a_grup6, a_okod, a_aadi, a_marka, a_model, a_acik, a_bilgi1, a_bilgi2, a_bilgi3 "
        SQ = SQ & " FROM("
        SQ = SQ & " SELECT  ANTEX.a_id AS a_id, ANTEX.a_adi AS a_adi, ANTEX.a_kod AS a_kod, ANTEX.a_barkod AS a_barkod, ANTEX.a_sfiyat AS a_sfiyat,"
        SQ = SQ & " ANTEX.a_ofiyat1 AS a_ofiyat1, ANTEX.a_ofiyat2 As a_ofiyat2, ANTEX.a_ofiyat3 As a_ofiyat3, ANTEX.a_ofiyat4 As a_ofiyat4,"
        SQ = SQ & " ANTEX.a_ofiyat5 AS a_ofiyat5, ANTEX.a_ofiyat6 AS a_ofiyat6, ANTEX.a_dipfiyat AS a_dipfiyat, CASE WHEN ANTEXDEG.a_kalan < 0 THEN 0 ELSE ANTEXDEG.a_kalan END AS a_kalan,"
        SQ = SQ & " ANTEX.a_reftip AS a_reftip, ANTEX.a_refstokid AS a_refstokid, ANTEX.a_renkid AS a_renkid, ANTEX.a_renkadi AS a_renkadi, ANTEX.a_bedenid AS a_bedenid, ANTEX.a_beden AS a_beden, ANTEX.a_grup1 AS a_grup1,ANTEX.a_grup2 AS a_grup2, ANTEX.a_grup3 AS a_grup3, ANTEX.a_grup4 AS a_grup4,"
        SQ = SQ & " ANTEX.a_grup5 AS a_grup5, ANTEX.a_grup6 AS a_grup6, ANTEX.a_okod AS a_okod, ANTEX.a_aadi AS a_aadi, ANTEX.a_marka AS a_marka,"
        SQ = SQ & " ANTEX.a_model AS a_model, ANTEX.a_acik AS a_acik, ANTEX.a_bilgi1 AS a_bilgi1, ANTEX.a_bilgi2 AS a_bilgi2, ANTEX.a_bilgi3 AS a_bilgi3 "
        SQ = SQ & " FROM tbl_009_stok AS ANTEX "
        SQ = SQ & " LEFT JOIN tbl_009_stokdeg AS ANTEXDEG ON ANTEXDEG.a_id = ANTEX.a_id "
        SQ = SQ & " LEFT JOIN [AYHANLTD].[DBO].[tbl_003_stok] AS AYHANLTD on ANTEX.a_kod=AYHANLTD.a_kod and ANTEX.a_barkod=AYHANLTD.a_barkod "
        SQ = SQ & " LEFT JOIN [AYHANLTD].[DBO].[tbl_003_stokdeg] as AYHANDEG ON AYHANLTD.a_id=AYHANDEG.a_id "
        SQ = SQ & " WHERE ANTEX.a_webyay = 1 AND ANTEX.a_reftip = ('T') AND ANTEXDEG.a_ambid IN (13,24) AND ANTEX.a_kod LIKE '" & S_CODE & "%' OR  (AYHANLTD.a_webyay = 1 AND AYHANLTD.a_reftip = ('T') AND AYHANDEG.a_ambid = 21 AND AYHANLTD.a_kod LIKE '" & S_CODE & "%') "
        SQ = SQ & " GROUP BY ANTEX.a_id, ANTEX.a_adi, ANTEX.a_barkod, ANTEX.a_kod, ANTEX.a_sfiyat, ANTEX.a_ofiyat1, ANTEX.a_ofiyat2, ANTEX.a_ofiyat3,ANTEX.a_ofiyat4, ANTEX.a_ofiyat5, ANTEX.a_ofiyat6, ANTEX.a_dipfiyat, ANTEX.a_reftip, "
        SQ = SQ & " ANTEX.a_refstokid, ANTEX.a_renkid, ANTEX.a_renkadi, ANTEX.a_bedenid, ANTEX.a_beden, ANTEX.a_grup1, ANTEX.a_grup2, ANTEX.a_grup3, ANTEX.a_grup4, ANTEX.a_grup5, ANTEX.a_grup6, ANTEX.a_okod, ANTEX.a_aadi, ANTEX.a_marka, ANTEX.a_model,"
        SQ = SQ & " ANTEX.a_acik, ANTEX.a_bilgi1, ANTEX.a_bilgi2, ANTEX.a_bilgi3,  CASE WHEN ANTEXDEG.a_kalan < 0 THEN 0 ELSE ANTEXDEG.a_kalan END"
        SQ = SQ & " ) AS SORGU "
        SQ = SQ & " GROUP BY a_id, a_adi, a_kod, a_barkod, a_sfiyat, a_ofiyat1, a_ofiyat2, a_ofiyat3, a_ofiyat4, a_ofiyat5, a_ofiyat6, a_dipfiyat, a_reftip, a_refstokid, a_renkid, a_renkadi,"
        SQ = SQ & " a_bedenid, a_beden, a_grup1, a_grup2, a_grup3, a_grup4, a_grup5, a_grup6, a_okod, a_aadi, a_marka, a_model, a_acik, a_bilgi1, a_bilgi2, a_bilgi3 "
        SQ = SQ & " ORDER BY a_kod"

        'SQ = SQ.Replace("tbl_001_01_", KUL.tper)
        'SQ = SQ.Replace("tbl_001_", KUL.tfrm)
        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)

        Dim CHILD(DT.Rows.Count - 1) As WS_Stok

        For X = 0 To DT.Rows.Count - 1

            CHILD(X) = New WS_Stok

            CHILD(X).ID = NULN(DT, X, "a_id")
            CHILD(X).AD = NULA(DT, X, "a_adi")
            CHILD(X).KOD = NULA(DT, X, "a_kod")
            CHILD(X).BARKOD = NULA(DT, X, "a_barkod")
            CHILD(X).SFIYAT = NULD(DT, X, "a_sfiyat")
            CHILD(X).OFIYAT1 = NULD(DT, X, "a_ofiyat1")
            CHILD(X).OFIYAT2 = NULD(DT, X, "a_ofiyat2")
            CHILD(X).OFIYAT3 = NULD(DT, X, "a_ofiyat3")
            CHILD(X).OFIYAT4 = NULD(DT, X, "a_ofiyat4")
            CHILD(X).OFIYAT5 = NULD(DT, X, "a_ofiyat5")
            CHILD(X).OFIYAT6 = NULD(DT, X, "a_ofiyat6")
            CHILD(X).DIPFIYAT = NULD(DT, X, "a_dipfiyat")
            CHILD(X).KALAN = NULD(DT, X, "a_kalan")
            CHILD(X).REFID = NULN(DT, X, "a_refstokid")
            CHILD(X).REFTIP = NULA(DT, X, "a_reftip")
            CHILD(X).RENKID = NULN(DT, X, "a_renkid")
            CHILD(X).RENK = NULA(DT, X, "a_renkadi")
            CHILD(X).BEDENID = NULN(DT, X, "a_bedenid")
            CHILD(X).BEDEN = NULA(DT, X, "a_beden")
            CHILD(X).GRUP1 = NULA(DT, X, "a_grup1")
            CHILD(X).GRUP2 = NULA(DT, X, "a_grup2")
            CHILD(X).GRUP3 = NULA(DT, X, "a_grup3")
            CHILD(X).GRUP4 = NULA(DT, X, "a_grup4")
            CHILD(X).GRUP5 = NULA(DT, X, "a_grup5")
            CHILD(X).GRUP6 = NULA(DT, X, "a_grup6")
            CHILD(X).ACIKLAMA = NULA(DT, X, "a_acik")
            CHILD(X).BILGI1 = NULA(DT, X, "a_bilgi1")
            CHILD(X).BILGI2 = NULA(DT, X, "a_bilgi2")
            CHILD(X).BILGI3 = NULA(DT, X, "a_bilgi3")
            'CHILD(X).GRUP7 = NULA(DT, X, "a_grup7")

        Next

        Return CHILD
    End Function

    <WebMethod()>
    Public Function GetStockBarcode(S_BARCODE As String) As WS_Stok()

        Dim SQ As String
        Dim DT As New DataTable

        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()

        SQ = "SELECT a_id, a_adi, a_kod, a_barkod, a_sfiyat, a_ofiyat1, a_ofiyat2, a_ofiyat3, a_kalan, a_reftip, "
        SQ = SQ & "a_grup1, a_grup2, a_grup3, a_grup4, a_grup5, a_grup6, a_okod, a_aadi, a_marka, a_model, a_acik, a_bilgi1, a_bilgi2, a_bilgi3 "
        SQ = SQ & " FROM  tbl_001_stok "
        SQ = SQ & " WHERE a_barkod = '" + S_BARCODE + "' "
        SQ = SQ.Replace("tbl_001_01_", KUL.tper)
        SQ = SQ.Replace("tbl_001_", KUL.tfrm)

        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)

        Dim STK(DT.Rows.Count - 1) As WS_Stok


        For X = 0 To DT.Rows.Count - 1

            STK(X) = New WS_Stok
            STK(X).ID = NULN(DT, X, "a_id")
            STK(X).AD = NULA(DT, X, "a_adi")
            STK(X).KOD = NULA(DT, X, "a_kod")
            STK(X).BARKOD = NULA(DT, X, "a_barkod")
            STK(X).SFIYAT = NULD(DT, X, "a_sfiyat")
            STK(X).OFIYAT1 = NULD(DT, X, "a_ofiyat1")
            STK(X).OFIYAT2 = NULD(DT, X, "a_ofiyat2")
            STK(X).OFIYAT3 = NULD(DT, X, "a_ofiyat3")
            STK(X).KALAN = NULD(DT, X, "a_kalan")
            STK(X).REFTIP = NULA(DT, X, "a_reftip")
            STK(X).GRUP1 = NULA(DT, X, "a_grup1")
            STK(X).GRUP2 = NULA(DT, X, "a_grup2")
            STK(X).GRUP3 = NULA(DT, X, "a_grup3")
            STK(X).GRUP4 = NULA(DT, X, "a_grup4")
            STK(X).GRUP5 = NULA(DT, X, "a_grup5")
            STK(X).GRUP6 = NULA(DT, X, "a_grup6")
            STK(X).OKOD = NULA(DT, X, "a_okod")
            STK(X).AADI = NULA(DT, X, "a_aadi")
            STK(X).MARKA = NULA(DT, X, "a_marka")
            STK(X).MODEL = NULA(DT, X, "a_model")
            STK(X).ACIKLAMA = NULA(DT, X, "a_acik")
            STK(X).BILGI1 = NULA(DT, X, "a_bilgi1")
            STK(X).BILGI2 = NULA(DT, X, "a_bilgi2")
            STK(X).BILGI3 = NULA(DT, X, "a_bilgi3")

        Next

        Return STK


    End Function
#Region "ADS/Uyarı Mesajları"
    Public Enum NUMARATOR3
        BAŞARILI = 1
        MASAKAPATILMIS = 2
        HATA = 3
        TEKRARDENE = 4
        COKLUADISYON = 5



    End Enum
#End Region




#Region "ADS/ MASAYA ISLE"

    Public Class WS_MASAYAISLE
        Public DURUM As Long
        Public MID As Long
        Public MDURUM As String
        Public MADSBAS As String



    End Class

    <WebMethod>
    Public Function ADS_MASAYAISLE(ByVal PDA As String, ByVal SORGU As String, ByVal MASAID As String, ByVal ADISYONNO As String, ByVal ADISYONNOT As String, ByVal SUBEID As String, ByVal MEKANID As String) As WS_MASAYAISLE()
        Dim SQ As String
        Dim DT As New DataTable
        Dim DT2 As New DataTable
        Dim DT3 As New DataTable
        Dim DT4 As New DataTable
        Dim MAXSIRA As New Long

        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()

        SQ = ""
        SQ = "SELECT a_master_id"
        SQ = SQ & " FROM  " & KUL.tfrm & "men_adisyon "
        SQ = SQ & " WHERE a_master_id = '" + ADISYONNO + "'"

        DT = SQL_TABLO(SQ, "T", False, False, False)



        SORGU = SORGU.Replace("tbl_001_01_", KUL.tper)
        SORGU = SORGU.Replace("tbl_001_", KUL.tfrm)



        If DT.Rows.Count > 0 Or ADISYONNO = "0" Then


            SQ = ""
            SQ = SQ & "DELETE "
            SQ = SQ & " FROM  " & KUL.tfrm & "men_pdasiparis "
            SQ = SQ & " WHERE a_pda = '" + PDA + "'"

            SQL_KOS(SQ, False)

            SQ = ""
            SQ = SQ & "SELECT * FROM "
            SQ = SQ & "   " & KUL.tfrm & "men_pdasiparis "
            SQ = SQ & " WHERE a_pda = '" + PDA + "'"

            DT4 = SQL_TABLO(SQ, "T", False, False, False)

            If DT4.Rows.Count = 0 Then

                If SQL_KOS(SORGU, False) = True Then
                    Dim SONUC = SQL_WEB_Komut("MASAYAISLE MASA=" + MASAID + " PDA=" + PDA + " ADS_NO=" + ADISYONNO + " ADS_NOT=" + ADISYONNOT, "X")

                    If SONUC.ToLower = "true" Then

                        SQ = ""
                        SQ = "SELECT M.a_id as ID, M.a_durum as DURUM,isnull((M.a_adsbas),0) as ADSBAS"
                        SQ = SQ & " FROM  " & KUL.tfrm & "men_masa AS M"
                        SQ = SQ & " INNER JOIN " & KUL.tfrm & "sube As S ON M.a_sube = S.a_id"
                        SQ = SQ & " WHERE M.a_durum <> 'BIRLESIK'  AND S.a_id =" + SUBEID + "   ORDER BY M.a_kod"

                        DT2 = SQL_TABLO(SQ, "T", False, False, False)


                        Dim RESULT(DT2.Rows.Count - 1) As WS_MASAYAISLE



                        For X = 0 To DT2.Rows.Count - 1


                            RESULT(X) = New WS_MASAYAISLE
                            RESULT(X).DURUM = NUMARATOR3.BAŞARILI
                            RESULT(X).MID = NULN(DT2, X, "ID")
                            RESULT(X).MDURUM = NULA(DT2, X, "DURUM")
                            RESULT(X).MADSBAS = NULA(DT2, X, "ADSBAS")

                        Next


                        Return RESULT


                    Else
                        Dim RESULT(0) As WS_MASAYAISLE
                        RESULT(0) = New WS_MASAYAISLE
                        RESULT(0).DURUM = NUMARATOR3.HATA
                        RESULT(0).MID = 0
                        RESULT(0).MDURUM = " "
                        RESULT(0).MADSBAS = " "
                        Return RESULT
                    End If





                Else
                    Dim RESULT(0) As WS_MASAYAISLE
                    RESULT(0) = New WS_MASAYAISLE
                    RESULT(0).DURUM = NUMARATOR3.HATA
                    RESULT(0).MID = 0
                    RESULT(0).MDURUM = " "
                    RESULT(0).MADSBAS = " "
                    Return RESULT

                End If

            Else
                Dim RESULT(0) As WS_MASAYAISLE
                RESULT(0) = New WS_MASAYAISLE
                RESULT(0).DURUM = NUMARATOR3.TEKRARDENE
                RESULT(0).MID = 0
                RESULT(0).MDURUM = " "
                RESULT(0).MADSBAS = " "
                Return RESULT
            End If







        Else

            SQ = ""
            SQ = "SELECT M.a_id as ID, M.a_durum as DURUM,isnull((M.a_adsbas),0) as ADSBAS"
            SQ = SQ & " FROM  " & KUL.tfrm & "men_masa AS M"
            SQ = SQ & " INNER JOIN " & KUL.tfrm & "sube As S ON M.a_sube = S.a_id"
            SQ = SQ & " WHERE M.a_durum <> 'BIRLESIK'  AND S.a_id =" + SUBEID + "  ORDER BY M.a_kod"
            DT3 = SQL_TABLO(SQ, "T", False, False, False)



            Dim RESULT(DT3.Rows.Count - 1) As WS_MASAYAISLE

            For X = 0 To DT3.Rows.Count - 1


                RESULT(X) = New WS_MASAYAISLE
                RESULT(X).DURUM = NUMARATOR3.MASAKAPATILMIS
                RESULT(X).MID = NULN(DT3, X, "ID")
                RESULT(X).MDURUM = NULA(DT3, X, "DURUM")
                RESULT(X).MADSBAS = NULA(DT3, X, "ADSBAS")

            Next

            Return RESULT


        End If




    End Function




    <WebMethod> Public Function Grafik_Yazdir(ByVal ID As String, ByVal ADSNO As Long) As Boolean

        Dim SQ As String
        Dim DTURUN As New DataTable
        Dim DTISKONTO As New DataTable
        Dim ISK As Double
        Dim TUTTOP As Double
        Dim NETTUTAR As Double
        Dim DT As New DataTable
        Dim DM As New DataTable
        Dim X As Long
        Dim SIRA As Long



        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()



        SQ = ""
        SQ = "Select SUM (ADISYON.a_isktutar) AS ISK ,SUM(ADISYON.a_tut) AS TOPLAMTUTAR "
        SQ = SQ & " FROM " & KUL.tfrm & "men_adisyon As ADISYON "
        SQ = SQ & " WHERE ADISYON.a_master_id =  " & ADSNO & ""
        DTISKONTO = SQL_TABLO(SQ, "T", False, False, False)
        ISK = NULD(DTISKONTO, 0, "ISK")
        TUTTOP = NULD(DTISKONTO, 0, "TOPLAMTUTAR")
        NETTUTAR = TUTTOP - ISK



        SQ = ""
        SQ = "Select  ADISYON.a_master_id As AdsNo,STOK.a_adi As UrunAdi,ADISYON.a_mik As Miktar,ADISYON.a_adet As Adet,ADISYON.a_porsiyon As Porsiyon,STOK.a_birim As Birim,ADISYON.a_brf As BirimFiyat,ADISYON.a_not As AdsNot,ADISYON.a_tut As ToplamTutar FROM " & KUL.tfrm & "men_adisyon As ADISYON inner join " & KUL.tfrm & "stok  As STOK"
        SQ = SQ & " On ADISYON.a_sid=STOK.a_id "
        SQ = SQ & " where ADISYON.a_master_id =  " & ADSNO & ""
        DTURUN = SQL_TABLO(SQ, "T", False, False, False)

        SQ = "Select  Masa.a_adi As MasaAdi,MASA.a_kisisay As KisiSayisi,MASAYER.a_adi As Mekan FROM " & KUL.tfrm & "men_adisyon As ADISYON INNER JOIN "
        SQ = SQ & " " & KUL.tfrm & "men_masa As MASA On ADISYON.a_mid=MASA.a_id inner join "
        SQ = SQ & "" & KUL.tfrm & "men_masayer As MASAYER On MASA.a_yerid=MASAYER.a_id "
        SQ = SQ & "group by MASA.a_adi , Masa.a_kisisay, Masayer.a_adi, ADISYON.a_master_id "
        SQ = SQ & "having ADISYON.a_master_id = " & ADSNO & " "
        DT = SQL_TABLO(SQ, "T", False, False, False)

        SQ = ""
        SQ = "Select ADISYON.a_yer As Tur,Sube.a_adi As Sube,STOK.a_adi As UrunAdi,SUM(ADISYON.a_mik)As Miktar ,CARI.a_adi As Musteri,ADISYON.a_brf As BirimFiyat,STOK.a_birim As Birim,ADISYON.a_porsiyon As Porsiyon,sum(ADISYON.a_adet) As Adet,"
        SQ = SQ & " SUM(ADISYON.a_tut) As ToplamTutar, CARI.a_fadres1 As Adres1, CARI.a_fadres2 As Adres2,  CARI.a_fadres3 As Adres3, CARI.a_ftel As Tel, ADISYON.a_master_id As AdisyonNo, "
        SQ = SQ & "  per.a_adi As Personel, AdsTutar.a_tutar As AdsiyonTutar,  CARI.a_bakiye As Bakiye,CARI.a_bakiyetur As BakiyeTür,MAS.a_not As AdisyonNot,ADISYON.a_yildiz As Yazdýrma From " & KUL.tfrm & "men_adisyon As ADISYON INNER Join "
        SQ = SQ & " " & KUL.tfrm & "stok As Stok On ADISYON.a_sid = Stok.a_id INNER Join "
        SQ = SQ & " " & KUL.tfrm & "cari As CARI On ISNULL(ADISYON.a_cid, 1) = CARI.a_id  INNER Join "
        SQ = SQ & " " & KUL.tfrm & "sube As SUBE On SUBE.a_id=ADISYON.a_subeid inner join"
        SQ = SQ & " " & KUL.tfrm & "men_adisyonmas As MAS On ADISYON.a_master_id=MAS.a_id  inner join "
        SQ = SQ & " tbl_001_personel as per On ADISYON.a_personel = per.a_id INNER Join"
        SQ = SQ & " (Select a_mid, SUM(a_tut) As a_tutar FROM " & KUL.tfrm & "men_adisyon GROUP BY a_mid, a_master_id HAVING(a_master_id = " & ADSNO & ")) As AdsTutar On ADISYON.a_mid = AdsTutar.a_mid "
        SQ = SQ & " GROUP BY  STOK.a_birim,CARI.a_adi, CARI.a_fadres1,CARI.a_fadres2,CARI.a_fadres3,CARI.a_ftel, per.a_adi, AdsTutar.a_tutar,ADISYON.a_brf, ADISYON.a_master_id,CARI.a_bakiye ,CARI.a_bakiyetur,Sube.a_adi,MAS.a_not,ADISYON.a_yildiz,ADISYON.a_yer,STOK.a_adi,ADISYON.a_porsiyon   "
        SQ = SQ & " HAVING(ADISYON.a_master_id = " & ADSNO & ")  "
        DM = SQL_TABLO(SQ, "T", False, False, False)

        If DM.Rows.Count > 0 Then

            SQ = ""
            SQ = "INSERT INTO " & KUL.tfrm & "men_pservermas (a_id , a_adsno , a_adsnot , a_ttutar , a_sube , a_mekan , a_mutfak , a_masa , a_kisisay , a_personel , a_cad,  a_cadres , a_ctelno , a_cbakiye , a_siptip , a_odemetip,a_isktut,a_nettut )"
            SQ = SQ & " VALUES "
            SQ = SQ & " ( "
            SQ = SQ & " " & ID & ", "
            SQ = SQ & " " & ADSNO & ", "
            SQ = SQ & " N'" & NULA(DM, 0, "AdisyonNot") & "', "
            SQ = SQ & " " & NULD(DM, 0, "AdsiyonTutar") & " , "
            SQ = SQ & " N'" & NULA(DM, 0, "Sube") & "', "
            SQ = SQ & " N'" & NULA(DT, 0, "Mekan") & "', "
            SQ = SQ & " N'', "
            SQ = SQ & " N'" & NULA(DT, 0, "MasaAdi") & "', "
            SQ = SQ & " N'" & NULA(DT, 0, "KisiSay") & "', "
            SQ = SQ & " N'" & NULA(DM, 0, "Personel") & "', "
            SQ = SQ & " N'" & NULA(DM, 0, "Musteri") & "', "
            SQ = SQ & " N'" & NULA(DM, 0, "Adres1") & "', "
            SQ = SQ & " N'" & NULA(DM, 0, "Tel") & "', "
            SQ = SQ & " N'" & NULA(DM, 0, "Bakiye") & "', "
            SQ = SQ & " N'', "
            SQ = SQ & " N'', "
            SQ = SQ & " " & ISK & " ,"
            SQ = SQ & " " & NETTUTAR & " "
            SQ = SQ & " ) "
            Call SQL_KOS(SQ, False)
            SIRA = 1
            For X = 0 To DM.Rows.Count - 1

                SQ = ""
                SQ = "INSERT INTO " & KUL.tfrm & "men_pserverdet (a_id , a_sira , a_ad , a_birim , a_brmfiy , a_miktar , a_not , a_tutar,a_porsiyon,a_adet )"
                SQ = SQ & " VALUES "
                SQ = SQ & " ( "
                SQ = SQ & " " & ID & ", "
                SQ = SQ & " " & SIRA & ", "
                SQ = SQ & " N'" & NULA(DM, X, "UrunAdi") & "', "
                SQ = SQ & " N'" & NULA(DM, X, "Birim") & "' , "
                SQ = SQ & " " & NULD(DM, X, "BirimFiyat") & " , "
                SQ = SQ & " " & NULD(DM, X, "Miktar") & ", "
                SQ = SQ & " N'" & NULA(DM, X, "AdsNot") & "', "
                SQ = SQ & " " & NULD(DM, X, "ToplamTutar") & ", "
                SQ = SQ & " " & NULD(DM, X, "Porsiyon") & ","
                SQ = SQ & " " & NULN(DM, X, "Adet") & " "
                SQ = SQ & " ) "
                Call SQL_KOS(SQ, False)
                SIRA = SIRA + 1
            Next
            Return True
        Else
            Return False
        End If
    End Function

    <WebMethod>
    Public Function ADS_MUTFAKYAZDIR(ByVal SUBEID As Long, ByVal USERID As Long, ByVal CARIID As Long, ByVal MEKANID As Long, ByVal MASAID As Long, ByVal ADSNO As Long, ByVal KISISAY As Long, ByVal ADSNOT As String, ByVal SORGU As String, ByVal PDA As String) As WS_MASAYAISLE()

        Dim SQ As String
        Dim DM As New DataTable
        Dim DT As New DataTable
        Dim DT2 As New DataTable
        Dim DT3 As New DataTable
        Dim DT4 As New DataTable
        Dim DT5 As New DataTable
        Dim DT6 As New DataTable
        Dim DT7 As New DataTable
        Dim DTS As New DataTable
        Dim DTZ As New DataTable
        Dim DU As New DataTable
        Dim B As String = ""
        Dim KS As Long = 0
        Dim EB As Long = 0
        Dim ID_MUTFAK As Integer = 0
        Dim YILDIZ As String = ""
        Dim donusum As Boolean

        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()


        SORGU = SORGU.Replace("tbl_001_01_", KUL.tper)
        SORGU = SORGU.Replace("tbl_001_", KUL.tfrm)


        SQ = ""
        SQ = "SELECT a_master_id"
        SQ = SQ & " FROM  " & KUL.tfrm & "men_adisyon "
        SQ = SQ & " WHERE a_master_id = " & ADSNO

        DT5 = SQL_TABLO(SQ, "T", False, False, False)
        If DT5.Rows.Count > 0 Or ADSNO = "0" Then

            SQ = ""
            SQ = SQ & "DELETE "
            SQ = SQ & " FROM  " & KUL.tfrm & "men_pdasiparis "
            SQ = SQ & " WHERE a_pda = '" & PDA & "'"

            donusum = SQL_KOS(SQ, False)

            SQ = ""
            SQ = SQ & "SELECT * FROM "
            SQ = SQ & KUL.tfrm & "men_pdasiparis "
            SQ = SQ & " WHERE a_pda = '" & PDA & "'"

            DT4 = SQL_TABLO(SQ, "T", False, False, False)

            If DT4.Rows.Count = 0 Then

                If SQL_KOS(SORGU, False) = True Then

                    ' a_userid yerine personel kullanılmaktadır
                    SQ = ""
                    SQ = "SELECT a_pda, a_sira, a_mid, a_sid, a_adet, a_porsiyon, a_mik, a_brf, a_tut, a_not, a_tarih, a_saat, a_userid, a_yildiz,a_kisisay"
                    SQ = SQ & " FROM " & KUL.tfrm & "men_pdasiparis"
                    SQ = SQ & " WHERE "
                    SQ = SQ & " (a_pda    = '" & PDA & "') AND "
                    SQ = SQ & " (a_mid    = " & MASAID & ") AND "
                    SQ = SQ & " (a_master_id=" & ADSNO & ") AND"
                    SQ = SQ & " (a_yildiz = '*') "
                    'SQ = SQ & " ORDER BY a_sira"
                    SQ = SQ & " ORDER BY a_not DESC, a_sira DESC"
                    DU = SQL_TABLO(SQ, "T", False, False, False)
                    If DU.Rows.Count = 0 Then
                        Dim RESULT(0) As WS_MASAYAISLE
                        RESULT(0) = New WS_MASAYAISLE
                        RESULT(0).DURUM = NUMARATOR3.HATA
                        RESULT(0).MID = 0
                        RESULT(0).MDURUM = " "
                        RESULT(0).MADSBAS = 0
                        Return RESULT
                    End If



                    SQ = "SELECT a_id, a_veri1, a_veri2, a_veri3, a_veri4, a_yazici, a_esc1, a_esc2, a_zaman, a_bekle"
                    SQ = SQ & " FROM " & KUL.tfrm & "men_tanim "
                    SQ = SQ & " WHERE a_id = 1"
                    DT7 = SQL_TABLO(SQ, "T", False, False, False)
                    PRNADI = NULA(DT7, 0, "a_yazici")
                    PRNcm0 = NULN(DT7, 0, "a_esc1")
                    PRNcm1 = NULA(DT7, 0, "a_esc2")
                    PRNbek = NULN(DT7, 0, "a_bekle")




                    SQ = ""
                    SQ = "SELECT a_id, a_kod, a_adi, a_durum, a_adsnosu, a_sube, a_kisisay"
                    SQ = SQ & " FROM " & KUL.tfrm & "men_masa"
                    SQ = SQ & " WHERE(a_id = " & MASAID & ")"
                    DTS = SQL_TABLO(SQ, "VERI", False, False, False)




                    KS = 1


                    If ADSNO = 0 Then
                        Dim AF As New Adisyon_Fisleri
                        'AF.Kayit_ID_Nosu = 0
                        AF.Kayit_Tarihi = TTAR.TARIH
                        AF.Kayit_Saati = TTAR.SAAT
                        AF.Kayit_Masa_Id = NULN(DU, 0, "a_mid")
                        AF.Kayit_Sube_Id = NULN(DTS, 0, "a_sube")
                        AF.Kayit_Notu = ""
                        AF.Kayit_Cari_Id = 1
                        'AF.Kayit_Gun_Sira = 0
                        AF.Kayit_Notu = ADSNOT
                        AF.Gunluk_Sayac_Hesapla()
                        AF.Kayit_Fis_Nosu = ""
                        If NULN(DU, 0, "a_kisisay") <> 0 Then
                            AF.Kayit_Kisi_Sayisi = NULN(DU, 0, "a_kisisay")
                            KS = NULN(DU, 0, "a_kisisay")
                        Else
                            AF.Kayit_Kisi_Sayisi = 1
                            KS = 1
                        End If
                        AF.Kayit_Durum_Bilgisi = 0
                        AF.Kayit_Kullanici_Bilgisi = -NULN(DU, 0, "a_userid")
                        AF.Personel_Id = NULN(DU, 0, "a_userid")
                        ADSNO = AF.Ekle()
                    Else
                        ADSNO = ADSNO
                    End If


                    For Y = 0 To DU.Rows.Count - 1

                        If Y = 0 Then
                            SQ = ""
                            SQ = "SELECT a_id, a_durum, a_adsnosu "
                            SQ = SQ & " FROM " & KUL.tfrm & "men_masa"
                            SQ = SQ & " WHERE(a_id = " & NULN(DU, 0, "a_mid") & ")"
                            DT = SQL_TABLO(SQ, "VERI", False, False, False)

                            If NULA(DT, 0, "a_durum") <> "DOLU" Then
                                SQ = "Update " & KUL.tfrm & "men_masa"
                                SQ = SQ & " SET a_durum = 'DOLU'"
                                SQ = SQ & " , a_acsaat = '" & TTAR.TR2UK(TTAR.TARIH) & " " & Now.ToShortTimeString & "'"
                                SQ = SQ & " , a_adsnosu = " & Max_Bul(1) & ""
                                SQ = SQ & " , a_kisisay = " & KS & " "
                                SQ = SQ & " WHERE(a_id = " & NULN(DU, 0, "a_mid") & ")"
                                donusum = SQL_KOS(SQ, False)
                            End If

                        End If




                        SQ = ""
                        SQ = "SELECT  MAX(a_sira) AS EB"
                        SQ = SQ & " FROM " & KUL.tfrm & "men_adisyon"
                        SQ = SQ & " WHERE(a_mid = " & NULN(DU, 0, "a_mid") & ")"
                        DT = SQL_TABLO(SQ, "V", False, False, False)
                        EB = NULN(DT, 0, "EB") + 1

                        ID_MUTFAK = MUTFAK_BUL(NULN(DU, Y, "a_sid"), NULN(DTS, 0, "a_sube"), NULN(DU, 0, "a_mid"))

                        YILDIZ = "*"

                        'Mutfakta Yazdırılsın mı ? 
                        SQ = "SELECT a_mid, a_sid, a_noprn"
                        SQ = SQ & " FROM " & KUL.tfrm & "men_urunmutfak"
                        SQ = SQ & " WHERE (a_mid = " & ID_MUTFAK & ") AND (a_sid = " & NULN(DU, Y, "a_sid") & ")"
                        DTZ = SQL_TABLO(SQ, "T", False, False, False)
                        If NULN(DTZ, 0, "a_noprn") <> 0 Then
                            YILDIZ = ""
                        Else
                            YILDIZ = "*"
                        End If



                        SQ = ""
                        SQ = "INSERT INTO " & KUL.tfrm & "men_adisyon"
                        SQ = SQ & " (a_yildiz, a_mid, a_sira, a_cid, a_sid, a_adet, a_porsiyon, a_mik, a_brf, a_tut, a_tarih, a_saat, a_userid, a_not, "
                        SQ = SQ & " a_durum, a_yer, a_subeid, a_mutfakid, a_satdurum, a_iskoran, a_isktutar, a_master_id, a_personel)"
                        SQ = SQ & " VALUES ("
                        SQ = SQ & " '" & YILDIZ & "',"
                        SQ = SQ & " " & NULN(DU, 0, "a_mid") & ", "
                        SQ = SQ & " " & EB & ", "
                        SQ = SQ & " " & 1 & ", "
                        SQ = SQ & " " & NULN(DU, Y, "a_sid") & ", "
                        SQ = SQ & " " & S2D(NULD(DU, Y, "a_adet")) & ", "
                        SQ = SQ & " " & S2D(NULD(DU, Y, "a_porsiyon")) & ", "
                        SQ = SQ & " " & S2D(NULD(DU, Y, "a_mik")) & ", "
                        SQ = SQ & " " & S2D(NULD(DU, Y, "a_brf")) & ", "
                        SQ = SQ & " " & S2D(NULD(DU, Y, "a_tut")) & ", "
                        SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & "', "
                        SQ = SQ & " '" & Now.ToLongTimeString & "', "
                        SQ = SQ & " " & -NULN(DU, Y, "a_userid") & ","
                        SQ = SQ & " '" & NULA(DU, Y, "a_not") & "',"
                        SQ = SQ & " N'0-Bekleyen',"
                        SQ = SQ & " N'Masa Adisyon PDA',"
                        SQ = SQ & " " & NULN(DTS, 0, "a_sube") & ","
                        SQ = SQ & " " & ID_MUTFAK & ","
                        SQ = SQ & " " & 0 & " ," ' Satış Türü
                        SQ = SQ & " " & 0 & " ,"  ' iskoran
                        SQ = SQ & " " & 0 & " ,"   ' isktutar
                        SQ = SQ & " " & ADSNO & ", " ' Master ID
                        SQ = SQ & " " & NULN(DU, Y, "a_userid") & " " ' Master ID
                        SQ = SQ & " )"
                        donusum = SQL_KOS(SQ, False)

                    Next




                    CARIID = 1

                    SQ = ""
                    SQ = "SELECT     Adisyon.a_mutfakid, Yaz.a_yazici as a_yazyol"
                    SQ = SQ & " FROM  " & KUL.tfrm & "men_adisyon AS Adisyon INNER JOIN"
                    SQ = SQ & " " & KUL.tfrm & "men_mutfak AS Mutfak ON Adisyon.a_mutfakid = Mutfak.a_id INNER JOIN "
                    SQ = SQ & " " & KUL.tfrm & "men_mutfak_yazici as YAZ on YAZ.a_mid=Mutfak.a_id"
                    SQ = SQ & " WHERE     (Adisyon.a_mid = " & MASAID & ") AND (Adisyon.a_yildiz = '*') "
                    SQ = SQ & " GROUP BY Adisyon.a_mutfakid, Yaz.a_yazici"
                    DM = SQL_TABLO(SQ, "T", False, False, False)



                    For Z = 0 To DM.Rows.Count - 1

                        Dim K As Long
                        Dim DTID As New DataTable
                        SQ = ""
                        SQ &= "INSERT INTO ZMX_001_men_pserver (a_tur) "
                        SQ &= "VALUES(10) "
                        SQ &= "SELECT SCOPE_IDENTITY() as ads"
                        SQ = SQ.Replace("001", Mid(KUL.tfrm, 5, 3))

                        DTID = SQL_TABLO(B_SQL, SQ, "T", False, False, False)
                        K = NULN(DTID, 0, "ads") + 1

                        Grafik_MutfakYazdir(K, ADSNO, MASAID, CARIID, Z)



                        SQ = ""
                        SQ = "INSERT INTO " & KUL.tfrm & "men_pserver (a_id, a_tur, a_pda, a_user, a_tarih, a_saat, a_komut, a_veri, a_durum, a_sube, a_masa)"
                        SQ = SQ & " VALUES "
                        SQ = SQ & " ( "
                        SQ = SQ & " " & K & ", "
                        SQ = SQ & " '" & "MASA MUTFAK" & "', "
                        SQ = SQ & " '', "
                        SQ = SQ & " 0 , "
                        SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & "', "
                        SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & " " & TTAR.SAAT & "', "
                        SQ = SQ & " '" & NULA(DM, Z, "a_yazyol") & "', "
                        SQ = SQ & " '" & B & "', "
                        SQ = SQ & " '" & "H" & "', "
                        SQ = SQ & "  " & SUBEID & " , "
                        SQ = SQ & "  " & MASAID & " "
                        SQ = SQ & " ) "

                        donusum = SQL_KOS(SQ, False)
                    Next

                    SQ = ""
                    SQ = "UPDATE " & KUL.tfrm & "men_adisyon"
                    SQ = SQ & " SET "
                    SQ = SQ & " a_yildiz = ''"
                    SQ = SQ & " WHERE (a_mid = " & MASAID & ")"
                    If SQL_KOS(SQ, False) Then




                        SQ = ""
                        SQ = "SELECT M.a_id as ID, M.a_durum as DURUM,isnull((M.a_adsbas),0) as ADSBAS"
                        SQ = SQ & " FROM  " & KUL.tfrm & "men_masa AS M"
                        SQ = SQ & " INNER JOIN " & KUL.tfrm & "sube As S ON M.a_sube = S.a_id"
                        SQ = SQ & " WHERE M.a_durum <> 'BIRLESIK'  AND S.a_id =" & SUBEID & "  ORDER BY M.a_kod"

                        DT2 = SQL_TABLO(SQ, "T", False, False, False)


                        Dim RESULT(DT2.Rows.Count - 1) As WS_MASAYAISLE



                        For X = 0 To DT2.Rows.Count - 1


                            RESULT(X) = New WS_MASAYAISLE
                            RESULT(X).DURUM = NUMARATOR3.BAŞARILI
                            RESULT(X).MID = NULN(DT2, X, "ID")
                            RESULT(X).MDURUM = NULA(DT2, X, "DURUM")
                            RESULT(X).MADSBAS = NULA(DT2, X, "ADSBAS")

                        Next


                        Return RESULT



                    Else
                        Dim RESULT(0) As WS_MASAYAISLE
                        RESULT(0) = New WS_MASAYAISLE
                        RESULT(0).DURUM = NUMARATOR3.HATA
                        RESULT(0).MID = 0
                        RESULT(0).MDURUM = " "
                        RESULT(0).MADSBAS = 0
                        Return RESULT


                    End If






                Else




                    Dim RESULT(0) As WS_MASAYAISLE
                    RESULT(0) = New WS_MASAYAISLE
                    RESULT(0).DURUM = NUMARATOR3.HATA
                    RESULT(0).MID = 0
                    RESULT(0).MDURUM = " "
                    RESULT(0).MADSBAS = 0
                    Return RESULT

                End If



            Else
                Dim RESULT(0) As WS_MASAYAISLE
                RESULT(0) = New WS_MASAYAISLE
                RESULT(0).DURUM = NUMARATOR3.TEKRARDENE
                RESULT(0).MID = 0
                RESULT(0).MDURUM = " "
                RESULT(0).MADSBAS = 0
                Return RESULT
            End If


        Else

            SQ = ""
            SQ = "SELECT M.a_id as ID, M.a_durum as DURUM,isnull((M.a_adsbas),0) as ADSBAS"
            SQ = SQ & " FROM  " & KUL.tfrm & "men_masa AS M"
            SQ = SQ & " INNER JOIN " & KUL.tfrm & "sube As S ON M.a_sube = S.a_id"
            SQ = SQ & " WHERE M.a_durum <> 'BIRLESIK'  AND S.a_id =" & SUBEID & "   ORDER BY M.a_kod"
            DT3 = SQL_TABLO(SQ, "T", False, False, False)



            Dim RESULT(DT3.Rows.Count - 1) As WS_MASAYAISLE

            For X = 0 To DT3.Rows.Count - 1


                RESULT(X) = New WS_MASAYAISLE
                RESULT(X).DURUM = NUMARATOR3.MASAKAPATILMIS
                RESULT(X).MID = NULN(DT3, X, "ID")
                RESULT(X).MDURUM = NULA(DT3, X, "DURUM")
                RESULT(X).MADSBAS = NULA(DT3, X, "ADSBAS")

            Next

            Return RESULT

        End If





    End Function


    Public Class WS_IPTAL
        Public DURUM As Long
        Public MID As Long
        Public MDURUM As String
        Public MADSBAS As String


        Public MASAID As Long
        Public SIRA As Long
        Public STOKID As Long
        Public YILDIZ As String
        Public KOD As String
        Public ADI As String
        Public ADET As Long
        Public PORSIYON As Double
        Public MIKTAR As Double
        Public BIRIMFIYAT As Double
        Public TUTAR As Double
        Public TARIH As String
        Public SAAT As String
        Public USERID As Long
        Public ACIKLAMA As String
        Public USERAD As String
        Public ADSNO As Long
        Public KISISAY As Long




    End Class

    <WebMethod> Public Function ADS_IPTALYAZDIR(ByVal USERID As Long, ByVal IPTALNEDENID As Long, ByVal SUBEID As Long, ByVal ADSNO As Long, ByVal CARIID As Long, ByVal MASAID As Long, ByVal SIRA As Long, ByVal STOKID As Long, ByVal MIKTAR As Double, ByVal BIRIMFIYAT As Double, ByVal TUTAR As Double, ByVal ACIKLAMA As String, ByVal KALANMIKTAR As Double, ByVal PORSIYON As Double, ByVal ACK As String) As WS_IPTAL()


        Dim ISLEM As Boolean
        Dim SQ As String
        Dim DT As New DataTable

        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()

        ISLEM = Grafik_İptalYazdir(USERID, IPTALNEDENID, SUBEID, ADSNO, CARIID, MASAID, SIRA, STOKID, MIKTAR, BIRIMFIYAT, TUTAR, ACIKLAMA, KALANMIKTAR, PORSIYON, ACK)

        SQ = ""
        SQ = SQ & " SELECT M.a_mid as MASAID , M.a_sira AS SIRA , M.a_sid AS STOKID, M.a_yildiz AS YILDIZ,S.a_kod AS KOD ,s.a_adi AS ADI, M.a_adet AS ADET, M.a_porsiyon AS PORSIYON, M.a_mik AS MIKTAR,"
        SQ = SQ & " M.a_brf AS BIRIMFIYAT , M.a_tut AS TUTAR, M.a_tarih AS TARIH,  M.a_saat AS SAAT, M.a_userid AS USERID,  M.a_not AS ACIKLAMA, per.a_adi AS USERAD, M.a_master_id AS ADSNO ,A.a_kisi_sayisi AS KISISAY "
        SQ = SQ & " FROM " & KUL.tfrm & "men_adisyon As M "
        SQ = SQ & " INNER JOIN " & KUL.tfrm & "stok AS S ON M.a_sid = S.a_id  "
        SQ = SQ & " INNER JOIN tbl_001_personel as per ON M.a_personel = per.a_id  "
        SQ = SQ & " INNER JOIN " & KUL.tfrm & "men_adisyonmas As A ON M.a_master_id=A.a_id "
        SQ = SQ & " WHERE(M.a_mid=" & MASAID & ")  And (M.a_master_id=" & ADSNO & ") ORDER BY M.a_sira"

        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)


        If DT.Rows.Count > 0 Then

            Dim RESULT(DT.Rows.Count - 1) As WS_IPTAL




            For X = 0 To DT.Rows.Count - 1


                RESULT(X) = New WS_IPTAL
                RESULT(X).DURUM = NUMARATOR3.BAŞARILI
                RESULT(X).MID = 0
                RESULT(X).MDURUM = " "
                RESULT(X).MADSBAS = " "

                RESULT(X).MASAID = NULN(DT, X, "MASAID")
                RESULT(X).SIRA = NULN(DT, X, "SIRA")
                RESULT(X).STOKID = NULN(DT, X, "STOKID")
                RESULT(X).YILDIZ = NULA(DT, X, "YILDIZ")
                RESULT(X).KOD = NULA(DT, X, "KOD")
                RESULT(X).ADI = NULA(DT, X, "ADI")
                RESULT(X).ADET = NULN(DT, X, "ADET")
                RESULT(X).PORSIYON = NULD(DT, X, "PORSIYON")
                RESULT(X).MIKTAR = NULD(DT, X, "MIKTAR")
                RESULT(X).BIRIMFIYAT = NULD(DT, X, "BIRIMFIYAT")
                RESULT(X).TUTAR = NULD(DT, X, "TUTAR")
                RESULT(X).TARIH = NULA(DT, X, "TARIH")
                RESULT(X).SAAT = NULA(DT, X, "SAAT")
                RESULT(X).USERID = NULN(DT, X, "USERID")

                If NULA(DT, X, "ACIKLAMA") = "" Then
                    RESULT(X).ACIKLAMA = " "

                Else
                    RESULT(X).ACIKLAMA = NULA(DT, X, "ACIKLAMA")

                End If

                RESULT(X).USERAD = NULA(DT, X, "USERAD")
                RESULT(X).ADSNO = NULN(DT, X, "ADSNO")
                RESULT(X).KISISAY = NULN(DT, X, "KISISAY")





            Next


            Return RESULT

        Else

            SQ = ""
            SQ = "SELECT M.a_id as ID, M.a_durum as DURUM,isnull((M.a_adsbas),0) as ADSBAS"
            SQ = SQ & " FROM  " & KUL.tfrm & "men_masa AS M"
            SQ = SQ & " INNER JOIN " & KUL.tfrm & "sube As S ON M.a_sube = S.a_id"
            SQ = SQ & " WHERE M.a_durum <> 'BIRLESIK'  AND S.a_id =" & SUBEID & "  ORDER BY M.a_kod"

            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)



            Dim RESULT(DT.Rows.Count - 1) As WS_IPTAL



            For X = 0 To DT.Rows.Count - 1
                RESULT(X) = New WS_IPTAL
                RESULT(X).DURUM = NUMARATOR3.HATA
                RESULT(X).MID = NULN(DT, X, "ID")
                RESULT(X).MDURUM = NULA(DT, X, "DURUM")
                RESULT(X).MADSBAS = NULA(DT, X, "ADSBAS")

                RESULT(X).MASAID = 0
                RESULT(X).SIRA = 0
                RESULT(X).STOKID = 0
                RESULT(X).YILDIZ = " "
                RESULT(X).KOD = " "
                RESULT(X).ADI = " "
                RESULT(X).ADET = 0
                RESULT(X).PORSIYON = 0
                RESULT(X).MIKTAR = 0
                RESULT(X).BIRIMFIYAT = 0
                RESULT(X).TUTAR = 0
                RESULT(X).TARIH = " "
                RESULT(X).SAAT = " "
                RESULT(X).USERID = 0
                RESULT(X).ACIKLAMA = " "
                RESULT(X).USERAD = " "
                RESULT(X).ADSNO = 0
                RESULT(X).KISISAY = 0





            Next


            Return RESULT
        End If





    End Function
    <WebMethod> Public Function ADS_ADISYONYAZDIR(ByVal SUBEID As Long, ByVal MEKANID As Long, ByVal MASAID As Long, ByVal ADSNO As Long) As WS_MASAYAISLE()

        Dim SQ As String
        Dim YAZICIYOLU As String
        Dim B As String = ""
        Dim DT As New DataTable
        Dim DT2 As New DataTable
        Dim TDT As New DataTable

        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()

        SQ = ""
        SQ = "SELECT a_master_id"
        SQ = SQ & " FROM  " & KUL.tfrm & "men_adisyon "
        SQ = SQ & " WHERE a_master_id = " & ADSNO
        DT = SQL_TABLO(SQ, "T", False, False, False)

        If DT.Rows.Count > 0 Or ADSNO = "0" Then


            SQ = ""
            SQ = "SELECT     Men_MasaYer.a_yazyol"
            SQ = SQ & " " & "FROM  " & KUL.tfrm & "men_masa AS Men_Masa INNER JOIN"
            SQ = SQ & "  " & KUL.tfrm & "men_mekan_yazici AS YAZ ON YAZ.a_mekan_id=Men_masa.a_yerid INNER JOIN "
            SQ = SQ & " " & KUL.tfrm & "men_masayer AS Men_MasaYer ON Men_Masa.a_yerid = Men_MasaYer.a_id"
            SQ = SQ & " " & "WHERE     (Men_Masa.a_id = " & MASAID & ") "
            TDT = SQL_TABLO(SQ, "d", False, False, False)

            If NULA(TDT, 0, "a_yazyol") <> "" Then
                YAZICIYOLU = NULA(TDT, 0, "a_yazyol")

                SQ = ""
                SQ = "SELECT a_kasiyer"
                SQ = SQ & " FROM " & KUL.tfrm & "men_yetki_user WHERE a_userid = " & KUL.KOD & " "
                TDT = SQL_TABLO(SQ, "T", True, True, True)
                If NULA(TDT, 0, "a_kasiyer") = "Evet" Then GoTo y

            Else
y:
                SQ = ""
                SQ = "Select YAZ.a_yazici as a_yazyol "
                SQ = SQ & " FROM " & KUL.tfrm & "sube As sube "
                SQ = SQ & " INNER JOIN " & KUL.tfrm & "men_masa As masa On masa.a_sube=sube.a_id "
                SQ = SQ & "INNER JOIN " & KUL.tfrm & "men_sube_yazici as YAZ on Yaz.a_sube_id=sube.a_id "
                SQ = SQ & " " & "WHERE     (masa.a_id = " & MASAID & ") "
                TDT = SQL_TABLO(SQ, "subeyazici", False, False, False)
                If NULA(TDT, 0, "a_yazyol") <> "" Then
                    YAZICIYOLU = NULA(TDT, 0, "a_yazyol")
                Else
                    YAZICIYOLU = "YAZ.BAT"
                End If
            End If

            Dim K As Long
            Dim DTID As New DataTable
            SQ = ""
            SQ &= "INSERT INTO ZMX_001_men_pserver (a_tur) "
            SQ &= "VALUES(10) "
            SQ &= "SELECT SCOPE_IDENTITY() as ads"
            SQ = SQ.Replace("001", Mid(KUL.tfrm, 5, 3))

            DTID = SQL_TABLO(B_SQL, SQ, "T", False, False, False)

            K = NULN(DTID, 0, "ads") + 1

            If Grafik_Yazdir(K, ADSNO) = True Then
                SQ = ""
                SQ = "INSERT INTO " & KUL.tfrm & "men_pserver (a_id, a_tur, a_pda, a_user, a_tarih, a_saat, a_komut, a_veri, a_durum, a_sube, a_masa)"
                SQ = SQ & " VALUES "
                SQ = SQ & " ( "
                SQ = SQ & " " & K & ", "
                SQ = SQ & " '" & "MASA ADISYON" & "', "
                SQ = SQ & " '', "
                SQ = SQ & " 0 , "
                SQ = SQ & " N'" & TTAR.TR2UK(TTAR.TARIH) & "', "
                SQ = SQ & " N'" & TTAR.TR2UK(TTAR.TARIH) & " " & TTAR.SAAT & "', "
                SQ = SQ & " N'" & YAZICIYOLU & "', "
                SQ = SQ & " N'" & B & "', "
                SQ = SQ & " N'" & "H" & "', "
                SQ = SQ & "  " & SUBEID & " , "
                SQ = SQ & "  " & MASAID & " "
                SQ = SQ & " ) "

                Call SQL_KOS(SQ, False)
                SQ = ""
                SQ = "UPDATE " & KUL.tfrm & "men_masa"
                SQ = SQ & " SET "
                SQ = SQ & " a_adsbas = 1"
                SQ = SQ & " WHERE(a_id = " & MASAID & ")"
                Call SQL_KOS(SQ, False)
                SQ = ""
                SQ = "UPDATE " & KUL.tfrm & "men_adisyon"
                SQ = SQ & " SET "
                SQ = SQ & " a_yildiz = ''"
                SQ = SQ & " WHERE (a_master_id = " & ADSNO & ")"

                If SQL_KOS(SQ, False) Then




                    SQ = ""
                    SQ = "SELECT M.a_id as ID, M.a_durum as DURUM,isnull((M.a_adsbas),0) as ADSBAS"
                    SQ = SQ & " FROM  " & KUL.tfrm & "men_masa AS M"
                    SQ = SQ & " INNER JOIN " & KUL.tfrm & "sube As S ON M.a_sube = S.a_id"
                    SQ = SQ & " WHERE M.a_durum <> 'BIRLESIK'  AND S.a_id =" & SUBEID & "  ORDER BY M.a_kod"

                    DT2 = SQL_TABLO(SQ, "T", False, False, False)


                    Dim RESULT(DT2.Rows.Count - 1) As WS_MASAYAISLE



                    For X = 0 To DT2.Rows.Count - 1


                        RESULT(X) = New WS_MASAYAISLE
                        RESULT(X).DURUM = NUMARATOR3.BAŞARILI
                        RESULT(X).MID = NULN(DT2, X, "ID")
                        RESULT(X).MDURUM = NULA(DT2, X, "DURUM")
                        RESULT(X).MADSBAS = NULA(DT2, X, "ADSBAS")

                    Next


                    Return RESULT



                Else
                    Dim RESULT(0) As WS_MASAYAISLE
                    RESULT(0) = New WS_MASAYAISLE
                    RESULT(0).DURUM = NUMARATOR3.HATA
                    RESULT(0).MID = 0
                    RESULT(0).MDURUM = " "
                    RESULT(0).MADSBAS = 0
                    Return RESULT


                End If
            Else
                Dim RESULT(0) As WS_MASAYAISLE
                RESULT(0) = New WS_MASAYAISLE
                RESULT(0).DURUM = NUMARATOR3.HATA
                RESULT(0).MID = 0
                RESULT(0).MDURUM = " "
                RESULT(0).MADSBAS = 0
                Return RESULT
            End If




        Else

            SQ = ""
            SQ = "SELECT M.a_id as ID, M.a_durum as DURUM,isnull((M.a_adsbas),0) as ADSBAS"
            SQ = SQ & " FROM  " & KUL.tfrm & "men_masa AS M"
            SQ = SQ & " INNER JOIN " & KUL.tfrm & "sube As S ON M.a_sube = S.a_id"
            SQ = SQ & " WHERE M.a_durum <> 'BIRLESIK'  AND S.a_id =" & SUBEID & "  ORDER BY M.a_kod"
            DT2 = SQL_TABLO(SQ, "T", False, False, False)



            Dim RESULT(DT2.Rows.Count - 1) As WS_MASAYAISLE

            For X = 0 To DT2.Rows.Count - 1


                RESULT(X) = New WS_MASAYAISLE
                RESULT(X).DURUM = NUMARATOR3.MASAKAPATILMIS
                RESULT(X).MID = NULN(DT2, X, "ID")
                RESULT(X).MDURUM = NULA(DT2, X, "DURUM")
                RESULT(X).MADSBAS = NULA(DT2, X, "ADSBAS")

            Next

            Return RESULT

        End If


    End Function

    Public Class WS_MASAKONTROL
        Public DURUM As Long
        Public MID As Long
        Public MDURUM As String
        Public MADSBAS As String
        Public ADSNO As Long
        Public ADSNOT As String

        Public MASAADI As String
        Public SIRA As Long
        Public ADET As Double
        Public PORS As Double
        Public MIKTAR As Double
        Public TUTAR As Double
        Public STOKADI As String
        Public URUNNOT As String


        Public STOKID As Long
        Public BRMFYT As Double
        Public TARIH As String
        Public SAAT As String
        Public USERID As Long
        Public ISKORAN As Double
        Public ISTUTAR As Double
        Public SATDURUM As Double
        Public MUTFAKID As Long
        Public SUBEID As Long
        Public YER As String
        Public MENUID As Long





    End Class

    <WebMethod> Public Function ADS_MASAKONTROL(ByVal SUBEID As Long, ByVal MEKANID As Long, ByVal MASAID As Long) As WS_MASAKONTROL()
        Dim SQ As String
        Dim DT As New DataTable
        Dim DT2 As New DataTable
        Dim DT3 As New DataTable

        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()

        SQ = ""
        SQ = SQ & " SELECT a_durum"
        SQ = SQ & " FROM  " & KUL.tfrm & "men_masa "
        SQ = SQ & " WHERE a_id = '" & MASAID & "'"

        DT = SQL_TABLO(SQ, "T", False, False, False)


        If NULA(DT, 0, "a_durum").ToLower = "dolu" Then

            SQ = ""
            SQ = SQ & " SELECT  a_id AS ADSNO , a_mid AS MID , a_not AS ADSNOT ,a_durum AS DURUM"
            SQ = SQ & " FROM " & KUL.tfrm & "men_adisyonmas "
            SQ = SQ & "  WHERE a_mid =" & MASAID & " AND a_durum=0"


            DT2 = SQL_TABLO(SQ, "T", False, False, False)

            If DT2.Rows.Count = 0 Then

                SQ = ""
                SQ = "SELECT M.a_id as ID, M.a_durum as DURUM,isnull((M.a_adsbas),0) as ADSBAS"
                SQ = SQ & " FROM  " & KUL.tfrm & "men_masa AS M"
                SQ = SQ & " INNER JOIN " & KUL.tfrm & "sube As S ON M.a_sube = S.a_id"
                SQ = SQ & " WHERE M.a_durum <> 'BIRLESIK'  AND S.a_id =" & SUBEID & "  ORDER BY M.a_kod"

                DT2 = SQL_TABLO(SQ, "T", False, False, False)



                Dim RESULT(DT2.Rows.Count - 1) As WS_MASAKONTROL



                For X = 0 To DT2.Rows.Count - 1


                    RESULT(X) = New WS_MASAKONTROL
                    RESULT(X).DURUM = NUMARATOR3.MASAKAPATILMIS
                    RESULT(X).MID = NULN(DT2, X, "ID")
                    RESULT(X).MDURUM = NULA(DT2, X, "DURUM")
                    RESULT(X).MADSBAS = NULA(DT2, X, "ADSBAS")
                    RESULT(X).ADSNO = 0
                    RESULT(X).ADSNOT = " "


                Next


                Return RESULT
            ElseIf DT2.Rows.Count = 1 Then

                SQ = ""
                SQ = SQ & " SELECT ADS.a_master_id AS ADSNO,ADS.a_mid AS MID, ADS.a_sira AS SIRA, ADS.a_adet AS ADET, ADS.a_porsiyon AS PORS, ADS.a_mik AS MIKTAR, ADS.a_tut AS TUTAR ,S.a_adi AS STOKADI,ADS.a_not AS URUNNOT,MASA.a_adi AS MASAADI,ADS.a_sid AS STOKID,ADS.a_brf AS BRMFYT,ADS.a_tarih AS TARIH,ADS.a_saat AS SAAT, ADS.a_userid AS USERID, ADS.a_iskoran AS ISKORAN, ADS.a_isktutar AS ISTUTAR, ADS.a_satdurum AS SATDURUM,ADS.a_mutfakid AS MUTFAKID, ADS.a_subeid AS SUBEID, ADS.a_yer AS YER, ADS.a_menuid AS MENUID"
                SQ = SQ & " FROM " & KUL.tfrm & "men_adisyon AS ADS "
                SQ = SQ & " INNER JOIN " & KUL.tfrm & "stok AS S ON ADS.a_sid =S.a_id  "
                SQ = SQ & " INNER JOIN " & KUL.tfrm & "men_masa AS MASA ON MASA.a_id=ADS.a_mid"
                SQ = SQ & " WHERE ADS.a_mid=" & MASAID & "  AND ADS.a_master_id=" & NULN(DT2, 0, "ADSNO") & ""

                DT3 = SQL_TABLO(SQ, "T", False, False, False)

                Dim RESULT(DT3.Rows.Count - 1) As WS_MASAKONTROL



                For X = 0 To DT3.Rows.Count - 1

                    Dim oTime As DateTime = Convert.ToDateTime(NULA(DT3, X, "SAAT"))



                    RESULT(X) = New WS_MASAKONTROL
                    RESULT(X).DURUM = NUMARATOR3.BAŞARILI
                    RESULT(X).ADSNO = NULN(DT3, X, "ADSNO")
                    RESULT(X).MID = NULN(DT3, X, "MID")
                    RESULT(X).SIRA = NULN(DT3, X, "SIRA")
                    RESULT(X).ADET = NULD(DT3, X, "ADET")
                    RESULT(X).PORS = NULD(DT3, X, "PORS")
                    RESULT(X).MIKTAR = NULD(DT3, X, "MIKTAR")
                    RESULT(X).TUTAR = NULD(DT3, X, "TUTAR")
                    RESULT(X).STOKADI = NULA(DT3, X, "STOKADI")


                    RESULT(X).STOKID = NULN(DT3, X, "STOKID")
                    RESULT(X).BRMFYT = NULD(DT3, X, "BRMFYT")
                    RESULT(X).TARIH = TTAR.TR2UK(NULA(DT3, X, "TARIH"))
                    RESULT(X).SAAT = oTime
                    RESULT(X).USERID = NULN(DT3, X, "USERID")
                    RESULT(X).ISKORAN = NULD(DT3, X, "ISKORAN")
                    RESULT(X).ISTUTAR = NULD(DT3, X, "ISTUTAR")
                    RESULT(X).SATDURUM = NULN(DT3, X, "SATDURUM")
                    RESULT(X).MUTFAKID = NULN(DT3, X, "MUTFAKID")
                    RESULT(X).SUBEID = NULN(DT3, X, "SUBEID")
                    RESULT(X).YER = NULA(DT3, X, "YER")
                    RESULT(X).MENUID = NULN(DT3, X, "MENUID")


                    If NULA(DT3, X, "URUNNOT") = "" Then
                        RESULT(X).URUNNOT = " "
                    Else

                        RESULT(X).URUNNOT = NULA(DT3, X, "URUNNOT")

                    End If
                    RESULT(X).MASAADI = NULA(DT3, X, "MASAADI")

                Next


                Return RESULT




            Else



                Dim RESULT(DT2.Rows.Count - 1) As WS_MASAKONTROL

                For X = 0 To DT2.Rows.Count - 1

                    RESULT(X) = New WS_MASAKONTROL

                    RESULT(X).DURUM = NUMARATOR3.COKLUADISYON

                    RESULT(X).MID = NULN(DT2, X, "MID")
                    RESULT(X).MDURUM = "DOLU"
                    RESULT(X).MADSBAS = 0
                    RESULT(X).ADSNO = NULN(DT2, X, "ADSNO")

                    If NULA(DT2, X, "ADSNOT") = "" Then
                        RESULT(X).ADSNOT = " "

                    Else

                        RESULT(X).ADSNOT = NULA(DT2, X, "ADSNOT")


                    End If
                Next

                Return RESULT





            End If



        Else
            SQ = ""
            SQ = "SELECT M.a_id as ID, M.a_durum as DURUM,isnull((M.a_adsbas),0) as ADSBAS"
            SQ = SQ & " FROM  " & KUL.tfrm & "men_masa AS M"
            SQ = SQ & " INNER JOIN " & KUL.tfrm & "sube As S ON M.a_sube = S.a_id"
            SQ = SQ & " WHERE M.a_durum <> 'BIRLESIK'  AND S.a_id =" & SUBEID & "   ORDER BY M.a_kod"

            DT2 = SQL_TABLO(SQ, "T", False, False, False)



            Dim RESULT(DT2.Rows.Count - 1) As WS_MASAKONTROL



            For X = 0 To DT2.Rows.Count - 1


                RESULT(X) = New WS_MASAKONTROL
                RESULT(X).DURUM = NUMARATOR3.MASAKAPATILMIS
                RESULT(X).MID = NULN(DT2, X, "ID")
                RESULT(X).MDURUM = NULA(DT2, X, "DURUM")
                RESULT(X).MADSBAS = NULA(DT2, X, "ADSBAS")
                RESULT(X).ADSNO = 0
                RESULT(X).ADSNOT = " "


            Next


            Return RESULT

        End If






    End Function

    Public Class WS_MASATASI

        Public DURUM As Long
        Public YENIADSNO As Long


        Public MDURUM As String
        Public MADSBAS As Long
        Public MID As Long



    End Class

    <WebMethod> Public Function ADS_MASATASI(ByVal SUBEID As Long, ByVal ESKIMEKANID As Long, ByVal ESKIMASAID As Long, ByVal YENIMEKANID As Long, ByVal YENIMASAID As Long, ByVal ADSNO As Long, ByVal ISLEM As Long, ByVal ESKISORGU As String, ByVal YENISORGU As String, ByVal ADSNOT As String, ByVal KISISAYISI As Long, ByVal USERID As Long) As WS_MASATASI()



        Dim SQ As String
        Dim DT As New DataTable
        Dim DT2 As New DataTable
        Dim ESKIADISYON As New DataTable
        Dim YENIADSNO As Long

        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()

        ' ISLEM =0 TÜM ÜRÜNLERİ TAŞIMA
        ' ISLEM =1 ÜRÜNLERİN BİR KISMINI TAŞIMA



        SQ = ""
        SQ = "SELECT a_durum"
        SQ = SQ & " FROM " & KUL.tfrm & "men_masa"
        SQ = SQ & " WHERE a_id=" & YENIMASAID
        DT = SQL_TABLO(SQ, "T", False, False, False)

        If NULA(DT, 0, "a_durum") = "DOLU" Then


            SQ = ""
            SQ = "SELECT M.a_id as ID, M.a_durum as DURUM,isnull((M.a_adsbas),0) as ADSBAS"
            SQ = SQ & " FROM  " & KUL.tfrm & "men_masa AS M"
            SQ = SQ & " INNER JOIN " & KUL.tfrm & "sube As S ON M.a_sube = S.a_id"
            SQ = SQ & " WHERE M.a_durum <> 'BIRLESIK'  AND S.a_id =" & SUBEID & "   ORDER BY M.a_kod"

            DT = SQL_TABLO(SQ, "T", False, False, False)

            Dim RESULT(DT.Rows.Count - 1) As WS_MASATASI





            For X = 0 To DT.Rows.Count - 1


                RESULT(X) = New WS_MASATASI
                RESULT(X).DURUM = NUMARATOR3.MASAKAPATILMIS
                RESULT(X).MID = NULN(DT, X, "ID")
                RESULT(X).MDURUM = NULA(DT, X, "DURUM")
                RESULT(X).MADSBAS = NULN(DT, X, "ADSBAS")



            Next


            Return RESULT


        Else


            SQ = ""
            SQ = "SELECT a_durum"
            SQ = SQ & " FROM " & KUL.tfrm & "men_masa"
            SQ = SQ & " WHERE a_id=" & ESKIMASAID
            DT = SQL_TABLO(SQ, "T", False, False, False)

            If NULA(DT, 0, "a_durum") = "DOLU" Then


                If ISLEM = 0 Then

                    MASATASI(ESKIMASAID, YENIMASAID, ADSNO)



                    SQ = ""
                    SQ = "SELECT M.a_id as ID, M.a_durum as DURUM,isnull((M.a_adsbas),0) as ADSBAS"
                    SQ = SQ & " FROM  " & KUL.tfrm & "men_masa AS M"
                    SQ = SQ & " INNER JOIN " & KUL.tfrm & "sube As S ON M.a_sube = S.a_id"
                    SQ = SQ & " WHERE M.a_durum <> 'BIRLESIK'  AND S.a_id =" & SUBEID & "   ORDER BY M.a_kod"

                    DT = SQL_TABLO(SQ, "T", False, False, False)

                    Dim RESULT(DT.Rows.Count - 1) As WS_MASATASI



                    For X = 0 To DT.Rows.Count - 1


                        RESULT(X) = New WS_MASATASI
                        RESULT(X).DURUM = NUMARATOR3.BAŞARILI
                        RESULT(X).MID = NULN(DT, X, "ID")
                        RESULT(X).MDURUM = NULA(DT, X, "DURUM")
                        RESULT(X).MADSBAS = NULN(DT, X, "ADSBAS")



                    Next


                    Return RESULT

                Else

                    SQ = ""
                    SQ = SQ & " SELECT ADS.a_master_id AS ADSNO,ADS.a_mid AS MID, ADS.a_sira AS SIRA, ADS.a_adet AS ADET, ADS.a_porsiyon AS PORS, ADS.a_mik AS MIKTAR, ADS.a_tut AS TUTAR ,S.a_adi AS STOKADI,ADS.a_not AS URUNNOT,MASA.a_adi AS MASAADI,ADS.a_sid AS STOKID,ADS.a_brf AS BRMFYT,ADS.a_tarih AS TARIH,ADS.a_saat AS SAAT, ADS.a_userid AS USERID, ADS.a_iskoran AS ISKORAN, ADS.a_isktutar AS ISTUTAR, ADS.a_satdurum AS SATDURUM,ADS.a_mutfakid AS MUTFAKID, ADS.a_subeid AS SUBEID, ADS.a_yer AS YER, ADS.a_menuid AS MENUID"
                    SQ = SQ & " FROM " & KUL.tfrm & "men_adisyon AS ADS "
                    SQ = SQ & " INNER JOIN " & KUL.tfrm & "stok AS S ON ADS.a_sid =S.a_id  "
                    SQ = SQ & " INNER JOIN " & KUL.tfrm & "men_masa AS MASA ON MASA.a_id=ADS.a_mid"
                    SQ = SQ & " WHERE ADS.a_mid=" & ESKIMASAID & "  AND ADS.a_master_id=" & ADSNO & ""

                    ESKIADISYON = SQL_TABLO(SQ, "T", False, False, False)


                    SQ = ""
                    SQ = SQ & " DELETE FROM " & KUL.tfrm & "men_adisyon"
                    SQ = SQ & " WHERE a_mid= " & ESKIMASAID & " AND a_master_id= " & ADSNO & ""




                    If SQL_KOS(SQ, False) Then


                        If SQL_KOS(ESKISORGU, False) Then


                            Dim AF As New Adisyon_Fisleri
                            'AF.Kayit_ID_Nosu = 0
                            AF.Kayit_Tarihi = Date.Now.ToShortDateString
                            AF.Kayit_Saati = Date.Now.ToShortTimeString
                            AF.Kayit_Masa_Id = YENIMASAID
                            AF.Kayit_Sube_Id = SUBEID
                            AF.Kayit_Notu = ADSNOT
                            AF.Kayit_Cari_Id = 1
                            'AF.Kayit_Gun_Sira = 0
                            AF.Gunluk_Sayac_Hesapla()
                            AF.Kayit_Fis_Nosu = ""
                            AF.Kayit_Kisi_Sayisi = KISISAYISI
                            AF.Kayit_Durum_Bilgisi = 0
                            AF.Kayit_Kullanici_Bilgisi = -USERID
                            AF.Personel_Id = USERID
                            YENIADSNO = AF.Ekle()

                            YENISORGU = YENISORGU.Replace("a_masterid", YENIADSNO)

                            If SQL_KOS(YENISORGU, False) Then


                                SQ = "Update " & KUL.tfrm & "men_masa"
                                SQ = SQ & " SET a_durum =N'" & XL_DOLU & "'"
                                SQ = SQ & "  , a_acsaat = '" & TTAR.TR2UK(TTAR.TARIH) & " " & Now.ToShortTimeString & "'"
                                SQ = SQ & "  , a_kisisay = " & KISISAYISI & " "
                                SQ = SQ & "  , a_adsnosu = " & YENIADSNO & ""
                                SQ = SQ & "  , a_cariid = " & 1 & " "
                                SQ = SQ & " WHERE(a_id = " & YENIMASAID & ")"
                                Call SQL_KOS(SQ, False)



                                SQ = ""
                                SQ = "SELECT M.a_id as ID, M.a_durum as DURUM,isnull((M.a_adsbas),0) as ADSBAS"
                                SQ = SQ & " FROM  " & KUL.tfrm & "men_masa AS M"
                                SQ = SQ & " INNER JOIN " & KUL.tfrm & "sube As S ON M.a_sube = S.a_id"
                                SQ = SQ & " WHERE M.a_durum <> 'BIRLESIK'  AND S.a_id =" & SUBEID & "   ORDER BY M.a_kod"

                                DT = SQL_TABLO(SQ, "T", False, False, False)

                                Dim RESULT(DT.Rows.Count - 1) As WS_MASATASI



                                For X = 0 To DT.Rows.Count - 1


                                    RESULT(X) = New WS_MASATASI
                                    RESULT(X).DURUM = NUMARATOR3.BAŞARILI
                                    RESULT(X).MID = NULN(DT, X, "ID")
                                    RESULT(X).MDURUM = NULA(DT, X, "DURUM")
                                    RESULT(X).MADSBAS = NULN(DT, X, "ADSBAS")



                                Next


                                Return RESULT





                            Else

                                SQ = ""
                                SQ = " DELETE FROM " & KUL.tfrm & "men_adisyonmas "
                                SQ = SQ & " WHERE a_id = " & YENIADSNO & " AND a_mid= " & YENIMASAID & " "
                                Call SQL_KOS(SQ, False)

                                SQ = ""
                                SQ = " DELETE FROM " & KUL.tfrm & "men_adisyon "
                                SQ = SQ & " WHERE a_master_id = " & ADSNO & " AND a_mid= " & ESKIMASAID & " "
                                Call SQL_KOS(SQ, False)



                                SQ = ""
                                SQ = SQ & " INSERT INTO " & KUL.tfrm & "men_adisyon "
                                SQ = SQ & " (a_mid, a_sira, a_sid, a_mik, a_brf, a_tut, a_tarih, a_saat, a_userid, a_not, a_iskoran, a_isktutar, a_satdurum,a_master_id,"
                                SQ = SQ & " a_mutfakid, a_subeid, a_yer,a_adet,a_porsiyon, a_menuid, a_personel)"
                                SQ = SQ & " VALUES "

                                For X = 0 To ESKIADISYON.Rows.Count - 1
                                    SQ = SQ & "("
                                    SQ = SQ & NULN(ESKIADISYON, X, "MID") & ","
                                    SQ = SQ & NULN(ESKIADISYON, X, "SIRA") & ","
                                    SQ = SQ & NULN(ESKIADISYON, X, "STOKID") & ","
                                    SQ = SQ & NULD(ESKIADISYON, X, "MIKTAR") & ","
                                    SQ = SQ & NULD(ESKIADISYON, X, "BRMFYT") & ","
                                    SQ = SQ & NULD(ESKIADISYON, X, "TUTAR") & ","
                                    SQ = SQ & "'" & TR2UK(NULA(ESKIADISYON, X, "TARIH")) & "',"
                                    SQ = SQ & "'" & NULA(ESKIADISYON, X, "SAAT") & "',"
                                    SQ = SQ & -NULN(ESKIADISYON, X, "USERID") & ","
                                    SQ = SQ & "'" & NULA(ESKIADISYON, X, "URUNNOT") & "',"
                                    SQ = SQ & NULD(ESKIADISYON, X, "ISKORAN") & ","
                                    SQ = SQ & NULD(ESKIADISYON, X, "ISTUTAR") & ","
                                    SQ = SQ & NULN(ESKIADISYON, X, "SATDURUM") & ","
                                    SQ = SQ & NULN(ESKIADISYON, X, "ADSNO") & ","
                                    SQ = SQ & NULN(ESKIADISYON, X, "MUTFAKID") & ","
                                    SQ = SQ & NULN(ESKIADISYON, X, "SUBEID") & ","
                                    SQ = SQ & "'" & NULA(ESKIADISYON, X, "YER") & "',"
                                    SQ = SQ & NULD(ESKIADISYON, X, "ADET") & ","
                                    SQ = SQ & NULD(ESKIADISYON, X, "PORS") & ","
                                    SQ = SQ & NULN(ESKIADISYON, X, "MENUID") & ","
                                    SQ = SQ & NULN(ESKIADISYON, X, "USERID") & " "
                                    SQ = SQ & ")"

                                    If X <> ESKIADISYON.Rows.Count - 1 Then
                                        SQ = SQ & " ,"
                                    End If



                                Next

                                Call SQL_KOS(SQ, False)



                                Dim VERI(0) As WS_MASATASI
                                VERI(0) = New WS_MASATASI
                                VERI(0).DURUM = NUMARATOR3.HATA
                                VERI(0).YENIADSNO = 0


                                Return VERI




                            End If



                        Else

                            SQ = ""

                            SQ = SQ & " INSERT INTO " & KUL.tfrm & "men_adisyon "
                            SQ = SQ & " (a_mid, a_sira, a_sid, a_mik, a_brf, a_tut, a_tarih, a_saat, a_userid, a_not, a_iskoran, a_isktutar, a_satdurum,a_master_id,"
                            SQ = SQ & " a_mutfakid, a_subeid, a_yer,a_adet,a_porsiyon, a_menuid,a_personel)"
                            SQ = SQ & " VALUES "

                            For X = 0 To ESKIADISYON.Rows.Count - 1
                                SQ = SQ & "("
                                SQ = SQ & NULN(ESKIADISYON, X, "MID") & ","
                                SQ = SQ & NULN(ESKIADISYON, X, "SIRA") & ","
                                SQ = SQ & NULN(ESKIADISYON, X, "STOKID") & ","
                                SQ = SQ & NULD(ESKIADISYON, X, "MIKTAR") & ","
                                SQ = SQ & NULD(ESKIADISYON, X, "BRMFYT") & ","
                                SQ = SQ & NULD(ESKIADISYON, X, "TUTAR") & ","
                                SQ = SQ & "'" & NULA(ESKIADISYON, X, "TARIH") & "',"
                                SQ = SQ & "'" & NULA(ESKIADISYON, X, "SAAT") & "',"
                                SQ = SQ & NULN(ESKIADISYON, X, "USERID") & ","
                                SQ = SQ & "'" & NULA(ESKIADISYON, X, "URUNNOT") & "',"
                                SQ = SQ & NULD(ESKIADISYON, X, "ISKORAN") & ","
                                SQ = SQ & NULD(ESKIADISYON, X, "ISTUTAR") & ","
                                SQ = SQ & NULN(ESKIADISYON, X, "SATDURUM") & ","
                                SQ = SQ & NULN(ESKIADISYON, X, "ADSNO") & ","
                                SQ = SQ & NULN(ESKIADISYON, X, "MUTFAKID") & ","
                                SQ = SQ & NULN(ESKIADISYON, X, "SUBEID") & ","
                                SQ = SQ & "'" & NULA(ESKIADISYON, X, "YER") & "',"
                                SQ = SQ & NULD(ESKIADISYON, X, "ADET") & ","
                                SQ = SQ & NULD(ESKIADISYON, X, "PORS") & ","
                                SQ = SQ & NULN(ESKIADISYON, X, "MENUID") & ","
                                SQ = SQ & NULN(ESKIADISYON, X, "USERID") & " "
                                SQ = SQ & ")"

                                If X <> ESKIADISYON.Rows.Count - 1 Then
                                    SQ = SQ & " ,"
                                End If





                            Next

                            Call SQL_KOS(SQ, False)

                            Dim VERI(0) As WS_MASATASI
                            VERI(0) = New WS_MASATASI
                            VERI(0).DURUM = NUMARATOR3.HATA
                            VERI(0).YENIADSNO = 0


                            Return VERI



                        End If




                    Else


                        Dim VERI(0) As WS_MASATASI
                        VERI(0) = New WS_MASATASI
                        VERI(0).DURUM = NUMARATOR3.HATA
                        VERI(0).YENIADSNO = 0


                        Return VERI



                    End If



                End If



            Else


                SQ = ""
                SQ = "SELECT M.a_id as ID, M.a_durum as DURUM,isnull((M.a_adsbas),0) as ADSBAS"
                SQ = SQ & " FROM  " & KUL.tfrm & "men_masa AS M"
                SQ = SQ & " INNER JOIN " & KUL.tfrm & "sube As S ON M.a_sube = S.a_id"
                SQ = SQ & " WHERE M.a_durum <> 'BIRLESIK'  AND S.a_id =" & SUBEID & "   ORDER BY M.a_kod"

                DT = SQL_TABLO(SQ, "T", False, False, False)

                Dim RESULT(DT.Rows.Count - 1) As WS_MASATASI



                For X = 0 To DT.Rows.Count - 1


                    RESULT(X) = New WS_MASATASI
                    RESULT(X).DURUM = NUMARATOR3.MASAKAPATILMIS
                    RESULT(X).MID = NULN(DT, X, "ID")
                    RESULT(X).MDURUM = NULA(DT, X, "DURUM")
                    RESULT(X).MADSBAS = NULN(DT, X, "ADSBAS")



                Next


                Return RESULT

            End If



        End If










    End Function

    Public Class WS_ADSDETAY



        Public DURUM As Long
        Public MDURUM As String
        Public MADSBAS As String
        Public MID As Long
        Public ADSNO As Long
        Public MASAADI As String
        Public SIRA As Long
        Public ADET As Double
        Public PORS As Double
        Public MIKTAR As Double
        Public TUTAR As Double
        Public STOKADI As String
        Public URUNNOT As String

        Public STOKID As Long
        Public BRMFYT As Double
        Public TARIH As String
        Public SAAT As String
        Public USERID As Long
        Public ISKORAN As Double
        Public ISTUTAR As Double
        Public SATDURUM As Double
        Public MUTFAKID As Long
        Public SUBEID As Long
        Public YER As String
        Public MENUID As Long








    End Class




    <WebMethod> Public Function ADS_ADISYONDETAY(ByVal SUBEID As Long, ByVal MEKANID As Long, ByVal MASAID As Long, ByVal ADSNO As Long) As WS_ADSDETAY()


        Dim SQ As String
        Dim DT As New DataTable
        Dim DT2 As New DataTable

        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()



        SQ = ""
        SQ = SQ & " SELECT ADS.a_master_id AS ADSNO,ADS.a_mid AS MID, ADS.a_sira AS SIRA, ADS.a_adet AS ADET, ADS.a_porsiyon AS PORS, ADS.a_mik AS MIKTAR, ADS.a_tut AS TUTAR ,S.a_adi AS STOKADI,ADS.a_not AS URUNNOT,MASA.a_adi AS MASAADI,ADS.a_sid AS STOKID,ADS.a_brf AS BRMFYT,ADS.a_tarih AS TARIH,ADS.a_saat AS SAAT, ADS.a_personel AS USERID, ADS.a_iskoran AS ISKORAN, ADS.a_isktutar AS ISTUTAR, ADS.a_satdurum AS SATDURUM,ADS.a_mutfakid AS MUTFAKID, ADS.a_subeid AS SUBEID, ADS.a_yer AS YER, ADS.a_menuid AS MENUID"
        SQ = SQ & " FROM " & KUL.tfrm & "men_adisyon AS ADS "
        SQ = SQ & " INNER JOIN " & KUL.tfrm & "stok AS S ON ADS.a_sid =S.a_id  "
        SQ = SQ & " INNER JOIN " & KUL.tfrm & "men_masa AS MASA ON MASA.a_id=ADS.a_mid"
        SQ = SQ & " WHERE ADS.a_mid=" & MASAID & "  AND ADS.a_master_id=" & ADSNO & ""

        DT = SQL_TABLO(SQ, "T", False, False, False)

        If DT.Rows.Count > 0 Then






            Dim RESULT(DT.Rows.Count - 1) As WS_ADSDETAY




            For X = 0 To DT.Rows.Count - 1

                RESULT(X) = New WS_ADSDETAY


                Dim oTime As DateTime = Convert.ToDateTime(NULA(DT, X, "SAAT"))


                RESULT(X).DURUM = NUMARATOR3.BAŞARILI
                RESULT(X).ADSNO = NULN(DT, X, "ADSNO")
                RESULT(X).MID = NULN(DT, X, "MID")
                RESULT(X).SIRA = NULN(DT, X, "SIRA")
                RESULT(X).ADET = NULD(DT, X, "ADET")
                RESULT(X).PORS = NULD(DT, X, "PORS")
                RESULT(X).MIKTAR = NULD(DT, X, "MIKTAR")
                RESULT(X).TUTAR = NULD(DT, X, "TUTAR")
                RESULT(X).STOKADI = NULA(DT, X, "STOKADI")
                RESULT(X).STOKID = NULN(DT, X, "STOKID")
                RESULT(X).BRMFYT = NULD(DT, X, "BRMFYT")
                RESULT(X).TARIH = TTAR.TR2UK(NULA(DT, X, "TARIH"))

                RESULT(X).SAAT = oTime.ToShortTimeString
                RESULT(X).USERID = NULN(DT, X, "USERID")
                RESULT(X).ISKORAN = NULD(DT, X, "ISKORAN")
                RESULT(X).ISTUTAR = NULD(DT, X, "ISTUTAR")
                RESULT(X).SATDURUM = NULN(DT, X, "SATDURUM")
                RESULT(X).MUTFAKID = NULN(DT, X, "MUTFAKID")
                RESULT(X).SUBEID = NULN(DT, X, "SUBEID")
                RESULT(X).YER = NULA(DT, X, "YER")
                RESULT(X).MENUID = NULN(DT, X, "MENUID")





                If NULA(DT, X, "URUNNOT") = "" Then
                    RESULT(X).URUNNOT = " "
                Else

                    RESULT(X).URUNNOT = NULA(DT, X, "URUNNOT")

                End If
                RESULT(X).MASAADI = NULA(DT, X, "MASAADI")

            Next


            Return RESULT


        Else

            SQ = ""
            SQ = "SELECT M.a_id as ID, M.a_durum as DURUM,isnull((M.a_adsbas),0) as ADSBAS"
            SQ = SQ & " FROM  " & KUL.tfrm & "men_masa AS M"
            SQ = SQ & " INNER JOIN " & KUL.tfrm & "sube As S ON M.a_sube = S.a_id"
            SQ = SQ & " WHERE M.a_durum <> 'BIRLESIK'  AND S.a_id =" & SUBEID & "  ORDER BY M.a_kod"

            DT2 = SQL_TABLO(SQ, "T", False, False, False)



            Dim RESULT(DT2.Rows.Count - 1) As WS_ADSDETAY



            For X = 0 To DT2.Rows.Count - 1
                RESULT(X) = New WS_ADSDETAY


                RESULT(X).DURUM = NUMARATOR3.MASAKAPATILMIS
                RESULT(X).MDURUM = NULA(DT2, X, "DURUM")
                RESULT(X).MADSBAS = NULA(DT2, X, "ADSBAS")
                RESULT(X).MID = NULN(DT2, X, "ID")



            Next


            Return RESULT

        End If


    End Function


    Public Class WS_BOSMASALAR

        Public DURUM As Long
        Public MID As Long
        Public ID As Long
        Public MDURUM As String
        Public MADSBAS As String
        Public AD As String
        Public MASADURUM As String


    End Class

    <WebMethod> Public Function ADS_BOSMASALAR(ByVal SUBEID As Long, ByVal MEKANID As Long) As WS_BOSMASALAR()

        Dim SQ As String
        Dim DT As New DataTable

        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()


        SQ = ""
        SQ = SQ & " SELECT a_yerid AS MEKANID , a_id AS ID, a_adi AS AD, a_durum AS DURUM"
        SQ = SQ & " FROM " & KUL.tfrm & "men_masa"
        SQ = SQ & " WHERE a_durum ='BOŞ' AND a_sube =" & SUBEID & ""

        DT = SQL_TABLO(SQ, "T", False, False, False)


        If DT.Rows.Count > 0 Then

            Dim RESULT(DT.Rows.Count - 1) As WS_BOSMASALAR

            For X = 0 To DT.Rows.Count - 1

                RESULT(X) = New WS_BOSMASALAR

                RESULT(X).DURUM = NUMARATOR3.BAŞARILI
                RESULT(X).MID = NULN(DT, X, "MEKANID")
                RESULT(X).ID = NULN(DT, X, "ID")

                If NULA(DT, X, "AD") = "" Then
                    RESULT(X).AD = " "
                Else
                    RESULT(X).AD = NULA(DT, X, "AD")

                End If

                RESULT(X).MASADURUM = NULA(DT, X, "DURUM")




            Next

            Return RESULT

        Else

            SQ = ""
            SQ = "SELECT M.a_id as ID, M.a_durum as DURUM,isnull((M.a_adsbas),0) as ADSBAS"
            SQ = SQ & " FROM  " & KUL.tfrm & "men_masa AS M"
            SQ = SQ & " INNER JOIN " & KUL.tfrm & "sube As S ON M.a_sube = S.a_id"
            SQ = SQ & " WHERE M.a_durum <> 'BIRLESIK'  AND S.a_id =" & SUBEID & "   ORDER BY M.a_kod"

            DT = SQL_TABLO(SQ, "T", False, False, False)

            Dim RESULT(DT.Rows.Count - 1) As WS_BOSMASALAR





            For X = 0 To DT.Rows.Count - 1


                RESULT(X) = New WS_BOSMASALAR
                RESULT(X).DURUM = NUMARATOR3.MASAKAPATILMIS
                RESULT(X).ID = NULN(DT, X, "ID")
                RESULT(X).MDURUM = NULA(DT, X, "DURUM")
                RESULT(X).MADSBAS = NULA(DT, X, "ADSBAS")



            Next


            Return RESULT



        End If




    End Function

    Public Function Grafik_MutfakYazdir(ByVal ID As String, ByVal ADSNO As Long, ByVal MASAID As Long, ByVal CARIID As Long, ByVal d As Long) As Boolean
        Dim cariad As String
        Dim cariadres As String
        Dim caritel As String
        Dim caribakiye As Double
        Dim SQ As String
        Dim DT As New DataTable
        Dim DM As New DataTable
        Dim DTM As New DataTable
        Dim DTURUN As New DataTable
        Dim DTUTAR As New DataTable
        Dim Z As Integer = 0
        Dim x As Long
        Dim SIRA As Long



        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()



        SQ = ""
        SQ = " SELECT     Adisyon.a_mutfakid, Mutfak.a_yazyol "
        SQ = SQ & " FROM  " & KUL.tfrm & "men_adisyon AS Adisyon INNER JOIN  "
        SQ = SQ & " " & KUL.tfrm & "men_mutfak AS Mutfak ON Adisyon.a_mutfakid = Mutfak.a_id "
        SQ = SQ & " WHERE     (Adisyon.a_mid = " & MASAID & ") AND (Adisyon.a_yildiz = '*') "
        SQ = SQ & " AND (Adisyon.a_master_id = " & ADSNO & ") "
        SQ = SQ & " GROUP BY Adisyon.a_mutfakid, Mutfak.a_yazyol "
        DM = SQL_TABLO(SQ, "T", False, False, False)
        SQ = ""
        SQ = "Select  Masa.a_adi As MasaAdi,MASA.a_kisisay As KisiSayisi,MASAYER.a_adi As Mekan FROM " & KUL.tfrm & "men_adisyon As ADISYON INNER JOIN "
        SQ = SQ & " " & KUL.tfrm & "men_masa as MASA ON ADISYON.a_mid=MASA.a_id inner join "
        SQ = SQ & "" & KUL.tfrm & "men_masayer as MASAYER ON MASA.a_yerid=MASAYER.a_id "
        SQ = SQ & "group by MASA.a_adi , Masa.a_kisisay, Masayer.a_adi, ADISYON.a_master_id "
        SQ = SQ & "having ADISYON.a_master_id = " & ADSNO & " "
        DTM = SQL_TABLO(SQ, "T", False, False, False)

        SQ = ""
        SQ = " SELECT a_adi,a_fadres1,a_ftel,a_abakiye,a_bbakiye,a_bakiye FROM " & KUL.tfrm & "cari where a_id= " & CARIID
        DT = SQL_TABLO(SQ, "T", False, False, False)
        cariad = NULA(DT, 0, "a_adi")
        cariadres = NULA(DT, 0, "a_fadres1")
        caritel = NULA(DT, 0, "a_ftel")
        caribakiye = NULA(DT, 0, "a_bakiye")

        SQ = ""
        SQ = " Select SUM(ADISYON.a_tut) AS TUTTOP"
        SQ = SQ & " From  " & KUL.tfrm & "men_adisyon As ADISYON INNER Join "
        SQ = SQ & "" & KUL.tfrm & "stok As Stok On ADISYON.a_sid = Stok.a_id INNER Join "
        SQ = SQ & " tbl_user On ADISYON.a_userid = tbl_user.a_id INNER Join "
        SQ = SQ & "" & KUL.tfrm & "men_adisyonmas As MAS On ADISYON.a_master_id=MAS.a_id "
        SQ = SQ & " WHERE (ADISYON.a_master_id = " & ADSNO & ") And  (ADISYON.a_mutfakid = " & NULD(DM, d, "a_mutfakid") & ")  "

        DTUTAR = SQL_TABLO(SQ, "T", False, False, False)


        SQ = ""
        SQ = " Select ADISYON.a_yer As Tur,ADISYON.a_sid As StokID, ADISYON.a_not As UrunNot, Stok.a_adi As UrunAdi, ADISYON.a_porsiyon As Porsiyon,SUM(ADISYON.a_adet) As Adet, "
        SQ = SQ & " ADISYON.a_brf AS BrmFiyat,  MAS.a_not As AdsNot, SUM(ADISYON.a_tut) AS TUTTOP , SUM(ADISYON.a_mik) AS MIKTOP, per.a_adi AS Personel  "
        SQ = SQ & " From  " & KUL.tfrm & "men_adisyon As ADISYON INNER Join "
        SQ = SQ & "" & KUL.tfrm & "stok As Stok On ADISYON.a_sid = Stok.a_id INNER Join "
        SQ = SQ & "  " & KUL.tfrm & "personel as per On ADISYON.a_personel = per.a_id INNER Join "
        SQ = SQ & "" & KUL.tfrm & "men_adisyonmas As MAS On ADISYON.a_master_id=MAS.a_id "
        SQ = SQ & " WHERE (ADISYON.a_master_id = " & ADSNO & ") And  (ADISYON.a_mutfakid = " & NULD(DM, d, "a_mutfakid") & ") And (ADISYON.a_yildiz=N'*') "
        SQ = SQ & " group by ADISYON.a_yer,ADISYON.a_sid , ADISYON.a_not , Stok.a_adi , ADISYON.a_porsiyon ,ADISYON.a_brf , MAS.a_not,per.a_adi  "
        DT = SQL_TABLO(SQ, "T", False, False, False)
        If DT.Rows.Count > 0 Then

            SQ = ""
            SQ = "INSERT INTO " & KUL.tfrm & "men_pservermas (a_id , a_adsno , a_adsnot , a_ttutar , a_sube , a_mekan , a_mutfak , a_masa , a_kisisay , a_personel , a_cad,  a_cadres , a_ctelno , a_cbakiye , a_siptip , a_odemetip )"
            SQ = SQ & " VALUES "
            SQ = SQ & " ( "
            SQ = SQ & " " & ID & ", "
            SQ = SQ & " " & ADSNO & ", "
            SQ = SQ & " N'', "
            SQ = SQ & " " & NULD(DTUTAR, 0, "TUTTOP") & " , "
            SQ = SQ & " N'" & NULA(DT, 0, "Sube") & "', "
            SQ = SQ & " N'" & NULA(DTM, 0, "Mekan") & "', "
            SQ = SQ & " N'', "
            SQ = SQ & " N'" & NULA(DTM, 0, "MasaAdi") & "', "
            SQ = SQ & " N'" & NULA(DTM, 0, "KisiSayisi") & "', "
            SQ = SQ & " N'" & NULA(DT, 0, "Personel") & "', "
            SQ = SQ & " N'" & cariad & "', "
            SQ = SQ & " N'" & cariadres & "', "
            SQ = SQ & " N'" & caritel & "', "
            SQ = SQ & " N'" & caribakiye & "', "
            SQ = SQ & " N'', "
            SQ = SQ & " N'' "
            SQ = SQ & " ) "
            Call SQL_KOS(SQ, False)
            SIRA = 1
            For x = 0 To DT.Rows.Count - 1

                SQ = ""
                SQ = "INSERT INTO " & KUL.tfrm & "men_pserverdet (a_id , a_sira , a_ad , a_birim , a_brmfiy , a_miktar , a_not , a_tutar,a_porsiyon,a_adet )"
                SQ = SQ & " VALUES "
                SQ = SQ & " ( "
                SQ = SQ & " " & ID & ", "
                SQ = SQ & " " & SIRA & ", "
                SQ = SQ & " N'" & NULA(DT, x, "UrunAdi") & "', "
                SQ = SQ & " N'" & NULA(DT, x, "Birim") & "' , "
                SQ = SQ & " " & NULD(DT, x, "BrmFiyat") & " , "
                SQ = SQ & " " & NULD(DT, x, "MIKTOP") & ", "
                SQ = SQ & " N'" & NULA(DT, x, "UrunNot") & "', "
                SQ = SQ & " " & NULD(DT, x, "TUTTOP") & ", "
                SQ = SQ & " " & NULD(DT, x, "Porsiyon") & ","
                SQ = SQ & " " & NULN(DT, x, "Adet") & " "
                SQ = SQ & " ) "
                Call SQL_KOS(SQ, False)
                SIRA = SIRA + 1
            Next
            Return True
        Else
            Return False
        End If
    End Function

    Public Function Grafik_İptalYazdir(ByVal USERID As Long, ByVal IPTALNEDENID As Long, ByVal SUBEID As Long, ByVal ADSNO As Long, ByVal CARIID As Long, ByVal MASAID As Long, ByVal SIRA As Long, ByVal STOKID As Long, ByVal MIKTAR As Double, ByVal BIRIMFIYAT As Double, ByVal TUTAR As Double, ByVal ACIKLAMA As String, ByVal KALANMIKTAR As Double, ByVal PORSIYON As Double, ByVal ACK As String) As Boolean

        Dim cariad As String
        Dim cariadres As String
        Dim caritel As String
        Dim caribakiye As Double
        Dim SQ As String
        Dim SUBEADI As String
        Dim yaz As String

        Dim DTIPTAL As New DataTable
        Dim DT As New DataTable
        Dim DT2 As New DataTable
        Dim DTSUBE As New DataTable
        Dim DM As New DataTable

        Dim SBID As Long
        Dim MUTFAKID As Long

        Dim EB_SIL As Long
        Dim AdsYazdir As Long
        Dim sonuc As Boolean
        EB_SIL = Max_Bul(2)

        SQ = ""
        SQ = "SELECT a_id, a_durum, a_adsnosu, a_adsbas"
        SQ = SQ & " FROM " & KUL.tfrm & "men_masa"
        SQ = SQ & " WHERE(a_id = " & MASAID & ")"
        DT = SQL_TABLO(SQ, "VERI", False, False, False)
        AdsYazdir = NULN(DT, 0, "a_adsbas")


        SQ = "INSERT INTO " & KUL.tfrm & "men_silinen (a_id, a_sirax, a_adno, a_cid, a_mid, a_sira, a_sid, a_mik, a_brf, a_tut, "
        SQ = SQ & " a_not, a_tarih, a_saat, a_userid, a_sebepid, a_subeid, a_adsyaz, a_master_id,a_ack,a_personelid)"
        SQ = SQ & " VALUES "
        SQ = SQ & " ( "
        SQ = SQ & " " & EB_SIL & ", "
        SQ = SQ & " " & SIRA & ", "
        SQ = SQ & " '" & ADSNO & "', "
        SQ = SQ & " " & CARIID & ", "
        SQ = SQ & " " & MASAID & ", "
        SQ = SQ & " " & SIRA & ", "
        SQ = SQ & " " & STOKID & ", "
        SQ = SQ & " " & MIKTAR - KALANMIKTAR & ", "
        SQ = SQ & " " & BIRIMFIYAT & ", "
        SQ = SQ & " " & TUTAR & ", "
        SQ = SQ & " '" & ACIKLAMA & "', "
        SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & "', "
        SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & " " & Now.ToShortTimeString & "', "
        SQ = SQ & " " & USERID & ", "
        SQ = SQ & " " & IPTALNEDENID & " ,"
        SQ = SQ & " " & -SUBEID & " ,"
        SQ = SQ & " " & AdsYazdir & " ,"
        SQ = SQ & " " & ADSNO & ", "
        SQ = SQ & " '" & ACK & "', "
        SQ = SQ & " " & USERID & " "
        SQ = SQ & " ) "

        If SQL_KOS(SQ, False) = True Then
            SQ = ""
            SQ = "Select  STOK.a_kod As urunkodu, STOK.a_adi As urunadi, MASA.a_adi As masaadi, MASA.a_kod As masakodu,MUTFAK.a_noprn ,"
            SQ = SQ & " MEKAN.a_adi AS salon, SILINEN.a_mik As miktar, SILINEN.a_not As nota, per.a_adi As personel, "
            SQ = SQ & " MASA.a_id, SILINEN.a_tarih, SILINEN.a_saat"
            SQ = SQ & " From " & KUL.tfrm & "stok as STOK INNER Join"
            SQ = SQ & " " & KUL.tfrm & "men_silinen SILINEN On STOK.a_id = SILINEN.a_sid INNER Join"
            SQ = SQ & " tbl_001_personel as per On SILINEN.a_personelid = per.a_id INNER Join"
            SQ = SQ & " " & KUL.tfrm & "men_masayer AS MEKAN INNER Join"
            SQ = SQ & " " & KUL.tfrm & "men_masa AS MASA On MEKAN.a_id = MASA.a_yerid On SILINEN.a_mid = MASA.a_id inner join "
            SQ = SQ & " " & KUL.tfrm & "men_urunmutfak AS MUTFAK on MUTFAK.a_sid=STOK.a_id"
            SQ = SQ & " Where SILINEN.a_master_id=" & ADSNO & "  And SILINEN.a_id=" & EB_SIL & " AND  MUTFAK.a_noprn=0"
            DTIPTAL = SQL_TABLO(SQ, "VERI", False, False, False)
            If DTIPTAL.Rows.Count > 0 Then
                SQ = ""
                SQ = " SELECT a_adi,a_fadres1,a_ftel,a_abakiye,a_bbakiye,a_bakiye FROM " & KUL.tfrm & "cari where a_id= " & CARIID
                DT = SQL_TABLO(SQ, "T", False, False, False)
                cariad = NULA(DT, 0, "a_adi")
                cariadres = NULA(DT, 0, "a_fadres1")
                caritel = NULA(DT, 0, "a_ftel")
                caribakiye = NULD(DT, 0, "a_bakiye")

                SQ = ""
                SQ = "Select a_id, a_kod, a_adi, a_sube "
                SQ = SQ & " FROM " & KUL.tfrm & "men_masa"
                SQ = SQ & " WHERE(a_id = " & MASAID & ")"
                DT = SQL_TABLO(SQ, "VERI", False, False, False)

                If NULN(DT, 0, "a_sube") <> 0 Then
                    SBID = NULN(DT, 0, "a_sube")
                Else
                    SQ = "Select a_yildiz, a_mid, a_sira, a_sid, a_mik, a_brf, a_tut, a_tarih, a_saat,a_personel as a_userid, a_not, a_durum, a_yer, a_cid, a_subeid, a_mutfakid"
                    SQ = SQ & " FROM " & KUL.tfrm & "men_adisyon"
                    SQ = SQ & " WHERE(a_mid = " & MASAID & ") And (a_sid = " & STOKID & ")"
                    SQ = SQ & " And (a_master_id = " & ADSNO & ")"
                    DT = SQL_TABLO(SQ, "VERI", False, False, False)
                    SBID = NULN(DT, 0, "a_subeid")
                End If

                'SQ = "Select M.a_id, M.a_sube, M.a_yazyol, U.a_sid, M.a_adi"
                'SQ = SQ & " FROM " & KUL.tfrm & "men_mutfak As M INNER JOIN"
                'SQ = SQ & " " & KUL.tfrm & "men_urunmutfak As U On M.a_id = U.a_mid"
                'SQ = SQ & " WHERE (M.a_sube = " & SBID & ") And (U.a_sid = " & SID & ")"
                'DMU = SQL_TABLO(SQ, "T", False, False, False)
                SQ = ""
                SQ = "Select a_adi "
                SQ = SQ & " FROM " & KUL.tfrm & "sube"
                SQ = SQ & " WHERE(a_id = " & SBID & ")"
                DTSUBE = SQL_TABLO(SQ, "VERI", False, False, False)
                SUBEADI = NULA(DTSUBE, 0, "a_adi")
                MUTFAKID = MUTFAK_BUL(STOKID, SBID, MASAID)

                SQ = ""
                SQ = "SELECT     Adisyon.a_mutfakid, Yaz.a_yazici as a_yazyol"
                SQ = SQ & " FROM  " & KUL.tfrm & "men_adisyon AS Adisyon INNER JOIN"
                SQ = SQ & " " & KUL.tfrm & "men_mutfak AS Mutfak ON Adisyon.a_mutfakid = Mutfak.a_id INNER JOIN "
                SQ = SQ & " " & KUL.tfrm & "men_mutfak_yazici as YAZ on YAZ.a_mid=Mutfak.a_id"
                SQ = SQ & " WHERE     (Adisyon.a_mutfakid = " & MUTFAKID & ") "
                SQ = SQ & " GROUP BY Adisyon.a_mutfakid, Yaz.a_yazici"
                DM = SQL_TABLO(SQ, "T", False, False, False)

                yaz = NULA(DM, 0, "a_yazyol")

                Dim K As Long
                Dim DTID As New DataTable
                SQ = ""
                SQ &= "INSERT INTO ZMX_001_men_pserver (a_tur) "
                SQ &= "VALUES(10) "
                SQ &= "SELECT SCOPE_IDENTITY() as ads"
                SQ = SQ.Replace("001", Mid(KUL.tfrm, 5, 3))

                DTID = SQL_TABLO(B_SQL, SQ, "T", False, False, False)

                K = NULN(DTID, 0, "ads") + 1

                SQ = ""
                SQ = "INSERT INTO " & KUL.tfrm & "men_pservermas (a_id , a_adsno , a_adsnot , a_ttutar , a_sube , a_mekan , a_mutfak , a_masa , a_kisisay , a_personel , a_cad,  a_cadres , a_ctelno , a_cbakiye , a_siptip , a_odemetip )"
                SQ = SQ & " VALUES "
                SQ = SQ & " ( "
                SQ = SQ & " " & K & ", "
                SQ = SQ & " " & ADSNO & ", "
                SQ = SQ & " N' ', "
                SQ = SQ & "  0 , "
                SQ = SQ & " N'" & SUBEADI & "', "
                SQ = SQ & " N'" & NULA(DTIPTAL, 0, "Mekan") & "', "
                SQ = SQ & " N'', "
                SQ = SQ & " N'" & NULA(DTIPTAL, 0, "masaadi") & "', "
                SQ = SQ & " N' ', "
                SQ = SQ & " N'" & NULA(DTIPTAL, 0, "personel") & "', "
                SQ = SQ & " N'" & cariad & "', "
                SQ = SQ & " N'" & cariadres & "', "
                SQ = SQ & " N'" & caritel & "', "
                SQ = SQ & " N'" & caribakiye & "', "
                SQ = SQ & " N'', "
                SQ = SQ & " N'' "
                SQ = SQ & " ) "
                sonuc = SQL_KOS(SQ, False)


                SQ = ""
                SQ = "INSERT INTO " & KUL.tfrm & "men_pserverdet (a_id , a_sira , a_ad , a_birim , a_brmfiy , a_miktar , a_not , a_tutar )"
                SQ = SQ & " VALUES "
                SQ = SQ & " ( "
                SQ = SQ & " " & K & ", "
                SQ = SQ & " 1 , "
                SQ = SQ & " N'" & NULA(DTIPTAL, 0, "urunadi") & "', "
                SQ = SQ & " N' ' , "
                SQ = SQ & " 0  , "
                SQ = SQ & " " & NULD(DTIPTAL, 0, "miktar") & ", "
                SQ = SQ & " N'" & NULA(DTIPTAL, 0, "not") & "', "
                SQ = SQ & " 0 "
                'SQ = SQ & ", " & NULD(DTIPTAL, 0, "porsiyon") & ", "
                'SQ = SQ & " " & NULD(DTIPTAL, 0, "adet") & " "
                SQ = SQ & " ) "
                sonuc = SQL_KOS(SQ, False)

                SQ = ""
                SQ = "INSERT INTO " & KUL.tfrm & "men_pserver (a_id, a_tur, a_pda, a_user, a_tarih, a_saat, a_komut, a_veri, a_durum, a_sube, a_masa)"
                SQ = SQ & " VALUES "
                SQ = SQ & " ( "
                SQ = SQ & " " & K & ", "
                SQ = SQ & " '" & "IPTAL" & "', "
                SQ = SQ & " '" & NULA(DM, 0, "a_pda") & "', "
                SQ = SQ & "  " & NULN(DM, 0, "Garson_Id") & " , "
                SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & "', "
                SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & " " & TTAR.SAAT & "', "
                SQ = SQ & " '" & yaz & "', "
                SQ = SQ & " '', "
                SQ = SQ & " '" & "H" & "', "
                SQ = SQ & "  " & SBID & " , "
                SQ = SQ & "  " & MASAID & " "
                SQ = SQ & " ) "
                sonuc = SQL_KOS(SQ, False)

                'SQ = ""
                'SQ = "DELETE FROM " & KUL.tfrm & "men_adisyon"
                'SQ = SQ & " WHERE (a_mid = " & MA_ID & ") AND (a_sira = " & SI_ID & ") AND a_master_id=" & Val(L_ANOSU.Text)
                'Call SQL_KOS(SQ, False)
            End If


            If KALANMIKTAR = 0 Then

                SQ = ""
                SQ = "DELETE FROM " & KUL.tfrm & "men_adisyon"
                SQ = SQ & " WHERE (a_mid = " & MASAID & ") AND (a_sira = " & SIRA & ") AND a_master_id=" & ADSNO
                sonuc = SQL_KOS(SQ, False)



                SQ = ""
                SQ = "SELECT a_mid,a_sid "
                SQ = SQ & " FROM " & KUL.tfrm & "men_adisyon "
                SQ = SQ & " WHERE "
                SQ = SQ & " a_mid  = " & MASAID & " AND a_master_id =" & ADSNO
                DT2 = SQL_TABLO(SQ, "T", False, False, False)

                If DT2.Rows.Count = 0 Then

                    SQ = ""
                    SQ = "UPDATE " & KUL.tfrm & "men_adisyonmas"
                    SQ = SQ & " SET "
                    SQ = SQ & " a_durum=1"
                    SQ = SQ & " WHERE a_id=" & ADSNO
                    sonuc = SQL_KOS(SQ, False)

                    SQ = "SELECT AdsMas.a_id "
                    SQ = SQ & " From tbl_001_men_adisyonmas AS AdsMas "
                    SQ = SQ & " INNER JOIN tbl_user ON AdsMas.a_userid = tbl_user.a_id  "
                    SQ = SQ & " WHERE AdsMas.a_mid=" & MASAID & " AND AdsMas.a_durum=0 ORDER BY AdsMas.a_id ASC"
                    DT2 = SQL_TABLO(SQ, "T", False, False, False)

                    If DT2.Rows.Count = 0 Then
                        SQ = ""
                        SQ = "UPDATE " & KUL.tfrm & "men_masa "
                        SQ = SQ & " SET "
                        SQ = SQ & " a_kisisay = 0, "
                        SQ = SQ & " a_durum = 'BOŞ'"
                        SQ = SQ & " WHERE "
                        SQ = SQ & " a_id  = " & MASAID
                        sonuc = SQL_KOS(SQ, False)
                    End If
                End If


            Else
                SQ = ""
                SQ = "UPDATE " & KUL.tfrm & "men_adisyon "
                SQ = SQ & " SET "
                SQ = SQ & " a_mik = " & KALANMIKTAR & ", "
                SQ = SQ & " a_adet = " & KALANMIKTAR / PORSIYON & ", "


                SQ = SQ & " a_tut = a_brf * " & KALANMIKTAR
                SQ = SQ & " WHERE "
                SQ = SQ & " a_mid  = " & MASAID & " AND "
                SQ = SQ & " a_sira = " & SIRA & " "
                sonuc = SQL_KOS(SQ, False)

            End If

            Return True


        Else

            Return False



        End If

    End Function




#End Region
#Region "MB/Uyarı Mesajları"
    Public Enum NUMARATOR
        BAŞARILI = 1
        UYARI = 2
        HATA = 3
        TESLIMALMAHATASI = 4
        AYARLARTABLOSU = 5
        KULLANICIGRUP = 6
        KONTOLLUGONDERIMBOSBELGE = 7
        BOSBELGE = 8
        BELGEBULUNAMADI = 9
        EKLENEMEYENURUNVAR = 10
        SILINEMEYENURUNVAR = 11
        KULLANICIONDEGERFIRMABOS = 12
        DEGERDONMEDI = 13 'FONKSİYONDA HERHANGİ KOŞUL SAĞLANMAZSA VE DİREK ENDFUNCTION' A DÜŞERSE DOESN'T RETURN VALUE HATASINA KARŞILIK FONKSİYONLARIN SONUNDA KULLANILDI. 
        BULUNAMAYANBARKOD = 14
        APKVERSIYONESKI = 15
        WSVERSIYONESKI = 16

    End Enum
#End Region
#Region "MB/Giriş İşlemleri"
    Public Class WS_GIRIS
        Public DURUM As Long
        Public KULKOD As String
        Public KULAD As String
        Public DEPOID As Integer
        Public DEPOADI As String
        Public KASAID As Integer
        Public KASAKOD As String
        Public KASAADI As String
        Public KASASUBE As String
        Public BANKAID As Integer
        Public BANKAKOD As String
        Public BANKAADI As String
        Public BANKASUBE As String
        Public SUBEID As Integer
        Public SUBEADI As String
        Public SUBEKODU As String
        Public USERID As Integer
        Public GRUPTUR As Integer
        Public DEPOYETKI As String
        Public KASAYETKI As String
        Public BANKAYETKI As String
        Public SUBEYETKI As String
        Public PARAMETRE As Long
        Public KONTROLLUDEPOID As Long
        Public KONTROLLUDEPOADI As String
        Public KONTROLLUDEPOGIRIS As Long
        Public KONTROLLUDEPOCIKIS As Long
        Public FIRMA As String
        Public GIRIS As Long
        Public CIKIS As Long
        Public FFIYATDEGIS As String
        Public IFIYATDEGIS As String
        Public YETKISEMAKULLANIM As String
        Public KURUSHASSASIYET As Integer
        Public TEXT_SFIYAT As String
        Public TEXT_AFIYAT As String
        Public TEXT_OFIYAT1 As String
        Public TEXT_OFIYAT2 As String
        Public TEXT_OFIYAT3 As String
        Public TEXT_OFIYAT4 As String
        Public TEXT_OFIYAT5 As String
        Public TEXT_OFIYAT6 As String
        Public TEXT_SMALIYET As String
        Public TEXT_OMALIYET As String
        Public TEXT_STOKKARTFIYAT As String
        Public TEXT_STOKKARTOZELFIYAT As String
        Public TEXT_MALIYETLER As String

    End Class
    ''' <summary>
    ''' Gönderilen Kullanıcı Kodu ve Şifresine göre giriş yaptırılıyor.
    ''' </summary>
    ''' <param name="KULKOD">Kullanıcı Kodu</param>
    ''' <param name="KULSIFRE">Kullanıcı Şifresi</param>
    ''' <returns></returns>
    <WebMethod()>
    Public Function MB_Giris(ByVal VERSION As String, ByVal KULKOD As String, ByVal KULSIFRE As String) As WS_GIRIS()
        'KULKOD= KULLANICI KODU
        'KULSIFRE=KULLANICI ŞİFRESİ
        Dim SQ As String
        Dim DT As New DataTable
        Dim DT2 As New DataTable
        Dim DT3 As New DataTable
        Dim DT4 As New DataTable
        Dim DEPO As String
        Dim PARAMETREKONTROL As Long
        Dim KONTROLLUDEPOID As Long
        Dim SUBEADI As String
        Dim SUBEKODU As String
        Dim DEPOADI As String
        Dim GIRIS As Long
        Dim CIKIS As Long
        Dim KASAADI As String
        Dim KASAKOD As String
        Dim BANKAADI As String
        Dim BANKAKOD As String
        Dim KONTROLLUDEPOADI As String = ""
        Dim KONTROLLUDEPOGIRIS As Long = 0
        Dim KONTROLLUDEPOCIKIS As Long = 0
        Dim YETKISEMAKULLANIM As String
        Dim KURUSHASSASIYET As Integer = 2
        Dim TEXT_SFIYAT As String = "Satış Fiyatı"
        Dim TEXT_AFIYAT As String = "Alış Fiyatı"
        Dim TEXT_OFIYAT1 As String = "Özel Fiyat (1)"
        Dim TEXT_OFIYAT2 As String = "Özel Fiyat (2)"
        Dim TEXT_OFIYAT3 As String = "Özel Fiyat (3)"
        Dim TEXT_OFIYAT4 As String = "Özel Fiyat (4)"
        Dim TEXT_OFIYAT5 As String = "Özel Fiyat (5)"
        Dim TEXT_OFIYAT6 As String = "Özel Fiyat (6)"
        Dim TEXT_SMALIYET As String = "Son Maliyet"
        Dim TEXT_OMALIYET As String = "Ortalama Maliyet"
        Dim TEXT_STOKKARTFIYAT As String = "Stok Kart Fiyatları"
        Dim TEXT_STOKKARTOZELFIYAT As String = "Stok Kart Özel Fiyatları"
        Dim TEXT_MALIYETLER As String = "Maliyetler"






        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"

            Dim VERI(0) As WS_GIRIS
            VERI(0) = New WS_GIRIS
            VERI(0).DURUM = NUMARATOR.APKVERSIYONESKI
            VERI(0).KULKOD = " "
            VERI(0).KULAD = " "
            VERI(0).DEPOID = 0
            VERI(0).GIRIS = 0
            VERI(0).CIKIS = 0
            VERI(0).KASAID = 0
            VERI(0).KASAKOD = " "
            VERI(0).KASAADI = " "
            VERI(0).KASASUBE = " "
            VERI(0).BANKAID = 0
            VERI(0).BANKAADI = " "
            VERI(0).BANKAKOD = " "
            VERI(0).BANKASUBE = " "
            VERI(0).SUBEID = 0
            VERI(0).USERID = 0
            VERI(0).GRUPTUR = 0
            VERI(0).DEPOYETKI = 0
            VERI(0).SUBEYETKI = 0
            VERI(0).KASAYETKI = 0
            VERI(0).BANKAYETKI = 0
            VERI(0).KONTROLLUDEPOID = 0
            VERI(0).KONTROLLUDEPOADI = " "
            VERI(0).KONTROLLUDEPOGIRIS = 0
            VERI(0).KONTROLLUDEPOCIKIS = 0
            VERI(0).PARAMETRE = 0
            VERI(0).FIRMA = " "
            VERI(0).FFIYATDEGIS = " "
            VERI(0).IFIYATDEGIS = " "
            VERI(0).YETKISEMAKULLANIM = " "
            VERI(0).KURUSHASSASIYET = 0
            VERI(0).TEXT_SFIYAT = " "
            VERI(0).TEXT_AFIYAT = " "
            VERI(0).TEXT_OFIYAT1 = " "
            VERI(0).TEXT_OFIYAT2 = " "
            VERI(0).TEXT_OFIYAT3 = " "
            VERI(0).TEXT_OFIYAT4 = " "
            VERI(0).TEXT_OFIYAT5 = " "
            VERI(0).TEXT_OFIYAT6 = " "
            VERI(0).TEXT_SMALIYET = " "
            VERI(0).TEXT_OMALIYET = " "
            VERI(0).TEXT_STOKKARTFIYAT = " "
            VERI(0).TEXT_STOKKARTOZELFIYAT = " "
            VERI(0).TEXT_MALIYETLER = " "

            Return VERI
#End Region

        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

#Region "WS HATA"
            Dim VERI(0) As WS_GIRIS
            VERI(0) = New WS_GIRIS
            VERI(0).DURUM = NUMARATOR.WSVERSIYONESKI
            VERI(0).KULKOD = " "
            VERI(0).KULAD = " "
            VERI(0).DEPOID = 0
            VERI(0).GIRIS = 0
            VERI(0).CIKIS = 0
            VERI(0).KASAID = 0
            VERI(0).KASAKOD = " "
            VERI(0).KASAADI = " "
            VERI(0).KASASUBE = " "
            VERI(0).BANKAID = 0
            VERI(0).BANKAADI = " "
            VERI(0).BANKAKOD = " "
            VERI(0).BANKASUBE = " "
            VERI(0).SUBEID = 0
            VERI(0).USERID = 0
            VERI(0).GRUPTUR = 0
            VERI(0).DEPOYETKI = 0
            VERI(0).SUBEYETKI = 0
            VERI(0).KASAYETKI = 0
            VERI(0).BANKAYETKI = 0
            VERI(0).KONTROLLUDEPOID = 0
            VERI(0).KONTROLLUDEPOADI = " "
            VERI(0).KONTROLLUDEPOGIRIS = 0
            VERI(0).KONTROLLUDEPOCIKIS = 0
            VERI(0).PARAMETRE = 0
            VERI(0).FIRMA = " "
            VERI(0).FFIYATDEGIS = " "
            VERI(0).IFIYATDEGIS = " "
            VERI(0).YETKISEMAKULLANIM = " "
            VERI(0).KURUSHASSASIYET = 0
            VERI(0).TEXT_SFIYAT = " "
            VERI(0).TEXT_AFIYAT = " "
            VERI(0).TEXT_OFIYAT1 = " "
            VERI(0).TEXT_OFIYAT2 = " "
            VERI(0).TEXT_OFIYAT3 = " "
            VERI(0).TEXT_OFIYAT4 = " "
            VERI(0).TEXT_OFIYAT5 = " "
            VERI(0).TEXT_OFIYAT6 = " "
            VERI(0).TEXT_SMALIYET = " "
            VERI(0).TEXT_OMALIYET = " "
            VERI(0).TEXT_STOKKARTFIYAT = " "
            VERI(0).TEXT_STOKKARTOZELFIYAT = " "
            VERI(0).TEXT_MALIYETLER = " "

            Return VERI

#End Region



        End If












        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()

        KULKOD = Filte_injection(KULKOD)
        KULSIFRE = Filte_injection(KULSIFRE)



        bym_dll_Prm_Init(-1)
        SQ = ""
        SQ = "Select KULGRUP.a_tur as KULLANICIGRUP,KUL.a_id AS ID, KUL.a_kod AS KULKODU,KUL.a_adi AS KULADI,KUL.a_depoid AS DEPOID,KUL.a_kasaid AS KASAID,KASA.a_adi KASAADI,KASA.a_kod AS KASAKOD,KASA.a_sube_id AS KASASUBE,KUL.a_bankaid AS BANKAID,
		BANKA.a_adi AS BANKAADI,BANKA.a_kod AS BANKAKOD,BANKA.a_sube_id AS BANKASUBE,KUL.a_sube_id as SUBEID, KUL.a_firma_id AS FIRMA, KUL.a_grpid AS GRUPID, KULYETKI.a_degbank as BANKAYETKI,KULYETKI.a_degsube as SUBEYETKI,KULYETKI.a_degkasa as KASAYETKI,KULYETKI.a_degdepo as DEPOYETKI,KULYETKI.a_ffiyat as FFIYATDEGIS,KULYETKI.a_ifiyat AS IFIYATDEGIS "
        SQ = SQ & " FROM  tbl_user as KUL "
        SQ = SQ & " INNER JOIN tbl_userfrm AS KULYETKI ON KUL.a_id=KULYETKI.a_userid AND KUL.a_firma_id = KULYETKI.a_firma "
        SQ = SQ & " INNER JOIN tbl_user_grp AS KULGRUP ON KUL.a_grpid=KULGRUP.a_id "
        SQ = SQ & " INNER JOIN " & KUL.tfrm & "kasa AS KASA ON KUL.a_kasaid=KASA.a_id "
        SQ = SQ & " INNER JOIN " & KUL.tfrm & "banka AS BANKA ON KUL.a_bankaid=BANKA.a_id "
        SQ = SQ & " WHERE KUL.a_kod = '" + KULKOD + "' and a_sifre='" + KULSIFRE + "'"
        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
        KUL.KOD = NULA(DT, 0, "ID")
        KUL.ADI = NULA(DT, 0, "KULADI")
        KUL.FIRMA = NULN(DT, 0, "FIRMA")

        KASAADI = NULA(DT, 0, "KASAADI")
        If KASAADI = Nothing Then KASAADI = " "
        KASAKOD = NULA(DT, 0, "KASAKOD")
        If KASAKOD = Nothing Then KASAKOD = 0

        BANKAADI = NULA(DT, 0, "BANKAADI")
        If BANKAADI = Nothing Then BANKAADI = " "
        BANKAKOD = NULA(DT, 0, "BANKAKOD")
        If BANKAKOD = Nothing Then BANKAKOD = 0







        'SQ = ""
        'SQ = "SELECT a_userid, a_firma, a_depoid, a_kasaid, a_bankaid, a_perid, "
        'SQ = SQ & " a_onay, a_onaylar, a_silme, a_uyar, a_sfiyon, a_sfbseri, a_sfbno, a_sibseri, a_sibno, a_prnbas, a_prnson, "
        'SQ = SQ & " a_iisk, a_ikdv, a_ifiyat, a_fisk, a_fkdv, a_ffiyat, a_bbkod, a_sekler, a_sduzelt, a_ssiler, "
        'SQ = SQ & " a_cekler, a_cduzelt, a_csiler, a_frmdiz, a_afbseri, a_afbno, a_otoyedek, a_yedekfiyat, "
        'SQ = SQ & " a_degdepo, a_degkasa, a_degbank, a_degtari, a_sube_id, a_degsube "
        'SQ = SQ & " FROM tbl_userfrm"
        'SQ = SQ & " WHERE (a_userid = " & Val(KUL.KOD) & ") AND (a_firma = " & Val(KUL.FIRMA) & ")"
        'dt1 = SQL_TABLO(SQ, "T", False, False, False)

        'KUL.ID_BANKA = NULN(dt1, 0, "a_bankaid")
        'KUL.ID_DEPO = NULN(dt1, 0, "a_depoid")
        'KUL.ID_KASA = NULN(dt1, 0, "a_kasaid")

        'KUL.PRM_EHa_degdepo = NULA(dt1, 0, "a_degdepo")
        'KUL.PRM_EHa_degkasa = NULA(dt1, 0, "a_degkasa")
        'KUL.PRM_EHa_degbank = NULA(dt1, 0, "a_degbank")
        'KUL.PRM_EHa_degtari = NULA(dt1, 0, "a_degtari")

        'KUL.PRM_BelgeIptal = NULA(dt1, 0, "a_onay")
        'KUL.PRM_BelgeOnay = NULA(dt1, 0, "a_onaylar")

        'KUL.PRM_Uyari = NULA(dt1, 0, "a_uyar")
        'KUL.PRM_Silme = NULA(dt1, 0, "a_silme")
        'KUL.PRM_SFiyon = NULN(dt1, 0, "a_sfiyon")
        'KUL.PRM_prnbas = NULA(dt1, 0, "a_prnbas")
        'KUL.PRM_prnson = NULA(dt1, 0, "a_prnson")
        'KUL.PRM_EHa_iisk = NULA(dt1, 0, "a_iisk")
        'KUL.PRM_EHa_ikdv = NULA(dt1, 0, "a_ikdv")
        'KUL.PRM_EHa_ifiyat = NULA(dt1, 0, "a_ifiyat")
        'KUL.PRM_EHa_fisk = NULA(dt1, 0, "a_fisk")
        'KUL.PRM_EHa_fkdv = NULA(dt1, 0, "a_fkdv")
        'KUL.PRM_EHa_ffiyat = NULA(dt1, 0, "a_ffiyat")

        'KUL.PRM_belgeuser = Trim(Str(Val(VT_BILGI("tbl_user", "a_kod", "a_id", Trim(NULA(dt1, 0, "a_bbkod"))))))

        'KUL.PERSONEL_ID = NULN(dt1, 0, "a_perid")
        'KUL.PERSONEL_KOD = VT_BILGI(KUL.tfrm & "personel", "a_id", "a_kod", NULN(dt1, 0, "a_perid"))
        'KUL.PERSONEL_ADI = VT_BILGI(KUL.tfrm & "personel", "a_id", "a_adi", NULN(dt1, 0, "a_perid"))

        'KUL.ID_SUBE = NULN(dt1, 0, "a_sube_id")
        'KUL.PRM_EHa_degsube = NULA(dt1, 0, "a_degsube")



        If Val(Parametre(-1, "059")) = 0 Then

            KURUSHASSASIYET = 2

        Else
            KURUSHASSASIYET = Val(Parametre(-1, "059"))
        End If



        YETKISEMAKULLANIM = Parametre(-1, "129")
        If YETKISEMAKULLANIM <> "1" Then
            YETKISEMAKULLANIM = "0"
        End If


        DEPO = Parametre(-1, "133")
        If DEPO = "" Or DEPO = "0" Then
            KONTROLLUDEPOID = 0
            PARAMETREKONTROL = 0
        Else
            SQ = "SELECT a_id,a_adi,a_giris,a_cikis FROM " & KUL.tfrm & "ambar WHERE a_kod='" & DEPO & "'"
            DT2 = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            KONTROLLUDEPOID = NULN(DT2, 0, "a_id")
            KONTROLLUDEPOADI = NULA(DT2, 0, "a_adi")
            KONTROLLUDEPOGIRIS = NULN(DT2, 0, "a_giris")
            KONTROLLUDEPOCIKIS = NULN(DT2, 0, "a_cikis")
            PARAMETREKONTROL = 1
        End If
        SQ = ""
        SQ = "SELECT a_kod,a_adi FROM " & KUL.tfrm & "sube WHERE a_id=" & NULA(DT, 0, "SUBEID") & ""
        DT2 = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
        SUBEADI = NULA(DT2, 0, "a_adi")
        If SUBEADI = Nothing Then SUBEADI = " "
        SUBEKODU = NULA(DT2, 0, "a_kod")
        If SUBEKODU = Nothing Then SUBEKODU = " "




        SQ = ""
        SQ = "SELECT a_id,a_adi,a_giris,a_cikis FROM " & KUL.tfrm & "ambar WHERE a_id=" & NULN(DT, 0, "DEPOID") & " And a_durum=1"
        DT2 = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
        DEPOADI = NULA(DT2, 0, "a_adi")
        GIRIS = NULN(DT2, 0, "a_giris")
        CIKIS = NULN(DT2, 0, "a_cikis")
        If DEPOADI = Nothing Then DEPOADI = " "
        If DT.Rows.Count > 0 Then

            Dim VERI(0) As WS_GIRIS
            SQ = ""
            SQ = "SELECT TOP 1 * FROM tbl_mob_kprm"
            If SQL_KOS(SQ, False) = False Then

                VERI(0) = New WS_GIRIS
                VERI(0).DURUM = NUMARATOR.AYARLARTABLOSU
                VERI(0).GRUPTUR = NULN(DT, 0, "KULLANICIGRUP")
                Return VERI

            End If
            SQ = ""
            SQ = "SELECT TOP 1 * FROM tbl_mob_mprm"
            If SQL_KOS(SQ, False) = False Then

                VERI(0) = New WS_GIRIS
                VERI(0).DURUM = NUMARATOR.AYARLARTABLOSU
                VERI(0).GRUPTUR = NULN(DT, 0, "KULLANICIGRUP")
                Return VERI
            End If
            SQ = ""
            SQ = "SELECT TOP 1 * FROM tbl_mob_menu"
            If SQL_KOS(SQ, False) = False Then

                VERI(0) = New WS_GIRIS
                VERI(0).DURUM = NUMARATOR.AYARLARTABLOSU
                VERI(0).GRUPTUR = NULN(DT, 0, "KULLANICIGRUP")
                Return VERI
            End If



            SQ = ""
            SQ = "SELECT a_Name,a_Text FROM tbl_ozellikler WHERE a_userid='" & KUL.KOD & "'"
            DT3 = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)




            For X = 0 To DT3.Rows.Count - 1

                If NULA(DT3, X, "a_Name") = "L_OZ_FI_1" Then

                    TEXT_OFIYAT1 = NULA(DT3, X, "a_Text")

                ElseIf NULA(DT3, X, "a_Name") = "L_OZ_FI_2" Then

                    TEXT_OFIYAT2 = NULA(DT3, X, "a_Text")
                ElseIf NULA(DT3, X, "a_Name") = "L_OZ_FI_3" Then

                    TEXT_OFIYAT3 = NULA(DT3, X, "a_Text")
                ElseIf NULA(DT3, X, "a_Name") = "L_OZ_FI_4" Then

                    TEXT_OFIYAT4 = NULA(DT3, X, "a_Text")
                ElseIf NULA(DT3, X, "a_Name") = "L_OZ_FI_5" Then

                    TEXT_OFIYAT5 = NULA(DT3, X, "a_Text")
                ElseIf NULA(DT3, X, "a_Name") = "L_OZ_FI_6" Then

                    TEXT_OFIYAT6 = NULA(DT3, X, "a_Text")
                ElseIf NULA(DT3, X, "a_Name") = "L_OZ_FI_6" Then

                    TEXT_OFIYAT6 = NULA(DT3, X, "a_Text")

                ElseIf NULA(DT3, X, "a_Name") = "L_SONM" Then

                    TEXT_SMALIYET = NULA(DT3, X, "a_Text")
                ElseIf NULA(DT3, X, "a_Name") = "L_YAORT" Then

                    TEXT_OMALIYET = NULA(DT3, X, "a_Text")

                ElseIf NULA(DT3, X, "a_Name") = "GroupBox6" Then

                    TEXT_STOKKARTFIYAT = NULA(DT3, X, "a_Text")

                ElseIf NULA(DT3, X, "a_Name") = "GroupBox14" Then

                    TEXT_STOKKARTOZELFIYAT = NULA(DT3, X, "a_Text")

                ElseIf NULA(DT3, X, "a_Name") = "GroupBox16" Then

                    TEXT_MALIYETLER = NULA(DT3, X, "a_Text")
                ElseIf NULA(DT3, X, "a_Name") = "L_AL_FIYAT" Then

                    TEXT_AFIYAT = NULA(DT3, X, "a_Text")
                ElseIf NULA(DT3, X, "a_Name") = "L_SAT_FIYAT" Then

                    TEXT_SFIYAT = NULA(DT3, X, "a_Text")




                End If


            Next










            VERI(0) = New WS_GIRIS
            VERI(0).DURUM = NUMARATOR.BAŞARILI
            VERI(0).KULKOD = NULA(DT, 0, "KULKODU")
            VERI(0).KULAD = NULA(DT, 0, "KULADI")
            VERI(0).DEPOID = NULN(DT, 0, "DEPOID")
            VERI(0).DEPOADI = DEPOADI
            VERI(0).GIRIS = GIRIS
            VERI(0).CIKIS = CIKIS
            VERI(0).KASAID = NULN(DT, 0, "KASAID")
            VERI(0).KASAADI = KASAADI
            VERI(0).KASAKOD = KASAKOD
            VERI(0).KASASUBE = NULA(DT, 0, "KASASUBE")
            VERI(0).BANKAID = NULN(DT, 0, "BANKAID")
            VERI(0).BANKAADI = BANKAADI
            VERI(0).BANKAKOD = BANKAKOD
            VERI(0).BANKASUBE = NULA(DT, 0, "BANKASUBE")
            VERI(0).SUBEID = NULN(DT, 0, "SUBEID")
            VERI(0).SUBEADI = SUBEADI
            VERI(0).SUBEKODU = SUBEKODU
            VERI(0).USERID = NULN(DT, 0, "ID")
            VERI(0).GRUPTUR = NULN(DT, 0, "KULLANICIGRUP")
            VERI(0).DEPOYETKI = NULA(DT, 0, "DEPOYETKI")
            VERI(0).SUBEYETKI = NULA(DT, 0, "SUBEYETKI")
            VERI(0).KASAYETKI = NULA(DT, 0, "KASAYETKI")
            VERI(0).BANKAYETKI = NULA(DT, 0, "BANKAYETKI")
            VERI(0).FFIYATDEGIS = NULA(DT, 0, "FFIYATDEGIS")
            VERI(0).IFIYATDEGIS = NULA(DT, 0, "IFIYATDEGIS")
            VERI(0).KONTROLLUDEPOID = KONTROLLUDEPOID
            VERI(0).KONTROLLUDEPOADI = KONTROLLUDEPOADI
            VERI(0).KONTROLLUDEPOGIRIS = KONTROLLUDEPOGIRIS
            VERI(0).KONTROLLUDEPOCIKIS = KONTROLLUDEPOCIKIS
            VERI(0).PARAMETRE = PARAMETREKONTROL
            VERI(0).FIRMA = initialcatalog
            VERI(0).YETKISEMAKULLANIM = YETKISEMAKULLANIM
            VERI(0).KURUSHASSASIYET = KURUSHASSASIYET
            VERI(0).TEXT_SFIYAT = TEXT_SFIYAT
            VERI(0).TEXT_AFIYAT = TEXT_AFIYAT
            VERI(0).TEXT_OFIYAT1 = TEXT_OFIYAT1
            VERI(0).TEXT_OFIYAT2 = TEXT_OFIYAT2
            VERI(0).TEXT_OFIYAT3 = TEXT_OFIYAT3
            VERI(0).TEXT_OFIYAT4 = TEXT_OFIYAT4
            VERI(0).TEXT_OFIYAT5 = TEXT_OFIYAT5
            VERI(0).TEXT_OFIYAT6 = TEXT_OFIYAT6
            VERI(0).TEXT_SMALIYET = TEXT_SMALIYET
            VERI(0).TEXT_OMALIYET = TEXT_OMALIYET
            VERI(0).TEXT_STOKKARTFIYAT = TEXT_STOKKARTFIYAT
            VERI(0).TEXT_STOKKARTOZELFIYAT = TEXT_STOKKARTOZELFIYAT
            VERI(0).TEXT_MALIYETLER = TEXT_MALIYETLER

            Return VERI
        Else
            Dim VERI(0) As WS_GIRIS
            VERI(0) = New WS_GIRIS
            VERI(0).DURUM = NUMARATOR.HATA
            VERI(0).KULKOD = " "
            VERI(0).KULAD = " "
            VERI(0).DEPOID = 0
            VERI(0).GIRIS = 0
            VERI(0).CIKIS = 0
            VERI(0).KASAID = 0
            VERI(0).KASAADI = " "
            VERI(0).KASAKOD = " "
            VERI(0).KASASUBE = " "
            VERI(0).BANKAID = 0
            VERI(0).BANKAADI = " "
            VERI(0).BANKAKOD = " "
            VERI(0).BANKASUBE = " "
            VERI(0).SUBEID = 0
            VERI(0).USERID = 0
            VERI(0).GRUPTUR = 0
            VERI(0).DEPOYETKI = 0
            VERI(0).SUBEYETKI = 0
            VERI(0).KASAYETKI = 0
            VERI(0).BANKAYETKI = 0
            VERI(0).KONTROLLUDEPOID = 0
            VERI(0).KONTROLLUDEPOADI = " "
            VERI(0).KONTROLLUDEPOGIRIS = 0
            VERI(0).KONTROLLUDEPOCIKIS = 0
            VERI(0).PARAMETRE = 0
            VERI(0).FIRMA = " "
            VERI(0).FFIYATDEGIS = " "
            VERI(0).IFIYATDEGIS = " "
            VERI(0).YETKISEMAKULLANIM = " "
            VERI(0).KURUSHASSASIYET = 0
            VERI(0).TEXT_SFIYAT = " "
            VERI(0).TEXT_AFIYAT = " "
            VERI(0).TEXT_OFIYAT1 = " "
            VERI(0).TEXT_OFIYAT2 = " "
            VERI(0).TEXT_OFIYAT3 = " "
            VERI(0).TEXT_OFIYAT4 = " "
            VERI(0).TEXT_OFIYAT5 = " "
            VERI(0).TEXT_OFIYAT6 = " "
            VERI(0).TEXT_SMALIYET = " "
            VERI(0).TEXT_OMALIYET = " "
            VERI(0).TEXT_STOKKARTFIYAT = " "
            VERI(0).TEXT_STOKKARTOZELFIYAT = " "
            VERI(0).TEXT_MALIYETLER = " "

            Return VERI
        End If







    End Function
#End Region
#Region "MB/Belge Listeleme"
    Public Class WS_BELGE
        Public DURUM As Long
        Public FISID As Long
        Public BELGENO As String
        Public FISTURID As Long
        Public FISTUR As String
        Public FISTARIH As DateTime
        Public FISSAAT As DateTime
        Public DEPOADI As String
        Public CARIADI As String
        Public DEPOID As Long
        Public USERID As Integer
        Public GDEPO As String
        Public CDEPO As String
        Public SUBEADI As String

    End Class

    ''' <summary>
    ''' Gönderilen belge türü,tarih,limit,kullanıcıgrupıd ve kullanıcıId ya göre belgerli listeletiyoruz.
    ''' </summary>
    ''' <param name="TUR">İşlem Türü</param>
    ''' <param name="GUNLIMIT">Son Kaç Günün Fişleri Listenecek</param>
    ''' <param name="LIMIT">Sıralama Limiti</param>
    ''' <param name="KULLANICIGRUPID">Kullanıcı Grup ID</param>
    ''' <param name="KULLANICIID">Kullanıcı ID</param>
    ''' <returns></returns>
    <WebMethod()>
    Public Function MB_BelgeListele(ByVal VERSION As String, ByVal TUR As String, ByVal GUNLIMIT As Integer, ByVal LIMIT As Long, ByVal KULLANICIGRUPID As Long, ByVal KULLANICIID As Long) As WS_BELGE()
        Dim SQ As String
        Dim DT As New DataTable
        Dim TARIH_1 As String
        Dim TARIH_2 As String


        TARIH_1 = Date.Now.AddDays(-GUNLIMIT).ToString("yyyy-MM-dd")
        TARIH_2 = Date.Today.ToString("yyyy-MM-dd")


        B_SQL = connstr

        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()

        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"
            Dim VERI(0) As WS_BELGE
            VERI(0) = New WS_BELGE
            VERI(0).DURUM = NUMARATOR.APKVERSIYONESKI
            VERI(0).BELGENO = " "
            VERI(0).FISID = 0
            VERI(0).FISTARIH = Date.Now
            VERI(0).FISSAAT = DateTime.Now
            VERI(0).DEPOID = 0
            VERI(0).DEPOADI = " "
            VERI(0).FISTUR = "0"
            VERI(0).GDEPO = "0"
            VERI(0).CDEPO = "0"
            VERI(0).CARIADI = "0"
            VERI(0).SUBEADI = "0"
            VERI(0).USERID = KULLANICIID
            Return VERI

#End Region

        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

#Region "WS HATA"
            Dim VERI(0) As WS_BELGE
            VERI(0) = New WS_BELGE
            VERI(0).DURUM = NUMARATOR.WSVERSIYONESKI
            VERI(0).BELGENO = " "
            VERI(0).FISID = 0
            VERI(0).FISTARIH = Date.Now
            VERI(0).FISSAAT = DateTime.Now
            VERI(0).DEPOID = 0
            VERI(0).DEPOADI = " "
            VERI(0).FISTUR = "0"
            VERI(0).GDEPO = "0"
            VERI(0).CDEPO = "0"
            VERI(0).CARIADI = "0"
            VERI(0).SUBEADI = "0"
            VERI(0).USERID = KULLANICIID
            Return VERI

#End Region



        End If


        '// KABUL İŞLEMİ //' 

        If TUR = "kabul" Then
            Dim KullaniciKosul As String = ""
            Dim LimitSayi As Long = 0
            Dim LimitKosul As String = ""
            LimitSayi = LIMIT
            If LimitSayi <> 0 Then
                LimitKosul = "TOP " & LimitSayi & " "
            Else
                LimitKosul = ""
            End If
            If KULLANICIGRUPID <> 1 Then
                KullaniciKosul = " AND SGMAS.a_cuser = " & KULLANICIID & " "
            End If

            SQ = " SELECT " + LimitKosul + " SGMAS.a_id AS fisId, SGMAS.a_tur AS fisTurId,SGMAS.a_bno  as BELGENO,CONVERT(VARCHAR, SGMAS.a_cdate, 104) AS fisTarih, CONVERT(VARCHAR, SGMAS.a_ctime, 8) AS fisSaat,ISNULL (SUBE.a_adi,'Şube seçilmemiş.') as subeAdi,
        CASE SGMAS.a_tur WHEN 17 THEN 'FT' WHEN 43 THEN 'İR' WHEN 71 THEN 'FŞ' END AS fisTur, DEPO.a_adi AS depoAdi, CARI.a_adi AS cariAdi "
            SQ = SQ & " FROM " & KUL.tper & "stkgmas AS SGMAS "
            SQ = SQ & " LEFT JOIN " & KUL.tfrm & "cari AS CARI ON CARI.a_id = SGMAS.a_cari_id LEFT JOIN " & KUL.tfrm & "ambar AS DEPO ON DEPO.a_id = SGMAS.a_ambar_id"
            SQ = SQ & " LEFT JOIN " & KUL.tfrm & "sube AS SUBE ON SUBE.a_id = SGMAS.a_sube_id  "
            SQ = SQ & " WHERE SGMAS.a_tur IN (17, 43, 71) AND SGMAS.a_tarih BETWEEN '" + TARIH_1 + "' AND '" + TARIH_2 + "' " + KullaniciKosul + "  AND a_kesin = 0  ORDER BY CONVERT(NVARCHAR(25),SGMAS.a_cdate,23)+' '+CONVERT(NVARCHAR(25),SGMAS.a_ctime,108)  DESC"

            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            If DT.Rows.Count > 0 Then
                Dim VERI(DT.Rows.Count - 1) As WS_BELGE

                For X = 0 To DT.Rows.Count - 1
                    VERI(X) = New WS_BELGE
                    VERI(X).DURUM = NUMARATOR.BAŞARILI
                    VERI(X).FISID = NULN(DT, X, "fisID")
                    VERI(X).FISTURID = NULN(DT, X, "fisTurId")
                    VERI(X).FISTARIH = NULA(DT, X, "fisTarih")
                    VERI(X).FISSAAT = NULA(DT, X, "fisSaat")
                    VERI(X).FISTUR = NULA(DT, X, "fisTur")
                    VERI(X).DEPOADI = NULA(DT, X, "depoAdi")
                    VERI(X).CARIADI = NULA(DT, X, "cariAdi")
                    VERI(X).SUBEADI = NULA(DT, X, "subeAdi")
                    VERI(X).USERID = KULLANICIID
                    VERI(X).GDEPO = "0"
                    VERI(X).CDEPO = "0"
                    VERI(X).BELGENO = NULA(DT, X, "BELGENO")
                    If VERI(X).BELGENO = "" Then
                        VERI(X).BELGENO = " "
                    End If

                Next
                Return VERI
            Else
                Dim VERI(0) As WS_BELGE
                VERI(0) = New WS_BELGE
                VERI(0).DURUM = NUMARATOR.HATA
                VERI(0).BELGENO = " "
                VERI(0).FISID = 0
                VERI(0).FISTARIH = Date.Now
                VERI(0).FISSAAT = DateTime.Now
                VERI(0).DEPOID = 0
                VERI(0).DEPOADI = " "
                VERI(0).FISTUR = "0"
                VERI(0).GDEPO = "0"
                VERI(0).CDEPO = "0"
                VERI(0).CARIADI = "0"
                VERI(0).SUBEADI = "0"
                VERI(0).USERID = KULLANICIID
                Return VERI
            End If


        End If

        '// SAYIM İŞLEMİ //'

        If TUR = "sayim" Then
            Dim LimitSayi As Long = 0
            Dim LimitKosul As String = ""
            Dim KullaniciKosul As String = ""
            LimitSayi = LIMIT
            If LimitSayi <> 0 Then
                LimitKosul = "TOP " & LimitSayi & " "
            Else
                LimitKosul = ""
            End If
            If KULLANICIGRUPID <> 1 Then
                KullaniciKosul = " AND FIS.a_cuser = " & KULLANICIID & " "
            End If
            SQ = ""
            SQ = " SELECT " + LimitKosul + " FIS.a_bno AS belgeno,FIS.a_id AS fisId, CONVERT(varchar, FIS.a_cdate, 104) AS fisTarih, CONVERT(varchar, FIS.a_ctime, 8) AS fisSaat,
            DEPO.a_id AS depoId, DEPO.a_adi AS depoAdi "
            SQ = SQ & " FROM " & KUL.tper & "stsaymas AS FIS  "
            SQ = SQ & " INNER JOIN " & KUL.tfrm & "ambar AS DEPO ON DEPO.a_id = FIS.a_depo_id"
            SQ = SQ & " WHERE FIS.a_kesin = 0 AND FIS.a_tarih BETWEEN '" + TARIH_1 + "' AND '" + TARIH_2 + "'" + KullaniciKosul + " ORDER BY CONVERT(NVARCHAR(25),FIS.a_cdate,23)+' '+CONVERT(NVARCHAR(25),FIS.a_ctime,108)  DESC"

            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            If DT.Rows.Count > 0 Then
                Dim VERI(DT.Rows.Count - 1) As WS_BELGE
                For X = 0 To DT.Rows.Count - 1
                    VERI(X) = New WS_BELGE
                    VERI(X).DURUM = NUMARATOR.BAŞARILI
                    VERI(X).BELGENO = NULA(DT, X, "belgeno")
                    If VERI(X).BELGENO = "" Then
                        VERI(X).BELGENO = " "
                    End If
                    VERI(X).FISID = NULN(DT, X, "fisID")
                    VERI(X).FISTARIH = NULA(DT, X, "fisTarih")
                    VERI(X).FISSAAT = NULA(DT, X, "fisSaat")
                    VERI(X).DEPOID = NULA(DT, X, "depoId")
                    VERI(X).DEPOADI = NULA(DT, X, "depoAdi")
                    VERI(X).FISTUR = "0"
                    VERI(X).GDEPO = "0"
                    VERI(X).CDEPO = "0"
                    VERI(X).CARIADI = "0"
                    VERI(X).SUBEADI = "0"
                    VERI(X).USERID = KULLANICIID
                Next
                Return VERI
            Else
                Dim VERI(0) As WS_BELGE
                VERI(0) = New WS_BELGE
                VERI(0).DURUM = NUMARATOR.HATA
                VERI(0).BELGENO = " "
                VERI(0).FISID = 0
                VERI(0).FISTARIH = Date.Now
                VERI(0).FISSAAT = DateTime.Now
                VERI(0).DEPOID = 0
                VERI(0).DEPOADI = " "
                VERI(0).FISTUR = "0"
                VERI(0).GDEPO = "0"
                VERI(0).CDEPO = "0"
                VERI(0).CARIADI = "0"
                VERI(0).SUBEADI = "0"
                Return VERI
            End If

        End If

        '// DEPO SEVK İŞLEMİ //'

        If TUR = "depoSevk" Then
            Dim KullaniciKosul As String = ""
            Dim LimitSayi As Long = 0
            Dim LimitKosul As String = ""
            LimitSayi = LIMIT
            If LimitSayi <> 0 Then
                LimitKosul = "TOP " & LimitSayi & " "
            Else
                LimitKosul = ""
            End If
            If KULLANICIGRUPID <> 1 Then
                KullaniciKosul = " AND SHMAS.a_cuser = " & KULLANICIID & " "
            End If
            SQ = ""
            SQ = " SELECT " + LimitKosul + " SHMAS.a_id AS fisId,SHMAS.a_bno as BELGENO, CONVERT(VARCHAR, SHMAS.a_cdate, 104) AS fisTarih, CONVERT(VARCHAR, SHMAS.a_ctime, 8) AS fisSaat, GDEPO.a_adi AS girisDepo, CDEPO.a_adi AS cikisDepo"
            SQ = SQ & " FROM " & KUL.tper & "stkhmas AS SHMAS  "
            SQ = SQ & " INNER JOIN " & KUL.tfrm & "ambar AS CDEPO ON CDEPO.a_id = SHMAS.a_ambar_id  "
            SQ = SQ & " INNER JOIN " & KUL.tfrm & "ambar AS GDEPO ON GDEPO.a_id = SHMAS.a_oplan_id "
            SQ = SQ & " WHERE a_tur = 49 AND SHMAS.a_tarih BETWEEN '" + TARIH_1 + "' AND '" + TARIH_2 + "' AND SHMAS.a_bno <> '@ISTEK@' " + KullaniciKosul + " ORDER BY CONVERT(NVARCHAR(25),SHMAS.a_cdate,23)+' '+CONVERT(NVARCHAR(25),SHMAS.a_ctime,108)  DESC"

            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            If DT.Rows.Count > 0 Then
                Dim VERI(DT.Rows.Count - 1) As WS_BELGE
                For X = 0 To DT.Rows.Count - 1
                    VERI(X) = New WS_BELGE
                    VERI(X).DURUM = NUMARATOR.BAŞARILI
                    VERI(X).FISID = NULN(DT, X, "fisID")
                    VERI(X).BELGENO = NULA(DT, X, "BELGENO")
                    If VERI(X).BELGENO = "" Then
                        VERI(X).BELGENO = " "
                    End If
                    VERI(X).FISTARIH = NULA(DT, X, "fisTarih")
                    VERI(X).FISSAAT = NULA(DT, X, "fisSaat")
                    VERI(X).GDEPO = NULA(DT, X, "girisDepo")
                    VERI(X).CDEPO = NULA(DT, X, "cikisDepo")
                    VERI(X).FISTUR = "49"
                    VERI(X).DEPOADI = "0"
                    VERI(X).CARIADI = "0"
                    VERI(X).SUBEADI = "0"
                    VERI(X).USERID = KULLANICIID

                Next
                Return VERI
            Else

                Dim VERI(0) As WS_BELGE
                VERI(0) = New WS_BELGE
                VERI(0).DURUM = NUMARATOR.HATA
                VERI(0).BELGENO = " "
                VERI(0).FISID = 0
                VERI(0).FISTARIH = Date.Now
                VERI(0).FISSAAT = DateTime.Now
                VERI(0).DEPOID = 0
                VERI(0).DEPOADI = " "
                VERI(0).FISTUR = "0"
                VERI(0).GDEPO = "0"
                VERI(0).CDEPO = "0"
                VERI(0).CARIADI = "0"
                VERI(0).SUBEADI = "0"
                Return VERI
            End If

        End If

        '// DEPO SEVK İSTEK İŞLEMİ //'

        If TUR = "depoSevkIstek" Or TUR = "depoSevkIstekYonetimi" Then
            Dim limitSayi As Long = 0
            Dim limitKosul As String = ""
            Dim onayDurum As Long = 0
            If TUR = "DEPOSEVKISTEK" Then
                onayDurum = 0
            ElseIf TUR = "deposevkistekyonetimi" Then
                onayDurum = 1
            End If
            limitSayi = LIMIT
            If limitSayi <> 0 Then
                limitKosul = "TOP " & limitSayi & " "
            Else
                limitKosul = ""
            End If
            SQ = ""
            SQ = " SELECT " + limitKosul + " DIMAS.a_id AS fisId, CONVERT(VARCHAR, DIMAS.a_cdate, 104) AS fisTarih,  CONVERT(VARCHAR, DIMAS.a_ctime, 104) AS fisTarih, DIMAS.a_bno AS fisBelgeNo, GDEPO.a_adi AS girisDepo, CDEPO.a_adi AS cikisDepo, SUBE.a_adi AS subeAdi "
            SQ = SQ & " FROM " & KUL.tfrm & "01_depistmas AS DIMAS   "
            SQ = SQ & " INNER JOIN " & KUL.tfrm & "ambar AS CDEPO ON CDEPO.a_id = DIMAS.a_cdepo_id  "
            SQ = SQ & " INNER JOIN " & KUL.tfrm & "ambar AS GDEPO ON GDEPO.a_id = DIMAS.a_gdepo_id  "
            SQ = SQ & " INNER JOIN " & KUL.tfrm & "sube AS SUBE ON SUBE.a_id = DIMAS.a_sube_id  "
            SQ = SQ & " WHERE DIMAS.a_kulonay =" & onayDurum & " AND DIMAS.a_tarih BETWEEN '" + TARIH_1 + "' AND '" + TARIH_2 + "' ORDER BY CONVERT(NVARCHAR(25),DIMAS.a_cdate,23)+' '+CONVERT(NVARCHAR(25), DIMAS.a_ctime,108) DESC"

            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)

            Dim VERI(DT.Rows.Count - 1) As WS_BELGE
            For X = 0 To DT.Rows.Count - 1
                VERI(X) = New WS_BELGE
                VERI(X).FISID = NULN(DT, X, "fisID")
                VERI(X).FISTARIH = NULA(DT, X, "fisTarih")
                VERI(X).BELGENO = NULA(DT, X, "fisBelgeNo")
                If VERI(X).BELGENO = "" Then
                    VERI(X).BELGENO = " "
                End If
                VERI(X).GDEPO = NULA(DT, X, "girisDepo")
                VERI(X).CDEPO = NULA(DT, X, "cikisDepo")
                VERI(X).SUBEADI = NULA(DT, X, "subeAdi")
                VERI(X).FISTUR = "89"
            Next
            Return VERI


        End If


        '// SATIŞ İŞLEMİ //'

        If TUR = "satis" Then
            Dim SubeId As String
            Dim KullaniciKosul As String = ""
            Dim LimitSayi As Long = 0
            Dim LimitKosul As String = ""
            LimitSayi = LIMIT
            If LimitSayi <> 0 Then
                LimitKosul = "TOP " & LimitSayi & " "
            Else
                LimitKosul = ""
            End If
            If KULLANICIGRUPID <> 1 Then
                KullaniciKosul = " AND SCMAS.a_cuser = " & KULLANICIID & " "
            End If
            SQ = ""
            SQ = " SELECT " + LimitKosul + " SCMAS.a_bno AS BELGENO ,SCMAS.a_id AS fisId, SCMAS.a_tur AS fisTurId, CONVERT(VARCHAR, SCMAS.a_cdate, 104) AS fisTarih, CONVERT(VARCHAR, SCMAS.a_ctime, 8) AS fisSaat,ISNULL (SUBE.a_adi,'Şube seçilmemiş.') as subeAdi,"
            SQ = SQ & " CASE SCMAS.a_tur WHEN 11 THEN 'FT' WHEN 41 THEN 'İR' WHEN 70 THEN 'FŞ' END AS fisTur, DEPO.a_adi AS depoAdi, CARI.a_adi AS cariAdi  "
            SQ = SQ & " FROM " & KUL.tper & "stkcmas AS SCMAS  "
            SQ = SQ & " LEFT JOIN " & KUL.tfrm & "cari AS CARI ON CARI.a_id = SCMAS.a_cari_id  "
            SQ = SQ & " LEFT JOIN " & KUL.tfrm & "ambar AS DEPO ON DEPO.a_id = SCMAS.a_ambar_id"
            SQ = SQ & " LEFT JOIN " & KUL.tfrm & "sube AS SUBE ON SUBE.a_id = SCMAS.a_sube_id  "
            SQ = SQ & " WHERE SCMAS.a_tur IN (11,41,70) AND SCMAS.a_tarih BETWEEN '" + TARIH_1 + "' AND '" + TARIH_2 + "' " + KullaniciKosul + " AND a_kesin = 0 ORDER BY  CONVERT(NVARCHAR(25), SCMAS.a_cdate,23)+' '+CONVERT(NVARCHAR(25), SCMAS.a_ctime,108) DESC"

            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            If DT.Rows.Count > 0 Then
                Dim VERI(DT.Rows.Count - 1) As WS_BELGE
                For X = 0 To DT.Rows.Count - 1
                    VERI(X) = New WS_BELGE
                    VERI(X).DURUM = NUMARATOR.BAŞARILI
                    VERI(X).FISID = NULN(DT, X, "fisID")
                    VERI(X).FISTURID = NULN(DT, X, "fisTurId")
                    VERI(X).FISTARIH = NULA(DT, X, "fisTarih")
                    VERI(X).FISSAAT = NULA(DT, X, "fisSaat")
                    VERI(X).FISTUR = NULA(DT, X, "fisTur")
                    VERI(X).DEPOADI = NULA(DT, X, "depoAdi")
                    If VERI(X).DEPOADI = Nothing Then
                        VERI(X).DEPOADI = " "
                    End If
                    VERI(X).CARIADI = NULA(DT, X, "cariAdi")
                    If VERI(X).CARIADI = Nothing Then
                        VERI(X).CARIADI = " "
                    End If
                    VERI(X).GDEPO = "0"
                    VERI(X).CDEPO = "0"
                    VERI(X).SUBEADI = NULA(DT, X, "subeAdi")
                    VERI(X).BELGENO = NULA(DT, X, "BELGENO")
                    If VERI(X).BELGENO = Nothing Then
                        VERI(X).BELGENO = " "
                    End If
                    VERI(X).USERID = KULLANICIID
                Next
                Return VERI
            Else
                Dim VERI(0) As WS_BELGE
                VERI(0) = New WS_BELGE
                VERI(0).DURUM = NUMARATOR.HATA
                VERI(0).BELGENO = " "
                VERI(0).FISID = 0
                VERI(0).FISTARIH = Date.Now
                VERI(0).FISSAAT = DateTime.Now
                VERI(0).DEPOID = 0
                VERI(0).DEPOADI = " "
                VERI(0).FISTUR = "0"
                VERI(0).GDEPO = "0"
                VERI(0).CDEPO = "0"
                VERI(0).CARIADI = "0"
                VERI(0).SUBEADI = "0"
                Return VERI
            End If
        End If
        '// KABUL İADE İŞLEMİ //'

        If TUR = "kabulIade" Then
            Dim SubeId As Long
            Dim KullaniciKosul As String = ""
            Dim LimitSayi As Long = 0
            Dim LimitKosul As String = ""
            Dim dt1 As DataTable
            LimitSayi = LIMIT
            If LimitSayi <> 0 Then
                LimitKosul = "TOP " & LimitSayi & " "
            Else
                LimitKosul = ""
            End If
            If KULLANICIGRUPID <> 1 Then
                KullaniciKosul = " AND SGMAS.a_cuser = " & KULLANICIID & " "
            End If
            SQ = ""
            SQ = " SELECT " + LimitKosul + " SGMAS.a_id AS fisId,SGMAS.a_sube_id as SUBEID,SGMAS.a_bno AS BELGENO,DEPO.a_id as DEPOID , SGMAS.a_tur AS fisTurId, CONVERT(VARCHAR, SGMAS.a_cdate, 104) AS fisTarih, CONVERT(VARCHAR, SGMAS.a_ctime, 8) AS fisSaat,ISNULL(SUBE.a_adi,'Şube seçilmemiş.') as subeAdi,"
            SQ = SQ & " CASE SGMAS.a_tur WHEN 20 THEN 'FT' WHEN 69 THEN 'İR' END AS fisTur, ISNULL(DEPO.a_adi,'Depo seçilmemiş') AS depoAdi, CARI.a_adi AS cariAdi  "
            SQ = SQ & " FROM " & KUL.tper & "stkgmas AS SGMAS  "
            SQ = SQ & " left JOIN " & KUL.tfrm & "cari AS CARI ON CARI.a_id = SGMAS.a_cari_id  "
            SQ = SQ & " left JOIN " & KUL.tfrm & "ambar AS DEPO ON DEPO.a_id = SGMAS.a_ambar_id"
            SQ = SQ & " left JOIN " & KUL.tfrm & "sube AS SUBE ON SUBE.a_id = SGMAS.a_sube_id  "
            SQ = SQ & " WHERE SGMAS.a_tur IN (20, 69) AND SGMAS.a_tarih BETWEEN '" + TARIH_1 + "' AND '" + TARIH_2 + "' " + KullaniciKosul + " AND a_kesin = 0  ORDER BY CONVERT(NVARCHAR(25),SGMAS.a_cdate,23)+' '+CONVERT(NVARCHAR(25),SGMAS.a_ctime,108) DESC"

            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)

            If DT.Rows.Count > 0 Then
                Dim VERI(DT.Rows.Count - 1) As WS_BELGE
                For X = 0 To DT.Rows.Count - 1
                    VERI(X) = New WS_BELGE
                    VERI(X).DURUM = NUMARATOR.BAŞARILI
                    VERI(X).FISID = NULN(DT, X, "fisID")
                    VERI(X).FISTURID = NULN(DT, X, "fisTurId")
                    VERI(X).FISTARIH = NULA(DT, X, "fisTarih")
                    VERI(X).FISSAAT = NULA(DT, X, "fisSaat")
                    VERI(X).FISTUR = NULA(DT, X, "fisTur")
                    VERI(X).BELGENO = NULA(DT, X, "BELGENO")
                    If VERI(X).BELGENO = Nothing Then
                        VERI(X).BELGENO = " "
                    End If
                    VERI(X).DEPOADI = NULA(DT, X, "depoAdi")
                    If VERI(X).DEPOADI = Nothing Then
                        VERI(X).DEPOADI = " "
                    End If
                    VERI(X).DEPOID = NULA(DT, X, "DEPOID")
                    VERI(X).CARIADI = NULA(DT, X, "cariAdi")
                    If VERI(X).CARIADI = Nothing Then
                        VERI(X).CARIADI = " "
                    End If
                    VERI(X).GDEPO = "0"
                    VERI(X).CDEPO = "0"
                    VERI(X).SUBEADI = NULA(DT, X, "subeAdi")
                    If VERI(X).SUBEADI = Nothing Then
                        VERI(X).SUBEADI = " "
                    End If
                    VERI(X).USERID = KULLANICIID
                Next
                Return VERI
            Else
                Dim VERI(0) As WS_BELGE
                VERI(0) = New WS_BELGE
                VERI(0).DURUM = NUMARATOR.HATA
                VERI(0).BELGENO = " "
                VERI(0).FISID = 0
                VERI(0).FISTARIH = Date.Now
                VERI(0).FISSAAT = DateTime.Now
                VERI(0).DEPOID = 0
                VERI(0).DEPOADI = " "
                VERI(0).FISTUR = "0"
                VERI(0).GDEPO = "0"
                VERI(0).CDEPO = "0"
                VERI(0).CARIADI = "0"
                VERI(0).SUBEADI = "0"
                Return VERI
            End If
        End If
        '// SATIŞ İADE İŞLEMİ //'
        If TUR = "satisIade" Then
            Dim KullaniciKosul As String = ""
            Dim LimitSayi As Long = 0
            Dim LimitKosul As String = ""
            LimitSayi = LIMIT
            If LimitSayi <> 0 Then
                LimitKosul = "TOP " & LimitSayi & " "
            Else
                LimitKosul = ""
            End If
            If KULLANICIGRUPID <> 1 Then
                KullaniciKosul = " AND SCMAS.a_cuser = " & KULLANICIID & " "
            End If
            SQ = ""
            SQ = " SELECT " + LimitKosul + " SCMAS.a_id AS fisId, SCMAS.a_tur AS fisTurId,SCMAS.a_bno AS BELGENO,DEPO.a_id as DEPOID, CONVERT(VARCHAR, SCMAS.a_cdate, 104) AS fisTarih, CONVERT(VARCHAR, SCMAS.a_ctime, 8) AS fisSaat,ISNULL(SUBE.a_adi,'Şube seçilmemiş.') as subeAdi,"
            SQ = SQ & " CASE SCMAS.a_tur WHEN 13 THEN 'FT' WHEN 95 THEN 'İR' END AS fisTur,ISNULL( DEPO.a_adi,'Depo seçilmemiş.') AS depoAdi, CARI.a_adi AS cariAdi  "
            SQ = SQ & " FROM " & KUL.tper & "stkcmas AS SCMAS  "
            SQ = SQ & " LEFT JOIN " & KUL.tfrm & "cari AS CARI ON CARI.a_id = SCMAS.a_cari_id  "
            SQ = SQ & " LEFT JOIN " & KUL.tfrm & "ambar AS DEPO ON DEPO.a_id = SCMAS.a_ambar_id"
            SQ = SQ & " LEFT JOIN " & KUL.tfrm & "sube AS SUBE ON SUBE.a_id = SCMAS.a_sube_id  "
            SQ = SQ & " WHERE SCMAS.a_tur IN (13,95) AND SCMAS.a_tarih BETWEEN '" + TARIH_1 + "' AND '" + TARIH_2 + "' " + KullaniciKosul + " AND a_kesin = 0  ORDER BY CONVERT(NVARCHAR(25),SCMAS.a_cdate,23)+' '+CONVERT(NVARCHAR(25),SCMAS.a_ctime,108) DESC"

            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            If DT.Rows.Count > 0 Then
                Dim VERI(DT.Rows.Count - 1) As WS_BELGE
                For X = 0 To DT.Rows.Count - 1
                    VERI(X) = New WS_BELGE
                    VERI(X).DURUM = NUMARATOR.BAŞARILI
                    VERI(X).FISID = NULN(DT, X, "fisID")
                    VERI(X).FISTURID = NULN(DT, X, "fisTurId")
                    VERI(X).FISTARIH = NULA(DT, X, "fisTarih")
                    VERI(X).FISSAAT = NULA(DT, X, "fisSaat")
                    VERI(X).FISTUR = NULA(DT, X, "fisTur")
                    VERI(X).DEPOADI = NULA(DT, X, "depoAdi")
                    VERI(X).BELGENO = NULA(DT, X, "BELGENO")
                    If VERI(X).BELGENO = Nothing Then
                        VERI(X).BELGENO = " "
                    End If
                    VERI(X).DEPOID = NULA(DT, X, "DEPOID")
                    If VERI(X).DEPOADI = Nothing Then
                        VERI(X).DEPOADI = " "
                    End If
                    VERI(X).CARIADI = NULA(DT, X, "cariAdi")
                    If VERI(X).CARIADI = Nothing Then
                        VERI(X).CARIADI = " "
                    End If
                    VERI(X).GDEPO = "0"
                    VERI(X).CDEPO = "0"
                    VERI(X).SUBEADI = NULA(DT, X, "subeAdi")
                    If VERI(X).SUBEADI = Nothing Then
                        VERI(X).SUBEADI = " "
                    End If
                    VERI(X).USERID = KULLANICIID
                Next

                Return VERI
            Else
                Dim VERI(0) As WS_BELGE
                VERI(0) = New WS_BELGE
                VERI(0).DURUM = NUMARATOR.HATA
                VERI(0).BELGENO = " "
                VERI(0).FISID = 0
                VERI(0).FISTARIH = Date.Now
                VERI(0).FISSAAT = DateTime.Now
                VERI(0).DEPOID = 0
                VERI(0).DEPOADI = " "
                VERI(0).FISTUR = "0"
                VERI(0).GDEPO = "0"
                VERI(0).CDEPO = "0"
                VERI(0).CARIADI = "0"
                VERI(0).SUBEADI = "0"
                Return VERI
            End If

        End If
        If TUR = "alinansiparis" Then

            Dim KullaniciKosul As String = ""
            Dim LimitSayi As Long = 0
            Dim LimitKosul As String = ""
            LimitSayi = LIMIT
            If LimitSayi <> 0 Then
                LimitKosul = "TOP " & LimitSayi & " "
            Else
                LimitKosul = ""
            End If
            If KULLANICIGRUPID <> 1 Then
                KullaniciKosul = " AND SIP.a_cuser = " & KULLANICIID & " "
            End If
            SQ = ""
            SQ = " SELECT " + LimitKosul + " SIP.a_id AS fisId, SIP.a_tur AS fisTurId,SIP.a_bno AS BELGENO,DEPO.a_id as DEPOID, CONVERT(VARCHAR, SIP.a_cdate, 104) AS fisTarih, CONVERT(VARCHAR, SIP.a_ctime, 8) AS fisSaat,ISNULL(SUBE.a_adi,'Şube seçilmemiş.') as subeAdi,"
            SQ = SQ & " CASE SIP.a_tur WHEN 33 THEN 'FT'  END AS fisTur,ISNULL( DEPO.a_adi,'Depo seçilmemiş.') AS depoAdi, CARI.a_adi AS cariAdi  "
            SQ = SQ & " FROM " & KUL.tper & "sipmas AS SIP  "
            SQ = SQ & " LEFT JOIN " & KUL.tfrm & "cari AS CARI ON CARI.a_id = SIP.a_cari_id  "
            SQ = SQ & " LEFT JOIN " & KUL.tfrm & "ambar AS DEPO ON DEPO.a_id = SIP.a_ambar_id"
            SQ = SQ & " LEFT JOIN " & KUL.tfrm & "sube AS SUBE ON SUBE.a_id = SIP.a_sube_id  "
            SQ = SQ & " WHERE SIP.a_tur=33 AND SIP.a_tarih BETWEEN '" + TARIH_1 + "' AND '" + TARIH_2 + "' " + KullaniciKosul + " AND a_kesin = 0  ORDER BY CONVERT(NVARCHAR(25),SIP.a_cdate,23)+' '+CONVERT(NVARCHAR(25),SIP.a_ctime,108) DESC"

            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            If DT.Rows.Count > 0 Then
                Dim VERI(DT.Rows.Count - 1) As WS_BELGE
                For X = 0 To DT.Rows.Count - 1
                    VERI(X) = New WS_BELGE
                    VERI(X).DURUM = NUMARATOR.BAŞARILI
                    VERI(X).FISID = NULN(DT, X, "fisID")
                    VERI(X).FISTURID = NULN(DT, X, "fisTurId")
                    VERI(X).FISTARIH = NULA(DT, X, "fisTarih")
                    VERI(X).FISSAAT = NULA(DT, X, "fisSaat")
                    VERI(X).FISTUR = NULA(DT, X, "fisTur")
                    VERI(X).DEPOADI = NULA(DT, X, "depoAdi")
                    VERI(X).BELGENO = NULA(DT, X, "BELGENO")
                    If VERI(X).BELGENO = Nothing Then
                        VERI(X).BELGENO = " "
                    End If
                    VERI(X).DEPOID = NULA(DT, X, "DEPOID")
                    If VERI(X).DEPOADI = Nothing Then
                        VERI(X).DEPOADI = " "
                    End If
                    VERI(X).CARIADI = NULA(DT, X, "cariAdi")
                    If VERI(X).CARIADI = Nothing Then
                        VERI(X).CARIADI = " "
                    End If
                    VERI(X).GDEPO = "0"
                    VERI(X).CDEPO = "0"
                    VERI(X).SUBEADI = NULA(DT, X, "subeAdi")
                    If VERI(X).SUBEADI = Nothing Then
                        VERI(X).SUBEADI = " "
                    End If
                    VERI(X).USERID = KULLANICIID
                Next

                Return VERI
            Else
                Dim VERI(0) As WS_BELGE
                VERI(0) = New WS_BELGE
                VERI(0).DURUM = NUMARATOR.HATA
                VERI(0).BELGENO = " "
                VERI(0).FISID = 0
                VERI(0).FISTARIH = Date.Now
                VERI(0).FISSAAT = DateTime.Now
                VERI(0).DEPOID = 0
                VERI(0).DEPOADI = " "
                VERI(0).FISTUR = "0"
                VERI(0).GDEPO = "0"
                VERI(0).CDEPO = "0"
                VERI(0).CARIADI = "0"
                VERI(0).SUBEADI = "0"
                Return VERI
            End If




        End If

        If TUR = "verilensiparis" Then

            Dim KullaniciKosul As String = ""
            Dim LimitSayi As Long = 0
            Dim LimitKosul As String = ""
            LimitSayi = LIMIT
            If LimitSayi <> 0 Then
                LimitKosul = "TOP " & LimitSayi & " "
            Else
                LimitKosul = ""
            End If
            If KULLANICIGRUPID <> 1 Then
                KullaniciKosul = " AND SIP.a_cuser = " & KULLANICIID & " "
            End If
            SQ = ""
            SQ = " SELECT " + LimitKosul + " SIP.a_id AS fisId, SIP.a_tur AS fisTurId,SIP.a_bno AS BELGENO,DEPO.a_id as DEPOID, CONVERT(VARCHAR, SIP.a_cdate, 104) AS fisTarih, CONVERT(VARCHAR, SIP.a_ctime, 8) AS fisSaat,ISNULL(SUBE.a_adi,'Şube seçilmemiş.') as subeAdi,"
            SQ = SQ & " CASE SIP.a_tur WHEN 34 THEN 'FT'  END AS fisTur,ISNULL( DEPO.a_adi,'Depo seçilmemiş.') AS depoAdi, CARI.a_adi AS cariAdi  "
            SQ = SQ & " FROM " & KUL.tper & "sipmas AS SIP  "
            SQ = SQ & " LEFT JOIN " & KUL.tfrm & "cari AS CARI ON CARI.a_id = SIP.a_cari_id  "
            SQ = SQ & " LEFT JOIN " & KUL.tfrm & "ambar AS DEPO ON DEPO.a_id = SIP.a_ambar_id"
            SQ = SQ & " LEFT JOIN " & KUL.tfrm & "sube AS SUBE ON SUBE.a_id = SIP.a_sube_id  "
            SQ = SQ & " WHERE SIP.a_tur=34 AND SIP.a_tarih BETWEEN '" + TARIH_1 + "' AND '" + TARIH_2 + "' " + KullaniciKosul + " AND a_kesin = 0  ORDER BY CONVERT(NVARCHAR(25),SIP.a_cdate,23)+' '+CONVERT(NVARCHAR(25),SIP.a_ctime,108) DESC"

            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            If DT.Rows.Count > 0 Then
                Dim VERI(DT.Rows.Count - 1) As WS_BELGE
                For X = 0 To DT.Rows.Count - 1
                    VERI(X) = New WS_BELGE
                    VERI(X).DURUM = NUMARATOR.BAŞARILI
                    VERI(X).FISID = NULN(DT, X, "fisID")
                    VERI(X).FISTURID = NULN(DT, X, "fisTurId")
                    VERI(X).FISTARIH = NULA(DT, X, "fisTarih")
                    VERI(X).FISSAAT = NULA(DT, X, "fisSaat")
                    VERI(X).FISTUR = NULA(DT, X, "fisTur")
                    VERI(X).DEPOADI = NULA(DT, X, "depoAdi")
                    VERI(X).BELGENO = NULA(DT, X, "BELGENO")
                    If VERI(X).BELGENO = Nothing Then
                        VERI(X).BELGENO = " "
                    End If
                    VERI(X).DEPOID = NULA(DT, X, "DEPOID")
                    If VERI(X).DEPOADI = Nothing Then
                        VERI(X).DEPOADI = " "
                    End If
                    VERI(X).CARIADI = NULA(DT, X, "cariAdi")
                    If VERI(X).CARIADI = Nothing Then
                        VERI(X).CARIADI = " "
                    End If
                    VERI(X).GDEPO = "0"
                    VERI(X).CDEPO = "0"
                    VERI(X).SUBEADI = NULA(DT, X, "subeAdi")
                    If VERI(X).SUBEADI = Nothing Then
                        VERI(X).SUBEADI = " "
                    End If
                    VERI(X).USERID = KULLANICIID
                Next

                Return VERI
            Else
                Dim VERI(0) As WS_BELGE
                VERI(0) = New WS_BELGE
                VERI(0).DURUM = NUMARATOR.HATA
                VERI(0).BELGENO = " "
                VERI(0).FISID = 0
                VERI(0).FISTARIH = Date.Now
                VERI(0).FISSAAT = DateTime.Now
                VERI(0).DEPOID = 0
                VERI(0).DEPOADI = " "
                VERI(0).FISTUR = "0"
                VERI(0).GDEPO = "0"
                VERI(0).CDEPO = "0"
                VERI(0).CARIADI = "0"
                VERI(0).SUBEADI = "0"
                Return VERI
            End If




        End If
        Dim DEGER(0) As WS_BELGE
        DEGER(0) = New WS_BELGE
        DEGER(0).DURUM = NUMARATOR.DEGERDONMEDI
        DEGER(0).BELGENO = " "
        DEGER(0).FISID = 0
        DEGER(0).FISTARIH = Date.Now
        DEGER(0).FISSAAT = DateTime.Now
        DEGER(0).DEPOID = 0
        DEGER(0).DEPOADI = " "
        DEGER(0).FISTUR = "0"
        DEGER(0).GDEPO = "0"
        DEGER(0).CDEPO = "0"
        DEGER(0).CARIADI = "0"
        DEGER(0).SUBEADI = "0"
        Return DEGER

    End Function
#End Region
#Region "MB/Gonderim Koduyla Teslim Alma"
    Public Class TESLIMALMA
        Public DURUM As Long
        Public BELGEID As Long
        Public STOKID As Long
        Public STOKKODU As String
        Public STOKBARKOD As String
        Public STOKADI As String
        Public MIKTAR As Double
        Public BIRIMFIYAT As Double
        Public KONTROL As Long

    End Class

    <WebMethod()>
    Public Function MB_GonderimKoduTeslimAl(ByVal VERSION As String, ByVal BELGETURU As Long, ByVal BARKOD As String, ByVal ALANDEPID As Long, ByVal ACIKLAMA As String, ByVal ISLEM As Long) As TESLIMALMA()

        Dim SQ As String
        Dim DT As New DataTable
        Dim DT2 As New DataTable
        Dim belgeid As Long
        Dim aldepid As Long
        Dim kontrol As Long
        Dim ID As Long
        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()


        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"

            Dim VERI(0) As TESLIMALMA
            VERI(0) = New TESLIMALMA
            VERI(0).DURUM = NUMARATOR.APKVERSIYONESKI
            VERI(0).BELGEID = 0
            VERI(0).STOKID = 0
            VERI(0).STOKKODU = " "
            VERI(0).STOKADI = " "
            VERI(0).STOKBARKOD = " "
            VERI(0).MIKTAR = 0
            VERI(0).BIRIMFIYAT = 0
            VERI(0).KONTROL = 0
            Return VERI
#End Region

        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

#Region "WS HATA"
            Dim VERI(0) As TESLIMALMA
            VERI(0) = New TESLIMALMA
            VERI(0).DURUM = NUMARATOR.WSVERSIYONESKI
            VERI(0).BELGEID = 0
            VERI(0).STOKID = 0
            VERI(0).STOKKODU = " "
            VERI(0).STOKADI = " "
            VERI(0).STOKBARKOD = " "
            VERI(0).MIKTAR = 0
            VERI(0).BIRIMFIYAT = 0
            VERI(0).KONTROL = 0
            Return VERI

#End Region



        End If



        'Belge Bul
        If ISLEM = 1 Then
            If BARKOD.Length = 5 Then
                SQ = "SELECT  a_gfid,a_aambar,a_afid FROM " & KUL.tfrm & "depobag where a_gftur=" & BELGETURU & " and  a_key='" & BARKOD & "' and a_aambar=" & ALANDEPID & " "
                DT = SQL_TABLO(SQ, "T", False, False, False)
                belgeid = NULN(DT, 0, "a_gfid")
                aldepid = NULN(DT, 0, "a_aambar")
                kontrol = NULN(DT, 0, "a_afid")
                If kontrol = 0 Then


                    If aldepid = ALANDEPID Then
                        SQ = ""
                        SQ = "Select STK.a_id As STOKID,STKH.a_sira As SIRA,STK.a_kod As KOD,STK.a_barkod As BARKOD,STK.a_adi As AD,STKH.a_mik As MIKTAR,STKH.a_brmfiy as BIRIMFIYAT "
                        SQ = SQ & " FROM " & KUL.tper & "stkhdet As STKH INNER JOIN " & KUL.tfrm & "stok As STK On STKH.a_stok_id=STK.a_id "
                        SQ = SQ & " WHERE STKH.a_id=" & belgeid & " And STKH.a_tur=" & BELGETURU
                        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)

                        If DT.Rows.Count > 0 Then
                            Dim VERI(DT.Rows.Count - 1) As TESLIMALMA
                            For X = 0 To DT.Rows.Count - 1
                                VERI(X) = New TESLIMALMA
                                VERI(X).DURUM = NUMARATOR.BAŞARILI
                                VERI(X).BELGEID = belgeid
                                VERI(X).STOKID = NULN(DT, X, "STOKID")
                                VERI(X).STOKKODU = NULA(DT, X, "KOD")
                                VERI(X).STOKADI = NULA(DT, X, "AD")
                                VERI(X).STOKBARKOD = NULA(DT, X, "BARKOD")
                                VERI(X).MIKTAR = NULD(DT, X, "MIKTAR")
                                VERI(X).BIRIMFIYAT = NULD(DT, X, "BIRIMFIYAT")
                                VERI(X).KONTROL = 0
                            Next
                            Return VERI
                        Else
                            Dim VERI(0) As TESLIMALMA
                            VERI(0) = New TESLIMALMA
                            VERI(0).DURUM = NUMARATOR.KONTOLLUGONDERIMBOSBELGE
                            VERI(0).BELGEID = belgeid
                            VERI(0).STOKID = 0
                            VERI(0).STOKKODU = " "
                            VERI(0).STOKADI = " "
                            VERI(0).STOKBARKOD = " "
                            VERI(0).MIKTAR = 0
                            VERI(0).BIRIMFIYAT = 0
                            VERI(0).KONTROL = 0
                            Return VERI
                        End If
                    Else
                        Dim VERI(0) As TESLIMALMA

                        VERI(0) = New TESLIMALMA
                        VERI(0).DURUM = NUMARATOR.HATA
                        VERI(0).BELGEID = 0
                        VERI(0).STOKID = 0
                        VERI(0).STOKKODU = " "
                        VERI(0).STOKADI = " "
                        VERI(0).STOKBARKOD = " "
                        VERI(0).MIKTAR = 0
                        VERI(0).BIRIMFIYAT = 0
                        VERI(0).KONTROL = 0
                        Return VERI

                    End If
                Else
                    Dim VERI(0) As TESLIMALMA

                    VERI(0) = New TESLIMALMA
                    VERI(0).DURUM = NUMARATOR.UYARI
                    VERI(0).BELGEID = 0
                    VERI(0).STOKID = 0
                    VERI(0).STOKKODU = " "
                    VERI(0).STOKADI = " "
                    VERI(0).STOKBARKOD = " "
                    VERI(0).MIKTAR = 0
                    VERI(0).BIRIMFIYAT = 0
                    VERI(0).KONTROL = 0
                    Return VERI
                End If
            Else
                Dim VERI(0) As TESLIMALMA

                VERI(0) = New TESLIMALMA
                VERI(0).DURUM = NUMARATOR.HATA
                VERI(0).BELGEID = 0
                VERI(0).STOKID = 0
                VERI(0).STOKKODU = " "
                VERI(0).STOKADI = " "
                VERI(0).STOKBARKOD = " "
                VERI(0).MIKTAR = 0
                VERI(0).BIRIMFIYAT = 0
                VERI(0).KONTROL = 0
                Return VERI

            End If


        ElseIf ISLEM = 2 Then
            'Bulunan Belgeyi Ekle ALICI depoya ekle
            If BARKOD.Length = 5 Then
                SQ = ""
                SQ = "SELECT  a_gfid,a_aambar,a_afid FROM " & KUL.tfrm & "depobag where a_gftur=" & BELGETURU & " and  a_key='" & BARKOD & "' and a_aambar=" & ALANDEPID & " "
                DT = SQL_TABLO(SQ, "T", False, False, False)
                belgeid = NULN(DT, 0, "a_gfid")
                aldepid = NULN(DT, 0, "a_aambar")
                kontrol = NULN(DT, 0, "a_afid")
                If kontrol = 0 Then 'belge daha önceden kabul edilmedi ise
                    If aldepid = ALANDEPID Then 'işlemi yapanla alıcı deposu doğru mu 

                        Dim DTR_KOPYA As New X_DepoTransfer

                        DTR_KOPYA.Belge_Bul(49, belgeid)

                        DTR_KOPYA.Belge_Baslik.Cikis_Depo_Id = DTR_KOPYA.Belge_Baslik.Giris_Depo_Id
                        DTR_KOPYA.Belge_Baslik.Giris_Depo_Id = aldepid
                        DTR_KOPYA.Belge_Baslik.Belge_Nosu = DTR_KOPYA.Belge_Baslik.Belge_Nosu & "-" & BARKOD
                        DTR_KOPYA.Belge_Baslik.Aciklaması = ACIKLAMA
                        DTR_KOPYA.Belge_Baslik.Islem_Nosu = ""

                        ID = DTR_KOPYA.Belge_Ekle()
                        Dim VERI(0) As TESLIMALMA
                        If ID = 0 Then
                            VERI(0) = New TESLIMALMA
                            VERI(0).DURUM = NUMARATOR.HATA
                            VERI(0).BELGEID = ID
                            VERI(0).STOKID = 0
                            VERI(0).STOKKODU = " "
                            VERI(0).STOKADI = " "
                            VERI(0).STOKBARKOD = " "
                            VERI(0).MIKTAR = 0
                            VERI(0).BIRIMFIYAT = 0
                            VERI(0).KONTROL = 0

                            Return VERI

                        Else

                            SQ = ""
                            SQ = " UPDATE " & KUL.tfrm & "depobag SET a_aftur= 49 , a_afid=" & ID
                            SQ = SQ & " WHERE a_gfid=" & belgeid
                            SQL_KOS(B_SQL, SQ, False)

                            VERI(0) = New TESLIMALMA
                            VERI(0).DURUM = NUMARATOR.BAŞARILI
                            VERI(0).BELGEID = ID
                            VERI(0).STOKID = 0
                            VERI(0).STOKKODU = 0
                            VERI(0).STOKADI = 0
                            VERI(0).STOKBARKOD = 0
                            VERI(0).MIKTAR = 0
                            VERI(0).BIRIMFIYAT = 0
                            VERI(0).KONTROL = 0

                            Return VERI
                        End If





                        'belge eklenmezse 0 döner. HAta olarak yorunlanmalı.
                        'belge ekleme sorun olmazsa yeni belgenin ID si dönecektir
                    End If
                End If


            End If

        End If
        'Dim SQ As String
        'Dim DT As New DataTable
        'Dim DT2 As New DataTable
        'Dim belgeid As Long
        'Dim aldepid As Long
        'Dim kontrol As Long
        'B_SQL = connstr
        'SQL_TUR = "2005"
        'KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        'KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()


        'If BARKOD.Length = 5 Then
        '    SQ = "SELECT  a_gfid,a_aambar,a_afid FROM " & KUL.tfrm & "depobag where a_gftur=" & BELGETURU & " and  a_key='" & BARKOD & "' and a_aambar=" & ALANDEPID & " "
        '    DT = SQL_TABLO(SQ, "T", False, False, False)
        '    belgeid = NULN(DT, 0, "a_gfid")
        '    aldepid = NULN(DT, 0, "a_aambar")
        '    kontrol = NULN(DT, 0, "a_afid")
        '    If kontrol = 0 Then


        '        If aldepid = ALANDEPID Then
        '            SQ = ""
        '            SQ = "Select STK.a_id As STOKID,STKH.a_sira As SIRA,STK.a_kod As KOD,STK.a_barkod As BARKOD,STK.a_adi As AD,STKH.a_mik As MIKTAR,STKH.a_brmfiy as BIRIMFIYAT "
        '            SQ = SQ & " FROM " & KUL.tper & "stkhdet As STKH INNER JOIN " & KUL.tfrm & "stok As STK On STKH.a_stok_id=STK.a_id "
        '            SQ = SQ & " WHERE STKH.a_id=" & belgeid & " And STKH.a_tur=" & BELGETURU
        '            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)

        '            If DT.Rows.Count > 0 Then
        '                Dim VERI(DT.Rows.Count - 1) As TESLIMALMA
        '                For X = 0 To DT.Rows.Count - 1
        '                    VERI(X) = New TESLIMALMA
        '                    VERI(X).DURUM = NUMARATOR.BAŞARILI
        '                    VERI(X).BELGEID = belgeid
        '                    VERI(X).STOKID = NULN(DT, X, "STOKID")
        '                    VERI(X).STOKKODU = NULA(DT, X, "KOD")
        '                    VERI(X).STOKADI = NULA(DT, X, "AD")
        '                    VERI(X).STOKBARKOD = NULA(DT, X, "BARKOD")
        '                    VERI(X).MIKTAR = NULN(DT, X, "MIKTAR")
        '                    VERI(X).BIRIMFIYAT = NULN(DT, X, "BIRIMFIYAT")
        '                    VERI(X).KONTROL = 0
        '                Next
        '                Return VERI

        '            End If
        '        Else
        '            Dim VERI(0) As TESLIMALMA

        '            VERI(0) = New TESLIMALMA
        '            VERI(0).DURUM = NUMARATOR.HATA
        '            VERI(0).BELGEID = 0
        '            VERI(0).STOKID = 0
        '            VERI(0).STOKKODU = " "
        '            VERI(0).STOKADI = " "
        '            VERI(0).STOKBARKOD = " "
        '            VERI(0).MIKTAR = 0
        '            VERI(0).MIKTAR = 0
        '            VERI(0).KONTROL = 0
        '            Return VERI

        '        End If
        '    Else
        '        Dim VERI(0) As TESLIMALMA

        '        VERI(0) = New TESLIMALMA
        '        VERI(0).DURUM = NUMARATOR.UYARI
        '        VERI(0).BELGEID = 0
        '        VERI(0).STOKID = 0
        '        VERI(0).STOKKODU = " "
        '        VERI(0).STOKADI = " "
        '        VERI(0).STOKBARKOD = " "
        '        VERI(0).MIKTAR = 0
        '        VERI(0).MIKTAR = 0
        '        VERI(0).KONTROL = 0
        '        Return VERI
        '    End If
        'Else
        '    Dim VERI(0) As TESLIMALMA

        '    VERI(0) = New TESLIMALMA
        '    VERI(0).DURUM = NUMARATOR.HATA
        '    VERI(0).BELGEID = 0
        '    VERI(0).STOKID = 0
        '    VERI(0).STOKKODU = " "
        '    VERI(0).STOKADI = " "
        '    VERI(0).STOKBARKOD = " "
        '    VERI(0).MIKTAR = 0
        '    VERI(0).MIKTAR = 0
        '    VERI(0).KONTROL = 0
        '    Return VERI

        'End If

        Dim DEGER(0) As TESLIMALMA

        DEGER(0) = New TESLIMALMA
        DEGER(0).DURUM = NUMARATOR.DEGERDONMEDI
        DEGER(0).BELGEID = 0
        DEGER(0).STOKID = 0
        DEGER(0).STOKKODU = " "
        DEGER(0).STOKADI = " "
        DEGER(0).STOKBARKOD = " "
        DEGER(0).MIKTAR = 0
        DEGER(0).BIRIMFIYAT = 0
        DEGER(0).KONTROL = 0
        Return DEGER

    End Function
#End Region
#Region "MB/Belge Ekleme"
    Public Class WS_BELGEEKLE
        Public DURUM As Long
        Public BELGETURU As Long
        Public BELGENO As String
        Public ID As Long
        Public GONDERIMKODU As String
    End Class
    ''' <summary>
    ''' Gelen parametrelere göre belge eklemesi yapılıyor.
    ''' </summary>
    ''' <param name="BelgeTuru"> Belgenin Türü Örn:49 </param>
    ''' <param name="Aciklama"> Belge Açıklaması </param>
    ''' <param name="cariId"> Carinin Id'si </param>
    ''' <param name="depoId"> Depo Id'si(Bir depo kullanılan alanlarda kullanıyor.) </param>
    ''' <param name="SubeId"> Şube Id'si </param>
    ''' <returns></returns>
    <WebMethod()>
    Public Function MB_BelgeEkle(ByVal VERSION As String, ByVal BelgeTuru As Long, ByVal BelgeNo As String, ByVal Aciklama As String, ByVal cariId As Long, ByVal depoId As Long, ByVal SubeId As Long, ByVal oplan As Long, ByVal userid As Long, ByVal varisdepo As Long, ByVal gonderenfisid As Long, ByVal parametre As Long, ByVal CYANSIT As String, ByVal TESLIMTARIH As String) As WS_BELGEEKLE()
        Dim ID As Long
        Dim FT As New X_Fatura
        Dim SP As New X_Siparis
        Dim SQ As String
        Dim DT As New DataTable
        Dim CariAd As String

        Dim KA As String
        Dim FADRES1 As String
        Dim FADRES2 As String
        Dim FVDAIRE As String
        Dim FVNO As String
        Dim gonderimKodu As String = " "
        B_SQL = connstr




        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"
            Dim BELGE(0) As WS_BELGEEKLE


            BELGE(0) = New WS_BELGEEKLE
            BELGE(0).DURUM = NUMARATOR.APKVERSIYONESKI
            BELGE(0).ID = 0
            BELGE(0).BELGETURU = 0
            BELGE(0).BELGENO = " "
            BELGE(0).GONDERIMKODU = " "
            Return BELGE


#End Region

        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

#Region "WS HATA"
            Dim BELGE(0) As WS_BELGEEKLE
            BELGE(0) = New WS_BELGEEKLE
            BELGE(0).DURUM = NUMARATOR.WSVERSIYONESKI
            BELGE(0).ID = 0
            BELGE(0).BELGETURU = 0
            BELGE(0).BELGENO = " "
            BELGE(0).GONDERIMKODU = " "
            Return BELGE



#End Region



        End If




        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()



        KUL.PRM_belgeuser = userid
        KUL.FIRMA = VT_BILGI("tbl_user", "a_id", "a_firma_id", userid)
        KUL.ID_SUBE = SubeId
        'İşlem türü DepoSevk ve DepoSevkIstek değilse 



        If BelgeTuru <> 49 And BelgeTuru <> 89 And BelgeTuru <> 4 And BelgeTuru <> 33 And BelgeTuru <> 34 Then



            Dim a As String
            a = bym.Parametre(-1, "083")
            Dim BELGE(0) As WS_BELGEEKLE
            Select Case BelgeTuru
                Case 17, 18, 19, 20, 21, 22, 54, 71, 87, 88
                    If bym.Parametre(-1, "083") = "1" Then
                        SQ = ""
                        SQ = "SELECT a_bno From " & KUL.tper & "stkgmas "
                        SQ = SQ & " WHERE a_bno='" & BelgeNo & "' and a_cari_id=" & cariId
                        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                        If DT.Rows.Count > 0 Then

                            BELGE(0) = New WS_BELGEEKLE
                            BELGE(0).DURUM = NUMARATOR.UYARI
                            BELGE(0).ID = 0
                            BELGE(0).BELGETURU = 0
                            BELGE(0).BELGENO = " "
                            BELGE(0).GONDERIMKODU = " "
                            Return BELGE
                        End If
                    ElseIf bym.Parametre(-1, "083") = "2" Then
                        SQ = ""
                        SQ = "SELECT a_bno From " & KUL.tper & "stkgmas "
                        SQ = SQ & " WHERE a_bno='" & BelgeNo & "' "
                        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                        If DT.Rows.Count > 0 Then

                            BELGE(0) = New WS_BELGEEKLE
                            BELGE(0).DURUM = NUMARATOR.UYARI
                            BELGE(0).ID = 0
                            BELGE(0).BELGETURU = 0
                            BELGE(0).BELGENO = " "
                            BELGE(0).GONDERIMKODU = " "
                            Return BELGE
                        End If
                    End If

                Case 10, 11, 12, 13, 70, 75

                    If bym.Parametre(-1, "050").ToLower = "evet" Then

                        SQ = ""
                        SQ = "SELECT a_id From " & KUL.tper & "stkcmas "
                        SQ = SQ & " WHERE a_bno='" & BelgeNo & "' and a_tur= " & BelgeTuru
                        DT = SQL_TABLO(SQ, "T", False, False, False)

                        If DT.Rows.Count > 0 Then
                            Dim BELGE1(0) As WS_BELGEEKLE
                            BELGE1(0) = New WS_BELGEEKLE
                            BELGE1(0).DURUM = NUMARATOR.UYARI
                            BELGE1(0).ID = 0
                            BELGE1(0).BELGETURU = 0
                            BELGE1(0).BELGENO = " "
                            BELGE1(0).GONDERIMKODU = " "
                            Return BELGE1

                        End If

                    End If

                Case 40, 41, 94, 95, 96

                    If bym.Parametre(-1, "051").ToLower = "evet" Then

                        SQ = ""
                        SQ = "SELECT a_id From " & KUL.tper & "stkcmas "
                        SQ = SQ & " WHERE a_bno='" & BelgeNo & "' and a_tur= " & BelgeTuru
                        DT = SQL_TABLO(SQ, "T", False, False, False)

                        If DT.Rows.Count > 0 Then
                            Dim BELGE1(0) As WS_BELGEEKLE
                            BELGE1(0) = New WS_BELGEEKLE
                            BELGE1(0).DURUM = NUMARATOR.UYARI
                            BELGE1(0).ID = 0
                            BELGE1(0).BELGETURU = 0
                            BELGE1(0).BELGENO = " "
                            BELGE1(0).GONDERIMKODU = " "
                            Return BELGE1

                        End If

                    End If
            End Select


            ' Eğer iki değerde dolu ise belge no kullanıcı tanımlarındaki ayara göre üretilmiştir.
            Dim BAGKUL As String = VT_BILGI("tbl_user", "a_id", "a_bbkod", userid)
            Dim BAGKID As String = VT_BILGI("tbl_user", "a_kod", "a_id", BAGKUL)

            'Eğer belge no kullanıcı tanımlarındaki ayara göre üretilmiş ve belge no güncellenemediyse
            If BAGKUL <> "" And BAGKID <> "" And X_Belge_No_Duzelt(BelgeTuru) = False Then

                Dim BELGE1(0) As WS_BELGEEKLE
                BELGE1(0) = New WS_BELGEEKLE
                BELGE1(0).DURUM = NUMARATOR.HATA
                BELGE1(0).ID = 0
                BELGE1(0).BELGETURU = 0
                BELGE1(0).BELGENO = " "
                BELGE1(0).GONDERIMKODU = " "
                Return BELGE1

            End If






            SQ = " "
            SQ = "SELECT a_id FROM " & KUL.tper & "maxid"
            SQ = SQ & " WHERE a_id=" & BelgeTuru
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            If DT.Rows.Count > 0 Then

                SQ = ""
                SQ = "SELECT a_adi, a_iskoran,a_bakiye,a_bakiyetur,a_fadres1,a_fadres2,a_ssehir,a_vn,a_vd"
                SQ = SQ & " FROM " & KUL.tfrm & "cari"
                SQ = SQ & " WHERE a_id=" & cariId & ""
                DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                CariAd = NULA(DT, 0, "a_adi")
                FADRES1 = NULA(DT, 0, "a_fadres1")
                FADRES2 = NULA(DT, 0, "a_fadres2")
                FVNO = NULA(DT, 0, "a_vn")
                FVDAIRE = NULA(DT, 0, "a_vd")
                Select Case BelgeTuru
                    Case 10, 11, 12, 13, 14, 16, 16, 70, 75, 17, 18, 19, 20, 21, 54, 71, 87, 88
                        FT.Fatura_Baslik.Irsaliye_Tarihi = TTAR.TARIH
                        FT.Fatura_Baslik.Vade_Tarihi = TTAR.TARIH
                End Select
                FT.Fatura_Baslik.Fatura_Turu = BelgeTuru
                FT.Fatura_Baslik.Aciklaması = Aciklama
                FT.Fatura_Baslik.Fatura_Cari_Adi = CariAd
                FT.Fatura_Baslik.Cari_Id = cariId
                FT.Fatura_Baslik.Depo_Id = depoId
                FT.Fatura_Baslik.Sube_Id = SubeId
                FT.Fatura_Baslik.Belge_Nosu = BelgeNo
                FT.Fatura_Baslik.Fatura_Cari_AdresSat1 = FADRES1
                FT.Fatura_Baslik.Fatura_Cari_AdresSat2 = FADRES2
                FT.Fatura_Baslik.Fatura_Cari_VergiDairesi = FVDAIRE
                FT.Fatura_Baslik.Fatura_Cari_VergiNo = FVNO
                FT.Fatura_Baslik.Doviz_Kuru = 1
                FT.Fatura_Baslik.Depo_Id_Ters = 0

                KUL.KOD = userid


                ID = FT.Fatura_Ekle()


                If ID = 0 Then
                    BELGE(0) = New WS_BELGEEKLE
                    BELGE(0).DURUM = NUMARATOR.HATA
                    BELGE(0).ID = 0
                    BELGE(0).BELGETURU = 0
                    BELGE(0).BELGENO = " "
                    BELGE(0).GONDERIMKODU = " "
                    Return BELGE
                Else


                    BELGE(0) = New WS_BELGEEKLE
                    BELGE(0).DURUM = NUMARATOR.BAŞARILI
                    BELGE(0).ID = ID
                    BELGE(0).BELGETURU = BelgeTuru
                    BELGE(0).BELGENO = BelgeNo
                    BELGE(0).GONDERIMKODU = gonderimKodu
                    Return BELGE


                End If



            End If






            'DEPO TRANSFER FİŞİ
        ElseIf BelgeTuru = 49 Then
            Dim K_ID As Long = 0
            Dim DP As New X_DepoTransfer
            Dim depoadi As String


            'If depoId <> 0 Then
            '    SQ = ""
            '    SQ = "Select  a_id,a_adi,a_subeid "
            '    SQ = SQ & " FROM " & KUL.tfrm & "ambar"
            '    SQ = SQ & " WHERE a_id=" & depoId & ""
            '    DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            '    depoadi = NULA(DT, 0, "a_adi")
            'End If

            'If cikisDepoId <> 0 Then
            '    SQ = ""
            '    SQ = "Select  a_id,a_adi,a_subeid "
            '    SQ = SQ & " FROM " & KUL.tfrm & "ambar"
            '    SQ = SQ & " WHERE a_id=" & cikisDepoId & ""
            '    DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            '    cikisDepo = NULA(DT, 0, "a_adi")
            'End If

            'K_ID = Belge_Baslik.a_id
            'DP.Belge_Baslik.Sube_Id = SubeId
            DP.Belge_Baslik.Cikis_Depo_Id = depoId
            DP.Belge_Baslik.Giris_Depo_Id = oplan
            DP.Belge_Baslik.Aciklaması = Aciklama
            K_ID = TSAY.MAX_FIS_DEPO(49, 0)

            If parametre = 1 Then
                gonderimKodu = sifre_uret(5)

                SQ = "INSERT INTO " & KUL.tfrm & "depobag (a_key,a_gambar,a_gftur,a_gfid,a_aambar,a_aftur,a_afid) "
                SQ = SQ & " Select "
                SQ = SQ & " N'" & gonderimKodu & "' as a_key,"
                SQ = SQ & " " & depoId & " as a_gambar,"
                SQ = SQ & " " & BelgeTuru & " as a_gftur,"
                SQ = SQ & " " & K_ID & " as a_gfid,"
                SQ = SQ & " " & varisdepo & " as a_aambar,"
                SQ = SQ & " 0 as a_aftur,"
                SQ = SQ & " 0 as a_afid"
                SQL_KOS(B_SQL, SQ, False)
            End If

            KUL.KOD = userid

            'If BelgeNo = " " Then

            '    Select Case K_ID
            '        Case 0 To 9
            '            BelgeNo = "DP-00000000." & K_ID
            '        Case 10 To 99
            '            BelgeNo = "DP-0000000." & K_ID
            '        Case 100 To 999
            '            BelgeNo = "DP-000000." & K_ID
            '        Case 1000 To 9999
            '            BelgeNo = "DP-00000." & K_ID
            '        Case 10000 To 99999
            '            BelgeNo = "DP-0000." & K_ID
            '        Case 100000 To 999999
            '            BelgeNo = "DP-000." & K_ID
            '        Case 1000000 To 9999999
            '            BelgeNo = "DP-00." & K_ID

            '        Case Else

            '    End Select
            'End If

            DP.Belge_Baslik.Belge_Nosu = BelgeNo
            ID = DP.Belge_Ekle()

            Dim BELGE(0) As WS_BELGEEKLE
            If ID = 0 Then
                BELGE(0) = New WS_BELGEEKLE
                BELGE(0).DURUM = NUMARATOR.HATA
                BELGE(0).ID = 0
                BELGE(0).BELGETURU = 0
                BELGE(0).GONDERIMKODU = " "
                Return BELGE
            Else

                If parametre = 2 Then
                    SQ = ""
                    SQ = " UPDATE " & KUL.tfrm & "depobag SET a_aftur=" & BelgeTuru & ", a_afid=" & ID
                    SQ = SQ & " WHERE a_gfid=" & gonderenfisid
                    SQL_KOS(B_SQL, SQ, False)

                End If
                BELGE(0) = New WS_BELGEEKLE
                BELGE(0).DURUM = NUMARATOR.BAŞARILI
                BELGE(0).ID = ID
                BELGE(0).BELGETURU = BelgeTuru
                BELGE(0).GONDERIMKODU = gonderimKodu
                Return BELGE
            End If
            'DEPO SEVK İSTEK FİŞİ
        ElseIf BelgeTuru = 89 Then
            Dim K_ID As Long = 0
            Dim KOD As String = ""

            'K_ID = TSAY.MAX_FIS_DEPO(89, 1)
            'If BelgeNo = " " Then

            '    Select Case K_ID
            '        Case K_ID < 10
            '            BelgeNo = "DP-00000000." & K_ID
            '        Case 10 <= K_ID < 100
            '            BelgeNo = "DP-0000000." & K_ID
            '        Case 100 <= K_ID < 1000
            '            BelgeNo = "DP-000000." & K_ID
            '        Case 1000 <= K_ID < 10000
            '            BelgeNo = "DP-00000." & K_ID
            '        Case 10000 <= K_ID < 100000
            '            BelgeNo = "DP-0000." & K_ID
            '        Case 100000 <= K_ID < 1000000
            '            BelgeNo = "DP-000." & K_ID
            '        Case 1000000 <= K_ID < 10000000
            '            BelgeNo = "DP-00." & K_ID

            '        Case Else

            '    End Select
            'End If
            If KOD = "" Then
                KOD = Val(KUL.KOD) & "-" & K_ID
            End If

            SQ = ""
            SQ = "INSERT INTO " & KUL.tper & "depistmas"
            SQ = SQ & " (a_id, a_kod, a_bno, a_cdepo_id, a_gdepo_id, a_sube_id, a_tarih, a_starih, a_ack, a_kulonay, a_yononay, a_tutar, a_cuser, a_cdate, a_ctime, a_muser, a_mdate, a_mtime, a_statu) "
            SQ = SQ & " Select "
            SQ = SQ & " " & K_ID & " as a_id,"
            SQ = SQ & " N'" & KOD & "' as a_kod,"
            SQ = SQ & " '" & BelgeNo & "' as a_bno,"
            SQ = SQ & " " & depoId & " as a_cdepo_id,"
            SQ = SQ & " " & oplan & " as a_gdepo_id,"
            SQ = SQ & " " & SubeId & " as a_sube_id,"
            SQ = SQ & " N'" & TTAR.TR2UK(Date.Now.ToShortDateString) & "' as a_tarih,"
            SQ = SQ & " N'" & TTAR.TR2UK(Date.Now.ToShortDateString) & "' as a_starih,"
            SQ = SQ & " N'" & Aciklama & "' as a_ack,"
            SQ = SQ & " " & 0 & " as a_kulonay,"
            SQ = SQ & " " & 0 & " as a_yononay,"
            SQ = SQ & " " & 0 & " as a_tutar,"
            SQ = SQ & " " & Val(KUL.KOD) & " as a_cuser,"
            SQ = SQ & " N'" & TTAR.TR2UK(Date.Now.ToShortDateString) & "' as a_cdate,"
            SQ = SQ & " N'" & Date.Now.ToShortTimeString & "' as a_ctime,"
            SQ = SQ & " " & Val(KUL.KOD) & " as a_muser,"
            SQ = SQ & " N'" & TTAR.TR2UK(Date.Now.ToShortDateString) & "' as a_mdate,"
            SQ = SQ & " N'" & Date.Now.ToShortTimeString & "' as a_mtime, "
            SQ = SQ & " " & 0 & " as a_statu"



            Dim BELGE(0) As WS_BELGEEKLE
            If SQL_KOS(B_SQL, SQ, False) = False Then
                BELGE(0) = New WS_BELGEEKLE
                BELGE(0).DURUM = NUMARATOR.HATA
                BELGE(0).ID = 0
                BELGE(0).BELGETURU = 0
                BELGE(0).GONDERIMKODU = " "
                Return BELGE
            Else


                BELGE(0) = New WS_BELGEEKLE
                BELGE(0).DURUM = NUMARATOR.BAŞARILI
                BELGE(0).ID = ID
                BELGE(0).BELGETURU = BelgeTuru
                BELGE(0).GONDERIMKODU = gonderimKodu
                Return BELGE
            End If
        ElseIf BelgeTuru = 4 Then
            Dim MAXİD As Long
            SQ = ""
            SQ = "Select MAX(a_id) AS ID"
            SQ = SQ & " FROM " & KUL.tper & "stsaymas"

            DT = SQL_TABLO(SQ, "T", False, False, False)
            MAXİD = NULN(DT, 0, "ID")
            ID = MAXİD + 1
            'If BelgeNo = " " Then

            '    Select Case ID

            '        Case 0 To 9
            '            BelgeNo = "SY-00000000." & ID
            '        Case 10 To 99
            '            BelgeNo = "SY-0000000." & ID
            '        Case 100 To 999
            '            BelgeNo = "SY-000000." & ID
            '        Case 1000 To 9999
            '            BelgeNo = "SY-00000." & ID
            '        Case 10000 To 99999
            '            BelgeNo = "SY-0000." & ID
            '        Case 100000 To 999999
            '            BelgeNo = "SY-000." & ID
            '        Case 1000000 To 9999999
            '            BelgeNo = "SY-00." & ID


            '    End Select
            'End If


            Dim BELGE(0) As WS_BELGEEKLE

            SQ = ""
            SQ = "Select a_id"
            SQ = SQ & " FROM " & KUL.tper & "stsaymas"
            SQ = SQ & " WHERE(a_id = " & ID & ")"
            DT = SQL_TABLO(SQ, "T", False, False, False)

            If ID = NULN(DT, 0, "a_id") Then

                BELGE(0) = New WS_BELGEEKLE
                BELGE(0).DURUM = NUMARATOR.HATA
                BELGE(0).ID = 0
                BELGE(0).BELGETURU = 0
                BELGE(0).GONDERIMKODU = " "
                Return BELGE

            End If


            KUL.KOD = userid


            SQ = ""
            SQ = "INSERT INTO " & KUL.tper & "stsaymas"
            SQ = SQ & " (a_id, a_bno, a_depo_id, a_ack, a_tarih, a_tutar, a_kesin, a_cuser, a_cdate, a_ctime, a_muser, a_mdate, a_mtime, a_gfisid, a_cfisid)"
            SQ = SQ & " VALUES ("
            SQ = SQ & " " & ID & ", "
            SQ = SQ & " N'" & BelgeNo & "', "
            SQ = SQ & " " & depoId & ", "
            SQ = SQ & " N'" & Aciklama & "', "
            SQ = SQ & " N'" & TTAR.TR2UK(Date.Now.ToShortDateString) & "', "
            SQ = SQ & " 0, "
            SQ = SQ & " 0, "
            SQ = SQ & " " & Val(KUL.KOD) & ", "
            SQ = SQ & " N'" & TTAR.TR2UK(TTAR.TARIH) & "', "
            SQ = SQ & " N'" & Date.Now.ToShortTimeString & "', "
            SQ = SQ & " " & Val(KUL.KOD) & ", "
            SQ = SQ & " N'" & TTAR.TR2UK(TTAR.TARIH) & "', "
            SQ = SQ & " N'" & Date.Now.ToShortTimeString & "', "
            SQ = SQ & " -1, "
            SQ = SQ & " -1 "
            SQ = SQ & " )"
            If SQL_KOS(SQ, False) = True Then

                BELGE(0) = New WS_BELGEEKLE
                BELGE(0).DURUM = NUMARATOR.BAŞARILI
                BELGE(0).ID = ID
                BELGE(0).BELGETURU = BelgeTuru
                BELGE(0).GONDERIMKODU = gonderimKodu
                Return BELGE
            Else
                BELGE(0) = New WS_BELGEEKLE
                BELGE(0).DURUM = NUMARATOR.HATA
                BELGE(0).ID = 0
                BELGE(0).BELGETURU = 0
                BELGE(0).GONDERIMKODU = " "
                Return BELGE
            End If
        ElseIf BelgeTuru = 33 Or BelgeTuru = 34 Then



            If BelgeTuru = 33 Then
                ' Eğer iki değerde dolu ise belge no kullanıcı tanımlarındaki ayara göre üretilmiştir.
                Dim BAGKUL As String = VT_BILGI("tbl_user", "a_id", "a_bbkod", userid)
                Dim BAGKID As String = VT_BILGI("tbl_user", "a_kod", "a_id", BAGKUL)

                'Eğer belge no kullanıcı tanımlarındaki ayara göre üretilmiş ve belge no güncellenemediyse
                If BAGKUL <> "" And BAGKID <> "" And X_Belge_No_Duzelt(BelgeTuru) = False Then

                    Dim BELGE1(0) As WS_BELGEEKLE
                    BELGE1(0) = New WS_BELGEEKLE
                    BELGE1(0).DURUM = NUMARATOR.HATA
                    BELGE1(0).ID = 0
                    BELGE1(0).BELGETURU = 0
                    BELGE1(0).BELGENO = " "
                    BELGE1(0).GONDERIMKODU = " "
                    Return BELGE1

                End If

            End If




            SQ = " "
            SQ = "SELECT a_id FROM " & KUL.tper & "maxid"
            SQ = SQ & " WHERE a_id=" & BelgeTuru

            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            If DT.Rows.Count > 0 Then


                SQ = ""
                SQ = "SELECT a_adi, a_iskoran,a_bakiye,a_bakiyetur,a_fadres1,a_fadres2,a_ssehir,a_vn,a_vd"
                SQ = SQ & " FROM " & KUL.tfrm & "cari"
                SQ = SQ & " WHERE a_id=" & cariId & ""
                DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                CariAd = NULA(DT, 0, "a_adi")
                FADRES1 = NULA(DT, 0, "a_fadres1")
                FADRES2 = NULA(DT, 0, "a_fadres2")
                FVNO = NULA(DT, 0, "a_vn")
                FVDAIRE = NULA(DT, 0, "a_vd")
                SP.Siparis_Baslik.Mas_Tur = BelgeTuru
                SP.Siparis_Baslik.Aciklamasi = Aciklama
                SP.Siparis_Baslik.Fatura_Cari_Adi = CariAd
                SP.Siparis_Baslik.Cari_Id = cariId
                SP.Siparis_Baslik.Depo_Id = depoId
                SP.Siparis_Baslik.Sube_Id = SubeId
                SP.Siparis_Baslik.Belge_Nosu = BelgeNo
                SP.Siparis_Baslik.Cikis_Depo_Id = oplan
                SP.Siparis_Baslik.Fatura_Cari_Adres1 = FADRES1
                SP.Siparis_Baslik.Fatura_Cari_Adres2 = FADRES2
                SP.Siparis_Baslik.Fatura_Cari_VargiNo = FVNO
                SP.Siparis_Baslik.Fatura_Cari_VargiDairesi = FVDAIRE
                SP.Siparis_Baslik.Cariye_Yansit = CYANSIT
                SP.Siparis_Baslik.Stoga_Yansit = "H"
                SP.Siparis_Baslik.Doviz_Id = 0
                SP.Siparis_Baslik.Doviz_Kuru = 1
                SP.Siparis_Baslik.Teslim_Tarihi = TESLIMTARIH



                KUL.KOD = userid


                ID = SP.Siparis_Ekle()

                Dim BELGE(0) As WS_BELGEEKLE
                If ID = 0 Then
                    BELGE(0) = New WS_BELGEEKLE
                    BELGE(0).DURUM = NUMARATOR.HATA
                    BELGE(0).ID = 0
                    BELGE(0).BELGETURU = 0
                    BELGE(0).BELGENO = BelgeNo
                    BELGE(0).GONDERIMKODU = " "
                    Return BELGE
                Else



                    BELGE(0) = New WS_BELGEEKLE
                    BELGE(0).DURUM = NUMARATOR.BAŞARILI
                    BELGE(0).ID = ID
                    BELGE(0).BELGETURU = BelgeTuru
                    BELGE(0).GONDERIMKODU = gonderimKodu
                    Return BELGE


                End If
            End If

        End If




    End Function
#End Region
#Region "MB/Belge Getir"
    Public Class WS_BELGEGETIR
        Public DURUM As Long
        Public FISID As Long
        Public BELGENO As String
        Public FISTUR As String
        Public FISTARIH As DateTime
        Public FISSAAT As DateTime
        Public CARIADI As String
        Public DEPOID As Long
        Public CARIID As Long
        Public DEPOADI As String
        Public OPLANADI As String
        Public OPLANID As Long
        Public SUBEID As Long
        Public SUBEADI As String
        Public ACIKLAMA As String
        Public TUTAR As Double
        Public AD As String
        Public BARKOD As String
        Public KOD As String
        Public MIKTAR As Double
        Public MEVCUTMIKTAR As Double
        Public FISTURADI As String
        Public VARISDEPO As String
    End Class



    <WebMethod()>
    Public Function MB_BelgeGetir(ByVal VERSION As String, ByVal ISLEMTURU As String, ByVal TUR As Long, ByVal ID As Long, ByVal USERID As Long) As WS_BELGEGETIR()
        Dim SQ As String
        Dim DT As New DataTable
        Dim TARIH_1 As String
        Dim SUBE_ID As Long
        Dim SUBE_ADI As String
        Dim URUNAD As String
        Dim URUNKOD As String
        Dim URUNBARKOD As String
        Dim URUNMIKTAR As Double
        Dim TABLO As String
        Dim Sira As Long = 0
        Dim DT1 As New DataTable
        TARIH_1 = Date.Today.ToString("yyyy-MM-dd")
        B_SQL = connstr




        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"
            Dim BELGE(0) As WS_BELGEGETIR

            BELGE(0) = New WS_BELGEGETIR
            BELGE(0).DURUM = NUMARATOR.APKVERSIYONESKI
            BELGE(0).FISID = 0
            BELGE(0).BELGENO = " "
            BELGE(0).FISTUR = 0
            BELGE(0).FISTARIH = Date.Now
            BELGE(0).FISSAAT = DateTime.Now
            BELGE(0).DEPOID = 0
            BELGE(0).CARIADI = " "
            BELGE(0).CARIID = 0
            BELGE(0).DEPOADI = " "
            BELGE(0).OPLANADI = " "
            BELGE(0).OPLANID = 0
            BELGE(0).SUBEID = 0
            BELGE(0).SUBEADI = " "
            BELGE(0).ACIKLAMA = " "
            BELGE(0).TUTAR = 0
            BELGE(0).AD = " "
            BELGE(0).KOD = " "
            BELGE(0).BARKOD = " "
            BELGE(0).MIKTAR = 0
            BELGE(0).MEVCUTMIKTAR = 0
            BELGE(0).FISTURADI = " "
            Return BELGE

#End Region

        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

#Region "WS HATA"
            Dim BELGE(0) As WS_BELGEGETIR

            BELGE(0) = New WS_BELGEGETIR
            BELGE(0).DURUM = NUMARATOR.WSVERSIYONESKI
            BELGE(0).FISID = 0
            BELGE(0).BELGENO = " "
            BELGE(0).FISTUR = 0
            BELGE(0).FISTARIH = Date.Now
            BELGE(0).FISSAAT = DateTime.Now
            BELGE(0).DEPOID = 0
            BELGE(0).CARIADI = " "
            BELGE(0).CARIID = 0
            BELGE(0).DEPOADI = " "
            BELGE(0).OPLANADI = " "
            BELGE(0).OPLANID = 0
            BELGE(0).SUBEID = 0
            BELGE(0).SUBEADI = " "
            BELGE(0).ACIKLAMA = " "
            BELGE(0).TUTAR = 0
            BELGE(0).AD = " "
            BELGE(0).KOD = " "
            BELGE(0).BARKOD = " "
            BELGE(0).MIKTAR = 0
            BELGE(0).MEVCUTMIKTAR = 0
            BELGE(0).FISTURADI = " "

            Return BELGE


#End Region



        End If

        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        KUL.KOD = USERID



        'If Belge_Kilitli_mi(ISLEMTURU, TUR, ID, Sira) = True Then

        '    Dim BELGE(0) As WS_BELGEGETIR
        '    BELGE(0) = New WS_BELGEGETIR
        '    BELGE(0).DURUM = NUMARATOR.UYARI
        '    BELGE(0).FISID = 0
        '    BELGE(0).BELGENO = " "
        '    BELGE(0).FISTUR = 0
        '    BELGE(0).FISTARIH = Date.Now
        '    BELGE(0).FISSAAT = DateTime.Now
        '    BELGE(0).DEPOID = 0
        '    BELGE(0).CARIADI = " "
        '    BELGE(0).CARIID = 0
        '    BELGE(0).DEPOADI = " "
        '    BELGE(0).OPLANADI = " "
        '    BELGE(0).OPLANID = 0
        '    BELGE(0).SUBEID = 0
        '    BELGE(0).SUBEADI = " "
        '    BELGE(0).ACIKLAMA = " "
        '    BELGE(0).TUTAR = 0
        '    BELGE(0).AD = " "
        '    BELGE(0).KOD = " "
        '    BELGE(0).BARKOD = " "
        '    BELGE(0).MIKTAR = 0

        'End If


        SQ = ""
        SQ = "select a_id,a_adi from " & KUL.tper & "maxid where a_id=" & TUR
        DT1 = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)


        Dim FT As New X_Fatura
        If TUR <> 49 And TUR <> 4 And TUR <> 33 And TUR <> 34 Then

            If TUR <> 0 And ID <> 0 Then
                FT.Fatura_Bul(TUR, ID)
            End If
            Dim BELGE(0) As WS_BELGEGETIR
            SUBE_ID = FT.Fatura_Baslik.Sube_Id
            Dim KILIT As String
            Select Case TUR
                Case 10, 11, 12, 13, 14, 16, 70, 75, 40, 41, 42, 95, 96
                    TABLO = "stkcdet"
                Case 17, 18, 19, 20, 21, 22, 54, 71, 87, 88, 43, 44, 69
                    TABLO = "stkgdet"
                Case Else
                    BELGE(0) = New WS_BELGEGETIR
                    BELGE(0).DURUM = NUMARATOR.HATA
                    BELGE(0).FISID = 0
                    BELGE(0).BELGENO = " "
                    BELGE(0).FISTUR = 0
                    BELGE(0).FISTARIH = Date.Now
                    BELGE(0).FISSAAT = DateTime.Now
                    BELGE(0).DEPOID = 0
                    BELGE(0).CARIADI = " "
                    BELGE(0).CARIID = 0
                    BELGE(0).DEPOADI = " "
                    BELGE(0).OPLANADI = " "
                    BELGE(0).OPLANID = 0
                    BELGE(0).SUBEID = 0
                    BELGE(0).SUBEADI = " "
                    BELGE(0).ACIKLAMA = " "
                    BELGE(0).TUTAR = 0
                    BELGE(0).AD = " "
                    BELGE(0).KOD = " "
                    BELGE(0).BARKOD = " "
                    BELGE(0).MIKTAR = 0
                    BELGE(0).MEVCUTMIKTAR = 0
                    BELGE(0).FISTURADI = " "

                    Return BELGE

            End Select
            Select Case TUR
                Case 10, 11, 13, 17, 20, 71, 70
                    KILIT = "FATURA"
                Case 43, 69, 95, 41
                    KILIT = "IRSALIYE"
                Case Else
                    KILIT = " "
            End Select
            'If MB_BelgeKilitEkle(KILIT, TUR, ID, USERID) = False Then

            '    BELGE(0) = New WS_BELGEGETIR
            '    BELGE(0).DURUM = NUMARATOR.UYARI
            '    BELGE(0).FISID = 0
            '    BELGE(0).BELGENO = " "
            '    BELGE(0).FISTUR = 0
            '    BELGE(0).FISTARIH = Date.Now
            '    BELGE(0).FISSAAT = DateTime.Now
            '    BELGE(0).DEPOID = 0
            '    BELGE(0).CARIADI = " "
            '    BELGE(0).CARIID = 0
            '    BELGE(0).DEPOADI = " "
            '    BELGE(0).OPLANADI = " "
            '    BELGE(0).OPLANID = 0
            '    BELGE(0).SUBEID = 0
            '    BELGE(0).SUBEADI = " "
            '    BELGE(0).ACIKLAMA = " "
            '    BELGE(0).TUTAR = 0
            '    BELGE(0).AD = " "
            '    BELGE(0).KOD = " "
            '    BELGE(0).BARKOD = " "
            '    BELGE(0).MIKTAR = 0
            '    BELGE(0).MEVCUTMIKTAR = 0
            '    Return BELGE
            'End If
            Dim DEPO_ADI As String = " "
            SQ = ""
            SQ = "SELECT a_adi FROM " & KUL.tfrm & "ambar where a_id=" & FT.Fatura_Baslik.Depo_Id
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            DEPO_ADI = NULA(DT, 0, "a_adi")
            SQ = ""
            SQ = "SELECT a_adi FROM " & KUL.tfrm & "sube where a_id=" & SUBE_ID
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            SUBE_ADI = NULA(DT, 0, "a_adi")
            SQ = ""
            SQ = "SELECT TOP(1) stk.a_adi AS AD,STK.a_kod AS KOD,STK.a_barkod AS BARKOD,STHD.a_mik AS MIKTAR FROM " & KUL.tfrm & "stok as STK INNER JOIN " & KUL.tper & TABLO & " as STHD "
            SQ = SQ & " ON STK.a_id=STHD.a_stok_id "
            SQ = SQ & " where STHD.a_tur=" & TUR & " and STHD.a_id=" & ID
            SQ = SQ & " ORDER BY a_sira DESC"
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)

            URUNAD = NULA(DT, 0, "AD")
            URUNKOD = NULA(DT, 0, "KOD")
            URUNBARKOD = NULA(DT, 0, "BARKOD")
            URUNMIKTAR = NULD(DT, 0, "MIKTAR")

            BELGE(0) = New WS_BELGEGETIR
            BELGE(0).DURUM = NUMARATOR.BAŞARILI
            BELGE(0).FISID = FT.Fatura_Baslik.Fatura_Id
            BELGE(0).BELGENO = FT.Fatura_Baslik.Belge_Nosu
            BELGE(0).FISTUR = TUR
            BELGE(0).FISTARIH = FT.Fatura_Baslik.Tarihi
            BELGE(0).FISSAAT = FT.Fatura_Baslik.Eklendigi_Saat
            BELGE(0).DEPOID = FT.Fatura_Baslik.Depo_Id
            BELGE(0).CARIADI = FT.Fatura_Baslik.Fatura_Cari_Adi
            BELGE(0).CARIID = FT.Fatura_Baslik.Cari_Id
            BELGE(0).DEPOADI = DEPO_ADI
            BELGE(0).OPLANADI = "0"
            BELGE(0).OPLANID = "0"
            BELGE(0).SUBEID = FT.Fatura_Baslik.Sube_Id
            BELGE(0).SUBEADI = SUBE_ADI
            If FT.Fatura_Baslik.Aciklaması = "" Then
                BELGE(0).ACIKLAMA = " "
            Else
                BELGE(0).ACIKLAMA = FT.Fatura_Baslik.Aciklaması
            End If

            BELGE(0).TUTAR = FT.Fatura_Baslik.Genel_Toplam
            BELGE(0).AD = URUNAD
            If BELGE(0).AD = "" Then
                BELGE(0).AD = " "
            End If
            BELGE(0).KOD = URUNKOD
            If BELGE(0).KOD = "" Then
                BELGE(0).KOD = " "
            End If
            BELGE(0).BARKOD = URUNBARKOD
            If BELGE(0).BARKOD = "" Then
                BELGE(0).BARKOD = " "
            End If
            BELGE(0).MIKTAR = URUNMIKTAR
            BELGE(0).MEVCUTMIKTAR = 0
            BELGE(0).FISTURADI = NULA(DT1, 0, "a_adi")
            Return BELGE

        End If
        If TUR = 49 Then

            'If MB_BelgeKilitEkle("DTRANS", 49, ID, USERID) = False Then
            '    Dim BELGE(0) As WS_BELGEGETIR
            '    BELGE(0) = New WS_BELGEGETIR
            '    BELGE(0).DURUM = NUMARATOR.UYARI
            '    BELGE(0).FISID = 0
            '    BELGE(0).BELGENO = " "
            '    BELGE(0).FISTUR = 0
            '    BELGE(0).FISTARIH = Date.Now
            '    BELGE(0).FISSAAT = DateTime.Now
            '    BELGE(0).DEPOID = 0
            '    BELGE(0).CARIADI = " "
            '    BELGE(0).CARIID = 0
            '    BELGE(0).DEPOADI = " "
            '    BELGE(0).OPLANADI = " "
            '    BELGE(0).OPLANID = 0
            '    BELGE(0).SUBEID = 0
            '    BELGE(0).SUBEADI = " "
            '    BELGE(0).ACIKLAMA = " "
            '    BELGE(0).TUTAR = 0
            '    BELGE(0).AD = " "
            '    BELGE(0).KOD = " "
            '    BELGE(0).BARKOD = " "
            '    BELGE(0).MIKTAR = 0
            '    BELGE(0).MEVCUTMIKTAR = 0
            '    Return BELGE
            'End If

            SQ = ""
            SQ = "SELECT stk.a_adi AS AD,STK.a_kod AS KOD,STK.a_barkod AS BARKOD,STHD.a_mik AS MIKTAR FROM " & KUL.tfrm & "stok as STK INNER JOIN " & KUL.tper & "stkhdet as STHD "
            SQ = SQ & " ON STK.a_id=STHD.a_stok_id "
            SQ = SQ & " where STHD.a_tur=" & TUR & " and STHD.a_id=" & ID
            SQ = SQ & " ORDER BY a_sira DESC"
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            URUNAD = NULA(DT, 0, "AD")
            URUNKOD = NULA(DT, 0, "KOD")
            URUNBARKOD = NULA(DT, 0, "BARKOD")
            URUNMIKTAR = NULD(DT, 0, "MIKTAR")
            If URUNAD = "" Then
                URUNAD = " "
            End If
            If URUNKOD = "" Then
                URUNKOD = " "
            End If
            SQ = ""
            SQ = " Select  SHMAS.a_tuttop As TUTARTOPLAM,SHMAS.a_ack As ACIKLAMA,SHMAS.a_id As fisId, CONVERT(VARCHAR, SHMAS.a_tarih, 104) As fisTarih, CONVERT(VARCHAR, SHMAS.a_ctime, 8) As fisSaat, GDEPO.a_adi As girisDepo, CDEPO.a_adi As cikisDepo,SHMAS.a_bno As BELGENO,SHMAS.a_ambar_id As GDEPOID,SHMAS.a_oplan_id As CDEPOID"
            SQ = SQ & " FROM " & KUL.tper & "stkhmas As SHMAS  "
            SQ = SQ & " INNER JOIN " & KUL.tfrm & "ambar As CDEPO On CDEPO.a_id = SHMAS.a_ambar_id  "
            SQ = SQ & " INNER JOIN " & KUL.tfrm & "ambar As GDEPO On GDEPO.a_id = SHMAS.a_oplan_id "
            SQ = SQ & " WHERE a_tur = 49 And SHMAS.a_id=" & ID & " And SHMAS.a_bno <> '@ISTEK@' "
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            If DT.Rows.Count > 0 Then
                Dim VERI(DT.Rows.Count - 1) As WS_BELGEGETIR
                For X = 0 To DT.Rows.Count - 1
                    VERI(X) = New WS_BELGEGETIR
                    VERI(X).DURUM = NUMARATOR.BAŞARILI
                    VERI(X).FISID = NULN(DT, X, "fisID")
                    VERI(X).BELGENO = NULA(DT, X, "BELGENO")
                    If VERI(X).BELGENO = "" Then
                        VERI(X).BELGENO = " "
                    End If
                    VERI(X).FISTARIH = NULA(DT, X, "fisTarih")
                    VERI(X).FISSAAT = NULA(DT, X, "fisSaat")
                    VERI(X).DEPOADI = NULA(DT, X, "girisDepo")
                    VERI(X).OPLANADI = NULA(DT, X, "cikisDepo")
                    VERI(X).DEPOID = NULA(DT, X, "GDEPOID")
                    VERI(X).OPLANID = NULA(DT, X, "CDEPOID")
                    VERI(X).FISTUR = "49"
                    VERI(X).CARIADI = "0"
                    VERI(X).SUBEADI = "0"
                    VERI(X).CARIID = "0"
                    VERI(X).SUBEADI = "0"
                    VERI(X).ACIKLAMA = NULA(DT, X, "ACIKLAMA")
                    If VERI(X).ACIKLAMA = "" Then
                        VERI(X).ACIKLAMA = " "
                    Else
                        VERI(X).ACIKLAMA = NULA(DT, X, "ACIKLAMA")
                    End If
                    VERI(X).TUTAR = NULA(DT, X, "TUTARTOPLAM")
                    VERI(X).AD = URUNAD
                    VERI(X).KOD = URUNKOD
                    VERI(X).BARKOD = URUNBARKOD
                    If VERI(X).BARKOD = "" Then
                        VERI(X).BARKOD = " "
                    End If
                    VERI(X).MIKTAR = URUNMIKTAR
                    VERI(X).MEVCUTMIKTAR = 0
                    VERI(0).FISTURADI = NULA(DT1, 0, "a_adi")
                Next
                Return VERI
            Else
                Dim belge(0) As WS_BELGEGETIR
                belge(0) = New WS_BELGEGETIR
                belge(0).DURUM = NUMARATOR.HATA
                belge(0).FISID = 0
                belge(0).BELGENO = " "
                belge(0).FISTUR = 0
                belge(0).FISTARIH = Date.Now
                belge(0).FISSAAT = DateTime.Now
                belge(0).DEPOID = 0
                belge(0).CARIADI = " "
                belge(0).CARIID = 0
                belge(0).DEPOADI = " "
                belge(0).OPLANADI = "0"
                belge(0).OPLANID = "0"
                belge(0).SUBEID = 0
                belge(0).SUBEADI = " "
                If FT.Fatura_Baslik.Aciklaması = "" Then
                    belge(0).ACIKLAMA = " "
                Else
                    belge(0).ACIKLAMA = " "
                End If

                belge(0).TUTAR = 0
                belge(0).AD = URUNAD
                If belge(0).AD = "" Then
                    belge(0).AD = " "
                End If
                belge(0).KOD = URUNKOD
                If belge(0).KOD = "" Then
                    belge(0).KOD = " "
                End If
                belge(0).BARKOD = URUNBARKOD
                If belge(0).BARKOD = "" Then
                    belge(0).BARKOD = " "
                End If
                belge(0).MIKTAR = 0
                belge(0).MEVCUTMIKTAR = 0
                belge(0).FISTURADI = " "
                Return belge
            End If
        End If
        If TUR = 4 Then
            'If MB_BelgeKilitEkle("SAYIM", 999, ID, USERID) = False Then
            '    Dim BELGE(0) As WS_BELGEGETIR
            '    BELGE(0) = New WS_BELGEGETIR
            '    BELGE(0).DURUM = NUMARATOR.UYARI
            '    BELGE(0).FISID = 0
            '    BELGE(0).BELGENO = " "
            '    BELGE(0).FISTUR = 0
            '    BELGE(0).FISTARIH = Date.Now
            '    BELGE(0).FISSAAT = DateTime.Now
            '    BELGE(0).DEPOID = 0
            '    BELGE(0).CARIADI = " "
            '    BELGE(0).CARIID = 0
            '    BELGE(0).DEPOADI = " "
            '    BELGE(0).OPLANADI = " "
            '    BELGE(0).OPLANID = 0
            '    BELGE(0).SUBEID = 0
            '    BELGE(0).SUBEADI = " "
            '    BELGE(0).ACIKLAMA = " "
            '    BELGE(0).TUTAR = 0
            '    BELGE(0).AD = " "
            '    BELGE(0).KOD = " "
            '    BELGE(0).BARKOD = " "
            '    BELGE(0).MIKTAR = 0
            '    BELGE(0).MEVCUTMIKTAR = 0
            '    Return BELGE
            'End If

            Dim DEPO_ID As Long
            Dim Aciklama As String
            Dim BNo As String
            Dim STOKID As Long
            Dim KALAN As Double = 0
            SQ = ""
            SQ = "SELECT TOP(1) STS.a_depo_id as DEPOID ,STS.a_stok_id AS STOKID,stk.a_adi AS AD,STK.a_kod AS KOD,STK.a_barkod AS BARKOD,STS.a_sayilan AS MIKTAR FROM " & KUL.tfrm & "stok as STK INNER JOIN " & KUL.tper & "stsaydet as STS  "
            SQ = SQ & " ON STK.a_id=STS.a_stok_id "
            SQ = SQ & " where  STS.a_id=" & ID
            SQ = SQ & " ORDER BY a_sira DESC"
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            STOKID = NULN(DT, 0, "STOKID")
            URUNAD = NULA(DT, 0, "AD")
            URUNKOD = NULA(DT, 0, "KOD")
            URUNBARKOD = NULA(DT, 0, "BARKOD")
            URUNMIKTAR = NULD(DT, 0, "MIKTAR")
            DEPO_ID = NULN(DT, 0, "DEPOID")
            If URUNAD = "" Then
                URUNAD = " "
            End If
            If URUNKOD = "" Then
                URUNKOD = " "
            End If
            SQ = ""
            SQ = "select a_bno AS BELGENO,a_ack as ACIKLAMA,a_id from " & KUL.tper & "stsaymas  where a_id=" & ID
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            Aciklama = NULA(DT, 0, "ACIKLAMA")
            If Aciklama = Nothing Then
                Aciklama = " "
            End If
            BNo = NULA(DT, 0, "BELGENO")
            If BNo = Nothing Then
                BNo = " "
            End If
            SQ = ""
            SQ = "select a_kalan as KALAN from " & KUL.tfrm & "stokdeg where a_ambid=" & DEPO_ID & " and a_id=" & STOKID
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)

            KALAN = NULD(DT, 0, "KALAN")

            SQ = ""
            SQ = " Select TOP(1) STS.a_id As fisId, CONVERT(VARCHAR, STS.a_tarih, 104) As fisTarih, CONVERT(VARCHAR, STS.a_ctime, 8) As fisSaat, CDEPO.a_adi As cikisDepo ,STS.a_depo_id GDEPOID,STS.a_tutar As TUTARTOPLAM"
            SQ = SQ & " FROM " & KUL.tper & "stsaymas As STS  "
            SQ = SQ & " INNER JOIN " & KUL.tfrm & "ambar As CDEPO On CDEPO.a_id = STS.a_depo_id  "
            SQ = SQ & " WHERE STS.a_id=" & ID

            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            If DT.Rows.Count > 0 Then
                Dim VERI(DT.Rows.Count - 1) As WS_BELGEGETIR
                For X = 0 To DT.Rows.Count - 1
                    VERI(X) = New WS_BELGEGETIR
                    VERI(X).DURUM = NUMARATOR.BAŞARILI
                    VERI(X).FISID = NULN(DT, X, "fisID")
                    VERI(X).BELGENO = BNo
                    VERI(X).FISTARIH = NULA(DT, X, "fisTarih")
                    VERI(X).FISSAAT = NULA(DT, X, "fisSaat")
                    VERI(X).DEPOADI = NULA(DT, X, "cikisDepo")
                    VERI(X).DEPOID = NULA(DT, X, "GDEPOID")
                    VERI(X).FISTUR = TUR
                    VERI(X).OPLANADI = " "
                    VERI(X).CARIADI = "0"
                    VERI(X).OPLANID = "0"
                    VERI(X).SUBEID = "0"
                    VERI(X).CARIID = "0"
                    VERI(X).SUBEADI = "0"
                    VERI(X).ACIKLAMA = Aciklama
                    VERI(X).TUTAR = NULA(DT, X, "TUTARTOPLAM")
                    VERI(X).AD = URUNAD
                    VERI(X).KOD = URUNKOD
                    VERI(X).BARKOD = URUNBARKOD
                    If VERI(X).BARKOD = "" Then
                        VERI(X).BARKOD = " "
                    End If
                    VERI(X).MIKTAR = URUNMIKTAR
                    VERI(X).MEVCUTMIKTAR = KALAN
                    VERI(0).FISTURADI = NULA(DT1, 0, "a_adi")
                Next
                Return VERI
            Else
                Dim belge(0) As WS_BELGEGETIR
                belge(0) = New WS_BELGEGETIR
                belge(0).DURUM = NUMARATOR.HATA
                belge(0).FISID = 0
                belge(0).BELGENO = " "
                belge(0).FISTUR = 0
                belge(0).FISTARIH = Date.Now
                belge(0).FISSAAT = DateTime.Now
                belge(0).DEPOID = 0
                belge(0).CARIADI = " "
                belge(0).CARIID = 0
                belge(0).DEPOADI = " "
                belge(0).OPLANADI = "0"
                belge(0).OPLANID = "0"
                belge(0).SUBEID = 0
                belge(0).SUBEADI = " "
                If FT.Fatura_Baslik.Aciklaması = "" Then
                    belge(0).ACIKLAMA = " "
                Else
                    belge(0).ACIKLAMA = " "
                End If

                belge(0).TUTAR = 0
                belge(0).AD = URUNAD
                If belge(0).AD = "" Then
                    belge(0).AD = " "
                End If
                belge(0).KOD = URUNKOD
                If belge(0).KOD = "" Then
                    belge(0).KOD = " "
                End If
                belge(0).BARKOD = URUNBARKOD
                If belge(0).BARKOD = "" Then
                    belge(0).BARKOD = " "
                End If
                belge(0).MIKTAR = 0
                belge(0).MEVCUTMIKTAR = 0
                belge(0).FISTURADI = " "
                Return belge

            End If
        End If
        If TUR = 33 Or TUR = 34 Then
            Dim SP As New X_Siparis
            If TUR <> 0 And ID <> 0 Then
                SP.Siparis_Bul(TUR, ID)
            End If
            Dim BELGE(0) As WS_BELGEGETIR
            SUBE_ID = SP.Siparis_Baslik.Sube_Id
            Select Case TUR
                Case 33, 34
                    TABLO = "sipdet"
                Case Else
                    BELGE(0) = New WS_BELGEGETIR
                    BELGE(0).DURUM = NUMARATOR.HATA
                    BELGE(0).FISID = 0
                    BELGE(0).BELGENO = " "
                    BELGE(0).FISTUR = 0
                    BELGE(0).FISTARIH = Date.Now
                    BELGE(0).FISSAAT = DateTime.Now
                    BELGE(0).DEPOID = 0
                    BELGE(0).CARIADI = " "
                    BELGE(0).CARIID = 0
                    BELGE(0).DEPOADI = " "
                    BELGE(0).OPLANADI = " "
                    BELGE(0).OPLANID = 0
                    BELGE(0).SUBEID = 0
                    BELGE(0).SUBEADI = " "
                    BELGE(0).ACIKLAMA = " "
                    BELGE(0).TUTAR = 0
                    BELGE(0).AD = " "
                    BELGE(0).KOD = " "
                    BELGE(0).BARKOD = " "
                    BELGE(0).MIKTAR = 0
                    BELGE(0).MEVCUTMIKTAR = 0
                    BELGE(0).FISTURADI = " "
                    Return BELGE


            End Select
            Dim DEPO_ADI As String = " "
            SQ = ""
            SQ = "SELECT a_adi FROM " & KUL.tfrm & "ambar where a_id=" & SP.Siparis_Baslik.Depo_Id
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            DEPO_ADI = NULA(DT, 0, "a_adi")
            SQ = ""
            SQ = "SELECT a_adi FROM " & KUL.tfrm & "sube where a_id=" & SUBE_ID
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            SUBE_ADI = NULA(DT, 0, "a_adi")
            SQ = ""
            SQ = "SELECT TOP(1) stk.a_adi AS AD,STK.a_kod AS KOD,STK.a_barkod AS BARKOD,STHD.a_mik AS MIKTAR FROM " & KUL.tfrm & "stok as STK INNER JOIN " & KUL.tper & TABLO & " as STHD "
            SQ = SQ & " ON STK.a_id=STHD.a_stok_id "
            SQ = SQ & " where STHD.a_tur=" & TUR & " and STHD.a_id=" & ID
            SQ = SQ & " ORDER BY a_sira DESC"
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            URUNAD = NULA(DT, 0, "AD")
            URUNKOD = NULA(DT, 0, "KOD")
            URUNBARKOD = NULA(DT, 0, "BARKOD")
            URUNMIKTAR = NULD(DT, 0, "MIKTAR")

            BELGE(0) = New WS_BELGEGETIR
            BELGE(0).DURUM = NUMARATOR.BAŞARILI
            BELGE(0).FISID = SP.Siparis_Baslik.Mas_Id
            BELGE(0).BELGENO = SP.Siparis_Baslik.Belge_Nosu
            BELGE(0).FISTUR = TUR
            BELGE(0).FISTARIH = SP.Siparis_Baslik.Ekleme_Tarihi
            BELGE(0).FISSAAT = SP.Siparis_Baslik.Ekleme_Saati
            BELGE(0).DEPOID = SP.Siparis_Baslik.Depo_Id
            BELGE(0).CARIADI = SP.Siparis_Baslik.Fatura_Cari_Adi
            BELGE(0).CARIID = SP.Siparis_Baslik.Cari_Id
            BELGE(0).DEPOADI = DEPO_ADI
            BELGE(0).OPLANADI = "0"
            BELGE(0).OPLANID = "0"
            BELGE(0).SUBEID = SP.Siparis_Baslik.Sube_Id
            BELGE(0).SUBEADI = SUBE_ADI
            If SP.Siparis_Baslik.Aciklamasi = "" Then
                BELGE(0).ACIKLAMA = " "
            Else
                BELGE(0).ACIKLAMA = SP.Siparis_Baslik.Aciklamasi
            End If

            BELGE(0).TUTAR = SP.Siparis_Baslik.Tutar_Toplami
            BELGE(0).AD = URUNAD
            If BELGE(0).AD = "" Then
                BELGE(0).AD = " "
            End If
            BELGE(0).KOD = URUNKOD
            If BELGE(0).KOD = "" Then
                BELGE(0).KOD = " "
            End If
            BELGE(0).BARKOD = URUNBARKOD
            If BELGE(0).BARKOD = "" Then
                BELGE(0).BARKOD = " "
            End If
            BELGE(0).MIKTAR = URUNMIKTAR
            BELGE(0).MEVCUTMIKTAR = 0
            BELGE(0).FISTURADI = NULA(DT1, 0, "a_adi")
            Return BELGE


        End If

        Dim DEGER(0) As WS_BELGEGETIR
        DEGER(0) = New WS_BELGEGETIR
        DEGER(0).DURUM = NUMARATOR.DEGERDONMEDI
        DEGER(0).FISID = 0
        DEGER(0).BELGENO = " "
        DEGER(0).FISTUR = 0
        DEGER(0).FISTARIH = Date.Now
        DEGER(0).FISSAAT = DateTime.Now
        DEGER(0).DEPOID = 0
        DEGER(0).CARIADI = " "
        DEGER(0).CARIID = 0
        DEGER(0).DEPOADI = " "
        DEGER(0).OPLANADI = "0"
        DEGER(0).OPLANID = "0"
        DEGER(0).SUBEID = 0
        DEGER(0).SUBEADI = " "
        DEGER(0).ACIKLAMA = " "
        DEGER(0).TUTAR = 0
        DEGER(0).AD = " "
        DEGER(0).KOD = " "
        DEGER(0).BARKOD = " "
        DEGER(0).MIKTAR = 0
        DEGER(0).MEVCUTMIKTAR = 0
        DEGER(0).FISTURADI = " "
        Return DEGER
    End Function

#End Region
#Region "MB/Depo Arama"
    Public Class WS_Depo
        Public DURUM As Long
        Public ID As Integer
        Public KOD As String
        Public AD As String
        Public TIP As String
        Public GIRIS As Long
        Public CIKIS As Long

    End Class
    ''' <summary>
    ''' USERID VE KULLANICI GRUP ID'YE GÖRE DEPOLARI GERİ DÖNDÜRÜYOR.
    ''' </summary>
    ''' <param name="USERID">USERID</param>
    ''' <param name="KULLANICIGRUPID">KULLANICI GRUP ID (ÖRN:YÖNETİCİ GRUBU)</param>
    ''' <returns></returns>
    <WebMethod()>
    Public Function MB_Depolar(ByVal VERSION As String, ByVal USERID As Long, ByVal KULLANICIGRUPID As Long) As WS_Depo()


        Dim SQ As String
        Dim DT As New DataTable


        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"
            Dim VERI1(0) As WS_Depo
            VERI1(0) = New WS_Depo
            VERI1(0).DURUM = NUMARATOR.APKVERSIYONESKI
            VERI1(0).ID = 0
            VERI1(0).KOD = 0
            VERI1(0).AD = 0
            VERI1(0).TIP = " "
            VERI1(0).GIRIS = 0
            VERI1(0).CIKIS = 0
            Return VERI1

#End Region

        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

#Region "WS HATA"
            Dim VERI1(0) As WS_Depo
            VERI1(0) = New WS_Depo
            VERI1(0).DURUM = NUMARATOR.WSVERSIYONESKI
            VERI1(0).ID = 0
            VERI1(0).KOD = 0
            VERI1(0).AD = 0
            VERI1(0).TIP = " "
            VERI1(0).GIRIS = 0
            VERI1(0).CIKIS = 0
            Return VERI1

#End Region



        End If


        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        'Yönetici ise tüm depoları getir.
        If KULLANICIGRUPID = 1 Then
            SQ = ""
            SQ = "SELECT a_id,a_adi,a_kod,a_tip,a_giris,a_cikis "
            SQ = SQ & "  FROM " & KUL.tfrm & "ambar "
            SQ = SQ & "  Where a_durum=1 "
            DT = SQL_TABLO(SQ, "T", False, False, False)
            If DT.Rows.Count > 0 Then
                Dim VERI1(DT.Rows.Count - 1) As WS_Depo
                For X = 0 To DT.Rows.Count - 1

                    VERI1(X) = New WS_Depo
                    VERI1(X).DURUM = NUMARATOR.BAŞARILI
                    VERI1(X).ID = NULN(DT, X, "a_id")
                    VERI1(X).KOD = NULA(DT, X, "a_kod")
                    VERI1(X).AD = NULA(DT, X, "a_adi")
                    VERI1(X).TIP = NULA(DT, X, "a_tip")
                    VERI1(X).GIRIS = NULN(DT, X, "a_giris")
                    VERI1(X).CIKIS = NULA(DT, X, "a_cikis")

                Next

                Return VERI1
            Else
                Dim VERI1(0) As WS_Depo
                VERI1(0) = New WS_Depo
                VERI1(0).DURUM = NUMARATOR.HATA
                VERI1(0).ID = 0
                VERI1(0).KOD = 0
                VERI1(0).AD = 0
                VERI1(0).TIP = " "
                VERI1(0).GIRIS = 0
                VERI1(0).CIKIS = 0
                Return VERI1

            End If
        Else
            'Yönetici değilse yetkili olduğu depoları getir.
            SQ = ""
            SQ = "SELECT AMBAR.a_id,AMBAR.a_adi,AMBAR.a_kod, AMBAR.a_tip, Sube.a_depoid,AMBAR.a_giris,AMBAR.a_cikis"
            SQ = SQ & "  FROM " & KUL.tfrm & "sube_kul AS SubeKul "
            SQ = SQ & "  INNER JOIN " & KUL.tfrm & "sube_depo AS Sube ON SubeKul.a_subeid = Sube.a_subeid"
            SQ = SQ & "  INNER Join " & KUL.tfrm & "ambar as AMBAR ON SUBE.a_depoid =AMBAR.a_id"
            SQ = SQ & "  WHERE SubeKul.a_userid=" & USERID & " and a_durum=1"
            SQ = SQ & " GROUP BY AMBAR.a_id,AMBAR.a_adi,AMBAR.a_kod, Sube.a_depoid, AMBAR.a_tip,AMBAR.a_giris,AMBAR.a_cikis "

            DT = SQL_TABLO(SQ, "T", False, False, False)


            If DT.Rows.Count > 0 Then
                Dim VERI1(DT.Rows.Count - 1) As WS_Depo
                For X = 0 To DT.Rows.Count - 1

                    VERI1(X) = New WS_Depo
                    VERI1(X).DURUM = NUMARATOR.BAŞARILI
                    VERI1(X).ID = NULN(DT, X, "a_id")
                    VERI1(X).KOD = NULA(DT, X, "a_kod")
                    VERI1(X).AD = NULA(DT, X, "a_adi")
                    VERI1(X).TIP = NULA(DT, X, "a_tip")
                    VERI1(X).GIRIS = NULN(DT, X, "a_giris")
                    VERI1(X).CIKIS = NULN(DT, X, "a_cikis")
                Next

                Return VERI1
            Else
                Dim VERI1(0) As WS_Depo
                VERI1(0) = New WS_Depo
                VERI1(0).DURUM = NUMARATOR.HATA
                VERI1(0).ID = 0
                VERI1(0).KOD = 0
                VERI1(0).AD = 0
                VERI1(0).TIP = " "
                VERI1(0).GIRIS = 0
                VERI1(0).CIKIS = 0
                Return VERI1

            End If
        End If


    End Function
#End Region
#Region "MB/Şube Arama"
    Public Class WS_Sube
        Public DURUM As Long
        Public ID As Integer
        Public KOD As String
        Public AD As String
    End Class
    ''' <summary>
    ''' USERID VE KULLANICI GRUP ID'YE GÖRE ŞUBELERİ GERİ DÖNDÜRÜYOR.
    ''' </summary>
    ''' <param name="USERID"></param>
    ''' <param name="KULLANICIGRUPID"></param>
    ''' <returns></returns>
    <WebMethod()>
    Public Function MB_Subeler(ByVal VERSION As String, ByVal USERID As Long, ByVal GRUPTUR As Long) As WS_Sube()


        Dim SQ As String
        Dim DT As New DataTable

        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()


        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"
            Dim VERI1(0) As WS_Sube
            VERI1(0) = New WS_Sube
            VERI1(0).DURUM = NUMARATOR.APKVERSIYONESKI
            VERI1(0).ID = 0
            VERI1(0).KOD = 0
            VERI1(0).AD = 0
            Return VERI1
#End Region

        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

#Region "WS HATA"
            Dim VERI1(0) As WS_Sube
            VERI1(0) = New WS_Sube
            VERI1(0).DURUM = NUMARATOR.WSVERSIYONESKI
            VERI1(0).ID = 0
            VERI1(0).KOD = 0
            VERI1(0).AD = 0
            Return VERI1

#End Region



        End If
        'Yönetici ise tüm şubeleri getir.
        If GRUPTUR = 1 Then
            SQ = ""
            SQ = "SELECT a_id,a_adi,a_kod"
            SQ = SQ & "  FROM " & KUL.tfrm & "sube "
            DT = SQL_TABLO(SQ, "T", False, False, False)
            If DT.Rows.Count > 0 Then
                Dim VERI(DT.Rows.Count - 1) As WS_Sube


                For X = 0 To DT.Rows.Count - 1

                    VERI(X) = New WS_Sube
                    VERI(X).DURUM = NUMARATOR.BAŞARILI
                    VERI(X).ID = NULN(DT, X, "a_id")
                    VERI(X).KOD = NULA(DT, X, "a_kod")
                    VERI(X).AD = NULA(DT, X, "a_adi")

                Next

                Return VERI
            Else
                Dim VERI1(0) As WS_Sube
                VERI1(0) = New WS_Sube
                VERI1(0).DURUM = NUMARATOR.HATA
                VERI1(0).ID = 0
                VERI1(0).KOD = 0
                VERI1(0).AD = 0
                Return VERI1
            End If
        Else
            'Yönetici değilse yetkili olduğu şubeleri getir.
            SQ = ""
            SQ = "SELECT Sube.a_id, Sube.a_adi,Sube.a_kod"
            SQ = SQ & " FROM " & KUL.tfrm & "sube_kul AS SubeKul"
            SQ = SQ & "  INNER JOIN " & KUL.tfrm & "sube AS Sube ON SubeKul.a_subeid = Sube.a_id"
            SQ = SQ & "  WHERE SubeKul.a_userid=" & USERID


            DT = SQL_TABLO(SQ, "T", False, False, False)
            If DT.Rows.Count > 0 Then
                Dim VERI1(DT.Rows.Count - 1) As WS_Sube


                For X = 0 To DT.Rows.Count - 1

                    VERI1(X) = New WS_Sube
                    VERI1(X).DURUM = NUMARATOR.BAŞARILI
                    VERI1(X).ID = NULN(DT, X, "a_id")
                    VERI1(X).KOD = NULA(DT, X, "a_kod")
                    VERI1(X).AD = NULA(DT, X, "a_adi")

                Next

                Return VERI1
            Else
                Dim VERI1(0) As WS_Sube
                VERI1(0) = New WS_Sube
                VERI1(0).DURUM = NUMARATOR.HATA
                VERI1(0).ID = 0
                VERI1(0).KOD = 0
                VERI1(0).AD = 0
                Return VERI1
            End If
        End If


    End Function
#End Region
#Region "MB/Cari Arama"
    Public Class WS_Cari
        Public DURUM As Long
        Public KOD As String
        Public AD As String
        Public ID As Long
    End Class
    ''' <summary>
    ''' GÖNDERİLEN METİNE GÖRE CARİLERİ GERİ DÖNDÜRÜYOR.
    ''' </summary>
    ''' <param name="CARI">Gönderilen metin</param>
    ''' <returns></returns>
    <WebMethod()>
    Public Function MB_Cariler(ByVal VERSION As String, ByVal CARI As String) As WS_Cari()


        Dim SQ As String
        Dim DT As New DataTable



        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"
            Dim VERI(0) As WS_Cari
            VERI(0) = New WS_Cari
            VERI(0).DURUM = NUMARATOR.APKVERSIYONESKI
            VERI(0).ID = 0
            VERI(0).AD = ""
            VERI(0).KOD = 0


            Return VERI

#End Region

        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

#Region "WS HATA"
            Dim VERI(0) As WS_Cari
            VERI(0) = New WS_Cari
            VERI(0).DURUM = NUMARATOR.HATA
            VERI(0).ID = 0
            VERI(0).AD = ""
            VERI(0).KOD = 0


            Return VERI

#End Region



        End If



        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()

        SQ = ""
        SQ = " Select TOP(5) a_kod,a_adi,a_akpas,a_id "
        SQ = SQ & " FROM " & KUL.tfrm & "cari "
        SQ = SQ & " WHERE (a_adi Like '%" + CARI + "%' or a_kod like '%" + CARI + "%') AND a_akpas='Aktif' "
        SQ = SQ & " ORDER BY a_adi "
        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)

        If DT.Rows.Count > 0 Then
            Dim VERI(DT.Rows.Count - 1) As WS_Cari

            For X = 0 To DT.Rows.Count - 1
                VERI(X) = New WS_Cari
                VERI(X).DURUM = NUMARATOR.BAŞARILI
                VERI(X).ID = NULN(DT, X, "a_id")
                VERI(X).AD = NULA(DT, X, "a_adi")
                VERI(X).KOD = NULA(DT, X, "a_kod")

            Next
            Return VERI
        Else
            Dim VERI(0) As WS_Cari
            VERI(0) = New WS_Cari
            VERI(0).DURUM = NUMARATOR.HATA
            VERI(0).ID = 0
            VERI(0).AD = ""
            VERI(0).KOD = 0


            Return VERI
        End If
    End Function
#End Region
#Region "MB/Stok Arama"
    Public Class WS_StokAra
        Public DURUM As Long
        Public ID As Integer
        Public URUNFIYAT As Double
    End Class

    ''' <summary>
    ''' Stoğun barkoduna göre stok detaylarını getiriyoruz.
    ''' </summary>
    ''' <param name="BARKOD"></param>
    ''' <returns></returns>
    <WebMethod()>
    Public Function MB_StokArama(ByVal VERSION As String, ByVal BARKOD As String, ByVal FIYATTUR As Long, ByVal CARIID As Long) As WS_StokAra()

        Dim SQ As String
        Dim DT As New DataTable
        Dim ALAN As String = ""
        Dim STOKKOD As String
        Dim URUNFIYAT As Double
        Dim CARIKOD As String = ""
        B_SQL = connstr

        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()


        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"
            Dim STK(0) As WS_StokAra
            STK(0) = New WS_StokAra
            STK(0).DURUM = NUMARATOR.APKVERSIYONESKI
            STK(0).ID = 0

            Return STK
#End Region

        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

#Region "WS HATA"
            Dim STK(0) As WS_StokAra
            STK(0) = New WS_StokAra
            STK(0).DURUM = NUMARATOR.WSVERSIYONESKI
            STK(0).ID = 0

            Return STK

#End Region



        End If

        SQ = ""
        SQ = "Select TOP(1) a_kod "
        SQ = SQ & "From " & KUL.tfrm & "cari "
        SQ = SQ & " Where a_id = " & CARIID & " And a_akpas ='Aktif' "
        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
        CARIKOD = NULA(DT, 0, "a_kod")

        SQ = " "
        SQ = "SELECT STOK.a_id AS STOKID,STOK.a_kod as STOKKOD"
        SQ = SQ & " FROM " & KUL.tfrm & "stok as STOK "
        SQ = SQ & " LEFT JOIN " & KUL.tfrm & "stoklot as STOKLOT on STOK.a_id=STOKLOT.a_sid"
        SQ = SQ & " WHERE a_barkod='" & BARKOD & "' AND  a_akpas='Aktif' AND a_reftip NOT IN ('A')"
        SQ = SQ & " ORDER BY a_cdate"

        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)


        If DT.Rows.Count > 0 Then
            STOKKOD = NULA(DT, 0, "STOKKOD")
            URUNFIYAT = FIYATBULMA(FIYATTUR, STOKKOD, CARIKOD)
            Dim STK(DT.Rows.Count - 1) As WS_StokAra
            For X = 0 To DT.Rows.Count - 1

                STK(X) = New WS_StokAra
                STK(X).DURUM = NUMARATOR.BAŞARILI
                STK(X).ID = NULN(DT, X, "STOKID")
                STK(X).URUNFIYAT = URUNFIYAT

            Next
            Return STK

        End If

        SQ = " "
        SQ = "SELECT  STOK.a_id AS STOKID,STOK.a_kod as STOKKOD"
        SQ = SQ & " FROM " & KUL.tfrm & "stok as STOK "
        SQ = SQ & " LEFT JOIN " & KUL.tfrm & "stoklot as STOKLOT on STOK.a_id=STOKLOT.a_sid"
        SQ = SQ & " WHERE  a_kod like  '" & BARKOD & "' AND  a_akpas='Aktif' AND a_reftip NOT IN ('A')"
        SQ = SQ & " ORDER BY a_cdate"

        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)


        If DT.Rows.Count > 0 Then
            STOKKOD = NULA(DT, 0, "STOKKOD")
            URUNFIYAT = FIYATBULMA(FIYATTUR, STOKKOD, CARIKOD)
            Dim STK(DT.Rows.Count - 1) As WS_StokAra
            For X = 0 To DT.Rows.Count - 1

                STK(X) = New WS_StokAra
                STK(X).DURUM = NUMARATOR.BAŞARILI
                STK(X).ID = NULN(DT, X, "STOKID")
                STK(X).URUNFIYAT = URUNFIYAT
            Next
            Return STK

        End If



        SQ = " "
        SQ = "SELECT  STOK.a_id AS STOKID,STOK.a_kod as STOKKOD"
        SQ = SQ & " FROM " & KUL.tfrm & "stok as STOK "
        SQ = SQ & " LEFT JOIN " & KUL.tfrm & "stoklot as STOKLOT on STOK.a_id=STOKLOT.a_sid"
        SQ = SQ & " WHERE  a_1brmbar='" & BARKOD & "' AND  a_akpas='Aktif' AND a_reftip NOT IN ('A')"
        SQ = SQ & " ORDER BY a_cdate"

        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)


        If DT.Rows.Count > 0 Then
            STOKKOD = NULA(DT, 0, "STOKKOD")
            URUNFIYAT = FIYATBULMA(FIYATTUR, STOKKOD, CARIKOD)
            Dim STK(DT.Rows.Count - 1) As WS_StokAra
            For X = 0 To DT.Rows.Count - 1

                STK(X) = New WS_StokAra
                STK(X).DURUM = NUMARATOR.BAŞARILI
                STK(X).ID = NULN(DT, X, "STOKID")
                STK(X).URUNFIYAT = URUNFIYAT

            Next
            Return STK

        End If
        SQ = " "
        SQ = "SELECT  STOK.a_id AS STOKID,STOK.a_kod as STOKKOD"
        SQ = SQ & " FROM " & KUL.tfrm & "stok as STOK "
        SQ = SQ & " LEFT JOIN " & KUL.tfrm & "stoklot as STOKLOT on STOK.a_id=STOKLOT.a_sid"
        SQ = SQ & " WHERE  a_2brmbar='" & BARKOD & "' AND  a_akpas='Aktif' AND a_reftip NOT IN ('A')"
        SQ = SQ & " ORDER BY a_cdate"

        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)


        If DT.Rows.Count > 0 Then
            STOKKOD = NULA(DT, 0, "STOKKOD")
            URUNFIYAT = FIYATBULMA(FIYATTUR, STOKKOD, CARIKOD)
            Dim STK(DT.Rows.Count - 1) As WS_StokAra
            For X = 0 To DT.Rows.Count - 1

                STK(X) = New WS_StokAra
                STK(X).DURUM = NUMARATOR.BAŞARILI
                STK(X).ID = NULN(DT, X, "STOKID")
                STK(X).URUNFIYAT = URUNFIYAT

            Next
            Return STK


        End If
        SQ = " "
        SQ = "SELECT  STOK.a_id AS STOKID,STOK.a_kod as STOKKOD"
        SQ = SQ & " FROM " & KUL.tfrm & "stok as STOK "
        SQ = SQ & " LEFT JOIN " & KUL.tfrm & "stoklot as STOKLOT on STOK.a_id=STOKLOT.a_sid"
        SQ = SQ & " WHERE  a_lotadi='" & BARKOD & "' AND  a_akpas='Aktif' AND a_reftip NOT IN ('A')"
        SQ = SQ & " ORDER BY a_cdate"

        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)


        If DT.Rows.Count > 0 Then
            STOKKOD = NULA(DT, 0, "STOKKOD")
            URUNFIYAT = FIYATBULMA(FIYATTUR, STOKKOD, CARIKOD)
            Dim STK(DT.Rows.Count - 1) As WS_StokAra
            For X = 0 To DT.Rows.Count - 1

                STK(X) = New WS_StokAra
                STK(X).DURUM = NUMARATOR.BAŞARILI
                STK(X).ID = NULN(DT, X, "STOKID")
                STK(X).URUNFIYAT = URUNFIYAT

            Next
            Return STK

        End If
        SQ = " "
        SQ = "SELECT  STOK.a_id AS STOKID,STOK.a_kod as STOKKOD"
        SQ = SQ & " FROM " & KUL.tfrm & "stok as STOK "
        SQ = SQ & " LEFT JOIN " & KUL.tfrm & "stoklot as STOKLOT on STOK.a_id=STOKLOT.a_sid"
        SQ = SQ & " WHERE  a_adi like '" & BARKOD & "' AND  a_akpas='Aktif' AND a_reftip NOT IN ('A')"
        SQ = SQ & " ORDER BY a_cdate"

        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)


        If DT.Rows.Count > 0 Then
            STOKKOD = NULA(DT, 0, "STOKKOD")
            URUNFIYAT = FIYATBULMA(FIYATTUR, STOKKOD, CARIKOD)
            Dim STK(DT.Rows.Count - 1) As WS_StokAra
            For X = 0 To DT.Rows.Count - 1

                STK(X) = New WS_StokAra
                STK(X).DURUM = NUMARATOR.BAŞARILI
                STK(X).ID = NULN(DT, X, "STOKID")
                STK(X).URUNFIYAT = URUNFIYAT

            Next
            Return STK
        Else
            Dim STK(0) As WS_StokAra
            STK(0) = New WS_StokAra
            STK(0).DURUM = NUMARATOR.HATA
            STK(0).ID = 0

            Return STK
        End If

    End Function
#End Region
#Region "MB/Stok Detay"
    Public Class WS_StokDetay
        Public DURUM As Long
        Public ID As Integer
        Public KOD As String
        Public AD As String
        Public BARKOD As String
        Public REFTİP As String
        Public KALAN As Long
        'Public FIYAT As Double
    End Class
    <WebMethod()>
    Public Function MB_StokDetay(ByVal VERSION As String, ByVal STOKID As String, ByVal DEPOID As Long) As WS_StokDetay()

        Dim SQ As String
        Dim DT As New DataTable
        Dim KalanStok As Double
        B_SQL = connstr

        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()


        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"
            Dim STK(0) As WS_StokDetay
            STK(0) = New WS_StokDetay
            STK(0).DURUM = NUMARATOR.APKVERSIYONESKI
            STK(0).ID = 0
            STK(0).AD = " "
            STK(0).KOD = 0
            STK(0).BARKOD = 0
            STK(0).REFTİP = 0
            STK(0).KALAN = 0
            'STK(X).FIYAT="0"

            Return STK
#End Region

        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

#Region "WS HATA"
            Dim STK(0) As WS_StokDetay
            STK(0) = New WS_StokDetay
            STK(0).DURUM = NUMARATOR.WSVERSIYONESKI
            STK(0).ID = 0
            STK(0).AD = " "
            STK(0).KOD = 0
            STK(0).BARKOD = 0
            STK(0).REFTİP = 0
            STK(0).KALAN = 0
            'STK(X).FIYAT="0"

            Return STK

#End Region



        End If

        SQ = ""
        SQ = " SELECT a_kalan FROM " & KUL.tfrm & "stokdeg "
        SQ = SQ & " WHERE a_id=" & STOKID & "  AND a_ambid=" & DEPOID & " "
        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
        KalanStok = NULD(DT, 0, "a_kalan")

        'StokId ile stoğun detayları değişkenlere atanıyor.
        SQ = " "
        SQ = "SELECT  a_id,a_adi,a_kod,a_barkod,a_sfiyat,a_ofiyat1,a_ofiyat2,a_ofiyat3,a_kalan,a_reftip,"
        SQ = SQ & "a_grup1,a_grup2,a_grup3,a_grup4,a_grup5,a_grup6, a_okod, a_aadi, a_marka, a_model, a_acik, a_bilgi1, a_bilgi2, a_bilgi3 "
        SQ = SQ & " FROM " & KUL.tfrm & "stok "
        SQ = SQ & " WHERE a_id='" & STOKID & "' AND a_akpas='Aktif' "


        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)

        If DT.Rows.Count > 0 Then
            'Dim STK(DT.Rows.Count) As WS_StokDetay
            Dim STK(DT.Rows.Count - 1) As WS_StokDetay

            For X = 0 To DT.Rows.Count - 1
                STK(X) = New WS_StokDetay
                STK(X).DURUM = NUMARATOR.BAŞARILI
                STK(X).ID = NULN(DT, X, "a_id")
                STK(X).AD = NULA(DT, X, "a_adi")
                STK(X).KOD = NULA(DT, X, "a_kod")
                STK(X).BARKOD = NULA(DT, X, "a_barkod")
                If STK(X).BARKOD = "" Then
                    STK(X).BARKOD = "0"
                End If
                STK(X).REFTİP = NULA(DT, X, "a_reftip")
                STK(X).KALAN = KalanStok
                'STK(X).FIYAT="0"
            Next



            Return STK
        Else
            Dim STK(0) As WS_StokDetay
            STK(0) = New WS_StokDetay
            STK(0).DURUM = NUMARATOR.HATA
            STK(0).ID = 0
            STK(0).AD = " "
            STK(0).KOD = 0
            STK(0).BARKOD = 0
            STK(0).REFTİP = 0
            STK(0).KALAN = 0
            'STK(X).FIYAT="0"

            Return STK
        End If


    End Function
#End Region
#Region "MB/Ürün Ekleme"
    ''' <summary>
    ''' Bu class a açıklama eklenecek
    ''' </summary>
    Public Class WS_UrunEkle
        Public DURUM As Long
        Public BELGETURU As String
        'Public BELGEID As String
        Public AD As String
        Public BARKOD As String
        Public KOD As String
        Public MIKTAR As Double
        Public MEVCUTMIKTAR As Double
    End Class
    ''' <summary>
    ''' GELEN PARAMETRELERE GÖRE BELGEYE ÜRÜN EKLE
    ''' </summary>
    ''' <param name="BELGETURU">BELGETURU(49,10 vs.)</param>
    ''' <param name="BARKOD">ÜRÜNÜN BARKODU</param>
    ''' <param name="BELGEID"></param>
    ''' <param name="STOKID"></param>
    ''' <param name="MIKTAR"></param>
    ''' <param name="REFTIPI"></param>
    ''' <param name="DEPOID"></param>
    ''' <param name="OPLANID"></param>
    ''' <param name="SUBEID"></param>
    ''' <param name="CARIID"></param>
    ''' <param name="MIKTARKONTROL"></param>
    ''' <param name="URUNFIYAT"></param>
    ''' <param name="YENISATIR"></param>
    ''' <returns></returns>' LONG v DOUBLE TEMİZLENDİ
    <WebMethod()>
    Public Function MB_UrunEkle(ByVal VERSION As String, ByVal BELGETURU As String, ByVal BARKOD As String, ByVal BELGEID As Long, ByVal STOKID As Long, ByVal MIKTAR As Double, ByVal REFTIPI As String, ByVal DEPOID As Long, ByVal OPLANID As Long, ByVal SUBEID As Long, ByVal CARIID As Long, ByVal MIKTARKONTROL As Long, ByVal URUNFIYAT As Double, ByVal USERID As Long, ByVal URUNFIYATTUR As String) As WS_UrunEkle()
        Dim FT As New X_Fatura
        Dim DP As New X_DepoTransfer
        Dim S As New X_Siparis
        Dim SQ As String
        Dim DT As New DataTable
        Dim DT1 As New DataTable
        Dim DT2 As New DataTable
        Dim SIRA As Long = 0
        Dim UPDATESIRA As Integer
        Dim fisStokKontrol As Long
        Dim gecerliFiyat As Double
        Dim birimfiyat As Double
        Dim birim As String
        Dim birimCarpan As Double
        Dim StokMiktar As Double
        Dim gelenmiktar As Double
        Dim tutartoplam As Double
        Dim stokAdi As String
        Dim stokKodu As String
        Dim birimBarkod As String
        Dim STOK_ID As String
        Dim mevcut_miktar As Double
        Dim stoktip As String
        Dim kdval As Double = 0
        Dim kdvsat As Double = 0
        Dim CARIKOD As String
        Dim KDV As Double
        Dim GIRISSUBE As Long
        Dim CIKISSUBE As Long
        'Dim KOSULEKLEME As Boolean = False
        'Dim KOSULEKLEMET As Boolean = False
        B_SQL = connstr
        Dim BELGE(0) As WS_UrunEkle

        Dim STOK As New STOK_BILGI
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()


        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"
            BELGE(0) = New WS_UrunEkle
            BELGE(0).DURUM = NUMARATOR.APKVERSIYONESKI
            BELGE(0).BELGETURU = " "
            BELGE(0).BARKOD = " "
            BELGE(0).AD = " "
            BELGE(0).KOD = " "
            BELGE(0).MIKTAR = 0
            BELGE(0).MEVCUTMIKTAR = 0

            Return BELGE
#End Region

        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

#Region "WS HATA"
            BELGE(0) = New WS_UrunEkle
            BELGE(0).DURUM = NUMARATOR.WSVERSIYONESKI
            BELGE(0).BELGETURU = " "
            BELGE(0).BARKOD = " "
            BELGE(0).AD = " "
            BELGE(0).KOD = " "
            BELGE(0).MIKTAR = 0
            BELGE(0).MEVCUTMIKTAR = 0

            Return BELGE

#End Region



        End If
        Select Case BELGETURU

            Case 17, 18, 19, 20, 21, 22, 71, 87, 88, 43, 44, 69, 98, 11, 13, 14, 16, 70, 75, 40, 41, 42, 96, 95, 94
                If FT.Fatura_Bul(BELGETURU, BELGEID) = False Then
                    BELGE(0) = New WS_UrunEkle
                    BELGE(0).DURUM = NUMARATOR.BELGEBULUNAMADI
                    BELGE(0).BELGETURU = " "
                    BELGE(0).BARKOD = " "
                    BELGE(0).AD = " "
                    BELGE(0).KOD = " "
                    BELGE(0).MIKTAR = 0
                    BELGE(0).MEVCUTMIKTAR = 0

                    Return BELGE
                End If
            Case 49
                If DP.Belge_Bul(BELGETURU, BELGEID) = False Then
                    BELGE(0) = New WS_UrunEkle
                    BELGE(0).DURUM = NUMARATOR.BELGEBULUNAMADI
                    BELGE(0).BELGETURU = " "
                    BELGE(0).BARKOD = " "
                    BELGE(0).AD = " "
                    BELGE(0).KOD = " "
                    BELGE(0).MIKTAR = 0
                    BELGE(0).MEVCUTMIKTAR = 0

                    Return BELGE
                End If
            Case 4
                SQ = ""
                SQ = "SELECT a_id FROM " & KUL.tper & "stsaymas where a_id=" & BELGEID
                DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                If DT.Rows.Count = 0 Then
                    BELGE(0) = New WS_UrunEkle
                    BELGE(0).DURUM = NUMARATOR.BELGEBULUNAMADI
                    BELGE(0).BELGETURU = " "
                    BELGE(0).BARKOD = " "
                    BELGE(0).AD = " "
                    BELGE(0).KOD = " "
                    BELGE(0).MIKTAR = 0
                    BELGE(0).MEVCUTMIKTAR = 0

                    Return BELGE
                End If
            Case 33, 34

                If S.Siparis_Bul(BELGETURU, BELGEID) = False Then
                    BELGE(0) = New WS_UrunEkle
                    BELGE(0).DURUM = NUMARATOR.BELGEBULUNAMADI
                    BELGE(0).BELGETURU = " "
                    BELGE(0).BARKOD = " "
                    BELGE(0).AD = " "
                    BELGE(0).KOD = " "
                    BELGE(0).MIKTAR = 0
                    BELGE(0).MEVCUTMIKTAR = 0

                    Return BELGE
                End If


        End Select


        SQ = ""
        SQ = "Select TOP(1) a_kod "
        SQ = SQ & "From " & KUL.tfrm & "cari "
        SQ = SQ & " Where a_id = " & CARIID & " And a_akpas ='Aktif' "
        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
        CARIKOD = NULA(DT, 0, "a_kod")
        If MIKTARKONTROL = 1 Then
            SQ = ""
            SQ = " SELECT a_kalan FROM " & KUL.tfrm & "stokdeg WHERE a_ambid=" & DEPOID & " AND a_id=" & STOKID
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            StokMiktar = NULD(DT, 0, "a_kalan")
            If MIKTAR > StokMiktar Then

                BELGE(0) = New WS_UrunEkle
                BELGE(0).DURUM = NUMARATOR.HATA
                BELGE(0).BELGETURU = " "
                BELGE(0).BARKOD = " "
                BELGE(0).AD = " "
                BELGE(0).KOD = " "
                BELGE(0).MIKTAR = 0
                BELGE(0).MEVCUTMIKTAR = 0

                Return BELGE
            End If
        End If


        If STOK.BUL(BARKOD) = True Then
            stokKodu = STOK.SKART.S01_kod
            stokAdi = STOK.SKART.S02_adi
            STOK_ID = STOK.SKART.S00_id
            birim = STOK.SKART.S03_birim
            birimCarpan = STOK.SKART.S98_Carpan
            BARKOD = STOK.SKART.S11_barkod
            stoktip = STOK.SKART.S43_a_reftip
            Double.TryParse(STOK.SKART.S07_alkdvor, kdval)
            Double.TryParse(STOK.SKART.S07_alkdvor, kdvsat)
            If URUNFIYATTUR = "@" Then
                '    Long.TryParse(BELGETURU, BELGETURU)
                'Select Case BELGETURU


                '    Case BELGETURU = 17, 18, 19, 20, 21, 22, 71, 87, 88, 43, 44, 69, 98
                '        'İlgili faturalarda kdv dahil fiyattan kdv düş
                '        gecerliFiyat = URUNFIYAT / (1 + (kdval / 100))
                '    Case BELGETURU = 11, 13, 14, 16, 70, 75, 40, 41, 42, 96, 95, 94
                '        gecerliFiyat = URUNFIYAT / (1 + (kdvsat / 100))
                '    Case Else
                gecerliFiyat = URUNFIYAT
                'End Select
            ElseIf URUNFIYATTUR = "-1" Then
                gecerliFiyat = 0
                '    Else
                '        gecerliFiyat = FIYATBULMA(URUNFIYATTUR, stokKodu, CARIKOD)

            End If
            'Else
            'If BARKOD.Length = 5 Then
            '    Dim fisId As Long
            '    Dim alDepoId As Long
            '    SQ = ""
            '    SQ = "select a_key,a_aambar,a_gfid FROM " & KUL.tfrm & "depobag where a_key='" & BARKOD & "'"
            '    DT = SQL_TABLO(SQ, "T", False, False, False)
            '    alDepoId = NULN(DT, 0, "a_aambar")
            '    fisId = NULN(DT, 0, "a_gfid")
            '    If alDepoId = DEPOID Then

            '        SQ = ""
            '        SQ = "SELECT STK.a_kod AS KOD,isnull (STK.a_barkod,'BARKOD GİRİLMEMİŞ.') as BARKOD,isnull(STK.a_adi,'STOK') as AD,SDET.a_id AS ID ,SDET.a_mik  AS MIKTAR  "
            '        SQ = SQ & "From " & KUL.tper & "stkhdet as SDET LEFT join " & KUL.tfrm & "stok AS STK "
            '        SQ = SQ & "On SDET.a_stok_id=STK.a_id   WHERE SDET.a_id =" & fisId & "AND SDET.a_tur=49"
            '        DT = SQL_TABLO(SQ, "T", False, False, False)


            '        If DT.Rows.Count > 0 Then
            '            Dim VERI1(DT.Rows.Count - 1) As WS_UrunEkle
            '            For X = 0 To DT.Rows.Count - 1

            '                VERI1(X) = New WS_UrunEkle
            '                VERI1(X).DURUM = NUMARATOR.BAŞARILI
            '                VERI1(X).BELGETURU = 49
            '                VERI1(X).BELGEID = NULN(DT, X, "ID")
            '                VERI1(X).AD = NULA(DT, X, "AD")
            '                VERI1(X).BARKOD = NULA(DT, X, "BARKOD")
            '                VERI1(X).KOD = NULA(DT, X, "KOD")
            '                VERI1(X).MIKTAR = NULN(DT, X, "MIKTAR")
            '                VERI1(X).MEVCUTMIKTAR = 0
            '            Next

            '            Return VERI1
            '        Else
            '            BELGE(0) = New WS_UrunEkle
            '            BELGE(0).DURUM = NUMARATOR.HATA
            '            BELGE(0).BELGETURU = " "
            '            BELGE(0).BARKOD = " "
            '            BELGE(0).AD = " "
            '            BELGE(0).KOD = " "
            '            BELGE(0).MIKTAR = 0
            '            BELGE(0).MEVCUTMIKTAR = 0
            '            Return BELGE
            '        End If
            '    End If
        Else
            BELGE(0) = New WS_UrunEkle
            BELGE(0).DURUM = NUMARATOR.BULUNAMAYANBARKOD
            BELGE(0).BELGETURU = " "
            BELGE(0).BARKOD = " "
            BELGE(0).AD = " "
            BELGE(0).KOD = " "
            BELGE(0).MIKTAR = 0
            BELGE(0).MEVCUTMIKTAR = 0
            Return BELGE
            'End If
        End If

        'Dim BILGI As String
        'BILGI = bym.VT_BILGI("tbl_user", "a_id", "a_sube_id", USERID)

#Region "Depo Transfer"
        If BELGETURU = 49 Then
            Dim DTSIRA As New DataTable
            Dim DTSube As New DataTable
            SQ = ""
            SQ = "Select COUNT(a_id) AS kontrol, a_sira,a_sirano, a_mik FROM " & KUL.tper & "stkhdet "
            SQ = SQ & " WHERE a_tur = " & BELGETURU & " And a_id = " & BELGEID & " And a_stok_id = " & STOKID & " AND a_brmfiy= " & URUNFIYAT & " GROUP BY a_sirano, a_sira, a_mik ORDER BY a_sira DESC"
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            UPDATESIRA = NULN(DT, 0, "a_sira")
            'fisStokKontrol = NULN(DT, 0, "kontrol")
            Dim KOSULUPDATE As Boolean = False
            Dim KOSULINSERT As Boolean = False
            Dim KOSULINSERTTERS As Boolean = False

            'eklenecek ürünün sırası bulunuyor
            SQ = ""
            SQ = "Select MAX(a_sira) AS SIRA FROM " & KUL.tper & "stkhdet"
            SQ = SQ & " WHERE a_tur=" & BELGETURU & " And a_id=" & BELGEID & " "
            DTSIRA = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            SIRA = NULN(DTSIRA, 0, "SIRA")
            If SIRA = Nothing Then
                SIRA = 1
            Else
                SIRA = SIRA + 1
            End If

            gelenmiktar = birimCarpan * MIKTAR

            If DT.Rows.Count = 0 Then  'INSERT

                SQ = ""
                SQ = "Select a_subeid, a_id from " & KUL.tfrm & "ambar where a_id = " & DEPOID
                DTSube = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                GIRISSUBE = NULN(DTSube, 0, "a_subeid")

                SQ = ""
                SQ = " INSERT INTO " & KUL.tper & "stkhdet (a_gc, a_tur, a_id, a_fattur, a_fatid, a_sira,a_sirano, a_tarih, a_cari_id, a_sube_id, a_stok_id, a_ambar_id, a_mik, a_brmfiy, a_kdv, a_kdvtut, a_isk, a_isktut, a_tutar, a_ack, a_bloke, a_kesin, a_sbrmmik, a_sbrm, a_sbrmcrp, a_sbrmfiy)"
                SQ = SQ & " Select 'C' AS a_gc, 49 AS a_tur, " & BELGEID & " AS a_id, 49 AS a_fattur, " & BELGEID & " AS a_fatid, " & SIRA & " AS a_sira," & SIRA & " a_sirano, "
                SQ = SQ & " Convert(VARCHAR(10), GETDATE(), 110) As a_tarih, " & CARIID & " As a_cari_id, " & GIRISSUBE & " AS a_sube_id, "

                Select Case STOK.SKART.S99_Nerede_Buldun

                    Case STOK_BILGI.Bul_Tur.Stok_1BrmBarkod
                        birimBarkod = STOK.SKART.S26_a_1brmbar
                        birimCarpan = STOK.SKART.S25_a_1brmcar
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S24_a_1birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = MIKTAR * birimfiyat

                        SQ = SQ & STOK_ID & "As a_stok_id, " & DEPOID & " As a_ambar_id , " & gelenmiktar & " As a_mik, " & birimfiyat & " As a_brmfiy, 0 As a_kdv, 0 As a_kdvtut, 0 As a_isk, 0 As a_isktut, " & tutartoplam & " As a_tutar, N'' AS a_ack, 0 AS a_bloke, 0 AS a_kesin, " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy"
                        KOSULINSERT = SQL_KOS(B_SQL, SQ, False)
                    Case STOK_BILGI.Bul_Tur.Stok_2BrmBarkod
                        birimBarkod = STOK.SKART.S31_a_2brmbar
                        birimCarpan = STOK.SKART.S30_a_2brmcar
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S29_a_2birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = MIKTAR * birimfiyat
                        SQ = SQ & STOK_ID & "As a_stok_id, " & DEPOID & " As a_ambar_id , " & gelenmiktar & " As a_mik, " & birimfiyat & " As a_brmfiy, 0 As a_kdv, 0 As a_kdvtut, 0 As a_isk, 0 As a_isktut, " & tutartoplam & " As a_tutar, N'' AS a_ack, 0 AS a_bloke, 0 AS a_kesin, " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy"
                        KOSULINSERT = SQL_KOS(B_SQL, SQ, False)

                    Case STOK_BILGI.Bul_Tur.Stok_Barkod
                        birimBarkod = STOK.SKART.S11_barkod
                        birimCarpan = STOK.SKART.S98_Carpan
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S03_birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = MIKTAR * birimfiyat
                        SQ = SQ & STOK_ID & "As a_stok_id, " & DEPOID & " As a_ambar_id , " & gelenmiktar & " As a_mik, " & birimfiyat & " As a_brmfiy, 0 As a_kdv, 0 As a_kdvtut, 0 As a_isk, 0 As a_isktut, " & tutartoplam & " As a_tutar, N'' AS a_ack, 0 AS a_bloke, 0 AS a_kesin, " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy"
                        KOSULINSERT = SQL_KOS(B_SQL, SQ, False)
                    Case STOK_BILGI.Bul_Tur.Stok_Kodu
                        birimBarkod = STOK.SKART.S11_barkod
                        birimCarpan = STOK.SKART.S98_Carpan
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S03_birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = MIKTAR * birimfiyat
                        SQ = SQ & STOK_ID & "As a_stok_id, " & DEPOID & " As a_ambar_id , " & gelenmiktar & " As a_mik, " & birimfiyat & " As a_brmfiy, 0 As a_kdv, 0 As a_kdvtut, 0 As a_isk, 0 As a_isktut, " & tutartoplam & " As a_tutar, N'' AS a_ack, 0 AS a_bloke, 0 AS a_kesin, " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy"
                        KOSULINSERT = SQL_KOS(B_SQL, SQ, False)
                    Case STOK_BILGI.Bul_Tur.Stok_EkBarkod
                        birimBarkod = STOK.SKART.S46_a_EK_brmbar
                        birimCarpan = STOK.SKART.S45_a_EK_brmcar
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S44_a_EK_birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = MIKTAR * birimfiyat
                        SQ = SQ & STOK_ID & "As a_stok_id, " & DEPOID & " As a_ambar_id , " & gelenmiktar & " As a_mik, " & birimfiyat & " As a_brmfiy, 0 As a_kdv, 0 As a_kdvtut, 0 As a_isk, 0 As a_isktut, " & tutartoplam & " As a_tutar, N'' AS a_ack, 0 AS a_bloke, 0 AS a_kesin, " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy"
                        KOSULINSERT = SQL_KOS(B_SQL, SQ, False)

                    Case Else
                        BELGE(0) = New WS_UrunEkle
                        BELGE(0).DURUM = NUMARATOR.UYARI
                        BELGE(0).BELGETURU = " "
                        BELGE(0).BARKOD = " "
                        BELGE(0).AD = " "
                        BELGE(0).KOD = " "
                        BELGE(0).MIKTAR = 0
                        BELGE(0).MEVCUTMIKTAR = 0
                        Return BELGE
                End Select
                If KOSULINSERT = False Then
                    BELGE(0) = New WS_UrunEkle
                    BELGE(0).DURUM = NUMARATOR.HATA
                    BELGE(0).BELGETURU = " "
                    BELGE(0).BARKOD = " "
                    BELGE(0).AD = " "
                    BELGE(0).KOD = " "
                    BELGE(0).MIKTAR = 0
                    BELGE(0).MEVCUTMIKTAR = 0
                    Return BELGE
                End If
                SQ = ""
                SQ = "Select a_subeid, a_id from " & KUL.tfrm & "ambar where a_id = " & OPLANID
                DTSube = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                CIKISSUBE = NULN(DTSube, 0, "a_subeid")
                SQ = ""
                SQ = " INSERT INTO " & KUL.tper & "stkhdet (a_gc, a_tur, a_id, a_fattur, a_fatid, a_sira,a_sirano, a_tarih, a_cari_id, a_sube_id, a_stok_id, a_ambar_id, a_mik, a_brmfiy, a_kdv, a_kdvtut, a_isk, a_isktut, a_tutar, a_ack, a_bloke, a_kesin, a_sbrmmik, a_sbrm, a_sbrmcrp, a_sbrmfiy)"
                SQ = SQ & " Select 'G' AS a_gc, 50 AS a_tur, " & BELGEID & " AS a_id, 50 AS a_fattur, " & BELGEID & " AS a_fatid, " & SIRA & " AS a_sira," & SIRA & " a_sirano, "
                SQ = SQ & " Convert(VARCHAR(10), GETDATE(), 110) As a_tarih, " & CARIID & " As a_cari_id, " & CIKISSUBE & " AS a_sube_id, "

                Select Case STOK.SKART.S99_Nerede_Buldun

                    Case STOK_BILGI.Bul_Tur.Stok_1BrmBarkod
                        birimBarkod = STOK.SKART.S26_a_1brmbar
                        birimCarpan = STOK.SKART.S25_a_1brmcar
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S24_a_1birim
                        gelenmiktar = birimCarpan * MIKTAR

                        SQ = SQ & STOK_ID & "As a_stok_id, " & OPLANID & " As a_ambar_id, " & gelenmiktar & " As a_mik, " & birimfiyat & " As a_brmfiy, 0 As a_kdv, 0 As a_kdvtut, 0 As a_isk, 0 As a_isktut, " & tutartoplam & " As a_tutar, N'' AS a_ack, 0 AS a_bloke, 0 AS a_kesin,  " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy"
                        KOSULINSERTTERS = SQL_KOS(B_SQL, SQ, False)
                    Case STOK_BILGI.Bul_Tur.Stok_2BrmBarkod
                        birimBarkod = STOK.SKART.S31_a_2brmbar
                        birimCarpan = STOK.SKART.S30_a_2brmcar
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S29_a_2birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = MIKTAR * birimfiyat
                        SQ = SQ & STOK_ID & "As a_stok_id, " & OPLANID & " As a_ambar_id, " & gelenmiktar & " As a_mik, " & birimfiyat & " As a_brmfiy, 0 As a_kdv, 0 As a_kdvtut, 0 As a_isk, 0 As a_isktut, " & tutartoplam & " As a_tutar, N'' AS a_ack, 0 AS a_bloke, 0 AS a_kesin,  " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy"
                        KOSULINSERTTERS = SQL_KOS(B_SQL, SQ, False)

                    Case STOK_BILGI.Bul_Tur.Stok_Barkod
                        birimBarkod = STOK.SKART.S11_barkod
                        birimCarpan = STOK.SKART.S98_Carpan
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S03_birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = MIKTAR * birimfiyat
                        SQ = SQ & STOK_ID & "As a_stok_id, " & OPLANID & " As a_ambar_id, " & gelenmiktar & " As a_mik, " & birimfiyat & " As a_brmfiy, 0 As a_kdv, 0 As a_kdvtut, 0 As a_isk, 0 As a_isktut, " & tutartoplam & " As a_tutar, N'' AS a_ack, 0 AS a_bloke, 0 AS a_kesin,  " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy"
                        KOSULINSERTTERS = SQL_KOS(B_SQL, SQ, False)
                    Case STOK_BILGI.Bul_Tur.Stok_Kodu
                        birimBarkod = STOK.SKART.S11_barkod
                        birimCarpan = STOK.SKART.S98_Carpan
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S03_birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = MIKTAR * birimfiyat
                        SQ = SQ & STOK_ID & "As a_stok_id, " & OPLANID & " As a_ambar_id, " & gelenmiktar & " As a_mik, " & birimfiyat & " As a_brmfiy, 0 As a_kdv, 0 As a_kdvtut, 0 As a_isk, 0 As a_isktut, " & tutartoplam & " As a_tutar, N'' AS a_ack, 0 AS a_bloke, 0 AS a_kesin,  " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy"
                        KOSULINSERTTERS = SQL_KOS(B_SQL, SQ, False)
                    Case STOK_BILGI.Bul_Tur.Stok_EkBarkod
                        birimBarkod = STOK.SKART.S46_a_EK_brmbar
                        birimCarpan = STOK.SKART.S45_a_EK_brmcar
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S44_a_EK_birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = MIKTAR * birimfiyat
                        SQ = SQ & STOK_ID & "As a_stok_id, " & OPLANID & " As a_ambar_id, " & gelenmiktar & " As a_mik, " & birimfiyat & " As a_brmfiy, 0 As a_kdv, 0 As a_kdvtut, 0 As a_isk, 0 As a_isktut, " & tutartoplam & " As a_tutar, N'' AS a_ack, 0 AS a_bloke, 0 AS a_kesin,  " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy"
                        KOSULINSERTTERS = SQL_KOS(B_SQL, SQ, False)
                    Case Else
                        BELGE(0) = New WS_UrunEkle
                        BELGE(0).DURUM = NUMARATOR.UYARI
                        BELGE(0).BELGETURU = " "
                        BELGE(0).BARKOD = " "
                        BELGE(0).AD = " "
                        BELGE(0).KOD = " "
                        BELGE(0).MIKTAR = 0
                        BELGE(0).MEVCUTMIKTAR = 0
                        Return BELGE
                End Select

            ElseIf DT.Rows.Count > 0 Then 'UPDATE

                SQ = ""
                SQ = " UPDATE " & KUL.tper & "stkhdet SET a_sbrmmik = a_sbrmmik + " & MIKTAR & ", a_mik = a_mik + " & gelenmiktar & ", a_tutar = a_brmfiy * (a_mik + " & gelenmiktar & ") "
                SQ = SQ & " WHERE a_tur IN (49, 50) And a_id = " & BELGEID & " And a_stok_id = " & STOKID & " And a_sira=" & UPDATESIRA
                KOSULUPDATE = SQL_KOS(B_SQL, SQ, False)

            End If
            If KOSULINSERT = True And KOSULINSERTTERS = True Or KOSULUPDATE = True Then
                If REFTIPI = "T" Then
                    Dim stokrefId As Long
                    SQ = ""
                    SQ = "SELECT a_refstokid FROM " & KUL.tfrm & "stok  where a_id=" & STOKID & ""
                    DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                    stokrefId = NULN(DT, 0, "a_refstokid")
                    SQ = ""
                    SQ = " UPDATE " & KUL.tfrm & "stokdeg SET a_cikan = a_cikan + " & gelenmiktar & ", a_kalan = a_giren - (" & gelenmiktar & " + a_cikan) "
                    SQ = SQ & " WHERE a_ambid = " & DEPOID & " AND a_id =" & stokrefId
                    SQL_KOS(B_SQL, SQ, False)
                    SQ = ""
                    SQ = " UPDATE " & KUL.tfrm & "stokdeg SET a_giren = a_giren + " & gelenmiktar & ", a_kalan = a_kalan + " & gelenmiktar & "  "
                    SQ = SQ & " WHERE a_ambid = " & DEPOID & " AND a_id =" & stokrefId
                    SQL_KOS(B_SQL, SQ, False)
                End If
                SQ = ""
                SQ = " UPDATE " & KUL.tfrm & "stokdeg SET a_cikan = a_cikan + " & gelenmiktar & ", a_kalan = a_giren - (" & gelenmiktar & " + a_cikan) "
                SQ = SQ & " WHERE a_ambid = " & DEPOID & " AND a_id =" & STOKID
                SQL_KOS(B_SQL, SQ, False)
                SQ = ""
                SQ = " UPDATE " & KUL.tfrm & "stokdeg SET a_giren = a_giren + " & gelenmiktar & ", a_kalan = a_kalan +" & gelenmiktar & "  "
                SQ = SQ & " WHERE a_ambid = " & OPLANID & " AND a_id =" & STOKID
                SQL_KOS(B_SQL, SQ, False)
                Dim TUTARTOPLAMI As Double
                SQ = ""
                SQ = "SELECT ISNULL(SUM(a_tutar),0) AS tutar FROM " & KUL.tper & "stkhdet WHERE a_tur = 49 AND a_id = " & BELGEID
                DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                TUTARTOPLAMI = NULD(DT, 0, "tutar")
                SQ = ""
                SQ = "UPDATE " & KUL.tper & "stkhmas SET a_tuttop = " & TUTARTOPLAMI & " WHERE a_gc = 'C' AND a_tur = 49 AND a_id = " & BELGEID
                SQL_KOS(B_SQL, SQ, False)

                BELGE(0) = New WS_UrunEkle
                BELGE(0).DURUM = NUMARATOR.BAŞARILI
                BELGE(0).BELGETURU = BELGETURU
                BELGE(0).BARKOD = BARKOD
                BELGE(0).AD = stokAdi
                BELGE(0).KOD = stokKodu
                BELGE(0).MIKTAR = gelenmiktar
                BELGE(0).MEVCUTMIKTAR = 0
                Return BELGE
            Else
                BELGE(0) = New WS_UrunEkle
                BELGE(0).DURUM = NUMARATOR.HATA
                BELGE(0).BELGETURU = 0
                BELGE(0).BARKOD = " "
                BELGE(0).AD = " "
                BELGE(0).KOD = " "
                BELGE(0).MIKTAR = 0
                BELGE(0).MEVCUTMIKTAR = 0
                Return BELGE
            End If
        End If
#End Region

#Region "DT SAYIM Ve SİP HARIÇ"
        If BELGETURU <> 49 And BELGETURU <> 89 And BELGETURU <> 4 And BELGETURU <> 33 And BELGETURU <> 34 Then
            Dim TABLO1 As String
            Dim TABLO As String
            Dim GC As String
            Dim KOSULINSERT As Boolean = False
            Dim KOSULUPDATE As Boolean = False

            Dim TUTARTOPLAMI As Double



            Select Case BELGETURU
                Case 10, 11, 12, 13, 14, 16, 70, 75, 40, 41, 42, 95, 96
                    TABLO = "stkcdet"
                    TABLO1 = "stkcmas"
                    GC = "C"
                    KDV = kdvsat
                Case 17, 18, 19, 20, 21, 22, 54, 71, 87, 88, 43, 44, 69
                    TABLO = "stkgdet"
                    TABLO1 = "stkgmas"
                    GC = "G"
                    KDV = kdval
                Case Else
                    BELGE(0) = New WS_UrunEkle
                    BELGE(0).DURUM = NUMARATOR.HATA
                    BELGE(0).BELGETURU = 0
                    BELGE(0).BARKOD = " "
                    BELGE(0).AD = " "
                    BELGE(0).KOD = " "
                    BELGE(0).MIKTAR = 0
                    BELGE(0).MEVCUTMIKTAR = 0
                    Return BELGE
            End Select



            SQ = ""
            SQ = "SELECT STS.a_sira,STS.a_mik, COUNT(STS.a_id)  AS kontrol FROM " & KUL.tper & TABLO & " as STS"
            SQ = SQ & " WHERE a_id = " & BELGEID & " AND a_stok_id = " & STOKID & " AND a_brmfiy= " & URUNFIYAT & " AND a_tur=" & BELGETURU & "GROUP BY STS.a_mik, STS.a_sira ORDER BY STS.a_sira DESC "
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            UPDATESIRA = NULN(DT, 0, "a_sira")

            gelenmiktar = birimCarpan * MIKTAR

            If DT.Rows.Count = 0 Then 'INSERT
                SQ = ""
                SQ = "SELECT a_sira FROM " & KUL.tper & TABLO & " "
                SQ = SQ & " WHERE  a_id=" & BELGEID & " and a_tur=" & BELGETURU & "order by a_sira desc"
                DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                SIRA = NULN(DT, 0, "a_sira")

                If SIRA = Nothing Then
                    SIRA = 1
                Else
                    SIRA = SIRA + 1
                End If
                SQ = ""
                SQ = " INSERT INTO " & KUL.tper & TABLO & " ( a_tur, a_id, a_fattur, a_fatid, a_sira,a_sirano, a_tarih, a_cari_id, a_stok_id, a_ambar_id, a_mik, a_brmfiy, a_kdv, a_kdvtut, a_isk, a_isk1, a_isk2, a_isktut, a_otv, a_otvtut, a_tutar, a_ack, a_perid, a_bloke, a_kesin, a_did, a_dtut, a_dkur, a_indirim, a_isk3, a_isk4, a_isk5, a_fatfiyat, a_sbrmmik, a_sbrm, a_sbrmcrp, a_sbrmfiy, a_gsisko, a_gsiskt)"
                SQ = SQ & " Select  " & BELGETURU & " AS a_tur, " & BELGEID & " AS a_id, " & BELGETURU & " AS a_fattur, " & BELGEID & " AS a_fatid, " & SIRA & " AS a_sira," & SIRA & " a_sirano, "
                SQ = SQ & " Convert(VARCHAR(10), GETDATE(), 110) As a_tarih, " & CARIID & " As a_cari_id, "

                Select Case STOK.SKART.S99_Nerede_Buldun

                    Case STOK_BILGI.Bul_Tur.Stok_1BrmBarkod
                        birimBarkod = STOK.SKART.S26_a_1brmbar
                        birimCarpan = STOK.SKART.S25_a_1brmcar
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S24_a_1birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = (MIKTAR * birimfiyat * KDV / 100.0) + (MIKTAR * birimfiyat)
                        SQ = SQ & STOK_ID & "As a_stok_id, " & DEPOID & " As a_ambar_id, " & gelenmiktar & " As a_mik, " & gecerliFiyat & " As a_brmfiy, " & KDV & " As a_kdv, ( " & KDV & " * (" & gecerliFiyat & " * " & gelenmiktar & "))/100.0  As a_kdvtut, 0 As a_isk, 0 AS a_isk1, 0 AS a_isk2, 0 As a_isktut, 0 AS a_otv, 0 AS a_otvtut, " & tutartoplam & " As a_tutar, N'" & stokAdi & "' AS a_ack, 0 AS a_perid, 0 AS a_bloke, 0 AS a_kesin, 0 AS a_did , " & tutartoplam & " AS a_dtut, 1 AS a_dkur ,0 AS a_indirim, 0 AS a_isk3, 0 AS a_isk4, 0 AS a_isk5, 0 AS a_fatfiyat, " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy, 0 AS a_gsisko, 0 AS a_gsiskt"
                        KOSULINSERT = SQL_KOS(B_SQL, SQ, False)
                    Case STOK_BILGI.Bul_Tur.Stok_2BrmBarkod
                        birimBarkod = STOK.SKART.S31_a_2brmbar
                        birimCarpan = STOK.SKART.S30_a_2brmcar
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S29_a_2birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = (MIKTAR * birimfiyat * KDV / 100.0) + (MIKTAR * birimfiyat)

                        SQ = SQ & STOK_ID & "As a_stok_id, " & DEPOID & " As a_ambar_id, " & gelenmiktar & " As a_mik, " & gecerliFiyat & " As a_brmfiy, " & KDV & " As a_kdv, ( " & KDV & " * (" & gecerliFiyat & " * " & gelenmiktar & "))/100.0  As a_kdvtut, 0 As a_isk, 0 AS a_isk1, 0 AS a_isk2, 0 As a_isktut, 0 AS a_otv, 0 AS a_otvtut, " & tutartoplam & " As a_tutar, N'" & stokAdi & "' AS a_ack, 0 AS a_perid, 0 AS a_bloke, 0 AS a_kesin, 0 AS a_did , " & tutartoplam & " AS a_dtut, 1 AS a_dkur, 0 AS a_indirim, 0 AS a_isk3, 0 AS a_isk4, 0 AS a_isk5, 0 AS a_fatfiyat, " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy, 0 AS a_gsisko, 0 AS a_gsiskt"
                        KOSULINSERT = SQL_KOS(B_SQL, SQ, False)
                    Case STOK_BILGI.Bul_Tur.Stok_Barkod
                        birimBarkod = STOK.SKART.S11_barkod
                        birimCarpan = STOK.SKART.S98_Carpan
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S03_birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = (MIKTAR * birimfiyat * KDV / 100.0) + (MIKTAR * birimfiyat)

                        SQ = SQ & STOK_ID & "As a_stok_id, " & DEPOID & " As a_ambar_id, " & gelenmiktar & " As a_mik, " & gecerliFiyat & " As a_brmfiy, " & KDV & " As a_kdv, ( " & KDV & " * (" & gecerliFiyat & " * " & gelenmiktar & "))/100.0  As a_kdvtut, 0 As a_isk, 0 AS a_isk1, 0 AS a_isk2, 0 As a_isktut, 0 AS a_otv, 0 AS a_otvtut, " & tutartoplam & " As a_tutar, N'" & stokAdi & "' AS a_ack, 0 AS a_perid, 0 AS a_bloke, 0 AS a_kesin, 0 AS a_did , " & tutartoplam & " AS a_dtut, 1 AS a_dkur, 0 AS a_indirim, 0 AS a_isk3, 0 AS a_isk4, 0 AS a_isk5, 0 AS a_fatfiyat, " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy, 0 AS a_gsisko, 0 AS a_gsiskt"
                        KOSULINSERT = SQL_KOS(B_SQL, SQ, False)
                    Case STOK_BILGI.Bul_Tur.Stok_Kodu
                        birimBarkod = STOK.SKART.S11_barkod
                        birimCarpan = STOK.SKART.S98_Carpan
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S03_birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = (MIKTAR * birimfiyat * KDV / 100.0) + (MIKTAR * birimfiyat)

                        SQ = SQ & STOK_ID & "As a_stok_id, " & DEPOID & " As a_ambar_id, " & gelenmiktar & " As a_mik, " & gecerliFiyat & " As a_brmfiy, " & KDV & " As a_kdv, ( " & KDV & " * (" & gecerliFiyat & " * " & gelenmiktar & "))/100.0  As a_kdvtut, 0 As a_isk, 0 AS a_isk1, 0 AS a_isk2, 0 As a_isktut, 0 AS a_otv, 0 AS a_otvtut, " & tutartoplam & " As a_tutar, N'" & stokAdi & "' AS a_ack, 0 AS a_perid, 0 AS a_bloke, 0 AS a_kesin, 0 AS a_did , " & tutartoplam & " AS a_dtut, 1 AS a_dkur, 0 AS a_indirim, 0 AS a_isk3, 0 AS a_isk4, 0 AS a_isk5, 0 AS a_fatfiyat, " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy, 0 AS a_gsisko, 0 AS a_gsiskt"
                        KOSULINSERT = SQL_KOS(B_SQL, SQ, False)
                    Case STOK_BILGI.Bul_Tur.Stok_EkBarkod
                        birimBarkod = STOK.SKART.S46_a_EK_brmbar
                        birimCarpan = STOK.SKART.S45_a_EK_brmcar
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S44_a_EK_birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = (MIKTAR * birimfiyat * KDV / 100.0) + (MIKTAR * birimfiyat)

                        SQ = SQ & STOK_ID & "As a_stok_id, " & DEPOID & " As a_ambar_id, " & gelenmiktar & " As a_mik, " & gecerliFiyat & " As a_brmfiy, " & KDV & " As a_kdv, ( " & KDV & " * (" & gecerliFiyat & " * " & gelenmiktar & "))/100.0  As a_kdvtut, 0 As a_isk, 0 AS a_isk1, 0 AS a_isk2, 0 As a_isktut, 0 AS a_otv, 0 AS a_otvtut, " & tutartoplam & " As a_tutar, N'" & stokAdi & "' AS a_ack, 0 AS a_perid, 0 AS a_bloke, 0 AS a_kesin, 0 AS a_did , " & tutartoplam & " AS a_dtut, 1 AS a_dkur, 0 AS a_indirim, 0 AS a_isk3, 0 AS a_isk4, 0 AS a_isk5, 0 AS a_fatfiyat, " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy, 0 AS a_gsisko, 0 AS a_gsiskt"
                        KOSULINSERT = SQL_KOS(B_SQL, SQ, False)
                    Case Else
                        BELGE(0) = New WS_UrunEkle
                        BELGE(0).DURUM = NUMARATOR.HATA
                        BELGE(0).BELGETURU = 0
                        BELGE(0).BARKOD = " "
                        BELGE(0).AD = " "
                        BELGE(0).KOD = " "
                        BELGE(0).MIKTAR = 0
                        BELGE(0).MEVCUTMIKTAR = 0
                        Return BELGE
                End Select

            ElseIf DT.Rows.Count > 0 Then 'UPDATE
                SQ = "UPDATE " & KUL.tper & TABLO & "  SET a_sbrmmik = a_sbrmmik + " & MIKTAR & ", a_mik = a_mik + " & gelenmiktar & ", a_tutar = a_brmfiy * (a_mik + " & gelenmiktar & ") * " & KDV & "/100.0 + a_brmfiy * (a_mik + " & gelenmiktar & ") ,a_kdvtut=a_kdvtut + ( " & KDV & " * (" & gecerliFiyat & " * " & gelenmiktar & "))/100.0 "
                SQ = SQ & " WHERE a_tur IN (17,20,13,95,69, 43,71,11,41,70) And a_id = " & BELGEID & " And a_stok_id = " & STOKID & " and a_sira=" & UPDATESIRA
                KOSULUPDATE = SQL_KOS(B_SQL, SQ, False)

            End If

            If KOSULINSERT = True Or KOSULUPDATE = True Then
                SQ = ""
                SQ = "SELECT ISNULL(SUM(a_tutar),0) AS tutar,ISNULL(SUM(a_kdvtut),0) AS kdv  FROM " & KUL.tper & TABLO & " WHERE a_tur = " & BELGETURU & " AND a_id = " & BELGEID
                DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                TUTARTOPLAMI = NULD(DT, 0, "tutar")
                KDV = NULD(DT, 0, "kdv")
                SQ = ""
                'SQ = "UPDATE " & KUL.tper & TABLO1 & " SET a_tuttop = " & TUTARTOPLAMI & " ,a_kdvtop=( " & TUTARTOPLAMI & " * " & KDV & "  )/100.0 WHERE  a_tur = " & BELGETURU & " AND a_id = " & BELGEID
                SQ = "UPDATE " & KUL.tper & TABLO1 & " SET a_tuttop = " & (TUTARTOPLAMI) & " ,a_kdvtop= " & KDV & ", a_dtut = " & (TUTARTOPLAMI) & "* a_dkur   WHERE  a_tur = " & BELGETURU & " AND a_id = " & BELGEID
                If SQL_KOS(B_SQL, SQ, False) = True Then

                    BELGE(0) = New WS_UrunEkle
                    BELGE(0).DURUM = NUMARATOR.BAŞARILI
                    BELGE(0).BELGETURU = BELGETURU
                    BELGE(0).BARKOD = BARKOD
                    BELGE(0).AD = stokAdi
                    BELGE(0).KOD = stokKodu
                    BELGE(0).MIKTAR = MIKTAR
                    BELGE(0).MEVCUTMIKTAR = 0
                    Return BELGE
                End If
            Else
                BELGE(0) = New WS_UrunEkle
                BELGE(0).DURUM = NUMARATOR.HATA
                BELGE(0).BELGETURU = 0
                BELGE(0).BARKOD = " "
                BELGE(0).AD = " "
                BELGE(0).KOD = " "
                BELGE(0).MIKTAR = 0
                BELGE(0).MEVCUTMIKTAR = 0
                Return BELGE
            End If
        End If
#End Region

#Region "SAYIM"
        If BELGETURU = 4 Then

            Dim KOSULINSERT As Boolean = False
            Dim SAYILANMIKTAR As Double
            Dim GIRISMIKTAR As Double
            Dim CIKISMIKTAR As Double
            Dim MEVCUTMIK As Double
            'SQ = ""
            'SQ = "select a_kalan as KALAN from " & KUL.tfrm & "stokdeg where a_ambid=" & DEPOID & " and a_id=" & STOKID
            'DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            'mevcut_miktar = NULD(DT, 0, "KALAN")
            GIRISMIKTAR = Stok_Gir_Mik(TTAR.TARIH, DEPOID, STOKID, stoktip)
            CIKISMIKTAR = Stok_Cik_Mik(TTAR.TARIH, DEPOID, STOKID, stoktip)
            MEVCUTMIK = GIRISMIKTAR - CIKISMIKTAR

            SQ = "Select max(a_sira) AS max_sira From " & KUL.tper & "stsaydet  WHERE a_id = " & BELGEID & ""
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            SIRA = NULN(DT, 0, "max_sira")

            SQ = ""
            SQ = "Select STS.a_sira, STS.a_miktar,COUNT(STS.a_id) AS kontrol,STS.a_sayilan as SAYILANMIKTAR FROM " & KUL.tper & "stsaydet as STS"
            SQ = SQ & " WHERE a_id = " & BELGEID & " AND a_stok_id = " & STOKID & " AND a_fiyat= " & URUNFIYAT & "  Group By STS.a_sayilan,STS.a_id, STS.a_miktar, STS.a_sira ORDER BY STS.a_sira DESC "
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)

            SAYILANMIKTAR = NULD(DT, 0, "SAYILANMIKTAR")
            UPDATESIRA = NULN(DT, 0, "a_sira")


            If SIRA = 0 Then
                SIRA = 1
            Else
                SIRA = SIRA + 1
            End If

            If DT.Rows.Count = 0 Then 'INSERT
                SQ = ""
                SQ = " INSERT INTO " & KUL.tper & "stsaydet (a_id, a_sira, a_depo_id, a_stok_id, a_tarih, a_mevcut, a_sayilan, a_miktar, a_mikdur, a_fiyat, a_tutar, a_kesin, a_serilot, a_serilotmik, a_seriparti, a_seriack, a_cuser, a_cdate, a_ctime) "
                SQ = SQ & " Select " & BELGEID & " As a_id , " & SIRA & " As a_sira," & DEPOID & " As a_depo_id," & STOKID & " As a_stok_id,Convert(VARCHAR(10), GETDATE(), 110) As a_tarih," & MEVCUTMIK & " As a_mevcut," & MIKTAR & " As a_sayilan," & MIKTAR - MEVCUTMIK & " As a_miktar, 'E' AS a_mikdur, "

                Select Case STOK.SKART.S99_Nerede_Buldun

                    Case STOK_BILGI.Bul_Tur.Stok_1BrmBarkod
                        birimBarkod = STOK.SKART.S26_a_1brmbar
                        birimCarpan = STOK.SKART.S25_a_1brmcar
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S24_a_1birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = MIKTAR * gecerliFiyat

                        SQ = SQ & "" & birimfiyat & " as a_fiyat," & tutartoplam & " as a_tutar,0 as a_kesin,N'' AS a_serilot,N'' AS a_serilotmik,N'' AS a_seriparti,N'' AS a_seriack," & USERID & "AS a_cuser,Convert(VARCHAR(10), GETDATE(), 110) As a_cdate,N'" & Date.Now.ToShortTimeString & "' as a_ctime "
                        KOSULINSERT = SQL_KOS(B_SQL, SQ, False)
                    Case STOK_BILGI.Bul_Tur.Stok_2BrmBarkod
                        birimBarkod = STOK.SKART.S31_a_2brmbar
                        birimCarpan = STOK.SKART.S30_a_2brmcar
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S29_a_2birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = MIKTAR * gecerliFiyat
                        SQ = SQ & "" & birimfiyat & " as a_fiyat," & tutartoplam & " as a_tutar,0 as a_kesin,N'' AS a_serilot,N'' AS a_serilotmik,N'' AS a_seriparti,N'' AS a_seriack," & USERID & "AS a_cuser,Convert(VARCHAR(10), GETDATE(), 110) As a_cdate,N'" & Date.Now.ToShortTimeString & "' as a_ctime "
                        KOSULINSERT = SQL_KOS(B_SQL, SQ, False)

                    Case STOK_BILGI.Bul_Tur.Stok_Barkod
                        birimBarkod = STOK.SKART.S11_barkod
                        birimCarpan = STOK.SKART.S98_Carpan
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S03_birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = MIKTAR * gecerliFiyat
                        SQ = SQ & "" & birimfiyat & " as a_fiyat," & tutartoplam & " as a_tutar,0 as a_kesin,N'' AS a_serilot,N'' AS a_serilotmik,N'' AS a_seriparti,N'' AS a_seriack," & USERID & "AS a_cuser,Convert(VARCHAR(10), GETDATE(), 110) As a_cdate,N'" & Date.Now.ToShortTimeString & "' as a_ctime "
                        KOSULINSERT = SQL_KOS(B_SQL, SQ, False)
                    Case STOK_BILGI.Bul_Tur.Stok_Kodu
                        birimBarkod = STOK.SKART.S11_barkod
                        birimCarpan = STOK.SKART.S98_Carpan
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S03_birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = MIKTAR * gecerliFiyat
                        SQ = SQ & "" & gecerliFiyat & " as a_fiyat," & tutartoplam & " as a_tutar,0 as a_kesin,N'' AS a_serilot,N'' AS a_serilotmik,N'' AS a_seriparti,N'' AS a_seriack," & USERID & "AS a_cuser,Convert(VARCHAR(10), GETDATE(), 110) As a_cdate,N'" & Date.Now.ToShortTimeString & "' as a_ctime "
                        KOSULINSERT = SQL_KOS(B_SQL, SQ, False)
                    Case STOK_BILGI.Bul_Tur.Stok_EkBarkod
                        birimBarkod = STOK.SKART.S46_a_EK_brmbar
                        birimCarpan = STOK.SKART.S45_a_EK_brmcar
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S44_a_EK_birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = MIKTAR * gecerliFiyat
                        SQ = SQ & "" & birimfiyat & " as a_fiyat," & tutartoplam & " as a_tutar,0 as a_kesin,N'' AS a_serilot,N'' AS a_serilotmik,N'' AS a_seriparti,N'' AS a_seriack," & USERID & "AS a_cuser,Convert(VARCHAR(10), GETDATE(), 110) As a_cdate,N'" & Date.Now.ToShortTimeString & "' as a_ctime "
                        KOSULINSERT = SQL_KOS(B_SQL, SQ, False)

                    Case Else
                        BELGE(0) = New WS_UrunEkle
                        BELGE(0).DURUM = NUMARATOR.HATA
                        BELGE(0).BELGETURU = 0
                        BELGE(0).BARKOD = " "
                        BELGE(0).AD = " "
                        BELGE(0).KOD = " "
                        BELGE(0).MIKTAR = 0
                        BELGE(0).MEVCUTMIKTAR = 0
                        Return BELGE
                End Select
                If KOSULINSERT = True Then
                    BELGE(0) = New WS_UrunEkle
                    BELGE(0).DURUM = NUMARATOR.BAŞARILI
                    BELGE(0).BELGETURU = BELGETURU
                    BELGE(0).BARKOD = BARKOD
                    BELGE(0).AD = stokAdi
                    BELGE(0).KOD = stokKodu
                    BELGE(0).MIKTAR = gelenmiktar
                    BELGE(0).MEVCUTMIKTAR = MEVCUTMIK
                    Return BELGE
                Else
                    BELGE(0) = New WS_UrunEkle
                    BELGE(0).DURUM = NUMARATOR.HATA
                    BELGE(0).BELGETURU = 0
                    BELGE(0).BARKOD = " "
                    BELGE(0).AD = " "
                    BELGE(0).KOD = " "
                    BELGE(0).MIKTAR = 0
                    BELGE(0).MEVCUTMIKTAR = 0
                    Return BELGE
                End If
            ElseIf DT.Rows.Count > 0 Then 'UPDATE

                SQ = " UPDATE " & KUL.tper & "stsaydet Set  a_miktar =" & (SAYILANMIKTAR + MIKTAR) - MEVCUTMIK & ", a_tutar = " & gecerliFiyat & " * (a_miktar +  " & MIKTAR & "), a_sayilan = a_sayilan +  " & MIKTAR & " "
                SQ = SQ & " WHERE  a_id = " & BELGEID & "  And a_stok_id = " & STOKID & " AND a_sira=" & UPDATESIRA
                If SQL_KOS(B_SQL, SQ, False) = True Then
                    BELGE(0) = New WS_UrunEkle
                    BELGE(0).DURUM = NUMARATOR.BAŞARILI
                    BELGE(0).BELGETURU = BELGETURU
                    BELGE(0).BARKOD = BARKOD
                    BELGE(0).AD = stokAdi
                    BELGE(0).KOD = stokKodu
                    BELGE(0).MIKTAR = MIKTAR
                    BELGE(0).MEVCUTMIKTAR = MEVCUTMIK
                    Return BELGE
                Else
                    BELGE(0) = New WS_UrunEkle
                    BELGE(0).DURUM = NUMARATOR.HATA
                    BELGE(0).BELGETURU = 0
                    BELGE(0).BARKOD = " "
                    BELGE(0).AD = " "
                    BELGE(0).KOD = " "
                    BELGE(0).MIKTAR = 0
                    BELGE(0).MEVCUTMIKTAR = 0
                    Return BELGE
                End If


            End If

        End If
#End Region

#Region "SIPARIS"
        If BELGETURU = 33 Or BELGETURU = 34 Then

            Dim DTSIRA As DataTable = New DataTable

            SQ = ""
            SQ = "SELECT  STS.a_sira,COUNT(STS.a_id) AS kontrol  FROM " & KUL.tper & "sipdet as STS"
            SQ = SQ & " WHERE a_id = " & BELGEID & " AND a_stok_id = " & STOKID
            SQ = SQ & " GROUP BY STS.a_sira "
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            fisStokKontrol = NULN(DT, 0, "kontrol")

            SIRA = NULN(DT, 0, "a_sira")
            If SIRA = 0 Then

                SQ = ""
                SQ = "Select MAX(a_sira) AS SIRA FROM " & KUL.tper & "sipdet"
                SQ = SQ & " WHERE a_tur=" & BELGETURU & " And a_id=" & BELGEID & " "
                DTSIRA = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                SIRA = NULN(DTSIRA, 0, "SIRA")
                If SIRA = 0 Then
                    SIRA = 1
                Else
                    SIRA = SIRA + 1
                End If

            Else
                SIRA = SIRA + 1
            End If




            If DT.Rows.Count = 0 Then 'INSERT

                Dim DT3 As DataTable

                SQ = ""
                SQ = "Select a_teslim AS TESLIMTARIH FROM " & KUL.tper & "sipmas"
                SQ = SQ & " WHERE a_tur=" & BELGETURU & " And a_id=" & BELGEID & " "
                DT3 = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)

                Dim TESLIMTARIH = NULA(DT3, 0, "TESLIMTARIH")


                SQ = ""
                SQ = "INSERT INTO " & KUL.tper & "sipdet ( a_tur, a_id, a_sira, a_sirano, a_tarih, a_cari_id, a_stok_id, a_ambar_id, a_mik, a_brmfiy, a_kdv, a_kdvtut, a_isk, a_isktut, a_tutar, a_ack, a_bloke, a_kesin, a_sbrmmik, a_sbrm, a_sbrmcrp, a_sbrmfiy,a_did,a_dtut,a_dkur,a_kapamik,a_tel,a_durum,a_recete_id,a_gsisko,a_gsiskt,a_indirim,a_sube_id,a_perid,a_teslim)"
                SQ = SQ & " Select " & BELGETURU & " AS a_tur, " & BELGEID & " AS a_id, " & SIRA & " AS a_sira," & SIRA & " as  a_sirano,Convert(VARCHAR(10), GETDATE(), 110) As a_tarih, " & CARIID & " As a_cari_id, " & STOK_ID & "As a_stok_id, " & DEPOID & " As a_ambar_id , "

                Select Case STOK.SKART.S99_Nerede_Buldun

                    Case STOK_BILGI.Bul_Tur.Stok_1BrmBarkod
                        birimBarkod = STOK.SKART.S26_a_1brmbar
                        birimCarpan = STOK.SKART.S25_a_1brmcar
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S24_a_1birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = MIKTAR * gecerliFiyat

                        SQ = SQ & gelenmiktar & " As a_mik, " & birimfiyat & " As a_brmfiy, 0 As a_kdv, 0 As a_kdvtut, 0 As a_isk, 0 As a_isktut, " & tutartoplam & " As a_tutar, '" & stokAdi & "' AS a_ack, 0 AS a_bloke, 0 AS a_kesin, " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy,1 as a_did," & tutartoplam & "as a_dtut,1 as a_dkur,0 as a_kapamik, N'' as a_tel, N'' as a_durum,0 as a_recete_id,0 as a_gsisko,0 as a_gsiskt,0 as a_indirim," & SUBEID & "as a_sube_id,0 as _perid , '" & TR2UK(TESLIMTARIH) & "'as a_teslim"
                        SQL_KOS(B_SQL, SQ, False)
                    Case STOK_BILGI.Bul_Tur.Stok_2BrmBarkod
                        birimBarkod = STOK.SKART.S31_a_2brmbar
                        birimCarpan = STOK.SKART.S30_a_2brmcar
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S29_a_2birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = MIKTAR * gecerliFiyat
                        SQ = SQ & gelenmiktar & " As a_mik, " & birimfiyat & " As a_brmfiy, 0 As a_kdv, 0 As a_kdvtut, 0 As a_isk, 0 As a_isktut, " & tutartoplam & " As a_tutar, '" & stokAdi & "' AS a_ack, 0 AS a_bloke, 0 AS a_kesin, " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy,1 as a_did," & tutartoplam & "as a_dtut,1 as a_dkur,0 as a_kapamik, N'' as a_tel, N'' as a_durum,0 as a_recete_id,0 as a_gsisko,0 as a_gsiskt,0 as a_indirim," & SUBEID & "as a_sube_id,0 as _perid, '" & TR2UK(TESLIMTARIH) & "'as a_teslim"
                        SQL_KOS(B_SQL, SQ, False)

                    Case STOK_BILGI.Bul_Tur.Stok_Barkod
                        birimBarkod = STOK.SKART.S11_barkod
                        birimCarpan = STOK.SKART.S98_Carpan
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S03_birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = MIKTAR * gecerliFiyat
                        SQ = SQ & gelenmiktar & " As a_mik, " & birimfiyat & " As a_brmfiy, 0 As a_kdv, 0 As a_kdvtut, 0 As a_isk, 0 As a_isktut, " & tutartoplam & " As a_tutar, '" & stokAdi & "' AS a_ack, 0 AS a_bloke, 0 AS a_kesin, " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy,1 as a_did," & tutartoplam & "as a_dtut,1 as a_dkur,0 as a_kapamik, N'' as a_tel, N'' as a_durum,0 as a_recete_id,0 as a_gsisko,0 as a_gsiskt,0 as a_indirim," & SUBEID & "as a_sube_id,0 as _perid, '" & TR2UK(TESLIMTARIH) & "'as a_teslim"
                        SQL_KOS(B_SQL, SQ, False)
                    Case STOK_BILGI.Bul_Tur.Stok_Kodu
                        birimBarkod = STOK.SKART.S11_barkod
                        birimCarpan = STOK.SKART.S98_Carpan
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S03_birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = MIKTAR * gecerliFiyat
                        SQ = SQ & gelenmiktar & " As a_mik, " & birimfiyat & " As a_brmfiy, 0 As a_kdv, 0 As a_kdvtut, 0 As a_isk, 0 As a_isktut, " & tutartoplam & " As a_tutar, '" & stokAdi & "' AS a_ack, 0 AS a_bloke, 0 AS a_kesin, " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy,1 as a_did," & tutartoplam & "as a_dtut,1 as a_dkur,0 as a_kapamik, N'' as a_tel, N'' as a_durum,0 as a_recete_id,0 as a_gsisko,0 as a_gsiskt,0 as a_indirim," & SUBEID & "as a_sube_id,0 as _perid, '" & TR2UK(TESLIMTARIH) & "'as a_teslim"
                        SQL_KOS(B_SQL, SQ, False)
                    Case STOK_BILGI.Bul_Tur.Stok_EkBarkod
                        birimBarkod = STOK.SKART.S46_a_EK_brmbar
                        birimCarpan = STOK.SKART.S45_a_EK_brmcar
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S44_a_EK_birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = MIKTAR * gecerliFiyat
                        SQ = SQ & gelenmiktar & " As a_mik, " & birimfiyat & " As a_brmfiy, 0 As a_kdv, 0 As a_kdvtut, 0 As a_isk, 0 As a_isktut, " & tutartoplam & " As a_tutar, '" & stokAdi & "' AS a_ack, 0 AS a_bloke, 0 AS a_kesin, " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy,1 as a_did," & tutartoplam & "as a_dtut,1 as a_dkur,0 as a_kapamik, N'' as a_tel, N'' as a_durum,0 as a_recete_id,0 as a_gsisko,0 as a_gsiskt,0 as a_indirim," & SUBEID & "as a_sube_id,0 as _perid, '" & TR2UK(TESLIMTARIH) & "'as a_teslim"
                        SQL_KOS(B_SQL, SQ, False)

                    Case Else
                        BELGE(0) = New WS_UrunEkle
                        BELGE(0).DURUM = NUMARATOR.HATA
                        BELGE(0).BELGETURU = 0
                        BELGE(0).BARKOD = " "
                        BELGE(0).AD = " "
                        BELGE(0).KOD = " "
                        BELGE(0).MIKTAR = 0
                        BELGE(0).MEVCUTMIKTAR = 0
                        Return BELGE
                End Select
                Dim TUTARTOPLAMI As Double
                SQ = ""
                SQ = "SELECT ISNULL(SUM(a_tutar),0) AS tutar FROM " & KUL.tper & "sipdet  WHERE a_tur = " & BELGETURU & " AND a_id = " & BELGEID
                DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                TUTARTOPLAMI = NULD(DT, 0, "tutar")
                SQ = ""
                SQ = "UPDATE " & KUL.tper & "sipmas SET a_tuttop = " & TUTARTOPLAMI & " WHERE  a_tur = " & BELGETURU & " AND a_id = " & BELGEID
                If SQL_KOS(B_SQL, SQ, False) = True Then

                    BELGE(0) = New WS_UrunEkle
                    BELGE(0).DURUM = NUMARATOR.BAŞARILI
                    BELGE(0).BELGETURU = BELGETURU
                    BELGE(0).BARKOD = BARKOD
                    BELGE(0).AD = stokAdi
                    BELGE(0).KOD = stokKodu
                    BELGE(0).MIKTAR = MIKTAR
                    BELGE(0).MEVCUTMIKTAR = 0
                    Return BELGE


                End If

            ElseIf DT.Rows.Count > 0 Then 'UPDATE

                SQ = "UPDATE " & KUL.tper & "sipdet Set  a_mik =a_mik + " & MIKTAR & ", a_tutar = " & gecerliFiyat & " * (a_mik +  " & MIKTAR & "),a_sbrmmik=a_sbrmmik + " & MIKTAR
                SQ = SQ & "WHERE  a_id = " & BELGEID & "  And a_stok_id = " & STOKID

                Dim UPDET As Boolean = SQL_KOS(B_SQL, SQ, False)

                Dim TUTARTOPLAMI As Double
                SQ = ""
                SQ = "SELECT ISNULL(SUM(a_tutar),0) AS tutar FROM " & KUL.tper & "sipdet  WHERE a_tur = " & BELGETURU & " AND a_id = " & BELGEID
                DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                TUTARTOPLAMI = NULD(DT, 0, "tutar")
                SQ = ""
                SQ = "UPDATE " & KUL.tper & "sipmas SET a_tuttop = " & TUTARTOPLAMI & " WHERE  a_tur = " & BELGETURU & " AND a_id = " & BELGEID

                Dim UPMAS As Boolean = SQL_KOS(B_SQL, SQ, False)


                If UPDET = False And UPMAS = False Then

                    BELGE(0) = New WS_UrunEkle
                    BELGE(0).DURUM = NUMARATOR.HATA
                    BELGE(0).BELGETURU = 0
                    BELGE(0).BARKOD = " "
                    BELGE(0).AD = " "
                    BELGE(0).KOD = " "
                    BELGE(0).MIKTAR = 0
                    BELGE(0).MEVCUTMIKTAR = 0
                    Return BELGE

                Else

                    BELGE(0) = New WS_UrunEkle
                    BELGE(0).DURUM = NUMARATOR.BAŞARILI
                    BELGE(0).BELGETURU = BELGETURU
                    BELGE(0).BARKOD = BARKOD
                    BELGE(0).AD = stokAdi
                    BELGE(0).KOD = stokKodu
                    BELGE(0).MIKTAR = MIKTAR
                    BELGE(0).MEVCUTMIKTAR = mevcut_miktar
                    Return BELGE

                End If

                'If EKLEMETURU = 1 Then
                '    SQ = "UPDATE " & KUL.tper & "sipdet Set  a_mik =a_mik + " & MIKTAR & ", a_tutar = " & gecerliFiyat & " * (a_mik +  " & MIKTAR & "),a_sbrmmik=a_sbrmmik + " & MIKTAR
                '    SQ = SQ & "WHERE  a_id = " & BELGEID & "  And a_stok_id = " & STOKID
                '    If SQL_KOS(B_SQL, SQ, False) = True Then
                '        BELGE(0) = New WS_UrunEkle
                '        BELGE(0).DURUM = NUMARATOR.BAŞARILI
                '        BELGE(0).BELGETURU = BELGETURU
                '        BELGE(0).BARKOD = barkodd
                '        BELGE(0).AD = stokAdi
                '        BELGE(0).KOD = stokKodu
                '        BELGE(0).MIKTAR = MIKTAR
                '        BELGE(0).MEVCUTMIKTAR = mevcut_miktar
                '        Return BELGE
                '    Else

                '    End If
                'End If
                'If EKLEMETURU = 2 Then

                'SQ = ""
                'SQ = "INSERT INTO " & KUL.tper & "sipdet ( a_tur, a_id, a_sira, a_sirano, a_tarih, a_cari_id, a_stok_id, a_ambar_id, a_mik, a_brmfiy, a_kdv, a_kdvtut, a_isk, a_isktut, a_tutar, a_ack, a_bloke, a_kesin, a_sbrmmik, a_sbrm, a_sbrmcrp, a_sbrmfiy,a_did,a_dtut,a_dkur,a_kapamik)"
                'SQ = SQ & " Select " & BELGETURU & " AS a_tur, " & BELGEID & " AS a_id, " & SIRA & " AS a_sira," & SIRA & " as  a_sirano,Convert(VARCHAR(10), GETDATE(), 110) As a_tarih, 0 As a_cari_id, " & STOK_ID & "As a_stok_id, " & DEPOID & " As a_ambar_id , "

                'Select Case STOK.SKART.S99_Nerede_Buldun

                '    Case STOK_BILGI.Bul_Tur.Stok_1BrmBarkod
                '        birimBarkod = STOK.SKART.S26_a_1brmbar
                '        birimCarpan = STOK.SKART.S25_a_1brmcar
                '        birimfiyat = gecerliFiyat
                '        birim = STOK.SKART.S24_a_1birim
                '        gelenmiktar = birimCarpan * MIKTAR
                '        tutartoplam = MIKTAR * gecerliFiyat

                '        SQ = SQ & gelenmiktar & " As a_mik, " & birimfiyat & " As a_brmfiy, 0 As a_kdv, 0 As a_kdvtut, 0 As a_isk, 0 As a_isktut, " & tutartoplam & " As a_tutar, N'' AS a_ack, 0 AS a_bloke, 0 AS a_kesin, " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy,1 as a_did," & tutartoplam & "as a_dtut,1 as a_dkur,0 as a_kapamik  "
                '        SQL_KOS(B_SQL, SQ, False)
                '    Case STOK_BILGI.Bul_Tur.Stok_2BrmBarkod
                '        birimBarkod = STOK.SKART.S31_a_2brmbar
                '        birimCarpan = STOK.SKART.S30_a_2brmcar
                '        birimfiyat = gecerliFiyat
                '        birim = STOK.SKART.S29_a_2birim
                '        gelenmiktar = birimCarpan * MIKTAR
                '        tutartoplam = MIKTAR * gecerliFiyat
                '        SQ = SQ & gelenmiktar & " As a_mik, " & birimfiyat & " As a_brmfiy, 0 As a_kdv, 0 As a_kdvtut, 0 As a_isk, 0 As a_isktut, " & tutartoplam & " As a_tutar, N'' AS a_ack, 0 AS a_bloke, 0 AS a_kesin, " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy,1 as a_did," & tutartoplam & "as a_dtut,1 as a_dkur,0 as a_kapamik"
                '        SQL_KOS(B_SQL, SQ, False)

                '    Case STOK_BILGI.Bul_Tur.Stok_Barkod
                '        birimBarkod = STOK.SKART.S11_barkod
                '        birimCarpan = STOK.SKART.S98_Carpan
                '        birimfiyat = gecerliFiyat
                '        birim = STOK.SKART.S03_birim
                '        gelenmiktar = birimCarpan * MIKTAR
                '        tutartoplam = MIKTAR * gecerliFiyat
                '        SQ = SQ & gelenmiktar & " As a_mik, " & birimfiyat & " As a_brmfiy, 0 As a_kdv, 0 As a_kdvtut, 0 As a_isk, 0 As a_isktut, " & tutartoplam & " As a_tutar, N'' AS a_ack, 0 AS a_bloke, 0 AS a_kesin, " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy,1 as a_did," & tutartoplam & "as a_dtut,1 as a_dkur,0 as a_kapamik"
                '        SQL_KOS(B_SQL, SQ, False)
                '    Case STOK_BILGI.Bul_Tur.Stok_Kodu
                '        birimBarkod = STOK.SKART.S11_barkod
                '        birimCarpan = STOK.SKART.S98_Carpan
                '        birimfiyat = gecerliFiyat
                '        birim = STOK.SKART.S03_birim
                '        gelenmiktar = birimCarpan * MIKTAR
                '        tutartoplam = MIKTAR * gecerliFiyat
                '        SQ = SQ & gelenmiktar & " As a_mik, " & birimfiyat & " As a_brmfiy, 0 As a_kdv, 0 As a_kdvtut, 0 As a_isk, 0 As a_isktut, " & tutartoplam & " As a_tutar, N'' AS a_ack, 0 AS a_bloke, 0 AS a_kesin, " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy,1 as a_did," & tutartoplam & "as a_dtut,1 as a_dkur,0 as a_kapamik"
                '        SQL_KOS(B_SQL, SQ, False)
                '    Case STOK_BILGI.Bul_Tur.Stok_EkBarkod
                '        birimBarkod = STOK.SKART.S46_a_EK_brmbar
                '        birimCarpan = STOK.SKART.S45_a_EK_brmcar
                '        birimfiyat = gecerliFiyat
                '        birim = STOK.SKART.S44_a_EK_birim
                '        gelenmiktar = birimCarpan * MIKTAR
                '        tutartoplam = MIKTAR * gecerliFiyat
                '        SQ = SQ & gelenmiktar & " As a_mik, " & birimfiyat & " As a_brmfiy, 0 As a_kdv, 0 As a_kdvtut, 0 As a_isk, 0 As a_isktut, " & tutartoplam & " As a_tutar, N'' AS a_ack, 0 AS a_bloke, 0 AS a_kesin, " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy,1 as a_did," & tutartoplam & "as a_dtut,1 as a_dkur,0 as a_kapamik"
                '        SQL_KOS(B_SQL, SQ, False)

                '    Case Else
                '        BELGE(0) = New WS_UrunEkle
                '        BELGE(0).DURUM = NUMARATOR.HATA
                '        BELGE(0).BELGETURU = 0
                '        BELGE(0).BARKOD = " "
                '        BELGE(0).AD = " "
                '        BELGE(0).KOD = " "
                '        BELGE(0).MIKTAR = 0
                '        BELGE(0).MEVCUTMIKTAR = 0
                '        Return BELGE
                'End Select
                'Dim TUTARTOPLAMI As Double
                'SQ = ""
                'SQ = "SELECT ISNULL(SUM(a_tutar),0) AS tutar FROM " & KUL.tper & "sipdet  WHERE a_tur = " & BELGETURU & " AND a_id = " & BELGEID
                'DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                'TUTARTOPLAMI = NULD(DT, 0, "tutar")
                'SQ = ""
                'SQ = "UPDATE " & KUL.tper & "sipmas SET a_tuttop = " & TUTARTOPLAMI & " WHERE  a_tur = " & BELGETURU & " AND a_id = " & BELGEID
                'If SQL_KOS(B_SQL, SQ, False) = True Then

                '    BELGE(0) = New WS_UrunEkle
                '    BELGE(0).DURUM = NUMARATOR.BAŞARILI
                '    BELGE(0).BELGETURU = BELGETURU
                '    BELGE(0).BARKOD = barkodd
                '    BELGE(0).AD = stokAdi
                '    BELGE(0).KOD = stokKodu
                '    BELGE(0).MIKTAR = MIKTAR
                '    BELGE(0).MEVCUTMIKTAR = 0
                '    Return BELGE


                'End If
                'End If
            End If
        End If


#End Region

        BELGE(0) = New WS_UrunEkle
        BELGE(0).DURUM = NUMARATOR.DEGERDONMEDI
        BELGE(0).BELGETURU = " "
        BELGE(0).BARKOD = " "
        BELGE(0).AD = " "
        BELGE(0).KOD = " "
        BELGE(0).MIKTAR = 0
        BELGE(0).MEVCUTMIKTAR = 0

        Return BELGE
    End Function

#End Region




#Region "MB/Hata Mesajı"
    Public Class WS_Hata_Mesaji
        Public DURUM As Long
    End Class


    <WebMethod()>
    Public Function MB_HataMesaji(ByVal VERSION As String, ByVal SORGU As String) As WS_Hata_Mesaji()

        Dim SONUC As Boolean
        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()


        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"

            Dim STK(0) As WS_Hata_Mesaji
            STK(0) = New WS_Hata_Mesaji
            STK(0).DURUM = NUMARATOR.APKVERSIYONESKI




            Return STK
#End Region

        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

#Region "WS HATA"
            Dim STK(0) As WS_Hata_Mesaji
            STK(0) = New WS_Hata_Mesaji
            STK(0).DURUM = NUMARATOR.WSVERSIYONESKI




            Return STK

#End Region



        End If



        SONUC = SQL_KOS(SORGU, False)
        If SONUC = True Then

            Dim STK(0) As WS_Hata_Mesaji
            STK(0) = New WS_Hata_Mesaji
            STK(0).DURUM = NUMARATOR.BAŞARILI



            Return STK
        Else
            Dim STK(0) As WS_Hata_Mesaji
            STK(0) = New WS_Hata_Mesaji
            STK(0).DURUM = NUMARATOR.HATA

            Return STK
        End If

    End Function
#End Region


#Region "MB/Hızlı Stok Arama"
    Public Class WS_Hızlı_Arama
        Public DURUM As Long
        Public ID As Integer
        Public KOD As String
        Public AD As String
        Public BARKOD As String
        Public REFTİP As String
        Public FIYAT As Double
    End Class

    ''' <summary>
    ''' Stoğun barkoduna göre stok detaylarını getiriyoruz.
    ''' </summary>
    ''' <param name="BARKOD"></param>
    ''' <returns></returns>
    <WebMethod()>
    Public Function MB_HızlıStokAra(ByVal VERSION As String, ByVal BARKOD As String, ByVal FIYATTUR As Long, ByVal CARIID As Long) As WS_Hızlı_Arama()

        Dim SQ As String
        Dim DT As New DataTable
        B_SQL = connstr
        Dim URUNFIYAT As Double
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()


        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"

            Dim STK(0) As WS_Hızlı_Arama
            STK(0) = New WS_Hızlı_Arama
            STK(0).DURUM = NUMARATOR.APKVERSIYONESKI
            STK(0).ID = 0
            STK(0).AD = " "
            STK(0).KOD = " "
            STK(0).BARKOD = " "
            STK(0).REFTİP = " "
            STK(0).FIYAT = 0



            Return STK
#End Region

        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

#Region "WS HATA"
            Dim STK(0) As WS_Hızlı_Arama
            STK(0) = New WS_Hızlı_Arama
            STK(0).DURUM = NUMARATOR.WSVERSIYONESKI
            STK(0).ID = 0
            STK(0).AD = " "
            STK(0).KOD = " "
            STK(0).BARKOD = " "
            STK(0).REFTİP = " "
            STK(0).FIYAT = 0



            Return STK

#End Region



        End If

        SQ = " "
        SQ = "SELECT STOK.a_id AS STOKID,STOK.a_kod as STOKKOD,STOK.a_adi AS STOKADI,STOK.a_barkod AS STOKBARKOD, STOK.a_reftip AS STOKREF"
        SQ = SQ & " FROM " & KUL.tfrm & "stok as STOK "
        SQ = SQ & " LEFT JOIN " & KUL.tfrm & "stoklot as STOKLOT on STOK.a_id=STOKLOT.a_sid"
        SQ = SQ & " WHERE a_barkod='" & BARKOD & "' AND  a_akpas='Aktif' AND a_reftip NOT IN ('A')"
        SQ = SQ & " ORDER BY a_cdate"

        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)

        URUNFIYAT = FIYATBULMA(FIYATTUR, NULA(DT, 0, "STOKKOD"), 0)

        If DT.Rows.Count > 0 Then

            Dim STK(0) As WS_Hızlı_Arama
            STK(0) = New WS_Hızlı_Arama
            STK(0).DURUM = NUMARATOR.BAŞARILI
            STK(0).ID = NULN(DT, 0, "STOKID")
            STK(0).AD = NULA(DT, 0, "STOKADI")
            STK(0).KOD = NULA(DT, 0, "STOKKOD")
            STK(0).BARKOD = NULA(DT, 0, "STOKBARKOD")
            STK(0).REFTİP = NULA(DT, 0, "STOKREF")
            STK(0).FIYAT = URUNFIYAT



            Return STK
        Else
            Dim STK(0) As WS_Hızlı_Arama
            STK(0) = New WS_Hızlı_Arama
            STK(0).DURUM = NUMARATOR.HATA
            STK(0).ID = 0
            STK(0).AD = " "
            STK(0).KOD = " "
            STK(0).BARKOD = " "
            STK(0).REFTİP = " "
            STK(0).FIYAT = 0



            Return STK
        End If

    End Function
#End Region


#Region "BANKALAR"
    Public Class WS_BANKALAR
        Public DURUM As Long
        Public BANKAID As Integer
        Public BANKAAD As String
        Public BANKAKOD As String
        Public SUBEID As Long

    End Class

    <WebMethod()>
    Public Function MB_Bankalar(ByVal VERSION As String, ByVal USERID As Long, ByVal KULLANICIGRUPID As Long) As WS_BANKALAR()


        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"
            Dim VERI1(0) As WS_BANKALAR
            VERI1(0) = New WS_BANKALAR
            VERI1(0).DURUM = NUMARATOR.APKVERSIYONESKI
            VERI1(0).BANKAID = 0
            VERI1(0).BANKAAD = " "
            VERI1(0).BANKAKOD = " "
            VERI1(0).SUBEID = 0


            Return VERI1

#End Region

        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

#Region "WS HATA"
            Dim VERI1(0) As WS_BANKALAR
            VERI1(0) = New WS_BANKALAR
            VERI1(0).DURUM = NUMARATOR.WSVERSIYONESKI
            VERI1(0).BANKAID = 0
            VERI1(0).BANKAAD = " "
            VERI1(0).BANKAKOD = " "
            VERI1(0).SUBEID = 0
            Return VERI1

#End Region



        End If


        Dim SQ As String
        Dim DT As New DataTable

        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()

        'Yönetici ise tüm bankaları getir.

        If KULLANICIGRUPID = 1 Then
            SQ = ""
            SQ = "SELECT a_id,a_adi,a_kod,a_sube_id "
            SQ = SQ & " FROM " & KUL.tfrm & "banka "
            SQ = SQ & " WHERE a_akpas=0 "

            DT = SQL_TABLO(SQ, "T", False, False, False)
            If DT.Rows.Count > 0 Then
                Dim VERI1(DT.Rows.Count - 1) As WS_BANKALAR
                For X = 0 To DT.Rows.Count - 1

                    VERI1(X) = New WS_BANKALAR
                    VERI1(X).DURUM = NUMARATOR.BAŞARILI
                    VERI1(X).BANKAID = NULN(DT, X, "a_id")
                    VERI1(X).BANKAKOD = NULA(DT, X, "a_kod")
                    VERI1(X).BANKAAD = NULA(DT, X, "a_adi")
                    VERI1(X).SUBEID = NULN(DT, X, "a_sube_id")


                Next

                Return VERI1
            Else
                Dim VERI1(0) As WS_BANKALAR
                VERI1(0) = New WS_BANKALAR
                VERI1(0).DURUM = NUMARATOR.HATA
                VERI1(0).BANKAID = 0
                VERI1(0).BANKAKOD = " "
                VERI1(0).BANKAAD = " "
                VERI1(0).SUBEID = 0
                Return VERI1

            End If

        Else

            'Yönetici değilse yetkili olduğu bankaları getir.
            SQ = ""
            SQ = "SELECT BANKA.a_id,BANKA.a_adi,BANKA.a_kod,BANKA.a_sube_id "
            SQ = SQ & "  FROM " & KUL.tfrm & "sube_kul AS SubeKul "
            SQ = SQ & "  INNER JOIN " & KUL.tfrm & "sube_banka AS SUBEBANKA ON SubeKul.a_subeid = SUBEBANKA.a_subeid"
            SQ = SQ & "  INNER JOIN " & KUL.tfrm & "banka as BANKA ON SUBEBANKA.a_bankaid =BANKA.a_id"
            SQ = SQ & "  WHERE SubeKul.a_userid=" & USERID & ""
            SQ = SQ & " GROUP BY BANKA.a_id,BANKA.a_adi,BANKA.a_kod,BANKA.a_sube_id "

            DT = SQL_TABLO(SQ, "T", False, False, False)


            If DT.Rows.Count > 0 Then
                Dim VERI1(DT.Rows.Count - 1) As WS_BANKALAR
                For X = 0 To DT.Rows.Count - 1


                    VERI1(X) = New WS_BANKALAR
                    VERI1(X).DURUM = NUMARATOR.BAŞARILI
                    VERI1(X).BANKAID = NULN(DT, X, "a_id")
                    VERI1(X).BANKAKOD = NULA(DT, X, "a_kod")
                    VERI1(X).BANKAAD = NULA(DT, X, "a_adi")
                    VERI1(X).SUBEID = NULN(DT, X, "a_sube_id")

                Next

                Return VERI1
            Else
                Dim VERI1(0) As WS_BANKALAR
                VERI1(0) = New WS_BANKALAR
                VERI1(0).DURUM = NUMARATOR.HATA
                VERI1(0).BANKAID = 0
                VERI1(0).BANKAKOD = " "
                VERI1(0).BANKAAD = " "
                VERI1(0).SUBEID = 0
                Return VERI1

            End If

        End If


    End Function


#End Region


#Region "MB/Belge Detay"
    Public Class WS_BELGEDETAY
        Public DURUM As Long
        Public SATIRSAYISI As Long
        Public URUNADI As String
        Public URUNKODU As String
        Public URUNBARKODU As String
        Public URUNMİKTARI As Double
        Public URUNSIRA As Long
        Public TUTARTOPLAM As Double
        Public URUNID As Long
        Public TOPLAMMIKTAR As Double
        Public BIRIMFIYAT As Double
    End Class
    ''' <summary>
    ''' GELEN PARAMETRELERE GÖRE BELGE DETAYI DÖNDÜRÜLÜYOR.
    ''' </summary>
    ''' <param name="ISLEMTURU"></param>
    ''' <param name="TUR">(49,10 vb.)</param>
    ''' <param name="ID">Belge Id</param>
    ''' <returns></returns> 'LONG v DOUBLE TEMİZLENDİ
    <WebMethod()>
    Public Function MB_BelgeDetay(ByVal VERSION As String, ByVal ISLEMTURU As String, ByVal TUR As Long, ByVal ID As Long) As WS_BELGEDETAY()
        Dim SQ As String
        Dim DT As New DataTable
        Dim DT1 As New DataTable
        Dim DT2 As New DataTable
        Dim DT3 As New DataTable
        Dim SATIRSAYISI As Long
        Dim TOPLAMTUTAR As Double
        Dim TABLO As String
        Dim TABLO1 As String
        Dim URUNAD As String
        Dim URUNKOD As String
        Dim URUNBARKOD As String
        Dim URUNMIKTAR As Double
        Dim TOPLAM_MIKTAR As Double
        Dim DT4 As New DataTable
        B_SQL = connstr




        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"
            Dim VERI(0) As WS_BELGEDETAY
            VERI(0) = New WS_BELGEDETAY

            VERI(0).DURUM = NUMARATOR.APKVERSIYONESKI
            VERI(0).SATIRSAYISI = 0
            VERI(0).URUNSIRA = 0
            VERI(0).URUNADI = " "
            VERI(0).URUNID = 0
            VERI(0).URUNBARKODU = " "
            VERI(0).URUNKODU = 0
            VERI(0).URUNMİKTARI = 0
            VERI(0).TUTARTOPLAM = 0
            VERI(0).BIRIMFIYAT = 0
            Return VERI


#End Region

        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

#Region "WS HATA"
            Dim VERI(0) As WS_BELGEDETAY
            VERI(0) = New WS_BELGEDETAY

            VERI(0).DURUM = NUMARATOR.WSVERSIYONESKI
            VERI(0).SATIRSAYISI = 0
            VERI(0).URUNSIRA = 0
            VERI(0).URUNADI = " "
            VERI(0).URUNID = 0
            VERI(0).URUNBARKODU = " "
            VERI(0).URUNKODU = 0
            VERI(0).URUNMİKTARI = 0
            VERI(0).TUTARTOPLAM = 0
            VERI(0).BIRIMFIYAT = 0
            Return VERI


#End Region



        End If


        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()

        Select Case TUR
            Case 10, 11, 12, 13, 14, 16, 70, 75, 40, 41, 42, 95, 96
                TABLO = "stkcdet"
                TABLO1 = "stkcmas"
            Case 17, 18, 19, 20, 21, 22, 54, 71, 87, 88, 43, 44, 69
                TABLO = "stkgdet"
                TABLO1 = "stkgmas"

            Case Else
                Dim VERI(0) As WS_BELGEDETAY
                VERI(0) = New WS_BELGEDETAY

                VERI(0).DURUM = NUMARATOR.HATA
                VERI(0).SATIRSAYISI = 0
                VERI(0).URUNSIRA = 0
                VERI(0).URUNADI = " "
                VERI(0).URUNID = 0
                VERI(0).URUNBARKODU = " "
                VERI(0).URUNKODU = 0
                VERI(0).URUNMİKTARI = 0
                VERI(0).TUTARTOPLAM = 0
                VERI(0).BIRIMFIYAT = 0


        End Select
        If TUR <> 49 And TUR <> 4 And TUR <> 33 And TUR <> 34 Then

            SQ = "SELECT  stk.a_adi AS AD,STK.a_kod AS KOD,STK.a_barkod AS BARKOD,STHD.a_mik AS MIKTAR,STHD.a_sira as SIRA,STK.a_id as STOKID,CAST(STHD.a_brmfiy as DECIMAL(25,4)) as BIRIMFIYAT FROM " & KUL.tfrm & "stok as STK INNER JOIN " & KUL.tper & TABLO & " as STHD "
            SQ = SQ & " ON STK.a_id=STHD.a_stok_id "
            SQ = SQ & " where STHD.a_tur=" & TUR & " and STHD.a_id=" & ID
            SQ = SQ & " ORDER BY a_sira DESC"
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            URUNAD = NULA(DT, 0, "AD")
            URUNKOD = NULA(DT, 0, "KOD")
            URUNBARKOD = NULA(DT, 0, "BARKOD")
            URUNMIKTAR = NULD(DT, 0, "MIKTAR")
            SQ = ""
            SQ = "Select count(*) As MIKTAR FROM " & KUL.tper & TABLO & " WHERE a_id=" & ID & " And a_tur=" & TUR
            DT2 = SQL_TABLO(SQ, "T", False, False, False)
            SATIRSAYISI = NULN(DT2, 0, "MIKTAR")
            'BELGENİN TOPLAM TUTARI BULUNUYOR
            SQ = ""
            SQ = "Select CAST(a_tuttop as DECIMAL(25,4))  As TUTAR FROM " & KUL.tper & TABLO1 & " WHERE a_id=" & ID & " And a_tur=" & TUR
            DT3 = SQL_TABLO(SQ, "T", False, False, False)
            TOPLAMTUTAR = NULD(DT3, 0, "TUTAR")
            SQ = ""
            SQ = "Select SUM(a_mik) As MIKTAR FROM " & KUL.tper & TABLO & " WHERE a_id=" & ID & " And a_tur=" & TUR
            DT4 = SQL_TABLO(SQ, "T", False, False, False)
            TOPLAM_MIKTAR = NULD(DT4, 0, "MIKTAR")
            If DT.Rows.Count > 0 Then
                Dim VERI(DT.Rows.Count - 1) As WS_BELGEDETAY
                For X = 0 To DT.Rows.Count - 1
                    VERI(X) = New WS_BELGEDETAY
                    VERI(X).DURUM = NUMARATOR.BAŞARILI
                    VERI(X).SATIRSAYISI = SATIRSAYISI
                    VERI(X).URUNSIRA = NULN(DT, X, "SIRA")
                    VERI(X).URUNADI = NULA(DT, X, "AD")
                    VERI(X).URUNID = NULN(DT, X, "STOKID")
                    VERI(X).URUNBARKODU = NULA(DT, X, "BARKOD")
                    If VERI(X).URUNBARKODU = "" Then
                        VERI(X).URUNBARKODU = " "
                    Else
                        VERI(X).URUNBARKODU = NULA(DT, X, "BARKOD")
                    End If
                    VERI(X).URUNKODU = NULA(DT, X, "KOD")
                    VERI(X).URUNMİKTARI = NULD(DT, X, "MIKTAR")
                    VERI(X).BIRIMFIYAT = NULD(DT, X, "BIRIMFIYAT")
                    VERI(X).TUTARTOPLAM = TOPLAMTUTAR
                    VERI(X).TOPLAMMIKTAR = TOPLAM_MIKTAR
                Next
                Return VERI
            Else
                SQ = " SELECT a_id FROM " & KUL.tper & TABLO1 & " WHERE a_tur= " & TUR & " AND a_id=" & ID
                DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)

                If DT.Rows.Count = 0 Then
                    Dim VERI(0) As WS_BELGEDETAY
                    VERI(0) = New WS_BELGEDETAY
                    VERI(0).DURUM = NUMARATOR.HATA
                    VERI(0).SATIRSAYISI = 0
                    VERI(0).URUNSIRA = 0
                    VERI(0).URUNADI = " "
                    VERI(0).URUNID = 0
                    VERI(0).URUNBARKODU = " "
                    VERI(0).URUNKODU = 0
                    VERI(0).URUNMİKTARI = 0
                    VERI(0).TUTARTOPLAM = 0
                    VERI(0).TOPLAMMIKTAR = 0
                    VERI(0).BIRIMFIYAT = 0

                    Return VERI

                Else
                    Dim VERI(0) As WS_BELGEDETAY
                    VERI(0) = New WS_BELGEDETAY
                    VERI(0).DURUM = NUMARATOR.BOSBELGE
                    VERI(0).SATIRSAYISI = 0
                    VERI(0).URUNSIRA = 0
                    VERI(0).URUNADI = " "
                    VERI(0).URUNID = 0
                    VERI(0).URUNBARKODU = " "
                    VERI(0).URUNKODU = 0
                    VERI(0).URUNMİKTARI = 0
                    VERI(0).TUTARTOPLAM = 0
                    VERI(0).TOPLAMMIKTAR = 0
                    VERI(0).BIRIMFIYAT = 0

                    Return VERI
                End If

            End If
        End If



        If TUR = 49 Then
            'BELGENIN İÇİNDEKİ URUNLERIN DETAYLARI BULUNUYOR
            SQ = ""
            SQ = "Select STK.a_id As STOKID,STKH.a_sira As SIRA,STK.a_kod As KOD,STK.a_barkod As BARKOD,STK.a_adi As AD,STKH.a_mik As MIKTAR,CAST(STKH.a_brmfiy as DECIMAL(25,4)) as BIRIMFIYAT "
            SQ = SQ & " FROM " & KUL.tper & "stkhdet As STKH INNER JOIN " & KUL.tfrm & "stok As STK On STKH.a_stok_id=STK.a_id "
            SQ = SQ & " WHERE STKH.a_id=" & ID & " And STKH.a_tur=" & TUR & " ORDER BY a_sira desc "
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            'URUN MIKTARI BULUNUYOR
            SQ = ""
            SQ = "Select count(*) As MIKTAR FROM " & KUL.tper & "stkhdet WHERE a_id=" & ID & " And a_tur=" & TUR
            DT2 = SQL_TABLO(SQ, "T", False, False, False)
            SATIRSAYISI = NULN(DT2, 0, "MIKTAR")
            'BELGENİN TOPLAM TUTARI BULUNUYOR
            SQ = ""
            SQ = "Select CAST(a_tuttop as DECIMAL(25,4))  As TUTAR FROM " & KUL.tper & "stkhmas  WHERE a_id=" & ID & " And a_tur=" & TUR
            DT3 = SQL_TABLO(SQ, "T", False, False, False)
            TOPLAMTUTAR = NULD(DT3, 0, "TUTAR")
            SQ = ""
            SQ = "Select SUM(a_mik) As MIKTAR FROM " & KUL.tper & "stkhdet WHERE a_id=" & ID & " And a_tur=" & TUR
            DT4 = SQL_TABLO(SQ, "T", False, False, False)
            TOPLAM_MIKTAR = NULD(DT4, 0, "MIKTAR")

            If DT.Rows.Count > 0 Then
                Dim VERI(DT.Rows.Count - 1) As WS_BELGEDETAY
                For X = 0 To DT.Rows.Count - 1
                    VERI(X) = New WS_BELGEDETAY
                    VERI(X).DURUM = NUMARATOR.BAŞARILI
                    VERI(X).SATIRSAYISI = SATIRSAYISI
                    VERI(X).URUNSIRA = NULN(DT, X, "SIRA")
                    VERI(X).URUNADI = NULA(DT, X, "AD")
                    VERI(X).URUNID = NULN(DT, X, "STOKID")
                    VERI(X).URUNBARKODU = NULA(DT, X, "BARKOD")
                    If VERI(X).URUNBARKODU = "" Then
                        VERI(X).URUNBARKODU = " "
                    End If
                    VERI(X).URUNKODU = NULA(DT, X, "KOD")
                    VERI(X).URUNMİKTARI = NULD(DT, X, "MIKTAR")
                    VERI(X).BIRIMFIYAT = NULD(DT, X, "BIRIMFIYAT")
                    VERI(X).TUTARTOPLAM = TOPLAMTUTAR
                    VERI(X).TOPLAMMIKTAR = TOPLAM_MIKTAR
                Next
                Return VERI
            Else
                SQ = " SELECT a_id FROM " & KUL.tper & "stkhmas WHERE a_tur= " & TUR & " AND a_id=" & ID
                DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)

                If DT.Rows.Count = 0 Then
                    Dim VERI(0) As WS_BELGEDETAY
                    VERI(0) = New WS_BELGEDETAY
                    VERI(0).DURUM = NUMARATOR.HATA
                    VERI(0).SATIRSAYISI = 0
                    VERI(0).URUNSIRA = 0
                    VERI(0).URUNADI = " "
                    VERI(0).URUNID = 0
                    VERI(0).URUNBARKODU = " "
                    VERI(0).URUNKODU = 0
                    VERI(0).URUNMİKTARI = 0
                    VERI(0).TUTARTOPLAM = 0
                    VERI(0).TOPLAMMIKTAR = 0
                    VERI(0).BIRIMFIYAT = 0

                    Return VERI

                Else
                    Dim VERI(0) As WS_BELGEDETAY
                    VERI(0) = New WS_BELGEDETAY
                    VERI(0).DURUM = NUMARATOR.BOSBELGE
                    VERI(0).SATIRSAYISI = 0
                    VERI(0).URUNSIRA = 0
                    VERI(0).URUNADI = " "
                    VERI(0).URUNID = 0
                    VERI(0).URUNBARKODU = " "
                    VERI(0).URUNKODU = 0
                    VERI(0).URUNMİKTARI = 0
                    VERI(0).TUTARTOPLAM = 0
                    VERI(0).TOPLAMMIKTAR = 0
                    VERI(0).BIRIMFIYAT = 0
                    Return VERI
                End If

            End If
        End If
        If TUR = 4 Then
            'BELGENIN İÇİNDEKİ URUNLERIN DETAYLARI BULUNUYOR
            SQ = ""
            SQ = " Select STK.a_id As STOKID,STS.a_sira As SIRA,STK.a_kod As KOD,STK.a_barkod As BARKOD,STK.a_adi As AD,STS.a_sayilan As MIKTAR,CAST(STS.a_fiyat as DECIMAL(25,4)) as BIRIMFIYAT "
            SQ = SQ & " FROM " & KUL.tper & "stsaydet As STS INNER JOIN " & KUL.tfrm & "stok As STK On STS.a_stok_id=STK.a_id "
            SQ = SQ & " WHERE STS.a_id=" & ID & "order by  STS.a_sira DESC"
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            'URUN MIKTARI BULUNUYOR
            SQ = ""
            SQ = "Select count(*) As MIKTAR FROM " & KUL.tper & "stsaydet WHERE a_id=" & ID
            DT2 = SQL_TABLO(SQ, "T", False, False, False)
            SATIRSAYISI = NULN(DT2, 0, "MIKTAR")
            SQ = ""
            SQ = "Select  sum(a_sayilan) As MIKTAR FROM " & KUL.tper & "stsaydet WHERE a_id=" & ID
            DT3 = SQL_TABLO(SQ, "T", False, False, False)
            TOPLAM_MIKTAR = NULD(DT3, 0, "MIKTAR")
            'BELGENİN TOPLAM TUTARI BULUNUYOR
            'SQ = ""
            'SQ = "Select a_tuttop As TUTAR FROM " & KUL.tper & "stsaymas  WHERE a_id=" & ID
            'DT3 = SQL_TABLO(SQ, "T", False, False, False)
            'TOPLAMTUTAR = NULD(DT3, 0, "TUTAR")
            'SQ = ""
            'SQ = "Select SUM(a_mik) As MIKTAR FROM " & KUL.tper & "stsaydet WHERE a_id=" & ID & " And a_tur=" & TUR
            'DT4 = SQL_TABLO(SQ, "T", False, False, False)
            'TOPLAM_MIKTAR = NULN(DT4, 0, "MIKTAR")
            If DT.Rows.Count > 0 Then
                Dim VERI(DT.Rows.Count - 1) As WS_BELGEDETAY
                For X = 0 To DT.Rows.Count - 1
                    VERI(X) = New WS_BELGEDETAY
                    VERI(X).DURUM = NUMARATOR.BAŞARILI
                    VERI(X).SATIRSAYISI = SATIRSAYISI
                    VERI(X).URUNSIRA = NULN(DT, X, "SIRA")
                    VERI(X).URUNADI = NULA(DT, X, "AD")
                    VERI(X).URUNID = NULN(DT, X, "STOKID")
                    VERI(X).URUNBARKODU = NULA(DT, X, "BARKOD")
                    If VERI(X).URUNBARKODU = "" Then
                        VERI(X).URUNBARKODU = " "
                    End If
                    VERI(X).URUNKODU = NULA(DT, X, "KOD")
                    VERI(X).URUNMİKTARI = NULD(DT, X, "MIKTAR")
                    VERI(X).BIRIMFIYAT = NULD(DT, X, "BIRIMFIYAT")
                    VERI(X).TUTARTOPLAM = 0.0
                    VERI(X).TOPLAMMIKTAR = TOPLAM_MIKTAR
                Next
                Return VERI
            Else
                SQ = " SELECT a_id FROM " & KUL.tper & "stsaymas WHERE a_id=" & ID
                DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)

                If DT.Rows.Count = 0 Then
                    Dim VERI(0) As WS_BELGEDETAY
                    VERI(0) = New WS_BELGEDETAY
                    VERI(0).DURUM = NUMARATOR.HATA
                    VERI(0).SATIRSAYISI = 0
                    VERI(0).URUNSIRA = 0
                    VERI(0).URUNADI = " "
                    VERI(0).URUNID = 0
                    VERI(0).URUNBARKODU = " "
                    VERI(0).URUNKODU = 0
                    VERI(0).URUNMİKTARI = 0
                    VERI(0).TUTARTOPLAM = 0
                    VERI(0).TOPLAMMIKTAR = 0
                    VERI(0).BIRIMFIYAT = 0
                    Return VERI

                Else
                    Dim VERI(0) As WS_BELGEDETAY
                    VERI(0) = New WS_BELGEDETAY
                    VERI(0).DURUM = NUMARATOR.BOSBELGE
                    VERI(0).SATIRSAYISI = 0
                    VERI(0).URUNSIRA = 0
                    VERI(0).URUNADI = " "
                    VERI(0).URUNID = 0
                    VERI(0).URUNBARKODU = " "
                    VERI(0).URUNKODU = 0
                    VERI(0).URUNMİKTARI = 0
                    VERI(0).TUTARTOPLAM = 0
                    VERI(0).TOPLAMMIKTAR = 0
                    VERI(0).BIRIMFIYAT = 0
                    Return VERI
                End If
            End If
        End If
        If TUR = 33 Or 34 Then

            SQ = "SELECT  STK.a_id as STOKID,stk.a_adi AS AD,STK.a_kod AS KOD,STK.a_barkod AS BARKOD,SIP.a_mik AS MIKTAR,SIP.a_sira as SIRA,CAST(SIP.a_brmfiy as DECIMAL(25,4)) as BIRIMFIYAT FROM " & KUL.tfrm & "stok as STK INNER JOIN " & KUL.tper & "sipdet as SIP "
            SQ = SQ & " ON STK.a_id=SIP.a_stok_id "
            SQ = SQ & " where SIP.a_tur=" & TUR & " and SIP.a_id=" & ID
            SQ = SQ & " ORDER BY a_sira DESC"
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            SQ = ""
            SQ = "Select count(*) As MIKTAR FROM " & KUL.tper & "sipdet  WHERE a_id=" & ID & " And a_tur=" & TUR
            DT2 = SQL_TABLO(SQ, "T", False, False, False)
            SATIRSAYISI = NULN(DT2, 0, "MIKTAR")

            SQ = ""
            SQ = "Select  sum(a_mik) As MIKTAR FROM " & KUL.tper & "sipdet WHERE a_id=" & ID
            DT3 = SQL_TABLO(SQ, "T", False, False, False)
            TOPLAM_MIKTAR = NULD(DT3, 0, "MIKTAR")

            'BELGENİN TOPLAM TUTARI BULUNUYOR
            SQ = ""
            SQ = "Select CAST(a_tuttop as DECIMAL(25,4))  As TUTAR  FROM " & KUL.tper & "sipmas WHERE a_id=" & ID & " And a_tur=" & TUR
            DT3 = SQL_TABLO(SQ, "T", False, False, False)
            TOPLAMTUTAR = NULD(DT3, 0, "TUTAR")
            If DT.Rows.Count > 0 Then
                Dim VERI(DT.Rows.Count - 1) As WS_BELGEDETAY
                For X = 0 To DT.Rows.Count - 1
                    VERI(X) = New WS_BELGEDETAY
                    VERI(X).DURUM = NUMARATOR.BAŞARILI
                    VERI(X).SATIRSAYISI = SATIRSAYISI
                    VERI(X).URUNSIRA = NULN(DT, X, "SIRA")
                    VERI(X).URUNADI = NULA(DT, X, "AD")
                    VERI(X).URUNID = NULN(DT, X, "STOKID")
                    VERI(X).URUNBARKODU = NULA(DT, X, "BARKOD")
                    If VERI(X).URUNBARKODU = "" Then
                        VERI(X).URUNBARKODU = " "
                    Else
                        VERI(X).URUNBARKODU = NULA(DT, X, "BARKOD")
                    End If
                    VERI(X).URUNKODU = NULA(DT, X, "KOD")
                    VERI(X).URUNMİKTARI = NULD(DT, X, "MIKTAR")
                    VERI(X).BIRIMFIYAT = NULD(DT, X, "BIRIMFIYAT")
                    VERI(X).TUTARTOPLAM = TOPLAMTUTAR
                    VERI(X).TOPLAMMIKTAR = TOPLAM_MIKTAR
                Next
                Return VERI
            Else
                SQ = " SELECT a_id FROM " & KUL.tper & "sipmas WHERE a_tur= " & TUR & " AND a_id=" & ID
                DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)

                If DT.Rows.Count = 0 Then
                    Dim VERI(0) As WS_BELGEDETAY
                    VERI(0) = New WS_BELGEDETAY
                    VERI(0).DURUM = NUMARATOR.HATA
                    VERI(0).SATIRSAYISI = 0
                    VERI(0).URUNSIRA = 0
                    VERI(0).URUNADI = " "
                    VERI(0).URUNID = 0
                    VERI(0).URUNBARKODU = " "
                    VERI(0).URUNKODU = 0
                    VERI(0).URUNMİKTARI = 0
                    VERI(0).TUTARTOPLAM = 0
                    VERI(0).TOPLAMMIKTAR = 0
                    VERI(0).BIRIMFIYAT = 0
                    Return VERI

                Else
                    Dim VERI(0) As WS_BELGEDETAY
                    VERI(0) = New WS_BELGEDETAY
                    VERI(0).DURUM = NUMARATOR.BOSBELGE
                    VERI(0).SATIRSAYISI = 0
                    VERI(0).URUNSIRA = 0
                    VERI(0).URUNADI = " "
                    VERI(0).URUNID = 0
                    VERI(0).URUNBARKODU = " "
                    VERI(0).URUNKODU = 0
                    VERI(0).URUNMİKTARI = 0
                    VERI(0).TUTARTOPLAM = 0
                    VERI(0).TOPLAMMIKTAR = 0
                    VERI(0).BIRIMFIYAT = 0
                    Return VERI
                End If
            End If
        End If

        Dim DEGER(0) As WS_BELGEDETAY
        DEGER(0) = New WS_BELGEDETAY
        DEGER(0).DURUM = NUMARATOR.DEGERDONMEDI
        DEGER(0).SATIRSAYISI = 0
        DEGER(0).URUNSIRA = 0
        DEGER(0).URUNADI = " "
        DEGER(0).URUNID = 0
        DEGER(0).URUNBARKODU = " "
        DEGER(0).URUNKODU = 0
        DEGER(0).URUNMİKTARI = 0
        DEGER(0).TUTARTOPLAM = 0
        DEGER(0).TOPLAMMIKTAR = 0
        DEGER(0).BIRIMFIYAT = 0
        Return DEGER
    End Function
#End Region
#Region "MB/Ürün Silme"
    Public Class WS_UrunSil
        Public DURUM As Long
    End Class
    ''' <summary>
    ''' GÖNDERİLEN PARAMETRELERE GÖRE BELGEDEKİ URUNLER SİLİNİYOR.
    ''' </summary>
    ''' <param name="BELGEID">BELGE ID</param>
    ''' <param name="BELGETURU">BELGENİN TURU(49,10 VS.)</param>
    ''' <param name="SATIR">SATIR NO</param>
    ''' <param name="STOKID">STOK ID</param>
    ''' <param name="DEPOID">ÇIKIŞ DEPO ID</param>
    ''' <param name="OPLANID">GİRİS DEPO ID</param>
    ''' <param name="MIKTAR">URUN MİKTARI</param>
    ''' <returns></returns> 'LONG v DOUBLE TEMİZLENDİ
    <WebMethod()>
    Public Function MB_UrunSil(ByVal VERSION As String, ByVal BELGEID As Long, ByVal BELGETURU As Long, ByVal SATIR As Long, ByVal STOKID As Long, ByVal DEPOID As Long, ByVal OPLANID As Long, ByVal MIKTAR As Double) As WS_BELGEDETAY()

        Dim KDV As Double
        Dim SQ As String
        Dim DT As New DataTable
        Dim GIREN As Double
        Dim CIKAN As Double
        Dim KALAN As Double
        Dim TUTAR As Double
        Dim URUNAD As String
        Dim URUNKOD As String
        Dim URUNBARKOD As String
        Dim URUNMIKTAR As Double
        Dim TOPLAMTUTAR As Double
        Dim TOPLAM_MIKTAR As Double

        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        Dim DEGER(0) As WS_BELGEDETAY


        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"
            DEGER(0) = New WS_BELGEDETAY
            DEGER(0).DURUM = NUMARATOR.APKVERSIYONESKI
            DEGER(0).SATIRSAYISI = 0
            DEGER(0).URUNSIRA = 0
            DEGER(0).URUNADI = " "
            DEGER(0).URUNID = 0
            DEGER(0).URUNBARKODU = " "
            DEGER(0).URUNKODU = 0
            DEGER(0).URUNMİKTARI = 0
            DEGER(0).TUTARTOPLAM = 0
            DEGER(0).TOPLAMMIKTAR = 0
            Return DEGER
#End Region

        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

#Region "WS HATA"
            DEGER(0) = New WS_BELGEDETAY
            DEGER(0).DURUM = NUMARATOR.WSVERSIYONESKI
            DEGER(0).SATIRSAYISI = 0
            DEGER(0).URUNSIRA = 0
            DEGER(0).URUNADI = " "
            DEGER(0).URUNID = 0
            DEGER(0).URUNBARKODU = " "
            DEGER(0).URUNKODU = 0
            DEGER(0).URUNMİKTARI = 0
            DEGER(0).TUTARTOPLAM = 0
            DEGER(0).TOPLAMMIKTAR = 0
            Return DEGER

#End Region



        End If


#Region "SAYIM"


        If BELGETURU = 4 Then
            SQ = ""
            SQ = "DELETE FROM " & KUL.tper & "stsaydet "
            SQ = SQ & " WHERE a_id=" & BELGEID & " And a_sira=" & SATIR & " and a_stok_id=" & STOKID
            If SQL_KOS(SQ, False) = True Then
                '    Dim BELGE(0) As WS_UrunSil
                '    BELGE(0) = New WS_UrunSil
                '    BELGE(0).DURUM = NUMARATOR.BAŞARILI
                '    Return BELGE
                'Else
                '    Dim BELGE(0) As WS_UrunSil
                '    BELGE(0) = New WS_UrunSil
                '    BELGE(0).DURUM = NUMARATOR.HATA
                '    Return BELGE
                Dim VERI As WS_BELGEDETAY()
                VERI = SayimBelgeDetayGetir(BELGEID)
                Return VERI
            Else
                Dim VERI As WS_BELGEDETAY()
                VERI = SayimBelgeDetayGetir(BELGEID)
                For Each x As WS_BELGEDETAY In VERI
                    x.DURUM = NUMARATOR.SILINEMEYENURUNVAR
                Next
                Return VERI
            End If
        End If
#End Region

#Region "SAYIM VE DTRF HARiCi"


        If BELGETURU <> 49 And BELGETURU <> 4 Then
            Dim TABLO As String = ""
            Dim TABLO1 As String = ""
            Select Case BELGETURU
                Case 10, 11, 12, 13, 14, 16, 70, 75, 40, 41, 42, 95, 96
                    TABLO = "stkcdet"
                    TABLO1 = "stkcmas"
                Case 17, 18, 19, 20, 21, 22, 54, 71, 87, 88, 43, 44, 69
                    TABLO = "stkgdet"
                    TABLO1 = "stkgmas"
                Case 33, 34
                    TABLO = "sipdet"
                    TABLO1 = "sipmas"
                Case Else
                    'Dim BELGE(0) As WS_UrunSil
                    'BELGE(0) = New WS_UrunSil
                    'BELGE(0).DURUM = NUMARATOR.HATA
                    'Return BELGE

                    Dim VERI(0) As WS_BELGEDETAY
                    VERI(0) = New WS_BELGEDETAY
                    VERI(0).DURUM = NUMARATOR.HATA
                    VERI(0).SATIRSAYISI = 0
                    VERI(0).URUNSIRA = 0
                    VERI(0).URUNADI = " "
                    VERI(0).URUNID = 0
                    VERI(0).URUNBARKODU = " "
                    VERI(0).URUNKODU = 0
                    VERI(0).URUNMİKTARI = 0
                    VERI(0).TUTARTOPLAM = 0
                    VERI(0).TOPLAMMIKTAR = 0
                    Return VERI

            End Select
            'BELGEDEKİ SEÇİLİ SATIRDAKİ TOPLAM TUTAR BULUNUYOR
            SQ = ""
            SQ = "Select a_tutar,a_kdvtut FROM " & KUL.tper & TABLO & " WHERE a_id=" & BELGEID & " And a_tur=" & BELGETURU & " And a_sira=" & SATIR
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            KDV = NULD(DT, 0, "a_kdvtut")
            TUTAR = NULD(DT, 0, "a_tutar")

            'SEÇİLİ SATIR SİLİNİYOR
            SQ = ""
            SQ = "DELETE FROM " & KUL.tper & TABLO & " "
            SQ = SQ & " WHERE a_id=" & BELGEID & " And a_tur=" & BELGETURU & " And a_sira=" & SATIR & " and a_stok_id=" & STOKID
            'STOKID ekle bu sorguya
            If SQL_KOS(SQ, False) = True Then
                'BELGE TUTARI GÜNCELLENİYOR
                SQ = ""
                SQ = " Update " & KUL.tper & TABLO1 & " "
                SQ = SQ & "Set a_tuttop = a_tuttop - " & TUTAR & ",a_kdvtop=a_kdvtop -" & KDV & " "
                SQ = SQ & " WHERE(a_tur = " & BELGETURU & ") And (a_id = " & BELGEID & ")"
                SQL_KOS(SQ, False)

                '    'Dim BELGE(0) As WS_UrunSil
                '    'BELGE(0) = New WS_UrunSil
                '    'BELGE(0).DURUM = NUMARATOR.BAŞARILI
                '    'Return BELGE

                'Else
                '    Dim BELGE(0) As WS_UrunSil
                '    BELGE(0) = New WS_UrunSil
                '    BELGE(0).DURUM = NUMARATOR.HATA
                '    Return BELGE

                'belge tutarı alınıyor
                SQ = ""
                SQ = "Select a_tuttop FROM " & KUL.tper & TABLO1 & " WHERE a_id= " & BELGEID & " And a_tur= " & BELGETURU & " "
                DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                TOPLAMTUTAR = NULD(DT, 0, "a_tuttop")


                'ürün silindi belge detayı alınıyor
                SQ = "SELECT  stk.a_adi AS AD,STK.a_kod AS KOD,STK.a_barkod AS BARKOD,STHD.a_mik AS MIKTAR, "
                SQ = SQ & " STHD.a_sira as SIRA,STK.a_id as STOKID,CAST(STHD.a_brmfiy as DECIMAL(25,4)) as BIRIMFIYAT "
                SQ = SQ & " FROM " & KUL.tfrm & "stok as STK INNER JOIN " & KUL.tper & TABLO & " as STHD "
                SQ = SQ & " ON STK.a_id=STHD.a_stok_id "
                SQ = SQ & " where STHD.a_tur=" & BELGETURU & " and STHD.a_id=" & BELGEID
                SQ = SQ & " ORDER BY a_sira DESC"
                DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                URUNAD = NULA(DT, 0, "AD")
                URUNKOD = NULA(DT, 0, "KOD")
                URUNBARKOD = NULA(DT, 0, "BARKOD")


                If DT.Rows.Count > 0 Then
                    'Belge detayı alınan datatable dan içindeki ürünlerin toplam adedini hesapla
                    TOPLAM_MIKTAR = DT.Compute("SUM(MIKTAR)", "")
                    Dim VERI(DT.Rows.Count - 1) As WS_BELGEDETAY
                    For X = 0 To DT.Rows.Count - 1
                        VERI(X) = New WS_BELGEDETAY
                        VERI(X).DURUM = NUMARATOR.BAŞARILI
                        VERI(X).SATIRSAYISI = DT.Rows.Count
                        VERI(X).URUNSIRA = NULN(DT, X, "SIRA")
                        VERI(X).URUNADI = NULA(DT, X, "AD")
                        VERI(X).URUNID = NULN(DT, X, "STOKID")
                        VERI(X).URUNBARKODU = NULA(DT, X, "BARKOD")
                        If VERI(X).URUNBARKODU = "" Then
                            VERI(X).URUNBARKODU = " "
                        End If
                        VERI(X).URUNKODU = NULA(DT, X, "KOD")
                        VERI(X).URUNMİKTARI = NULD(DT, X, "MIKTAR")
                        VERI(X).BIRIMFIYAT = NULD(DT, X, "BIRIMFIYAT")
                        VERI(X).TUTARTOPLAM = TOPLAMTUTAR
                        VERI(X).TOPLAMMIKTAR = TOPLAM_MIKTAR
                    Next
                    Return VERI
                Else

                    SQ = " SELECT a_id FROM " & KUL.tper & TABLO1 & " WHERE a_tur= " & BELGETURU & " AND a_id=" & BELGEID
                    DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)

                    If DT.Rows.Count = 0 Then
                        Dim VERI(0) As WS_BELGEDETAY
                        VERI(0) = New WS_BELGEDETAY
                        VERI(0).DURUM = NUMARATOR.HATA
                        VERI(0).SATIRSAYISI = 0
                        VERI(0).URUNSIRA = 0
                        VERI(0).URUNADI = " "
                        VERI(0).URUNID = 0
                        VERI(0).URUNBARKODU = " "
                        VERI(0).URUNKODU = 0
                        VERI(0).URUNMİKTARI = 0
                        VERI(0).TUTARTOPLAM = 0
                        VERI(0).TOPLAMMIKTAR = 0
                        Return VERI
                    Else
                        Dim VERI(0) As WS_BELGEDETAY
                        VERI(0) = New WS_BELGEDETAY
                        VERI(0).DURUM = NUMARATOR.BOSBELGE
                        VERI(0).SATIRSAYISI = 0
                        VERI(0).URUNSIRA = 0
                        VERI(0).URUNADI = " "
                        VERI(0).URUNID = 0
                        VERI(0).URUNBARKODU = " "
                        VERI(0).URUNKODU = 0
                        VERI(0).URUNMİKTARI = 0
                        VERI(0).TUTARTOPLAM = 0
                        VERI(0).TOPLAMMIKTAR = 0
                        Return VERI
                    End If

                End If
            Else
                Dim VERI As WS_BELGEDETAY()
                VERI = SatisKabulBelgeDetayGetir(BELGETURU, BELGEID)
                For Each x As WS_BELGEDETAY In VERI
                    x.DURUM = NUMARATOR.SILINEMEYENURUNVAR
                Next
                Return VERI
            End If


        End If
#End Region

#Region "DTRF"


        If BELGETURU = 49 Then
            'BELGEDEKİ SEÇİLİ SATIRDAKİ TOPLAM TUTAR BULUNUYOR
            SQ = ""
            SQ = "Select a_tutar FROM " & KUL.tper & "stkhdet WHERE a_id=" & BELGEID & " And a_tur=" & BELGETURU & " And a_sira=" & SATIR
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            TUTAR = NULD(DT, 0, "a_tutar")

            'SEÇİLİ SATIR SİLİNİYOR
            SQ = ""
            SQ = "DELETE FROM " & KUL.tper & "stkhdet "
            SQ = SQ & " WHERE a_id=" & BELGEID & " And a_tur=" & BELGETURU & " And a_sira=" & SATIR & " and a_stok_id=" & STOKID

            If SQL_KOS(SQ, False) = True Then
                'DEPODAKİ GİREN ÇIKAN VE KALAN MİKTARLARI BULUNUYOR
                SQ = ""
                SQ = "Select deg.a_ambid, deg.a_id, deg.a_giren As GIREN, deg.a_cikan As CIKAN, deg.a_kalan As KALAN, stk.a_reftip, stk.a_refstokid "
                SQ = SQ & " FROM  " & KUL.tfrm & "stokdeg As deg LEFT OUTER JOIN " & KUL.tfrm & "stok As stk On deg.a_id = stk.a_id "
                SQ = SQ & " WHERE(deg.a_ambid = " & DEPOID & ") And (deg.a_id = " & STOKID & ")"
                DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                GIREN = NULD(DT, 0, "GIREN")
                CIKAN = NULD(DT, 0, "CIKAN")
                KALAN = NULD(DT, 0, "KALAN")
                'MİKTARLAR GÜNCELLENİYOR
                SQ = ""
                SQ = " UPDATE " & KUL.tfrm & "stokdeg "
                SQ = SQ & " Set  a_giren = " & GIREN & ", a_cikan = " & CIKAN - MIKTAR & ", a_kalan = " & (GIREN) - (CIKAN - MIKTAR)
                SQ = SQ & " WHERE  a_ambid = " & DEPOID & " And a_id = " & STOKID
                SQL_KOS(SQ, False)
            Else
                'Dim BELGE(0) As WS_UrunSil
                'BELGE(0) = New WS_UrunSil
                'BELGE(0).DURUM = NUMARATOR.HATA
                'Return BELGE

                Dim VERI(0) As WS_BELGEDETAY
                VERI(0) = New WS_BELGEDETAY
                VERI(0).DURUM = NUMARATOR.HATA
                VERI(0).SATIRSAYISI = 0
                VERI(0).URUNSIRA = 0
                VERI(0).URUNADI = " "
                VERI(0).URUNID = 0
                VERI(0).URUNBARKODU = " "
                VERI(0).URUNKODU = 0
                VERI(0).URUNMİKTARI = 0
                VERI(0).TUTARTOPLAM = 0
                VERI(0).TOPLAMMIKTAR = 0
                Return VERI

            End If
            'BELGE TUTARI GÜNCELLENİYOR
            SQ = ""
            SQ = " Update " & KUL.tper & "stkhmas "
            SQ = SQ & "Set a_tuttop = a_tuttop - " & TUTAR
            SQ = SQ & " WHERE(a_tur = " & BELGETURU & ") And (a_id = " & BELGEID & ")"
            SQL_KOS(SQ, False)

            'DEPO TRANSFER GİRİŞTEN SİLİNİYOR
            SQ = ""
            SQ = "DELETE FROM " & KUL.tper & "stkhdet "
            SQ = SQ & " WHERE a_id=" & BELGEID & " And a_tur=50 And a_sira=" & SATIR & " and a_stok_id=" & STOKID

            If SQL_KOS(SQ, False) = True Then

                'SQ = ""
                'SQ = "Select count(*) As MIKTAR FROM " & KUL.tper & "stkhdet WHERE a_id=" & BELGEID & " And a_tur=" & BELGETURU
                'DT = SQL_TABLO(SQ, "T", False, False, False)
                'MIKTAR = NULN(DT, 0, "MIKTAR")
                'BELGENİN TOPLAM TUTARI BULUNUYOR
                SQ = ""
                SQ = "Select CAST(a_tuttop as DECIMAL(25,4))  As TUTAR FROM " & KUL.tper & "stkhmas WHERE a_id=" & BELGEID & " And a_tur=" & BELGETURU
                DT = SQL_TABLO(SQ, "T", False, False, False)
                TOPLAMTUTAR = NULD(DT, 0, "TUTAR")
                'SQ = ""
                'SQ = "Select SUM(a_mik) As MIKTAR FROM " & KUL.tper & "stkhdet WHERE a_id=" & BELGEID & " And a_tur=" & BELGETURU
                'DT = SQL_TABLO(SQ, "T", False, False, False)
                'TOPLAM_MIKTAR = NULD(DT, 0, "MIKTAR")

                SQ = "SELECT  stk.a_adi AS AD,STK.a_kod AS KOD,STK.a_barkod AS BARKOD,STHD.a_mik AS MIKTAR, "
                SQ = SQ & " STHD.a_sira as SIRA,STK.a_id as STOKID,CAST(STHD.a_brmfiy as DECIMAL(25,4)) as BIRIMFIYAT "
                SQ = SQ & " FROM " & KUL.tfrm & "stok as STK INNER JOIN " & KUL.tper & "stkhdet" & " as STHD "
                SQ = SQ & " ON STK.a_id=STHD.a_stok_id "
                SQ = SQ & " where STHD.a_tur=" & BELGETURU & " and STHD.a_id=" & BELGEID
                SQ = SQ & " ORDER BY a_sira DESC"
                DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)



                URUNAD = NULA(DT, 0, "AD")
                URUNKOD = NULA(DT, 0, "KOD")
                URUNBARKOD = NULA(DT, 0, "BARKOD")

                If DT.Rows.Count > 0 Then
                    TOPLAM_MIKTAR = DT.Compute("SUM(MIKTAR)", "")

                    Dim VERI(DT.Rows.Count - 1) As WS_BELGEDETAY
                    For X = 0 To DT.Rows.Count - 1
                        VERI(X) = New WS_BELGEDETAY
                        VERI(X).DURUM = NUMARATOR.BAŞARILI
                        VERI(X).SATIRSAYISI = DT.Rows.Count
                        VERI(X).URUNSIRA = NULN(DT, X, "SIRA")
                        VERI(X).URUNADI = NULA(DT, X, "AD")
                        VERI(X).URUNID = NULN(DT, X, "STOKID")
                        VERI(X).URUNBARKODU = NULA(DT, X, "BARKOD")
                        If VERI(X).URUNBARKODU = "" Then
                            VERI(X).URUNBARKODU = " "
                        End If
                        VERI(X).URUNKODU = NULA(DT, X, "KOD")
                        URUNMIKTAR = NULD(DT, X, "MIKTAR")
                        VERI(X).URUNMİKTARI = URUNMIKTAR 'NULD(DT, X, "MIKTAR")
                        VERI(X).BIRIMFIYAT = NULD(DT, X, "BIRIMFIYAT")
                        VERI(X).TUTARTOPLAM = TOPLAMTUTAR
                        VERI(X).TOPLAMMIKTAR = TOPLAM_MIKTAR
                    Next
                    Return VERI
                Else

                    SQ = " SELECT a_id FROM " & KUL.tper & "stkhmas WHERE a_tur= " & BELGETURU & " AND a_id=" & BELGEID
                    DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)

                    If DT.Rows.Count = 0 Then
                        Dim VERI(0) As WS_BELGEDETAY
                        VERI(0) = New WS_BELGEDETAY
                        VERI(0).DURUM = NUMARATOR.HATA
                        VERI(0).SATIRSAYISI = 0
                        VERI(0).URUNSIRA = 0
                        VERI(0).URUNADI = " "
                        VERI(0).URUNID = 0
                        VERI(0).URUNBARKODU = " "
                        VERI(0).URUNKODU = 0
                        VERI(0).URUNMİKTARI = 0
                        VERI(0).TUTARTOPLAM = 0
                        VERI(0).TOPLAMMIKTAR = 0
                        Return VERI
                        '    'DEPODAKİ GİREN ÇIKAN VE KALAN MİKTARLARI BULUNUYOR
                        '    SQ = ""
                        '    SQ = "Select deg.a_ambid, deg.a_id, deg.a_giren As GIREN, deg.a_cikan As CIKAN, deg.a_kalan As KALAN, stk.a_reftip, stk.a_refstokid "
                        '    SQ = SQ & "FROM  " & KUL.tfrm & "stokdeg As deg LEFT OUTER JOIN " & KUL.tfrm & "stok As stk On deg.a_id = stk.a_id"
                        '    SQ = SQ & " WHERE(deg.a_ambid = " & OPLANID & ") And (deg.a_id = " & STOKID & ")"
                        '    DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                        '    GIREN = NULN(DT, 0, "GIREN")
                        '    CIKAN = NULN(DT, 0, "CIKAN")
                        '    KALAN = NULN(DT, 0, "KALAN")

                        '    'MİKTARLAR GÜNCELLENİYOR.
                        '    SQ = ""
                        '    SQ = "UPDATE " & KUL.tfrm & "stokdeg "
                        '    SQ = SQ & "Set  a_giren = " & GIREN - MIKTAR & ", a_cikan = " & CIKAN & ", a_kalan = " & (GIREN - MIKTAR) - (CIKAN)
                        '    SQ = SQ & " WHERE  a_ambid = " & OPLANID & " And a_id = " & STOKID
                        '    SQL_KOS(SQ, False)

                        '    Dim BELGE(0) As WS_UrunSil
                        '    BELGE(0) = New WS_UrunSil
                        '    BELGE(0).DURUM = NUMARATOR.BAŞARILI
                        '    Return BELGE
                        'Else
                        '    Dim BELGE(0) As WS_UrunSil
                        '    BELGE(0) = New WS_UrunSil
                        '    BELGE(0).DURUM = NUMARATOR.HATA
                        '    Return BELGE

                    Else
                        Dim VERI(0) As WS_BELGEDETAY
                        VERI(0) = New WS_BELGEDETAY
                        VERI(0).DURUM = NUMARATOR.BOSBELGE
                        VERI(0).SATIRSAYISI = 0
                        VERI(0).URUNSIRA = 0
                        VERI(0).URUNADI = " "
                        VERI(0).URUNID = 0
                        VERI(0).URUNBARKODU = " "
                        VERI(0).URUNKODU = 0
                        VERI(0).URUNMİKTARI = 0
                        VERI(0).TUTARTOPLAM = 0
                        VERI(0).TOPLAMMIKTAR = 0
                        Return VERI
                    End If
                End If


            End If

            Dim BOSBELGE(0) As WS_BELGEDETAY
            BOSBELGE(0) = New WS_BELGEDETAY
            BOSBELGE(0).DURUM = NUMARATOR.HATA
            BOSBELGE(0).SATIRSAYISI = 0
            BOSBELGE(0).URUNSIRA = 0
            BOSBELGE(0).URUNADI = " "
            BOSBELGE(0).URUNID = 0
            BOSBELGE(0).URUNBARKODU = " "
            BOSBELGE(0).URUNKODU = 0
            BOSBELGE(0).URUNMİKTARI = 0
            BOSBELGE(0).TUTARTOPLAM = 0
            BOSBELGE(0).TOPLAMMIKTAR = 0
            Return BOSBELGE

        End If
#End Region
        DEGER(0) = New WS_BELGEDETAY
        DEGER(0).DURUM = NUMARATOR.DEGERDONMEDI
        DEGER(0).SATIRSAYISI = 0
        DEGER(0).URUNSIRA = 0
        DEGER(0).URUNADI = " "
        DEGER(0).URUNID = 0
        DEGER(0).URUNBARKODU = " "
        DEGER(0).URUNKODU = 0
        DEGER(0).URUNMİKTARI = 0
        DEGER(0).TUTARTOPLAM = 0
        DEGER(0).TOPLAMMIKTAR = 0
        Return DEGER
    End Function
#End Region


#Region "BELGEKAPAT BANKA"
    Public Class WS_BANKATAHSIL
        Public DURUM As Long


    End Class


    <WebMethod()>
    Public Function MB_BelgeKapat_Banka(ByVal VERSION As String, ByVal FATTUR As String, ByVal FISID As Long, ByVal SUBEID As Long, ByVal BANKAID As Integer, ByVal TUTAR As Double, ByVal CARIID As Long, ByVal FISACIKLAMA As String, ByVal TARIH As String) As WS_BANKATAHSIL()
        Dim VERI1(0) As WS_BANKATAHSIL


        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"
            VERI1(0) = New WS_BANKATAHSIL
            VERI1(0).DURUM = NUMARATOR.APKVERSIYONESKI


            Return VERI1

#End Region

        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

#Region "WS HATA"

            VERI1(0) = New WS_BANKATAHSIL
            VERI1(0).DURUM = NUMARATOR.WSVERSIYONESKI


            Return VERI1

#End Region



        End If


        Dim SQ As String
        Dim DT As New DataTable

        Dim BN_TUR As Long = 0
        Dim BN_ID As Long = 0
        Dim VTarih As String = ""
        Dim DTX As New DataTable
        Dim EBN As Long = 0
        Dim EVRAKADI As String = ""


        B_SQL = connstr
        SQL_TUR = "2005"




        If FATTUR = "17" Or FATTUR = "71" Or FATTUR = "13" Then

            BN_TUR = Islem_Turu.a074_Kredi_Karti_Alis_Bankadan_Odeme
            EVRAKADI = "Banka Ödeme"


        ElseIf FATTUR = "11" Or FATTUR = "70" Or FATTUR = "20" Then
            BN_TUR = Islem_Turu.a065_Kredi_Karti_Satis_Bankaya_Yatan
            EVRAKADI = "Banka Tahsil"


        End If

        BN_ID = 0
        VTarih = Vade_Tahsil_TarihiBul(BANKAID, TARIH)
        Dim XBT As New XBanka_Dekontlari
        XBT.V00_id = BN_ID
        XBT.V00_tur = BN_TUR
        XBT.V00_TurAdi = "Kredi Kartı Satış Bankaya Yatan"
        XBT.V01_Cari_Id = CARIID



        XBT.V01_Banka_Id = BANKAID
        XBT.V01_Kasa_Id = 0
        XBT.V01_VBanka_Id = 0
        XBT.V02_IslemNosu = ""
        XBT.V03_BelgeNosu = ""
        XBT.V04_RefId = CARIID

        XBT.V05_TahsilTarihi = TARIH
        XBT.V05_VadeTarihi = VTarih
        XBT.V05_Tarihi = TARIH
        XBT.V06_Aciklama = FISACIKLAMA
        XBT.V07_Tutar = TUTAR
        XBT.V08_IslemYeri = "BANKA"
        XBT.V09_YerTur = 0
        XBT.V10_YerId = 0
        XBT.V11_Dovid = 0
        XBT.V12_Dovkur = 1
        XBT.V13_Dovtut = TUTAR
        XBT.V14_MasMer = 0
        XBT.V14_MasYer = 0
        XBT.V15_SubeId = SUBEID
        'Select Case Val(Label59.Tag)
        '    Case -1
        XBT.V16_Cekim_Turu = "Tek"
        '    Case -3
        '        XBT.V16_Cekim_Turu = "Diğer Banka"
        '    Case Else
        '        XBT.V16_Cekim_Turu = "Taksit"
        'End Select

        'If Val(Label59.Tag) > 0 Then
        '        XBT.V17_Cekim_Taksit = Val(Label59.Tag)
        '    Else
        XBT.V17_Cekim_Taksit = 1
        '    End If

        XBT.V18_Bloke = 1
        BN_ID = XBT.Dekont_Ekle(BN_TUR)
        If BN_ID <> 0 Then

            Dim Fisler As New DataTable
            Dim KKSay As Integer = 0

            SQ = ""
            SQ = "SELECT a_tur, a_id, a_mebla"
            SQ = SQ & " FROM " & KUL.tper & "nakitgc"
            SQ = SQ & " WHERE a_tur=" & BN_TUR & " AND a_id=" & BN_ID & ""
            Fisler = SQL_TABLO(SQ, "T", False, False, False)



            For KKSay = 0 To Fisler.Rows.Count - 1
                SQ = ""
                SQ = "SELECT MAX(a_sira) AS EB"
                SQ = SQ & " FROM " & KUL.tper & "faturakapat"
                SQ = SQ & " WHERE(a_ftur = " & FATTUR & ") AND (a_fid = " & FISID & ")"
                DTX = SQL_TABLO(SQ, "T", False, False, False)
                EBN = NULN(DTX, 0, "EB") + 1

                SQ = "INSERT INTO " & KUL.tper & "faturakapat (a_ftur, a_fid, a_sira, a_evrak, a_refid, a_etur, a_eid, a_cstur, a_masterid,a_cekimturu,a_cekimtaksit, a_cihazid, a_tarih, a_ack, a_tutar)"
                SQ = SQ & " VALUES "
                SQ = SQ & " ( "
                SQ = SQ & " " & FATTUR & ", "
                SQ = SQ & " " & FISID & ", "
                SQ = SQ & " " & EBN & ", "
                SQ = SQ & " N'" & EVRAKADI & "', "
                SQ = SQ & " " & BANKAID & ", "
                SQ = SQ & " " & BN_TUR & ", "
                SQ = SQ & " " & BN_ID & ", "
                SQ = SQ & " '" & "" & "', "
                SQ = SQ & " " & 0 & ", "
                SQ = SQ & " N'" & "Tek" & "', "
                SQ = SQ & " " & 0 & ", "
                SQ = SQ & " " & 0 & ", "
                SQ = SQ & " '" & TTAR.TR2UK(TARIH) & "' , "
                SQ = SQ & " N'" & FISACIKLAMA & "', "
                'If GelenBankaId = 8 Then
                '    SQ = SQ & " N'" & "Puan Tahsilat Fatura İçin" & "', "
                'Else
                '    SQ = SQ & " N'" & "Banka Tahsilat Fatura İçin" & "', "
                'End If
                SQ = SQ & " " & TUTAR & " "
                SQ = SQ & " ) "
                SQL_KOS(SQ, False)
            Next


            VERI1(0) = New WS_BANKATAHSIL
            VERI1(0).DURUM = NUMARATOR.BAŞARILI

        Else



            VERI1(0) = New WS_BANKATAHSIL
            VERI1(0).DURUM = NUMARATOR.HATA



        End If



        Return VERI1

    End Function


#End Region


#Region "BELGEKAPAT KASA"
    Public Class WS_KASATAHSIL
        Public DURUM As Long


    End Class


    <WebMethod()>
    Public Function MB_BelgeKapat_Kasa(ByVal VERSION As String, ByVal FATTUR As String, ByVal FISID As Long, ByVal SUBEID As Long, ByVal KASAID As Integer, ByVal TUTAR As Double, ByVal CARIID As Long, ByVal FISACIKLAMA As String, ByVal TARIH As String) As WS_KASATAHSIL()
        Dim VERI1(0) As WS_KASATAHSIL


        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"
            VERI1(0) = New WS_KASATAHSIL
            VERI1(0).DURUM = NUMARATOR.APKVERSIYONESKI


            Return VERI1

#End Region

        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

#Region "WS HATA"

            VERI1(0) = New WS_KASATAHSIL
            VERI1(0).DURUM = NUMARATOR.WSVERSIYONESKI


            Return VERI1

#End Region



        End If


        Dim SQ As String
        Dim DT As New DataTable

        Dim KS_TUR As Long = 0
        Dim KS_ID As Long = 0
        Dim VTarih As String = ""
        Dim DTX As New DataTable
        Dim EBN As Long = 0
        Dim EVRAKADI As String = ""

        B_SQL = connstr
        SQL_TUR = "2005"




        If FATTUR = "17" Or FATTUR = "71" Or FATTUR = "13" Then

            KS_TUR = Islem_Turu.a030_Kasa_Tediye_Fisi
            EVRAKADI = "Kasa Tediye"

        ElseIf FATTUR = "11" Or FATTUR = "70" Or FATTUR = "20" Then
            KS_TUR = Islem_Turu.a029_Kasa_Tahsilat_Fisi
            EVRAKADI = "Kasa Tahsil"


        End If


        Dim XKT As New XKasa_Dekontlari
        XKT.V00_id = KASAID
        XKT.V00_tur = KS_TUR
        XKT.V00_TurAdi = "Kasa Tahsilat Fişi"

        XKT.V01_Cari_Id = CARIID


        XKT.V01_Kasa_Id = KASAID
        XKT.V01_VKasa_Id = 0
        XKT.V02_IslemNosu = ""
        XKT.V03_BelgeNosu = ""
        XKT.V04_RefId = CARIID
        XKT.V05_Tarihi = TARIH

        XKT.V05_VadeTarihi = TARIH
        XKT.V06_Aciklama = FISACIKLAMA
        'If X <> ListBox1.Items.Count - 1 Then
        '    XKT.V07_Tutar = Val(V_SIL(ListBox1.Items(X).ToString.Substring(32, 10).Trim))
        '    XKT.V11_Dovid = Val(ListBox1.Items(X).ToString.Substring(41, 20).Trim)
        '    XKT.V12_Dovkur = Val(V_SIL(ListBox1.Items(X).ToString.Substring(22, 10).Trim))
        '    XKT.V13_Dovtut = Val(V_SIL(ListBox1.Items(X).ToString.Substring(12, 10).Trim))
        'Else
        XKT.V07_Tutar = TUTAR
        XKT.V11_Dovid = 0
        XKT.V12_Dovkur = 1
        XKT.V13_Dovtut = TUTAR

        'XKT.V07_Tutar = Val(V_SIL(ListBox1.Items(X).ToString.Substring(32, 10).Trim)) + Val(L_Kalan.Text)
        'XKT.V11_Dovid = Val(ListBox1.Items(X).ToString.Substring(41, 20).Trim)
        'XKT.V12_Dovkur = Val(V_SIL(ListBox1.Items(X).ToString.Substring(22, 10).Trim))
        'XKT.V13_Dovtut = Val(V_SIL(ListBox1.Items(X).ToString.Substring(12, 10).Trim)) + (Val(L_Kalan.Text) / Val(V_SIL(ListBox1.Items(X).ToString.Substring(22, 10).Trim)))
        ''  End If
        XKT.V08_IslemYeri = "KASA"
        XKT.V09_YerTur = 0
        XKT.V10_YerId = 0
        XKT.V14_MasMer = 0
        XKT.V14_MasYer = 0
        XKT.V15_SubeId = SUBEID
        KS_ID = XKT.Dekont_Ekle(KS_TUR)

        If KS_ID <> 0 Then
            SQ = ""
            SQ = "SELECT MAX(a_sira) AS EB"
            SQ = SQ & " FROM " & KUL.tper & "faturakapat"
            SQ = SQ & " WHERE(a_ftur = " & FATTUR & ") AND (a_fid = " & FISID & ")"
            DTX = SQL_TABLO(SQ, "T", False, False, False)
            EBN = NULN(DTX, 0, "EB") + 1
            SQ = "INSERT INTO " & KUL.tper & "faturakapat (a_ftur, a_fid, a_sira, a_evrak, a_refid, a_etur, a_eid, a_cstur, a_masterid, a_cihazid, a_tarih, a_ack, a_tutar)"
            SQ = SQ & " VALUES "
            SQ = SQ & " ( "
            SQ = SQ & " " & FATTUR & ", "
            SQ = SQ & " " & FISID & ", "
            SQ = SQ & " " & EBN & ", "
            SQ = SQ & " N'" & EVRAKADI & "', "
            SQ = SQ & " " & KASAID & ", "
            SQ = SQ & " " & KS_TUR & ", "
            SQ = SQ & " " & KS_ID & ", "
            SQ = SQ & " '" & "" & "', "
            SQ = SQ & " " & 0 & ", "
            SQ = SQ & " " & 0 & ", "
            SQ = SQ & " '" & TTAR.TR2UK(TARIH) & "' , "
            SQ = SQ & " N'" & FISACIKLAMA & "', "
            '  If X <> ListBox1.Items.Count - 1 Then
            'SQ = SQ & " " & Val(V_SIL(ListBox1.Items(X).ToString.Substring(32, 10).Trim)) & " "
            'Else
            SQ = SQ & " " & TUTAR & " " ' Val(V_SIL(L_Tutar.Text)) & " "
            'End If
            SQ = SQ & " ) "
            SQL_KOS(SQ, False)

            VERI1(0) = New WS_KASATAHSIL
            VERI1(0).DURUM = NUMARATOR.BAŞARILI

        Else

            VERI1(0) = New WS_KASATAHSIL
            VERI1(0).DURUM = NUMARATOR.HATA
        End If




        Return VERI1

    End Function


#End Region


#Region "BELGEKAPAT LİSTE"
    Public Class WS_LISTE
        Public DURUM As Long
        Public EVRAKADI As String
        Public BELGENO As String
        Public ODEMEYERI As String
        Public TARIH As String
        Public TUTAR As String


    End Class


    <WebMethod()>
    Public Function MB_BelgeKapat_Liste(ByVal VERSION As String, ByVal FATTUR As Long, ByVal FISID As Long) As WS_LISTE()

        Dim SQ As String
        Dim DT As New DataTable
        Dim ODEMEYERI As String

        B_SQL = connstr
        SQL_TUR = "2005"

        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()


        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"



            Dim VERI(0) As WS_LISTE
            VERI(0) = New WS_LISTE
            VERI(0).DURUM = NUMARATOR.APKVERSIYONESKI
            VERI(0).EVRAKADI = " "
            VERI(0).BELGENO = " "
            VERI(0).ODEMEYERI = " "
            VERI(0).TARIH = " "
            VERI(0).TUTAR = " "



            Return VERI

#End Region

        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

#Region "WS HATA"

            Dim VERI(0) As WS_LISTE
            VERI(0) = New WS_LISTE
            VERI(0).DURUM = NUMARATOR.WSVERSIYONESKI
            VERI(0).EVRAKADI = " "
            VERI(0).BELGENO = " "
            VERI(0).ODEMEYERI = " "
            VERI(0).TARIH = " "
            VERI(0).TUTAR = " "

            Return VERI

#End Region



        End If




        SQ = ""
        SQ = "SELECT FK.a_evrak AS EVREAKADI,NGC.a_belgeno AS BELGENO, K.a_adi AS KASAADI, B.a_adi AS BANKAADI,FK.a_tarih AS TARIH,FK.a_tutar AS TUTAR"
        SQ = SQ & " FROM  " & KUL.tper & "faturakapat AS FK "
        SQ = SQ & " INNER JOIN " & KUL.tper & "nakitgc NGC ON FK.a_eid= NGC.a_id AND FK.a_etur = NGC.a_tur "
        SQ = SQ & " LEFT JOIN " & KUL.tfrm & "kasa AS K ON K.a_id=NGC.a_kasa_id "
        SQ = SQ & " LEFT JOIN " & KUL.tfrm & "banka AS B ON B.a_id=NGC.a_banka_id"
        SQ = SQ & " WHERE FK.a_ftur = '" & FATTUR & "' AND FK.a_fid='" & FISID & "' AND NGC.a_tur IN(30,29,65,74)"

        DT = SQL_TABLO(SQ, "T", False, False, False)

        If DT.Rows.Count > 0 Then


            Dim VERI(DT.Rows.Count - 1) As WS_LISTE

            For X = 0 To DT.Rows.Count - 1

                If NULA(DT, X, "KASAADI") = "" Then

                    ODEMEYERI = NULA(DT, X, "BANKAADI")
                Else
                    ODEMEYERI = NULA(DT, X, "KASAADI")


                End If


                VERI(X) = New WS_LISTE
                VERI(X).DURUM = NUMARATOR.BAŞARILI
                VERI(X).EVRAKADI = NULA(DT, X, "EVREAKADI")
                VERI(X).BELGENO = NULA(DT, X, "BELGENO")
                VERI(X).ODEMEYERI = ODEMEYERI
                VERI(X).TARIH = NULA(DT, X, "TARIH")
                VERI(X).TUTAR = NULA(DT, X, "TUTAR")
            Next





            Return VERI

        Else

            Dim VERI(0) As WS_LISTE
            VERI(0) = New WS_LISTE
            VERI(0).DURUM = NUMARATOR.HATA
            VERI(0).EVRAKADI = " "
            VERI(0).BELGENO = " "
            VERI(0).ODEMEYERI = " "
            VERI(0).TARIH = " "
            VERI(0).TUTAR = " "

            Return VERI

        End If





    End Function


#End Region


#Region "MB/Belge Kilit"
    Public Class WS_BelgeKilit
        Public DURUM As Long


    End Class
    ''' <summary>
    ''' GÖNDERİLEN PARAMETRELERE GÖRE ;
    ''' -BELGE KİLİDİ SİLİNİYOR
    ''' </summary>
    ''' <param name="ISLEM">ISLEM TÜRÜ(EKLEME,KONTROL,SILME)</param>
    ''' <param name="TUR">FİŞ TURU(49,10 VS.)</param>
    ''' <param name="ID">FİŞ ID</param>
    ''' <param name="KULLANICI">KULLANICI KODU</param>
    ''' <returns></returns>
    <WebMethod()>
    Public Function MB_BelgeKilitKaldır(ByVal ISLEM As String, ByVal TUR As Long, ByVal ID As Long, ByVal KULLANICI As Long) As WS_BelgeKilit()

        Dim SQ As String
        Dim DT As New DataTable
        B_SQL = connstr

        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()

        ''SQ = ""
        ''SQ = "DELETE FROM " & KUL.tfrm & "kilit"
        ''SQ = SQ & " WHERE (a_form = N'" & ISLEM & "') AND (a_tur = " & TUR & ") AND (a_id = " & ID & ") AND (a_sira = 0) AND (a_user = " & KULLANICI & ")"
        Dim BELGE(0) As WS_BelgeKilit
        '    If SQL_KOS(SQ, False) = True Then

        BELGE(0) = New WS_BelgeKilit
        BELGE(0).DURUM = NUMARATOR.BAŞARILI

        Return BELGE
        'Else
        '    BELGE(0) = New WS_BelgeKilit
        '    BELGE(0).DURUM = NUMARATOR.HATA

        '    Return BELGE

        'End If

    End Function

#End Region
#Region "MB/Belge Kilit Ekleme"
    Public Function MB_BelgeKilitEkle(ByVal ISLEM As String, ByVal TUR As Long, ByVal ID As Long, ByVal KULLANICI As Long) As Boolean
        'Dim SIRANO As Long
        Dim SQ As String
        Dim DT As New DataTable
        B_SQL = connstr

        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        SQ = ""
        SQ = "INSERT INTO " & KUL.tfrm & "kilit"
        SQ = SQ & " (a_form, a_tur, a_id, a_sira, a_user, a_tarih, a_saat)"
        SQ = SQ & " VALUES  (N'" & ISLEM & "', " & TUR & ", " & ID & ", 0 , " & KULLANICI & ", N'" & TTAR.TR2UK(Date.Now.ToShortDateString).ToString & "', N'" & Date.Now.ToShortTimeString & "')"


        If SQL_KOS(SQ, False) = True Then


            Return True
        Else

            Return False

        End If

    End Function
#End Region
#Region "MB/Stok Duzeltme Ve Ekleme"
    Public Class WS_StokIslem
        Public DURUM As Long
        Public MESAJ As String
        Public URUNFIYAT As Double
        Public STOKID As Long
        Public STOKKOD As String
        Public STOKAD As String
        Public STOKBARKOD As String
        Public BIRIM As String
        Public KDVDH As String = " "
        Public ALKDV As Double = 0
        Public SAKDV As Double = 0
        Public AFIYAT As Double = 0
        Public SFIYAT As Double
        Public OFIYAT1 As Double
        Public OFIYAT2 As Double
        Public OFIYAT3 As Double
        Public OFIYAT4 As Double
        Public OFIYAT5 As Double
        Public OFIYAT6 As Double
        Public USERID As Long
        Public BARTUR As String
    End Class

    ''' <summary>
    ''' Stoğun barkoduna göre stok detaylarını getiriyoruz.
    ''' </summary>
    ''' <param name="BARKOD"></param>
    ''' <returns></returns> LONG v DOUBLE TEMİZ
    <WebMethod()>
    Public Function MB_StokIslem(ByVal VERSION As String, ByVal STOKID As Long, ByVal STOKKODU As String, ByVal BARKOD As String, ByVal STOKADI As String, ByVal BIRIM As String, ByVal KDV As String, ByVal ALKDVORAN As Double, ByVal SATKDVORAN As Double, ByVal ALISFIYAT As Double, ByVal SATISFIYAT As Double, ByVal OFIYAT1 As Double, ByVal OFIYAT2 As Double, ByVal OFIYAT3 As Double, ByVal OFIYAT4 As Double, ByVal OFIYAT5 As Double, ByVal OFIYAT6 As Double, ByVal KOMUT As Long, ByVal USERID As Long, ByVal BARTUR As String) As WS_StokIslem()

        Dim SQ As String
        Dim DT As New DataTable
        Dim ALAN As String = ""
        Dim STOK As New X_Stok
        Dim ID As Long
        Dim MESAJ As String
        B_SQL = connstr


        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()


        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"
            Dim BELGE(0) As WS_StokIslem
            BELGE(0) = New WS_StokIslem
            BELGE(0).DURUM = NUMARATOR.APKVERSIYONESKI
            BELGE(0).MESAJ = MESAJ
            BELGE(0).URUNFIYAT = 0
            BELGE(0).STOKID = 0
            BELGE(0).STOKKOD = " "
            BELGE(0).STOKAD = " "
            BELGE(0).STOKBARKOD = " "
            BELGE(0).BIRIM = " "
            BELGE(0).KDVDH = " "
            BELGE(0).ALKDV = 0
            BELGE(0).SAKDV = 0
            BELGE(0).AFIYAT = 0
            BELGE(0).SFIYAT = 0
            BELGE(0).OFIYAT1 = 0
            BELGE(0).OFIYAT2 = 0
            BELGE(0).OFIYAT3 = 0
            BELGE(0).OFIYAT4 = 0
            BELGE(0).OFIYAT5 = 0
            BELGE(0).OFIYAT6 = 0
            BELGE(0).USERID = USERID
            BELGE(0).BARTUR = " "
            Return BELGE
#End Region

        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

#Region "WS HATA"
            Dim BELGE(0) As WS_StokIslem
            BELGE(0) = New WS_StokIslem
            BELGE(0).DURUM = NUMARATOR.WSVERSIYONESKI
            BELGE(0).MESAJ = MESAJ
            BELGE(0).URUNFIYAT = 0
            BELGE(0).STOKID = 0
            BELGE(0).STOKKOD = " "
            BELGE(0).STOKAD = " "
            BELGE(0).STOKBARKOD = " "
            BELGE(0).BIRIM = " "
            BELGE(0).KDVDH = " "
            BELGE(0).ALKDV = 0
            BELGE(0).SAKDV = 0
            BELGE(0).AFIYAT = 0
            BELGE(0).SFIYAT = 0
            BELGE(0).OFIYAT1 = 0
            BELGE(0).OFIYAT2 = 0
            BELGE(0).OFIYAT3 = 0
            BELGE(0).OFIYAT4 = 0
            BELGE(0).OFIYAT5 = 0
            BELGE(0).OFIYAT6 = 0
            BELGE(0).USERID = USERID
            BELGE(0).BARTUR = " "
            Return BELGE

#End Region



        End If
        '1 = STOK KARTI GÜNCELLEME
        If KOMUT = 1 Then
            If STOK.Kart_Bul(STOKID) = True Then
                STOK.Stok_Kodu = STOKKODU
                STOK.Stok_Barkod = BARKOD
                STOK.Stok_Adi = STOKADI
                STOK.Stok_Birim = BIRIM
                STOK.Stok_AlKdvOran = ALKDVORAN
                STOK.Stok_SatKdvOran = SATKDVORAN
                STOK.Stok_KdvDH = KDV
                STOK.Stok_AlFiyat = ALISFIYAT
                STOK.Stok_SatFiyat = SATISFIYAT
                STOK.Stok_OzelFiyat1 = OFIYAT1
                STOK.Stok_OzelFiyat2 = OFIYAT2
                'STOK.Stok_OzelFiyat3 = OFIYAT3
                'STOK.Stok_OzelFiyat4 = OFIYAT4
                'STOK.Stok_OzelFiyat5 = OFIYAT5
                'STOK.Stok_OzelFiyat6 = OFIYAT6
                STOK.Stok_BarkodTuru = BARTUR
                KUL.KOD = USERID
                STOK.Kart_Guncelle()




            End If


            MESAJ = STOK.Hata_Text
            If STOK.Hata_Text = "Kayıt Güncellenemedi" Then
                Dim BELGE(0) As WS_StokIslem
                BELGE(0) = New WS_StokIslem
                BELGE(0).DURUM = NUMARATOR.HATA
                BELGE(0).MESAJ = MESAJ
                BELGE(0).URUNFIYAT = 0
                BELGE(0).STOKID = 0
                BELGE(0).STOKKOD = " "
                BELGE(0).STOKAD = " "
                BELGE(0).STOKBARKOD = " "
                BELGE(0).BIRIM = " "
                BELGE(0).KDVDH = " "
                BELGE(0).ALKDV = 0
                BELGE(0).SAKDV = 0
                BELGE(0).AFIYAT = 0
                BELGE(0).SFIYAT = 0
                BELGE(0).OFIYAT1 = 0
                BELGE(0).OFIYAT2 = 0
                BELGE(0).OFIYAT3 = 0
                BELGE(0).OFIYAT4 = 0
                BELGE(0).OFIYAT5 = 0
                BELGE(0).OFIYAT6 = 0
                BELGE(0).USERID = USERID
                BELGE(0).BARTUR = " "
                Return BELGE

            ElseIf STOK.Hata_Text = "Stok kartı düzelme yetkiniz yok." Then
                Dim BELGE(0) As WS_StokIslem
                BELGE(0) = New WS_StokIslem
                BELGE(0).DURUM = NUMARATOR.HATA
                BELGE(0).MESAJ = MESAJ
                BELGE(0).URUNFIYAT = 0
                BELGE(0).STOKID = 0
                BELGE(0).STOKKOD = " "
                BELGE(0).STOKAD = " "
                BELGE(0).STOKBARKOD = " "
                BELGE(0).BIRIM = " "
                BELGE(0).KDVDH = " "
                BELGE(0).ALKDV = 0
                BELGE(0).SAKDV = 0
                BELGE(0).AFIYAT = 0
                BELGE(0).SFIYAT = 0
                BELGE(0).OFIYAT1 = 0
                BELGE(0).OFIYAT2 = 0
                BELGE(0).OFIYAT3 = 0
                BELGE(0).OFIYAT4 = 0
                BELGE(0).OFIYAT5 = 0
                BELGE(0).OFIYAT6 = 0
                BELGE(0).USERID = USERID
                BELGE(0).BARTUR = " "
                Return BELGE
            ElseIf STOK.Hata_Text = "Güncellenecek Stok bulunamadı." Then
                Dim BELGE(0) As WS_StokIslem
                BELGE(0) = New WS_StokIslem
                BELGE(0).DURUM = NUMARATOR.HATA
                BELGE(0).MESAJ = MESAJ
                BELGE(0).URUNFIYAT = 0
                BELGE(0).STOKID = 0
                BELGE(0).STOKKOD = " "
                BELGE(0).STOKAD = " "
                BELGE(0).STOKBARKOD = " "
                BELGE(0).BIRIM = " "
                BELGE(0).KDVDH = " "
                BELGE(0).ALKDV = 0
                BELGE(0).SAKDV = 0
                BELGE(0).AFIYAT = 0
                BELGE(0).SFIYAT = 0
                BELGE(0).OFIYAT1 = 0
                BELGE(0).OFIYAT2 = 0
                BELGE(0).OFIYAT3 = 0
                BELGE(0).OFIYAT4 = 0
                BELGE(0).OFIYAT5 = 0
                BELGE(0).OFIYAT6 = 0
                BELGE(0).USERID = USERID
                BELGE(0).BARTUR = " "
                Return BELGE

            ElseIf STOK.Hata_Text = "Kod tekrarı." Then
                Dim BELGE(0) As WS_StokIslem
                BELGE(0) = New WS_StokIslem
                BELGE(0).DURUM = NUMARATOR.HATA
                BELGE(0).MESAJ = MESAJ
                BELGE(0).URUNFIYAT = 0
                BELGE(0).STOKID = 0
                BELGE(0).STOKKOD = " "
                BELGE(0).STOKAD = " "
                BELGE(0).STOKBARKOD = " "
                BELGE(0).BIRIM = " "
                BELGE(0).KDVDH = " "
                BELGE(0).ALKDV = 0
                BELGE(0).SAKDV = 0
                BELGE(0).AFIYAT = 0
                BELGE(0).SFIYAT = 0
                BELGE(0).OFIYAT1 = 0
                BELGE(0).OFIYAT2 = 0
                BELGE(0).OFIYAT3 = 0
                BELGE(0).OFIYAT4 = 0
                BELGE(0).OFIYAT5 = 0
                BELGE(0).OFIYAT6 = 0
                BELGE(0).USERID = USERID
                BELGE(0).BARTUR = " "
                Return BELGE

            ElseIf STOK.Hata_Text = "Barkod başka bir stokta kullanılıyor." Then
                Dim BELGE(0) As WS_StokIslem
                BELGE(0) = New WS_StokIslem
                BELGE(0).DURUM = NUMARATOR.HATA
                BELGE(0).MESAJ = MESAJ
                BELGE(0).URUNFIYAT = 0
                BELGE(0).STOKID = 0
                BELGE(0).STOKKOD = " "
                BELGE(0).STOKAD = " "
                BELGE(0).STOKBARKOD = " "
                BELGE(0).BIRIM = " "
                BELGE(0).KDVDH = " "
                BELGE(0).ALKDV = 0
                BELGE(0).SAKDV = 0
                BELGE(0).AFIYAT = 0
                BELGE(0).SFIYAT = 0
                BELGE(0).OFIYAT1 = 0
                BELGE(0).OFIYAT2 = 0
                BELGE(0).OFIYAT3 = 0
                BELGE(0).OFIYAT4 = 0
                BELGE(0).OFIYAT5 = 0
                BELGE(0).OFIYAT6 = 0
                BELGE(0).USERID = USERID
                BELGE(0).BARTUR = " "
                Return BELGE

            ElseIf STOK.Hata_Text = "Stok kartı bulunamadı, başka bir kulanıcı tarafından silinmiş olabilir." Then
                Dim BELGE(0) As WS_StokIslem
                BELGE(0) = New WS_StokIslem
                BELGE(0).DURUM = NUMARATOR.HATA
                BELGE(0).MESAJ = MESAJ
                BELGE(0).URUNFIYAT = 0
                BELGE(0).STOKID = 0
                BELGE(0).STOKKOD = " "
                BELGE(0).STOKAD = " "
                BELGE(0).STOKBARKOD = " "
                BELGE(0).BIRIM = " "
                BELGE(0).KDVDH = " "
                BELGE(0).ALKDV = 0
                BELGE(0).SAKDV = 0
                BELGE(0).AFIYAT = 0
                BELGE(0).SFIYAT = 0
                BELGE(0).OFIYAT1 = 0
                BELGE(0).OFIYAT2 = 0
                BELGE(0).OFIYAT3 = 0
                BELGE(0).OFIYAT4 = 0
                BELGE(0).OFIYAT5 = 0
                BELGE(0).OFIYAT6 = 0
                BELGE(0).USERID = USERID
                BELGE(0).BARTUR = " "
                Return BELGE


            Else
                Dim BELGE(0) As WS_StokIslem
                BELGE(0) = New WS_StokIslem
                BELGE(0).DURUM = NUMARATOR.BAŞARILI
                BELGE(0).MESAJ = "BAŞARILI"
                BELGE(0).STOKID = STOK.Stok_ID
                BELGE(0).STOKAD = STOKADI
                BELGE(0).STOKKOD = STOKKODU
                BELGE(0).STOKBARKOD = BARKOD
                BELGE(0).BIRIM = BIRIM
                BELGE(0).KDVDH = KDV
                BELGE(0).ALKDV = ALKDVORAN
                BELGE(0).SAKDV = SATKDVORAN
                BELGE(0).AFIYAT = ALISFIYAT
                BELGE(0).SFIYAT = SATISFIYAT
                BELGE(0).OFIYAT1 = OFIYAT1
                BELGE(0).OFIYAT2 = OFIYAT2
                BELGE(0).OFIYAT3 = OFIYAT3
                BELGE(0).OFIYAT4 = OFIYAT4
                BELGE(0).OFIYAT5 = OFIYAT5
                BELGE(0).OFIYAT6 = OFIYAT6
                BELGE(0).USERID = USERID
                BELGE(0).BARTUR = BARTUR
                Return BELGE
            End If
        End If
        '2 = STOK KARTI EKLEME
        If KOMUT = 2 Then
            STOK.Stok_Kodu = STOKKODU
            STOK.Stok_Barkod = BARKOD
            STOK.Stok_Adi = STOKADI
            STOK.Stok_Akpas = "Aktif"
            STOK.Stok_Birim = BIRIM
            STOK.Stok_AlKdvOran = ALKDVORAN
            STOK.Stok_SatKdvOran = SATKDVORAN
            STOK.Stok_KdvDH = KDV
            STOK.Stok_AlFiyat = ALISFIYAT
            STOK.Stok_SatFiyat = SATISFIYAT
            STOK.Stok_Tur = "MALZEME"
            STOK.Stok_1BirimBarkod = ""
            STOK.Stok_2BirimBarkod = ""
            STOK.Stok_OzelFiyat1 = OFIYAT1
            STOK.Stok_OzelFiyat2 = OFIYAT2
            STOK.Stok_OzelFiyat3 = OFIYAT3
            STOK.Stok_OzelFiyat4 = OFIYAT4
            STOK.Stok_OzelFiyat5 = OFIYAT5
            STOK.Stok_OzelFiyat6 = OFIYAT6
            STOK.Stok_BarkodTuru = BARTUR
            STOK.Stok_ReferansTipi = "S"

            ID = STOK.Kart_Ekle()
            MESAJ = STOK.Hata_Text
            Dim BELGE(0) As WS_StokIslem
            If ID = 0 Then

                BELGE(0) = New WS_StokIslem
                BELGE(0).DURUM = NUMARATOR.HATA
                BELGE(0).MESAJ = MESAJ
                BELGE(0).URUNFIYAT = 0
                BELGE(0).STOKID = 0
                BELGE(0).STOKKOD = " "
                BELGE(0).STOKAD = " "
                BELGE(0).STOKBARKOD = " "
                BELGE(0).BIRIM = "ADET"
                BELGE(0).KDVDH = "0"
                BELGE(0).ALKDV = 0
                BELGE(0).SAKDV = 0
                BELGE(0).AFIYAT = 0
                BELGE(0).SFIYAT = 0
                BELGE(0).OFIYAT1 = 0
                BELGE(0).OFIYAT2 = 0
                BELGE(0).OFIYAT3 = 0
                BELGE(0).OFIYAT4 = 0
                BELGE(0).OFIYAT5 = 0
                BELGE(0).OFIYAT6 = 0
                BELGE(0).USERID = USERID
                BELGE(0).BARTUR = " "
                Return BELGE
            End If



            BELGE(0) = New WS_StokIslem
            BELGE(0).DURUM = NUMARATOR.BAŞARILI
            BELGE(0).MESAJ = "BAŞARILI"
            BELGE(0).STOKID = ID
            BELGE(0).STOKID = STOK.Stok_ID
            BELGE(0).STOKAD = STOKADI
            BELGE(0).STOKKOD = STOKKODU
            BELGE(0).STOKBARKOD = BARKOD
            BELGE(0).BIRIM = BIRIM
            BELGE(0).KDVDH = KDV
            BELGE(0).ALKDV = ALKDVORAN
            BELGE(0).SAKDV = SATKDVORAN
            BELGE(0).AFIYAT = ALISFIYAT
            BELGE(0).SFIYAT = SATISFIYAT
            BELGE(0).OFIYAT1 = OFIYAT1
            BELGE(0).OFIYAT2 = OFIYAT2
            BELGE(0).OFIYAT3 = OFIYAT3
            BELGE(0).OFIYAT4 = OFIYAT4
            BELGE(0).OFIYAT5 = OFIYAT5
            BELGE(0).OFIYAT6 = OFIYAT6
            BELGE(0).USERID = USERID
            BELGE(0).BARTUR = BARTUR
            Return BELGE
        End If
        '3 = STOK KARTI ARAMA
        If KOMUT = 3 Then
            SQ = " "
            SQ = " SELECT  STOK.a_id AS STOKID,STOK.a_adi as STOKADI,STOK.a_kod as STOKKODU,STOK.a_barkod as STOKBARKOD ,STOK.a_birim as BIRIM ,STOK.a_kdvdh as KDV ,STOK.a_afiyat as ALISFIYAT, "
            SQ = SQ & " STOK.a_sfiyat as SATISFIYAT,STOK.a_alkdvor as ALKDVOR,STOK.a_sakdvor as SAKDVOR,STOK.a_ofiyat1 as OFIYAT1,STOK.a_ofiyat2 as OFIYAT2,STOK.a_ofiyat3 as OFIYAT3,  "
            SQ = SQ & " STOK.a_ofiyat4 as OFIYAT4,STOK.a_ofiyat5 as OFIYAT5,STOK.a_ofiyat6 as OFIYAT6 ,STOK.a_bartip as BARTUR  "
            SQ = SQ & " FROM " & KUL.tfrm & "stok as STOK "
            SQ = SQ & " LEFT JOIN " & KUL.tfrm & "stoklot as STOKLOT on STOK.a_id=STOKLOT.a_sid"
            SQ = SQ & " WHERE  a_kod like  '%" & BARKOD & "%' AND  a_akpas='Aktif' AND a_reftip NOT IN ('A')"
            SQ = SQ & " ORDER BY a_cdate"

            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)


            If DT.Rows.Count > 0 Then
                STOKKODU = NULA(DT, 0, "STOKKODU")

                Dim STK(DT.Rows.Count - 1) As WS_StokIslem
                For X = 0 To DT.Rows.Count - 1

                    STK(X) = New WS_StokIslem
                    STK(X).DURUM = NUMARATOR.BAŞARILI
                    STK(X).MESAJ = "BAŞARILI"
                    STK(X).STOKID = NULN(DT, X, "STOKID")
                    STK(X).STOKAD = NULA(DT, X, "STOKADI")
                    STK(X).STOKKOD = NULA(DT, X, "STOKKODU")
                    STK(X).STOKBARKOD = NULA(DT, X, "STOKBARKOD")
                    STK(X).BIRIM = NULA(DT, X, "BIRIM")
                    STK(X).KDVDH = NULA(DT, X, "KDV")
                    STK(X).ALKDV = NULD(DT, X, "ALKDVOR")
                    STK(X).SAKDV = NULD(DT, X, "SAKDVOR")
                    STK(X).AFIYAT = NULD(DT, X, "ALISFIYAT")
                    STK(X).SFIYAT = NULD(DT, X, "SATISFIYAT")
                    STK(X).OFIYAT1 = NULD(DT, X, "OFIYAT1")
                    STK(X).OFIYAT2 = NULD(DT, X, "OFIYAT2")
                    STK(X).OFIYAT3 = NULD(DT, X, "OFIYAT3")
                    STK(X).OFIYAT4 = NULD(DT, X, "OFIYAT4")
                    STK(X).OFIYAT5 = NULD(DT, X, "OFIYAT5")
                    STK(X).OFIYAT6 = NULD(DT, X, "OFIYAT6")
                    STK(X).BARTUR = NULA(DT, X, "BARTUR")
                    STK(X).USERID = USERID
                    If STK(X).STOKBARKOD = Nothing Then
                        STK(X).STOKBARKOD = " "
                    End If
                    If STK(X).KDVDH = Nothing Then
                        STK(X).KDVDH = " "
                    End If
                Next
                Return STK

            End If

            SQ = " "
            SQ = " "
            SQ = " SELECT  STOK.a_id AS STOKID,STOK.a_adi as STOKADI,STOK.a_kod as STOKKODU,STOK.a_barkod as STOKBARKOD ,STOK.a_kdvdh as KDV ,STOK.a_birim as BIRIM ,STOK.a_afiyat as ALISFIYAT, "
            SQ = SQ & " STOK.a_sfiyat as SATISFIYAT,STOK.a_alkdvor as ALKDVOR,STOK.a_sakdvor as SAKDVOR,STOK.a_ofiyat1 as OFIYAT1,STOK.a_ofiyat2 as OFIYAT2,STOK.a_ofiyat3 as OFIYAT3,  "
            SQ = SQ & " STOK.a_ofiyat4 as OFIYAT4,STOK.a_ofiyat5 as OFIYAT5,STOK.a_ofiyat6 as OFIYAT6 ,STOK.a_bartip as BARTUR  "
            SQ = SQ & " FROM " & KUL.tfrm & "stok as STOK "
            SQ = SQ & " LEFT JOIN " & KUL.tfrm & "stoklot as STOKLOT on STOK.a_id=STOKLOT.a_sid"
            SQ = SQ & " WHERE a_barkod='" & BARKOD & "' AND  a_akpas='Aktif' AND a_reftip NOT IN ('A')"
            SQ = SQ & " ORDER BY a_cdate"

            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)


            If DT.Rows.Count > 0 Then
                STOKKODU = NULA(DT, 0, "STOKKODU")

                Dim STK(DT.Rows.Count - 1) As WS_StokIslem
                For X = 0 To DT.Rows.Count - 1

                    STK(X) = New WS_StokIslem
                    STK(X).DURUM = NUMARATOR.BAŞARILI
                    STK(X).MESAJ = "BAŞARILI"
                    STK(X).STOKID = NULN(DT, X, "STOKID")
                    STK(X).STOKAD = NULA(DT, X, "STOKADI")
                    STK(X).STOKKOD = NULA(DT, X, "STOKKODU")
                    STK(X).STOKBARKOD = NULA(DT, X, "STOKBARKOD")
                    STK(X).BIRIM = NULA(DT, X, "BIRIM")
                    STK(X).KDVDH = NULA(DT, X, "KDV")
                    STK(X).ALKDV = NULD(DT, X, "ALKDVOR")
                    STK(X).SAKDV = NULD(DT, X, "SAKDVOR")
                    STK(X).AFIYAT = NULD(DT, X, "ALISFIYAT")
                    STK(X).SFIYAT = NULD(DT, X, "SATISFIYAT")
                    STK(X).OFIYAT1 = NULD(DT, X, "OFIYAT1")
                    STK(X).OFIYAT2 = NULD(DT, X, "OFIYAT2")
                    STK(X).OFIYAT3 = NULD(DT, X, "OFIYAT3")
                    STK(X).OFIYAT4 = NULD(DT, X, "OFIYAT4")
                    STK(X).OFIYAT5 = NULD(DT, X, "OFIYAT5")
                    STK(X).OFIYAT6 = NULD(DT, X, "OFIYAT6")
                    STK(X).BARTUR = NULA(DT, X, "BARTUR")
                    STK(X).USERID = USERID
                    If STK(X).STOKBARKOD = Nothing Then
                        STK(X).STOKBARKOD = " "
                    End If
                    If STK(X).KDVDH = Nothing Then
                        STK(X).KDVDH = " "
                    End If

                Next
                Return STK

            End If

            SQ = " "
            SQ = " "
            SQ = " SELECT  STOK.a_id AS STOKID,STOK.a_adi as STOKADI,STOK.a_kod as STOKKODU,STOK.a_barkod as STOKBARKOD ,STOK.a_kdvdh as KDV ,STOK.a_afiyat as ALISFIYAT, "
            SQ = SQ & " STOK.a_sfiyat as SATISFIYAT,STOK.a_alkdvor as ALKDVOR,STOK.a_sakdvor as SAKDVOR,STOK.a_ofiyat1 as OFIYAT1,STOK.a_ofiyat2 as OFIYAT2,STOK.a_ofiyat3 as OFIYAT3,  "
            SQ = SQ & " STOK.a_ofiyat4 as OFIYAT4,STOK.a_ofiyat5 as OFIYAT5,STOK.a_ofiyat6 as OFIYAT6 ,STOK.a_bartip as BARTUR  "
            SQ = SQ & " FROM " & KUL.tfrm & "stok as STOK "
            SQ = SQ & " LEFT JOIN " & KUL.tfrm & "stoklot as STOKLOT on STOK.a_id=STOKLOT.a_sid"
            SQ = SQ & " WHERE  a_1brmbar='" & BARKOD & "' AND  a_akpas='Aktif' AND a_reftip NOT IN ('A')"
            SQ = SQ & " ORDER BY a_cdate"

            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)


            If DT.Rows.Count > 0 Then
                STOKKODU = NULA(DT, 0, "STOKKODU")

                Dim STK(DT.Rows.Count - 1) As WS_StokIslem
                For X = 0 To DT.Rows.Count - 1

                    STK(X) = New WS_StokIslem
                    STK(X).DURUM = NUMARATOR.BAŞARILI
                    STK(X).MESAJ = "BAŞARILI"
                    STK(X).STOKID = NULN(DT, X, "STOKID")
                    STK(X).STOKAD = NULA(DT, X, "STOKADI")
                    STK(X).STOKKOD = NULA(DT, X, "STOKKODU")
                    STK(X).STOKBARKOD = NULA(DT, X, "STOKBARKOD")
                    STK(X).BIRIM = NULA(DT, X, "BIRIM")
                    STK(X).KDVDH = NULA(DT, X, "KDV")
                    STK(X).ALKDV = NULD(DT, X, "ALKDVOR")
                    STK(X).SAKDV = NULD(DT, X, "SAKDVOR")
                    STK(X).AFIYAT = NULD(DT, X, "ALISFIYAT")
                    STK(X).SFIYAT = NULD(DT, X, "SATISFIYAT")
                    STK(X).OFIYAT1 = NULD(DT, X, "OFIYAT1")
                    STK(X).OFIYAT2 = NULD(DT, X, "OFIYAT2")
                    STK(X).OFIYAT3 = NULD(DT, X, "OFIYAT3")
                    STK(X).OFIYAT4 = NULD(DT, X, "OFIYAT4")
                    STK(X).OFIYAT5 = NULD(DT, X, "OFIYAT5")
                    STK(X).OFIYAT6 = NULD(DT, X, "OFIYAT6")
                    STK(X).BARTUR = NULA(DT, X, "BARTUR")
                    STK(X).USERID = USERID
                    If STK(X).STOKBARKOD = Nothing Then
                        STK(X).STOKBARKOD = " "
                    End If
                    If STK(X).KDVDH = Nothing Then
                        STK(X).KDVDH = " "
                    End If
                Next
                Return STK

            End If
            SQ = " "
            SQ = " SELECT  STOK.a_id AS STOKID,STOK.a_adi as STOKADI,STOK.a_kod as STOKKODU,STOK.a_barkod as STOKBARKOD ,STOK.a_kdvdh as KDV ,STOK.a_afiyat as ALISFIYAT, "
            SQ = SQ & " STOK.a_sfiyat as SATISFIYAT,STOK.a_alkdvor as ALKDVOR,STOK.a_sakdvor as SAKDVOR,STOK.a_ofiyat1 as OFIYAT1,STOK.a_ofiyat2 as OFIYAT2,STOK.a_ofiyat3 as OFIYAT3,  "
            SQ = SQ & " STOK.a_ofiyat4 as OFIYAT4,STOK.a_ofiyat5 as OFIYAT5,STOK.a_ofiyat6 as OFIYAT6 ,STOK.a_bartip as BARTUR  "
            SQ = SQ & " FROM " & KUL.tfrm & "stok as STOK "
            SQ = SQ & " LEFT JOIN " & KUL.tfrm & "stoklot as STOKLOT on STOK.a_id=STOKLOT.a_sid"
            SQ = SQ & " WHERE  a_2brmbar='" & BARKOD & "' AND  a_akpas='Aktif' AND a_reftip NOT IN ('A')"
            SQ = SQ & " ORDER BY a_cdate"

            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)


            If DT.Rows.Count > 0 Then
                STOKKODU = NULA(DT, 0, "STOKKODU")

                Dim STK(DT.Rows.Count - 1) As WS_StokIslem
                For X = 0 To DT.Rows.Count - 1

                    STK(X) = New WS_StokIslem
                    STK(X).DURUM = NUMARATOR.BAŞARILI
                    STK(X).MESAJ = "BAŞARILI"
                    STK(X).STOKID = NULN(DT, X, "STOKID")
                    STK(X).STOKAD = NULA(DT, X, "STOKADI")
                    STK(X).STOKKOD = NULA(DT, X, "STOKKODU")
                    STK(X).STOKBARKOD = NULA(DT, X, "STOKBARKOD")
                    STK(X).BIRIM = NULA(DT, X, "BIRIM")
                    STK(X).KDVDH = NULA(DT, X, "KDV")
                    STK(X).ALKDV = NULD(DT, X, "ALKDVOR")
                    STK(X).SAKDV = NULD(DT, X, "SAKDVOR")
                    STK(X).AFIYAT = NULD(DT, X, "ALISFIYAT")
                    STK(X).SFIYAT = NULD(DT, X, "SATISFIYAT")
                    STK(X).OFIYAT1 = NULD(DT, X, "OFIYAT1")
                    STK(X).OFIYAT2 = NULD(DT, X, "OFIYAT2")
                    STK(X).OFIYAT3 = NULD(DT, X, "OFIYAT3")
                    STK(X).OFIYAT4 = NULD(DT, X, "OFIYAT4")
                    STK(X).OFIYAT5 = NULD(DT, X, "OFIYAT5")
                    STK(X).OFIYAT6 = NULD(DT, X, "OFIYAT6")
                    STK(X).BARTUR = NULA(DT, X, "BARTUR")
                    STK(X).USERID = USERID
                    If STK(X).STOKBARKOD = Nothing Then
                        STK(X).STOKBARKOD = " "
                    End If
                    If STK(X).KDVDH = Nothing Then
                        STK(X).KDVDH = " "
                    End If
                Next
                Return STK


            End If
            SQ = " "
            SQ = " "
            SQ = " SELECT  STOK.a_id AS STOKID,STOK.a_adi as STOKADI,STOK.a_kod as STOKKODU,STOK.a_barkod as STOKBARKOD ,STOK.a_kdvdh as KDV ,STOK.a_afiyat as ALISFIYAT, "
            SQ = SQ & " STOK.a_sfiyat as SATISFIYAT,STOK.a_alkdvor as ALKDVOR,STOK.a_sakdvor as SAKDVOR,STOK.a_ofiyat1 as OFIYAT1,STOK.a_ofiyat2 as OFIYAT2,STOK.a_ofiyat3 as OFIYAT3,  "
            SQ = SQ & " STOK.a_ofiyat4 as OFIYAT4,STOK.a_ofiyat5 as OFIYAT5,STOK.a_ofiyat6 as OFIYAT6 ,STOK.a_bartip as BARTUR  "
            SQ = SQ & " FROM " & KUL.tfrm & "stok as STOK "
            SQ = SQ & " LEFT JOIN " & KUL.tfrm & "stoklot as STOKLOT on STOK.a_id=STOKLOT.a_sid"
            SQ = SQ & " WHERE  a_lotadi='" & BARKOD & "' AND  a_akpas='Aktif' AND a_reftip NOT IN ('A')"
            SQ = SQ & " ORDER BY a_cdate"

            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)


            If DT.Rows.Count > 0 Then
                STOKKODU = NULA(DT, 0, "STOKKODU")

                Dim STK(DT.Rows.Count - 1) As WS_StokIslem
                For X = 0 To DT.Rows.Count - 1

                    STK(X) = New WS_StokIslem
                    STK(X).DURUM = NUMARATOR.BAŞARILI
                    STK(X).MESAJ = "BAŞARILI"
                    STK(X).STOKID = NULN(DT, X, "STOKID")
                    STK(X).STOKAD = NULA(DT, X, "STOKADI")
                    STK(X).STOKKOD = NULA(DT, X, "STOKKODU")
                    STK(X).STOKBARKOD = NULA(DT, X, "STOKBARKOD")
                    STK(X).BIRIM = NULA(DT, X, "BIRIM")
                    STK(X).KDVDH = NULA(DT, X, "KDV")
                    STK(X).ALKDV = NULD(DT, X, "ALKDVOR")
                    STK(X).SAKDV = NULD(DT, X, "SAKDVOR")
                    STK(X).AFIYAT = NULD(DT, X, "ALISFIYAT")
                    STK(X).SFIYAT = NULD(DT, X, "SATISFIYAT")
                    STK(X).OFIYAT1 = NULD(DT, X, "OFIYAT1")
                    STK(X).OFIYAT2 = NULD(DT, X, "OFIYAT2")
                    STK(X).OFIYAT3 = NULD(DT, X, "OFIYAT3")
                    STK(X).OFIYAT4 = NULD(DT, X, "OFIYAT4")
                    STK(X).OFIYAT5 = NULD(DT, X, "OFIYAT5")
                    STK(X).OFIYAT6 = NULD(DT, X, "OFIYAT6")
                    STK(X).BARTUR = NULA(DT, X, "BARTUR")
                    STK(X).USERID = USERID
                    If STK(X).STOKBARKOD = Nothing Then
                        STK(X).STOKBARKOD = " "
                    End If
                    If STK(X).KDVDH = Nothing Then
                        STK(X).KDVDH = " "
                    End If

                Next
                Return STK

            End If

            SQ = " "
            SQ = " SELECT  STOK.a_id AS STOKID,STOK.a_adi as STOKADI,STOK.a_kod as STOKKODU,STOK.a_barkod as STOKBARKOD ,STOK.a_kdvdh as KDV ,STOK.a_afiyat as ALISFIYAT, "
            SQ = SQ & " STOK.a_sfiyat as SATISFIYAT,STOK.a_alkdvor as ALKDVOR,STOK.a_sakdvor as SAKDVOR,STOK.a_ofiyat1 as OFIYAT1,STOK.a_ofiyat2 as OFIYAT2,STOK.a_ofiyat3 as OFIYAT3,  "
            SQ = SQ & " STOK.a_ofiyat4 as OFIYAT4,STOK.a_ofiyat5 as OFIYAT5,STOK.a_ofiyat6 as OFIYAT6 ,STOK.a_bartip as BARTUR  "
            SQ = SQ & " FROM " & KUL.tfrm & "stok as STOK "
            SQ = SQ & " LEFT JOIN " & KUL.tfrm & "stoklot as STOKLOT on STOK.a_id=STOKLOT.a_sid"
            SQ = SQ & " WHERE  a_adi like '%" & BARKOD & "%' AND  a_akpas='Aktif' AND a_reftip NOT IN ('A')"
            SQ = SQ & " ORDER BY a_cdate"

            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)


            If DT.Rows.Count > 0 Then
                STOKKODU = NULA(DT, 0, "STOKKODU")

                Dim STK(DT.Rows.Count - 1) As WS_StokIslem
                For X = 0 To DT.Rows.Count - 1

                    STK(X) = New WS_StokIslem
                    STK(X).DURUM = NUMARATOR.BAŞARILI
                    STK(X).MESAJ = "BAŞARILI"
                    STK(X).STOKID = NULN(DT, X, "STOKID")
                    STK(X).STOKAD = NULA(DT, X, "STOKADI")
                    STK(X).STOKKOD = NULA(DT, X, "STOKKODU")
                    STK(X).STOKBARKOD = NULA(DT, X, "STOKBARKOD")
                    STK(X).BIRIM = NULA(DT, X, "BIRIM")
                    STK(X).KDVDH = NULA(DT, X, "KDV")
                    STK(X).ALKDV = NULD(DT, X, "ALKDVOR")
                    STK(X).SAKDV = NULD(DT, X, "SAKDVOR")
                    STK(X).AFIYAT = NULD(DT, X, "ALISFIYAT")
                    STK(X).SFIYAT = NULD(DT, X, "SATISFIYAT")
                    STK(X).OFIYAT1 = NULD(DT, X, "OFIYAT1")
                    STK(X).OFIYAT2 = NULD(DT, X, "OFIYAT2")
                    STK(X).OFIYAT3 = NULD(DT, X, "OFIYAT3")
                    STK(X).OFIYAT4 = NULD(DT, X, "OFIYAT4")
                    STK(X).OFIYAT5 = NULD(DT, X, "OFIYAT5")
                    STK(X).OFIYAT6 = NULD(DT, X, "OFIYAT6")
                    STK(X).BARTUR = NULA(DT, X, "BARTUR")
                    STK(X).USERID = USERID

                    If STK(X).STOKBARKOD = Nothing Then
                        STK(X).STOKBARKOD = " "
                    End If
                    If STK(X).KDVDH = Nothing Then
                        STK(X).KDVDH = " "
                    End If

                Next
                Return STK
            Else
                'BULUNAMADI
                Dim BELGE(0) As WS_StokIslem
                BELGE(0) = New WS_StokIslem
                BELGE(0).DURUM = NUMARATOR.HATA
                BELGE(0).MESAJ = "BAŞARISIZ"
                BELGE(0).URUNFIYAT = 0
                BELGE(0).STOKID = 0
                BELGE(0).STOKKOD = " "
                BELGE(0).STOKAD = " "
                BELGE(0).STOKBARKOD = " "
                BELGE(0).BIRIM = "ADET"
                BELGE(0).KDVDH = "0"
                BELGE(0).ALKDV = 0
                BELGE(0).SAKDV = 0
                BELGE(0).AFIYAT = 0
                BELGE(0).SFIYAT = 0
                BELGE(0).OFIYAT1 = 0
                BELGE(0).OFIYAT2 = 0
                BELGE(0).OFIYAT3 = 0
                BELGE(0).OFIYAT4 = 0
                BELGE(0).OFIYAT5 = 0
                BELGE(0).OFIYAT6 = 0
                BELGE(0).USERID = USERID
                BELGE(0).BARTUR = BARTUR
                Return BELGE


                Return BELGE
            End If


        End If

        Dim DEGER(0) As WS_StokIslem
        DEGER(0) = New WS_StokIslem
        DEGER(0).DURUM = NUMARATOR.HATA
        DEGER(0).MESAJ = "İŞLEM BAŞARISIZ"
        DEGER(0).URUNFIYAT = 0
        DEGER(0).STOKID = 0
        DEGER(0).STOKKOD = " "
        DEGER(0).STOKAD = " "
        DEGER(0).STOKBARKOD = " "
        DEGER(0).BIRIM = "ADET"
        DEGER(0).KDVDH = "0"
        DEGER(0).ALKDV = 0
        DEGER(0).SAKDV = 0
        DEGER(0).AFIYAT = 0
        DEGER(0).SFIYAT = 0
        DEGER(0).OFIYAT1 = 0
        DEGER(0).OFIYAT2 = 0
        DEGER(0).OFIYAT3 = 0
        DEGER(0).OFIYAT4 = 0
        DEGER(0).OFIYAT5 = 0
        DEGER(0).OFIYAT6 = 0
        DEGER(0).USERID = USERID
        DEGER(0).BARTUR = " "
        Return DEGER
    End Function
#End Region
#Region "MB/Etiket Yazdir"
    Public Class WS_Etiket_Yazdir
        Public DURUM As Long
    End Class 'LONG v DOUBLE TEMİZ

    <WebMethod()>
    Public Function MB_EtiketYazdir(ByVal VERSION As String, ByVal USERID As Long, ByVal SUBEID As Long, ByVal STOKBARDKODU As String, ByVal ETIKETID As Long, ByVal YAZICIID As Long) As WS_Etiket_Yazdir()

        Dim STOK As New STOK_BILGI
        Dim SQ As String
        Dim DT As New DataTable


        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"

            Dim BELGE(0) As WS_Etiket_Yazdir
            BELGE(0) = New WS_Etiket_Yazdir
            BELGE(0).DURUM = NUMARATOR.APKVERSIYONESKI

            Return BELGE
#End Region

        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

#Region "WS HATA"

            Dim BELGE(0) As WS_Etiket_Yazdir
            BELGE(0) = New WS_Etiket_Yazdir
            BELGE(0).DURUM = NUMARATOR.WSVERSIYONESKI

            Return BELGE

#End Region



        End If


        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        KUL.KOD = USERID
        Dim K As String = ""

        If STOK.BUL(STOKBARDKODU) = True Then






            K = KUL.KOD & "-" & Now.Year & "-" & Now.Month & "-" & Now.Day & " " & Now.Hour & ":" & Now.Minute & ":" & Now.Second & ":" & sifre_uret(8)

            SQ = ""
            SQ = "INSERT INTO " & KUL.tfrm & "mob_pserver( a_id, a_tur, a_pda,a_yazici_id, a_user,a_tarih, a_saat, a_komut, a_durum, a_sube,a_etiket_id ,a_kod, a_adi, a_birim,a_barkod, "
            SQ = SQ & " a_1birim, a_1brmcar, a_2birim, a_2brmcar, a_alkdvor, a_sakdvor, "
            SQ = SQ & "  a_afiyat, a_sfiyat,"
            SQ = SQ & "  a_grup1, a_grup2, a_grup3, a_aon, a_son, "
            SQ = SQ & "   a_ofiyat1, a_ofiyat2,a_ofiyat3, a_ofiyat4, a_ofiyat5, a_ofiyat6,"
            SQ = SQ & "  a_ykasa, a_bartip, a_grup4, a_grup5, a_grup6, "
            SQ = SQ & " a_1brmbar, a_1brmfiy, a_2brmbar, a_2brmfiy, a_kdvdh,"
            SQ = SQ & " a_kulrenk, a_kulbeden, a_kulkavala, a_beden,a_kavala, "
            SQ = SQ & " a_renkadi, a_reftip,"
            SQ = SQ & " a_ofiyisk1, a_ofiyisk2, a_ofiyisk3, a_ofiyisk4, a_ofiyisk5, a_ofiyisk6 ,a_xagir) "
            SQ = SQ & " VALUES "
            SQ = SQ & " ( "
            SQ = SQ & " '" & K & "', "
            SQ = SQ & " N'" & "MOBWAY" & "', "
            SQ = SQ & " N'', "
            SQ = SQ & " " & YAZICIID & " , "
            SQ = SQ & " " & USERID & " , "
            SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & "', "
            SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & " " & Now.Hour & ":" & Now.Minute & ":" & Now.Second & ":" & Now.Millisecond & "', "
            SQ = SQ & " N'YAZICI', "
            SQ = SQ & " '" & "H" & "', "
            SQ = SQ & "  " & SUBEID & " , "
            SQ = SQ & "  " & ETIKETID & ", "
            SQ = SQ & "  N'" & STOK.SKART.S01_kod & "', "
            SQ = SQ & "  N'" & STOK.SKART.S02_adi & "', "
            SQ = SQ & "  N'" & STOK.SKART.S03_birim & "' ,"
            SQ = SQ & "  N'" & STOK.SKART.S11_barkod & "' ,"
            SQ = SQ & "  N'" & STOK.SKART.S03_1birim & "' ,"
            SQ = SQ & "  " & STOK.SKART.S03_1brmcar & ", "
            SQ = SQ & "  N'" & STOK.SKART.S03_2birim & "', "
            SQ = SQ & "  " & STOK.SKART.S03_2brmcar & " ,"
            SQ = SQ & "  " & STOK.SKART.S07_alkdvor & " ,"
            SQ = SQ & "  " & STOK.SKART.S08_sakdvor & " ,"
            SQ = SQ & "  " & STOK.SKART.S05_afiyat & ", "
            SQ = SQ & "  " & STOK.SKART.S06_sfiyat & " ,"
            SQ = SQ & "  N'" & STOK.SKART.S49_a_grup1 & "', "
            SQ = SQ & "  N'" & STOK.SKART.S50_a_grup2 & "' ,"
            SQ = SQ & " N' " & STOK.SKART.S51_a_grup3 & "', "
            SQ = SQ & "  " & STOK.SKART.S13_aon & ", "
            SQ = SQ & "  " & STOK.SKART.S12_son & ", "
            SQ = SQ & "  " & STOK.SKART.F24_ofiyat1 & " ,"
            SQ = SQ & "  " & STOK.SKART.F25_ofiyat2 & " ,"
            SQ = SQ & "  " & STOK.SKART.F26_ofiyat3 & ", "
            SQ = SQ & "  " & STOK.SKART.F27_ofiyat4 & " ,"
            SQ = SQ & "  " & STOK.SKART.F28_ofiyat5 & " ,"
            SQ = SQ & "  " & STOK.SKART.F29_ofiyat6 & ", "
            SQ = SQ & "  N'" & STOK.SKART.S105_a_ykasa & "' ,"
            SQ = SQ & "  N'" & STOK.SKART.S103_a_bartip & "', "
            SQ = SQ & "  N'" & STOK.SKART.S52_a_grup4 & "', "
            SQ = SQ & "  N'" & STOK.SKART.S53_a_grup5 & "', "
            SQ = SQ & "  N'" & STOK.SKART.S54_a_grup6 & "' ,"
            SQ = SQ & "  N'" & STOK.SKART.S26_a_1brmbar & "' ,"
            SQ = SQ & "  " & STOK.SKART.S27_a_1brmfiy & ", "
            SQ = SQ & " N' " & STOK.SKART.S31_a_2brmbar & " ',"
            SQ = SQ & "  " & STOK.SKART.S32_a_2brmfiy & " ,"
            SQ = SQ & " N' " & STOK.SKART.S23_kdvdh & "', "
            SQ = SQ & " N' " & STOK.SKART.S34_a_kulrenk & "' ,"
            SQ = SQ & " N' " & STOK.SKART.S35_a_kulbeden & "', "
            SQ = SQ & "  N'" & STOK.SKART.S36_a_kulkavala & "', "
            SQ = SQ & "  N'" & STOK.SKART.S40_a_beden & "', "
            SQ = SQ & "  N'" & STOK.SKART.S42_a_kavala & "' ,"
            SQ = SQ & " N' " & STOK.SKART.S39_a_renkadi & "', "
            SQ = SQ & " N' " & STOK.SKART.S43_a_reftip & " ',"
            SQ = SQ & "  " & STOK.SKART.F30_ofiyisk1 & ", "
            SQ = SQ & "  " & STOK.SKART.F30_ofiyisk2 & ", "
            SQ = SQ & "  " & STOK.SKART.F30_ofiyisk3 & ", "
            SQ = SQ & "  " & STOK.SKART.F30_ofiyisk4 & " ,"
            SQ = SQ & "  " & STOK.SKART.F30_ofiyisk5 & ", "
            SQ = SQ & "  " & STOK.SKART.F30_ofiyisk6 & ", "
            SQ = SQ & "  " & STOK.SKART.S58_a_xagir & " "
            SQ = SQ & " ) "

            If SQL_KOS(SQ, False) = True Then
                Dim BELGE(0) As WS_Etiket_Yazdir
                BELGE(0) = New WS_Etiket_Yazdir
                BELGE(0).DURUM = NUMARATOR.BAŞARILI
                Return BELGE
            Else
                Dim BELGE(0) As WS_Etiket_Yazdir
                BELGE(0) = New WS_Etiket_Yazdir
                BELGE(0).DURUM = NUMARATOR.HATA
                Return BELGE
            End If
        Else
            Dim BELGE(0) As WS_Etiket_Yazdir
            BELGE(0) = New WS_Etiket_Yazdir
            BELGE(0).DURUM = NUMARATOR.UYARI
            Return BELGE
        End If



    End Function
#End Region
#Region "MB/Fiyat Bulma"
    Public Function FIYATBULMA(ByVal FIYATTUR As Long, ByVal STOKKOD As String, ByVal CARIKOD As String) As Double
        Dim ALAN As String
        If FIYATTUR = -1 Then

        End If


        ALAN = Format(FIYAT_BUL(FIYATTUR.ToString, STOKKOD, CARIKOD, Date.Now, True), "###,###.0000")
        Return ALAN




    End Function
#End Region

#Region "MB/Fiyat Gör"
    Public Class WS_FiyatGor
        Public DURUM As Long
        Public ID As Integer
        Public KOD As String
        Public AD As String
        Public BARKOD As String
        Public AFIYAT As Double
        Public SFIYAT As Double
        Public OFIYAT1 As Double
        Public OFIYAT2 As Double
        Public OFIYAT3 As Double
        Public OFIYAT4 As Double
        Public OFIYAT5 As Double
        Public OFIYAT6 As Double
        Public OMALIYET As Double
        Public SMALIYET As Double
    End Class

    ''' <summary>
    ''' Stoğun barkoduna göre stok fiyat detaylarını getiriyoruz.
    ''' </summary>
    ''' <param name="BARKOD"></param>
    ''' <returns></returns>
    <WebMethod()>
    Public Function MB_FiyatGor(ByVal VERSION As String, ByVal BARKOD As String) As WS_FiyatGor()

        Dim SQ As String
        Dim DT As New DataTable
        Dim ALAN As String = ""
        Dim KURUSHASSASIYET As Integer = 2
        B_SQL = connstr

        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()


        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"

            Dim STK(0) As WS_FiyatGor
            STK(0) = New WS_FiyatGor
            STK(0).DURUM = NUMARATOR.APKVERSIYONESKI
            STK(0).ID = 0
            STK(0).KOD = " "
            STK(0).AD = " "
            STK(0).BARKOD = " "
            STK(0).AFIYAT = 0
            STK(0).SFIYAT = 0
            STK(0).OFIYAT1 = 0
            STK(0).OFIYAT2 = 0
            STK(0).OFIYAT3 = 0
            STK(0).OFIYAT4 = 0
            STK(0).OFIYAT5 = 0
            STK(0).OFIYAT6 = 0
            STK(0).OMALIYET = 0
            STK(0).SMALIYET = 0

            Return STK
#End Region

        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

#Region "WS HATA"

            Dim STK(0) As WS_FiyatGor
            STK(0) = New WS_FiyatGor
            STK(0).DURUM = NUMARATOR.WSVERSIYONESKI
            STK(0).ID = 0
            STK(0).KOD = " "
            STK(0).AD = " "
            STK(0).BARKOD = " "
            STK(0).AFIYAT = 0
            STK(0).SFIYAT = 0
            STK(0).OFIYAT1 = 0
            STK(0).OFIYAT2 = 0
            STK(0).OFIYAT3 = 0
            STK(0).OFIYAT4 = 0
            STK(0).OFIYAT5 = 0
            STK(0).OFIYAT6 = 0
            STK(0).OMALIYET = 0
            STK(0).SMALIYET = 0

            Return STK

#End Region



        End If



        If Val(Parametre(-1, "059")) = 0 Then

            KURUSHASSASIYET = 2

        Else
            KURUSHASSASIYET = Val(Parametre(-1, "059"))
        End If


        SQ = " "
        SQ = "SELECT STOK.a_id AS STOKID,STOK.a_kod as STOKKOD,STOK.a_barkod AS STOKBARKOD,STOK.a_adi AS STOKAD,"
        SQ = SQ & "STOK.a_afiyat AS AFIYAT,STOK.a_sfiyat AS SFIYAT ,STOK.a_ofiyat1 AS OFIYAT1,"
        SQ = SQ & "STOK.a_ofiyat2 AS OFIYAT2,STOK.a_ofiyat3 AS OFIYAT3,STOK.a_ofiyat4 AS OFIYAT4,"
        SQ = SQ & "STOK.a_ofiyat5 AS OFIYAT5,STOK.a_ofiyat6 AS OFIYAT6,STOK.a_mayuragort AS OMALIYET,STOK.a_sonmaliyet AS SMALIYET"
        SQ = SQ & " FROM " & KUL.tfrm & "stok as STOK "
        SQ = SQ & " LEFT JOIN " & KUL.tfrm & "stoklot as STOKLOT on STOK.a_id=STOKLOT.a_sid"
        SQ = SQ & " WHERE a_barkod='" & BARKOD & "' AND  a_akpas='Aktif' AND a_reftip NOT IN ('A')"
        SQ = SQ & " ORDER BY a_cdate"

        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)


        If DT.Rows.Count > 0 Then
            Dim STK(0) As WS_FiyatGor


            STK(0) = New WS_FiyatGor
            STK(0).DURUM = NUMARATOR.BAŞARILI
            STK(0).ID = NULN(DT, 0, "STOKID")
            STK(0).KOD = NULA(DT, 0, "STOKKOD")
            STK(0).AD = NULA(DT, 0, "STOKAD")
            STK(0).BARKOD = NULA(DT, 0, "STOKBARKOD")
            STK(0).AFIYAT = kurus(NULD(DT, 0, "AFIYAT"), KURUSHASSASIYET)
            STK(0).SFIYAT = kurus(NULD(DT, 0, "SFIYAT"), KURUSHASSASIYET)
            STK(0).OFIYAT1 = kurus(NULD(DT, 0, "OFIYAT1"), KURUSHASSASIYET)
            STK(0).OFIYAT2 = kurus(NULD(DT, 0, "OFIYAT2"), KURUSHASSASIYET)
            STK(0).OFIYAT3 = kurus(NULD(DT, 0, "OFIYAT3"), KURUSHASSASIYET)
            STK(0).OFIYAT4 = kurus(NULD(DT, 0, "OFIYAT4"), KURUSHASSASIYET)
            STK(0).OFIYAT5 = kurus(NULD(DT, 0, "OFIYAT5"), KURUSHASSASIYET)
            STK(0).OFIYAT6 = kurus(NULD(DT, 0, "OFIYAT6"), KURUSHASSASIYET)
            STK(0).OMALIYET = kurus(NULD(DT, 0, "OMALIYET"), KURUSHASSASIYET)
            STK(0).SMALIYET = kurus(NULD(DT, 0, "SMALIYET"), KURUSHASSASIYET)


            Return STK

        End If

        SQ = " "
        SQ = "SELECT STOK.a_id AS STOKID,STOK.a_kod as STOKKOD,STOK.a_barkod AS STOKBARKOD,STOK.a_adi AS STOKAD,"
        SQ = SQ & "STOK.a_afiyat AS AFIYAT,STOK.a_sfiyat AS SFIYAT ,STOK.a_ofiyat1 AS OFIYAT1,"
        SQ = SQ & "STOK.a_ofiyat2 AS OFIYAT2,STOK.a_ofiyat3 AS OFIYAT3,STOK.a_ofiyat4 AS OFIYAT4,"
        SQ = SQ & "STOK.a_ofiyat5 AS OFIYAT5,STOK.a_ofiyat6 AS OFIYAT6,STOK.a_mayuragort AS OMALIYET,STOK.a_sonmaliyet AS SMALIYET"
        SQ = SQ & " FROM " & KUL.tfrm & "stok as STOK "
        SQ = SQ & " LEFT JOIN " & KUL.tfrm & "stoklot as STOKLOT on STOK.a_id=STOKLOT.a_sid"
        SQ = SQ & " WHERE  a_kod =  '" & BARKOD & "' AND  a_akpas='Aktif' AND a_reftip NOT IN ('A')"
        SQ = SQ & " ORDER BY a_cdate"

        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)


        If DT.Rows.Count > 0 Then
            Dim STK(0) As WS_FiyatGor

            STK(0) = New WS_FiyatGor
            STK(0).DURUM = NUMARATOR.BAŞARILI
            STK(0).ID = NULN(DT, 0, "STOKID")
            STK(0).KOD = NULA(DT, 0, "STOKKOD")
            STK(0).AD = NULA(DT, 0, "STOKAD")
            STK(0).BARKOD = NULA(DT, 0, "STOKBARKOD")
            STK(0).AFIYAT = kurus(NULD(DT, 0, "AFIYAT"), KURUSHASSASIYET)
            STK(0).SFIYAT = kurus(NULD(DT, 0, "SFIYAT"), KURUSHASSASIYET)
            STK(0).OFIYAT1 = kurus(NULD(DT, 0, "OFIYAT1"), KURUSHASSASIYET)
            STK(0).OFIYAT2 = kurus(NULD(DT, 0, "OFIYAT2"), KURUSHASSASIYET)
            STK(0).OFIYAT3 = kurus(NULD(DT, 0, "OFIYAT3"), KURUSHASSASIYET)
            STK(0).OFIYAT4 = kurus(NULD(DT, 0, "OFIYAT4"), KURUSHASSASIYET)
            STK(0).OFIYAT5 = kurus(NULD(DT, 0, "OFIYAT5"), KURUSHASSASIYET)
            STK(0).OFIYAT6 = kurus(NULD(DT, 0, "OFIYAT6"), KURUSHASSASIYET)
            STK(0).OMALIYET = kurus(NULD(DT, 0, "OMALIYET"), KURUSHASSASIYET)
            STK(0).SMALIYET = kurus(NULD(DT, 0, "SMALIYET"), KURUSHASSASIYET)


            Return STK

        End If



        SQ = " "
        SQ = "SELECT STOK.a_id AS STOKID,STOK.a_kod as STOKKOD,STOK.a_barkod AS STOKBARKOD,STOK.a_adi AS STOKAD,"
        SQ = SQ & "STOK.a_afiyat AS AFIYAT,STOK.a_sfiyat AS SFIYAT ,STOK.a_ofiyat1 AS OFIYAT1,"
        SQ = SQ & "STOK.a_ofiyat2 AS OFIYAT2,STOK.a_ofiyat3 AS OFIYAT3,STOK.a_ofiyat4 AS OFIYAT4,"
        SQ = SQ & "STOK.a_ofiyat5 AS OFIYAT5,STOK.a_ofiyat6 AS OFIYAT6,STOK.a_mayuragort AS OMALIYET,STOK.a_sonmaliyet AS SMALIYET"
        SQ = SQ & " FROM " & KUL.tfrm & "stok as STOK "
        SQ = SQ & " LEFT JOIN " & KUL.tfrm & "stoklot as STOKLOT on STOK.a_id=STOKLOT.a_sid"
        SQ = SQ & " WHERE  a_1brmbar='" & BARKOD & "' AND  a_akpas='Aktif' AND a_reftip NOT IN ('A')"
        SQ = SQ & " ORDER BY a_cdate"

        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)


        If DT.Rows.Count > 0 Then

            Dim STK(0) As WS_FiyatGor

            STK(0) = New WS_FiyatGor
            STK(0).DURUM = NUMARATOR.BAŞARILI
            STK(0).ID = NULN(DT, 0, "STOKID")
            STK(0).KOD = NULA(DT, 0, "STOKKOD")
            STK(0).AD = NULA(DT, 0, "STOKAD")
            STK(0).BARKOD = NULA(DT, 0, "STOKBARKOD")
            STK(0).AFIYAT = kurus(NULD(DT, 0, "AFIYAT"), KURUSHASSASIYET)
            STK(0).SFIYAT = kurus(NULD(DT, 0, "SFIYAT"), KURUSHASSASIYET)
            STK(0).OFIYAT1 = kurus(NULD(DT, 0, "OFIYAT1"), KURUSHASSASIYET)
            STK(0).OFIYAT2 = kurus(NULD(DT, 0, "OFIYAT2"), KURUSHASSASIYET)
            STK(0).OFIYAT3 = kurus(NULD(DT, 0, "OFIYAT3"), KURUSHASSASIYET)
            STK(0).OFIYAT4 = kurus(NULD(DT, 0, "OFIYAT4"), KURUSHASSASIYET)
            STK(0).OFIYAT5 = kurus(NULD(DT, 0, "OFIYAT5"), KURUSHASSASIYET)
            STK(0).OFIYAT6 = kurus(NULD(DT, 0, "OFIYAT6"), KURUSHASSASIYET)
            STK(0).OMALIYET = kurus(NULD(DT, 0, "OMALIYET"), KURUSHASSASIYET)
            STK(0).SMALIYET = kurus(NULD(DT, 0, "SMALIYET"), KURUSHASSASIYET)

            Return STK

        End If
        SQ = " "
        SQ = "SELECT STOK.a_id AS STOKID,STOK.a_kod as STOKKOD,STOK.a_barkod AS STOKBARKOD,STOK.a_adi AS STOKAD,"
        SQ = SQ & "STOK.a_afiyat AS AFIYAT,STOK.a_sfiyat AS SFIYAT ,STOK.a_ofiyat1 AS OFIYAT1,"
        SQ = SQ & "STOK.a_ofiyat2 AS OFIYAT2,STOK.a_ofiyat3 AS OFIYAT3,STOK.a_ofiyat4 AS OFIYAT4,"
        SQ = SQ & "STOK.a_ofiyat5 AS OFIYAT5,STOK.a_ofiyat6 AS OFIYAT6,STOK.a_mayuragort AS OMALIYET,STOK.a_sonmaliyet AS SMALIYET"
        SQ = SQ & " FROM " & KUL.tfrm & "stok as STOK "
        SQ = SQ & " LEFT JOIN " & KUL.tfrm & "stoklot as STOKLOT on STOK.a_id=STOKLOT.a_sid"
        SQ = SQ & " WHERE  a_2brmbar='" & BARKOD & "' AND  a_akpas='Aktif' AND a_reftip NOT IN ('A')"
        SQ = SQ & " ORDER BY a_cdate"

        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)


        If DT.Rows.Count > 0 Then
            Dim STK(0) As WS_FiyatGor

            STK(0) = New WS_FiyatGor
            STK(0).DURUM = NUMARATOR.BAŞARILI
            STK(0).ID = NULN(DT, 0, "STOKID")
            STK(0).KOD = NULA(DT, 0, "STOKKOD")
            STK(0).AD = NULA(DT, 0, "STOKAD")
            STK(0).BARKOD = NULA(DT, 0, "STOKBARKOD")
            STK(0).AFIYAT = kurus(NULD(DT, 0, "AFIYAT"), KURUSHASSASIYET)
            STK(0).SFIYAT = kurus(NULD(DT, 0, "SFIYAT"), KURUSHASSASIYET)
            STK(0).OFIYAT1 = kurus(NULD(DT, 0, "OFIYAT1"), KURUSHASSASIYET)
            STK(0).OFIYAT2 = kurus(NULD(DT, 0, "OFIYAT2"), KURUSHASSASIYET)
            STK(0).OFIYAT3 = kurus(NULD(DT, 0, "OFIYAT3"), KURUSHASSASIYET)
            STK(0).OFIYAT4 = kurus(NULD(DT, 0, "OFIYAT4"), KURUSHASSASIYET)
            STK(0).OFIYAT5 = kurus(NULD(DT, 0, "OFIYAT5"), KURUSHASSASIYET)
            STK(0).OFIYAT6 = kurus(NULD(DT, 0, "OFIYAT6"), KURUSHASSASIYET)
            STK(0).OMALIYET = kurus(NULD(DT, 0, "OMALIYET"), KURUSHASSASIYET)
            STK(0).SMALIYET = kurus(NULD(DT, 0, "SMALIYET"), KURUSHASSASIYET)


            Return STK


        End If
        SQ = " "
        SQ = "SELECT STOK.a_id AS STOKID,STOK.a_kod as STOKKOD,STOK.a_barkod AS STOKBARKOD,STOK.a_adi AS STOKAD,"
        SQ = SQ & "STOK.a_afiyat AS AFIYAT,STOK.a_sfiyat AS SFIYAT ,STOK.a_ofiyat1 AS OFIYAT1,"
        SQ = SQ & "STOK.a_ofiyat2 AS OFIYAT2,STOK.a_ofiyat3 AS OFIYAT3,STOK.a_ofiyat4 AS OFIYAT4,"
        SQ = SQ & "STOK.a_ofiyat5 AS OFIYAT5,STOK.a_ofiyat6 AS OFIYAT6,STOK.a_mayuragort AS OMALIYET,STOK.a_sonmaliyet AS SMALIYET"
        SQ = SQ & " FROM " & KUL.tfrm & "stok as STOK "
        SQ = SQ & " LEFT JOIN " & KUL.tfrm & "stoklot as STOKLOT on STOK.a_id=STOKLOT.a_sid"
        SQ = SQ & " WHERE  a_lotadi='" & BARKOD & "' AND  a_akpas='Aktif' AND a_reftip NOT IN ('A')"
        SQ = SQ & " ORDER BY a_cdate"

        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)


        If DT.Rows.Count > 0 Then
            Dim STK(0) As WS_FiyatGor

            STK(0) = New WS_FiyatGor
            STK(0).DURUM = NUMARATOR.BAŞARILI
            STK(0).ID = NULN(DT, 0, "STOKID")
            STK(0).KOD = NULA(DT, 0, "STOKKOD")
            STK(0).AD = NULA(DT, 0, "STOKAD")
            STK(0).BARKOD = NULA(DT, 0, "STOKBARKOD")
            STK(0).AFIYAT = kurus(NULD(DT, 0, "AFIYAT"), KURUSHASSASIYET)
            STK(0).SFIYAT = kurus(NULD(DT, 0, "SFIYAT"), KURUSHASSASIYET)
            STK(0).OFIYAT1 = kurus(NULD(DT, 0, "OFIYAT1"), KURUSHASSASIYET)
            STK(0).OFIYAT2 = kurus(NULD(DT, 0, "OFIYAT2"), KURUSHASSASIYET)
            STK(0).OFIYAT3 = kurus(NULD(DT, 0, "OFIYAT3"), KURUSHASSASIYET)
            STK(0).OFIYAT4 = kurus(NULD(DT, 0, "OFIYAT4"), KURUSHASSASIYET)
            STK(0).OFIYAT5 = kurus(NULD(DT, 0, "OFIYAT5"), KURUSHASSASIYET)
            STK(0).OFIYAT6 = kurus(NULD(DT, 0, "OFIYAT6"), KURUSHASSASIYET)
            STK(0).OMALIYET = kurus(NULD(DT, 0, "OMALIYET"), KURUSHASSASIYET)
            STK(0).SMALIYET = kurus(NULD(DT, 0, "SMALIYET"), KURUSHASSASIYET)

            Return STK

        End If
        SQ = " "
        SQ = "SELECT STOK.a_id AS STOKID,STOK.a_kod as STOKKOD,STOK.a_barkod AS STOKBARKOD,STOK.a_adi AS STOKAD,"
        SQ = SQ & "STOK.a_afiyat AS AFIYAT,STOK.a_sfiyat AS SFIYAT ,STOK.a_ofiyat1 AS OFIYAT1,"
        SQ = SQ & "STOK.a_ofiyat2 AS OFIYAT2,STOK.a_ofiyat3 AS OFIYAT3,STOK.a_ofiyat4 AS OFIYAT4,"
        SQ = SQ & "STOK.a_ofiyat5 AS OFIYAT5,STOK.a_ofiyat6 AS OFIYAT6,STOK.a_mayuragort AS OMALIYET,STOK.a_sonmaliyet AS SMALIYET"
        SQ = SQ & " FROM " & KUL.tfrm & "stok as STOK "
        SQ = SQ & " LEFT JOIN " & KUL.tfrm & "stoklot as STOKLOT on STOK.a_id=STOKLOT.a_sid"
        SQ = SQ & " WHERE  a_adi = '" & BARKOD & "' AND  a_akpas='Aktif' AND a_reftip NOT IN ('A')"
        SQ = SQ & " ORDER BY a_cdate"

        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)


        If DT.Rows.Count > 0 Then
            Dim STK(0) As WS_FiyatGor

            STK(0) = New WS_FiyatGor
            STK(0).DURUM = NUMARATOR.BAŞARILI
            STK(0).ID = NULN(DT, 0, "STOKID")
            STK(0).KOD = NULA(DT, 0, "STOKKOD")
            STK(0).AD = NULA(DT, 0, "STOKAD")
            STK(0).BARKOD = NULA(DT, 0, "STOKBARKOD")
            STK(0).AFIYAT = kurus(NULD(DT, 0, "AFIYAT"), KURUSHASSASIYET)
            STK(0).SFIYAT = kurus(NULD(DT, 0, "SFIYAT"), KURUSHASSASIYET)
            STK(0).OFIYAT1 = kurus(NULD(DT, 0, "OFIYAT1"), KURUSHASSASIYET)
            STK(0).OFIYAT2 = kurus(NULD(DT, 0, "OFIYAT2"), KURUSHASSASIYET)
            STK(0).OFIYAT3 = kurus(NULD(DT, 0, "OFIYAT3"), KURUSHASSASIYET)
            STK(0).OFIYAT4 = kurus(NULD(DT, 0, "OFIYAT4"), KURUSHASSASIYET)
            STK(0).OFIYAT5 = kurus(NULD(DT, 0, "OFIYAT5"), KURUSHASSASIYET)
            STK(0).OFIYAT6 = kurus(NULD(DT, 0, "OFIYAT6"), KURUSHASSASIYET)
            STK(0).OMALIYET = kurus(NULD(DT, 0, "OMALIYET"), KURUSHASSASIYET)
            STK(0).SMALIYET = kurus(NULD(DT, 0, "SMALIYET"), KURUSHASSASIYET)
            Return STK
        Else
            Dim STK(0) As WS_FiyatGor
            STK(0) = New WS_FiyatGor
            STK(0).DURUM = NUMARATOR.HATA
            STK(0).ID = 0
            STK(0).KOD = " "
            STK(0).AD = " "
            STK(0).BARKOD = " "
            STK(0).AFIYAT = 0
            STK(0).SFIYAT = 0
            STK(0).OFIYAT1 = 0
            STK(0).OFIYAT2 = 0
            STK(0).OFIYAT3 = 0
            STK(0).OFIYAT4 = 0
            STK(0).OFIYAT5 = 0
            STK(0).OFIYAT6 = 0
            STK(0).OMALIYET = 0
            STK(0).SMALIYET = 0

            Return STK
        End If

    End Function
#End Region


#Region "MB/Verilen Siparişi Teslim Alma"
    Public Class WS_TeslımAlma
        Public DURUM As Long
    End Class

    Public Function MB_SipTeslimAl(ByVal BELGETURU As String, ByVal BARKOD As String, ByVal BELGEID As Long, ByVal STOKID As Long, ByVal MIKTAR As Long, ByVal DEPOID As Long, ByVal SUBEID As Long, ByVal CARIID As Long, ByVal URUNFIYAT As Double, ByVal URUNFIYATTUR As String) As WS_TeslımAlma

        Dim gecerliFiyat As Double
        Dim birim As String
        Dim birimCarpan As Long
        Dim stokAdi As String
        Dim stokKodu As String
        Dim STOK_ID As String
        Dim barkodd As String
        Dim stoktip As String
        Dim kdval As String
        Dim kdvsat As String
        Dim CARIKOD As String
        Dim SQ As String
        Dim STOK As New STOK_BILGI
        Dim DT As New DataTable
        Dim sipkesinkont As Long
        Dim tekkont As Long
        Dim sipsirabag As Long
        Dim sipsirabagfis As Long
        Dim sipstkadkont As Long
        Dim satir As Long = 0
        Dim agirlikDeger As Long = 0
        Dim miktartop As Long = 0
        Dim sipsira As Long
        Dim sipid As Long
        Dim sipbagtk As Long
        Dim miktareklenen As Long
        Dim eskimiktar As Long
        Dim sayac As Long = 0
        Dim farkmiktar As Long
        Dim sipmikid As Long
        Dim sipmiksira As Long
        Dim sipmik As Long
        Dim fismiksira As Long
        Dim sipstkbfiy As Double
        Dim sipstkkdv As Long
        Dim miktarkalan As Long
        B_SQL = connstr

        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        STOK.BUL(BARKOD)
        stokKodu = STOK.SKART.S01_kod
        stokAdi = STOK.SKART.S02_adi
        STOK_ID = STOK.SKART.S00_id
        birim = STOK.SKART.S03_birim
        birimCarpan = STOK.SKART.S98_Carpan
        kdval = STOK.SKART.S07_alkdvor
        kdvsat = STOK.SKART.S08_sakdvor
        barkodd = STOK.SKART.S11_barkod
        stoktip = STOK.SKART.S43_a_reftip
        miktareklenen = MIKTAR
        SQ = ""
        SQ = "Select TOP(1) a_kod "
        SQ = SQ & "From " & KUL.tfrm & "cari "
        SQ = SQ & " Where a_id = " & CARIID & " And a_akpas ='Aktif' "
        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
        CARIKOD = NULA(DT, 0, "a_kod")

        If URUNFIYATTUR = "@" Then
            gecerliFiyat = URUNFIYAT
        ElseIf URUNFIYATTUR = "-1" Then
            gecerliFiyat = 0
        Else
            gecerliFiyat = FIYATBULMA(URUNFIYATTUR, stokKodu, CARIKOD)

        End If

        SQ = "select sum(a_kesin) as say from " & KUL.tper & "sipdet WHERE a_tur = 34 AND a_stok_id =" & STOKID & "  AND a_cari_id = " & CARIID & ""
        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
        sipkesinkont = NULN(DT, 0, "say")
        If sipkesinkont > 0 Then
            SQ = ""
            SQ = "select isnull(count(a_id),0) as say from " & KUL.tper & "stkgdet WHERE a_tur = " & BELGETURU & " AND a_stok_id = " & STOKID & " AND a_cari_id =  " & CARIID & " AND a_id =" & BELGEID
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            tekkont = NULN(DT, 0, "say")
            SQ = ""
            SQ = "select isnull(max(a_sira),0) as say from " & KUL.tper & "stkgdet where a_id =" & BELGEID & " and a_tur = " & BELGETURU & ""
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            sipsirabag = NULN(DT, 0, "say")
            SQ = ""
            SQ = "select isnull(max(a_sira),0) as say from " & KUL.tper & "stkgdet where a_id = " & BELGEID & " and a_tur = " & BELGETURU & ""
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            sipsirabagfis = NULN(DT, 0, "say")
            SQ = ""
            SQ = "select isnull(count(a_id),0) as say from " & KUL.tper & "sipdet where a_tur = 34 and a_kesin = 1 and a_stok_id =" & STOKID & " and a_cari_id = " & CARIID
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            sipstkadkont = NULN(DT, 0, "say")

            While agirlikDeger < miktartop
                satir = satir + 1
                SQ = ""
                SQ = "Select " & agirlikDeger & "= " & agirlikDeger & " + miktar from ( SELECT Row_Number() Over (Order By a_tarih, a_id, a_sira asc) AS sira, (a_mik-a_kapamik) as miktar "
                SQ = SQ & " From " & KUL.tper & "sipdet Where a_kesin = 1 And a_stok_id = " & STOKID & " And a_cari_id = " & CARIID & " And a_ambar_id = " & DEPOID & " And (a_mik-a_kapamik) > 0) as tbl Where SIRA =" & satir
                SQL_KOS(SQ, False)
                SQ = ""
                SQ = " select sipid from ( SELECT Row_Number() Over (Order By a_tarih, a_id, a_sira asc) AS sira, a_id as sipid"
                SQ = SQ & "From " & KUL.tper & "sipdet Where a_kesin = 1 And a_stok_id = " & STOKID & " And a_cari_id = " & CARIID & " And a_ambar_id = " & DEPOID & " And (a_mik-a_kapamik) > 0) as tbl Where SIRA = " & satir
                DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                sipid = NULN(DT, 0, "sipid")
                SQ = ""
                SQ = "SELECT sipsira from ( SELECT Row_Number() Over (Order By a_tarih, a_id, a_sira asc) AS sira, a_sira as sipsira "
                SQ = SQ & "From " & KUL.tper & "sipdet Where a_kesin = 1 And a_stok_id = " & STOKID & " And a_cari_id = " & CARIID & " And a_ambar_id = " & DEPOID & " And (a_mik-a_kapamik) > 0) as tbl Where SIRA = " & satir
                DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                sipsira = NULN(DT, 0, "sipsira")
                SQ = ""
                SQ = "SELECT COUNT(a_siptur) as tur FROM " & KUL.tper & "sipbag WHERE a_fisid = " & BELGEID & " And a_sipid = " & sipid & " And a_sipsira = " & sipsira
                DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                sipbagtk = NULN(DT, 0, "tur")
                If sipbagtk = 0 Then
                    sipsirabag = sipsirabag + 1
                    SQ = "INSERT INTO " & KUL.tper & "sipbag (a_siptur, a_sipid, a_sipsira, a_fistur, a_fisid, a_fissira) "
                    SQ = SQ & "VALUES ( 34, " & sipid & ", " & sipsira & ", " & BELGETURU & ", " & BELGEID & ", " & sipsirabag & " )"
                    SQL_KOS(SQ, False)
                End If

            End While

            satir = satir - 1
            miktareklenen = 0
            SQ = ""
            SQ = "SELECT  ISNULL(sum(a_kapamik), 0) as kapmik FROM " & KUL.tper & "sipdet WHERE a_kesin = 1 And a_stok_id =" & STOKID & " And a_cari_id = " & CARIID & " And a_ambar_id =" & DEPOID
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            eskimiktar = NULN(DT, 0, "kapmik")
            While sayac < satir

                SQ = ""
                SQ = "SELECT  TOP(1)(a_mik - a_kapamik) As farkmiktar,a_id,a_sira,a_mik,a_brmfiy,a_kdv FROM " & KUL.tper & "sipdet "
                SQ = SQ & " WHERE a_kesin = 1 And a_mik > a_kapamik And a_tur = 34 And a_kapamik >= 0 And a_cari_id = " & CARIID & "  And a_ambar_id = " & DEPOID & " And a_stok_id = " & STOKID & " order by a_id, a_sira ASC"
                DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                farkmiktar = NULN(DT, 0, "farkmiktar")
                sipmikid = NULN(DT, 0, "a_id")
                sipmiksira = NULN(DT, 0, "a_sira")
                sipmik = NULN(DT, 0, "a_mik")
                sipstkbfiy = NULD(DT, 0, "a_brmfiy")
                sipstkkdv = NULN(DT, 0, "a_kdv")
                SQ = "SELECT   TOP(1) a_sira FROM " & KUL.tper & "sipbag As sipbag INNER JOIN " & KUL.tper & "stkgdet As fat On fat.a_sira=sipbag.a_fissira And fat.a_id=sipbag.a_fisid "
                SQ = SQ & "WHERE a_stok_id = " & STOKID & " And a_cari_id =  " & CARIID & " And a_ambar_id = " & DEPOID & " And sipbag.a_sipid = " & sipid & " And sipbag.a_sipsira = " & sipsira & " And fat.a_id = " & BELGEID
                DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                fismiksira = NULN(DT, 0, "a_sira")
                SQ = ""
                SQ = "update " & KUL.tper & "stkgdet set a_mik = " & sipmik & ", a_sbrmmik = " & sipmik & ", a_brmfiy = " & sipstkbfiy & ", a_sbrmfiy = " & sipstkbfiy & ", a_kdv =" & sipstkbfiy & " "
                SQ = SQ & "where a_cari_id = " & CARIID & " and a_ambar_id = " & DEPOID & " and a_stok_id = " & STOKID & " and a_id = " & BELGEID & " and a_sira = " & fismiksira
                SQL_KOS(SQ, False)
                SQ = ""
                SQ = "WITH sipdet AS ( select top 1 a_kapamik from " & KUL.tper & "sipdet where a_kesin = 1 and a_mik > a_kapamik and a_tur = 34 and a_kapamik >= 0 and a_cari_id = " & CARIID & "    "
                SQ = SQ & " and a_ambar_id = " & DEPOID & " and a_stok_id = " & STOKID & " order by a_id, a_sira asc ) update sipdet set a_kapamik = a_kapamik + " & farkmiktar
                SQL_KOS(SQ, False)

                sipsirabagfis = sipsirabagfis + 1
                If tekkont = 0 Then
                    SQ = ""
                    SQ = "INSERT INTO " & KUL.tper & "stkgdet (a_tur, a_id, a_sira, a_sirano, a_tarih, a_cari_id, a_stok_id, a_ambar_id, a_mik, a_brmfiy, "
                    SQ = SQ & " a_kdv, a_kdvtut, a_isk, a_isk1, a_isk2, a_isk3, a_isk4, a_isk5, a_isktut, a_tutar, a_ack, a_otv, a_otvtut, a_bloke,"
                    SQ = SQ & " a_kesin, a_fattur, a_fatid, a_sbrm, a_sbrmcrp, a_sbrmmik, a_sbrmfiy,a_oivy, a_oivt , a_indirim, a_gsisko, a_gsiskt, a_perid, a_did, a_dtut, a_dkur)"
                    SQ = SQ & " Select " & BELGETURU & " as a_tur, " & BELGEID & " as a_id, " & sipsirabagfis & " as a_sira, " & sipsirabagfis & " as a_sirano,"
                    SQ = SQ & "CONVERT(VARCHAR(10),GETDATE(),110) as a_tarih, " & CARIID & " as a_cari_id," & STOKID & " as a_stok_id, " & DEPOID & " as a_ambar_id, "
                    SQ = SQ & "" & farkmiktar & " as a_mik, " & sipstkbfiy & " as a_brmfiy, " & sipstkkdv & " as a_kdv, " & farkmiktar * sipstkkdv & " as a_kdvtut, 0 as a_isk, 0 as a_isk1, 0 as a_isk2, "
                    SQ = SQ & "0 as a_isk3, 0 as a_isk4, 0 as a_isk5, 0 as a_isktut, " & farkmiktar * sipstkbfiy & " As a_tutar, N'" & stokAdi & "' as a_ack, 0 as a_otv, 0 as a_otvtut, 0 as a_bloke,0 as a_kesin,"
                    SQ = SQ & "" & BELGETURU & " as a_fattur, " & BELGEID & " as a_fatid, N'" & birim & "' as a_sbrm, 1 as a_sbrmcrp, " & farkmiktar & " as a_sbrmmik, " & sipstkbfiy & " as a_sbrmfiy, 0 as a_oivy, "
                    SQ = SQ & "0 as a_oivt, 0 as a_indirim, 0 as a_gsisko, 0 as a_gsiskt, 0 as a_perid, 0 as a_did, " & farkmiktar * sipstkbfiy & " as a_dtut, 1 as a_dkur"
                    SQL_KOS(SQ, False)
                Else
                    If satir > 0 And tekkont > 0 And farkmiktar > 0 Then
                        If (sipbagtk = 0) Then

                            'MB_UrunEkle(BELGETURU, BARKOD, BELGEID, STOKID, MIKTAR, REFTIPI, DEPOID, OPLANID, SUBEID, CARIID, MIKTARKONTROL, URUNFIYAT, YENISATIR, USERID, URUNFIYATTUR)
                            SQ = ""
                            SQ = "INSERT INTO " & KUL.tper & "stkgdet (a_tur, a_id, a_sira, a_sirano, a_tarih, a_cari_id, a_stok_id, a_ambar_id, a_mik, a_brmfiy, "
                            SQ = SQ & " a_kdv, a_kdvtut, a_isk, a_isk1, a_isk2, a_isk3, a_isk4, a_isk5, a_isktut, a_tutar, a_ack, a_otv, a_otvtut, a_bloke,"
                            SQ = SQ & " a_kesin, a_fattur, a_fatid, a_sbrm, a_sbrmcrp, a_sbrmmik, a_sbrmfiy,a_oivy, a_oivt , a_indirim, a_gsisko, a_gsiskt, a_perid, a_did, a_dtut, a_dkur)"
                            SQ = SQ & " Select " & BELGETURU & " as a_tur, " & BELGEID & " as a_id, " & sipsirabagfis & " as a_sira, " & sipsirabagfis & " as a_sirano,"
                            SQ = SQ & "CONVERT(VARCHAR(10),GETDATE(),110) as a_tarih, " & CARIID & " as a_cari_id," & STOKID & " as a_stok_id, " & DEPOID & " as a_ambar_id, "
                            SQ = SQ & "" & farkmiktar & " as a_mik, " & sipstkbfiy & " as a_brmfiy, " & sipstkkdv & " as a_kdv, 102 as a_kdvtut, 0 as a_isk, 0 as a_isk1, 0 as a_isk2, "
                            SQ = SQ & "0 as a_isk3, 0 as a_isk4, 0 as a_isk5, 0 as a_isktut, " & farkmiktar * sipstkbfiy & " As a_tutar, N'" & stokAdi & "' as a_ack, 0 as a_otv, 0 as a_otvtut, 0 as a_bloke,0 as a_kesin,"
                            SQ = SQ & "" & BELGETURU & " as a_fattur, " & BELGEID & " as a_fatid, N'" & birim & "' as a_sbrm, 1 as a_sbrmcrp, " & farkmiktar & " as a_sbrmmik, " & sipstkbfiy & " as a_sbrmfiy, 0 as a_oivy, "
                            SQ = SQ & "0 as a_oivt, 0 as a_indirim, 0 as a_gsisko, 0 as a_gsiskt, 0 as a_perid, 0 as a_did, " & farkmiktar * sipstkbfiy & " as a_dtut, 1 as a_dkur"
                            SQL_KOS(SQ, False)

                        End If
                        If (miktarkalan <> Nothing) Then
                            SQ = ""
                            SQ = "INSERT INTO " & KUL.tper & "stkgdet (a_tur, a_id, a_sira, a_sirano, a_tarih, a_cari_id, a_stok_id, a_ambar_id, a_mik, a_brmfiy, "
                            SQ = SQ & " a_kdv, a_kdvtut, a_isk, a_isk1, a_isk2, a_isk3, a_isk4, a_isk5, a_isktut, a_tutar, a_ack, a_otv, a_otvtut, a_bloke,"
                            SQ = SQ & " a_kesin, a_fattur, a_fatid, a_sbrm, a_sbrmcrp, a_sbrmmik, a_sbrmfiy,a_oivy, a_oivt , a_indirim, a_gsisko, a_gsiskt, a_perid, a_did, a_dtut, a_dkur)"
                            SQ = SQ & " Select " & BELGETURU & " as a_tur, " & BELGEID & " as a_id, " & sipsirabagfis & " as a_sira, " & sipsirabagfis & " as a_sirano,"
                            SQ = SQ & "CONVERT(VARCHAR(10),GETDATE(),110) as a_tarih, " & CARIID & " as a_cari_id," & STOKID & " as a_stok_id, " & DEPOID & " as a_ambar_id, "
                            SQ = SQ & "" & farkmiktar & " as a_mik, " & sipstkbfiy & " as a_brmfiy, " & sipstkkdv & " as a_kdv, 102 as a_kdvtut, 0 as a_isk, 0 as a_isk1, 0 as a_isk2, "
                            SQ = SQ & "0 as a_isk3, 0 as a_isk4, 0 as a_isk5, 0 as a_isktut, " & farkmiktar * sipstkbfiy & " As a_tutar, N'" & stokAdi & "' as a_ack, 0 as a_otv, 0 as a_otvtut, 0 as a_bloke,0 as a_kesin,"
                            SQ = SQ & "" & BELGETURU & " as a_fattur, " & BELGEID & " as a_fatid, N'" & birim & "' as a_sbrm, 1 as a_sbrmcrp, " & miktarkalan & " as a_sbrmmik, " & sipstkbfiy & " as a_sbrmfiy, 0 as a_oivy, "
                            SQ = SQ & "0 as a_oivt, 0 as a_indirim, 0 as a_gsisko, 0 as a_gsiskt, 0 as a_perid, 0 as a_did, " & farkmiktar * sipstkbfiy & " as a_dtut, 1 as a_dkur"
                            SQL_KOS(SQ, False)
                        End If

                    Else


                        SQ = ""
                        SQ = "INSERT INTO " & KUL.tper & "stkgdet (a_tur, a_id, a_sira, a_sirano, a_tarih, a_cari_id, a_stok_id, a_ambar_id, a_mik, a_brmfiy, "
                        SQ = SQ & " a_kdv, a_kdvtut, a_isk, a_isk1, a_isk2, a_isk3, a_isk4, a_isk5, a_isktut, a_tutar, a_ack, a_otv, a_otvtut, a_bloke,"
                        SQ = SQ & " a_kesin, a_fattur, a_fatid, a_sbrm, a_sbrmcrp, a_sbrmmik, a_sbrmfiy,a_oivy, a_oivt , a_indirim, a_gsisko, a_gsiskt, a_perid, a_did, a_dtut, a_dkur)"
                        SQ = SQ & " Select " & BELGETURU & " as a_tur, " & BELGEID & " as a_id, " & sipsirabagfis & " as a_sira, " & sipsirabagfis & " as a_sirano,"
                        SQ = SQ & "CONVERT(VARCHAR(10),GETDATE(),110) as a_tarih, " & CARIID & " as a_cari_id," & STOKID & " as a_stok_id, " & DEPOID & " as a_ambar_id, "
                        SQ = SQ & "" & miktartop - (miktareklenen + farkmiktar) & " as a_mik, " & sipstkbfiy & " as a_brmfiy, " & sipstkkdv & " as a_kdv, 102 as a_kdvtut, 0 as a_isk, 0 as a_isk1, 0 as a_isk2, "
                        SQ = SQ & "0 as a_isk3, 0 as a_isk4, 0 as a_isk5, 0 as a_isktut, " & miktartop - (miktareklenen + farkmiktar) * sipstkbfiy & " As a_tutar, N'" & stokAdi & "' as a_ack, 0 as a_otv, 0 as a_otvtut, 0 as a_bloke,0 as a_kesin,"
                        SQ = SQ & "" & BELGETURU & " as a_fattur, " & BELGEID & " as a_fatid, N'" & birim & "' as a_sbrm, 1 as a_sbrmcrp, " & miktartop - (miktareklenen + farkmiktar) & " as a_sbrmmik, " & sipstkbfiy & " as a_sbrmfiy, 0 as a_oivy, "
                        SQ = SQ & "0 as a_oivt, 0 as a_indirim, 0 as a_gsisko, 0 as a_gsiskt, 0 as a_perid, 0 as a_did, " & miktartop - (miktareklenen + farkmiktar) * sipstkbfiy & " as a_dtut, 1 as a_dkur"
                        SQL_KOS(SQ, False)

                    End If

                End If

                miktareklenen = miktareklenen + MIKTAR
                sayac = sayac + 1
            End While

            miktarkalan = (miktartop - miktareklenen)

            Dim sipmikidson As Long
            Dim sipmiksirason As Long
            Dim fismiksirason As Long
            Dim sipmikson As Double
            Dim sipmikkapson As Double
            Dim sipmiksonupd As Double
            Dim sipstkbfiyson As Double
            Dim sipstkkdvoson As Double
            Dim sipstktekise As Long
            SQ = " "
            SQ = "SELECT count(a_id) as id FROM " & KUL.tper & "sipdet WHERE a_kesin = 1 and a_stok_id =" & STOKID & " and a_cari_id = " & CARIID & " and a_ambar_id =" & DEPOID
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            sipstktekise = NULN(DT, 0, "id")
            If sipstktekise > 1 Then
                SQ = " "
                SQ = "select top (1) a_brmfiy,a_kdv,a_id,a_sira,a_mik,a_kapamik from " & KUL.tper & "sipdet where a_kesin = 1 and a_mik > a_kapamik and a_tur = 34 and a_kapamik >= 0 "
                SQ = SQ & "and a_cari_id = " & CARIID & " and a_ambar_id = " & DEPOID & " and a_stok_id = " & STOKID & " and a_mik >= a_kapamik + " & miktarkalan & " order by a_id, a_sira, a_kapamik desc"
                DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                sipmikidson = NULN(DT, 0, "a_id")
                sipmiksirason = NULN(DT, 0, "a_sira")
                sipstkbfiyson = NULD(DT, 0, "a_brmfiy")
                sipstkkdvoson = NULD(DT, 0, "a_kdv")
                sipmiksonupd = NULN(DT, 0, "a_mik")
                SQ = ""
                SQ = "ELECT top (1) a_sira FROM " & KUL.tper & "sipbag as sipbag inner join " & KUL.tper & "stkgdet as fat on fat.a_sira=sipbag.a_fissira and fat.a_id=sipbag.a_fisid "
                SQ = SQ & " WHERE a_stok_id =" & STOKID & " and a_cari_id =" & CARIID & " and a_ambar_id = " & DEPOID & " and sipbag.a_sipid = " & sipmikidson & " and sipbag.a_sipsira = " & sipmiksirason & " and fat.a_id =" & BELGEID
                DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                fismiksirason = NULN(DT, 0, "a_sira")
                SQ = " "
                SQ = "With sipdet As ( Select top (1) a_kapamik from " & KUL.tper & "sipdet where a_kesin = 1 And a_mik > a_kapamik And a_tur = 34 And a_kapamik >= 0 "
                SQ = SQ & "And a_cari_id = " & CARIID & " And a_ambar_id = " & DEPOID & " And a_stok_id = " & STOKID & " And a_mik >= a_kapamik + " & miktarkalan & " order by a_id, a_sira, a_kapamik desc )"
                SQ = SQ & " update sipdet set a_kapamik = a_kapamik + " & miktarkalan & ""
                SQL_KOS(SQ, False)

                SQ = " "
                SQ = "Select  top(1) a_mik,a_kapamik from " & KUL.tper & "sipdet where a_kesin = 1 And a_mik > a_kapamik And a_tur = 34 And a_kapamik >= 0 "
                SQ = SQ & "  And a_cari_id = " & CARIID & "    And a_ambar_id = " & DEPOID & " And a_stok_id = " & STOKID & " order by a_id, a_sira asc"
                DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                sipmikson = NULN(DT, 0, "a_mik")
                sipmikkapson = NULN(DT, 0, "a_kapamik")
            Else
                SQ = ""
                SQ = "select top (1) a_brmfiy,a_id,a_sira,a_kdv,a_mik from " & KUL.tper & "sipdet where a_kesin = 1 and a_mik > a_kapamik and a_tur = 34 and a_kapamik >= 0 and "
                SQ = SQ & "a_cari_id = " & CARIID & " and a_ambar_id = " & DEPOID & " and a_stok_id =" & STOKID & " order by a_id, a_sira asc"
                DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                sipmikidson = NULN(DT, 0, "a_id")
                sipmiksirason = NULN(DT, 0, "a_sira")
                sipstkbfiyson = NULD(DT, 0, "a_brmfiy")
                sipstkkdvoson = NULD(DT, 0, "a_kdv")
                sipmiksonupd = NULN(DT, 0, "a_mik")
                SQ = ""
                SQ = "SELECT top (1) a_sira FROM " & KUL.tper & "sipbag as sipbag inner join " & KUL.tper & "stkgdet as fat on fat.a_sira=sipbag.a_fissira and fat.a_id=sipbag.a_fisid "
                SQ = SQ & "WHERE a_stok_id = " & STOKID & " and a_cari_id = " & CARIID & " and a_ambar_id = " & DEPOID & " and sipbag.a_sipid = " & sipmikid & " and sipbag.a_sipsira = " & sipmiksira & " and fat.a_id = " & BELGEID
                DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                fismiksirason = NULN(DT, 0, "a_sira")
                SQ = ""
                SQ = " With sipdet As ( Select top (1) a_kapamik from " & KUL.tper & "sipdet where a_kesin = 1 And a_mik > a_kapamik And a_tur = 34 And a_kapamik >= 0 "
                SQ = SQ & "And a_cari_id = " & CARIID & " And a_ambar_id = " & DEPOID & " And a_stok_id = " & STOKID & " And a_mik >= a_kapamik + " & miktarkalan & " order by a_id, a_sira, a_kapamik desc )"
                SQ = SQ & "update sipdet set a_kapamik = a_kapamik + " & miktarkalan & ""
                SQL_KOS(SQ, False)
                SQ = " "
                SQ = "Select  top(1) a_mik,a_kapamik from " & KUL.tper & "sipdet where a_kesin = 1 And a_mik > a_kapamik And a_tur = 34 And a_kapamik >= 0 "
                SQ = SQ & "  And a_cari_id = " & CARIID & "    And a_ambar_id = " & DEPOID & " And a_stok_id = " & STOKID & " order by a_id, a_sira asc"
                DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                sipmikson = NULN(DT, 0, "a_mik")
                sipmikkapson = NULN(DT, 0, "a_kapamik")
            End If
            If (sipstkadkont = 1) Then
                Dim sipmiktekgunc As Long
                SQ = ""
                SQ = " Select Case top 1 a_kapamik from " & KUL.tper & "sipdet where a_kesin = 1 And a_mik >= a_kapamik And a_tur = 34 And a_kapamik >= 0 And a_cari_id = " & CARIID & " "
                SQ = SQ & " And a_ambar_id = " & DEPOID & " And a_stok_id = " & STOKID & " order by a_id, a_sira asc)"
                DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                sipmiktekgunc = NULN(DT, 0, "a_kapamik")
                SQ = ""
                SQ = "update " & KUL.tper & "stkgdet Set a_mik = " & sipmiktekgunc & ", a_sbrmmik = " & sipmiktekgunc & " where a_cari_id = " & CARIID & " And a_ambar_id = " & DEPOID & " And a_stok_id = " & STOKID & " And a_id =" & BELGEID
                SQL_KOS(SQ, False)
            End If
            If (sipmikson > 0) Then

                SQ = ""
                SQ = " update " & KUL.tper & "stkgdet Set a_mik = " & sipmikson & ", a_sbrmmik = " & sipmikson & ", a_brmfiy = " & sipstkbfiyson & ", a_sbrmfiy = @sipstkbfiyson, a_kdv = @sipstkkdvoson"
                SQ = SQ & "where a_cari_id =" & CARIID & " And a_ambar_id = " & DEPOID & " And a_stok_id = " & STOKID & " And a_id = " & BELGEID & " And a_sira = " & fismiksirason & ""
                SQL_KOS(SQ, False)
            Else
                If (farkmiktar <> Nothing) Then
                    SQ = ""
                    SQ = "update " & KUL.tper & "stkgdet Set a_mik = " & farkmiktar & ", a_sbrmmik = " & farkmiktar & ", a_brmfiy = " & sipstkbfiyson & ", a_sbrmfiy = " & sipstkbfiyson & ","
                    SQ = SQ & "a_kdv = " & sipstkkdvoson & " where a_cari_id = " & CARIID & "And a_ambar_id = " & DEPOID & " And a_stok_id = " & STOKID & " And a_id = " & BELGEID & " And a_sira = " & fismiksirason & ""
                    SQL_KOS(SQ, False)
                Else
                    SQ = ""
                    SQ = "update " & KUL.tper & "stkgdet Set a_mik = " & sipmiksonupd & ", a_sbrmmik = " & sipmiksonupd & ", a_brmfiy =" & sipstkbfiyson & ", a_sbrmfiy = " & sipstkbfiyson & ", a_kdv = " & sipstkkdvoson & ""
                    SQ = SQ & "where a_cari_id = " & CARIID & " And a_ambar_id = " & DEPOID & " And a_stok_id = " & STOKID & " And a_id = " & BELGEID & " And a_sira = " & sipstkkdvoson & ""
                    SQL_KOS(SQ, False)
                End If
            End If
            If (tekkont = 0 And satir = 0) Then
                SQ = ""
                SQ = "INSERT INTO " & KUL.tper & "stkgdet (a_tur, a_id, a_sira, a_sirano, a_tarih, a_cari_id, a_stok_id, a_ambar_id, a_mik, a_brmfiy, "
                SQ = SQ & " a_kdv, a_kdvtut, a_isk, a_isk1, a_isk2, a_isk3, a_isk4, a_isk5, a_isktut, a_tutar, a_ack, a_otv, a_otvtut, a_bloke,"
                SQ = SQ & " a_kesin, a_fattur, a_fatid, a_sbrm, a_sbrmcrp, a_sbrmmik, a_sbrmfiy,a_oivy, a_oivt , a_indirim, a_gsisko, a_gsiskt, a_perid, a_did, a_dtut, a_dkur)"
                SQ = SQ & " Select " & BELGETURU & " as a_tur, " & BELGEID & " as a_id, " & sipsirabag & " as a_sira, " & sipsirabag & " as a_sirano,"
                SQ = SQ & "CONVERT(VARCHAR(10),GETDATE(),110) as a_tarih, " & CARIID & " as a_cari_id," & STOKID & " as a_stok_id, " & DEPOID & " as a_ambar_id, "
                SQ = SQ & "" & miktarkalan & " as a_mik, " & sipstkbfiyson & " as a_brmfiy, " & sipstkkdvoson & " as a_kdv, " & miktarkalan * sipstkkdvoson & " as a_kdvtut, 0 as a_isk, 0 as a_isk1, 0 as a_isk2, "
                SQ = SQ & "0 as a_isk3, 0 as a_isk4, 0 as a_isk5, 0 as a_isktut, " & miktarkalan * sipstkbfiyson & " As a_tutar, N'" & stokAdi & "' as a_ack, 0 as a_otv, 0 as a_otvtut, 0 as a_bloke,0 as a_kesin,"
                SQ = SQ & "" & BELGETURU & " as a_fattur, " & BELGEID & " as a_fatid, N'" & birim & "' as a_sbrm, 1 as a_sbrmcrp, " & miktarkalan & " as a_sbrmmik, " & sipstkbfiyson & " as a_sbrmfiy, 0 as a_oivy, "
                SQ = SQ & "0 as a_oivt, 0 as a_indirim, 0 as a_gsisko, 0 as a_gsiskt, 0 as a_perid, 0 as a_did, " & miktarkalan * sipstkbfiyson & " as a_dtut, 1 as a_dkur"
                SQL_KOS(SQ, False)
            Else
                SQ = ""
                SQ = "INSERT INTO " & KUL.tper & "stkgdet (a_tur, a_id, a_sira, a_sirano, a_tarih, a_cari_id, a_stok_id, a_ambar_id, a_mik, a_brmfiy, "
                SQ = SQ & " a_kdv, a_kdvtut, a_isk, a_isk1, a_isk2, a_isk3, a_isk4, a_isk5, a_isktut, a_tutar, a_ack, a_otv, a_otvtut, a_bloke,"
                SQ = SQ & " a_kesin, a_fattur, a_fatid, a_sbrm, a_sbrmcrp, a_sbrmmik, a_sbrmfiy,a_oivy, a_oivt , a_indirim, a_gsisko, a_gsiskt, a_perid, a_did, a_dtut, a_dkur)"
                SQ = SQ & " Select " & BELGETURU & " as a_tur, " & BELGEID & " as a_id, " & sipsirabag & " as a_sira, " & sipsirabag & " as a_sirano,"
                SQ = SQ & "CONVERT(VARCHAR(10),GETDATE(),110) as a_tarih, " & CARIID & " as a_cari_id," & STOKID & " as a_stok_id, " & DEPOID & " as a_ambar_id, "
                SQ = SQ & "" & miktarkalan & " as a_mik, " & sipstkbfiyson & " as a_brmfiy, " & sipstkkdvoson & " as a_kdv, " & miktarkalan * sipstkkdvoson & " as a_kdvtut, 0 as a_isk, 0 as a_isk1, 0 as a_isk2, "
                SQ = SQ & "0 as a_isk3, 0 as a_isk4, 0 as a_isk5, 0 as a_isktut, " & miktarkalan * sipstkbfiyson & " As a_tutar, N'" & stokAdi & "' as a_ack, 0 as a_otv, 0 as a_otvtut, 0 as a_bloke,0 as a_kesin,"
                SQ = SQ & "" & BELGETURU & " as a_fattur, " & BELGEID & " as a_fatid, N'" & birim & "' as a_sbrm, 1 as a_sbrmcrp, " & miktarkalan & " as a_sbrmmik, " & sipstkbfiyson & " as a_sbrmfiy, 0 as a_oivy, "
                SQ = SQ & "0 as a_oivt, 0 as a_indirim, 0 as a_gsisko, 0 as a_gsiskt, 0 as a_perid, 0 as a_did, " & miktarkalan * sipstkbfiyson & " as a_dtut, 1 as a_dkur"
                SQL_KOS(SQ, False)
                If (tekkont = 0 And satir > 0) Then
                    SQ = ""
                    SQ = "INSERT INTO " & KUL.tper & "stkgdet (a_tur, a_id, a_sira, a_sirano, a_tarih, a_cari_id, a_stok_id, a_ambar_id, a_mik, a_brmfiy, "
                    SQ = SQ & " a_kdv, a_kdvtut, a_isk, a_isk1, a_isk2, a_isk3, a_isk4, a_isk5, a_isktut, a_tutar, a_ack, a_otv, a_otvtut, a_bloke,"
                    SQ = SQ & " a_kesin, a_fattur, a_fatid, a_sbrm, a_sbrmcrp, a_sbrmmik, a_sbrmfiy,a_oivy, a_oivt , a_indirim, a_gsisko, a_gsiskt, a_perid, a_did, a_dtut, a_dkur)"
                    SQ = SQ & " Select " & BELGETURU & " as a_tur, " & BELGEID & " as a_id, " & sipsirabag & " as a_sira, " & sipsirabag & " as a_sirano,"
                    SQ = SQ & "CONVERT(VARCHAR(10),GETDATE(),110) as a_tarih, " & CARIID & " as a_cari_id," & STOKID & " as a_stok_id, " & DEPOID & " as a_ambar_id, "
                    SQ = SQ & "" & miktarkalan & " as a_mik, " & sipstkbfiyson & " as a_brmfiy, " & sipstkkdvoson & " as a_kdv, " & miktarkalan * sipstkkdvoson & " as a_kdvtut, 0 as a_isk, 0 as a_isk1, 0 as a_isk2, "
                    SQ = SQ & "0 as a_isk3, 0 as a_isk4, 0 as a_isk5, 0 as a_isktut, " & miktarkalan * sipstkbfiyson & " As a_tutar, N'" & stokAdi & "' as a_ack, 0 as a_otv, 0 as a_otvtut, 0 as a_bloke,0 as a_kesin,"
                    SQ = SQ & "" & BELGETURU & " as a_fattur, " & BELGEID & " as a_fatid, N'" & birim & "' as a_sbrm, 1 as a_sbrmcrp, " & miktarkalan & " as a_sbrmmik, " & sipstkbfiyson & " as a_sbrmfiy, 0 as a_oivy, "
                    SQ = SQ & "0 as a_oivt, 0 as a_indirim, 0 as a_gsisko, 0 as a_gsiskt, 0 as a_perid, 0 as a_did, " & miktarkalan * sipstkbfiyson & " as a_dtut, 1 as a_dkur"
                    SQL_KOS(SQ, False)

                End If
                If (tekkont > 0 And satir = 0) Then
                    SQ = ""
                    SQ = "INSERT INTO " & KUL.tper & "stkgdet (a_tur, a_id, a_sira, a_sirano, a_tarih, a_cari_id, a_stok_id, a_ambar_id, a_mik, a_brmfiy, "
                    SQ = SQ & " a_kdv, a_kdvtut, a_isk, a_isk1, a_isk2, a_isk3, a_isk4, a_isk5, a_isktut, a_tutar, a_ack, a_otv, a_otvtut, a_bloke,"
                    SQ = SQ & " a_kesin, a_fattur, a_fatid, a_sbrm, a_sbrmcrp, a_sbrmmik, a_sbrmfiy,a_oivy, a_oivt , a_indirim, a_gsisko, a_gsiskt, a_perid, a_did, a_dtut, a_dkur)"
                    SQ = SQ & " Select " & BELGETURU & " as a_tur, " & BELGEID & " as a_id, " & sipsirabag & " as a_sira, " & sipsirabag & " as a_sirano,"
                    SQ = SQ & "CONVERT(VARCHAR(10),GETDATE(),110) as a_tarih, " & CARIID & " as a_cari_id," & STOKID & " as a_stok_id, " & DEPOID & " as a_ambar_id, "
                    SQ = SQ & "" & miktarkalan & " as a_mik, " & sipstkbfiyson & " as a_brmfiy, " & sipstkkdvoson & " as a_kdv, " & miktarkalan * sipstkkdvoson & " as a_kdvtut, 0 as a_isk, 0 as a_isk1, 0 as a_isk2, "
                    SQ = SQ & "0 as a_isk3, 0 as a_isk4, 0 as a_isk5, 0 as a_isktut, " & miktarkalan * sipstkbfiyson & " As a_tutar, N'" & stokAdi & "' as a_ack, 0 as a_otv, 0 as a_otvtut, 0 as a_bloke,0 as a_kesin,"
                    SQ = SQ & "" & BELGETURU & " as a_fattur, " & BELGEID & " as a_fatid, N'" & birim & "' as a_sbrm, 1 as a_sbrmcrp, " & miktarkalan & " as a_sbrmmik, " & sipstkbfiyson & " as a_sbrmfiy, 0 as a_oivy, "
                    SQ = SQ & "0 as a_oivt, 0 as a_indirim, 0 as a_gsisko, 0 as a_gsiskt, 0 as a_perid, 0 as a_did, " & miktarkalan * sipstkbfiyson & " as a_dtut, 1 as a_dkur"
                    SQL_KOS(SQ, False)
                End If
            End If
            Dim teslimtutgunc As Long
            Dim teslimkdvgunc As Long
            SQ = "SELECT sum((((a_mik*a_brmfiy)/100)*a_kdv)+(a_mik*a_brmfiy)) AS TOPLAM FROM " & KUL.tper & "stkgdet WHERE a_id = " & BELGEID & " And a_tur = " & BELGETURU & ""
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            teslimtutgunc = NULN(DT, 0, "TOPLAM")
            SQ = "Select sum((((a_mik*a_brmfiy)/100)*a_kdv)) FROM " & KUL.tper & "stkgdet WHERE a_id = " & BELGEID & " And a_tur =" & BELGETURU
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            teslimkdvgunc = NULN(DT, 0, "TOPLAM")
            SQ = "Update " & KUL.tper & "stkgmas Set a_kdvtop = " & teslimkdvgunc & ", a_tuttop = " & teslimtutgunc & ",  a_dtut = " & teslimtutgunc & " WHERE a_id = " & BELGEID & " And a_tur = " & BELGETURU & ""
            SQL_KOS(SQ, False)
        End If

    End Function
#End Region
#Region "KW_Uyarı Mesajları"
    Public Enum NUMARATOR2
        YENIBELGE = 1
        BELGENINUSTUNE = 2
        BIRDENFAZLABELGE = 3
        HATA = 4
    End Enum
#End Region
#Region "KW_Belge Oluşturma"
    Public Class KW_Belge_Olustur
        Public DURUM As Long
        Public BELGE_ID As Long
        Public CARI As String
        Public TARIH As Date
        Public STOKADI As String
    End Class
    <WebMethod()>
    Public Function KW_BelgeOlustur(ByVal BARKOD As String, ByVal MIKTAR As Long, ByVal CARIKOD As String, ByVal BOXNO As String) As KW_Belge_Olustur()
        Dim SQ As String
        Dim DT As New DataTable
        Dim STOK As New STOK_BILGI
        Dim CARI As New XCari.Cari_Kartlar
        Dim cariAd As String
        Dim cariKodu As String
        Dim cariAdres1 As String
        Dim cariAdres2 As String
        Dim cariVno As String
        Dim cariVdaire As String
        Dim cariTel As String
        Dim cariId As Long
        Dim Belge_Id As Long
        Dim FT As New X_Fatura
        Dim ID As Long
        Dim SIRA As Long
        Dim stokId As Long
        Dim stokAdi As String
        Dim stokKodu As String
        Dim barkodd As String
        Dim birim As String
        Dim BELGE(0) As KW_Belge_Olustur
        Dim Belge_Nosu As String
        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()

        If STOK.BUL(BARKOD) = True Then

            stokKodu = STOK.SKART.S01_kod
            stokAdi = STOK.SKART.S02_adi
            stokId = STOK.SKART.S00_id
            birim = STOK.SKART.S03_birim
            barkodd = STOK.SKART.S11_barkod
        Else
            BELGE(0) = New KW_Belge_Olustur
            BELGE(0).DURUM = NUMARATOR2.HATA
            BELGE(0).STOKADI = " "
            BELGE(0).BELGE_ID = 0
            BELGE(0).CARI = " "
            BELGE(0).TARIH = Date.Now
            Return BELGE

        End If
        CARIKOD = Val(Trim(CARIKOD))
        If CARI.Kayit_Bul_KOD(CARIKOD) = True Then
            cariKodu = CARI.Cari_Hesap_Kodu
            cariAd = CARI.Cari_Hesap_Adi
            cariAdres1 = CARI.Fatura_Adres_1Satiri
            cariAdres2 = CARI.Fatura_Adres_2Satiri
            cariTel = CARI.Telefon_Isyeri_1
            cariVno = CARI.Vergi_Numarasi
            cariVdaire = CARI.Vergi_Dairesi
            cariId = CARI.ID

        Else
            BELGE(0) = New KW_Belge_Olustur
            BELGE(0).DURUM = NUMARATOR2.HATA
            BELGE(0).STOKADI = " "
            BELGE(0).BELGE_ID = 0
            BELGE(0).CARI = " "
            BELGE(0).TARIH = Date.Now
            Return BELGE

        End If

        SQ = ""
        SQ = "SELECT a_id,a_fcari,a_tarih FROM " & KUL.tper & "stkcmas where a_cari_id=" & cariId & " and a_tur=70 and a_tarih='" & TTAR.TR2UK(Date.Now.ToShortDateString) & "' and a_bloke=1"
        DT = SQL_TABLO(SQ, "T", False, False, False)
        If DT.Rows.Count = 0 Then
            FT.Fatura_Baslik.Fatura_Turu = 70

            ID = TSAY.MAX_FIS_SCFAT(70, 0)

            Select Case ID
                Case 0 To 9
                    Belge_Nosu = "ST-000000000." & ID
                Case 10 To 99
                    Belge_Nosu = "ST-00000000." & ID
                Case 100 To 999
                    Belge_Nosu = "ST-0000000." & ID
                Case 1000 To 9999
                    Belge_Nosu = "ST-000000." & ID
                Case 10000 To 99999
                    Belge_Nosu = "ST-00000." & ID
                Case 100000 To 999999
                    Belge_Nosu = "ST-0000." & ID
                Case 1000000 To 9999999
                    Belge_Nosu = "ST-000." & ID
                Case 10000000 To 99999999
                    Belge_Nosu = "ST-00." & ID
                Case Else
                    Belge_Nosu = ID
            End Select
            FT.Fatura_Baslik.Belge_Nosu = Belge_Nosu
            FT.Fatura_Baslik.Aciklaması = cariAd & cariAdres1
            FT.Fatura_Baslik.Fatura_Cari_Adi = cariAd
            FT.Fatura_Baslik.Cari_Id = cariId
            FT.Fatura_Baslik.Depo_Id = 1
            FT.Fatura_Baslik.Sube_Id = 1
            FT.Fatura_Baslik.Acik_Kapali = "A"
            FT.Fatura_Baslik.Fatura_Cari_AdresSat1 = cariAdres1
            FT.Fatura_Baslik.Fatura_Cari_AdresSat2 = cariAdres2
            FT.Fatura_Baslik.Fatura_Cari_VergiDairesi = cariVdaire
            FT.Fatura_Baslik.Fatura_Cari_VergiNo = cariVno
            FT.Fatura_Baslik.Bloke = 1
            'KUL.KOD = userid
            ID = FT.Fatura_Ekle()
            If ID > 0 Then

                SIRA = 1
                SQ = ""
                SQ = " INSERT INTO " & KUL.tper & "stkcdet ( a_tur, a_id, a_fattur, a_fatid, a_sira,a_sirano, a_tarih, a_cari_id,a_sube_id, a_stok_id, a_ambar_id, a_mik, a_brmfiy, a_kdv, a_kdvtut, a_isk, a_isktut, a_tutar, a_ack, a_bloke, a_kesin, a_sbrmmik, a_sbrm, a_sbrmcrp, a_sbrmfiy,a_did,a_dtut,a_dkur)"
                SQ = SQ & " Select  70 AS a_tur, " & ID & " AS a_id, 70 AS a_fattur, " & ID & " AS a_fatid, " & SIRA & " AS a_sira," & SIRA & " a_sirano, "
                SQ = SQ & " Convert(VARCHAR(10), GETDATE(), 110) As a_tarih, " & cariId & " As a_cari_id, 1 as a_sube_id ,"
                SQ = SQ & stokId & "As a_stok_id, 1 As a_ambar_id, " & MIKTAR & " As a_mik, 0 As a_brmfiy, 0 As a_kdv, 0 As a_kdvtut, 0 As a_isk, 0 As a_isktut, 0 As a_tutar, '" & BOXNO & "' AS a_ack, 0 AS a_bloke, 0 AS a_kesin,  " & MIKTAR & "  As a_sbrmmik, ' ' AS a_sbrm, 1 AS a_sbrmcrp,  0 AS a_sbrmfiy,1 as a_did,0 as a_dtut,1 as a_dkur"
                If SQL_KOS(B_SQL, SQ, False) = True Then
                    BELGE(0) = New KW_Belge_Olustur
                    BELGE(0).DURUM = NUMARATOR2.YENIBELGE
                    BELGE(0).STOKADI = stokAdi
                    BELGE(0).BELGE_ID = ID
                    BELGE(0).CARI = cariAd
                    BELGE(0).TARIH = Date.Now
                    Return BELGE
                Else
                    BELGE(0) = New KW_Belge_Olustur
                    BELGE(0).DURUM = NUMARATOR2.HATA
                    BELGE(0).STOKADI = " "
                    BELGE(0).BELGE_ID = 0
                    BELGE(0).CARI = " "
                    BELGE(0).TARIH = Date.Now
                    Return BELGE
                End If


            Else
                BELGE(0) = New KW_Belge_Olustur
                BELGE(0).DURUM = NUMARATOR2.HATA
                BELGE(0).STOKADI = " "
                BELGE(0).BELGE_ID = 0
                BELGE(0).CARI = " "
                BELGE(0).TARIH = Date.Now
                Return BELGE
            End If
        End If
        If DT.Rows.Count = 1 Then
            Belge_Id = NULN(DT, 0, "a_id")
            SQ = "select max(a_sira) AS SIRA from " & KUL.tper & "stkcdet where a_id=" & Belge_Id & " and a_tur=70 "
            DT = SQL_TABLO(SQ, "T", False, False, False)
            SIRA = NULN(DT, 0, "SIRA") + 1

            SQ = ""
            SQ = " INSERT INTO " & KUL.tper & "stkcdet ( a_tur, a_id, a_fattur, a_fatid, a_sira,a_sirano, a_tarih, a_cari_id,a_sube_id, a_stok_id, a_ambar_id, a_mik, a_brmfiy, a_kdv, a_kdvtut, a_isk, a_isktut, a_tutar, a_ack, a_bloke, a_kesin, a_sbrmmik, a_sbrm, a_sbrmcrp, a_sbrmfiy,a_did,a_dtut,a_dkur)"
            SQ = SQ & " Select  70 AS a_tur, " & Belge_Id & " AS a_id, 70 AS a_fattur, " & Belge_Id & " AS a_fatid, " & SIRA & " AS a_sira," & SIRA & " a_sirano, "
            SQ = SQ & " Convert(VARCHAR(10), GETDATE(), 110) As a_tarih, " & cariId & " As a_cari_id," & 1 & "as a_sube_id ,"
            SQ = SQ & stokId & "As a_stok_id, " & 1 & " As a_ambar_id, " & MIKTAR & " As a_mik, 0 As a_brmfiy, 0 As a_kdv, 0 As a_kdvtut, 0 As a_isk, 0 As a_isktut, 0 As a_tutar, '" & BOXNO & "' AS a_ack, 0 AS a_bloke, 0 AS a_kesin,  " & MIKTAR & "  As a_sbrmmik, ' ' AS a_sbrm, 1 AS a_sbrmcrp,  0 AS a_sbrmfiy,1 as a_did,0 as a_dtut,1 as a_dkur"
            If SQL_KOS(B_SQL, SQ, False) = True Then
                BELGE(0) = New KW_Belge_Olustur
                BELGE(0).DURUM = NUMARATOR2.BELGENINUSTUNE
                BELGE(0).STOKADI = stokAdi
                BELGE(0).BELGE_ID = Belge_Id
                BELGE(0).CARI = cariAd
                BELGE(0).TARIH = Date.Now
                Return BELGE
            Else
                BELGE(0) = New KW_Belge_Olustur
                BELGE(0).DURUM = NUMARATOR2.HATA
                BELGE(0).STOKADI = " "
                BELGE(0).BELGE_ID = 0
                BELGE(0).CARI = " "
                BELGE(0).TARIH = Date.Now
                Return BELGE
            End If

        End If
        If DT.Rows.Count > 1 Then
            Dim BLG(DT.Rows.Count - 1) As KW_Belge_Olustur
            For X = 0 To DT.Rows.Count - 1
                BLG(X) = New KW_Belge_Olustur
                BLG(X).DURUM = NUMARATOR2.BIRDENFAZLABELGE
                BELGE(X).STOKADI = stokAdi
                BLG(X).BELGE_ID = NULN(DT, X, "a_id")
                BLG(X).CARI = NULA(DT, X, "a_fcari")
                BLG(X).TARIH = NULA(DT, X, "a_tarih")
            Next
            Return BLG
        End If

        BELGE(0) = New KW_Belge_Olustur
        BELGE(0).DURUM = NUMARATOR.DEGERDONMEDI
        BELGE(0).STOKADI = " "
        BELGE(0).BELGE_ID = 0
        BELGE(0).CARI = " "
        BELGE(0).TARIH = Date.Now
        Return BELGE

    End Function
#End Region
    '#Region "MB/Yetki"
    '    Public Class MB_Mob_Yetki
    '        Public DURUM As Long
    '        Public YETKI As Long
    '        Public MODUL As Long
    '    End Class
    '    <WebMethod()>
    '    Public Function MB_Yetki(ByVal userid As Long) As MB_Mob_Yetki()

    '        Dim SQ As String
    '        Dim DT As New DataTable



    '        B_SQL = connstr
    '        SQL_TUR = "2005"
    '        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
    '        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
    '        SQ = ""
    '        SQ = "SELECT a_uid,a_mid,a_yetki FROM tbl_mob_yetki "
    '        SQ += "WHERE a_uid=" & userid
    '        DT = SQL_TABLO(SQ, "T", False, False, False)
    '        If DT.Rows.Count > 0 Then


    '            Dim BLG(DT.Rows.Count - 1) As MB_Mob_Yetki
    '            For X = 0 To DT.Rows.Count - 1
    '                BLG(X) = New MB_Mob_Yetki
    '                BLG(X).DURUM = NUMARATOR.BAŞARILI
    '                BLG(X).YETKI = NULN(DT, X, "a_yetki")
    '                BLG(X).MODUL = NULN(DT, X, "a_mid")
    '            Next
    '            Return BLG
    '        Else
    '            Dim BLG(0) As MB_Mob_Yetki
    '            BLG(0) = New MB_Mob_Yetki
    '            BLG(0).DURUM = NUMARATOR.HATA
    '            BLG(0).YETKI = 0
    '            BLG(0).MODUL = 0
    '            Return BLG
    '        End If



    '    End Function
    '#End Region
#Region "MB/Toplu Etiket Yazdırma"
    Public Class WS_Toplu_Etiket
        Public DURUM As Long
        Public STOKID As Long
        Public STOKKODU As String
        Public STOKADI As String
        Public STOKBARKODU As String
        Public MIKTAR As Long

    End Class

    <WebMethod()>
    Public Function MB_TopluEtiketYazdir(ByVal VERSION As String, ByVal USERID As Long, ByVal SUBEID As Long, ByVal STOKID As Long, ByVal STOKBARDKODU As String, ByVal MIKTAR As Long, ByVal BASTARIH As String, ByVal BITTARIH As String, ByVal YAZICIID As Long, ByVal ISLEM As Long) As WS_Toplu_Etiket()

        Dim STOK As New STOK_BILGI
        Dim SQ As String
        Dim DT As New DataTable
        Dim BELGE(0) As WS_Toplu_Etiket
        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        KUL.KOD = USERID
        Dim K As String = ""


        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"

            BELGE(0) = New WS_Toplu_Etiket
            BELGE(0).DURUM = NUMARATOR.APKVERSIYONESKI
            BELGE(0).STOKID = 0
            BELGE(0).STOKKODU = " "
            BELGE(0).STOKADI = " "
            BELGE(0).STOKBARKODU = " "
            BELGE(0).MIKTAR = 0
            Return BELGE

#End Region

        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

#Region "WS HATA"

            BELGE(0) = New WS_Toplu_Etiket
            BELGE(0).DURUM = NUMARATOR.WSVERSIYONESKI
            BELGE(0).STOKID = 0
            BELGE(0).STOKKODU = " "
            BELGE(0).STOKADI = " "
            BELGE(0).STOKBARKODU = " "
            BELGE(0).MIKTAR = 0
            Return BELGE

#End Region



        End If



        'TABLOYA EKLEME
        If ISLEM = 1 Then
            If STOK.BUL(STOKBARDKODU) = True Then
                SQ = ""
                SQ = "INSERT INTO " & KUL.tfrm & "mob_etiket( a_user, a_tarih, a_saat,a_sube, a_kod, a_adi, a_birim,a_barkod,a_miktar)"
                SQ = SQ & " VALUES "
                SQ = SQ & " ( "
                SQ = SQ & " " & USERID & ", "
                SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & "', "
                SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & " " & TTAR.SAAT & "', "
                SQ = SQ & "  " & SUBEID & " , "
                SQ = SQ & "  N'" & STOK.SKART.S01_kod & "', "
                SQ = SQ & "  N'" & STOK.SKART.S02_adi & "', "
                SQ = SQ & "  N'" & STOK.SKART.S03_birim & "' ,"
                SQ = SQ & "  N'" & STOK.SKART.S11_barkod & "' ,"
                SQ = SQ & "  " & MIKTAR & " "
                SQ = SQ & " ) "

                If SQL_KOS(SQ, False) = True Then

                    BELGE(0) = New WS_Toplu_Etiket
                    BELGE(0).DURUM = NUMARATOR.BAŞARILI
                    BELGE(0).STOKID = 0
                    BELGE(0).STOKKODU = STOK.SKART.S01_kod
                    BELGE(0).STOKADI = STOK.SKART.S02_adi
                    BELGE(0).STOKBARKODU = STOK.SKART.S11_barkod
                    BELGE(0).MIKTAR = MIKTAR
                    Return BELGE
                Else

                    BELGE(0) = New WS_Toplu_Etiket
                    BELGE(0).DURUM = NUMARATOR.HATA
                    BELGE(0).STOKID = 0
                    BELGE(0).STOKKODU = " "
                    BELGE(0).STOKADI = " "
                    BELGE(0).STOKBARKODU = " "
                    BELGE(0).MIKTAR = 0
                    Return BELGE
                End If
            Else

                BELGE(0) = New WS_Toplu_Etiket
                BELGE(0).DURUM = NUMARATOR.HATA
                BELGE(0).STOKID = 0
                BELGE(0).STOKKODU = " "
                BELGE(0).STOKADI = " "
                BELGE(0).STOKBARKODU = " "
                BELGE(0).MIKTAR = 0
                Return BELGE
            End If
        End If
        'TABLOYU TEMİZLEME
        If ISLEM = 2 Then
            SQ = ""
            SQ = "DELETE FROM " & KUL.tfrm & "mob_etiket where a_user=" & USERID

            If SQL_KOS(SQ, False) = True Then

                BELGE(0) = New WS_Toplu_Etiket
                BELGE(0).DURUM = NUMARATOR.BAŞARILI
                BELGE(0).STOKID = 0
                BELGE(0).STOKKODU = " "
                BELGE(0).STOKADI = " "
                BELGE(0).STOKBARKODU = " "
                BELGE(0).MIKTAR = 0
                Return BELGE
            Else

                BELGE(0) = New WS_Toplu_Etiket
                BELGE(0).DURUM = NUMARATOR.HATA
                BELGE(0).STOKID = 0
                BELGE(0).STOKKODU = " "
                BELGE(0).STOKADI = " "
                BELGE(0).STOKBARKODU = " "
                BELGE(0).MIKTAR = 0
                Return BELGE
            End If

        End If
        'SECILEN SATIR SİLME
        If ISLEM = 3 Then
            SQ = ""
            SQ = "DELETE FROM " & KUL.tfrm & "mob_etiket "
            SQ = SQ & " WHERE a_user=" & USERID & " AND a_id=" & STOKID
            If SQL_KOS(SQ, False) = True Then

                BELGE(0) = New WS_Toplu_Etiket
                BELGE(0).DURUM = NUMARATOR.BAŞARILI
                BELGE(0).STOKID = 0
                BELGE(0).STOKKODU = " "
                BELGE(0).STOKADI = " "
                BELGE(0).STOKBARKODU = " "
                BELGE(0).MIKTAR = 0
                Return BELGE
            Else

                BELGE(0) = New WS_Toplu_Etiket
                BELGE(0).DURUM = NUMARATOR.HATA
                BELGE(0).STOKID = 0
                BELGE(0).STOKKODU = " "
                BELGE(0).STOKADI = " "
                BELGE(0).STOKBARKODU = " "
                BELGE(0).MIKTAR = 0
                Return BELGE

            End If

        End If
        'LİSTELEME
        If ISLEM = 4 Then

            SQ = ""
            SQ = " SELECT a_id,a_kod,a_adi,a_barkod,a_miktar FROM " & KUL.tfrm & "mob_etiket WHERE a_user=" & USERID
            SQ = SQ & " ORDER BY a_id "
            DT = SQL_TABLO(SQ, "T", False, False, False)
            If DT.Rows.Count > 0 Then


                Dim BLG(DT.Rows.Count - 1) As WS_Toplu_Etiket
                For X = 0 To DT.Rows.Count - 1
                    BLG(X) = New WS_Toplu_Etiket
                    BLG(X).DURUM = NUMARATOR.BAŞARILI
                    BLG(X).STOKID = NULN(DT, X, "a_id")
                    BLG(X).STOKKODU = NULA(DT, X, "a_kod")
                    BLG(X).STOKADI = NULA(DT, X, "a_adi")
                    BLG(X).STOKBARKODU = NULA(DT, X, "a_barkod")
                    BLG(X).MIKTAR = NULN(DT, X, "a_miktar")

                Next
                Return BLG
            Else
                Dim BLG(0) As WS_Toplu_Etiket
                BLG(0) = New WS_Toplu_Etiket
                BLG(0).DURUM = NUMARATOR.HATA
                BLG(0).STOKID = 0
                BLG(0).STOKKODU = " "
                BLG(0).STOKADI = " "
                BLG(0).STOKBARKODU = " "
                BLG(0).MIKTAR = 0
                Return BLG
            End If


        End If
        'iki tarih arası fiyatları değişen stokları bulma
        If ISLEM = 5 Then


            SQ = ""
            SQ = " SELECT 'COKLU' as tur, row_number() over(order by STK.a_adi) AS siraNo, STK.a_barkod as barkod,  STK.a_adi as stokAdi, FD.a_ysfiyat as satisFiyati,STK.a_kod as KOD,STK.a_id as ID "
            SQ = SQ & "FROM " & KUL.tfrm & "stok  As STK  "
            SQ = SQ & " LEFT JOIN (SELECT a_sid AS [Stok Id], Max(a_cdate) AS [Tarih], max(a_id) AS [fdId] "
            SQ = SQ & " FROM " & KUL.tfrm & "stokfd GROUP BY a_sid) As FDFILTERED On FDFILTERED.[Stok Id] =  STK.a_id  "
            SQ = SQ & " LEFT JOIN " & KUL.tfrm & "stokfd AS FD ON FD.a_sid = FDFILTERED.[Stok Id] AND FD.a_cdate = FDFILTERED.Tarih and FD.a_id = FDFILTERED.fdId  "
            SQ = SQ & " WHERE   FDFILTERED.Tarih BETWEEN '" + BASTARIH + " 00:00:00' AND '" + BITTARIH + " 23:59:00' AND  FD.a_esfiyat <> FD.a_ysfiyat and STK.a_reftip not in ('A') "

            DT = SQL_TABLO(SQ, "T", False, False, False)
            If DT.Rows.Count > 0 Then


                Dim BLG(DT.Rows.Count - 1) As WS_Toplu_Etiket
                For X = 0 To DT.Rows.Count - 1
                    BLG(X) = New WS_Toplu_Etiket
                    BLG(X).DURUM = NUMARATOR.BAŞARILI
                    BLG(X).STOKID = NULN(DT, X, "ID")
                    BLG(X).STOKKODU = NULA(DT, X, "KOD")
                    BLG(X).STOKADI = NULA(DT, X, "stokAdi")
                    BLG(X).STOKBARKODU = NULA(DT, X, "barkod")
                    BLG(X).MIKTAR = 0

                Next
                Return BLG
            Else
                Dim BLG(0) As WS_Toplu_Etiket
                BLG(0) = New WS_Toplu_Etiket
                BLG(0).DURUM = NUMARATOR.HATA
                BLG(0).STOKID = 0
                BLG(0).STOKKODU = " "
                BLG(0).STOKADI = " "
                BLG(0).STOKBARKODU = " "
                BLG(0).MIKTAR = 0
                Return BLG
            End If
        End If
        If ISLEM = 6 Then
            Dim EKLENEMEYENBARKOD As String = ""
            Dim X() As String

            Dim Y As Long
            Dim C As Long = 0
            X = STOKBARDKODU.Split("|")
            Dim VERI(X.Length - 1) As String
            Dim MIKTARLAR(X.Length - 1) As String
            Dim BARKODLAR(X.Length - 1) As String
            For C = 0 To X.Length - 1
                VERI = X(C).Split(",")
                MIKTARLAR(C) = VERI(1)
                BARKODLAR(C) = VERI(0)
            Next
            Dim SONUC(X.Length - 1) As Boolean
            For Y = 0 To BARKODLAR.Length - 1
                If STOK.BUL(BARKODLAR(Y)) = True Then
                    SQ = ""
                    SQ = "INSERT INTO " & KUL.tfrm & "mob_etiket( a_user, a_tarih, a_saat,a_sube, a_kod, a_adi, a_birim,a_barkod,a_miktar)"
                    SQ = SQ & " VALUES "
                    SQ = SQ & " ( "
                    SQ = SQ & " " & USERID & ", "
                    SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & "', "
                    SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & " " & TTAR.SAAT & "', "
                    SQ = SQ & "  " & SUBEID & " , "
                    SQ = SQ & "  N'" & STOK.SKART.S01_kod & "', "
                    SQ = SQ & "  N'" & STOK.SKART.S02_adi & "', "
                    SQ = SQ & "  N'" & STOK.SKART.S03_birim & "' ,"
                    SQ = SQ & "  N'" & STOK.SKART.S11_barkod & "' ,"
                    SQ = SQ & "  " & MIKTARLAR(Y) & " "
                    SQ = SQ & " ) "
                    SONUC(Y) = SQL_KOS(SQ, False)
                Else
                    If EKLENEMEYENBARKOD = "" Then
                        EKLENEMEYENBARKOD = BARKODLAR(Y)
                    Else
                        EKLENEMEYENBARKOD = EKLENEMEYENBARKOD & "|" & BARKODLAR(Y)
                    End If


                    SONUC(Y) = False
                End If
            Next

            For Y = 0 To SONUC.Length - 1
                If SONUC(Y) = False Then

                    BELGE(0) = New WS_Toplu_Etiket
                    BELGE(0).DURUM = NUMARATOR.EKLENEMEYENURUNVAR
                    BELGE(0).STOKID = 0
                    BELGE(0).STOKKODU = " "
                    BELGE(0).STOKADI = " "
                    BELGE(0).STOKBARKODU = EKLENEMEYENBARKOD
                    BELGE(0).MIKTAR = 0
                    Return BELGE
                End If
            Next

            BELGE(0) = New WS_Toplu_Etiket
            BELGE(0).DURUM = NUMARATOR.BAŞARILI
            BELGE(0).STOKID = 0
            BELGE(0).STOKKODU = " "
            BELGE(0).STOKADI = " "
            BELGE(0).STOKBARKODU = " "
            BELGE(0).MIKTAR = 0
            Return BELGE

        End If
        If ISLEM = 7 Then 'mob_etiket tablosundakileri mob_pserver tablosuna ekleme
            Dim j As Long
            Dim m As Long

            SQ = ""
            SQ = " SELECT a_id,a_kod,a_adi,a_barkod,a_miktar FROM " & KUL.tfrm & "mob_etiket WHERE a_user=" & USERID
            SQ = SQ & " ORDER BY a_id "
            DT = SQL_TABLO(SQ, "T", False, False, False)
            If DT.Rows.Count > 0 Then

                For j = 0 To DT.Rows.Count - 1
                    For m = 0 To NULN(DT, j, "a_miktar") - 1
                        MB_EtiketYazdir(VERSION, USERID, SUBEID, NULA(DT, j, "a_barkod"), 0, YAZICIID)
                    Next
                Next

                BELGE(0) = New WS_Toplu_Etiket
                BELGE(0).DURUM = NUMARATOR.BAŞARILI
                BELGE(0).STOKID = 0
                BELGE(0).STOKKODU = " "
                BELGE(0).STOKADI = " "
                BELGE(0).STOKBARKODU = " "
                BELGE(0).MIKTAR = 0
                Return BELGE
            Else

                BELGE(0) = New WS_Toplu_Etiket
                BELGE(0).DURUM = NUMARATOR.UYARI
                BELGE(0).STOKID = 0
                BELGE(0).STOKKODU = " "
                BELGE(0).STOKADI = " "
                BELGE(0).STOKBARKODU = " "
                BELGE(0).MIKTAR = 0
                Return BELGE
            End If
        End If

        BELGE(0) = New WS_Toplu_Etiket
        BELGE(0).DURUM = NUMARATOR.DEGERDONMEDI
        BELGE(0).STOKID = 0
        BELGE(0).STOKKODU = " "
        BELGE(0).STOKADI = " "
        BELGE(0).STOKBARKODU = " "
        BELGE(0).MIKTAR = 0
        Return BELGE
    End Function
#End Region
#Region "MB/Kontrollu Irsaliye Ekleme"
    Public Class WS_KontrolluIrsaliye
        Public DURUM As Long
        Public BELGETURU As Long
        Public BELGENO As String
        Public ID As Long
        Public GONDERIMKODU As String
    End Class
    ''' <summary>
    ''' Gelen parametrelere göre belge eklemesi yapılıyor.
    ''' </summary>
    ''' <param name="BelgeTuru"> Belgenin Türü Örn:49 </param>
    ''' <param name="Aciklama"> Belge Açıklaması </param>
    ''' <param name="cariId"> Carinin Id'si </param>
    ''' <param name="depoId"> Depo Id'si(Bir depo kullanılan alanlarda kullanıyor.) </param>
    ''' <param name="SubeId"> Şube Id'si </param>
    ''' <returns></returns>
    '    <WebMethod()>
    '    Public Function MB_KontrolluIrsaliyeEkleme(ByVal VERSION As String, ByVal BelgeTuru As Long, ByVal Aciklama As String, ByVal cariId As Long, ByVal depoId As Long, ByVal SubeId As Long, ByVal userid As Long, ByVal gonderenfisid As Long, ByVal gonderenfistur As Long) As WS_KontrolluIrsaliye()
    '        Dim ID As Long
    '        Dim FT As New X_Fatura
    '        Dim SQ As String
    '        Dim DT As New DataTable
    '        Dim CariAd As String = " "
    '        Dim BelgeNo As String = " "
    '        Dim FADRES1 As String = " "
    '        Dim FADRES2 As String = " "
    '        Dim FVDAIRE As String = " "
    '        Dim FVNO As String = " "
    '        Dim gonderimKodu As String = " "
    '        Dim BELGE(0) As WS_KontrolluIrsaliye
    '        'Dim VT As String

    '        B_SQL = connstr

    '        SQL_TUR = "2005"
    '        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
    '        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()




    '        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


    '#Region "APK HATA"
    '            BELGE(0) = New WS_KontrolluIrsaliye
    '            BELGE(0).DURUM = NUMARATOR.APKVERSIYONESKI
    '            BELGE(0).ID = 0
    '            BELGE(0).BELGETURU = 0
    '            BELGE(0).BELGENO = BelgeNo
    '            BELGE(0).GONDERIMKODU = " "
    '            Return BELGE
    '#End Region

    '        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

    '#Region "WS HATA"
    '            BELGE(0) = New WS_KontrolluIrsaliye
    '            BELGE(0).DURUM = NUMARATOR.WSVERSIYONESKI
    '            BELGE(0).ID = 0
    '            BELGE(0).BELGETURU = 0
    '            BELGE(0).BELGENO = BelgeNo
    '            BELGE(0).GONDERIMKODU = " "
    '            Return BELGE

    '#End Region



    '        End If



    '        SQ = " "
    '        SQ = "Select a_id FROM " & KUL.tper & "maxid"
    '        SQ = SQ & " WHERE a_id=" & BelgeTuru

    '        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
    '        If DT.Rows.Count > 0 Then

    '            SQ = ""
    '            SQ = "Select a_adi, a_iskoran,a_bakiye,a_bakiyetur,a_fadres1,a_fadres2,a_ssehir,a_vn,a_vd"
    '            SQ = SQ & " FROM " & KUL.tfrm & "cari"
    '            SQ = SQ & " WHERE a_id=" & cariId & ""
    '            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
    '            CariAd = NULA(DT, 0, "a_adi")
    '            FADRES1 = NULA(DT, 0, "a_fadres1")
    '            FADRES2 = NULA(DT, 0, "a_fadres2")
    '            FVNO = NULA(DT, 0, "a_vn")
    '            FVDAIRE = NULA(DT, 0, "a_vd")
    '            FT.Fatura_Baslik.Fatura_Turu = BelgeTuru
    '            FT.Fatura_Baslik.Belge_Nosu = "0"
    '            FT.Fatura_Baslik.Aciklaması = Aciklama
    '            FT.Fatura_Baslik.Fatura_Cari_Adi = CariAd
    '            FT.Fatura_Baslik.Cari_Id = cariId
    '            FT.Fatura_Baslik.Depo_Id = depoId
    '            FT.Fatura_Baslik.Sube_Id = SubeId
    '            FT.Fatura_Baslik.Belge_Nosu = BelgeNo
    '            FT.Fatura_Baslik.Acik_Kapali = "A"
    '            FT.Fatura_Baslik.Fatura_Cari_AdresSat1 = FADRES1
    '            FT.Fatura_Baslik.Fatura_Cari_AdresSat2 = FADRES2
    '            FT.Fatura_Baslik.Fatura_Cari_VergiDairesi = FVDAIRE
    '            FT.Fatura_Baslik.Fatura_Cari_VergiNo = FVNO
    '            KUL.KOD = userid


    '            ID = FT.Fatura_Ekle()
    '            If ID > 0 Then
    '                B_SQL = connstr2

    '                If BelgeTuru = 41 Or BelgeTuru = 69 Then
    '                    gonderimKodu = sifre_uret(6)



    '                    SQ = ""
    '                    SQ = "INSERT INTO TBL_KONTROLKOD (a_key,a_gfid,a_gvtid,a_gbelid,a_gbeltur,a_afid,a_avtid,a_abelid,a_abeltur,a_durum) "
    '                    SQ = SQ & " Select "
    '                    SQ = SQ & " N'" & gonderimKodu & "' as a_key,"
    '                    SQ = SQ & " N'" & sirketid & "' as a_gfid,"
    '                    SQ = SQ & " N'" & VT & "' as a_gvtid,"
    '                    SQ = SQ & " " & ID & " as a_gbelid,"
    '                    SQ = SQ & " " & BelgeTuru & " as a_gbeltur,"
    '                    SQ = SQ & " 0  as a_afid,"
    '                    SQ = SQ & " N'0' as a_avtid,"
    '                    SQ = SQ & " 0 as a_abelid,"
    '                    SQ = SQ & " 0 as a_abeltur,"
    '                    SQ = SQ & " 0 as a_durum"
    '                    SQL_KOS(B_SQL, SQ, False)

    '                ElseIf BelgeTuru = 43 Or 95 Then

    '                    SQ = ""
    '                    SQ = " UPDATE TBL_KONTROLKOD SET a_afid=" & sirketid & ",a_abeltur=" & BelgeTuru & ", a_abelid=" & ID & ", a_avtid=" & VT & ", a_durum=1"
    '                    SQ = SQ & " WHERE a_gbelid=" & gonderenfisid & " AND a_gbeltur=" & gonderenfistur
    '                    SQL_KOS(B_SQL, SQ, False)

    '                End If
    '            End If
    '            B_SQL = connstr


    '            If ID = 0 Then
    '                BELGE(0) = New WS_KontrolluIrsaliye
    '                BELGE(0).DURUM = NUMARATOR.HATA
    '                BELGE(0).ID = 0
    '                BELGE(0).BELGETURU = 0
    '                BELGE(0).BELGENO = BelgeNo
    '                BELGE(0).GONDERIMKODU = " "
    '                Return BELGE
    '            Else



    '                BELGE(0) = New WS_KontrolluIrsaliye
    '                BELGE(0).DURUM = NUMARATOR.BAŞARILI
    '                BELGE(0).ID = ID
    '                BELGE(0).BELGETURU = BelgeTuru
    '                BELGE(0).BELGENO = BelgeNo
    '                BELGE(0).GONDERIMKODU = gonderimKodu
    '                Return BELGE


    '            End If



    '        End If
    '        BELGE(0) = New WS_KontrolluIrsaliye
    '        BELGE(0).DURUM = NUMARATOR.DEGERDONMEDI
    '        BELGE(0).ID = 0
    '        BELGE(0).BELGETURU = 0
    '        BELGE(0).BELGENO = BelgeNo
    '        BELGE(0).GONDERIMKODU = " "
    '        Return BELGE
    '    End Function
#End Region
#Region "MB/Şirket Bul"



    'Public Function MB_SirketBul(ByVal islem As Long)


    '    Dim SQ As String
    '    Dim DT As New DataTable

    '    B_SQL = connstr
    '    SQL_TUR = "2005"
    '    KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
    '    KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
    '    B_SQL = connstr2
    '    Dim id As Long
    '    Dim vt As String
    '    Dim a_id As String

    '    If islem = 1 Then
    '        SQ = ""
    '        SQ = "SELECT * FROM ANL_COMPANIES WHERE code='" & initialcatalog & "'"
    '        DT = SQL_TABLO(SQ, "T", False, False, False)
    '        id = NULN(DT, 0, "id")
    '        Return id
    '    ElseIf islem = 2 Then
    '        SQ = ""
    '        SQ = "SELECT * FROM ANL_COMPANIES WHERE code='" & initialcatalog & "'"
    '        DT = SQL_TABLO(SQ, "T", False, False, False)
    '        vt = NULA(DT, 0, "a_vt")
    '        Return vt
    '    ElseIf islem = 3 Then
    '        SQ = ""
    '        SQ = "SELECT * FROM ANL_COMPANIES WHERE code='" & initialcatalog & "'"
    '        DT = SQL_TABLO(SQ, "T", False, False, False)
    '        a_id = NULA(DT, 0, "a_id")
    '        Return a_id
    '    End If




    'End Function
#End Region
#Region "MB/Kontrollu Irsaliye Teslim Alma"
    ''' <summary>
    ''' 
    ''' </summary> 'LONG v DOUBLE TEMİZLENDİ
    Public Class MB_IrsaliyeTeslimAl
        Public DURUM As Long
        Public BELGETURU As Long
        Public BELGEID As Long
        Public STOKID As Long
        Public STOKKODU As String
        Public STOKBARKOD As String
        Public STOKADI As String
        Public MIKTAR As Double
        Public KONTROL As Long
        Public BIRIMFIYAT As Double
    End Class
    '    <WebMethod()>
    '    Public Function MB_KontrolluIrsaliyeTeslimAlma(ByVal VERSION As String, ByVal KEY As String) As MB_IrsaliyeTeslimAl()

    '        Dim SQ As String
    '        Dim DT As New DataTable
    '        Dim DT2 As New DataTable
    '        Dim DT3 As New DataTable
    '        Dim belgeid As Long
    '        Dim al_firmaid As Long
    '        Dim kontrol As Long
    '        Dim belgetur As Long
    '        Dim VT As String
    '        Dim FIRMA As String
    '        Dim gonfirmaid As Long
    '        Dim gvtid As Long
    '        Dim TABLO As String = ""
    '        Dim x As Long
    '        Dim a As DataRow
    '        B_SQL = connstr
    '        SQL_TUR = "2005"
    '        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
    '        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()

    '        DT3.Columns.Add("a_kodu", GetType(String))
    '        DT3.Columns.Add("a_adi", GetType(String))
    '        DT3.Columns.Add("a_barkod", GetType(String))


    '        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


    '#Region "APK HATA"
    '            Dim VERI(0) As MB_IrsaliyeTeslimAl
    '            VERI(0) = New MB_IrsaliyeTeslimAl
    '            VERI(0).DURUM = NUMARATOR.APKVERSIYONESKI
    '            VERI(0).BELGETURU = 0
    '            VERI(0).BELGEID = 0
    '            VERI(0).STOKID = 0
    '            VERI(0).STOKKODU = " "
    '            VERI(0).STOKADI = " "
    '            VERI(0).STOKBARKOD = " "
    '            VERI(0).MIKTAR = 0
    '            VERI(0).KONTROL = 0
    '            Return VERI
    '#End Region

    '        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

    '#Region "WS HATA"
    '            Dim VERI(0) As MB_IrsaliyeTeslimAl
    '            VERI(0) = New MB_IrsaliyeTeslimAl
    '            VERI(0).DURUM = NUMARATOR.WSVERSIYONESKI
    '            VERI(0).BELGETURU = 0
    '            VERI(0).BELGEID = 0
    '            VERI(0).STOKID = 0
    '            VERI(0).STOKKODU = " "
    '            VERI(0).STOKADI = " "
    '            VERI(0).STOKBARKOD = " "
    '            VERI(0).MIKTAR = 0
    '            VERI(0).KONTROL = 0
    '            Return VERI

    '#End Region



    '        End If



    '        If KEY.Length = 6 Then
    '            B_SQL = connstr2


    '            SQ = ""
    '            SQ = "SELECT  a_gfid,a_gbelid,a_durum,a_gbeltur,a_gvtid,a_afid FROM TBL_KONTROLKOD WHERE  a_key='" & KEY & "' AND a_durum=0 "
    '            DT = SQL_TABLO(SQ, "T", False, False, False)
    '            belgeid = NULN(DT, 0, "a_gbelid")
    '            belgetur = NULN(DT, 0, "a_gbeltur")
    '            kontrol = NULN(DT, 0, "a_durum")
    '            gonfirmaid = NULN(DT, 0, "a_gfid")
    '            gvtid = NULN(DT, 0, "a_gvtid")


    '            If DT.Rows.Count = 0 Then
    '                Dim VERI(0) As MB_IrsaliyeTeslimAl
    '                VERI(0) = New MB_IrsaliyeTeslimAl
    '                VERI(0).DURUM = NUMARATOR.HATA
    '                VERI(0).BELGETURU = 0
    '                VERI(0).BELGEID = 0
    '                VERI(0).STOKID = 0
    '                VERI(0).STOKKODU = " "
    '                VERI(0).STOKADI = " "
    '                VERI(0).STOKBARKOD = " "
    '                VERI(0).MIKTAR = 0
    '                VERI(0).KONTROL = 0
    '                Return VERI

    '            End If


    '            SQ = ""
    '            SQ = "SELECT id,a_id,a_vt FROM ANL_COMPANIES WHERE id=" & gvtid
    '            DT = SQL_TABLO(SQ, "T", False, False, False)
    '            VT = NULA(DT, 0, "a_vt")
    '            FIRMA = NULA(DT, 0, "a_id")

    '            If kontrol = 0 Then
    '                If belgetur = 69 Then
    '                    TABLO = "stkgdet"
    '                ElseIf belgetur = 41 Then
    '                    TABLO = "stkcdet"

    '                End If
    '                SQ = ""
    '                SQ = "Select STK.a_id As STOKID,STKH.a_sira As SIRA,STK.a_kod As KOD,STK.a_barkod As BARKOD,STK.a_adi As AD,STKH.a_mik As MIKTAR,STKH.a_brmfiy as BIRIMFIYAT "
    '                SQ = SQ & " FROM " & VT & ".[tbl_" & FIRMA & "_01_" & TABLO & "] As STKH INNER JOIN " & VT & ".[tbl_" & FIRMA & "_stok] As STK On STKH.a_stok_id=STK.a_id "
    '                SQ = SQ & " WHERE STKH.a_id=" & belgeid & " And STKH.a_tur=" & belgetur
    '                DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)


    '                B_SQL = connstr
    '                For x = 0 To DT.Rows.Count - 1
    '                    SQ = ""
    '                    SQ = "Select a_id As STOKID,a_kod as KOD,a_barkod As BARKOD,a_adi As AD "
    '                    SQ = SQ & " FROM " & KUL.tfrm & "stok   "
    '                    SQ = SQ & " WHERE a_barkod='" & NULA(DT, x, "BARKOD") & "' "
    '                    DT2 = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
    '                    If DT2.Rows.Count > 0 Then

    '                        Continue For
    '                    Else

    '                        DT3.Rows.Add(NULA(DT, x, "KOD"), NULA(DT, x, "AD"), NULA(DT, x, "BARKOD"))
    '                    End If

    '                Next

    '                If DT3.Rows.Count > 0 Then
    '                    Dim VERI(DT3.Rows.Count - 1) As MB_IrsaliyeTeslimAl
    '                    For x = 0 To DT3.Rows.Count - 1
    '                        VERI(x) = New MB_IrsaliyeTeslimAl
    '                        VERI(x).DURUM = NUMARATOR.TESLIMALMAHATASI
    '                        VERI(x).BELGETURU = 0
    '                        VERI(x).BELGEID = 0
    '                        VERI(x).STOKID = 0
    '                        VERI(x).STOKKODU = NULA(DT3, x, "a_kodu")
    '                        VERI(x).STOKADI = NULA(DT3, x, "a_adi")
    '                        VERI(x).STOKBARKOD = NULA(DT3, x, "a_barkod")
    '                        VERI(x).MIKTAR = 0
    '                        VERI(x).BIRIMFIYAT = 0
    '                        VERI(x).KONTROL = 0
    '                    Next
    '                    Return VERI
    '                End If
    '                If DT.Rows.Count > 0 Then
    '                    Dim VERI(DT.Rows.Count - 1) As MB_IrsaliyeTeslimAl
    '                    For x = 0 To DT.Rows.Count - 1
    '                        VERI(x) = New MB_IrsaliyeTeslimAl
    '                        VERI(x).DURUM = NUMARATOR.BAŞARILI
    '                        VERI(x).BELGETURU = belgetur
    '                        VERI(x).BELGEID = belgeid
    '                        VERI(x).STOKID = NULN(DT, x, "STOKID")
    '                        VERI(x).STOKKODU = NULA(DT, x, "KOD")
    '                        VERI(x).STOKADI = NULA(DT, x, "AD")
    '                        VERI(x).STOKBARKOD = NULA(DT, x, "BARKOD")
    '                        VERI(x).MIKTAR = NULD(DT, x, "MIKTAR")
    '                        VERI(x).BIRIMFIYAT = NULD(DT, x, "BIRIMFIYAT")
    '                        VERI(x).KONTROL = 0
    '                    Next
    '                    Return VERI
    '                Else
    '                    Dim VERI(0) As MB_IrsaliyeTeslimAl

    '                    VERI(0) = New MB_IrsaliyeTeslimAl
    '                    VERI(0).DURUM = NUMARATOR.HATA
    '                    VERI(0).BELGETURU = 0
    '                    VERI(0).BELGEID = 0
    '                    VERI(0).STOKID = 0
    '                    VERI(0).STOKKODU = " "
    '                    VERI(0).STOKADI = " "
    '                    VERI(0).STOKBARKOD = " "
    '                    VERI(0).MIKTAR = 0
    '                    VERI(0).BIRIMFIYAT = 0
    '                    VERI(0).KONTROL = 0
    '                    Return VERI

    '                End If
    '            Else
    '                Dim VERI(0) As MB_IrsaliyeTeslimAl

    '                VERI(0) = New MB_IrsaliyeTeslimAl
    '                VERI(0).DURUM = NUMARATOR.UYARI
    '                VERI(0).BELGETURU = 0
    '                VERI(0).BELGEID = 0
    '                VERI(0).STOKID = 0
    '                VERI(0).STOKKODU = " "
    '                VERI(0).STOKADI = " "
    '                VERI(0).STOKBARKOD = " "
    '                VERI(0).MIKTAR = 0
    '                VERI(0).BIRIMFIYAT = 0
    '                VERI(0).KONTROL = 0
    '                Return VERI

    '            End If
    '        Else
    '            Dim VERI(0) As MB_IrsaliyeTeslimAl

    '            VERI(0) = New MB_IrsaliyeTeslimAl
    '            VERI(0).DURUM = NUMARATOR.UYARI
    '            VERI(0).BELGETURU = 0
    '            VERI(0).BELGEID = 0
    '            VERI(0).STOKID = 0
    '            VERI(0).STOKKODU = " "
    '            VERI(0).STOKADI = " "
    '            VERI(0).STOKBARKOD = " "
    '            VERI(0).MIKTAR = 0
    '            VERI(0).BIRIMFIYAT = 0
    '            VERI(0).KONTROL = 0
    '            Return VERI
    '        End If


    '    End Function
#End Region
#Region "MB/Yazici Listeleme"
    Public Class MB_YAZICI
        Public DURUM As Long
        Public ID As Long
        Public ADI As String

    End Class

    <WebMethod()>
    Public Function MB_Yazicilar(ByVal VERSION As String, ByVal KULLANICIID As Long, ByVal KULLANICIGRUPID As Long) As MB_YAZICI()
        Dim SQ As String
        Dim DT As New DataTable
        Dim DT2 As New DataTable

        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()



        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"
            Dim VERI1(0) As MB_YAZICI
            VERI1(0) = New MB_YAZICI
            VERI1(0).DURUM = NUMARATOR.APKVERSIYONESKI
            VERI1(0).ID = 0
            VERI1(0).ADI = 0

            Return VERI1
#End Region
        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski


#Region "WS HATA"
            Dim VERI1(0) As MB_YAZICI
            VERI1(0) = New MB_YAZICI
            VERI1(0).DURUM = NUMARATOR.WSVERSIYONESKI
            VERI1(0).ID = 0
            VERI1(0).ADI = 0

            Return VERI1

#End Region



        End If

        If KULLANICIGRUPID = 1 Then
            SQ = ""
            SQ = "SELECT a_id,a_yazici "
            SQ = SQ & "  FROM " & KUL.tfrm & "mob_yazyol "
            DT = SQL_TABLO(SQ, "T", False, False, False)
            If DT.Rows.Count > 0 Then
                Dim VERI(DT.Rows.Count - 1) As MB_YAZICI


                For X = 0 To DT.Rows.Count - 1

                    VERI(X) = New MB_YAZICI
                    VERI(X).DURUM = NUMARATOR.BAŞARILI
                    VERI(X).ID = NULN(DT, X, "a_id")
                    VERI(X).ADI = NULA(DT, X, "a_yazici")


                Next

                Return VERI
            Else
                Dim VERI1(0) As MB_YAZICI
                VERI1(0) = New MB_YAZICI
                VERI1(0).DURUM = NUMARATOR.HATA
                VERI1(0).ID = 0
                VERI1(0).ADI = 0

                Return VERI1
            End If
        Else
            'Yönetici değilse yetkili olduğu şubeleri getir.
            SQ = ""
            SQ = "SELECT Sube.a_id, Sube.a_adi,Mobway.a_id AS ID,Mobway.a_yazici as AD"
            SQ = SQ & " FROM " & KUL.tfrm & "sube_kul AS SubeKul"
            SQ = SQ & "  INNER JOIN " & KUL.tfrm & "sube AS Sube ON SubeKul.a_subeid = Sube.a_id "
            SQ = SQ & "  INNER JOIN " & KUL.tfrm & "mob_yazyol AS mobway ON Mobway.a_sube = Sube.a_id "
            SQ = SQ & "  WHERE SubeKul.a_userid=" & KULLANICIID


            DT = SQL_TABLO(SQ, "T", False, False, False)
            If DT.Rows.Count > 0 Then
                Dim VERI1(DT.Rows.Count - 1) As MB_YAZICI


                For X = 0 To DT.Rows.Count - 1

                    VERI1(X) = New MB_YAZICI
                    VERI1(X).DURUM = NUMARATOR.BAŞARILI
                    VERI1(X).ID = NULN(DT, X, "ID")
                    VERI1(X).ADI = NULA(DT, X, "AD")


                Next

                Return VERI1
            Else
                Dim VERI1(0) As MB_YAZICI
                VERI1(0) = New MB_YAZICI
                VERI1(0).DURUM = NUMARATOR.HATA
                VERI1(0).ID = 0
                VERI1(0).ADI = 0

                Return VERI1
            End If
        End If




    End Function
#End Region
#Region "MB/Depo Sevk Urun Ekleme"
    'LONG v DOUBLE TEMİZLENDİ
    <WebMethod()>
    Public Function MB_DepoSevkUrunEkle(ByVal VERSION As String, ByVal BELGETURU As String, ByVal BARKOD As String, ByVal BELGEID As Long, ByVal STOKID As Long, ByVal MIKTAR As Double, ByVal REFTIPI As String, ByVal DEPOID As Long, ByVal OPLANID As Long, ByVal SUBEID As Long, ByVal CARIID As Long, ByVal MIKTARKONTROL As Long, ByVal URUNFIYAT As Double, ByVal USERID As Long, ByVal URUNFIYATTUR As String, ByVal EKLEMETURU As Long, ByVal SIRA_ As Long) As WS_UrunEkle()

        Dim SQ As String
        Dim DT As New DataTable
        Dim DT1 As New DataTable
        Dim DT2 As New DataTable
        Dim SIRA As Long = 0
        Dim fisStokKontrol As Long
        Dim gecerliFiyat As Double
        Dim birimfiyat As Double
        Dim birim As String
        Dim birimCarpan As Double
        Dim StokMiktar As Double
        Dim gelenmiktar As Double
        Dim tutartoplam As Double
        Dim stokAdi As String
        Dim stokKodu As String
        Dim birimBarkod As String
        Dim STOK_ID As String
        Dim barkodd As String
        Dim stoktip As String
        Dim kdval As Double = 0
        Dim kdvsat As Double = 0
        Dim CARIKOD As String
        Dim BELGE(0) As WS_UrunEkle

        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"

            BELGE(0) = New WS_UrunEkle
            BELGE(0).DURUM = NUMARATOR.APKVERSIYONESKI
            BELGE(0).BELGETURU = " "
            BELGE(0).BARKOD = " "
            BELGE(0).AD = " "
            BELGE(0).KOD = " "
            BELGE(0).MIKTAR = 0
            BELGE(0).MEVCUTMIKTAR = 0

            Return BELGE
#End Region

        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

#Region "WS HATA"

            BELGE(0) = New WS_UrunEkle
            BELGE(0).DURUM = NUMARATOR.WSVERSIYONESKI
            BELGE(0).BELGETURU = " "
            BELGE(0).BARKOD = " "
            BELGE(0).AD = " "
            BELGE(0).KOD = " "
            BELGE(0).MIKTAR = 0
            BELGE(0).MEVCUTMIKTAR = 0

            Return BELGE

#End Region



        End If

        B_SQL = connstr
        Dim STOK As New STOK_BILGI
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        SQ = ""
        SQ = "Select TOP(1) a_kod "
        SQ = SQ & "From " & KUL.tfrm & "cari "
        SQ = SQ & " Where a_id = " & CARIID & " And a_akpas ='Aktif' "
        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
        CARIKOD = NULA(DT, 0, "a_kod")
        If MIKTARKONTROL = 1 Then
            SQ = ""
            SQ = " SELECT a_kalan FROM " & KUL.tfrm & "stokdeg WHERE a_ambid=" & DEPOID & " AND a_id=" & STOKID
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            StokMiktar = NULD(DT, 0, "a_kalan")
            If MIKTAR > StokMiktar Then

                BELGE(0) = New WS_UrunEkle
                BELGE(0).DURUM = NUMARATOR.HATA
                BELGE(0).BELGETURU = " "
                BELGE(0).BARKOD = " "
                BELGE(0).AD = " "
                BELGE(0).KOD = " "
                BELGE(0).MIKTAR = 0
                BELGE(0).MEVCUTMIKTAR = 0

                Return BELGE
            End If
        End If


        If STOK.BUL(BARKOD) = True Then

            stokKodu = STOK.SKART.S01_kod
            stokAdi = STOK.SKART.S02_adi
            STOK_ID = STOK.SKART.S00_id
            birim = STOK.SKART.S03_birim
            birimCarpan = STOK.SKART.S98_Carpan
            barkodd = STOK.SKART.S11_barkod
            stoktip = STOK.SKART.S43_a_reftip
            Double.TryParse(STOK.SKART.S07_alkdvor, kdval)
            Double.TryParse(STOK.SKART.S07_alkdvor, kdvsat)
            If URUNFIYATTUR = "@" Then
                'Long.TryParse(BELGETURU, BELGETURU)
                'Select Case BELGETURU


                '    Case BELGETURU = 17, 18, 19, 20, 21, 22, 71, 87, 88, 43, 44, 69, 98
                '        İlgili faturalarda kdv dahil fiyattan kdv düş
                '        gecerliFiyat = URUNFIYAT / (1 + (kdval / 100))
                '    Case BELGETURU = 11, 13, 14, 16, 70, 75, 40, 41, 42, 96, 95, 94
                '        gecerliFiyat = URUNFIYAT / (1 + (kdvsat / 100))
                '    Case Else
                gecerliFiyat = URUNFIYAT
                'End Select
            ElseIf URUNFIYATTUR = "-1" Then
                gecerliFiyat = 0

            End If
        Else
            BELGE(0) = New WS_UrunEkle
            BELGE(0).DURUM = NUMARATOR.UYARI
            BELGE(0).BELGETURU = " "
            BELGE(0).BARKOD = " "
            BELGE(0).AD = " "
            BELGE(0).KOD = " "
            BELGE(0).MIKTAR = 0
            BELGE(0).MEVCUTMIKTAR = 0
            Return BELGE

            'End If
        End If
        If BELGETURU = 49 Then

            'SQ = ""
            'SQ = "Select COUNT(a_id) As kontrol FROM " & KUL.tper & "stkhdet "
            'SQ = SQ & " WHERE a_tur = " & BELGETURU & " And a_id = " & BELGEID & " And a_stok_id = " & STOKID
            'DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)

            'fisStokKontrol = NULN(DT, 0, "kontrol")

            SQ = ""
            SQ = "Select a_sira,a_sirano FROM " & KUL.tper & "stkhdet"
            SQ = SQ & " WHERE a_tur=" & BELGETURU & " And a_id=" & BELGEID & " order by a_sira desc"

            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            SIRA = NULN(DT, 0, "a_sira")
            If SIRA = Nothing Then
                SIRA = 1
            Else
                SIRA = SIRA + 1
            End If

            gelenmiktar = birimCarpan * MIKTAR
            If EKLEMETURU = 1 Then
                SQ = ""
                SQ = " UPDATE " & KUL.tper & "stkhdet SET a_sbrmmik = a_sbrmmik + " & MIKTAR & ", a_mik = a_mik + " & gelenmiktar & ", a_tutar = a_brmfiy * (a_mik + " & gelenmiktar & ") "
                SQ = SQ & " WHERE a_tur IN (49, 50) And a_id = " & BELGEID & " And a_stok_id = " & STOKID & " AND a_sira=" & SIRA_
                SQL_KOS(B_SQL, SQ, False)
            End If
            If EKLEMETURU = 2 Then


                SQ = ""
                SQ = " INSERT INTO " & KUL.tper & "stkhdet (a_gc, a_tur, a_id, a_fattur, a_fatid, a_sira,a_sirano, a_tarih, a_cari_id, a_stok_id, a_ambar_id, a_mik, a_brmfiy, a_kdv, a_kdvtut, a_isk, a_isktut, a_tutar, a_ack, a_bloke, a_kesin, a_sbrmmik, a_sbrm, a_sbrmcrp, a_sbrmfiy)"
                SQ = SQ & " Select 'C' AS a_gc, 49 AS a_tur, " & BELGEID & " AS a_id, 49 AS a_fattur, " & BELGEID & " AS a_fatid, " & SIRA & " AS a_sira," & SIRA & " a_sirano, "
                SQ = SQ & " Convert(VARCHAR(10), GETDATE(), 110) As a_tarih, 0 As a_cari_id, "

                Select Case STOK.SKART.S99_Nerede_Buldun

                    Case STOK_BILGI.Bul_Tur.Stok_1BrmBarkod
                        birimBarkod = STOK.SKART.S26_a_1brmbar
                        birimCarpan = STOK.SKART.S25_a_1brmcar
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S24_a_1birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = MIKTAR * birimfiyat

                        SQ = SQ & STOK_ID & "As a_stok_id, " & DEPOID & " As a_ambar_id , " & gelenmiktar & " As a_mik, " & birimfiyat & " As a_brmfiy, 0 As a_kdv, 0 As a_kdvtut, 0 As a_isk, 0 As a_isktut, " & tutartoplam & " As a_tutar, N'' AS a_ack, 0 AS a_bloke, 0 AS a_kesin, " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy"
                        SQL_KOS(B_SQL, SQ, False)
                    Case STOK_BILGI.Bul_Tur.Stok_2BrmBarkod
                        birimBarkod = STOK.SKART.S31_a_2brmbar
                        birimCarpan = STOK.SKART.S30_a_2brmcar
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S29_a_2birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = MIKTAR * birimfiyat
                        SQ = SQ & STOK_ID & "As a_stok_id, " & DEPOID & " As a_ambar_id , " & gelenmiktar & " As a_mik, " & birimfiyat & " As a_brmfiy, 0 As a_kdv, 0 As a_kdvtut, 0 As a_isk, 0 As a_isktut, " & tutartoplam & " As a_tutar, N'' AS a_ack, 0 AS a_bloke, 0 AS a_kesin, " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy"
                        SQL_KOS(B_SQL, SQ, False)

                    Case STOK_BILGI.Bul_Tur.Stok_Barkod
                        birimBarkod = STOK.SKART.S11_barkod
                        birimCarpan = STOK.SKART.S98_Carpan
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S03_birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = MIKTAR * birimfiyat
                        SQ = SQ & STOK_ID & "As a_stok_id, " & DEPOID & " As a_ambar_id , " & gelenmiktar & " As a_mik, " & birimfiyat & " As a_brmfiy, 0 As a_kdv, 0 As a_kdvtut, 0 As a_isk, 0 As a_isktut, " & tutartoplam & " As a_tutar, N'' AS a_ack, 0 AS a_bloke, 0 AS a_kesin, " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy"
                        SQL_KOS(B_SQL, SQ, False)
                    Case STOK_BILGI.Bul_Tur.Stok_Kodu
                        birimBarkod = STOK.SKART.S11_barkod
                        birimCarpan = STOK.SKART.S98_Carpan
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S03_birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = MIKTAR * birimfiyat
                        SQ = SQ & STOK_ID & "As a_stok_id, " & DEPOID & " As a_ambar_id , " & gelenmiktar & " As a_mik, " & birimfiyat & " As a_brmfiy, 0 As a_kdv, 0 As a_kdvtut, 0 As a_isk, 0 As a_isktut, " & tutartoplam & " As a_tutar, N'' AS a_ack, 0 AS a_bloke, 0 AS a_kesin, " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy"
                        SQL_KOS(B_SQL, SQ, False)
                    Case STOK_BILGI.Bul_Tur.Stok_EkBarkod
                        birimBarkod = STOK.SKART.S46_a_EK_brmbar
                        birimCarpan = STOK.SKART.S45_a_EK_brmcar
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S44_a_EK_birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = MIKTAR * birimfiyat
                        SQ = SQ & STOK_ID & "As a_stok_id, " & DEPOID & " As a_ambar_id , " & gelenmiktar & " As a_mik, " & birimfiyat & " As a_brmfiy, 0 As a_kdv, 0 As a_kdvtut, 0 As a_isk, 0 As a_isktut, " & tutartoplam & " As a_tutar, N'' AS a_ack, 0 AS a_bloke, 0 AS a_kesin, " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy"
                        SQL_KOS(B_SQL, SQ, False)

                    Case Else
                        BELGE(0) = New WS_UrunEkle
                        BELGE(0).DURUM = NUMARATOR.UYARI
                        BELGE(0).BELGETURU = " "
                        BELGE(0).BARKOD = " "
                        BELGE(0).AD = " "
                        BELGE(0).KOD = " "
                        BELGE(0).MIKTAR = 0
                        BELGE(0).MEVCUTMIKTAR = 0
                        Return BELGE
                End Select



                SQ = ""
                SQ = " INSERT INTO " & KUL.tper & "stkhdet (a_gc, a_tur, a_id, a_fattur, a_fatid, a_sira,a_sirano, a_tarih, a_cari_id, a_stok_id, a_ambar_id, a_mik, a_brmfiy, a_kdv, a_kdvtut, a_isk, a_isktut, a_tutar, a_ack, a_bloke, a_kesin, a_sbrmmik, a_sbrm, a_sbrmcrp, a_sbrmfiy)"
                SQ = SQ & " Select 'G' AS a_gc, 50 AS a_tur, " & BELGEID & " AS a_id, 50 AS a_fattur, " & BELGEID & " AS a_fatid, " & SIRA & " AS a_sira," & SIRA & " a_sirano, "
                SQ = SQ & " Convert(VARCHAR(10), GETDATE(), 110) As a_tarih, 0 As a_cari_id, "

                Select Case STOK.SKART.S99_Nerede_Buldun

                    Case STOK_BILGI.Bul_Tur.Stok_1BrmBarkod
                        birimBarkod = STOK.SKART.S26_a_1brmbar
                        birimCarpan = STOK.SKART.S25_a_1brmcar
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S24_a_1birim
                        gelenmiktar = birimCarpan * MIKTAR

                        SQ = SQ & STOK_ID & "As a_stok_id, " & DEPOID & " As a_ambar_id, " & gelenmiktar & " As a_mik, " & birimfiyat & " As a_brmfiy, 0 As a_kdv, 0 As a_kdvtut, 0 As a_isk, 0 As a_isktut, " & tutartoplam & " As a_tutar, N'' AS a_ack, 0 AS a_bloke, 0 AS a_kesin,  " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy"
                        SQL_KOS(B_SQL, SQ, False)
                    Case STOK_BILGI.Bul_Tur.Stok_2BrmBarkod
                        birimBarkod = STOK.SKART.S31_a_2brmbar
                        birimCarpan = STOK.SKART.S30_a_2brmcar
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S29_a_2birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = MIKTAR * birimfiyat
                        SQ = SQ & STOK_ID & "As a_stok_id, " & DEPOID & " As a_ambar_id, " & gelenmiktar & " As a_mik, " & birimfiyat & " As a_brmfiy, 0 As a_kdv, 0 As a_kdvtut, 0 As a_isk, 0 As a_isktut, " & tutartoplam & " As a_tutar, N'' AS a_ack, 0 AS a_bloke, 0 AS a_kesin,  " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy"
                        SQL_KOS(B_SQL, SQ, False)

                    Case STOK_BILGI.Bul_Tur.Stok_Barkod
                        birimBarkod = STOK.SKART.S11_barkod
                        birimCarpan = STOK.SKART.S98_Carpan
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S03_birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = MIKTAR * birimfiyat
                        SQ = SQ & STOK_ID & "As a_stok_id, " & DEPOID & " As a_ambar_id, " & gelenmiktar & " As a_mik, " & birimfiyat & " As a_brmfiy, 0 As a_kdv, 0 As a_kdvtut, 0 As a_isk, 0 As a_isktut, " & tutartoplam & " As a_tutar, N'' AS a_ack, 0 AS a_bloke, 0 AS a_kesin,  " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy"
                        SQL_KOS(B_SQL, SQ, False)
                    Case STOK_BILGI.Bul_Tur.Stok_Kodu
                        birimBarkod = STOK.SKART.S11_barkod
                        birimCarpan = STOK.SKART.S98_Carpan
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S03_birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = MIKTAR * birimfiyat
                        SQ = SQ & STOK_ID & "As a_stok_id, " & DEPOID & " As a_ambar_id, " & gelenmiktar & " As a_mik, " & birimfiyat & " As a_brmfiy, 0 As a_kdv, 0 As a_kdvtut, 0 As a_isk, 0 As a_isktut, " & tutartoplam & " As a_tutar, N'' AS a_ack, 0 AS a_bloke, 0 AS a_kesin,  " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy"
                        SQL_KOS(B_SQL, SQ, False)
                    Case STOK_BILGI.Bul_Tur.Stok_EkBarkod
                        birimBarkod = STOK.SKART.S46_a_EK_brmbar
                        birimCarpan = STOK.SKART.S45_a_EK_brmcar
                        birimfiyat = gecerliFiyat
                        birim = STOK.SKART.S44_a_EK_birim
                        gelenmiktar = birimCarpan * MIKTAR
                        tutartoplam = MIKTAR * birimfiyat
                        SQ = SQ & STOK_ID & "As a_stok_id, " & DEPOID & " As a_ambar_id, " & gelenmiktar & " As a_mik, " & birimfiyat & " As a_brmfiy, 0 As a_kdv, 0 As a_kdvtut, 0 As a_isk, 0 As a_isktut, " & tutartoplam & " As a_tutar, N'' AS a_ack, 0 AS a_bloke, 0 AS a_kesin,  " & MIKTAR & "  As a_sbrmmik, '" & birim & "' AS a_sbrm," & birimCarpan & "AS a_sbrmcrp,  " & birimfiyat & " AS a_sbrmfiy"
                        SQL_KOS(B_SQL, SQ, False)
                    Case Else
                        BELGE(0) = New WS_UrunEkle
                        BELGE(0).DURUM = NUMARATOR.UYARI
                        BELGE(0).BELGETURU = " "
                        BELGE(0).BARKOD = " "
                        BELGE(0).AD = " "
                        BELGE(0).KOD = " "
                        BELGE(0).MIKTAR = 0
                        BELGE(0).MEVCUTMIKTAR = 0
                        Return BELGE
                End Select

            End If

            If REFTIPI = "T" Then
                Dim stokrefId As Long
                SQ = ""
                SQ = "SELECT a_refstokid FROM " & KUL.tfrm & "stok  where a_id=" & STOKID & ""
                DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
                stokrefId = NULN(DT, 0, "a_refstokid")
                SQ = ""
                SQ = " UPDATE " & KUL.tfrm & "stokdeg SET a_cikan = a_cikan + " & gelenmiktar & ", a_kalan = a_giren - (" & gelenmiktar & " + a_cikan) "
                SQ = SQ & " WHERE a_ambid = " & DEPOID & " AND a_id =" & stokrefId
                SQL_KOS(B_SQL, SQ, False)
                SQ = ""
                SQ = " UPDATE " & KUL.tfrm & "stokdeg SET a_giren = a_giren + " & gelenmiktar & ", a_kalan = a_kalan + " & gelenmiktar & "  "
                SQ = SQ & " WHERE a_ambid = " & DEPOID & " AND a_id =" & stokrefId
                SQL_KOS(B_SQL, SQ, False)
            End If
            SQ = ""
            SQ = " UPDATE " & KUL.tfrm & "stokdeg SET a_cikan = a_cikan + " & gelenmiktar & ", a_kalan = a_giren - (" & gelenmiktar & " + a_cikan) "
            SQ = SQ & " WHERE a_ambid = " & DEPOID & " AND a_id =" & STOKID
            SQL_KOS(B_SQL, SQ, False)
            SQ = ""
            SQ = " UPDATE " & KUL.tfrm & "stokdeg SET a_giren = a_giren + " & gelenmiktar & ", a_kalan = a_kalan +" & gelenmiktar & "  "
            SQ = SQ & " WHERE a_ambid = " & OPLANID & " AND a_id =" & STOKID
            SQL_KOS(B_SQL, SQ, False)

            Dim TUTARTOPLAMI As Double
            SQ = ""
            SQ = "SELECT ISNULL(SUM(a_tutar),0) AS tutar FROM " & KUL.tper & "stkhdet WHERE a_tur = 49 AND a_id = " & BELGEID
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            TUTARTOPLAMI = NULD(DT, 0, "tutar")

            SQ = ""
            SQ = "UPDATE " & KUL.tper & "stkhmas SET a_tuttop = " & TUTARTOPLAMI & " WHERE a_gc = 'C' AND a_tur = 49 AND a_id = " & BELGEID
            SQL_KOS(B_SQL, SQ, False)

            'SQ = ""
            'SQ = "SELECT TOP 1 a_mik FROM " & KUL.tper & "stkhdet WHERE a_id=" & BELGEID & " AND a_tur=" & BELGETURU & "  order by a_sira desc"
            'DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            'sonMiktar = NULN(DT, 0, "a_mik")


            BELGE(0) = New WS_UrunEkle
            BELGE(0).DURUM = NUMARATOR.BAŞARILI
            BELGE(0).BELGETURU = BELGETURU
            BELGE(0).BARKOD = barkodd
            BELGE(0).AD = stokAdi
            BELGE(0).KOD = stokKodu
            BELGE(0).MIKTAR = gelenmiktar
            BELGE(0).MEVCUTMIKTAR = 0
            Return BELGE
        End If
        BELGE(0) = New WS_UrunEkle
        BELGE(0).DURUM = NUMARATOR.DEGERDONMEDI
        BELGE(0).BELGETURU = " "
        BELGE(0).BARKOD = " "
        BELGE(0).AD = " "
        BELGE(0).KOD = " "
        BELGE(0).MIKTAR = 0
        BELGE(0).MEVCUTMIKTAR = 0
        Return BELGE
    End Function
#End Region



#Region "DÖVİZLER"
    Public Class WS_DOVIZ
        Public DURUM As Long
        Public ID As Long
        Public KOD As String
        Public ADI As String
        Public ABIRIM As String
        Public BSAY As Long
        Public SIMGE As String
        Public KUR As Double

    End Class


    <WebMethod()>
    Public Function MB_DOVIZ(ByVal VERSION As String) As WS_DOVIZ()


        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"
            Dim VERI1(0) As WS_DOVIZ
            VERI1(0) = New WS_DOVIZ
            VERI1(0).DURUM = NUMARATOR.APKVERSIYONESKI
            VERI1(0).ID = 0
            VERI1(0).KOD = " "
            VERI1(0).ADI = " "
            VERI1(0).ABIRIM = " "
            VERI1(0).BSAY = 0
            VERI1(0).SIMGE = " "
            VERI1(0).KUR = 0.0

            Return VERI1

#End Region

        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

#Region "WS HATA"
            Dim VERI1(0) As WS_DOVIZ
            VERI1(0) = New WS_DOVIZ
            VERI1(0).DURUM = NUMARATOR.WSVERSIYONESKI
            VERI1(0).ID = 0
            VERI1(0).KOD = " "
            VERI1(0).ADI = " "
            VERI1(0).ABIRIM = " "
            VERI1(0).BSAY = 0
            VERI1(0).SIMGE = " "
            VERI1(0).KUR = 0.0

            Return VERI1

#End Region



        End If


        Dim SQ As String
        Dim DT As New DataTable

        B_SQL = connstr
        SQL_TUR = "2005"


        SQ = ""
        SQ = "SELECT DOVIZ.a_id,DOVIZ.a_kod,DOVIZ.a_adi,DOVIZ.a_abrm,DOVIZ.a_bsay,DOVIZ.a_simge,KUR.a_mbsatis "
        SQ = SQ & " FROM tbl_doviz AS DOVIZ "
        SQ = SQ & " INNER JOIN tbl_dovkur AS KUR ON DOVIZ.a_id=KUR.a_did"

        DT = SQL_TABLO(SQ, "T", False, False, False)
        If DT.Rows.Count > 0 Then
            Dim VERI1(DT.Rows.Count - 1) As WS_DOVIZ
            For X = 0 To DT.Rows.Count - 1

                VERI1(X) = New WS_DOVIZ
                VERI1(X).DURUM = NUMARATOR.BAŞARILI
                VERI1(X).ID = NULN(DT, X, "a_id")
                VERI1(X).KOD = NULA(DT, X, "a_kod")
                VERI1(X).ADI = NULA(DT, X, "a_adi")
                VERI1(X).ABIRIM = NULA(DT, X, "a_abrm")
                VERI1(X).BSAY = NULN(DT, X, "a_bsay")
                VERI1(X).SIMGE = NULA(DT, X, "a_simge")
                VERI1(X).KUR = NULD(DT, X, "a_mbsatis")


            Next

            Return VERI1
        Else
            Dim VERI1(0) As WS_DOVIZ
            VERI1(0) = New WS_DOVIZ
            VERI1(0).DURUM = NUMARATOR.HATA
            VERI1(0).ID = 0
            VERI1(0).KOD = " "
            VERI1(0).ADI = " "
            VERI1(0).ABIRIM = " "
            VERI1(0).BSAY = 0
            VERI1(0).SIMGE = " "
            VERI1(0).KUR = 0.0

            Return VERI1

        End If





    End Function


#End Region

#Region "MB/BELGE NUMARASI"
    Public Class WS_BELGENUMARASI
        Public DURUM As Long
        Public BELGENO As String
    End Class


    <WebMethod()>
    Public Function MB_BelgeNo(ByVal VERSION As String, ByVal TUR As Long, ByVal USERID As Long) As WS_BELGENUMARASI()


        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"
            Dim VERI1(0) As WS_BELGENUMARASI
            VERI1(0) = New WS_BELGENUMARASI
            VERI1(0).DURUM = NUMARATOR.APKVERSIYONESKI
            VERI1(0).BELGENO = " "


            Return VERI1

#End Region

        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

#Region "WS HATA"
            Dim VERI1(0) As WS_BELGENUMARASI
            VERI1(0) = New WS_BELGENUMARASI
            VERI1(0).DURUM = NUMARATOR.WSVERSIYONESKI
            VERI1(0).BELGENO = " "

            Return VERI1

#End Region



        End If



        B_SQL = connstr
        Dim SQ As String
        Dim DT As New DataTable
        Dim DT2 As New DataTable
        SQL_TUR = "2005"
        Dim BELGENO As String = " "
        Dim BAGKUL As String
        Dim BAGKID As String



        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()

        If TUR = 49 Or TUR = 4 Or TUR = 34 Then

            Return MB_BelgenoUret(TUR, USERID)

        Else

            BAGKUL = VT_BILGI("tbl_user", "a_id", "a_bbkod", USERID)
            If BAGKUL = "" Then

                Return MB_BelgenoUret(TUR, USERID)

            Else

                BAGKID = VT_BILGI("tbl_user", "a_kod", "a_id", BAGKUL)

                If BAGKID = "" Then

                    Return MB_BelgenoUret(TUR, USERID)

                Else

                    SQ = ""
                    SQ = SQ & " Select KUL.a_sube_id as SUBEID, KUL.a_firma_id AS FIRMAID  "
                    SQ = SQ & " FROM  tbl_user as KUL "
                    SQ = SQ & " WHERE KUL.a_id =  " & BAGKID
                    DT = SQL_TABLO(SQ, "T", False, False, False)

                    KUL.FIRMA = NULN(DT, 0, "FIRMAID")
                    KUL.ID_SUBE = NULN(DT, 0, "SUBEID")
                    KUL.PRM_belgeuser = BAGKID

                    BELGENO = X_Belge_No(TUR)

                    If BELGENO = "" Then
                        Dim VERI(0) As WS_BELGENUMARASI
                        VERI(0) = New WS_BELGENUMARASI
                        VERI(0).DURUM = NUMARATOR.HATA
                        VERI(0).BELGENO = " "
                        Return VERI
                    Else

                        Dim VERI(0) As WS_BELGENUMARASI
                        VERI(0) = New WS_BELGENUMARASI
                        VERI(0).DURUM = NUMARATOR.BAŞARILI
                        VERI(0).BELGENO = BELGENO
                        Return VERI

                    End If

                End If

            End If

        End If


    End Function


#End Region









#Region "MB/Belge No"
    Public Function MB_BelgenoKontrol(ByVal belgeno As String, ByVal tur As Long) As WS_UrunEkle()
        Dim SQ As String
        Dim DT As New DataTable
        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        If tur = 1 Then
            SQ = ""
            SQ = "SELECT a_bno FROM " & KUL.tfrm & "stkhmas where a_ "
        End If


    End Function
#End Region
#Region "MB/BelgeUrunGetir"
    Public Class BelgeUrunGetir
        Public DURUM As Long
        Public STOKID As Long
        Public URUNTUTARI As Double
        Public SIRA As Long
    End Class 'LONG v DOUBLE TEMİZ
    <WebMethod()>
    Public Function MB_BelgeUrunGetir(ByVal VERSION As String, ByVal TUR As Long, ByVal ID As Long) As BelgeUrunGetir()
        Dim SQ As String
        Dim DT As New DataTable
        Dim TABLO As String
        Dim TABLO1 As String


        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"

            Dim VERI(0) As BelgeUrunGetir
            VERI(0) = New BelgeUrunGetir
            VERI(0).DURUM = NUMARATOR.APKVERSIYONESKI
            VERI(0).STOKID = 0
            VERI(0).URUNTUTARI = 0
            VERI(0).SIRA = 0
#End Region

        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski


#Region "WS HATA"

            Dim VERI(0) As BelgeUrunGetir
            VERI(0) = New BelgeUrunGetir
            VERI(0).DURUM = NUMARATOR.WSVERSIYONESKI
            VERI(0).STOKID = 0
            VERI(0).URUNTUTARI = 0
            VERI(0).SIRA = 0

#End Region



        End If

        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        Select Case TUR
            Case 10, 11, 12, 13, 14, 16, 70, 75, 40, 41, 42, 95, 96
                TABLO = "stkcdet"
                TABLO1 = "stkcmas"
            Case 17, 18, 19, 20, 21, 22, 54, 71, 87, 88, 43, 44, 69
                TABLO = "stkgdet"
                TABLO1 = "stkgmas"

            Case Else
                Dim VERI(0) As BelgeUrunGetir
                VERI(0) = New BelgeUrunGetir
                VERI(0).DURUM = NUMARATOR.HATA
                VERI(0).STOKID = 0
                VERI(0).URUNTUTARI = 0
                VERI(0).SIRA = 0


        End Select
        If TUR <> 49 And TUR <> 4 And TUR <> 33 And TUR <> 34 Then

            SQ = "SELECT  STK.a_id AS STOKID ,stk.a_adi AS AD,STK.a_kod AS KOD,STK.a_barkod AS BARKOD,STHD.a_sira as SIRA,STHD.a_brmfiy as FIYAT FROM " & KUL.tfrm & "stok as STK INNER JOIN " & KUL.tper & TABLO & " as STHD "
            SQ = SQ & " ON STK.a_id=STHD.a_stok_id "
            SQ = SQ & " where STHD.a_tur=" & TUR & " and STHD.a_id=" & ID
            SQ = SQ & " ORDER BY a_sira DESC"
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)

            If DT.Rows.Count > 0 Then
                Dim VERI(DT.Rows.Count - 1) As BelgeUrunGetir
                For X = 0 To DT.Rows.Count - 1
                    VERI(X) = New BelgeUrunGetir
                    VERI(X).DURUM = NUMARATOR.BAŞARILI
                    VERI(X).STOKID = NULN(DT, X, "STOKID")
                    VERI(X).URUNTUTARI = NULD(DT, X, "FIYAT")
                    VERI(X).SIRA = NULN(DT, X, "SIRA")

                Next
                Return VERI
            Else
                Dim VERI(0) As BelgeUrunGetir
                VERI(0) = New BelgeUrunGetir
                VERI(0).DURUM = NUMARATOR.HATA
                VERI(0).STOKID = 0
                VERI(0).URUNTUTARI = 0
                VERI(0).SIRA = 0
                Return VERI
            End If
        End If



        If TUR = 49 Then
            'BELGENIN İÇİNDEKİ URUNLERIN DETAYLARI BULUNUYOR
            SQ = ""
            SQ = "Select STK.a_id As STOKID,STKH.a_sira As SIRA,STK.a_kod As KOD,STK.a_barkod As BARKOD,STK.a_adi As AD,STKH.a_mik As MIKTAR,STKH.a_brmfiy as FIYAT "
            SQ = SQ & " FROM " & KUL.tper & "stkhdet As STKH INNER JOIN " & KUL.tfrm & "stok As STK On STKH.a_stok_id=STK.a_id "
            SQ = SQ & " WHERE STKH.a_id=" & ID & " And STKH.a_tur=" & TUR
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            'URUN MIKTARI BULUNUYOR

            If DT.Rows.Count > 0 Then
                Dim VERI(DT.Rows.Count - 1) As BelgeUrunGetir
                For X = 0 To DT.Rows.Count - 1
                    VERI(X) = New BelgeUrunGetir
                    VERI(X).DURUM = NUMARATOR.BAŞARILI
                    VERI(X).STOKID = NULN(DT, X, "STOKID")
                    VERI(X).URUNTUTARI = NULD(DT, X, "FIYAT")
                    VERI(X).SIRA = NULN(DT, X, "SIRA")

                Next
                Return VERI
            Else
                Dim VERI(0) As BelgeUrunGetir
                VERI(0) = New BelgeUrunGetir
                VERI(0).DURUM = NUMARATOR.HATA
                VERI(0).STOKID = 0
                VERI(0).URUNTUTARI = 0
                VERI(0).SIRA = 0

                Return VERI
            End If
        End If
        If TUR = 4 Then
            'BELGENIN İÇİNDEKİ URUNLERIN DETAYLARI BULUNUYOR
            SQ = ""
            SQ = " Select STK.a_id As STOKID,STS.a_sira As SIRA,STK.a_kod As KOD,STK.a_barkod As BARKOD,STK.a_adi As AD,STS.a_sayilan As MIKTAR,STS.a_fiyat AS FIYAT "
            SQ = SQ & " FROM " & KUL.tper & "stsaydet As STS INNER JOIN " & KUL.tfrm & "stok As STK On STS.a_stok_id=STK.a_id "
            SQ = SQ & " WHERE STS.a_id=" & ID & "order by  STS.a_sira DESC"
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)

            If DT.Rows.Count > 0 Then
                Dim VERI(DT.Rows.Count - 1) As BelgeUrunGetir
                For X = 0 To DT.Rows.Count - 1
                    VERI(X) = New BelgeUrunGetir
                    VERI(X).DURUM = NUMARATOR.BAŞARILI
                    VERI(X).STOKID = NULN(DT, X, "STOKID")
                    VERI(X).URUNTUTARI = NULD(DT, X, "FIYAT")
                    VERI(X).SIRA = NULN(DT, X, "SIRA")

                Next
                Return VERI
            Else
                Dim VERI(0) As BelgeUrunGetir
                VERI(0) = New BelgeUrunGetir
                VERI(0).DURUM = NUMARATOR.HATA
                VERI(0).STOKID = 0
                VERI(0).URUNTUTARI = 0
                VERI(0).SIRA = 0

                Return VERI
            End If
        End If
        If TUR = 33 Or 34 Then

            SQ = "SELECT  STK.a_id as STOKID,tk.a_adi AS AD,STK.a_kod AS KOD,STK.a_barkod AS BARKOD,SIP.a_mik AS MIKTAR,SIP.a_sira as SIRA,SIP.a_brmfiy as FIYAT FROM " & KUL.tfrm & "stok as STK INNER JOIN " & KUL.tper & "sipdet as SIP "
            SQ = SQ & " ON STK.a_id=SIP.a_stok_id "
            SQ = SQ & " where SIP.a_tur=" & TUR & " and SIP.a_id=" & ID
            SQ = SQ & " ORDER BY a_sira DESC"
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)

            If DT.Rows.Count > 0 Then
                Dim VERI(DT.Rows.Count - 1) As BelgeUrunGetir
                For X = 0 To DT.Rows.Count - 1
                    VERI(X) = New BelgeUrunGetir
                    VERI(X).DURUM = NUMARATOR.BAŞARILI
                    VERI(X).STOKID = NULN(DT, X, "STOKID")
                    VERI(X).URUNTUTARI = NULD(DT, X, "FIYAT")
                    VERI(X).SIRA = NULN(DT, X, "SIRA")

                Next
                Return VERI
            Else
                Dim VERI(0) As BelgeUrunGetir
                VERI(0) = New BelgeUrunGetir
                VERI(0).DURUM = NUMARATOR.HATA
                VERI(0).STOKID = 0
                VERI(0).URUNTUTARI = 0
                VERI(0).SIRA = 0
                Return VERI
            End If

        End If

        Dim DEGER(0) As BelgeUrunGetir
        DEGER(0) = New BelgeUrunGetir
        DEGER(0).DURUM = NUMARATOR.DEGERDONMEDI
        DEGER(0).STOKID = 0
        DEGER(0).URUNTUTARI = 0
        DEGER(0).SIRA = 0

        Return DEGER

    End Function
#End Region

#Region "MB/Tablo Oluştur"
    Public Class Tablo_Olustur
        Public DURUM As Long
        Public MESAJ As String
    End Class
    <WebMethod()>
    Public Function MB_Tablo_Olustur() As Tablo_Olustur()
        Dim SQ As String
        Dim SONUC As Boolean
        Dim SONUC2 As Boolean
        Dim SONUC3 As Boolean
        Dim TEXT As String = ""
        Dim SONUC4 As Boolean
        Dim FIRMAID As Long
        Dim SONUC5 As Boolean
        Dim SONUC6 As Boolean
        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        SQ = ""
        SQ = "CREATE Table [dbo].[tbl_mob_kprm]("
        SQ = SQ & "[a_uid] [Int]  Not NULL,"
        SQ = SQ & "[a_nesne] [nvarchar](50) Not NULL,"
        SQ = SQ & "[a_deger] [nvarchar](50) NULL,"
        SQ = SQ & "Constraint [PK_tbl_mob_kprm] PRIMARY KEY CLUSTERED "
        SQ = SQ & "("
        SQ = SQ & "[a_uid] Asc,"
        SQ = SQ & "[a_nesne] ASC"
        SQ = SQ & ")WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]"
        SQ = SQ & ") ON [PRIMARY]"
        SONUC = SQL_KOS(SQ, False)

        SQ = ""
        SQ = "CREATE Table [dbo].[tbl_mob_mprm]("
        SQ = SQ & "[a_fid] [Int] Not NULL,"
        SQ = SQ & "[a_uid] [Int] Not NULL,"
        SQ = SQ & "[a_mid] [Int] Not NULL,"
        SQ = SQ & "[a_nesne] [nvarchar](50) Not NULL, "
        SQ = SQ & "[a_deger] [nvarchar](50) NULL, "
        SQ = SQ & "[a_durum] [Int] NULL, "
        SQ = SQ & "Constraint [PK_tbl_mob_mprm] PRIMARY KEY CLUSTERED"
        SQ = SQ & "("
        SQ = SQ & "[a_fid] ASC, "
        SQ = SQ & "[a_uid] ASC,"
        SQ = SQ & "[a_mid] ASC,"
        SQ = SQ & "[a_nesne] ASC"
        SQ = SQ & ")WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]"
        SQ = SQ & ") ON [PRIMARY]"
        SONUC2 = SQL_KOS(SQ, False)

        If SONUC2 = True Then
            Dim SORGU As String
            FIRMAID = FirmaIdGetir(-1)
            SORGU = "insert into tbl_mob_mprm(a_fid,a_uid,a_mid,a_nesne,a_deger,a_durum) VALUES "
            SORGU += " (" & FIRMAID & ",-1, 1,'mikkontrol', 'Kapalı' , 0), (" & FIRMAID & ",-1, 1,'fiyatod', 'Son Maliyet' , 0), (" & FIRMAID & ",-1, 2,'fiyatod', 'Son Maliyet' , 0), (" & FIRMAID & ",-1, 3,'faturaod', 'Son Maliyet' , 0), (" & FIRMAID & ",-1, 3,'irsaliyeod', 'Son Maliyet' , 0), (" & FIRMAID & ",-1, 3,'fisod', 'Son Maliyet' , 0), "
            SORGU += " (" & FIRMAID & ",-1, 4,'negatifsat', 'Kapalı' , 0), (" & FIRMAID & ",-1, 4,'faturaod', 'Son Maliyet' , 0), (" & FIRMAID & ",-1, 4,'irsaliyeod', 'Son Maliyet' , 0), (" & FIRMAID & ",-1, 4,'fisod', 'Son Maliyet' , 0), (" & FIRMAID & ",-1, 5,'varsaybrm', 'ADET' , 0), "
            SORGU += " (" & FIRMAID & ",-1, 5,'kdv', 'Dahil' , 0), (" & FIRMAID & ",-1, 5,'akdvorn', '8' , 0), (" & FIRMAID & ",-1, 5,'skdvorn', '8' , 0), (" & FIRMAID & ",-1, 6,'mikkontrol', 'Kapalı' , 0) , (" & FIRMAID & ",-1, 6,'fiyatod', 'Son Maliyet' , 0)  "
            SONUC5 = SQL_KOS(SORGU, False)

        End If
        SQ = ""
        SQ = "CREATE Table [dbo].[tbl_mob_menu]("
        SQ = SQ & "[a_id] [Int] Not NULL,"
        SQ = SQ & "[a_adi] [nvarchar](50) NULL,"
        SQ = SQ & "[a_ust] [Int] NULL,"
        SQ = SQ & "[a_sirano] [Int] NULL,"
        SQ = SQ & "Constraint [PK_tbl_mob_menu] PRIMARY KEY CLUSTERED "
        SQ = SQ & "("
        SQ = SQ & "[a_id] Asc"
        SQ = SQ & ")WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]"
        SQ = SQ & ") ON [PRIMARY]"
        SONUC3 = SQL_KOS(SQ, False)

        If SONUC3 = True Then
            Dim sorgu As String = "INSERT INTO tbl_mob_menu(a_id,a_adi) VALUES "
            sorgu += " (1,'Depo Transfer'),(2,'Sayım'),(3,'Mal Kabul'),(4,'Satış'),(5,'Stok İşlemleri'),(6,'Sipariş')"
            SONUC4 = SQL_KOS(sorgu, False)
        End If


        SQ = ""
        SQ = "CREATE Table [dbo].[tbl_mob_hata]("
        SQ = SQ & "[a_id] [int] IDENTITY(1,1) NOT NULL,"
        SQ = SQ & "[a_uid] [int] NULL,"
        SQ = SQ & "[a_subeid] [int] NULL,"
        SQ = SQ & "[a_sinif] [nvarchar](50) NULL,"
        SQ = SQ & "[a_fonksiyon] [nvarchar](50) NULL,"
        SQ = SQ & "[a_hatametni] [nvarchar](250) NULL,"
        SQ = SQ & "[a_arguments] [nvarchar](max) NULL,"
        SQ = SQ & "[a_cdate] [smalldatetime] NULL,"
        SQ = SQ & ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]"
        SONUC6 = SQL_KOS(SQ, False)


        If SONUC = True Then
            TEXT = TEXT & "tbl_mob_kprm tablosu oluşturuldu." & vbCrLf
        End If
        If SONUC2 = True Then
            TEXT = TEXT & " \n tbl_mob_mprm tablosu oluşturuldu." & vbCrLf
        End If
        If SONUC3 = True Then
            TEXT = TEXT & " \n tbl_mob_menu tablosu oluşturuldu." & vbCrLf
        End If
        If SONUC4 = True Then
            TEXT = TEXT & "\n tbl_mob_menu tablosuna kayıtlar eklendi." & vbCrLf
        End If
        If SONUC5 = True Then
            TEXT = TEXT & "\n tbl_mob_mprm tablosuna kayıtlar eklendi." & vbCrLf
        End If
        If SONUC6 = True Then
            TEXT = TEXT & "\n tbl_mob_hata tablosu oluşturuldu." & vbCrLf
        End If

        Dim VERI(0) As Tablo_Olustur
        If SONUC = False And SONUC2 = False And SONUC3 = False And SONUC6 = False Then
            VERI(0) = New Tablo_Olustur
            VERI(0).DURUM = NUMARATOR.HATA
            VERI(0).MESAJ = " "
            Return VERI
        End If
        VERI(0) = New Tablo_Olustur
        VERI(0).DURUM = NUMARATOR.BAŞARILI
        VERI(0).MESAJ = TEXT
        Return VERI

    End Function
#End Region
#Region "MB/İşlem tbl_mob_Mprm"
    Public Class Islem_Mprm
        Public DURUM As Long
        Public MID As Long
        Public NESNE As String
        Public ADURUM As Long
        Public DEGER As String
    End Class
    <WebMethod()>
    Public Function MB_Islem_Mprm(ByVal VERSION As String, ByVal a_uid As Long) As Islem_Mprm()   'ByVal islem As Long, ByVal a_uid As Long, ByVal a_mid As String, ByVal a_nesne As String, ByVal a_deger As String, ByVal a_durum As String
        Dim SQ As String
        Dim TEXT As String = ""
        Dim KOSULUSER As String = " "
        Dim KOSULMENU As String = " "
        Dim KOSULNESNE As String = " "
        Dim FIRMAID As Long
        Dim DT As New DataTable
        Dim MENUID As Long
        Dim DURUM As Long
        Dim SONUC As Boolean
        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()


        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"
            Dim VERI(0) As Islem_Mprm
            VERI(0) = New Islem_Mprm
            VERI(0).DURUM = NUMARATOR.APKVERSIYONESKI
            VERI(0).MID = 0
            VERI(0).NESNE = " "
            VERI(0).DEGER = " "
            VERI(0).ADURUM = 0
            Return VERI
#End Region

        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

#Region "WS HATA"
            Dim VERI(0) As Islem_Mprm
            VERI(0) = New Islem_Mprm
            VERI(0).DURUM = NUMARATOR.WSVERSIYONESKI
            VERI(0).MID = 0
            VERI(0).NESNE = " "
            VERI(0).DEGER = " "
            VERI(0).ADURUM = 0
            Return VERI

#End Region



        End If



        FIRMAID = FirmaIdGetir(a_uid)

        SQ = ""
        SQ = " SELECT a_uid,a_mid,a_nesne,a_deger,a_durum FROM tbl_mob_mprm  "
        SQ = SQ & " WHERE a_fid=" & FIRMAID & " and a_uid= " & a_uid
        DT = SQL_TABLO(SQ, "T", False, False, False)

        If DT.Rows.Count > 0 Then
            Dim VERI(DT.Rows.Count - 1) As Islem_Mprm
            For x = 0 To DT.Rows.Count - 1
                VERI(x) = New Islem_Mprm
                VERI(x).DURUM = NUMARATOR.BAŞARILI
                VERI(x).MID = NULN(DT, x, "a_mid")
                VERI(x).NESNE = NULA(DT, x, "a_nesne")
                VERI(x).DEGER = NULA(DT, x, "a_deger")
                VERI(x).ADURUM = NULN(DT, x, "a_durum")
            Next
            Return VERI
        Else

            Dim SORGU As String
            FIRMAID = FirmaIdGetir(a_uid)
            SORGU = "insert into tbl_mob_mprm(a_fid,a_uid,a_mid,a_nesne,a_deger,a_durum) VALUES "
            SORGU += " (" & FIRMAID & "," & a_uid & ", 1,'mikkontrol', 'Kapalı' , 0), (" & FIRMAID & "," & a_uid & ", 1,'fiyatod', 'Son Maliyet' , 0), (" & FIRMAID & "," & a_uid & ", 2,'fiyatod', 'Son Maliyet' , 0), (" & FIRMAID & "," & a_uid & ", 3,'faturaod', 'Son Maliyet' , 0), (" & FIRMAID & "," & a_uid & ", 3,'irsaliyeod', 'Son Maliyet' , 0), "
            SORGU += " (" & FIRMAID & "," & a_uid & ", 3,'fisod', 'Son Maliyet' , 0), (" & FIRMAID & "," & a_uid & ", 4,'negatifsat', 'Kapalı' , 0), (" & FIRMAID & "," & a_uid & ", 4,'faturaod', 'Son Maliyet' , 0), (" & FIRMAID & "," & a_uid & ", 4,'irsaliyeod', 'Son Maliyet' , 0), (" & FIRMAID & "," & a_uid & ", 4,'fisod', 'Son Maliyet' , 0), "
            SORGU += " (" & FIRMAID & "," & a_uid & ", 5,'varsaybrm', 'ADET' , 0), (" & FIRMAID & "," & a_uid & ", 5,'kdv', 'Dahil' , 0), (" & FIRMAID & "," & a_uid & ", 5,'akdvorn', '8' , 0), (" & FIRMAID & "," & a_uid & ", 5,'skdvorn', '8' , 0), (" & FIRMAID & "," & a_uid & ", 6,'mikkontrol', 'Kapalı' , 0) "
            SONUC = SQL_KOS(SORGU, False)

            If SONUC = True Then
                SQ = ""
                SQ = " SELECT a_uid,a_mid,a_nesne,a_deger,a_durum FROM tbl_mob_mprm  "
                SQ = SQ & " WHERE a_fid=" & FIRMAID & " and a_uid= " & a_uid
                DT = SQL_TABLO(SQ, "T", False, False, False)

                Dim DEGER(DT.Rows.Count - 1) As Islem_Mprm
                For x = 0 To DT.Rows.Count - 1
                    DEGER(x) = New Islem_Mprm
                    DEGER(x).DURUM = NUMARATOR.UYARI
                    DEGER(x).MID = NULN(DT, x, "a_mid")
                    DEGER(x).NESNE = NULA(DT, x, "a_nesne")
                    DEGER(x).DEGER = NULA(DT, x, "a_deger")
                    DEGER(x).ADURUM = NULN(DT, x, "a_durum")
                Next
                Return DEGER

            End If
            Dim VERI(0) As Islem_Mprm
            VERI(0) = New Islem_Mprm
            VERI(0).DURUM = NUMARATOR.HATA
            VERI(0).MID = 0
            VERI(0).NESNE = " "
            VERI(0).DEGER = " "
            VERI(0).ADURUM = 0
            Return VERI

        End If




        '        ElseIf islem = 2 Then
        '            If a_uid <> 0 Then
        '                KOSULUSER = " AND a_uid=" & a_uid & " "
        '            End If
        '            If a_mid <> " " Then
        '                If Long.TryParse(a_mid, MENUID) = False Then
        '                    GoTo A
        '                End If
        '                KOSULMENU = " AND a_mid=" & MENUID & " "
        '            End If
        '            If a_nesne <> " " Then
        '                KOSULNESNE = " AND a_nesne='" & a_nesne & "' "
        '            End If

        '            SQ = ""
        '            SQ = " SELECT a_uid,a_mid,a_nesne,a_deger,a_durum FROM tbl_mob_mprm  "
        '            SQ = SQ & " WHERE a_fid=" & FIRMAID & " " & KOSULUSER & " " & KOSULNESNE & " " & KOSULMENU & "  "
        '            DT = SQL_TABLO(SQ, "T", False, False, False)
        '            If DT.Rows.Count > 0 Then
        '                Dim VERI(DT.Rows.Count - 1) As Islem_Mprm
        '                For x = 0 To DT.Rows.Count - 1

        '                    VERI(0) = New Islem_Mprm
        '                    VERI(0).DURUM = NUMARATOR.BAŞARILI
        '                    VERI(x).USERID = NULN(DT, x, "a_uid")
        '                    VERI(x).MID = NULN(DT, x, "a_mid")
        '                    VERI(x).NESNE = NULA(DT, x, "a_nesne")
        '                    VERI(x).DEGER = NULA(DT, x, "a_deger")
        '                    VERI(x).ADURUM = NULN(DT, x, "a_durum")
        '                Next
        '                Return VERI
        '            Else
        '                Dim VERI(0) As Islem_Mprm
        '                VERI(0) = New Islem_Mprm
        '                VERI(0).DURUM = NUMARATOR.HATA
        '                Return VERI
        '            End If

        '        If islem = 1 Then
        '            If a_uid <> 0 Then
        '                KOSULUSER = " AND a_uid=" & a_uid & " "
        '            End If
        '            If a_mid <> " " Then
        '                'longa çevir,çevrilmezse A'ya git
        '                If Long.TryParse(a_mid, MENUID) = False Then
        '                    GoTo A
        '                End If
        '                KOSULMENU = " AND a_mid=" & MENUID & " "
        '            End If
        '            If a_nesne <> " " Then
        '                KOSULNESNE = " AND a_nesne='" & a_nesne & "' "
        '            End If
        '            If Long.TryParse(a_durum, DURUM) = False Then
        '                GoTo A
        '            End If

        '            SQ = ""
        '            SQ = " UPDATE tbl_mob_mprm SET a_deger='" & a_deger & "' , a_durum=" & DURUM & " "
        '            SQ = SQ & " WHERE a_fid=" & FIRMAID & " " & KOSULUSER & " " & KOSULNESNE & " " & KOSULMENU & " "
        '            If SQL_KOS(SQ, False) = True Then
        '                Dim VERI(0) As Islem_Mprm
        '                VERI(0) = New Islem_Mprm
        '                VERI(0).DURUM = NUMARATOR.BAŞARILI

        '                VERI(0).USERID = a_uid
        '                VERI(0).MID = MENUID
        '                VERI(0).NESNE = a_nesne
        '                VERI(0).DEGER = a_deger
        '                VERI(0).ADURUM = DURUM
        '                Return VERI
        '            Else
        '                Dim VERI(0) As Islem_Mprm
        '                VERI(0) = New Islem_Mprm
        '                VERI(0).DURUM = NUMARATOR.HATA

        '                VERI(0).USERID = 0
        '                VERI(0).MID = 0
        '                VERI(0).NESNE = " "
        '                VERI(0).DEGER = " "
        '                VERI(0).ADURUM = 0
        '                Return VERI
        '            End If
        '        ElseIf islem = 2 Then
        '            If a_uid <> 0 Then
        '                KOSULUSER = " AND a_uid=" & a_uid & " "
        '            End If
        '            If a_mid <> " " Then
        '                If Long.TryParse(a_mid, MENUID) = False Then
        '                    GoTo A
        '                End If
        '                KOSULMENU = " AND a_mid=" & MENUID & " "
        '            End If
        '            If a_nesne <> " " Then
        '                KOSULNESNE = " AND a_nesne='" & a_nesne & "' "
        '            End If

        '            SQ = ""
        '            SQ = " SELECT a_uid,a_mid,a_nesne,a_deger,a_durum FROM tbl_mob_mprm  "
        '            SQ = SQ & " WHERE a_fid=" & FIRMAID & " " & KOSULUSER & " " & KOSULNESNE & " " & KOSULMENU & "  "
        '            DT = SQL_TABLO(SQ, "T", False, False, False)
        '            If DT.Rows.Count > 0 Then
        '                Dim VERI(DT.Rows.Count - 1) As Islem_Mprm
        '                For x = 0 To DT.Rows.Count - 1

        '                    VERI(0) = New Islem_Mprm
        '                    VERI(0).DURUM = NUMARATOR.BAŞARILI
        '                    VERI(x).USERID = NULN(DT, x, "a_uid")
        '                    VERI(x).MID = NULN(DT, x, "a_mid")
        '                    VERI(x).NESNE = NULA(DT, x, "a_nesne")
        '                    VERI(x).DEGER = NULA(DT, x, "a_deger")
        '                    VERI(x).ADURUM = NULN(DT, x, "a_durum")
        '                Next
        '                Return VERI
        '            Else
        '                Dim VERI(0) As Islem_Mprm
        '                VERI(0) = New Islem_Mprm
        '                VERI(0).DURUM = NUMARATOR.HATA
        '                Return VERI
        '            End If
        '        ElseIf islem = 3 Then
        '            If a_uid <> 0 Then
        '                KOSULUSER = " AND a_uid=" & a_uid & " "
        '            End If
        '            If a_mid <> " " Then
        '                'longa çevir,çevrilmezse A'ya git
        '                If Long.TryParse(a_mid, MENUID) = False Then
        '                    GoTo A
        '                End If
        '            End If
        '            If Long.TryParse(a_durum, DURUM) = False Then
        '                GoTo A
        '            End If

        '            SQ = ""
        '            SQ = "INSERT INTO tbl_mob_mprm "
        '            SQ = SQ & " (a_fid,a_uid,a_mid,a_nesne,a_deger,a_durum) "
        '            SQ = SQ & " VALUES ("
        '            SQ = SQ & " " & FIRMAID & ","
        '            SQ = SQ & " " & a_uid & ","
        '            SQ = SQ & " " & MENUID & ","
        '            SQ = SQ & " N'" & a_nesne & "',"
        '            SQ = SQ & " N'" & a_deger & "',"
        '            SQ = SQ & " " & DURUM & " "
        '            SQ = SQ & " )"
        '            If SQL_KOS(SQ, False) = True Then
        '                Dim VERI(0) As Islem_Mprm
        '                VERI(0) = New Islem_Mprm
        '                VERI(0).DURUM = NUMARATOR.BAŞARILI
        '                VERI(0).USERID = a_uid
        '                VERI(0).MID = MENUID
        '                VERI(0).NESNE = a_nesne
        '                VERI(0).DEGER = a_deger
        '                VERI(0).ADURUM = DURUM
        '                Return VERI
        '            Else
        '                Dim VERI(0) As Islem_Mprm
        '                VERI(0) = New Islem_Mprm
        '                VERI(0).DURUM = NUMARATOR.HATA
        '                VERI(0).USERID = 0
        '                VERI(0).MID = 0
        '                VERI(0).NESNE = " "
        '                VERI(0).DEGER = " "
        '                VERI(0).ADURUM = 0
        '                Return VERI
        '            End If
        '        ElseIf islem = 4 Then
        '            If a_uid <> 0 Then
        '                KOSULUSER = " AND a_uid=" & a_uid & " "
        '            End If
        '            If a_mid <> " " Then
        '                'longa çevir,çevrilmezse A'ya git
        '                If Long.TryParse(a_mid, MENUID) = False Then
        '                    GoTo A
        '                End If
        '                KOSULMENU = " AND a_mid=" & MENUID & " "
        '            End If
        '            If a_nesne <> " " Then
        '                KOSULNESNE = " AND a_nesne='" & a_nesne & "' "
        '            End If
        '            If Long.TryParse(a_durum, DURUM) = False Then
        '                GoTo A
        '            End If
        '            SQ = ""
        '            SQ = "DELETE FROM tbl_mob_mprm "
        '            SQ = SQ & " WHERE  WHERE a_fid=" & FIRMAID & " " & KOSULUSER & " " & KOSULNESNE & " " & KOSULMENU & " "
        '            If SQL_KOS(SQ, False) = True Then
        '                Dim VERI(0) As Islem_Mprm
        '                VERI(0) = New Islem_Mprm
        '                VERI(0).DURUM = NUMARATOR.BAŞARILI
        '                VERI(0).USERID = a_uid
        '                VERI(0).MID = MENUID
        '                VERI(0).NESNE = a_nesne
        '                VERI(0).DEGER = a_deger
        '                VERI(0).ADURUM = DURUM
        '                Return VERI
        '            Else
        '                Dim VERI(0) As Islem_Mprm
        '                VERI(0) = New Islem_Mprm
        '                VERI(0).DURUM = NUMARATOR.HATA
        '                VERI(0).USERID = 0
        '                VERI(0).MID = 0
        '                VERI(0).NESNE = " "
        '                VERI(0).DEGER = " "
        '                VERI(0).ADURUM = 0


        '                Return VERI
        '            End If
        '        Else
        'A:
        '            Dim VERI(0) As Islem_Mprm
        '            VERI(0) = New Islem_Mprm
        '            VERI(0).DURUM = NUMARATOR.HATA
        '            VERI(0).USERID = 0
        '            VERI(0).MID = 0
        '            VERI(0).NESNE = " "
        '            VERI(0).DEGER = " "
        '            VERI(0).ADURUM = 0

        '            Return VERI
        '        End If


    End Function
#End Region
#Region "MB/İşlem tbl_mob_Kprm"
    Public Class Islem_kprm
        Public DURUM As Long
        Public USERID As Long
        Public NESNE As String
        Public DEGER As String

    End Class

    Public Class Gelen_Veri
        Public VERI As Long
        Public NESNE As String
        Public DEGER As String

        Public Function ekle(ByVal gelenNesne As String, ByVal gelenDeger As String)
            NESNE = gelenNesne
            DEGER = gelenDeger
        End Function

    End Class
    <WebMethod()>
    Public Function MB_Islem_Kprm(ByVal VERSION As String, ByVal islem As Long, ByVal a_uid As Long, ByVal a_nesne As String, ByVal a_deger As String, ByVal a_veri As String) As Islem_kprm()
        Dim SQ As String
        Dim TEXT As String = ""
        Dim KOSULVALUE As String = " "
        Dim KOSULNESNE As String = "AND a_nesne IN("
        Dim DT As New DataTable
        Dim x As Long

        Dim gelenVeri As New Gelen_Veri
        Dim gelenVeriList = New List(Of Gelen_Veri)


        Dim bol_veri_string As String
        Dim bolunen_veri As New ArrayList
        Dim bolunen_veri_satir As String = ""
        Dim A(2) As String

        '   Dim Nesneler As New ArrayList
        Dim k As Long
        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()

        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"

            Dim VERI(0) As Islem_kprm
            VERI(0) = New Islem_kprm
            VERI(0).DURUM = NUMARATOR.APKVERSIYONESKI
            VERI(0).NESNE = " "
            VERI(0).DEGER = " "
            Return VERI
#End Region

        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

#Region "WS HATA"
            Dim VERI(0) As Islem_kprm
            VERI(0) = New Islem_kprm
            VERI(0).DURUM = NUMARATOR.WSVERSIYONESKI
            VERI(0).NESNE = " "
            VERI(0).DEGER = " "
            Return VERI

#End Region



        End If




        bolunen_veri.Add(a_veri.Split("|"))
        For k = 0 To bolunen_veri(0).Length - 1

            bolunen_veri_satir = bolunen_veri.Item(0)(k).ToString

            A = bolunen_veri_satir.Split(",")

            gelenVeri.ekle(A(0), A(1))

            gelenVeriList.Add(gelenVeri)
            gelenVeri = New Gelen_Veri
            'Nesneler.Add(A(0))
            'Degerler.Add(A(1))
        Next



        If a_nesne <> " " Then
            For k = 0 To gelenVeriList.Count - 1
                KOSULVALUE = KOSULVALUE & "('" & gelenVeriList(k).NESNE & "','" & gelenVeriList(k).DEGER & "','" & a_uid & "') ,"
                KOSULNESNE = KOSULNESNE & "'" & gelenVeriList(k).NESNE & "',"
            Next


        End If
        'update
        'If islem = 1 Then
        '    SQ = ""
        '    SQ = " UPDATE tbl_mob_kprm SET a_deger='" & a_deger & "' "
        '    SQ = SQ & " WHERE a_uid=" & a_uid & " " & KOSULNESNE & "  "
        '    If SQL_KOS(SQ, False) = True Then
        '        Dim VERI(0) As Islem_kprm
        '        VERI(0) = New Islem_kprm
        '        VERI(0).DURUM = NUMARATOR.BAŞARILI

        '        Return VERI
        '    Else
        '        Dim VERI(0) As Islem_kprm
        '        VERI(0) = New Islem_kprm
        '        VERI(0).DURUM = NUMARATOR.HATA
        '        Return VERI
        '    End If
        '    'Tabloyu koşullara göre listele
        'Else
        If islem = 2 Then
            SQ = ""
            SQ = " Select a_uid,a_nesne,a_deger From tbl_mob_kprm "
            SQ = SQ & " where a_uid=" & a_uid & " " & KOSULNESNE
            SQ = Mid(SQ, 1, SQ.Length - 1)
            SQ = SQ & ")"
            DT = SQL_TABLO(SQ, "T", False, False, False)

            If DT.Rows.Count = gelenVeriList.Count Then
                Dim VERI(DT.Rows.Count - 1) As Islem_kprm
                For x = 0 To DT.Rows.Count - 1
                    VERI(x) = New Islem_kprm
                    VERI(x).DURUM = NUMARATOR.BAŞARILI
                    VERI(x).USERID = NULN(DT, x, "a_uid")
                    VERI(x).NESNE = NULA(DT, x, "a_nesne")
                    VERI(x).DEGER = NULA(DT, x, "a_deger")
                Next
                Return VERI

            ElseIf DT.Rows.Count <> 0 Then
                Dim VERI(DT.Rows.Count - 1) As Islem_kprm
                For x = 0 To DT.Rows.Count - 1
                    VERI(x) = New Islem_kprm
                    VERI(x).DURUM = NUMARATOR.UYARI
                    VERI(x).USERID = NULN(DT, x, "a_uid")
                    VERI(x).NESNE = NULA(DT, x, "a_nesne")
                    VERI(x).DEGER = NULA(DT, x, "a_deger")
                Next
                Return VERI

            Else
                Dim VERI(0) As Islem_kprm
                VERI(0) = New Islem_kprm
                VERI(0).DURUM = NUMARATOR.HATA
                VERI(0).NESNE = " "
                VERI(0).DEGER = " "
                Return VERI
            End If
            'If DT.Rows.Count > 0 Then
            '    Dim VERI(DT.Rows.Count - 1) As Islem_kprm
            '    For x = 0 To DT.Rows.Count - 1
            '        VERI(x) = New Islem_kprm
            '        VERI(x).DURUM = NUMARATOR.BAŞARILI
            '        VERI(x).USERID = NULN(DT, x, "a_uid")
            '        VERI(x).NESNE = NULA(DT, x, "a_nesne")
            '        VERI(x).DEGER = NULA(DT, x, "a_deger")

            '    Next
            '    Return VERI
            'Else
            '    Dim VERI(0) As Islem_kprm
            '    VERI(0) = New Islem_kprm
            '    VERI(0).DURUM = NUMARATOR.HATA
            '    Return VERI
            'End If
            'insert
        ElseIf islem = 3 Then 'Once Delete sonra insert işlemi yapılıyor

            'For k = 1 To gelenVeriList.Count - 1
            '    If Trim(gelenVeriList(k).DEGER) = Nothing Then
            '        gelenVeriList(k).DEGER = "0"
            '    End If

            '    SQ = ""
            '    SQ = "INSERT INTO tbl_mob_kprm "
            '    SQ = SQ & " (a_uid,a_nesne,a_deger) "
            '    SQ = SQ & " VALUES ("
            '    SQ = SQ & " " & a_uid.ToString & ", "
            '    SQ = SQ & " N'" & gelenVeriList(k).NESNE(k) & "', "
            '    SQ = SQ & " N'" & Trim(gelenVeriList(k).DEGER) & "' "
            '    SQ = SQ & " ) "
            '    SQL_KOS(SQ, False)
            'Next


            SQ = ""
            SQ = "DELETE FROM tbl_mob_kprm "
            SQ = SQ & " Where a_uid=" & a_uid & " " & KOSULNESNE
            SQ = Mid(SQ, 1, SQ.Length - 1)
            SQ = SQ & ")"
            If SQL_KOS(SQ, False) = True Then
                Dim VERI(0) As Islem_kprm
                VERI(0) = New Islem_kprm
                VERI(0).DURUM = NUMARATOR.BAŞARILI

            Else
                Dim VERI(0) As Islem_kprm
                VERI(0) = New Islem_kprm
                VERI(0).DURUM = NUMARATOR.HATA

            End If

            SQ = ""
            SQ = SQ & "INSERT INTO tbl_mob_kprm "
            SQ = SQ & "([a_nesne], [a_deger], [a_uid]) "
            SQ = SQ & "VALUES "
            SQ = SQ & KOSULVALUE
            SQ = Mid(SQ, 1, SQ.Length - 1)

            If SQL_KOS(SQ, False) = True Then
                'Dim VERI(gelenVeriList.Count - 1) As Islem_kprm
                'For k = 0 To gelenVeriList.Count - 1
                '    VERI(k) = New Islem_kprm
                '    VERI(k).DURUM = NUMARATOR.BAŞARILI
                '    VERI(k).USERID = a_uid
                '    VERI(k).NESNE = gelenVeriList(k).NESNE
                '    VERI(k).DEGER = gelenVeriList(k).DEGER
                'Next
                Dim VERI(0) As Islem_kprm
                VERI(0) = New Islem_kprm
                VERI(0).DURUM = NUMARATOR.BAŞARILI
                VERI(0).USERID = a_uid
                VERI(0).NESNE = " "
                VERI(0).DEGER = " "
                Return VERI
            Else
                Dim VERI(0) As Islem_kprm
                VERI(0) = New Islem_kprm
                VERI(0).DURUM = NUMARATOR.HATA
                VERI(0).NESNE = " "
                VERI(0).DEGER = " "
                Return VERI
            End If

            'delete
        ElseIf islem = 4 Then
            'SQ = ""
            'SQ = "DELETE FROM tbl_mob_kprm "
            'SQ = SQ & " Where a_uid=" & a_uid & " " & KOSULNESNE
            'SQ = Mid(SQ, 1, SQ.Length - 1)
            'SQ = SQ & ")"
            'If SQL_KOS(SQ, False) = True Then
            '    Dim VERI(0) As Islem_kprm
            '    VERI(0) = New Islem_kprm
            '    VERI(0).DURUM = NUMARATOR.BAŞARILI
            '    Return VERI
            'Else
            '    Dim VERI(0) As Islem_kprm
            '    VERI(0) = New Islem_kprm
            '    VERI(0).DURUM = NUMARATOR.HATA
            '    Return VERI
            'End If
        End If

        Dim DEGER(0) As Islem_kprm
        DEGER(0) = New Islem_kprm
        DEGER(0).DURUM = NUMARATOR.DEGERDONMEDI
        DEGER(0).NESNE = " "
        DEGER(0).DEGER = " "
        Return DEGER
    End Function



#End Region
#Region "MB/Update tbl_mob_menu"
    Public Class Islem_menu
        Public DURUM As Long
        Public ID As Long
        Public ADI As String
        Public UST As Long
        Public SIRANO As Long
    End Class
    <WebMethod()>
    Public Function MB_Islem_Menu(ByVal VERSION As String, ByVal islem As Long, ByVal a_id As Long, ByVal a_adi As String, ByVal a_ust As String, ByVal a_sirano As String) As Islem_menu()
        Dim SQ As String
        Dim TEXT As String = ""
        Dim KOSULADI As String = " "
        Dim KOSULUST As String = " "
        Dim KOSULSIRANO As String = " "
        Dim DT As New DataTable
        Dim USTCONVERT As Long
        Dim SIRANOCONVERT As Long
        Dim MAXID As Long
        Dim x As Long

        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()


        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"

            Dim VERI(0) As Islem_menu
            VERI(0) = New Islem_menu
            VERI(0).DURUM = NUMARATOR.APKVERSIYONESKI
            Return VERI
#End Region

        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

#Region "WS HATA"
            Dim VERI(0) As Islem_menu
            VERI(0) = New Islem_menu
            VERI(0).DURUM = NUMARATOR.WSVERSIYONESKI
            Return VERI

#End Region



        End If





        If a_adi <> " " Then
            KOSULADI = "AND a_adi='" & a_adi & "' "
        End If
        If a_sirano <> " " Then
            'Longa çevir, hata verirse A'ya git
            If Long.TryParse(a_sirano, SIRANOCONVERT) = False Then
                GoTo A
            End If
            KOSULSIRANO = "AND a_sirano=" & SIRANOCONVERT & " "
        End If
        If a_ust <> " " Then
            'Longa çevir, hata verirse A'ya git
            If Long.TryParse(a_ust, USTCONVERT) = False Then
                GoTo A
            End If
            KOSULUST = "AND a_ust=" & USTCONVERT & " "
        End If
        If islem = 1 Then
            'Longa çevir, hata verirse A'ya git
            If Long.TryParse(a_sirano, SIRANOCONVERT) = False Then
                GoTo A
            End If
            'Longa çevir, hata verirse A'ya git
            If Long.TryParse(a_ust, USTCONVERT) = False Then
                GoTo A
            End If
            SQ = ""
            SQ = " UPDATE tbl_mob_menu SET a_adi='" & a_adi & "',a_ust=" & USTCONVERT & ",a_sirano=" & SIRANOCONVERT & " "
            SQ = SQ & " WHERE a_id=" & a_id & "  "
            If SQL_KOS(SQ, False) = True Then
                Dim VERI(0) As Islem_menu
                VERI(0) = New Islem_menu
                VERI(0).DURUM = NUMARATOR.BAŞARILI

                Return VERI
            Else
                Dim VERI(0) As Islem_menu
                VERI(0) = New Islem_menu
                VERI(0).DURUM = NUMARATOR.HATA
                Return VERI
            End If
        ElseIf islem = 2 Then
            'Id 0 ise bütün tabloyu listele
            If a_id = 0 Then
                SQ = ""
                SQ = " Select a_id,a_adi,a_ust,a_sirano From tbl_mob_menu "
                'SQ = SQ & " where a_id=" & a_id & " " & KOSULADI & " " & KOSULUST & " " & KOSULSIRANO & "  "
                DT = SQL_TABLO(SQ, "T", False, False, False)
                If DT.Rows.Count > 0 Then
                    Dim VERI(DT.Rows.Count - 1) As Islem_menu
                    For x = 0 To DT.Rows.Count - 1
                        VERI(x) = New Islem_menu
                        VERI(x).DURUM = NUMARATOR.BAŞARILI
                        VERI(x).ID = NULN(DT, x, "a_id")
                        VERI(x).ADI = NULA(DT, x, "a_adi")
                        VERI(x).UST = NULN(DT, x, "a_ust")
                        VERI(x).SIRANO = NULN(DT, x, "a_sirano")

                    Next
                    Return VERI
                Else
                    Dim VERI(0) As Islem_menu
                    VERI(0) = New Islem_menu
                    VERI(0).DURUM = NUMARATOR.HATA
                    Return VERI
                End If


                'ıd sıfır değilse koşullara göre listele
            Else
                SQ = ""
                SQ = " Select a_id,a_adi,a_ust,a_sirano From tbl_mob_menu "
                SQ = SQ & " where a_id=" & a_id & " " & KOSULADI & " " & KOSULUST & " " & KOSULSIRANO & "  "
                DT = SQL_TABLO(SQ, "T", False, False, False)
                If DT.Rows.Count > 0 Then
                    Dim VERI(DT.Rows.Count - 1) As Islem_menu
                    For x = 0 To DT.Rows.Count - 1
                        VERI(x) = New Islem_menu
                        VERI(x).DURUM = NUMARATOR.BAŞARILI
                        VERI(x).ID = NULN(DT, x, "a_id")
                        VERI(x).ADI = NULA(DT, x, "a_adi")
                        VERI(x).UST = NULA(DT, x, "a_ust")
                        VERI(x).SIRANO = NULN(DT, x, "a_sirano")

                    Next
                    Return VERI
                Else
                    Dim VERI(0) As Islem_menu
                    VERI(0) = New Islem_menu
                    VERI(0).DURUM = NUMARATOR.HATA
                    Return VERI
                End If
            End If
        ElseIf islem = 3 Then
            SQ = ""
            SQ = " SELECT MAX(a_id) AS MAXID  FROM tbl_mob_menu "
            DT = SQL_TABLO(SQ, "T", False, False, False)
            MAXID = NULN(DT, 0, "MAXID")
            MAXID = MAXID + 1

            SQ = ""
            SQ = "INSERT INTO tbl_mob_menu"
            SQ = SQ & " (a_id,a_adi,a_ust,a_sirano) "
            SQ = SQ & " VALUES ( "
            SQ = SQ & " " & MAXID & ", "
            SQ = SQ & " N'" & a_adi & "', "
            SQ = SQ & " " & USTCONVERT & ", "
            SQ = SQ & " " & SIRANOCONVERT & " "
            SQ = SQ & " )"
            If SQL_KOS(SQ, False) = True Then
                Dim VERI(0) As Islem_menu
                VERI(0) = New Islem_menu
                VERI(0).DURUM = NUMARATOR.BAŞARILI
                Return VERI
            Else
                Dim VERI(0) As Islem_menu
                VERI(0) = New Islem_menu
                VERI(0).DURUM = NUMARATOR.HATA
                Return VERI
            End If

        ElseIf islem = 4 Then
            SQ = ""
            SQ = "DELETE FROM tbl_mob_menu "
            SQ = SQ & " Where a_id=" & a_id & " " & KOSULADI & "  " & KOSULSIRANO & " " & KOSULUST & " "
            If SQL_KOS(SQ, False) = True Then
                Dim VERI(0) As Islem_menu
                VERI(0) = New Islem_menu
                VERI(0).DURUM = NUMARATOR.BAŞARILI
                Return VERI
            Else
                Dim VERI(0) As Islem_menu
                VERI(0) = New Islem_menu
                VERI(0).DURUM = NUMARATOR.HATA
                Return VERI
            End If
        Else
            'Eger longa dönüştürme başarısızsa A'ya geliyor.
A:
            Dim VERI(0) As Islem_menu
            VERI(0) = New Islem_menu
            VERI(0).DURUM = NUMARATOR.HATA
            Return VERI
        End If
    End Function
#End Region
#Region "MB/Versiyon Kontrol"
    Public Class Versiyon
        Public DURUM As Long
        Public VERSIYON_MAJOR As String
        Public VERSIYON_MINOR As String
        Public VERSIYON_REVIZ As String

    End Class
    <WebMethod()>
    Public Function MB_Versiyon() As Versiyon()

        Dim x() As String

        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        Dim VERI(0) As Versiyon
        VERI(0) = New Versiyon
        VERI(0).DURUM = NUMARATOR.BAŞARILI

        x = DLL_ASSEMBLY.Split(".")
        VERI(0).VERSIYON_MAJOR = x(0)
        VERI(0).VERSIYON_MINOR = x(1)
        VERI(0).VERSIYON_REVIZ = x(2)
        'VERI(0).VERSIYON_BUILD = x(3) 'Build no gerekirse açılır
        Return VERI

    End Function
#End Region



#Region "KASALAR"
    Public Class WS_KASALAR
        Public DURUM As Long
        Public KASAID As Integer
        Public KASAADI As String
        Public KASAKOD As String
        Public SUBEID As Long

    End Class

    <WebMethod()>
    Public Function MB_Kasalar(ByVal VERSION As String, ByVal USERID As Long, ByVal KULLANICIGRUPID As Long) As WS_KASALAR()


        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"
            Dim VERI1(0) As WS_KASALAR
            VERI1(0) = New WS_KASALAR
            VERI1(0).DURUM = NUMARATOR.APKVERSIYONESKI
            VERI1(0).KASAID = 0
            VERI1(0).KASAADI = " "
            VERI1(0).KASAKOD = " "
            VERI1(0).SUBEID = 0


            Return VERI1

#End Region

        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

#Region "WS HATA"
            Dim VERI1(0) As WS_KASALAR
            VERI1(0) = New WS_KASALAR
            VERI1(0).DURUM = NUMARATOR.WSVERSIYONESKI
            VERI1(0).KASAID = 0
            VERI1(0).KASAADI = " "
            VERI1(0).KASAKOD = " "
            VERI1(0).SUBEID = 0
            Return VERI1

#End Region



        End If


        Dim SQ As String
        Dim DT As New DataTable

        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()

        'Yönetici ise tüm kasaları getir.

        If KULLANICIGRUPID = 1 Then
            SQ = ""
            SQ = "SELECT a_id,a_adi,a_kod,a_sube_id "
            SQ = SQ & "  FROM " & KUL.tfrm & "kasa "

            DT = SQL_TABLO(SQ, "T", False, False, False)
            If DT.Rows.Count > 0 Then
                Dim VERI1(DT.Rows.Count - 1) As WS_KASALAR
                For X = 0 To DT.Rows.Count - 1

                    VERI1(X) = New WS_KASALAR
                    VERI1(X).DURUM = NUMARATOR.BAŞARILI
                    VERI1(X).KASAID = NULN(DT, X, "a_id")
                    VERI1(X).KASAKOD = NULA(DT, X, "a_kod")
                    VERI1(X).KASAADI = NULA(DT, X, "a_adi")
                    VERI1(X).SUBEID = NULN(DT, X, "a_sube_id")


                Next

                Return VERI1
            Else
                Dim VERI1(0) As WS_KASALAR
                VERI1(0) = New WS_KASALAR
                VERI1(0).DURUM = NUMARATOR.HATA
                VERI1(0).KASAID = 0
                VERI1(0).KASAKOD = " "
                VERI1(0).KASAADI = " "
                VERI1(0).SUBEID = 0
                Return VERI1

            End If

        Else

            'Yönetici değilse yetkili olduğu kasaları getir.
            SQ = ""
            SQ = "SELECT KASA.a_id,KASA.a_adi,KASA.a_kod,KASA.a_sube_id"
            SQ = SQ & "  FROM " & KUL.tfrm & "sube_kul AS SubeKul "
            SQ = SQ & "  INNER JOIN " & KUL.tfrm & "sube_kasa AS SUBEKASA ON SubeKul.a_subeid = SUBEKASA.a_subeid"
            SQ = SQ & "  INNER Join " & KUL.tfrm & "kasa as KASA ON SUBEKASA.a_kasaid =KASA.a_id"
            SQ = SQ & "  WHERE SubeKul.a_userid=" & USERID & ""
            SQ = SQ & " GROUP BY KASA.a_id,KASA.a_adi,KASA.a_kod,KASA.a_sube_id"

            DT = SQL_TABLO(SQ, "T", False, False, False)


            If DT.Rows.Count > 0 Then
                Dim VERI1(DT.Rows.Count - 1) As WS_KASALAR
                For X = 0 To DT.Rows.Count - 1


                    VERI1(X) = New WS_KASALAR
                    VERI1(X).DURUM = NUMARATOR.BAŞARILI
                    VERI1(X).KASAID = NULN(DT, X, "a_id")
                    VERI1(X).KASAKOD = NULA(DT, X, "a_kod")
                    VERI1(X).KASAADI = NULA(DT, X, "a_adi")
                    VERI1(X).SUBEID = NULN(DT, X, "a_sube_id")

                Next

                Return VERI1
            Else
                Dim VERI1(0) As WS_KASALAR
                VERI1(0) = New WS_KASALAR
                VERI1(0).DURUM = NUMARATOR.HATA
                VERI1(0).KASAID = 0
                VERI1(0).KASAKOD = " "
                VERI1(0).KASAADI = " "
                VERI1(0).SUBEID = 0
                Return VERI1

            End If

        End If


    End Function


#End Region



#Region "YETKİ TABLOSU"
    Public Class ISLEM_YETKI
        Public DURUM As Long
        Public MENUADI As String
        Public MENUKOD As String
        Public YETKI As String

    End Class
    <WebMethod()>
    Public Function MB_Islem_Yetki(ByVal VERSION As String, ByVal a_userid As Long) As ISLEM_YETKI()
        Dim SQ As String
        Dim DT As New DataTable
        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()


        If MB_VER_KONTROL(VERSION) = NUMARATOR.APKVERSIYONESKI Then 'Apk Versiyon Eski


#Region "APK HATA"
            Dim VERI(0) As ISLEM_YETKI
            VERI(0) = New ISLEM_YETKI
            VERI(0).DURUM = NUMARATOR.APKVERSIYONESKI
            VERI(0).MENUADI = " "
            VERI(0).MENUKOD = " "
            VERI(0).YETKI = " "
            Return VERI
#End Region

        ElseIf MB_VER_KONTROL(VERSION) = NUMARATOR.WSVERSIYONESKI Then 'Webservis Versiyon Eski

#Region "WS HATA"
            Dim VERI(0) As ISLEM_YETKI
            VERI(0) = New ISLEM_YETKI
            VERI(0).DURUM = NUMARATOR.WSVERSIYONESKI
            VERI(0).MENUADI = " "
            VERI(0).MENUKOD = " "
            VERI(0).YETKI = " "
            Return VERI

#End Region



        End If

        SQ = ""
        SQ = "SELECT a_menukod, a_menuadi, a_yetki FROM tbl_yetki "
        SQ = SQ & " WHERE a_userid=" & a_userid & " and a_menukod IN('01.04', '01.04.12', '01.04.02', '01.03.00', '01.02.00', '01.03.01', '01.02.01', '01.04.09', '00.00', '00.00','01.01','01.01.00','01.01.01') "
        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)

        If DT.Rows.Count > 0 Then
            Dim VERI(DT.Rows.Count - 1) As ISLEM_YETKI
            For x = 0 To DT.Rows.Count - 1

                VERI(x) = New ISLEM_YETKI
                VERI(x).DURUM = NUMARATOR.BAŞARILI
                VERI(x).MENUADI = NULA(DT, x, "a_menuadi")
                VERI(x).MENUKOD = NULA(DT, x, "a_menukod")
                VERI(x).YETKI = NULA(DT, x, "a_yetki")
            Next
            Return VERI
        Else
            Dim VERI(0) As ISLEM_YETKI
            VERI(0) = New ISLEM_YETKI
            VERI(0).DURUM = NUMARATOR.HATA
            VERI(0).MENUADI = " "
            VERI(0).MENUKOD = " "
            VERI(0).YETKI = " "
            Return VERI
        End If
    End Function
#End Region

#Region "VERSİYON KONTROL"


    Public Function MB_VER_KONTROL(ByVal VERSIYON As String) As String

        Dim Param_Ver As New ArrayList
        Dim PARAM_VERSIYON_MAJOR As Integer
        Dim PARAM_VERSIYON_MINOR As Integer
        Dim PARAM_VERSIYON_REVIZ As Integer

        Dim WS_Ver As New ArrayList
        Dim WS_VERSIYON_MAJOR As Integer
        Dim WS_VERSIYON_MINOR As Integer
        Dim WS_VERSIYON_REVIZ As Integer




        Param_Ver.Add(VERSIYON.Split("."))
        PARAM_VERSIYON_MAJOR = Convert.ToInt32(Param_Ver.Item(0)(0).ToString)
        PARAM_VERSIYON_MINOR = Convert.ToInt32(Param_Ver.Item(0)(1).ToString)
        PARAM_VERSIYON_REVIZ = Convert.ToInt32(Param_Ver.Item(0)(2).ToString)

        WS_Ver.Add(Module1.DLL_ASSEMBLY.Split("."))
        WS_VERSIYON_MAJOR = Convert.ToInt32(WS_Ver.Item(0)(0).ToString)
        WS_VERSIYON_MINOR = Convert.ToInt32(WS_Ver.Item(0)(1).ToString)
        WS_VERSIYON_REVIZ = Convert.ToInt32(WS_Ver.Item(0)(2).ToString)



        If WS_VERSIYON_MAJOR = PARAM_VERSIYON_MAJOR Then 'Major versiyon eşit mi?


            If WS_VERSIYON_MINOR = PARAM_VERSIYON_MINOR Then 'Minor Versiyon eşit mi?


                If WS_VERSIYON_REVIZ = PARAM_VERSIYON_REVIZ Then 'Reviz versiyon eşit mi?


                    Return NUMARATOR.BAŞARILI




                ElseIf WS_VERSIYON_REVIZ > PARAM_VERSIYON_REVIZ Then 'APK  Versiyonu eski

                    Return NUMARATOR.APKVERSIYONESKI


                Else 'Webservis  Versiyonu eski
                    Return NUMARATOR.WSVERSIYONESKI



                End If


            ElseIf WS_VERSIYON_MINOR > PARAM_VERSIYON_MINOR Then 'APK  Versiyonu eski

                Return NUMARATOR.APKVERSIYONESKI

            Else 'Webservis  Versiyonu eski
                Return NUMARATOR.WSVERSIYONESKI


            End If



        ElseIf WS_VERSIYON_MAJOR > PARAM_VERSIYON_MAJOR Then 'APK  Versiyonu eski
            Return NUMARATOR.APKVERSIYONESKI


        Else 'Webservis  Versiyonu eski
            Return NUMARATOR.WSVERSIYONESKI




        End If




    End Function




#End Region

#Region "MB/BelgenoUret"

    Public Function MB_BelgenoUret(ByVal TUR As Long, ByVal userid As Long) As WS_BELGENUMARASI()
        Dim SQ As String
        Dim DT As New DataTable
        Dim Belge_No As String
        Dim K_ID As Long
        Dim VERI(0) As WS_BELGENUMARASI





        B_SQL = connstr
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        If TUR = 49 Then
            K_ID = TSAY.MAX_FIS_DEPO(49, 0)

            Select Case K_ID
                Case 0 To 9
                    Belge_No = "M" & userid & "-00000000" & K_ID
                Case 10 To 99
                    Belge_No = "M" & userid & "-0000000" & K_ID
                Case 100 To 999
                    Belge_No = "M" & userid & "-000000" & K_ID
                Case 1000 To 9999
                    Belge_No = "M" & userid & "-00000" & K_ID
                Case 10000 To 99999
                    Belge_No = "M" & userid & "-0000" & K_ID
                Case 100000 To 999999
                    Belge_No = "M" & userid & "-000" & K_ID
                Case 1000000 To 9999999
                    Belge_No = "M" & userid & "-00" & K_ID

                Case Else
                    Belge_No = "M" & userid & "-" & K_ID
            End Select


            VERI(0) = New WS_BELGENUMARASI

            VERI(0).DURUM = NUMARATOR.BAŞARILI
            VERI(0).BELGENO = Belge_No
            Return VERI
        ElseIf TUR <> 49 And TUR <> 89 And TUR <> 4 And TUR <> 33 And TUR <> 34 Then
            Select Case TUR
                Case 10, 11, 12, 13, 14, 16, 16, 70, 75
                    K_ID = TSAY.MAX_FIS_SCFAT(TUR, 0)

                Case 40, 41, 42, 96, 95
                    K_ID = TSAY.MAX_FIS_SCIRS(TUR, 0)

                    'Case 17, 18, 19, 20, 21, 22, 54, 71, 87, 88
                    '    K_ID = TSAY.MAX_FIS_SGFAT(TUR, 0)

                Case 43, 44, 69
                    K_ID = TSAY.MAX_FIS_SGIRS(TUR, 0)
                Case Else

                    VERI(0) = New WS_BELGENUMARASI

                    VERI(0).DURUM = NUMARATOR.HATA
                    VERI(0).BELGENO = " "
                    Return VERI
            End Select
            If K_ID <> 0 Then

                Select Case K_ID
                    Case 0 To 9
                        Belge_No = "M" & userid & "-00000000" & K_ID
                    Case 10 To 99
                        Belge_No = "M" & userid & "-0000000" & K_ID
                    Case 100 To 999
                        Belge_No = "M" & userid & "-000000" & K_ID
                    Case 1000 To 9999
                        Belge_No = "M" & userid & "-00000" & K_ID
                    Case 10000 To 99999
                        Belge_No = "M" & userid & "-0000" & K_ID
                    Case 100000 To 999999
                        Belge_No = "M" & userid & "-000" & K_ID
                    Case 1000000 To 9999999
                        Belge_No = "M" & userid & "-00" & K_ID

                    Case Else
                        Belge_No = " "
                End Select

                VERI(0) = New WS_BELGENUMARASI
                VERI(0).DURUM = NUMARATOR.BAŞARILI
                VERI(0).BELGENO = Belge_No
                Return VERI
            Else

                VERI(0) = New WS_BELGENUMARASI
                VERI(0).DURUM = NUMARATOR.HATA
                VERI(0).BELGENO = " "
                Return VERI
            End If
        ElseIf TUR = 4 Then
            Dim MAXID As Long
            SQ = ""
            SQ = "Select MAX(a_id) AS ID"
            SQ = SQ & " FROM " & KUL.tper & "stsaymas"

            DT = SQL_TABLO(SQ, "T", False, False, False)
            MAXID = NULN(DT, 0, "ID")
            K_ID = MAXID + 1

            If K_ID <> 0 Then

                Select Case K_ID
                    Case 0 To 9
                        Belge_No = "M" & userid & "-00000000" & K_ID
                    Case 10 To 99
                        Belge_No = "M" & userid & "-0000000" & K_ID
                    Case 100 To 999
                        Belge_No = "M" & userid & "-000000" & K_ID
                    Case 1000 To 9999
                        Belge_No = "M" & userid & "-00000" & K_ID
                    Case 10000 To 99999
                        Belge_No = "M" & userid & "-0000" & K_ID
                    Case 100000 To 999999
                        Belge_No = "M" & userid & "-000" & K_ID
                    Case 1000000 To 9999999
                        Belge_No = "M" & userid & "-00" & K_ID

                    Case Else
                        Belge_No = "M" & userid & "-" & K_ID
                End Select

                VERI(0) = New WS_BELGENUMARASI
                VERI(0).DURUM = NUMARATOR.BAŞARILI
                VERI(0).BELGENO = Belge_No
                Return VERI
            Else

                VERI(0) = New WS_BELGENUMARASI
                VERI(0).DURUM = NUMARATOR.HATA
                VERI(0).BELGENO = " "
                Return VERI
            End If
        ElseIf TUR = 33 Or TUR = 34 Then
            K_ID = TSAY.MAX_FIS_SIP(TUR, 0)

            If K_ID <> 0 Then

                Select Case K_ID
                    Case 0 To 9
                        Belge_No = "M" & userid & "-00000000" & K_ID
                    Case 10 To 99
                        Belge_No = "M" & userid & "-0000000" & K_ID
                    Case 100 To 999
                        Belge_No = "M" & userid & "-000000" & K_ID
                    Case 1000 To 9999
                        Belge_No = "M" & userid & "-00000" & K_ID
                    Case 10000 To 99999
                        Belge_No = "M" & userid & "-0000" & K_ID
                    Case 100000 To 999999
                        Belge_No = "M" & userid & "-000" & K_ID
                    Case 1000000 To 9999999
                        Belge_No = "M" & userid & "-00" & K_ID

                    Case Else
                        Belge_No = "M" & userid & "-" & K_ID
                End Select

                VERI(0) = New WS_BELGENUMARASI
                VERI(0).DURUM = NUMARATOR.BAŞARILI
                VERI(0).BELGENO = Belge_No
                Return VERI
            Else

                VERI(0) = New WS_BELGENUMARASI
                VERI(0).DURUM = NUMARATOR.HATA
                VERI(0).BELGENO = " "
                Return VERI
            End If
        End If

        VERI(0) = New WS_BELGENUMARASI
        VERI(0).DURUM = NUMARATOR.DEGERDONMEDI
        VERI(0).BELGENO = " "
        Return VERI

    End Function
#End Region


    Public Sub MASATASI(ByVal M1 As Long, ByVal M2 As Long, ByVal ADSNO As Long)

        Dim SQ As String
        Dim DT As New DataTable
        Dim DTA As New DataTable


        SQ = ""
        SQ = "SELECT a_id, a_kod, a_adi, a_durum, a_acsaat, a_cariid, a_kisisay, a_adsnosu"
        SQ = SQ & " FROM " & KUL.tfrm & "men_masa"
        SQ = SQ & " WHERE(a_id = " & M1 & ")"
        DT = SQL_TABLO(SQ, "T", False, False, False)



        SQ = "Update " & KUL.tfrm & "men_masa"
        SQ = SQ & " SET a_durum = 'DOLU'"
        SQ = SQ & "  , a_acsaat = '" & TTAR.TR2UK(TTAR.TARIH) & " " & Now.ToShortTimeString & "'"
        SQ = SQ & "  , a_kisisay = " & NULN(DT, 0, "a_kisisay") & " "
        SQ = SQ & "  , a_adsnosu = " & NULN(DT, 0, "a_adsnosu") & ""
        SQ = SQ & "  , a_cariid = " & NULN(DT, 0, "a_cariid") & " "
        SQ = SQ & " WHERE(a_id = " & M2 & ")"
        Call SQL_KOS(SQ, False)


        SQ = ""
        SQ = "SELECT a_id ,a_mid"
        SQ = SQ & " FROM " & KUL.tfrm & "men_adisyonmas"
        SQ = SQ & " WHERE a_mid = " & M1 & " AND a_durum=0"
        SQ = SQ & " AND a_id <> " & ADSNO
        DTA = SQL_TABLO(SQ, "T", False, False, False)

        If DTA.Rows.Count = 0 Then
            SQ = "Update " & KUL.tfrm & "men_masa"
            SQ = SQ & " SET a_durum = 'BOŞ'"
            SQ = SQ & "  , a_adsbas = 0"
            SQ = SQ & "  , a_acsaat = '01.01.1900 00:00:00' "
            SQ = SQ & "  , a_kisisay = " & 0 & " "
            SQ = SQ & "  , a_adsnosu = " & 0 & ""
            SQ = SQ & "  , a_cariid = " & 0 & " "
            SQ = SQ & " WHERE(a_id = " & M1 & ")"
            Call SQL_KOS(SQ, False)
        End If

        SQ = "UPDATE " & KUL.tfrm & "men_adisyon "
        SQ = SQ & " SET "
        SQ = SQ & " a_mid = " & M2 & ""
        SQ = SQ & " WHERE a_master_id=" & ADSNO
        Call SQL_KOS(SQ, False)

        SQ = "UPDATE " & KUL.tfrm & "men_adisyonmas "
        SQ = SQ & " SET "
        SQ = SQ & " a_mid = " & M2 & ""
        SQ = SQ & " WHERE a_id=" & ADSNO
        Call SQL_KOS(SQ, False)



    End Sub


#Region "Vade Tahsil Tarihi"
    Public Function Vade_Tahsil_TarihiBul(ByVal Bnk As Long, ByVal ISLTar As String) As String



        Dim GUNSAY As Long = 0
        Dim XTR As New Date

        GUNSAY = Val(VT_BILGI(KUL.tfrm & "banka", "a_id", "a_hesgun", Bnk))

        Try
            XTR = CType(ISLTar, Date)
        Catch ex As Exception
            XTR = Date.Today
        End Try
        XTR = DateAdd(DateInterval.Day, GUNSAY, XTR)
        Select Case XTR.DayOfWeek
            Case DayOfWeek.Saturday
                'Cumartesi +2 
                XTR = DateAdd(DateInterval.Day, 2, XTR)
            Case DayOfWeek.Sunday
                'Pazar +1 
                XTR = DateAdd(DateInterval.Day, 1, XTR)
            Case Else
                'Hafta İçi + 0
                XTR = XTR
        End Select
        Return XTR.ToShortDateString

    End Function

#End Region

    Public Function X_Belge_No_Duzelt(ByVal Tur As Integer) As Boolean
        Dim SQ As String = ""
        Dim DT As New DataTable
        Dim KR As String = ""

        Select Case Tur

            Case Islem_Turu.a010_Perakende_Satis_Faturasi,
                Islem_Turu.a011_Toptan_Satis_Faturasi,
                Islem_Turu.a014_Verilen_Hizmet_Faturasi,
                Islem_Turu.a015_Verilen_Proforma_Faturasi,
                Islem_Turu.a016_Verilen_Fiyat_Farki_Faturasi,
                Islem_Turu.a075_On_Satis_Faturasi
                SQ = ""
                SQ = "SELECT a_sfbseri, a_sfbno "

            Case Islem_Turu.a070_Satis_Fisi
                SQ = ""
                SQ = "SELECT a_sfisseri, a_sfisbno "
            Case Islem_Turu.a040_Perakende_Satis_Irsaliyesi,
                    Islem_Turu.a041_Toptan_Satis_Irsaliyesi,
                    Islem_Turu.a042_Konsinye_Mal_Cikis_Irsaliyesi,
                    Islem_Turu.a096_On_Satis_Irsaliyesi,
                    Islem_Turu.a069_Mal_Alim_Iade_Irsaliyesi
                SQ = ""
                SQ = "SELECT a_sibseri, a_sibno "
            Case Islem_Turu.a022_Müstahsil_Makbuzu
                SQ = ""
                SQ = "  SELECT a_afbseri, a_afbno "

            Case Islem_Turu.a039_Verilen_Teklifler
                SQ = ""
                SQ = "  SELECT a_tebseri, a_tebno "
            Case Islem_Turu.a033_Alinan_Siparisler
                SQ = ""
                SQ = "  SELECT a_sipseri, a_sipbno "

            Case Islem_Turu.a020_Alim_Iade_Faturasi
                SQ = ""
                SQ = " SELECT a_aifbseri, a_aiffbno "
            Case Islem_Turu.a029_Kasa_Tahsilat_Fisi
                SQ = ""
                SQ = " SELECT a_kasatahsilseri, a_kasatahsilno "
            Case Islem_Turu.a030_Kasa_Tediye_Fisi
                SQ = ""
                SQ = " SELECT a_kasatediyeseri, a_kasatediyeno"
            Case Islem_Turu.a072_Kasa_Virman_Fisi
                SQ = ""
                SQ = " SELECT a_kasavirmanseri, a_kasavirmanno"
            Case Else

                Return False

        End Select

        If Val(Parametre(-1, "089")) = 0 Then
            SQ = SQ & " FROM tbl_userfrm"
            SQ = SQ & " WHERE (a_userid = " & Val(KUL.PRM_belgeuser) & ") AND (a_firma = " & Val(KUL.FIRMA) & ")"
        Else
            SQ = SQ & " FROM " & KUL.tfrm & "sube"
            SQ = SQ & " WHERE (a_id = " & Val(KUL.ID_SUBE) & ") "
        End If
        DT = SQL_TABLO(SQ, "T", False, False, False)



        If Val(Parametre(-1, "089")) = 0 Then
            SQ = ""
            SQ = SQ & " UPDATE tbl_userfrm SET"
            KR = " WHERE (a_userid = " & Val(KUL.PRM_belgeuser) & ") AND (a_firma = " & Val(KUL.FIRMA) & ")"
        Else
            SQ = ""
            SQ = SQ & " UPDATE " & KUL.tfrm & "sube SET"
            KR = " WHERE (a_id = " & Val(KUL.ID_SUBE) & ") "
        End If

        Select Case Tur

            Case Islem_Turu.a010_Perakende_Satis_Faturasi,
                Islem_Turu.a011_Toptan_Satis_Faturasi,
                Islem_Turu.a014_Verilen_Hizmet_Faturasi,
                Islem_Turu.a015_Verilen_Proforma_Faturasi,
                Islem_Turu.a016_Verilen_Fiyat_Farki_Faturasi,
                Islem_Turu.a075_On_Satis_Faturasi
                SQ = SQ & "   a_sfbno =" & (NULD(DT, 0, "a_sfbno") + 1)

            Case Islem_Turu.a070_Satis_Fisi
                SQ = SQ & "  a_sfisbno =" & (NULD(DT, 0, "a_sfisbno") + 1)

            Case Islem_Turu.a040_Perakende_Satis_Irsaliyesi,
                    Islem_Turu.a041_Toptan_Satis_Irsaliyesi,
                    Islem_Turu.a042_Konsinye_Mal_Cikis_Irsaliyesi,
                    Islem_Turu.a096_On_Satis_Irsaliyesi,
                    Islem_Turu.a069_Mal_Alim_Iade_Irsaliyesi
                SQ = SQ & " a_sibno =" & (NULD(DT, 0, "a_sibno") + 1)

            Case Islem_Turu.a022_Müstahsil_Makbuzu
                SQ = SQ & "  a_afbno =" & (NULD(DT, 0, "a_afbno") + 1)

            Case Islem_Turu.a039_Verilen_Teklifler
                SQ = SQ & "  a_tebno =" & (NULD(DT, 0, "a_tebno") + 1)
            Case Islem_Turu.a033_Alinan_Siparisler
                SQ = SQ & "  a_sipbno =" & (NULD(DT, 0, "a_sipbno") + 1)
            Case Islem_Turu.a020_Alim_Iade_Faturasi
                SQ = SQ & "  a_aiffbno =" & (NULD(DT, 0, "a_aiffbno") + 1)
            Case Islem_Turu.a029_Kasa_Tahsilat_Fisi
                SQ = SQ & "  a_kasatahsilno =" & (NULD(DT, 0, "a_kasatahsilno") + 1)
            Case Islem_Turu.a030_Kasa_Tediye_Fisi
                SQ = SQ & "  a_kasatediyeno =" & (NULD(DT, 0, "a_kasatediyeno") + 1)
            Case Islem_Turu.a072_Kasa_Virman_Fisi
                SQ = SQ & "  a_kasavirmanno =" & (NULD(DT, 0, "a_kasavirmanno") + 1)
        End Select

        SQ = SQ & KR


        Return SQL_KOS(SQ, False)


    End Function

    Public Function Max_Bul(ByVal Tur As Long) As Long
        Dim SQ As String = ""
        Dim DT As New DataTable
        Dim EB As Long = 0

        '1- Adisyon No
        '2- Silinen No

        SQ = ""
        SQ = "SELECT a_tur, a_sayac"
        SQ = SQ & " FROM " & KUL.tfrm & "men_max_id"
        SQ = SQ & " WHERE a_tur = " & Tur & ""
        DT = SQL_TABLO(SQ, "T", False, False, False)

        If DT.Rows.Count = 0 Then
            SQ = ""
            SQ = "INSERT INTO " & KUL.tfrm & "men_max_id (a_tur, a_sayac)"
            SQ = SQ & " VALUES "
            SQ = SQ & " ("
            SQ = SQ & " " & Tur & ","
            SQ = SQ & " " & 1 & ""
            SQ = SQ & " )"
            SQL_KOS(SQ, False)
            Max_Bul = 1
        Else
            EB = NULA(DT, 0, "a_sayac")
            EB = EB + 1
            SQ = "UPDATE " & KUL.tfrm & "men_max_id "
            SQ = SQ & " SET "
            SQ = SQ & " a_sayac = " & EB & ""
            SQ = SQ & " WHERE a_tur = " & Tur & ""
            SQL_KOS(SQ, False)
            Max_Bul = EB
        End If

    End Function

End Class



