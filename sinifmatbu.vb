﻿Imports bym_dll
Imports System.Data.OleDb

Public Class sinifmatbu

    Public Dov_Adi As String = "TL"
    Public Dov_Krs As String = "Kr"

    Public Basilan_Sayfa_Nosu As Long = 1

    Public Class Genel_Toplam
        Public Const Mik As Integer = 50

        Structure Yapi
            '<VBFixedString(50), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=50)> Public Adi As String
            Dim Adi As String
            Dim Top As Double
        End Structure

        Public ATop(Mik) As Yapi

        Public Sub New()
            Dim X As Long = 0
            For X = 0 To Mik
                ATop(X).Adi = ""
                ATop(X).Top = 0
            Next
        End Sub

        Public Function Adet() As Integer
            Dim X As Long = 0
            Dim C As Long = 0

            For X = 0 To Mik
                If ATop(X).Adi <> "" Then
                    C = C + 1
                End If
            Next
            Adet = C
        End Function

        Public Function Bul(ByVal S As String) As Integer
            Dim X As Long = 0

            For X = 0 To Mik
                If ATop(X).Adi = S Then
                    Bul = X
                    Exit Function
                End If
            Next
            Bul = -1
        End Function

        Public Function ToplamaEkle(ByVal S As String, ByVal Deger As Double) As Boolean
            Dim I As Long = 0
            I = Bul(S)
            If I >= 0 Then
                ToplamaEkle = True
                ATop(I).Top = ATop(I).Top + Deger
            Else
                ToplamaEkle = False
            End If
        End Function

        Public Function Deger(ByVal S As String) As String
            Dim I As Long = 0
            I = Bul(S)
            If I >= 0 Then
                Deger = Trim(Str(ATop(I).Top))
            Else
                Deger = ATop(I).Adi
            End If

        End Function


    End Class
    Public Class alan
        Public Bolum As String
        Public Satir As Long
        Public Sutun As Long
        Public Uzunluk As Long
        Public Yon As String
        Public Tipi As String
        Public Alan As String
        Public Evrak_En As Long
        Public Evrak_Boy As Long
        Public Evrak_Font As Long
        Public Evrak_Sifir As String
        Public Evrak_Kurus As Long
        Public Evrak_KFormat As String
        Public Evrak_Ssonu As Long

        Public Sub clear()
            Me.Bolum = ""
            Me.Satir = 0
            Me.Sutun = 0
            Me.Uzunluk = 0
            Me.Yon = ""
            Me.Tipi = ""
            Me.Alan = ""
            Me.Evrak_En = 0
            Me.Evrak_Boy = 0
            Me.Evrak_Font = 0
            Me.Evrak_Sifir = "0"
            Me.Evrak_Kurus = 2
            Me.Evrak_KFormat = "###,###0.00"
        End Sub



    End Class

    Public A_TOP As New Genel_Toplam

    Sub SATIR_YAZ(ByRef Olusan As String, ByVal FH As Integer, ByVal DATA As String, ByVal INFO As Boolean)
        If DATA.Length > AX.Evrak_En Then
            DATA = Mid(DATA, 1, AX.Evrak_En)
        End If

        If INFO = True Then
            Olusan = Olusan & CL & TURK(DATA, 0) & vbCrLf
        Else
            Olusan = Olusan & TURK(DATA, 0) & vbCrLf
        End If
    End Sub


    'Sub FAT_SATIR_YAZ(ByVal FH As Integer, ByVal DATA As String, ByVal INFO As Boolean)

    '    If DATA.Length > AX.Evrak_En Then
    '        DATA = Mid(DATA, 1, AX.Evrak_En)
    '    End If

    '    If INFO = True Then
    '        PrintLine(FH, CL & TURK(DATA, 1))
    '    Else
    '        PrintLine(FH, TURK(DATA, 1))
    '    End If


    'End Sub
    Public CL As Long

    Public S As New DataTable
    Public DT As New DataTable

    Public MAXL_A As Long
    Public MAXL_B As Long
    Public MAXL_C As Long
    Public bilgi As Boolean = False
    Public satir_birlestir As Boolean = False
    Public AX As New alan
    Public ilk_c As Integer



    Private Function SQL_DATA(ByVal SQL As String, ByVal tur As Long, ByVal id As Long) As String

        'Fiş için geferekli olan verinin sql cümleri

        Dim SQ As String = ""
        Dim S As String = ""
        SQ = ""
        S = ""


        If SQL = "" Then
            SQL_DATA = ""
            Exit Function
        End If

        SQ = SQL


        SQ = SQ.Replace("tbl_001_01_", KUL.tper)
        SQ = SQ.Replace("tbl_001_", KUL.tfrm)

        SQ = SQ.Replace("<#01fisturu#>", Trim(Str(tur)))
        SQ = SQ.Replace("<#02fisnosu#>", Trim(Str(id)))
        SQ = SQ.Replace("<#03user#>", KUL.KOD)

        SQL_DATA = SQ

    End Function
    Public Sub YAZDIR_b(ByRef Olusan As String, ByVal KID As Long, ByVal tur As Long, ByVal id As Long)
        Dim SQ As String

        Dim A As New alan

        Dim ES As Long
        Dim ESu As Long
        Dim EBo As Long


        Dim BOS As String

        Dim X As Long
        Dim XL As Long
        Dim sx As Long
        'Dim Y As Long = 0
        Dim Z As Long

        Dim L As String

        'Dim DAB As OleDbDataAdapter
        Dim B As New DataTable
        Dim AD As String = ""

        'Fişin x,y koordinat düzleminde girilen şablon bilgisi
        SQ = ""
        SQ = "SELECT a_kid, a_id, a_bolum, a_satir, a_sutun, a_uzunluk, a_yasla, a_tip, a_alan, a_eboy, a_egen, a_efont, a_sifir"
        SQ = SQ & " FROM " & KUL.tfrm & "fisdizdet"
        SQ = SQ & " WHERE (a_kid = " & KID & ") AND (a_id = " & tur & ") AND (a_bolum = N'B')"
        SQ = SQ & " ORDER BY a_id, a_bolum, a_satir, a_sutun, a_uzunluk"
        B = SQL_TABLO(SQ, "e_yaz", False, False, False)

        If B.Rows.Count <= 0 Then
            'SORU("Uyarı", "Evrak dizay kaydı bulunamadı" & vbCrLf & "[Ayarlar] menüsünden [Matbu Form Tanımları]'na bakınız. ", "T")
            Exit Sub
        End If

        'Başlarken fark kadar ilerle
        A.Satir = Val(Trim(B.Rows(0).Item("a_satir").ToString))

        ES = CL + 1
        If ES <= 0 Then ES = 1

        For XL = 1 To A.Satir - ES
            CL = CL + 1
            SATIR_YAZ(Olusan, 1, "", bilgi)
        Next


        For sx = 0 To DT.Rows.Count - 1

            L = ""

            A.clear()
            'Fiş şablonu ilk bilgisi okunuyor.
            A.Alan = Trim(B.Rows(0).Item("a_alan").ToString)
            A.Bolum = Trim(B.Rows(0).Item("a_bolum").ToString)
            A.Satir = Val(Trim(B.Rows(0).Item("a_satir").ToString))
            A.Sutun = Val(Trim(B.Rows(0).Item("a_sutun").ToString))
            A.Tipi = Trim(B.Rows(0).Item("a_tip").ToString)
            A.Uzunluk = Val(Trim(B.Rows(0).Item("a_uzunluk").ToString))
            A.Yon = Trim(B.Rows(0).Item("a_yasla").ToString)
            A.Evrak_Boy = Val(Trim(B.Rows(0).Item("a_eboy").ToString))
            A.Evrak_En = Val(Trim(B.Rows(0).Item("a_egen").ToString))
            A.Evrak_Font = Val(Trim(B.Rows(0).Item("a_efont").ToString))
            A.Evrak_Sifir = Trim(B.Rows(0).Item("a_sifir").ToString)

            MAXL_C = A.Satir

            Try

                'Alan Adı Alan Değerine Dönüşüyor.
                AD = A.Alan
                'A.Alan = DT.Rows(sx).Item(A.Alan).ToString
                A.Alan = Cok_Satirli_Alan(A.Alan, sx)
                A_TOP.ToplamaEkle(AD, Val(A.Alan))


                If A.Tipi = "Sayı" Then
                    If Val(A.Alan) = 0 Then
                        A.Alan = A.Evrak_Sifir
                    End If
                End If

                If A.Tipi = "Para" Then
                    If Val(A.Alan) = 0 Then
                        A.Alan = "IKRAM"
                    Else
                        A.Alan = Format(Val(A.Alan), AX.Evrak_KFormat)
                    End If
                End If

                If A.Tipi = "Para2" Then
                    If Val(A.Alan) = 0 Then
                        A.Alan = "IKRAM"
                    Else
                        A.Alan = Format(Val(A.Alan), "###,###0.00")
                    End If
                End If
                If A.Tipi = "Para3" Then
                    If Val(A.Alan) = 0 Then
                        A.Alan = "IKRAM"
                    Else
                        A.Alan = Format(Val(A.Alan), "###,###0.000")
                    End If
                End If
                If A.Tipi = "Para4" Then
                    If Val(A.Alan) = 0 Then
                        A.Alan = "IKRAM"
                    Else
                        A.Alan = Format(Val(A.Alan), "###,###0.0000")
                    End If
                End If
                If A.Tipi = "Para5" Then
                    If Val(A.Alan) = 0 Then
                        A.Alan = "IKRAM"
                    Else
                        A.Alan = Format(Val(A.Alan), "###,###0.00000")
                    End If
                End If




                If A.Tipi = "Yazıyla" Then A.Alan = SOKU(A.Alan, Dov_Adi, Dov_Krs)
                If A.Tipi = "Saat" Then A.Alan = TTAR.SAAT
                If A.Tipi = "Tarih" Then A.Alan = TTAR.TARIH
                If A.Tipi = "SayfaNo" Then A.Alan = Str(Basilan_Sayfa_Nosu)
            Catch ex As Exception
                If A.Tipi = "Yazıyla" Then A.Alan = SOKU(A.Alan, Dov_Adi, Dov_Krs)
                If A.Tipi = "Saat" Then A.Alan = TTAR.SAAT
                If A.Tipi = "Tarih" Then A.Alan = TTAR.TARIH
                If A.Tipi = "SayfaNo" Then A.Alan = Str(Basilan_Sayfa_Nosu)
            End Try

            'İlk Satır oluşturuluyor.
            BOS = ""
            If A.Sutun > 1 Then
                BOS = New String(" ", A.Sutun - 1)
            End If

            L = L & BOS & Yasla(A.Alan, A.Uzunluk, A.Yon)
            ES = A.Satir
            ESu = A.Sutun
            EBo = A.Uzunluk


            For X = 1 To B.Rows.Count - 1

                'Mizansendeki Yeni bilgiler Tampona alınıyor.
                '------------------------------------------------------------------------------------
                A.clear()
                A.Alan = Trim(B.Rows(X).Item("a_alan").ToString)
                A.Bolum = Trim(B.Rows(X).Item("a_bolum").ToString)
                A.Satir = Val(Trim(B.Rows(X).Item("a_satir").ToString))
                A.Sutun = Val(Trim(B.Rows(X).Item("a_sutun").ToString))
                A.Tipi = Trim(B.Rows(X).Item("a_tip").ToString)
                A.Uzunluk = Val(Trim(B.Rows(X).Item("a_uzunluk").ToString))
                A.Yon = Trim(B.Rows(X).Item("a_yasla").ToString)
                A.Evrak_Boy = Val(Trim(B.Rows(X).Item("a_eboy").ToString))
                A.Evrak_En = Val(Trim(B.Rows(X).Item("a_egen").ToString))
                A.Evrak_Font = Val(Trim(B.Rows(X).Item("a_efont").ToString))
                A.Evrak_Sifir = Trim(B.Rows(X).Item("a_sifir").ToString)
                MAXL_C = A.Satir

                Try


                    'Alan Adı Alan Değerine Dönüşüyor.
                    AD = A.Alan
                    'A.Alan = DT.Rows(sx).Item(A.Alan).ToString
                    A.Alan = Cok_Satirli_Alan(A.Alan, sx)
                    A_TOP.ToplamaEkle(AD, Val(A.Alan))

                    If A.Tipi = "Sayı" Then
                        If Val(A.Alan) = 0 Then
                            A.Alan = A.Evrak_Sifir
                        End If
                    End If

                    If A.Tipi = "Para" Then
                        If Val(A.Alan) = 0 Then
                            A.Alan = "IKRAM"
                        Else
                            A.Alan = Format(Val(A.Alan), AX.Evrak_KFormat)
                        End If
                    End If

                    If A.Tipi = "Para2" Then
                        If Val(A.Alan) = 0 Then
                            A.Alan = "IKRAM"
                        Else
                            A.Alan = Format(Val(A.Alan), "###,###0.00")
                        End If
                    End If

                    If A.Tipi = "Para3" Then
                        If Val(A.Alan) = 0 Then
                            A.Alan = "IKRAM"
                        Else
                            A.Alan = Format(Val(A.Alan), "###,###0.000")
                        End If
                    End If
                    If A.Tipi = "Para4" Then
                        If Val(A.Alan) = 0 Then
                            A.Alan = "IKRAM"
                        Else
                            A.Alan = Format(Val(A.Alan), "###,###0.0000")
                        End If
                    End If
                    If A.Tipi = "Para5" Then
                        If Val(A.Alan) = 0 Then
                            A.Alan = "IKRAM"
                        Else
                            A.Alan = Format(Val(A.Alan), "###,###0.00000")
                        End If
                    End If
                    If A.Tipi = "Yazıyla" Then A.Alan = SOKU(A.Alan, Dov_Adi, Dov_Krs)
                    If A.Tipi = "Saat" Then A.Alan = TTAR.SAAT
                    If A.Tipi = "Tarih" Then A.Alan = TTAR.TARIH
                    If A.Tipi = "SayfaNo" Then A.Alan = Str(Basilan_Sayfa_Nosu)

                Catch ex As Exception
                    If A.Tipi = "Yazıyla" Then A.Alan = SOKU(A.Alan, Dov_Adi, Dov_Krs)
                    If A.Tipi = "Saat" Then A.Alan = TTAR.SAAT
                    If A.Tipi = "Tarih" Then A.Alan = TTAR.TARIH
                    If A.Tipi = "SayfaNo" Then A.Alan = Str(Basilan_Sayfa_Nosu)

                End Try

                'Yeni bir satıra geçiliyor
                'Satırı yaz satır tambonunu boşalt
                'Satır aralığını bul ve yaz
                'Satır işaretcisini ayarla
                If A.Satir <> ES Then
                    CL = CL + 1
                    SATIR_YAZ(Olusan, 1, L, bilgi)
                    For XL = 1 To (A.Satir - ES) - 1
                        CL = CL + 1
                        SATIR_YAZ(Olusan, 1, "", bilgi)
                    Next
                    ES = A.Satir
                    ESu = 0
                    L = ""
                End If

                BOS = ""
                If ESu > 0 Then
                    If (A.Sutun - ESu) - EBo > 0 Then
                        BOS = New String(" ", (A.Sutun - ESu) - EBo)
                    End If
                Else
                    If A.Sutun - ESu > 0 Then
                        BOS = New String(" ", A.Sutun - 1)
                    End If
                End If

                L = L & BOS & Yasla(A.Alan, A.Uzunluk, A.Yon)
                ESu = A.Sutun
                EBo = A.Uzunluk
            Next

            CL = CL + 1
            SATIR_YAZ(Olusan, 1, L, bilgi)

            '--------------------------------------------------------------------------------
            '--------------------------------------------------------------------------------
            '--------------------------------------------------------------------------------

            'Sayfa Sonu Kontrolü Yapılacak mı ?
            If AX.Evrak_Ssonu = 1 Then

                'Bir Sayfaya Yazılacak Veri Bitti mi ?
                If CL + 1 > ilk_c Then

                    '----------------------------------------------------------------------
                    'Sayfa Sonu Nakli Yekün Bilgisini Yaz.
                    '----------------------------------------------------------------------
                    Call YAZDIR_e(Olusan, KID, tur, id)
                    '----------------------------------------------------------------------

                    'Saya Sonuna Kadar İlerie
                    If AX.Evrak_Boy > CL Then
                        Z = CL
                        For X = 1 To AX.Evrak_Boy - Z
                            CL = CL + 1
                            SATIR_YAZ(Olusan, 1, "", bilgi)
                        Next
                    End If

                    If KUL.PRM_prnson = XL_Evet Then
                        SATIR_YAZ(Olusan, 1, Chr(12), False)
                    End If

                    Basilan_Sayfa_Nosu = Basilan_Sayfa_Nosu + 1



                    'Yazıcı Daraltma Kodu
                    If AX.Evrak_Font <> 0 Then
                        SATIR_YAZ(Olusan, 1, Chr(AX.Evrak_Font), False)
                    End If
                    'Kullanıcı Bazında Yazıcıya ilkleme Kodu Yollanıyor.
                    If KUL.PRM_prnbas <> "" Then
                        SATIR_YAZ(Olusan, 1, Chr(27) & Chr(Val(KUL.PRM_prnbas)), False)
                    End If


                    CL = 0
                    For XL = 1 To A.Satir - ES
                        CL = CL + 1
                        SATIR_YAZ(Olusan, 1, "", bilgi)
                    Next

                    '------------------------------------------------------------------------------------
                    '------------------------------------------------------------------------------------
                    'Sayfa Başlığını Ve İlk Satırı Yaz.
                    Call YAZDIR_ac(Olusan, KID, "A", tur, id)
                    Call YAZDIR_d(Olusan, KID, tur, id)
                    '------------------------------------------------------------------------------------
                    '------------------------------------------------------------------------------------


                    'Başlarken fark kadar ilerle
                    A.Satir = Val(Trim(B.Rows(0).Item("a_satir").ToString))
                    ES = CL + 1
                    If ES <= 0 Then ES = 1
                    For XL = 1 To A.Satir - ES
                        CL = CL + 1
                        SATIR_YAZ(Olusan, 1, "", bilgi)
                    Next

                End If
            End If

            '--------------------------------------------------------------------------------
            '--------------------------------------------------------------------------------
            '--------------------------------------------------------------------------------

        Next


    End Sub
    Public Function Cok_Satirli_Alan(ByVal Fld As String, ByVal Satir As Integer) As String

        Dim LN As Integer = 0
        Dim POS(99) As Integer
        Dim X As Integer = 0
        Dim Y As Integer = 1
        Dim VERI As String = ""

        If Mid(Fld, 1, 3) = "@01" Then LN = 1
        If Mid(Fld, 1, 3) = "@02" Then LN = 2
        If Mid(Fld, 1, 3) = "@03" Then LN = 3
        If Mid(Fld, 1, 3) = "@04" Then LN = 4
        If Mid(Fld, 1, 3) = "@05" Then LN = 5
        If Mid(Fld, 1, 3) = "@06" Then LN = 6
        If Mid(Fld, 1, 3) = "@07" Then LN = 7
        If Mid(Fld, 1, 3) = "@08" Then LN = 8
        If Mid(Fld, 1, 3) = "@09" Then LN = 9
        If Mid(Fld, 1, 3) = "@10" Then LN = 10
        If Mid(Fld, 1, 3) = "@11" Then LN = 11
        If Mid(Fld, 1, 3) = "@12" Then LN = 12
        If Mid(Fld, 1, 3) = "@13" Then LN = 13
        If Mid(Fld, 1, 3) = "@14" Then LN = 14
        If Mid(Fld, 1, 3) = "@15" Then LN = 15
        If Mid(Fld, 1, 3) = "@16" Then LN = 16
        If Mid(Fld, 1, 3) = "@17" Then LN = 17
        If Mid(Fld, 1, 3) = "@18" Then LN = 18
        If Mid(Fld, 1, 3) = "@19" Then LN = 19
        If Mid(Fld, 1, 3) = "@20" Then LN = 20
        If Mid(Fld, 1, 3) = "@21" Then LN = 21
        If Mid(Fld, 1, 3) = "@22" Then LN = 22
        If Mid(Fld, 1, 3) = "@23" Then LN = 23
        If Mid(Fld, 1, 3) = "@24" Then LN = 24
        If Mid(Fld, 1, 3) = "@25" Then LN = 25
        If Mid(Fld, 1, 3) = "@26" Then LN = 26
        If Mid(Fld, 1, 3) = "@27" Then LN = 27
        If Mid(Fld, 1, 3) = "@28" Then LN = 28
        If Mid(Fld, 1, 3) = "@29" Then LN = 29
        If Mid(Fld, 1, 3) = "@30" Then LN = 30

        If LN = 0 Then
            VERI = DT.Rows(Satir).Item(Fld).ToString
            VERI = VERI.Replace(Chr(10), "")
            VERI = VERI.Replace(Chr(13), "")
            Cok_Satirli_Alan = VERI
            Exit Function
        End If


        Fld = Mid(Fld, 4, Len(Fld))
        VERI = DT.Rows(Satir).Item(Fld).ToString


        For X = 1 To Len(VERI)
            If Mid(VERI, X, 1) = Chr(13) Then
                POS(Y) = X
                Y = Y + 1
                If Y > 30 Then Exit For
            End If
        Next


        If LN = 1 Then
            Try
                VERI = Mid(VERI, 1, POS(1) - 1)
            Catch ex As Exception
                If Mid(VERI, 1, 1) = "@" Then
                    VERI = ""
                End If
            End Try
            VERI = VERI.Replace(Chr(10), "")
            VERI = VERI.Replace(Chr(13), "")
            Cok_Satirli_Alan = VERI
            Exit Function
        End If

        Try
            If LN = Y Then
                VERI = Mid(VERI, (POS(LN - 1) + 1), Len(VERI))
            Else
                VERI = Mid(VERI, (POS(LN - 1) + 1), (POS(LN) - POS(LN - 1)))
            End If
        Catch ex As Exception
            VERI = ""
        End Try


        VERI = VERI.Replace(Chr(10), "")
        VERI = VERI.Replace(Chr(13), "")
        Cok_Satirli_Alan = VERI

    End Function
    Public Sub YAZDIR_ac(ByRef Olusan As String, ByVal KID As Long, ByVal BOLUM_AC As String, ByVal tur As Long, ByVal id As Long)

        Dim SQ As String

        Dim A As New alan

        Dim ES As Long
        Dim ESu As Long
        Dim EBo As Long

        Dim BOS As String


        Dim X As Long
        Dim XL As Long
        'Dim Y As Long = 0

        Dim L As String

        'Dim DAB As OleDbDataAdapter
        Dim B As New DataTable


        'Fişin x,y koordinat düzleminde girilen şablon bilgisi
        SQ = ""
        SQ = "SELECT a_kid,  a_id, a_bolum, a_satir, a_sutun, a_uzunluk, a_yasla, a_tip, a_alan, a_eboy, a_egen, a_efont, a_sifir"
        SQ = SQ & " FROM " & KUL.tfrm & "fisdizdet"
        SQ = SQ & " WHERE (a_kid = " & KID & ") AND  (a_id = " & tur & ") AND (a_bolum = N'" & BOLUM_AC & "')"
        SQ = SQ & " ORDER BY a_id, a_bolum, a_satir, a_sutun, a_uzunluk"
        B = SQL_TABLO(SQ, "e_yaz", False, False, False)

        If B.Rows.Count <= 0 Then
            'SORU("Uyarı", "Evrak dizay kaydı bulunamadı" & vbCrLf & "[Ayarlar] menüsünden [Matbu Form Tanımları]'na bakınız. ", "T")
            Exit Sub
        End If

        L = ""

        'Başlarken fark kadar ilerle
        A.Satir = Val(Trim(B.Rows(0).Item("a_satir").ToString))

        ES = CL + 1
        If ES <= 0 Then ES = 1

        For XL = 1 To A.Satir - ES
            CL = CL + 1
            SATIR_YAZ(Olusan, 1, "", bilgi)
        Next

        A.clear()

        'Fiş şablonu ilk bilgisi okunuyor.
        A.Alan = Trim(B.Rows(0).Item("a_alan").ToString)
        A.Bolum = Trim(B.Rows(0).Item("a_bolum").ToString)
        A.Satir = Val(Trim(B.Rows(0).Item("a_satir").ToString))
        A.Sutun = Val(Trim(B.Rows(0).Item("a_sutun").ToString))
        A.Tipi = Trim(B.Rows(0).Item("a_tip").ToString)
        A.Uzunluk = Val(Trim(B.Rows(0).Item("a_uzunluk").ToString))
        A.Yon = Trim(B.Rows(0).Item("a_yasla").ToString)
        A.Evrak_Boy = Val(Trim(B.Rows(0).Item("a_eboy").ToString))
        A.Evrak_En = Val(Trim(B.Rows(0).Item("a_egen").ToString))
        A.Evrak_Font = Val(Trim(B.Rows(0).Item("a_efont").ToString))
        A.Evrak_Sifir = Trim(B.Rows(0).Item("a_sifir").ToString)

        MAXL_A = A.Satir

        Try

            'A.Alan = DT.Rows(0).Item(A.Alan).ToString
            A.Alan = Cok_Satirli_Alan(A.Alan, 0)

            If A.Tipi = "Sayı" Then
                If Val(A.Alan) = 0 Then
                    A.Alan = A.Evrak_Sifir
                End If
            End If

            If A.Tipi = "Para" Then
                If Val(A.Alan) = 0 Then
                    A.Alan = A.Evrak_Sifir
                Else
                    A.Alan = Format(Val(A.Alan), AX.Evrak_KFormat)
                End If
            End If


            If A.Tipi = "Para2" Then
                If Val(A.Alan) = 0 Then
                    A.Alan = A.Evrak_Sifir
                Else
                    A.Alan = Format(Val(A.Alan), "###,###0.00")
                End If
            End If

            If A.Tipi = "Para3" Then
                If Val(A.Alan) = 0 Then
                    A.Alan = A.Evrak_Sifir
                Else
                    A.Alan = Format(Val(A.Alan), "###,###0.000")
                End If
            End If
            If A.Tipi = "Para4" Then
                If Val(A.Alan) = 0 Then
                    A.Alan = A.Evrak_Sifir
                Else
                    A.Alan = Format(Val(A.Alan), "###,###0.0000")
                End If
            End If
            If A.Tipi = "Para5" Then
                If Val(A.Alan) = 0 Then
                    A.Alan = A.Evrak_Sifir
                Else
                    A.Alan = Format(Val(A.Alan), "###,###0.00000")
                End If
            End If

            If A.Tipi = "Yazıyla" Then A.Alan = SOKU(A.Alan, Dov_Adi, Dov_Krs)
            If A.Tipi = "Saat" Then A.Alan = TTAR.SAAT
            If A.Tipi = "Tarih" Then A.Alan = TTAR.TARIH
            If A.Tipi = "SayfaNo" Then A.Alan = Str(Basilan_Sayfa_Nosu)
        Catch ex As Exception
            If A.Tipi = "Yazıyla" Then A.Alan = SOKU(A.Alan, Dov_Adi, Dov_Krs)
            If A.Tipi = "Saat" Then A.Alan = TTAR.SAAT
            If A.Tipi = "Tarih" Then A.Alan = TTAR.TARIH
            If A.Tipi = "SayfaNo" Then A.Alan = Str(Basilan_Sayfa_Nosu)
        End Try

        'İlk Satır oluşturuluyor.
        BOS = ""
        If A.Sutun > 1 Then
            BOS = New String(" ", A.Sutun - 1)
        End If
        L = L & BOS & Yasla(A.Alan, A.Uzunluk, A.Yon)

        ES = A.Satir
        ESu = A.Sutun
        EBo = A.Uzunluk


        For X = 1 To B.Rows.Count - 1

            'Mizansendeki Yeni bilgiler Tampona alınıyor.
            '------------------------------------------------------------------------------------
            A.clear()
            A.Alan = Trim(B.Rows(X).Item("a_alan").ToString)
            A.Bolum = Trim(B.Rows(X).Item("a_bolum").ToString)
            A.Satir = Val(Trim(B.Rows(X).Item("a_satir").ToString))
            A.Sutun = Val(Trim(B.Rows(X).Item("a_sutun").ToString))
            A.Tipi = Trim(B.Rows(X).Item("a_tip").ToString)
            A.Uzunluk = Val(Trim(B.Rows(X).Item("a_uzunluk").ToString))
            A.Yon = Trim(B.Rows(X).Item("a_yasla").ToString)
            A.Evrak_Boy = Val(Trim(B.Rows(X).Item("a_eboy").ToString))
            A.Evrak_En = Val(Trim(B.Rows(X).Item("a_egen").ToString))
            A.Evrak_Font = Val(Trim(B.Rows(X).Item("a_efont").ToString))
            A.Evrak_Sifir = Trim(B.Rows(X).Item("a_sifir").ToString)

            MAXL_A = A.Satir

            Try
                'A.Alan = DT.Rows(0).Item(A.Alan).ToString
                A.Alan = Cok_Satirli_Alan(A.Alan, 0)

                If A.Tipi = "Sayı" Then
                    If Val(A.Alan) = 0 Then
                        A.Alan = A.Evrak_Sifir
                    End If
                End If

                If A.Tipi = "Para" Then
                    If Val(A.Alan) = 0 Then
                        A.Alan = A.Evrak_Sifir
                    Else
                        A.Alan = Format(Val(A.Alan), AX.Evrak_KFormat)
                    End If
                End If

                If A.Tipi = "Para2" Then
                    If Val(A.Alan) = 0 Then
                        A.Alan = A.Evrak_Sifir
                    Else
                        A.Alan = Format(Val(A.Alan), "###,###0.00")
                    End If
                End If

                If A.Tipi = "Para3" Then
                    If Val(A.Alan) = 0 Then
                        A.Alan = A.Evrak_Sifir
                    Else
                        A.Alan = Format(Val(A.Alan), "###,###0.000")
                    End If
                End If
                If A.Tipi = "Para4" Then
                    If Val(A.Alan) = 0 Then
                        A.Alan = A.Evrak_Sifir
                    Else
                        A.Alan = Format(Val(A.Alan), "###,###0.0000")
                    End If
                End If
                If A.Tipi = "Para5" Then
                    If Val(A.Alan) = 0 Then
                        A.Alan = A.Evrak_Sifir
                    Else
                        A.Alan = Format(Val(A.Alan), "###,###0.00000")
                    End If
                End If

                If A.Tipi = "Yazıyla" Then A.Alan = SOKU(A.Alan, Dov_Adi, Dov_Krs)
                If A.Tipi = "Saat" Then A.Alan = TTAR.SAAT
                If A.Tipi = "Tarih" Then A.Alan = TTAR.TARIH
                If A.Tipi = "SayfaNo" Then A.Alan = Str(Basilan_Sayfa_Nosu)

            Catch ex As Exception
                If A.Tipi = "Yazıyla" Then A.Alan = SOKU(A.Alan, Dov_Adi, Dov_Krs)
                If A.Tipi = "Saat" Then A.Alan = TTAR.SAAT
                If A.Tipi = "Tarih" Then A.Alan = TTAR.TARIH
                If A.Tipi = "SayfaNo" Then A.Alan = Str(Basilan_Sayfa_Nosu)
            End Try


            'Yeni bir satıra geçiliyor
            'Satırı yaz satır tambonunu boşalt
            'Satır aralığını bul ve yaz
            'Satır işaretcisini ayarla
            If A.Satir <> ES Then
                CL = CL + 1
                SATIR_YAZ(Olusan, 1, L, bilgi)
                For XL = 1 To (A.Satir - ES) - 1
                    CL = CL + 1
                    SATIR_YAZ(Olusan, 1, "", bilgi)
                Next
                ES = A.Satir
                ESu = 0
                L = ""
            End If

            BOS = ""
            If ESu > 0 Then
                If (A.Sutun - ESu) - EBo > 0 Then
                    BOS = New String(" ", (A.Sutun - ESu) - EBo)
                End If
            Else
                If A.Sutun - ESu > 0 Then
                    BOS = New String(" ", A.Sutun - 1)
                End If
            End If

            L = L & BOS & Yasla(A.Alan, A.Uzunluk, A.Yon)
            ESu = A.Sutun
            EBo = A.Uzunluk

        Next

        CL = CL + 1
        SATIR_YAZ(Olusan, 1, L, bilgi)

    End Sub
    Public Sub YAZDIR_d(ByRef Olusan As String, ByVal KID As Long, ByVal tur As Long, ByVal id As Long)

        Dim SQ As String

        Dim A As New alan

        Dim ES As Long
        Dim ESu As Long
        Dim EBo As Long

        Dim BOS As String


        Dim X As Long
        Dim XL As Long
        'Dim Y As Long

        Dim L As String

        'Dim DAB As OleDbDataAdapter
        Dim B As New DataTable


        'Fişin x,y koordinat düzleminde girilen şablon bilgisi
        SQ = ""
        SQ = "SELECT a_kid, a_id, a_bolum, a_satir, a_sutun, a_uzunluk, a_yasla, a_tip, a_alan, a_eboy, a_egen, a_efont, a_sifir"
        SQ = SQ & " FROM " & KUL.tfrm & "fisdizdet"
        SQ = SQ & " WHERE (a_kid = " & KID & ") AND (a_id = " & tur & ") AND (a_bolum = N'D')"
        SQ = SQ & " ORDER BY a_id, a_bolum, a_satir, a_sutun, a_uzunluk"
        B = SQL_TABLO(SQ, "e_yaz", False, False, False)


        If B.Rows.Count <= 0 Then
            'SORU("Uyarı", "Evrak dizay kaydı bulunamadı" & vbCrLf & "[Ayarlar] menüsünden [Matbu Form Tanımları]'na bakınız. ", "T")
            Exit Sub
        End If

        L = ""

        'Başlarken fark kadar ilerle
        A.Satir = Val(Trim(B.Rows(0).Item("a_satir").ToString))

        ES = CL + 1
        If ES <= 0 Then ES = 1

        For XL = 1 To A.Satir - ES
            CL = CL + 1
            SATIR_YAZ(Olusan, 1, "", bilgi)
        Next

        A.clear()

        'Fiş şablonu ilk bilgisi okunuyor.
        A.Alan = Trim(B.Rows(0).Item("a_alan").ToString)
        A.Bolum = Trim(B.Rows(0).Item("a_bolum").ToString)
        A.Satir = Val(Trim(B.Rows(0).Item("a_satir").ToString))
        A.Sutun = Val(Trim(B.Rows(0).Item("a_sutun").ToString))
        A.Tipi = Trim(B.Rows(0).Item("a_tip").ToString)
        A.Uzunluk = Val(Trim(B.Rows(0).Item("a_uzunluk").ToString))
        A.Yon = Trim(B.Rows(0).Item("a_yasla").ToString)
        A.Evrak_Boy = Val(Trim(B.Rows(0).Item("a_eboy").ToString))
        A.Evrak_En = Val(Trim(B.Rows(0).Item("a_egen").ToString))
        A.Evrak_Font = Val(Trim(B.Rows(0).Item("a_efont").ToString))
        A.Evrak_Sifir = Trim(B.Rows(0).Item("a_sifir").ToString)

        MAXL_A = A.Satir

        Try
            A.Alan = A_TOP.Deger(A.Alan)

            If A.Tipi = "Sayı" Then
                If Val(A.Alan) = 0 Then
                    A.Alan = A.Evrak_Sifir
                End If
            End If

            If A.Tipi = "Para" Then
                If Val(A.Alan) = 0 Then
                    A.Alan = A.Evrak_Sifir
                Else
                    A.Alan = Format(Val(A.Alan), AX.Evrak_KFormat)
                End If
            End If


            If A.Tipi = "Para2" Then
                If Val(A.Alan) = 0 Then
                    A.Alan = A.Evrak_Sifir
                Else
                    A.Alan = Format(Val(A.Alan), "###,###0.00")
                End If
            End If

            If A.Tipi = "Para3" Then
                If Val(A.Alan) = 0 Then
                    A.Alan = A.Evrak_Sifir
                Else
                    A.Alan = Format(Val(A.Alan), "###,###0.000")
                End If
            End If
            If A.Tipi = "Para4" Then
                If Val(A.Alan) = 0 Then
                    A.Alan = A.Evrak_Sifir
                Else
                    A.Alan = Format(Val(A.Alan), "###,###0.0000")
                End If
            End If
            If A.Tipi = "Para5" Then
                If Val(A.Alan) = 0 Then
                    A.Alan = A.Evrak_Sifir
                Else
                    A.Alan = Format(Val(A.Alan), "###,###0.00000")
                End If
            End If

            If A.Tipi = "Yazıyla" Then A.Alan = SOKU(A.Alan, Dov_Adi, Dov_Krs)
            If A.Tipi = "Saat" Then A.Alan = TTAR.SAAT
            If A.Tipi = "Tarih" Then A.Alan = TTAR.TARIH
            If A.Tipi = "SayfaNo" Then A.Alan = Str(Basilan_Sayfa_Nosu)
        Catch ex As Exception
            If A.Tipi = "Yazıyla" Then A.Alan = SOKU(A.Alan, Dov_Adi, Dov_Krs)
            If A.Tipi = "Saat" Then A.Alan = TTAR.SAAT
            If A.Tipi = "Tarih" Then A.Alan = TTAR.TARIH
            If A.Tipi = "SayfaNo" Then A.Alan = Str(Basilan_Sayfa_Nosu)
        End Try

        'İlk Satır oluşturuluyor.
        BOS = ""
        If A.Sutun > 1 Then
            BOS = New String(" ", A.Sutun - 1)
        End If
        L = L & BOS & Yasla(A.Alan, A.Uzunluk, A.Yon)

        ES = A.Satir
        ESu = A.Sutun
        EBo = A.Uzunluk


        For X = 1 To B.Rows.Count - 1

            'Mizansendeki Yeni bilgiler Tampona alınıyor.
            '------------------------------------------------------------------------------------
            A.clear()
            A.Alan = Trim(B.Rows(X).Item("a_alan").ToString)
            A.Bolum = Trim(B.Rows(X).Item("a_bolum").ToString)
            A.Satir = Val(Trim(B.Rows(X).Item("a_satir").ToString))
            A.Sutun = Val(Trim(B.Rows(X).Item("a_sutun").ToString))
            A.Tipi = Trim(B.Rows(X).Item("a_tip").ToString)
            A.Uzunluk = Val(Trim(B.Rows(X).Item("a_uzunluk").ToString))
            A.Yon = Trim(B.Rows(X).Item("a_yasla").ToString)
            A.Evrak_Boy = Val(Trim(B.Rows(X).Item("a_eboy").ToString))
            A.Evrak_En = Val(Trim(B.Rows(X).Item("a_egen").ToString))
            A.Evrak_Font = Val(Trim(B.Rows(X).Item("a_efont").ToString))
            A.Evrak_Sifir = Trim(B.Rows(X).Item("a_sifir").ToString)

            MAXL_A = A.Satir

            Try

                A.Alan = A_TOP.Deger(A.Alan)

                If A.Tipi = "Sayı" Then
                    If Val(A.Alan) = 0 Then
                        A.Alan = A.Evrak_Sifir
                    End If
                End If

                If A.Tipi = "Para" Then
                    If Val(A.Alan) = 0 Then
                        A.Alan = A.Evrak_Sifir
                    Else
                        A.Alan = Format(Val(A.Alan), AX.Evrak_KFormat)
                    End If
                End If

                If A.Tipi = "Para2" Then
                    If Val(A.Alan) = 0 Then
                        A.Alan = A.Evrak_Sifir
                    Else
                        A.Alan = Format(Val(A.Alan), "###,###0.00")
                    End If
                End If

                If A.Tipi = "Para3" Then
                    If Val(A.Alan) = 0 Then
                        A.Alan = A.Evrak_Sifir
                    Else
                        A.Alan = Format(Val(A.Alan), "###,###0.000")
                    End If
                End If
                If A.Tipi = "Para4" Then
                    If Val(A.Alan) = 0 Then
                        A.Alan = A.Evrak_Sifir
                    Else
                        A.Alan = Format(Val(A.Alan), "###,###0.0000")
                    End If
                End If
                If A.Tipi = "Para5" Then
                    If Val(A.Alan) = 0 Then
                        A.Alan = A.Evrak_Sifir
                    Else
                        A.Alan = Format(Val(A.Alan), "###,###0.00000")
                    End If
                End If

                If A.Tipi = "Yazıyla" Then A.Alan = SOKU(A.Alan, Dov_Adi, Dov_Krs)
                If A.Tipi = "Saat" Then A.Alan = TTAR.SAAT
                If A.Tipi = "Tarih" Then A.Alan = TTAR.TARIH
                If A.Tipi = "SayfaNo" Then A.Alan = Str(Basilan_Sayfa_Nosu)

            Catch ex As Exception
                If A.Tipi = "Yazıyla" Then A.Alan = SOKU(A.Alan, Dov_Adi, Dov_Krs)
                If A.Tipi = "Saat" Then A.Alan = TTAR.SAAT
                If A.Tipi = "Tarih" Then A.Alan = TTAR.TARIH
                If A.Tipi = "SayfaNo" Then A.Alan = Str(Basilan_Sayfa_Nosu)
            End Try


            'Yeni bir satıra geçiliyor
            'Satırı yaz satır tambonunu boşalt
            'Satır aralığını bul ve yaz
            'Satır işaretcisini ayarla
            If A.Satir <> ES Then
                CL = CL + 1
                SATIR_YAZ(Olusan, 1, L, bilgi)
                For XL = 1 To (A.Satir - ES) - 1
                    CL = CL + 1
                    SATIR_YAZ(Olusan, 1, "", bilgi)
                Next
                ES = A.Satir
                ESu = 0
                L = ""
            End If

            BOS = ""
            If ESu > 0 Then
                If (A.Sutun - ESu) - EBo > 0 Then
                    BOS = New String(" ", (A.Sutun - ESu) - EBo)
                End If
            Else
                If A.Sutun - ESu > 0 Then
                    BOS = New String(" ", A.Sutun - 1)
                End If
            End If

            L = L & BOS & Yasla(A.Alan, A.Uzunluk, A.Yon)
            ESu = A.Sutun
            EBo = A.Uzunluk

        Next

        CL = CL + 1
        SATIR_YAZ(Olusan, 1, L, bilgi)


    End Sub
    Public Sub YAZDIR_e(ByRef Olusan As String, ByVal KID As Long, ByVal tur As Long, ByVal id As Long)

        Dim SQ As String

        Dim A As New alan

        Dim ES As Long
        Dim ESu As Long
        Dim EBo As Long

        Dim BOS As String


        Dim X As Long
        Dim XL As Long
        'Dim Y As Long

        Dim L As String

        'Dim DAB As OleDbDataAdapter
        Dim B As New DataTable


        'Fişin x,y koordinat düzleminde girilen şablon bilgisi
        SQ = ""
        SQ = "SELECT a_kid, a_id, a_bolum, a_satir, a_sutun, a_uzunluk, a_yasla, a_tip, a_alan, a_eboy, a_egen, a_efont, a_sifir"
        SQ = SQ & " FROM " & KUL.tfrm & "fisdizdet"
        SQ = SQ & " WHERE (a_kid = " & KID & ") AND (a_id = " & tur & ") AND (a_bolum = N'E')"
        SQ = SQ & " ORDER BY a_id, a_bolum, a_satir, a_sutun, a_uzunluk"
        B = SQL_TABLO(SQ, "e_yaz", False, False, False)

        If B.Rows.Count <= 0 Then
            'SORU("Uyarı", "Evrak dizay kaydı bulunamadı" & vbCrLf & "[Ayarlar] menüsünden [Matbu Form Tanımları]'na bakınız. ", "T")
            Exit Sub
        End If

        L = ""

        'Başlarken fark kadar ilerle
        A.Satir = Val(Trim(B.Rows(0).Item("a_satir").ToString))

        ES = CL + 1
        If ES <= 0 Then ES = 1

        For XL = 1 To A.Satir - ES
            CL = CL + 1
            SATIR_YAZ(Olusan, 1, "", bilgi)
        Next

        A.clear()

        'Fiş şablonu ilk bilgisi okunuyor.
        A.Alan = Trim(B.Rows(0).Item("a_alan").ToString)
        A.Bolum = Trim(B.Rows(0).Item("a_bolum").ToString)
        A.Satir = Val(Trim(B.Rows(0).Item("a_satir").ToString))
        A.Sutun = Val(Trim(B.Rows(0).Item("a_sutun").ToString))
        A.Tipi = Trim(B.Rows(0).Item("a_tip").ToString)
        A.Uzunluk = Val(Trim(B.Rows(0).Item("a_uzunluk").ToString))
        A.Yon = Trim(B.Rows(0).Item("a_yasla").ToString)
        A.Evrak_Boy = Val(Trim(B.Rows(0).Item("a_eboy").ToString))
        A.Evrak_En = Val(Trim(B.Rows(0).Item("a_egen").ToString))
        A.Evrak_Font = Val(Trim(B.Rows(0).Item("a_efont").ToString))
        A.Evrak_Sifir = Trim(B.Rows(0).Item("a_sifir").ToString)

        MAXL_A = A.Satir

        Try
            A.Alan = A_TOP.Deger(A.Alan)

            If A.Tipi = "Sayı" Then
                If Val(A.Alan) = 0 Then
                    A.Alan = A.Evrak_Sifir
                End If
            End If

            If A.Tipi = "Para" Then
                If Val(A.Alan) = 0 Then
                    A.Alan = A.Evrak_Sifir
                Else
                    A.Alan = Format(Val(A.Alan), AX.Evrak_KFormat)
                End If
            End If


            If A.Tipi = "Para2" Then
                If Val(A.Alan) = 0 Then
                    A.Alan = A.Evrak_Sifir
                Else
                    A.Alan = Format(Val(A.Alan), "###,###0.00")
                End If
            End If

            If A.Tipi = "Para3" Then
                If Val(A.Alan) = 0 Then
                    A.Alan = A.Evrak_Sifir
                Else
                    A.Alan = Format(Val(A.Alan), "###,###0.000")
                End If
            End If
            If A.Tipi = "Para4" Then
                If Val(A.Alan) = 0 Then
                    A.Alan = A.Evrak_Sifir
                Else
                    A.Alan = Format(Val(A.Alan), "###,###0.0000")
                End If
            End If
            If A.Tipi = "Para5" Then
                If Val(A.Alan) = 0 Then
                    A.Alan = A.Evrak_Sifir
                Else
                    A.Alan = Format(Val(A.Alan), "###,###0.00000")
                End If
            End If

            If A.Tipi = "Yazıyla" Then A.Alan = SOKU(A.Alan, Dov_Adi, Dov_Krs)
            If A.Tipi = "Saat" Then A.Alan = TTAR.SAAT
            If A.Tipi = "Tarih" Then A.Alan = TTAR.TARIH
            If A.Tipi = "SayfaNo" Then A.Alan = Str(Basilan_Sayfa_Nosu)
        Catch ex As Exception
            If A.Tipi = "Yazıyla" Then A.Alan = SOKU(A.Alan, Dov_Adi, Dov_Krs)
            If A.Tipi = "Saat" Then A.Alan = TTAR.SAAT
            If A.Tipi = "Tarih" Then A.Alan = TTAR.TARIH
            If A.Tipi = "SayfaNo" Then A.Alan = Str(Basilan_Sayfa_Nosu)
        End Try

        'İlk Satır oluşturuluyor.
        BOS = ""
        If A.Sutun > 1 Then
            BOS = New String(" ", A.Sutun - 1)
        End If
        L = L & BOS & Yasla(A.Alan, A.Uzunluk, A.Yon)

        ES = A.Satir
        ESu = A.Sutun
        EBo = A.Uzunluk


        For X = 1 To B.Rows.Count - 1

            'Mizansendeki Yeni bilgiler Tampona alınıyor.
            '------------------------------------------------------------------------------------
            A.clear()
            A.Alan = Trim(B.Rows(X).Item("a_alan").ToString)
            A.Bolum = Trim(B.Rows(X).Item("a_bolum").ToString)
            A.Satir = Val(Trim(B.Rows(X).Item("a_satir").ToString))
            A.Sutun = Val(Trim(B.Rows(X).Item("a_sutun").ToString))
            A.Tipi = Trim(B.Rows(X).Item("a_tip").ToString)
            A.Uzunluk = Val(Trim(B.Rows(X).Item("a_uzunluk").ToString))
            A.Yon = Trim(B.Rows(X).Item("a_yasla").ToString)
            A.Evrak_Boy = Val(Trim(B.Rows(X).Item("a_eboy").ToString))
            A.Evrak_En = Val(Trim(B.Rows(X).Item("a_egen").ToString))
            A.Evrak_Font = Val(Trim(B.Rows(X).Item("a_efont").ToString))
            A.Evrak_Sifir = Trim(B.Rows(X).Item("a_sifir").ToString)

            MAXL_A = A.Satir

            Try

                A.Alan = A_TOP.Deger(A.Alan)

                If A.Tipi = "Sayı" Then
                    If Val(A.Alan) = 0 Then
                        A.Alan = A.Evrak_Sifir
                    End If
                End If

                If A.Tipi = "Para" Then
                    If Val(A.Alan) = 0 Then
                        A.Alan = A.Evrak_Sifir
                    Else
                        A.Alan = Format(Val(A.Alan), AX.Evrak_KFormat)
                    End If
                End If

                If A.Tipi = "Para2" Then
                    If Val(A.Alan) = 0 Then
                        A.Alan = A.Evrak_Sifir
                    Else
                        A.Alan = Format(Val(A.Alan), "###,###0.00")
                    End If
                End If

                If A.Tipi = "Para3" Then
                    If Val(A.Alan) = 0 Then
                        A.Alan = A.Evrak_Sifir
                    Else
                        A.Alan = Format(Val(A.Alan), "###,###0.000")
                    End If
                End If
                If A.Tipi = "Para4" Then
                    If Val(A.Alan) = 0 Then
                        A.Alan = A.Evrak_Sifir
                    Else
                        A.Alan = Format(Val(A.Alan), "###,###0.0000")
                    End If
                End If
                If A.Tipi = "Para5" Then
                    If Val(A.Alan) = 0 Then
                        A.Alan = A.Evrak_Sifir
                    Else
                        A.Alan = Format(Val(A.Alan), "###,###0.00000")
                    End If
                End If

                If A.Tipi = "Yazıyla" Then A.Alan = SOKU(A.Alan, Dov_Adi, Dov_Krs)
                If A.Tipi = "Saat" Then A.Alan = TTAR.SAAT
                If A.Tipi = "Tarih" Then A.Alan = TTAR.TARIH
                If A.Tipi = "SayfaNo" Then A.Alan = Str(Basilan_Sayfa_Nosu)

            Catch ex As Exception
                If A.Tipi = "Yazıyla" Then A.Alan = SOKU(A.Alan, Dov_Adi, Dov_Krs)
                If A.Tipi = "Saat" Then A.Alan = TTAR.SAAT
                If A.Tipi = "Tarih" Then A.Alan = TTAR.TARIH
                If A.Tipi = "SayfaNo" Then A.Alan = Str(Basilan_Sayfa_Nosu)
            End Try


            'Yeni bir satıra geçiliyor
            'Satırı yaz satır tambonunu boşalt
            'Satır aralığını bul ve yaz
            'Satır işaretcisini ayarla
            If A.Satir <> ES Then
                CL = CL + 1
                SATIR_YAZ(Olusan, 1, L, bilgi)
                For XL = 1 To (A.Satir - ES) - 1
                    CL = CL + 1
                    SATIR_YAZ(Olusan, 1, "", bilgi)
                Next
                ES = A.Satir
                ESu = 0
                L = ""
            End If

            BOS = ""
            If ESu > 0 Then
                If (A.Sutun - ESu) - EBo > 0 Then
                    BOS = New String(" ", (A.Sutun - ESu) - EBo)
                End If
            Else
                If A.Sutun - ESu > 0 Then
                    BOS = New String(" ", A.Sutun - 1)
                End If
            End If

            L = L & BOS & Yasla(A.Alan, A.Uzunluk, A.Yon)
            ESu = A.Sutun
            EBo = A.Uzunluk

        Next

        CL = CL + 1
        SATIR_YAZ(Olusan, 1, L, bilgi)


    End Sub
    Public Function MUTFAK_YAZDIR(ByVal KID As Long, ByVal GKDOS As String, ByVal MasaID As Long, ByVal MutfakID As Long, ByVal ADS_ID As Long)

        Dim SQ As String
        Dim KDOS As String = ""
        Dim Y As Integer
        Dim X As Integer
        Dim Z As Integer

        Dim xB As New DataTable
        Dim tdc As New DataTable
        Dim XDT As New DataTable
        Dim TDT As New DataTable


        Dim OlusanStr As String = ""


        KDOS = Hexd2String(GKDOS)
        'KID = NULN(TDT, 0, "a_kid")



        'Fiş için geferekli olan verinin sql cümleri
        SQ = SQL_DATA(KDOS, ADS_ID, MutfakID)
        If SQ = "" Then
            'SORU("Uyarı", "Evrağa ait SQL kaynağı boş.", "T")
            Return OlusanStr
        End If


        DT = SQL_TABLO(SQ, "e_yaz", False, False, False)

        If DT.Rows.Count = 0 Then
            'SORU("Uyarı", "Yazdırılacak bilgi bulunamadı", "T")
            DT.Dispose()
            Return OlusanStr
        End If



        'Fişin x,y koordinat düzleminde girilen şablon bilgisi
        SQ = ""
        SQ = "SELECT a_kid, a_id, a_bolum, a_satir, a_sutun, a_uzunluk, a_yasla, a_tip, a_alan, a_eboy, a_egen, a_efont, a_sifir, a_kurus, a_ssonu"
        SQ = SQ & " FROM " & KUL.tfrm & "fisdizdet"
        SQ = SQ & " WHERE (a_kid = " & KID & ") AND (a_id = 92)"
        SQ = SQ & " ORDER BY a_id, a_bolum, a_satir, a_sutun, a_uzunluk"
        xB = SQL_TABLO(SQ, "e_yaz", False, False, False)

        If xB.Rows.Count <= 0 Then
            'SORU("Uyarı", "Evrak dizayn kaydı bulunamadı" & vbCrLf & "[Ayarlar] menüsünden [Matbu Form Tanımları]'na bakınız. ", "T")
            Try
                xB.Dispose()
            Catch ex As Exception
            End Try
            Return OlusanStr

        End If



        'Sayfa Sonu Mizansenini Okuyoruz.
        SQ = ""
        SQ = "SELECT a_alan"
        SQ = SQ & " FROM " & KUL.tfrm & "fisdizdet"
        SQ = SQ & " WHERE (a_bolum = N'E' OR"
        SQ = SQ & " a_bolum = N'D') AND (a_tip = N'Sayı' OR"
        SQ = SQ & " a_tip LIKE 'Para%') AND (a_id = 92 ) AND (a_kid = " & KID & ")"
        SQ = SQ & " GROUP BY a_alan"
        XDT = SQL_TABLO(B_SQL, SQ, "T", False, False, False)
        For Y = 0 To XDT.Rows.Count - 1
            A_TOP.ATop(Y).Adi = NULA(XDT, Y, "a_alan")
            A_TOP.ATop(Y).Top = 0
        Next


        AX.clear()
        AX.Evrak_Boy = Val(Trim(xB.Rows(0).Item("a_eboy").ToString))
        AX.Evrak_En = Val(Trim(xB.Rows(0).Item("a_egen").ToString))
        AX.Evrak_Font = Val(Trim(xB.Rows(0).Item("a_efont").ToString))
        AX.Evrak_Sifir = Trim(xB.Rows(0).Item("a_sifir").ToString)
        AX.Evrak_Kurus = Val(Trim(xB.Rows(0).Item("a_kurus").ToString))
        AX.Evrak_KFormat = "###,###0.00"

        Try
            AX.Evrak_Ssonu = Val(Trim(xB.Rows(0).Item("a_ssonu").ToString))
        Catch ex As Exception
            AX.Evrak_Ssonu = 0
        End Try


        If AX.Evrak_Kurus = -1 Then AX.Evrak_KFormat = "###,###"
        If AX.Evrak_Kurus = 0 Then AX.Evrak_KFormat = "###,###0.00"
        If AX.Evrak_Kurus = 1 Then AX.Evrak_KFormat = "###,###0.0"
        If AX.Evrak_Kurus = 2 Then AX.Evrak_KFormat = "###,###0.00"
        If AX.Evrak_Kurus = 3 Then AX.Evrak_KFormat = "###,###0.000"
        If AX.Evrak_Kurus = 4 Then AX.Evrak_KFormat = "###,###0.0000"
        If AX.Evrak_Kurus = 5 Then AX.Evrak_KFormat = "###,###0.00000"

        Try

            If AX.Evrak_Font <> 0 Then
                SATIR_YAZ(OlusanStr, 1, Chr(AX.Evrak_Font), False)
            End If


            'Kullanıcı Bazında Yazıcıya ilkleme Kodu Yollanıyor.
            If KUL.PRM_prnbas <> "" Then
                SATIR_YAZ(OlusanStr, 1, Chr(27) & Chr(Val(KUL.PRM_prnbas)), False)
            End If


            CL = 0

            SQ = ""
            SQ = "SELECT a_kid, a_id, a_bolum, a_satir"
            SQ = SQ & " FROM " & KUL.tfrm & "fisdizdet"
            SQ = SQ & " WHERE (a_kid = " & KID & ") AND (a_id = 92) AND (a_bolum = N'C')"
            SQ = SQ & " ORDER BY a_satir"
            tdc = SQL_TABLO(B_SQL, SQ, "T", False, False, False)
            ilk_c = NULN(tdc, 0, "a_satir")


            'Evrak Çıktı Prosedürü
            '--------------------------------------------------------------------------
            YAZDIR_ac(OlusanStr, KID, "A", 92, MasaID)
            YAZDIR_b(OlusanStr, KID, 92, MasaID)
            YAZDIR_ac(OlusanStr, KID, "C", 92, MasaID)
            '--------------------------------------------------------------------------

            If AX.Evrak_Boy > CL Then
                Z = CL
                For X = 1 To AX.Evrak_Boy - Z
                    CL = CL + 1
                    SATIR_YAZ(OlusanStr, 1, "", bilgi)
                Next
            End If

            'Kullanıcı Bazında Yazıcıya Sayfa Sonu Yolla
            If KUL.PRM_prnson = XL_Evet Then
                SATIR_YAZ(OlusanStr, 1, Chr(12), False)
            End If

        Catch ex As Exception
            'Call SORU("Uyarı", ex.Message, "T")
        End Try

        Return OlusanStr
    End Function


    Public Function YAZDIR(ByVal KID As Long, ByVal GKDOS As String, ByVal MasaID As Long, ByVal Ads_ID As Long)

        Dim SQ As String
        Dim KDOS As String = ""
        Dim Y As Integer
        Dim X As Integer
        Dim Z As Integer

        Dim xB As New DataTable
        Dim tdc As New DataTable
        Dim XDT As New DataTable
        Dim TDT As New DataTable


        Dim OlusanStr As String = ""


        KDOS = Hexd2String(GKDOS)
        'KID = NULN(TDT, 0, "a_kid")



        'Fiş için geferekli olan verinin sql cümleri
        SQ = SQL_DATA(KDOS, 92, Ads_ID)
        If SQ = "" Then
            'SORU("Uyarı", "Evrağa ait SQL kaynağı boş.", "T")
            Return OlusanStr
        End If


        DT = SQL_TABLO(SQ, "e_yaz", False, False, False)

        If DT.Rows.Count = 0 Then
            'SORU("Uyarı", "Yazdırılacak bilgi bulunamadı", "T")
            DT.Dispose()
            Return OlusanStr
        End If



        'Fişin x,y koordinat düzleminde girilen şablon bilgisi
        SQ = ""
        SQ = "SELECT a_kid, a_id, a_bolum, a_satir, a_sutun, a_uzunluk, a_yasla, a_tip, a_alan, a_eboy, a_egen, a_efont, a_sifir, a_kurus, a_ssonu"
        SQ = SQ & " FROM " & KUL.tfrm & "fisdizdet"
        SQ = SQ & " WHERE (a_kid = " & KID & ") AND (a_id = 92)"
        SQ = SQ & " ORDER BY a_id, a_bolum, a_satir, a_sutun, a_uzunluk"
        xB = SQL_TABLO(SQ, "e_yaz", False, False, False)

        If xB.Rows.Count <= 0 Then
            'SORU("Uyarı", "Evrak dizayn kaydı bulunamadı" & vbCrLf & "[Ayarlar] menüsünden [Matbu Form Tanımları]'na bakınız. ", "T")
            Try
                xB.Dispose()
            Catch ex As Exception
            End Try
            Return OlusanStr

        End If



        'Sayfa Sonu Mizansenini Okuyoruz.
        SQ = ""
        SQ = "SELECT a_alan"
        SQ = SQ & " FROM " & KUL.tfrm & "fisdizdet"
        SQ = SQ & " WHERE (a_bolum = N'E' OR"
        SQ = SQ & " a_bolum = N'D') AND (a_tip = N'Sayı' OR"
        SQ = SQ & " a_tip LIKE 'Para%') AND (a_id = 92 ) AND (a_kid = " & KID & ")"
        SQ = SQ & " GROUP BY a_alan"
        XDT = SQL_TABLO(B_SQL, SQ, "T", False, False, False)
        For Y = 0 To XDT.Rows.Count - 1
            A_TOP.ATop(Y).Adi = NULA(XDT, Y, "a_alan")
            A_TOP.ATop(Y).Top = 0
        Next


        AX.clear()
        AX.Evrak_Boy = Val(Trim(xB.Rows(0).Item("a_eboy").ToString))
        AX.Evrak_En = Val(Trim(xB.Rows(0).Item("a_egen").ToString))
        AX.Evrak_Font = Val(Trim(xB.Rows(0).Item("a_efont").ToString))
        AX.Evrak_Sifir = Trim(xB.Rows(0).Item("a_sifir").ToString)
        AX.Evrak_Kurus = Val(Trim(xB.Rows(0).Item("a_kurus").ToString))
        AX.Evrak_KFormat = "###,###0.00"

        Try
            AX.Evrak_Ssonu = Val(Trim(xB.Rows(0).Item("a_ssonu").ToString))
        Catch ex As Exception
            AX.Evrak_Ssonu = 0
        End Try


        If AX.Evrak_Kurus = -1 Then AX.Evrak_KFormat = "###,###"
        If AX.Evrak_Kurus = 0 Then AX.Evrak_KFormat = "###,###0.00"
        If AX.Evrak_Kurus = 1 Then AX.Evrak_KFormat = "###,###0.0"
        If AX.Evrak_Kurus = 2 Then AX.Evrak_KFormat = "###,###0.00"
        If AX.Evrak_Kurus = 3 Then AX.Evrak_KFormat = "###,###0.000"
        If AX.Evrak_Kurus = 4 Then AX.Evrak_KFormat = "###,###0.0000"
        If AX.Evrak_Kurus = 5 Then AX.Evrak_KFormat = "###,###0.00000"

        Try
            'Yazıcıya daraltma kodu yollanıyor.
            If AX.Evrak_Font <> 0 Then
                SATIR_YAZ(OlusanStr, 1, Chr(AX.Evrak_Font), False)
            End If


            'Kullanıcı Bazında Yazıcıya ilkleme Kodu Yollanıyor.
            If KUL.PRM_prnbas <> "" Then
                SATIR_YAZ(OlusanStr, 1, Chr(27) & Chr(Val(KUL.PRM_prnbas)), False)
            End If


            CL = 0

            SQ = ""
            SQ = "SELECT a_kid, a_id, a_bolum, a_satir"
            SQ = SQ & " FROM " & KUL.tfrm & "fisdizdet"
            SQ = SQ & " WHERE (a_kid = " & KID & ") AND (a_id = 92) AND (a_bolum = N'C')"
            SQ = SQ & " ORDER BY a_satir"
            tdc = SQL_TABLO(B_SQL, SQ, "T", False, False, False)
            ilk_c = NULN(tdc, 0, "a_satir")


            'Evrak Çıktı Prosedürü
            '--------------------------------------------------------------------------
            YAZDIR_ac(OlusanStr, KID, "A", 92, MasaID)
            YAZDIR_b(OlusanStr, KID, 92, MasaID)
            YAZDIR_ac(OlusanStr, KID, "C", 92, MasaID)
            '--------------------------------------------------------------------------

            If AX.Evrak_Boy > CL Then
                Z = CL
                For X = 1 To AX.Evrak_Boy - Z
                    CL = CL + 1
                    SATIR_YAZ(OlusanStr, 1, "", bilgi)
                Next
            End If

            'Kullanıcı Bazında Yazıcıya Sayfa Sonu Yolla
            If KUL.PRM_prnson = XL_Evet Then
                SATIR_YAZ(OlusanStr, 1, Chr(12), False)
            End If

        Catch ex As Exception
            'Call SORU("Uyarı", ex.Message, "T")
        End Try



        Return OlusanStr
    End Function
    Private Function SQL_DATA2(ByVal SQL As String, ByVal tur As String, ByVal id As Long) As String

        'Fiş için geferekli olan verinin sql cümleri

        Dim SQ As String
        Dim S As String
        'Dim KP As Integer
        'Dim X As Integer

        SQ = ""
        S = ""

        If SQL = "" Then
            SQL_DATA2 = ""
            Exit Function
        End If

        SQ = SQL

        SQ = SQ.Replace("" & KUL.tper & "", KUL.tper)
        SQ = SQ.Replace("tbl_001_", KUL.tfrm)

        SQ = SQ.Replace("<#01fisturu#>", Trim(tur))
        SQ = SQ.Replace("<#02fisnosu#>", Trim(Str(id)))
        SQ = SQ.Replace("<#03user#>", KUL.KOD)

        SQL_DATA2 = SQ


    End Function


 

End Class
