﻿Imports System.IO
Imports System.Data.OleDb
Imports System.Net.Sockets
Imports System.Net
Imports bym_dll



Module Module1
    Public Const DLL_ASSEMBLY As String = "1.5.17.64"



    ' DEĞİŞKENLER
    Dim DEPO_ID As Long = 0
    Dim KASA_ID As Long = 0
    Dim KASA_TUTAR As Double = 0
    Dim BANKA_ID As Long = 0
    Dim BANKA_TUTAR As Double = 0
    Dim USER_ID As Long = 0
    Dim MASA_ID As Long = 0
    Dim PERSONEL_ID As Long = 0
    Dim ADISYON_ID As Long = 0
    Dim VARDIYA_ID As Long = 0



    Dim PER_ID As Long = 0
    Dim USR_ID As Long = 0
    Dim PER_AD As String = ""
    Dim MAS_ID As Long = 0
    Dim KID As Long = 0
    Dim KTUT As Double = 0
    Dim BID As Long = 0
    Dim BTUT As Double = 0
    Dim ADSNO As Long = 0



    Public STR_MSG As String = ""


    Public DGV As New DataTable

    Public Const Buf512 As Integer = 512

    Public LTD As String = ""

    Public PORT As String = ""
    Public IP As String = ""

    Public PERS_ID As Long = 0

    Public PRNADI As String = ""
    Public PRNcm0 As Integer = 0
    Public PRNcm1 As String = ""
    Public PRNbek As Integer = 0

    Public HTUR As String = ""
    Public HGRUP As String = ""

    Public RAP_YOL As String = ""

    'Public BAGLANTI As OleDbConnection

    Public Const BufLen As Integer = 64000

    Public KONTOR As String = "" 'Kullanılabilir kontör adedi
    Public AKORIGIN As String = "" 'AKTİF OLAN ORIGIN BİLİGİSİNİ TUTAR.
    Public KULKOD As String = "" 'KULLANICI mesaj KODUNU TUTUYOR 
    Public KULSIFRE As String = "" 'KULLANICI mesaj ŞİFRESİNİ  TUTUYOR 


    Public Const Tdin As Integer = 32
    Public Const TSaniye As Integer = 1
    Public TPoint As Integer = 0
    Public DINLE(Tdin) As System.Threading.Thread
    Public MASA_AT As System.Threading.Thread
    Public MASA_SIL As System.Threading.Thread

    Public KOMUT As OleDb.OleDbCommand


    Declare Auto Function PlaySound Lib "winmm.dll" (ByVal name _
   As String, ByVal hmod As Integer, ByVal flags As Integer) As Integer
    ' name specifies the sound file when the SND_FILENAME flag is set.
    ' hmod specifies an executable file handle.
    ' hmod must be Nothing if the SND_RESOURCE flag is not set.
    ' flags specifies which flags are set. 

    ' The PlaySound documentation lists all valid flags.
    Public Const SND_SYNC = &H0          ' play synchronously
    Public Const SND_ASYNC = &H1         ' play asynchronously
    Public Const SND_FILENAME = &H20000  ' name is file name
    Public Const SND_RESOURCE = &H40004  ' name is resource name or atom

    Public Function S2D(ByVal D As Double) As String

        If D = 0 Then
            S2D = "0"
            Exit Function
        End If

        Dim S As String = ""

        S = Str(D)
        S = S.Replace(",", ".")

        If S = "" Then S = "0"

        S2D = S

    End Function

    Public Sub SetText(ByVal M As String)


        STR_MSG = M

        'Soket Hatası :
        If Mid(M, 1, Len("Soket Hatası :")) = "Soket Hatası :" Then
            Try
                Dim FF As New Integer
                FF = FreeFile()
                FileOpen(FF, KUL.tyol & "LOG.TXT", OpenMode.Append)
                WriteLine(FF, "S")
                FileClose(FF)
            Catch ex As Exception
            End Try

            Exit Sub
        End If


        Try
            Dim FF As New Integer
            FF = FreeFile()
            FileOpen(FF, KUL.tyol & "LOG.TXT", OpenMode.Append)
            WriteLine(FF, M)
            FileClose(FF)
        Catch ex As Exception
        End Try

    End Sub


    Public Function PVersiyon() As String
        Dim VMajor As Integer
        Dim VMinor As Integer
        Dim VRevision As Integer
        Dim Version As String

        VMajor = System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly.Location).FileMajorPart()
        VMinor = System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly.Location).FileMinorPart()
        VRevision = System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly.Location).FileBuildPart()

        Version = System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly.Location).ProductVersion()

        PVersiyon = Str(VMajor) & "." & Str(VMinor) & "." & Str(VRevision)


    End Function

    Public Sub YUKLE()

        Dim SQ As String = ""
        Dim DT As New DataTable
        Dim X As Integer = 0

        SQ = ""
        SQ = "SELECT a_kulkod, a_kulsifre, a_kontor, a_madisoyadi, a_msjbas1, a_msjbas2, a_msjbas3, a_msjbas4, a_msjbas5, a_msjbas6, a_msjbas7, a_msjbas8, "
        SQ = SQ & " a_msjbas9, a_msjbas10, a_msjbas11, a_msjbas12, a_msjbas13, a_msjbas14, a_msjbas15, a_msjbas16, a_msjbas17, a_msjbas18, a_msjbas19, a_msjbas20"
        SQ = SQ & " FROM tbl_smsayar"
        DT = SQL_TABLO(B_SQL, SQ, "T", False, False, False)

        If DT.Rows.Count > 0 Then

            KONTOR = NULN(DT, 0, "a_kontor")

            AKORIGIN = NULA(DT, 0, "a_msjbas1")

            KULKOD = NULA(DT, 0, "a_kulkod")

            KULSIFRE = NULA(DT, 0, "a_kulsifre")

        End If

    End Sub


    Public Function Max_Bul(ByVal Tur As Long) As Long
        Dim SQ As String = ""
        Dim DT As New DataTable
        Dim EB As Long = 0

        '1- Adisyon No
        '2- Silinen No

        SQ = ""
        SQ = "SELECT a_tur, a_sayac"
        SQ = SQ & " FROM " & KUL.tfrm & "men_max_id"
        SQ = SQ & " WHERE a_tur = " & Tur & ""
        DT = SQL_TABLO(SQ, "T", False, False, False)

        If DT.Rows.Count = 0 Then
            SQ = ""
            SQ = "INSERT INTO " & KUL.tfrm & "men_max_id (a_tur, a_sayac)"
            SQ = SQ & " VALUES "
            SQ = SQ & " ("
            SQ = SQ & " " & Tur & ","
            SQ = SQ & " " & 1 & ""
            SQ = SQ & " )"
            SQL_KOS(SQ, False)
            Max_Bul = 1
        Else
            EB = NULA(DT, 0, "a_sayac")
            EB = EB + 1
            SQ = "UPDATE " & KUL.tfrm & "men_max_id "
            SQ = SQ & " SET "
            SQ = SQ & " a_sayac = " & EB & ""
            SQ = SQ & " WHERE a_tur = " & Tur & ""
            SQL_KOS(SQ, False)
            Max_Bul = EB
        End If


    End Function

    ''' <summary>
    ''' Gönderilen Parametrelere Göre Adisyona Nakit Tahsilat Ekler
    ''' </summary>
    ''' <param name="MASA_ID">Eklenen Masa ID</param>
    ''' <param name="ADISYON_ID">Eklenen Adisyon ID</param>
    ''' <param name="ODEME_TUR">Ödeme Türü (KASA / BANKA)</param>
    ''' <param name="YER_ID">Ödeme ID (KASA ID / BANKA ID)</param>
    ''' <param name="TUTAR">Ödeme Tutarı</param>
    Public Sub MEN_TAHSILAT(ByVal MASA_ID As Long, ByVal ADISYON_ID As Long, ByVal ODEME_TUR As String, ByVal YER_ID As Long, ByVal TUTAR As Double)
        Dim tahsilatEkleSql As String = ""
        Dim siraBulSql As String = ""
        Dim dt_sira As New DataTable
        Dim sira As Integer = 0

        ' Tahsilat Sıra No Bulunuyor
        siraBulSql = "SELECT MAX(a_sira) AS siraNo"
        siraBulSql = siraBulSql & " FROM " & KUL.tfrm & "men_masa_tahsil"
        siraBulSql = siraBulSql & " WHERE (a_masid = " & MASA_ID & ") AND (a_master_id = " & ADISYON_ID & ")"
        dt_sira = SQL_TABLO(siraBulSql, "T", False, False, False)
        sira = NULN(dt_sira, 0, "siraNo") + 1

        ' Tahsilat Adisyona Ekleniyor
        tahsilatEkleSql = "INSERT INTO " & KUL.tfrm & "men_masa_tahsil (a_masid, a_sira, a_turad, a_id, a_tutar, a_master_id)"
        tahsilatEkleSql = tahsilatEkleSql & " VALUES "
        tahsilatEkleSql = tahsilatEkleSql & " ( "
        tahsilatEkleSql = tahsilatEkleSql & " " & MASA_ID & ","
        tahsilatEkleSql = tahsilatEkleSql & " " & sira & ","
        tahsilatEkleSql = tahsilatEkleSql & " '" & ODEME_TUR & "',"
        tahsilatEkleSql = tahsilatEkleSql & " " & YER_ID & ","
        tahsilatEkleSql = tahsilatEkleSql & " " & TUTAR & ","
        tahsilatEkleSql = tahsilatEkleSql & " " & ADISYON_ID & ""
        tahsilatEkleSql = tahsilatEkleSql & " ) "
        Call SQL_KOS(tahsilatEkleSql, False)
    End Sub


    ''' <summary>
    ''' Gönderilen Belge ID'sine Göre Banka Tahsilat Belgesi Ekliyor.
    ''' </summary>
    ''' <param name="kesin"></param>
    ''' <param name="BANKA_ID">Banka ID</param>
    ''' <param name="TAKSIT_SAYISI">Taksit Sayısı</param>
    ''' <param name="TUTAR">İşlenecek Tutar</param>
    ''' <param name="FATURA_ID">Fatura ID</param>
    ''' <param name="USER_ID">Kullanıcı ID</param>
    ''' <param name="MASA_ID">Masa ID</param>
    ''' <param name="ADISYON_ID">Adisyon ID</param>
    ''' <param name="ADID">Adisyon Kapat ADID</param>
    ''' <param name="ADBOL">Sabit 1 Geliyor</param>
    ''' <param name="VARDIYA_ID">Kullanıcı Vardiya ID</param>
    Public Sub BANKAYA_TAHSIL(ByVal kesin As Integer, ByVal BANKA_ID As Long, ByVal TAKSIT_SAYISI As Double, ByVal TUTAR As Double, ByVal FATURA_ID As Long, ByVal USER_ID As Long, ByVal MASA_ID As Long, ByVal ADISYON_ID As Long, ByVal ADID As Long, ByVal ADBOL As Long, ByVal VARDIYA_ID As Long)
        'FATID = FATURANIN IDSIDIR.
        'PUANLI ÖDEME İÇİN DEĞİŞTİRİLDİ.31.05.2007 OZAN 
        'CB    : HANGİ COMBOBOXA BAKARAK İŞLEM YAPACAK 
        'BN    : SEÇİLEN BANKA GELECEK 
        'TSAYI  : TAKSİT SAYISI GELECEK 
        'TUTAR : KREDİ KARTLI SATIŞIN NE KADARLIK YAPILDIĞI GELECEK 

        Dim SQ As String
        Dim TUR As Long
        Dim CARI As Long
        Dim B_ID As Long
        Dim DT As New DataTable

        Dim DTX As New DataTable
        Dim EBN As Long = 0



        Dim TEKODE As Double        'Tek ödemede uygulanak oran
        Dim TAKSITODE As Double = 0 'Taksitli ödemede uygulanacak oran 
        Dim SERVİS As Double        'Servis ve kdv li oranı 
        Dim GUNSAY As Long          'ÖDEMENİN KAÇ GÜN SONRA HESABE GEÇECEĞİ
        Dim KOMTUT As Double        'KESİNTİLERİN TOPLAM TUTARI
        Dim TAKSITOR As Double = 0  'TASIT ORANI TUTULUYOR

        Dim TS As Long = 0 ' TAKSİT SAYISINI TUTACAK 

        CARI = 1

        TS = TAKSIT_SAYISI


        '*************************************************************************************
        TEKODE = Val(VT_BILGI(KUL.tfrm & "banka", "a_id", "a_tekor", BANKA_ID))
        SERVİS = Val(VT_BILGI(KUL.tfrm & "banka", "a_id", "a_serkdv", BANKA_ID))
        GUNSAY = Val(VT_BILGI(KUL.tfrm & "banka", "a_id", "a_hesgun", BANKA_ID))
        KOMTUT = kurus((((Val(V_SIL(TUTAR)) * (TEKODE + SERVİS))) / 100), 2)

        '**********************************************************************************
        If kesin = 1 Then
            '******* 1-)İlk Önce ilgili banka hesabına alış-veriş tutarı borç bakiyesine işleniyor. ********

            TUR = 65

            B_ID = TSAY.MAX_FIS_BANKA(TUR, 1)

            SQ = ""
            SQ = "INSERT INTO " & KUL.tper & "nakitgc ( a_tur, a_id, a_gc, a_banka_id, a_islemno, a_belgeno, a_ref_id, a_tarih, a_ack, a_mebla, a_yer, a_yerid, a_yertur, a_cuser, a_cdate, a_ctime, a_muser, a_mdate, a_mtime, a_var_id )"
            SQ = SQ & " SELECT "
            SQ = SQ & " " & TUR & " AS a_tur, "
            SQ = SQ & " " & B_ID & " AS a_id, "
            SQ = SQ & " 'G' AS a_gc, "
            SQ = SQ & " " & BANKA_ID & " AS a_banka_id, "
            SQ = SQ & " 'KKS-" & B_ID & "' AS a_islemno, "
            SQ = SQ & " 'KKS-" & B_ID & "' AS a_belgeno, "
            SQ = SQ & "  " & CARI & "  AS a_ref_id, "

            Dim A As Date
            Dim S As String
            A = TTAR.TARIH
            A = DateAdd(DateInterval.Day, GUNSAY, A)
            S = A

            SQ = SQ & " '" & TTAR.TR2UK(S) & "' AS a_tarih, "
            SQ = SQ & " '" & ADID & "-" & ADBOL & " Nolu Faturanın Kredi Kartı İle Ödemesi' AS a_ack, "
            SQ = SQ & " " & S2D(TUTAR) & " AS a_mebla, "
            SQ = SQ & " 'FATURA' AS a_yer, "
            SQ = SQ & " " & FATURA_ID & " AS a_yerid, "
            SQ = SQ & " 10 AS a_yertur, "
            SQ = SQ & " '" & USER_ID & "' AS a_cuser, "
            SQ = SQ & " '" & TTAR.TR2UK(Date.Now.ToShortDateString).ToString & "' AS a_cdate, "
            SQ = SQ & " '" & Date.Now.ToShortTimeString & "' AS a_ctime, "
            SQ = SQ & " '" & KUL.KOD & "' AS a_muser, "
            SQ = SQ & " '" & TTAR.TR2UK(Date.Now.ToShortDateString).ToString & "' AS a_mdate, "
            SQ = SQ & " '" & Date.Now.ToShortTimeString & "' AS a_mtime,"
            SQ = SQ & " " & VARDIYA_ID & " AS a_var_id;"
            If SQL_KOS(SQ) = True Then

                SQ = ""
                SQ = "SELECT MAX(a_sira) AS EB"
                SQ = SQ & " FROM " & KUL.tper & "faturakapat"
                SQ = SQ & " WHERE(a_ftur = " & 10 & ") AND (a_fid = " & FATURA_ID & ")"
                DTX = SQL_TABLO(SQ, "T", False, False, False)
                EBN = NULN(DTX, 0, "EB") + 1

                SQ = "INSERT INTO " & KUL.tper & "faturakapat (a_ftur, a_fid, a_sira, a_evrak, a_refid, a_etur, a_eid, a_cstur, a_masterid, a_cihazid, a_tarih, a_ack, a_tutar)"
                SQ = SQ & " VALUES "
                SQ = SQ & " ( "
                SQ = SQ & " " & 10 & ", "
                SQ = SQ & " " & FATURA_ID & ", "
                SQ = SQ & " " & EBN & ", "
                SQ = SQ & " '" & "BANKA" & "', "
                SQ = SQ & " " & BANKA_ID & ", "
                SQ = SQ & " " & TUR & ", "
                SQ = SQ & " " & B_ID & ", "
                SQ = SQ & " '" & "" & "', "
                SQ = SQ & " " & 0 & ", "
                SQ = SQ & " " & 0 & ", "
                SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & "' , "
                SQ = SQ & " '" & "Banka Tahsilat Fatura İçin" & "', "
                SQ = SQ & " " & S2D(TUTAR) & " "
                SQ = SQ & " ) "
                SQL_KOS(SQ, False)

                BG.banka_guncelle(BANKA_ID, TUTAR, "B+")


                '************  1-) ilk işlemin sonu  ************************************
                '******* 2-)2. işlem olarak ilgili bankanın hesabına komisyon tutarını ödüyoruz . ****
                TUR = 23
                Dim TERS As New TERS_ISLEM

                TERS.T03_T_ID = CARI
                TERS.T04_ISLEM_NO = "KT-" & B_ID
                TERS.T05_BELGE_NO = "KT-" & B_ID
                TERS.T06_REF_ID = CARI
                TERS.T07_TARIH = TTAR.TARIH
                TERS.T08_ACK = ADID & "-" & ADBOL & "  Nolu Faturanın Kredi Kartı İle Ödeme"
                TERS.T09_MEBLA = TUTAR
                TERS.T10_YER = "FATURA"
                TERS.T11_YER_ID = FATURA_ID
                TERS.T12_YER_TUR = 10
                If TERS.CARI_EKLE(TUR) = True Then
                    '*****************************************************************************************
                    BG.cari_guncelle(CARI, TUTAR, "A+")
                    '************  2-) ikinci işlemin sonu  ************************************
                    '************* 3-) 3.işlem olarak cari hesabın borcu kalmadığı için cari hesap alacak dekontu kesiyoruz.
                    If KOMTUT <> 0 Then

                        TUR = 66
                        B_ID = TSAY.MAX_FIS_BANKA(TUR, 1)
                        SQ = ""
                        SQ = "INSERT INTO " & KUL.tper & "nakitgc ( a_tur, a_id, a_gc, a_banka_id, a_islemno, a_belgeno, a_ref_id, a_tarih, a_ack, a_mebla, a_yer, a_yerid, a_yertur, a_cuser, a_cdate, a_ctime, a_muser, a_mdate, a_mtime )"
                        SQ = SQ & " SELECT "
                        SQ = SQ & " " & TUR & " AS a_tur, "
                        SQ = SQ & " " & B_ID & " AS a_id, "
                        SQ = SQ & " 'C' AS a_gc, "
                        SQ = SQ & " " & BANKA_ID & " AS a_banka_id, "
                        SQ = SQ & " 'KKS-" & B_ID & "' AS a_islemno, "
                        SQ = SQ & " 'KKS-" & B_ID & "' AS a_belgeno, "
                        SQ = SQ & "  " & CARI & "  AS a_ref_id, "
                        SQ = SQ & " '" & TTAR.TR2UK(S).ToString & "' AS a_tarih, "
                        SQ = SQ & " '" & ADID & "-" & ADBOL & "  Nolu Fatura Kredi Kartı Komisyon Tutarı ' AS a_ack, "
                        SQ = SQ & " " & S2D(KOMTUT) & " AS a_mebla, "
                        SQ = SQ & " 'FATURA' AS a_yer, "
                        SQ = SQ & " " & FATURA_ID & " AS a_yerid, "
                        SQ = SQ & " 10 AS a_yertur, "
                        SQ = SQ & " '" & KUL.KOD & "' AS a_cuser, "
                        SQ = SQ & " '" & TTAR.TR2UK(Date.Now.ToShortDateString).ToString & "' AS a_cdate, "
                        SQ = SQ & " '" & Date.Now.ToShortTimeString & "' AS a_ctime, "
                        SQ = SQ & " '" & KUL.KOD & "' AS a_muser, "
                        SQ = SQ & " '" & TTAR.TR2UK(Date.Now.ToShortDateString).ToString & "' AS a_mdate, "
                        SQ = SQ & " '" & Date.Now.ToShortTimeString & "' AS a_mtime;"
                        If SQL_KOS(SQ) = True Then
                            BG.banka_guncelle(BANKA_ID, KOMTUT, "A+")
                            '************  3-) üçüncü işlemin sonu  ************************************
                            '************  4-) 4.işlem komtut update ediliyor. ************************
                            SQ = ""
                            SQ = "UPDATE " & KUL.tper & "stkcmas"
                            SQ = SQ & " SET "
                            SQ = SQ & " a_komtut = " & S2D(KOMTUT) & ""
                            SQ = SQ & " WHERE a_tur = 10 AND a_id = " & FATURA_ID & ""
                            SQL_KOS(SQ)
                            '************  4-) 4.işlem işlemin sonu. *********************************
                        End If
                    End If
                End If
            End If
        End If

    End Sub


    ''' <summary>
    ''' Gönderilen Belge ID'sine Göre Kasa Tahsilat Belgesi Ekliyor.
    ''' </summary>
    ''' <param name="kesin"></param>
    ''' <param name="KASA_ID">İşlenecek Kasa ID</param>
    ''' <param name="TUTAR">Hareket Tutarı</param>
    ''' <param name="FATURA_ID">Fatura ID</param>
    ''' <param name="USER_ID">Kullanıcı ID</param>
    ''' <param name="MASA_ID">Masa ID</param>
    ''' <param name="ADISYON_ID">Adisyon ID</param>
    ''' <param name="ADID">Adisyon Kapat ADID</param>
    ''' <param name="ADBOL">Sabit 1 Geliyor</param>
    ''' <param name="VARDIYA_ID">Kullanıcı Vardiya ID</param>
    Public Sub KASAYA_TAHSIL(ByVal kesin As Integer, ByVal KASA_ID As Long, ByVal TUTAR As Double, ByVal FATURA_ID As Long, ByVal USER_ID As Long, ByVal MASA_ID As Long, ByVal ADISYON_ID As Long, ByVal ADID As Long, ByVal ADBOL As Long, ByVal VARDIYA_ID As Long)
        'FATID  : FATURANIN IDSINI TUTUYORUZ. 
        'KS     :  HANGİ KASA SEÇİLMİŞSE ONU GİRECEĞİZ
        'TUTAR  : NE KADARLIK KASA TASIL YAPILACAKSA O GELECEK  
        'PUAN LI ÖDEME İÇİN DEĞİŞTİRİLİYOR 31.05.2007 OZAN


        Dim SQ As String
        Dim TUR As Long
        Dim CARI As Long
        Dim ID As Long

        Dim DTX As New DataTable
        Dim EBN As Long = 0

        CARI = 1
        TUR = 29

        If kesin = 1 Then

            'Kasa Tahsilat Fişi

            ID = TSAY.MAX_FIS_KASA(TUR, 1)

            SQ = ""
            SQ = "INSERT INTO " & KUL.tper & "nakitgc ( a_tur, a_id, a_gc, a_kasa_id, a_islemno, a_belgeno, a_ref_id, a_tarih, a_ack, a_mebla, a_yer, a_yerid, a_yertur, a_cuser, a_cdate, a_ctime, a_muser, a_mdate, a_mtime, a_var_id )"
            SQ = SQ & " SELECT "
            SQ = SQ & " " & TUR & " AS a_tur, "
            SQ = SQ & " " & ID & " AS a_id, "
            SQ = SQ & " 'G' AS a_gc, "
            SQ = SQ & " " & KASA_ID & " AS a_kasa_id, "
            SQ = SQ & " 'KT-" & ID & "' AS a_islemno, "
            SQ = SQ & " 'KT-" & ID & "' AS a_belgeno, "
            SQ = SQ & "  " & CARI & "  AS a_ref_id, "
            SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH).ToString & "' AS a_tarih, "
            SQ = SQ & " '" & ADID & "-" & ADBOL & " Fatura karşılığı alınan' AS a_ack, "
            SQ = SQ & " " & S2D(TUTAR) & " AS a_mebla, "
            SQ = SQ & " 'FATURA' AS a_yer, "
            SQ = SQ & " " & FATURA_ID & " AS a_yerid, "
            SQ = SQ & " 10 AS a_yertur, "
            SQ = SQ & " '" & USER_ID & "' AS a_cuser, "
            SQ = SQ & " '" & TTAR.TR2UK(Date.Now.ToShortDateString).ToString & "' AS a_cdate, "
            SQ = SQ & " '" & Date.Now.ToShortTimeString & "' AS a_ctime, "
            SQ = SQ & " '" & KUL.KOD & "' AS a_muser, "
            SQ = SQ & " '" & TTAR.TR2UK(Date.Now.ToShortDateString).ToString & "' AS a_mdate, "
            SQ = SQ & " '" & Date.Now.ToShortTimeString & "' AS a_mtime,"
            SQ = SQ & " " & VARDIYA_ID & " AS a_var_id;"
            If SQL_KOS(SQ) = True Then

                ' Fatura Kapat Sıra No Alıyor
                SQ = ""
                SQ = "SELECT MAX(a_sira) AS EB"
                SQ = SQ & " FROM " & KUL.tper & "faturakapat"
                SQ = SQ & " WHERE(a_ftur = " & 10 & ") AND (a_fid = " & FATURA_ID & ")"
                DTX = SQL_TABLO(SQ, "T", False, False, False)
                EBN = NULN(DTX, 0, "EB") + 1

                ' Fatura Kapat Kaydı Ekleniyor
                SQ = "INSERT INTO " & KUL.tper & "faturakapat (a_ftur, a_fid, a_sira, a_evrak, a_refid, a_etur, a_eid, a_cstur, a_masterid, a_cihazid, a_tarih, a_ack, a_tutar)"
                SQ = SQ & " VALUES "
                SQ = SQ & " ( "
                SQ = SQ & " " & 10 & ", "
                SQ = SQ & " " & FATURA_ID & ", "
                SQ = SQ & " " & EBN & ", "
                SQ = SQ & " '" & "KASA" & "', "
                SQ = SQ & " " & KASA_ID & ", "
                SQ = SQ & " " & TUR & ", "
                SQ = SQ & " " & ID & ", "
                SQ = SQ & " '" & "" & "', "
                SQ = SQ & " " & 0 & ", "
                SQ = SQ & " " & 0 & ", "
                SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & "' , "
                SQ = SQ & " '" & "Kasa Tahsilat Fatura İçin" & "', "
                SQ = SQ & " " & S2D(TUTAR) & " "
                SQ = SQ & " ) "
                SQL_KOS(SQ, False)


                '**************************YAPILAN İŞLEMİN TERS İŞLEMİ *************************
                '*****yapılan işlem cari hesaba olduğu için cari hesaba ilgii fiş otomatik kesiliyor.
                Dim TERS As New TERS_ISLEM

                TERS.T03_T_ID = CARI
                TERS.T04_ISLEM_NO = "KT-" & ID
                TERS.T05_BELGE_NO = "KT-" & ID
                TERS.T06_REF_ID = CARI
                TERS.T07_TARIH = TTAR.TARIH
                TERS.T08_ACK = ADID & "-" & ADBOL & " Fatura karşılığı alınan"
                TERS.T09_MEBLA = TUTAR
                TERS.T10_YER = "FATURA"
                TERS.T11_YER_ID = FATURA_ID
                TERS.T12_YER_TUR = 10

                If TERS.EKLE(TUR) = True Then
                    '*****************************************************************************************
                    BG.cari_guncelle(CARI, TUTAR, "A+")
                    BG.kasa_guncelle(KASA_ID, TUTAR, "B+")
                End If

            End If

        End If


        If kesin = 0 Then
            BG.cari_guncelle(CARI, TUTAR, "A-")
            BG.kasa_guncelle(KASA_ID, TUTAR, "B-")
        End If


    End Sub

    Public Sub Masa_Tasi(ByVal Veri As String)

        Dim P1 As Integer = 0
        Dim P2 As Integer = 0
        Dim P3 As Integer = 0

        Dim m1 As Integer = 0
        Dim m2 As Integer = 0
        Dim a1 As Long = 0

        Dim SQ As String = ""
        Dim DT As New DataTable
        Dim DTA As New DataTable
        Dim X As Integer = 0

        '"MTASI M1=" & M1 & " M2=" & M2

        P1 = InStr(Veri, "M1=")
        P2 = InStr(Veri, "M2=")
        P3 = InStr(Veri, "A1=")

        m1 = Val(Trim(Mid(Veri, P1 + 3, P2 - (P1 + 3))))
        m2 = Val(Trim(Mid(Veri, P2 + 3, P3 - (P2 + 3))))
        a1 = Val(Trim(Mid(Veri, P3 + 3, Len(Veri))))


        SQ = ""
        SQ = "SELECT a_id, a_kod, a_adi, a_durum, a_acsaat, a_cariid, a_kisisay, a_adsnosu"
        SQ = SQ & " FROM " & KUL.tfrm & "men_masa"
        SQ = SQ & " WHERE(a_id = " & m1 & ")"
        DT = SQL_TABLO(SQ, "VERI", False, False, False)



        SQ = "Update " & KUL.tfrm & "men_masa"
        SQ = SQ & " SET a_durum = 'DOLU'"
        SQ = SQ & "  , a_acsaat = '" & TTAR.TR2UK(TTAR.TARIH) & " " & Now.ToShortTimeString & "'"
        SQ = SQ & "  , a_kisisay = " & NULN(DT, 0, "a_kisisay") & " "
        SQ = SQ & "  , a_adsnosu = " & NULN(DT, 0, "a_adsnosu") & ""
        SQ = SQ & "  , a_cariid = " & NULN(DT, 0, "a_cariid") & " "
        SQ = SQ & " WHERE(a_id = " & m2 & ")"
        Call SQL_KOS(SQ, False)


        SQ = ""
        SQ = "SELECT a_id ,a_mid"
        SQ = SQ & " FROM " & KUL.tfrm & "men_adisyonmas"
        SQ = SQ & " WHERE a_mid = " & m1 & " AND a_durum=0"
        SQ = SQ & " AND a_id <> " & a1
        DTA = SQL_TABLO(SQ, "VERI", False, False, False)

        If DTA.Rows.Count = 0 Then
            SQ = "Update " & KUL.tfrm & "men_masa"
            SQ = SQ & " SET a_durum = 'BOŞ'"
            SQ = SQ & "  , a_adsbas = 0"
            SQ = SQ & "  , a_acsaat = '01.01.1900 00:00:00' "
            SQ = SQ & "  , a_kisisay = " & 0 & " "
            SQ = SQ & "  , a_adsnosu = " & 0 & ""
            SQ = SQ & "  , a_cariid = " & 0 & " "
            SQ = SQ & " WHERE(a_id = " & m1 & ")"
            Call SQL_KOS(SQ, False)
        End If

        SQ = "UPDATE " & KUL.tfrm & "men_adisyon "
        SQ = SQ & " SET "
        SQ = SQ & " a_mid = " & m2 & ""
        SQ = SQ & " WHERE a_master_id=" & a1
        Call SQL_KOS(SQ, False)

        SQ = "UPDATE " & KUL.tfrm & "men_adisyonmas "
        SQ = SQ & " SET "
        SQ = SQ & " a_mid = " & m2 & ""
        SQ = SQ & " WHERE a_id=" & a1
        Call SQL_KOS(SQ, False)


    End Sub

    Public Sub Masa_Adisyon(ByVal Veri As String)
        Dim SQ As String
        Dim TDT As New DataTable
        Dim P1 As Integer = 0
        Dim P2 As Integer = 0
        Dim P3 As Integer = 0
        Dim P4 As Integer = 0

        Dim KID As Long = 0
        Dim PS As New sinifmatbu
        Dim KDOS As String = ""
        Dim B As String = ""
        Dim SUBEIDD As Integer = 0
        Dim MASTER As Long = 0
        Dim YAZICIYOLU As String = ""
        Dim Masa_Id As Long = 0

        P1 = InStr(Veri, "MASA=")
        P2 = InStr(Veri, "PDA=")
        P3 = InStr(Veri, "GARSON=") 'personel id
        P4 = InStr(Veri, "MASTER=")

        P4 = InStr(Veri, "MASTER=")

        MASTER = Val(Trim(Mid(Veri, P4 + 7, Len(Veri))))

        Masa_Id = Val(Trim(Mid(Veri, P1 + 5, P2 - (P1 + 5))))


        SQ = ""
        SQ = "SELECT a_kid, a_tur, a_sql, a_aciklama, a_ondeger, a_yazdosya"
        SQ = SQ & " FROM " & KUL.tfrm & "fisdizmas"
        SQ = SQ & " WHERE(a_tur =92) AND a_aciklama='ADISYON'"
        TDT = SQL_TABLO(SQ, "T", False, False, False)

        If TDT.Rows.Count = 0 Then
            Call Adisyon_Yazdir(Veri)
        Else


            KID = NULN(TDT, 0, "a_kid")
            KDOS = NULA(TDT, 0, "a_sql")
            B = PS.YAZDIR(KID, KDOS, Masa_Id, MASTER)

            B = TURK(B, 0)



            SQ = ""
            SQ = "SELECT     Men_MasaYer.a_yazyol,Men_MasaYer.a_sube"
            SQ = SQ & " " & "FROM  " & KUL.tfrm & "men_masa AS Men_Masa INNER JOIN"
            SQ = SQ & " " & KUL.tfrm & "men_masayer AS Men_MasaYer ON Men_Masa.a_yerid = Men_MasaYer.a_id"
            SQ = SQ & " " & "WHERE     (Men_Masa.a_id = " & Masa_Id & ") "
            TDT = SQL_TABLO(SQ, "d", False, False, False)

            If NULA(TDT, 0, "a_yazyol") <> "" Then
                YAZICIYOLU = NULA(TDT, 0, "a_yazyol")
            Else
                YAZICIYOLU = "YAZ.BAT"
            End If

            Dim K As String = ""
            K = KUL.KOD & "-" & Now.Year & "-" & Now.Month & "-" & Now.Day & " " & Now.Hour & ":" & Now.Minute & ":" & Now.Second & ":" & sifre_uret(8)

            SQ = ""
            SQ = "INSERT INTO " & KUL.tfrm & "men_pserver (a_id, a_tur, a_pda, a_user, a_tarih, a_saat, a_komut, a_veri, a_durum, a_sube, a_masa)"
            SQ = SQ & " VALUES "
            SQ = SQ & " ( "
            SQ = SQ & " '" & K & "', "
            SQ = SQ & " '" & "ADİSYON" & "', "
            SQ = SQ & " '" & "" & "', "
            SQ = SQ & "  " & Val(KUL.KOD) & " , "
            SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & "', "
            SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & " " & TTAR.SAAT & "', "
            SQ = SQ & " '" & YAZICIYOLU & "', "
            SQ = SQ & " '" & B & "', "
            SQ = SQ & " '" & "H" & "', "
            SQ = SQ & "  " & NULN(TDT, 0, "a_sube") & " , "
            SQ = SQ & "  " & Masa_Id & "  "
            SQ = SQ & " ) "

            Call SQL_KOS(SQ, False)

            SQ = ""
            SQ = "UPDATE " & KUL.tfrm & "men_masa"
            SQ = SQ & " SET "
            SQ = SQ & " a_adsbas = 1"
            SQ = SQ & " WHERE(a_id = " & Masa_Id & ")"
            Call SQL_KOS(SQ, False)
        End If


    End Sub

    Private Function Adisyon_Yazdir(ByVal Veri As String) As Boolean
        Dim P1 As Integer = 0
        Dim P2 As Integer = 0
        Dim P3 As Integer = 0
        Dim P4 As Integer = 0

        Dim Masa_Id As Long = 0
        Dim PDA As String = ""
        Dim PER_ID As Integer = 0
        Dim PER_AD As String = ""
        Dim SUBE As Long = 0
        Dim MASTER As Long = 0

        Dim SQ As String = ""
        Dim DT As New DataTable
        Dim Dx As New DataTable
        Dim DTM As New DataTable
        Dim DTA As New DataTable
        Dim DTT As New DataTable
        Dim DTADS

        Dim X As Integer = 0
        Dim EB As Long = 0
        Dim B As String = ""
        Dim S As String = ""
        Dim C_ID As Long = 0

        Dim xTutar As Double = 0
        Dim yTutar As Double = 0
        Dim iTutar As Double = 0

        Dim ADISYONS_NOSU As Long = 0
        Dim Masa As String = ""

        Dim KISI As Integer = 0

        Dim PRNcm0 As Long = 0
        Dim PRNcm1 As String = ""
        Dim K As String = ""

        '"ADISYON MASA=" & Val(Label5.Tag) & " PDA=" & PDA_AD & " GARSON=" & PERSON_ID

        '----------------------------------------------------------------------------------

        P1 = InStr(Veri, "MASA=")
        P2 = InStr(Veri, "PDA=")
        P3 = InStr(Veri, "GARSON=")
        P4 = InStr(Veri, "MASTER=")

        Masa_Id = Val(Trim(Mid(Veri, P1 + 5, P2 - (P1 + 5))))
        PDA = Trim(Mid(Veri, P2 + 4, P3 - (P2 + 4)))
        PER_ID = Val(Trim(Mid(Veri, P3 + 7, P4 - (P3 + 4))))
        MASTER = Val(Trim(Mid(Veri, P4 + 7, Len(Veri))))


        'SQ = "SELECT a_id, a_adi, a_depoid, a_kasaid, a_bankaid, a_perid"
        'SQ = SQ & " FROM tbl_user"
        'SQ = SQ & " WHERE(a_id = " & PER_ID & ")"
        'DT = SQL_TABLO(SQ, "T", False, False, False)
        'PER_AD = NULA(DT, 0, "a_adi")

        SQ = "SELECT a_adi "
        SQ = SQ & " FROM tbl_001_personel"
        SQ = SQ & " WHERE(a_id = " & PER_ID & ")"
        DT = SQL_TABLO(SQ, "T", False, False, False)
        PER_AD = NULA(DT, 0, "a_adi")

        '----------------------------------------------------------------------------------




        SQ = ""
        SQ = "SELECT a_id, a_kod, a_adi, a_durum, a_adsnosu, a_cariid, a_sube "
        SQ = SQ & " FROM " & KUL.tfrm & "men_masa"
        SQ = SQ & " WHERE(a_id = " & Masa_Id & ")"
        DT = SQL_TABLO(SQ, "VERI", False, False, False)
        Masa = NULA(DT, 0, "a_adi")
        SUBE = NULN(DT, 0, "a_sube")

        'Adisyon Bastırıldı.
        SQ = ""
        SQ = "UPDATE " & KUL.tfrm & "men_masa"
        SQ = SQ & " SET "
        SQ = SQ & " a_adsbas = 1"
        SQ = SQ & " WHERE(a_id = " & Masa_Id & ")"
        Call SQL_KOS(SQ, False)





        SQ = ""
        SQ = "SELECT     a_id, a_subeid, a_kisi_sayisi, a_cari_id"
        SQ = SQ & " FROM  " & KUL.tfrm & "men_adisyonmas"
        SQ = SQ & " WHERE a_id=" & MASTER
        DTADS = SQL_TABLO(SQ, "AD", False, False, False)


        ADISYONS_NOSU = NULN(DTADS, 0, "a_id")
        KISI = NULN(DTADS, 0, "a_kisi_sayisi")
        SUBE = NULN(DTADS, 0, "a_subeid")
        C_ID = NULN(DTADS, 0, "a_cari_id")


        SQ = "SELECT a_id, a_veri1, a_veri2, a_veri3, a_veri4, a_yazici, a_esc1, a_esc2"
        SQ = SQ & " FROM " & KUL.tfrm & "men_tanim "
        SQ = SQ & " WHERE a_id = 1"
        DT = SQL_TABLO(SQ, "T", False, False, False)
        PRNcm0 = NULN(DT, 0, "a_esc1")
        PRNcm1 = NULA(DT, 0, "a_esc2")




        SQ = ""
        SQ = "SELECT a_id, a_ftel, a_fistel1, a_fistel2, a_fgsm, a_fgsm2, a_kod, a_adi, a_fadres1, a_fadres2, a_fadres3 "
        SQ = SQ & " FROM " & KUL.tfrm & "cari"
        SQ = SQ & " WHERE  a_id = " & C_ID
        DT = SQL_TABLO(SQ, "cari", False, False, False)

        SQ = "SELECT a_id, a_veri1, a_veri2, a_veri3, a_veri4, a_yazici, a_esc1, a_esc2, a_zaman, a_bekle, a_adsbas, a_adsson1, a_adsson2, a_adsson3, a_adsson4, a_adsson5"
        SQ = SQ & " FROM " & KUL.tfrm & "men_tanim "
        SQ = SQ & " WHERE a_id = 1"
        DTT = SQL_TABLO(SQ, "T", False, False, False)

        B = ""
        B = B & Yasla(NULA(DTT, 0, "a_adsbas"), 40, "ORT") & vbCrLf
        B = B & Yasla(NULA(DT, 0, "a_ftel"), 40, "SOL") & vbCrLf
        B = B & Yasla(NULA(DT, 0, "a_adi"), 40, "SOL") & vbCrLf
        B = B & Yasla(NULA(DT, 0, "a_fadres1"), 40, "SOL") & vbCrLf
        B = B & Yasla(NULA(DT, 0, "a_fadres2"), 40, "SOL") & vbCrLf
        B = B & Yasla(NULA(DT, 0, "a_fadres3"), 40, "SOL") & vbCrLf

        B = B & "Personel Adı        : " & PER_AD & vbCrLf
        B = B & "PDA Kodu            : " & PDA & vbCrLf
        B = B & "Adisyon Fis  Nosu   : " & ADISYONS_NOSU & vbCrLf
        B = B & Yasla("Masa Nosu: " & Masa, 40, "SOL") & vbCrLf
        B = B & "Tarih :" & TTAR.TARIH & "     Saat : " & TTAR.SAAT & vbCrLf

        S = ""
        S = Yasla("Urun Adi", 17, "SOL")
        S = S & " " & Yasla("Mik.", 5, "SAG")
        S = S & " " & Yasla("Br.Fiyat", 6, "SAG")
        S = S & " " & Yasla("Tutar", 8, "SAG")
        B = B & S & vbCrLf

        S = ""
        S = Yasla("--------------------", 17, "SOL")
        S = S & " " & Yasla("-----", 5, "SAG")
        S = S & " " & Yasla("--------", 6, "SAG")
        S = S & " " & Yasla("----------", 8, "SAG")
        B = B & S & vbCrLf


        'SQ = ""
        'SQ = "SELECT "
        'SQ = SQ & " men_adisyon.a_mid,"
        'SQ = SQ & " men_adisyon.a_sira,"
        'SQ = SQ & " men_adisyon.a_sid, "
        'SQ = SQ & " men_adisyon.a_yildiz,"
        'SQ = SQ & " " & KUL.tfrm &"stok.a_kod, "
        'SQ = SQ & " " & KUL.tfrm &"stok.a_adi, "
        'SQ = SQ & " men_adisyon.a_mik, "
        'SQ = SQ & " men_adisyon.a_brf, "
        'SQ = SQ & " men_adisyon.a_tut, "
        'SQ = SQ & " men_adisyon.a_tarih, "
        'SQ = SQ & " men_adisyon.a_saat, "
        'SQ = SQ & " men_adisyon.a_userid, "
        'SQ = SQ & " men_adisyon.a_not, "
        'SQ = SQ & " tbl_user.a_adi AS padi"
        'SQ = SQ & " FROM men_adisyon INNER JOIN"
        'SQ = SQ & " " & KUL.tfrm &"stok ON men_adisyon.a_sid = " & KUL.tfrm &"stok.a_id INNER JOIN"
        'SQ = SQ & " tbl_user ON men_adisyon.a_userid = tbl_user.a_id"
        'SQ = SQ & " WHERE(men_adisyon.a_mid = " & Masa_Id & ")"
        'SQ = SQ & " ORDER BY men_adisyon.a_sira DESC"
        'DTM = SQL_TABLO(SQ, "VERI", False, False, False)


        SQ = ""
        SQ = "SELECT A.a_mid, A.a_sid, S.a_adi, SUM(A.a_mik) AS a_mik, A.a_brf, SUM(A.a_tut) AS a_tut , Sum(A.a_isktutar) as a_isktut"
        SQ = SQ & " FROM " & KUL.tfrm & "men_adisyon AS A INNER JOIN"
        SQ = SQ & " " & KUL.tfrm & "stok AS S ON A.a_sid = S.a_id"
        SQ = SQ & " GROUP BY A.a_mid, A.a_sid, S.a_adi, A.a_brf , A.a_master_id"
        SQ = SQ & " HAVING(A.a_mid = " & Masa_Id & ")  AND (A.a_master_id=" & MASTER & ")"
        DTM = SQL_TABLO(SQ, "VERI", False, False, False)

        For X = 0 To DTM.Rows.Count - 1

            S = ""
            S = " " 'Yasla(NULA(DTM, X, "a_yildiz"), 1, "SOL")
            S = S & " " & Yasla(NULA(DTM, X, "a_adi"), 15, "SOL")
            S = S & " " & Yasla(NULA(DTM, X, "a_mik"), 5, "SAG")
            S = S & " " & Yasla(NULA(DTM, X, "a_brf"), 6, "SAG")
            S = S & " " & Yasla(NULA(DTM, X, "a_tut"), 8, "SAG")
            B = B & S & vbCrLf
            xTutar = xTutar + (Val(NULD(DTM, X, "a_tut")) - Val(NULD(DTM, X, "a_isktut")))
            yTutar = yTutar + Val(NULD(DTM, X, "a_tut"))
            iTutar = iTutar + Val(NULD(DTM, X, "a_isktut"))
            'If NULA(DTM, X, "a_not") <> "" Then
            '    S = ""
            '    S = "( " & Yasla(NULA(DTM, X, "a_not"), 35, "SOL") & " )"
            '    B = B & S & vbCrLf
            'End If

        Next


        B = B & "------------------------------ -------- " & vbCrLf
        If iTutar <> 0 Then
            B = B & "                       AraTop: " & Yasla(Trim(Str(yTutar)), 8, "SAG") & "" & vbCrLf
            B = B & "                       IskTop: " & Yasla(Trim(Str(iTutar)), 8, "SAG") & "" & vbCrLf
            B = B & "                       Tutar : " & Yasla(Trim(Str(xTutar)), 8, "SAG") & "" & vbCrLf
        Else
            B = B & "                       Tutar : " & Yasla(Trim(Str(xTutar)), 8, "SAG") & "" & vbCrLf
        End If

        B = B & "Bilgilendirme Icindir. Mali Degeri Yok." & vbCrLf
        B = B & "------------------------------ -------- " & vbCrLf

        B = B & "" & vbCrLf
        B = B & Yasla(NULA(DTT, 0, "a_adsson1"), 40, "SOL") & vbCrLf
        B = B & Yasla(NULA(DTT, 0, "a_adsson2"), 40, "SOL") & vbCrLf
        B = B & Yasla(NULA(DTT, 0, "a_adsson3"), 40, "SOL") & vbCrLf
        B = B & Yasla(NULA(DTT, 0, "a_adsson4"), 40, "SOL") & vbCrLf
        B = B & Yasla(NULA(DTT, 0, "a_adsson5"), 40, "SOL") & vbCrLf

        For X = 1 To 4
            B = B & "" & vbCrLf
        Next

        'Kesme Komutu
        If PRNcm0 <> 0 Then
            B = B & Chr(PRNcm0) & PRNcm1 & vbCrLf
        End If




        B = TURK(B, 0)

        'Dim DSY As String = ""
        'DSY = sifre_uret(8) & ".TEX"
        'If VAR_MI(DSY) = True Then
        '    Try
        '        Kill(DSY)
        '    Catch ex As Exception
        '    End Try
        'End If
        'Try
        '    File.WriteAllText(DSY, B)
        '    Call Shell("YAZ.BAT " & DSY, AppWinStyle.Hide)
        'Catch ex As Exception
        'End Try

        K = PER_ID & "-" & Now.Year & "-" & Now.Month & "-" & Now.Day & " " & Now.Hour & ":" & Now.Minute & ":" & Now.Second & "-" & sifre_uret(8)

        SQ = ""
        SQ = "INSERT INTO " & KUL.tfrm & "men_pserver (a_id, a_tur, a_pda, a_user, a_tarih, a_saat, a_komut, a_veri, a_durum, a_sube, a_masa)"
        SQ = SQ & " VALUES "
        SQ = SQ & " ( "
        SQ = SQ & " '" & K & "', "
        SQ = SQ & " '" & "ADİSYON" & "', "
        SQ = SQ & " '" & PDA & "', "
        SQ = SQ & "  " & PER_ID & " , "
        SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & "', "
        SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & " " & TTAR.SAAT & "', "
        SQ = SQ & " '" & "YAZ.BAT" & "', "
        SQ = SQ & " '" & B & "', "
        SQ = SQ & " '" & "H" & "', "
        SQ = SQ & "  " & SUBE & ", "
        SQ = SQ & "  " & Masa_Id & "  "
        SQ = SQ & " ) "
        Call SQL_KOS(SQ, False)




        SQ = ""
        SQ = "UPDATE " & KUL.tfrm & "men_adisyon"
        SQ = SQ & " SET "
        SQ = SQ & " a_yildiz = ''"
        SQ = SQ & " WHERE (a_mid = " & Masa_Id & ")"
        Call SQL_KOS(SQ, False)

        Return True

    End Function

    ''' <summary>
    ''' Gönderilen Komut Türüne ve SQL'e Göre İşlem Yapar
    ''' </summary>
    ''' <param name="Tur">Komut Türü</param>
    ''' <param name="Veri">İşlenecek SQL</param>
    Public Sub Masa_Kapat(ByVal Tur As String, ByVal Veri As String)


        Dim DEP_ID As Long = 0
        Dim KAS_ID As Long = 0
        Dim BNK_ID As Long = 0
        Dim PDA As String = ""

        Dim P1 As Integer = 0
        Dim P2 As Integer = 0
        Dim P3 As Integer = 0
        Dim P4 As Integer = 0
        Dim P5 As Integer = 0
        Dim P6 As Integer = 0
        Dim P7 As Integer = 0
        Dim P8 As Integer = 0
        Dim P9 As Integer = 0


        Dim SQ As String = ""
        Dim DT As New DataTable
        Dim Dx As New DataTable
        Dim DTM As New DataTable
        Dim X As Integer = 0
        Dim EB As Long = 0
        Dim FIS_NOSU As Long = 0
        Dim ADISYONS_NOSU As Long = 0

        Dim ADSIRA As Long = 1
        Dim m_id As String = ""
        Dim C_ID As Long = 0
        Dim Masa As String = ""
        Dim PRNcm0 As Long = 0
        Dim PRNcm1 As String = ""
        Dim B As String = ""
        Dim S As String = ""
        Dim xTutar As Double = 0
        Dim KISI As Long = 0

        Dim K As String = ""
        Dim sq1 As String = ""
        Dim SUBE As Long = 0
        Dim sq3 As String = ""
        Dim SUBEID As Long = 0
        Dim ADSLER As Long
        '"NKAPAT PER=" & PERSON_ID & " MASA=" & Val(Label5.Tag)  
        '"KKAPAT PER=" & PERSON_ID & " MASA=" & Val(Label5.Tag)  

        '"ANKAPAT PER=" & PERSON_ID & " MASA=" & Val(Label5.Tag)  & " PDA=" & "PDA1"
        '"AKKAPAT PER=" & PERSON_ID & " MASA=" & Val(Label5.Tag)  & " PDA=" & "PDA1"

        'KKAPAT PER=1 MASA=2
        '12345678901234567890

        If Tur = "ANKAPAT" Or Tur = "AKKAPAT" Then

            P1 = InStr(Veri, "PER=")
            P2 = InStr(Veri, "MASA=")
            P3 = InStr(Veri, "PDA=")

            USR_ID = Val(Trim(Mid(Veri, P1 + 4, P2 - (P1 + 4))))
            MAS_ID = Val(Trim(Mid(Veri, P2 + 5, P3 - (P2 + 5))))
            PDA = Trim(Mid(Veri, P3 + 4, Len(Veri)))

        ElseIf Tur = "YKAPAT" Then

            P1 = InStr(Veri, "PER=")
            P2 = InStr(Veri, "MASA=")
            P3 = InStr(Veri, "PDA=")
            P4 = InStr(Veri, "KID=")
            P5 = InStr(Veri, "KTUTAR=")
            P6 = InStr(Veri, "BID=")
            P7 = InStr(Veri, "BTUT=")
            P8 = InStr(Veri, "ADSNO=")
            P9 = InStr(Veri, "VARID=")

            USR_ID = Val(Trim(Mid(Veri, P1 + 4, P2 - (P1 + 4))))
            MAS_ID = Val(Trim(Mid(Veri, P2 + 5, P3 - (P2 + 5))))
            PDA = Val(Trim(Mid(Veri, P3 + 4, P4 - (P3 + 4))))
            KID = Val(Trim(Mid(Veri, P4 + 4, P5 - (P4 + 4))))
            KTUT = Val(Trim(Mid(Veri, P5 + 7, P6 - (P5 + 7))))
            BID = Val(Trim(Mid(Veri, P6 + 4, P7 - (P6 + 4))))
            BTUT = Val(Trim(Mid(Veri, P7 + 5, P8 - (P7 + 5))))
            ADSNO = Val(Trim(Mid(Veri, P8 + 6, P9 - (P8 + 6))))

            ' DEĞİŞKENLER (EMRE)
            USER_ID = Val(Trim(Mid(Veri, P1 + 4, P2 - (P1 + 4))))
            MASA_ID = Val(Trim(Mid(Veri, P2 + 5, P3 - (P2 + 5))))
            KASA_ID = Val(Trim(Mid(Veri, P4 + 4, P5 - (P4 + 4))))
            KASA_TUTAR = Val(Trim(Mid(Veri, P5 + 7, P6 - (P5 + 7))))
            BANKA_ID = Val(Trim(Mid(Veri, P6 + 4, P7 - (P6 + 4))))
            BANKA_TUTAR = Val(Trim(Mid(Veri, P7 + 5, P8 - (P7 + 5))))
            ADISYON_ID = Val(Trim(Mid(Veri, P8 + 6, P9 - (P8 + 6))))
            VARDIYA_ID = Trim(Mid(Veri, P9 + 6, Len(Veri)))

        Else

            P1 = InStr(Veri, "PER=")
            P2 = InStr(Veri, "MASA=")

            USR_ID = Val(Trim(Mid(Veri, P1 + 4, P2 - (P1 + 4))))
            MAS_ID = Val(Trim(Mid(Veri, P2 + 5, Len(Veri))))
            PDA = ""

        End If




        SQ = ""
        SQ = "SELECT a_id, a_kod, a_adi, a_durum, a_adsnosu, a_cariid, a_sube"
        SQ = SQ & " FROM " & KUL.tfrm & "men_masa"
        SQ = SQ & " WHERE(a_id = " & MAS_ID & ")"
        DT = SQL_TABLO(SQ, "VERI", False, False, False)
        ADISYONS_NOSU = NULN(DT, 0, "a_adsnosu")
        C_ID = NULN(DT, 0, "a_cariid")
        Masa = NULA(DT, 0, "a_adi")
        SUBE = NULN(DT, 0, "a_sube")

        'SQ = "SELECT a_id, a_depoid, a_kasaid, a_bankaid "
        'SQ = SQ & " FROM tbl_user"
        'SQ = SQ & " WHERE(a_id = " & USR_ID & ")"
        'DT = SQL_TABLO(SQ, "T", False, False, False)

        ' Sube default bilgileri alınıyor

        DEP_ID = NULN(DT, 0, "a_depoid")
        KAS_ID = NULN(DT, 0, "a_kasaid")
        BNK_ID = NULN(DT, 0, "a_bankaid")

        SQ = ""
        SQ = "SELECT a_id, a_durum, a_adsnosu"
        SQ = SQ & " FROM " & KUL.tfrm & "men_masa"
        SQ = SQ & " WHERE(a_id = " & MAS_ID & ")"
        Dx = SQL_TABLO(SQ, "VERI", False, False, False)
        FIS_NOSU = NULN(Dx, 0, "a_adsnosu")

        ' Yeni ID Alınıyor...
        SQ = ""
        SQ = "SELECT MAX(a_adid) AS ads"
        SQ = SQ & " FROM " & KUL.tfrm & "men_adskapat"
        Dx = SQL_TABLO(B_SQL, SQ, "T", False, False, False)
        If Dx.Rows.Count > 0 Then
            EB = NULN(Dx, 0, "ads")
            If EB = 0 Then
                EB = 1
            Else
                EB = EB + 1
            End If
        End If

        'SQ = ""
        'SQ = "INSERT INTO " & KUL.tfrm & "men_adskapat"
        'SQ = SQ & " (a_adid, a_adsira)"
        'SQ = SQ & " VALUES(" & EB & ", 0)"
        'SQL_KOS(SQ, False)

        'ADSIRA = ADSIRA + 1



        'Şube id alınıyor.
        SQ = ""
        SQ = SQ & "SELECT a_mid,a_subeid,a_cari_id "
        SQ = SQ & "FROM " & KUL.tfrm & "men_adisyonmas "
        SQ = SQ & "WHERE (a_mid=" & MAS_ID & ")"
        DT = SQL_TABLO(SQ, "ADİSYONMAS", False, True, False)
        SUBEID = NULN(DT, 0, "a_subeid")

        'a_adsira

        SQ = ""
        SQ = SQ & " SELECT a_mid, a_sira, a_sid, a_mik, a_brf, a_tut, a_tarih, a_saat, a_userid, a_not"
        SQ = SQ & " FROM " & KUL.tfrm & "men_adisyon"
        SQ = SQ & " WHERE(a_mid = " & MAS_ID & ")"
        DT = SQL_TABLO(SQ, "Adisyon", False, True, False)

        For X = 0 To DT.Rows.Count - 1
            SQ = ""
            SQ = "INSERT INTO " & KUL.tfrm & "men_adskapat"
            SQ = SQ & " (a_adid, a_adsira, a_adno, a_cid, a_mid, a_sira, a_sid, a_mik, a_brf, a_tut, a_tarih, a_saat, a_userid, a_not, "
            SQ = SQ & " a_subeid, a_mutfakid, a_satdurum,"
            SQ = SQ & " a_mtarih, a_msaat, a_muserid,"
            SQ = SQ & " a_ttarih, a_tsaat, a_tuserid,"
            SQ = SQ & " a_starih, a_ssaat, a_suserid,"
            SQ = SQ & " a_ktarih, a_ksaat, a_kuserid,"
            SQ = SQ & " a_durum, a_yer, a_svkperid, a_iskoran,"
            SQ = SQ & " a_isktutar, a_master_id, a_mali, a_personel"
            SQ = SQ & " )"
            SQ = SQ & " SELECT " & EB & " AS adid, " & ADSIRA & " AS adsira, '" & EB & " - " & 1 & "' AS adno, " & C_ID & " AS cid, a_mid, a_sira AS sira, a_sid, a_mik, a_brf, a_tut, a_tarih, a_saat, a_userid, a_not, "
            SQ = SQ & " a_subeid, a_mutfakid, a_satdurum,"
            SQ = SQ & " a_mtarih, a_msaat, a_muserid,"
            SQ = SQ & " a_ttarih, a_tsaat, a_tuserid,"
            SQ = SQ & " a_starih, a_ssaat, a_suserid,"
            SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & "' As a_ktarih, '" & TTAR.TR2UK(TTAR.TARIH) & " " & Now.ToShortTimeString & "' As a_ksaat, " & Val(KUL.KOD) & " As a_kuserid,"
            SQ = SQ & " '4-Kapanan' As a_durum, a_yer, a_svkperid, a_iskoran, a_isktutar, " & ADISYON_ID & ", a_mali, a_personel "
            SQ = SQ & " FROM " & KUL.tfrm & "men_adisyon"
            SQ = SQ & " WHERE (a_mid = " & NULN(DT, X, "a_mid") & ") AND (a_sira = " & NULN(DT, X, "a_sira") & ")"
            If SQL_KOS(SQ) = True Then
            End If
            ADSIRA = ADSIRA + 1
        Next

        Call uretim_emri(DT, DEP_ID)


        If Tur = "YKAPAT" Then
            Try
                Call EVRAK_EKLE(1, "YKAPAT", USER_ID, "Kapalı", DT, MASA_ID, ADISYON_ID, VARDIYA_ID, EB, 1, KASA_ID, BANKA_ID, SUBEID)
            Catch ex As Exception
            End Try

        End If


        'Kasa İle Nakit Kapat
        If Tur = "NKAPAT" Or Tur = "ANKAPAT" Then
            Try
                Call EVRAK_EKLE(1, "NKAPAT", USR_ID, "Kapalı", DT, MAS_ID, FIS_NOSU, VARDIYA_ID, EB, 1, KAS_ID, BNK_ID, SUBEID)
            Catch ex As Exception
            End Try

        End If


        'Banka İle Kredi Kartı Kapat
        If Tur = "KKAPAT" Or Tur = "AKKAPAT" Then
            Try
                Call EVRAK_EKLE(1, "KKAPAT", USR_ID, "Kapalı", DT, MAS_ID, FIS_NOSU, VARDIYA_ID, EB, 1, KAS_ID, BNK_ID, SUBEID)
            Catch ex As Exception
                SetText(ex.Message)
            End Try
        End If





        If Tur = "ANKAPAT" Or Tur = "AKKAPAT" Then

            'SQ = "SELECT a_id, a_adi, a_depoid, a_kasaid, a_bankaid, a_perid"
            'SQ = SQ & " FROM tbl_user"
            'SQ = SQ & " WHERE(a_id = " & USR_ID & ")"
            'DT = SQL_TABLO(SQ, "T", False, False, False)
            'PER_AD = NULA(DT, 0, "a_adi")


            SQ = "SELECT a_adi "
            SQ = SQ & " FROM tbl_001_personel"
            SQ = SQ & " WHERE(a_id = " & USR_ID & ")"
            DT = SQL_TABLO(SQ, "T", False, False, False)
            PER_AD = NULA(DT, 0, "a_adi")

            SQ = ""
            SQ = "SELECT a_id, a_kod, a_adi, a_durum, a_adsnosu, a_cariid, a_sube, a_kisisay"
            SQ = SQ & " FROM " & KUL.tfrm & "men_masa"
            SQ = SQ & " WHERE(a_id = " & MAS_ID & ")"
            DT = SQL_TABLO(SQ, "VERI", False, False, False)
            ADISYONS_NOSU = NULN(DT, 0, "a_adsnosu")
            C_ID = NULN(DT, 0, "a_cariid")
            Masa = NULA(DT, 0, "a_adi")
            SUBE = NULN(DT, 0, "a_sube")
            KISI = NULN(DT, 0, "a_kisisay")


            SQ = "SELECT a_id, a_veri1, a_veri2, a_veri3, a_veri4, a_yazici, a_esc1, a_esc2"
            SQ = SQ & " FROM " & KUL.tfrm & "men_tanim "
            SQ = SQ & " WHERE a_id = 1"
            DT = SQL_TABLO(SQ, "T", False, False, False)
            PRNcm0 = NULN(DT, 0, "a_esc1")
            PRNcm1 = NULA(DT, 0, "a_esc2")


            B = ""

            SQ = ""
            SQ = "SELECT a_id, a_ftel, a_fistel1, a_fistel2, a_fgsm, a_fgsm2, a_kod, a_adi, a_fadres1, a_fadres2, a_fadres3 "
            SQ = SQ & " FROM " & KUL.tfrm & "cari"
            SQ = SQ & " WHERE  a_id = " & C_ID
            DT = SQL_TABLO(SQ, "cari", False, False, False)

            B = B & Yasla(NULA(DT, 0, "a_ftel"), 40, "SOL") & vbCrLf
            B = B & Yasla(NULA(DT, 0, "a_adi"), 40, "SOL") & vbCrLf
            B = B & Yasla(NULA(DT, 0, "a_fadres1"), 40, "SOL") & vbCrLf
            B = B & Yasla(NULA(DT, 0, "a_fadres2"), 40, "SOL") & vbCrLf
            B = B & Yasla(NULA(DT, 0, "a_fadres3"), 40, "SOL") & vbCrLf

            If Tur = "ANKAPAT" Then
                B = B & "Nakit Kapanan Adisyon" & vbCrLf
            End If

            If Tur = "AKKAPAT" Then
                B = B & "Krediyle Kapanan Adisyon" & vbCrLf
            End If


            B = B & "Kişi Sayısı         : " & KISI & vbCrLf
            B = B & "Personel Adı        : " & PER_AD & vbCrLf
            B = B & "PDA Kodu            : " & PDA & vbCrLf
            B = B & "Adisyon Fis  Nosu   : " & ADISYONS_NOSU & vbCrLf
            B = B & Yasla("Masa Nosu: " & Masa, 40, "SOL") & vbCrLf
            B = B & "Tarih :" & TTAR.TARIH & "     Saat : " & TTAR.SAAT & vbCrLf

            S = ""
            S = Yasla("Urun Adi", 17, "SOL")
            S = S & " " & Yasla("Mik.", 5, "SAG")
            S = S & " " & Yasla("Br.Fiyat", 6, "SAG")
            S = S & " " & Yasla("Tutar", 8, "SAG")
            B = B & S & vbCrLf

            S = ""
            S = Yasla("--------------------", 17, "SOL")
            S = S & " " & Yasla("-----", 5, "SAG")
            S = S & " " & Yasla("--------", 6, "SAG")
            S = S & " " & Yasla("----------", 8, "SAG")
            B = B & S & vbCrLf


            SQ = ""
            SQ = "SELECT "
            SQ = SQ & " " & KUL.tfrm & "men_adisyon.a_mid,"
            SQ = SQ & " " & KUL.tfrm & "men_adisyon.a_sira,"
            SQ = SQ & " " & KUL.tfrm & "men_adisyon.a_sid, "
            SQ = SQ & " " & KUL.tfrm & "men_adisyon.a_yildiz,"
            SQ = SQ & " " & KUL.tfrm & "stok.a_kod, "
            SQ = SQ & " " & KUL.tfrm & "stok.a_adi, "
            SQ = SQ & " " & KUL.tfrm & "men_adisyon.a_mik, "
            SQ = SQ & " " & KUL.tfrm & "men_adisyon.a_brf, "
            SQ = SQ & " " & KUL.tfrm & "men_adisyon.a_tut, "
            SQ = SQ & " " & KUL.tfrm & "men_adisyon.a_tarih, "
            SQ = SQ & " " & KUL.tfrm & "men_adisyon.a_saat, "
            SQ = SQ & " " & KUL.tfrm & "men_adisyon.a_userid, "
            SQ = SQ & " " & KUL.tfrm & "men_adisyon.a_not, "
            'SQ = SQ & " tbl_user.a_adi AS padi"
            SQ = SQ & " FROM " & KUL.tfrm & "men_adisyon INNER JOIN"
            SQ = SQ & " " & KUL.tfrm & "stok ON " & KUL.tfrm & "men_adisyon.a_sid = " & KUL.tfrm & "stok.a_id "
            'SQ = SQ & " INNER JOIN tbl_user ON " & KUL.tfrm & "men_adisyon.a_userid = tbl_user.a_id"
            SQ = SQ & " WHERE(" & KUL.tfrm & "men_adisyon.a_mid = " & MAS_ID & ")"
            SQ = SQ & " ORDER BY " & KUL.tfrm & "men_adisyon.a_sira DESC"
            DTM = SQL_TABLO(SQ, "VERI", False, False, False)


            For X = 0 To DTM.Rows.Count - 1

                S = ""
                S = Yasla(NULA(DTM, X, "a_yildiz"), 1, "SOL")
                S = S & " " & Yasla(NULA(DTM, X, "a_adi"), 15, "SOL")
                S = S & " " & Yasla(NULA(DTM, X, "a_mik"), 5, "SAG")
                S = S & " " & Yasla(NULA(DTM, X, "a_brf"), 6, "SAG")
                S = S & " " & Yasla(NULA(DTM, X, "a_tut"), 8, "SAG")
                B = B & S & vbCrLf

                xTutar = xTutar + Val(NULD(DTM, X, "a_tut"))

                If NULA(DTM, X, "a_not") <> "" Then
                    S = ""
                    S = "( " & Yasla(NULA(DTM, X, "a_not"), 35, "SOL") & " )"
                    B = B & S & vbCrLf
                End If

            Next


            B = B & "------------------------------ -------- " & vbCrLf
            B = B & "                       Tutar : " & Yasla(Trim(Str(xTutar)), 8, "SAG") & "" & vbCrLf
            B = B & "Bilgilendirme Icindir. Mali Degeri Yok." & vbCrLf
            B = B & "------------------------------ -------- " & vbCrLf



            For X = 1 To 10
                B = B & "" & vbCrLf
            Next

            'Kesme Komutu
            If PRNcm0 <> 0 Then
                B = B & Chr(PRNcm0) & PRNcm1 & vbCrLf
            End If




            B = TURK(B, 0)

            'Dim DSY As String = ""
            'DSY = sifre_uret(8) & ".TEX"
            'If VAR_MI(DSY) = True Then
            '    Try
            '        Kill(DSY)
            '    Catch ex As Exception
            '    End Try
            'End If
            'Try
            '    File.WriteAllText(DSY, B)
            '    Call Shell("YAZ.BAT " & DSY, AppWinStyle.Hide)
            'Catch ex As Exception
            'End Try



            K = USR_ID & "-" & Now.Year & "-" & Now.Month & "-" & Now.Day & " " & Now.Hour & ":" & Now.Minute & ":" & Now.Second & "-" & sifre_uret(8)

            SQ = ""
            SQ = "INSERT INTO " & KUL.tfrm & "men_pserver (a_id, a_tur, a_pda, a_user, a_tarih, a_saat, a_komut, a_veri, a_durum, a_sube, a_masa)"
            SQ = SQ & " VALUES "
            SQ = SQ & " ( "
            SQ = SQ & " '" & K & "', "
            SQ = SQ & " '" & "ADİSYON" & "', "
            SQ = SQ & " '" & PDA & "', "
            SQ = SQ & "  " & USR_ID & " , "
            SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & "', "
            SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & " " & TTAR.SAAT & "', "
            SQ = SQ & " '" & "YAZ.BAT" & "', "
            SQ = SQ & " '" & B & "', "
            SQ = SQ & " '" & "H" & "', "
            SQ = SQ & "  " & SUBE & ", "
            SQ = SQ & "  " & MAS_ID & "  "
            SQ = SQ & " ) "
            Call SQL_KOS(SQ, False)


            SQ = ""
            SQ = "UPDATE " & KUL.tfrm & "men_adisyon"
            SQ = SQ & " SET "
            SQ = SQ & " a_yildiz = ''"
            SQ = SQ & " WHERE (a_mid = " & MASA_ID & ") AND (a_master_id = " & ADISYON_ID & ")"
            Call SQL_KOS(SQ, False)

        End If


        SQ = ""
        SQ = "DELETE FROM " & KUL.tfrm & "men_adisyon"
        SQ = SQ & " WHERE (a_mid = " & MASA_ID & ") AND (a_master_id = " & ADISYON_ID & ")"
        If SQL_KOS(SQ) = True Then
            SQ = ""
            SQ = "DELETE FROM " & KUL.tfrm & "men_adisyonmas"
            SQ = SQ & " WHERE (a_mid = " & MASA_ID & ") AND (a_id = " & ADISYON_ID & ")"
            SQL_KOS(SQ)
        End If

        SQ = "SELECT a_mid,a_master_id FROM " & KUL.tfrm & "men_adisyon"
        SQ = SQ & " WHERE (a_mid = " & MASA_ID & ") "
        DT = SQL_TABLO(SQ, "T", False, False, False)
        If DT.Rows.Count < 1 Then

            SQ = "UPDATE " & KUL.tfrm & "men_masa"
            SQ = SQ & " SET a_durum = 'BOŞ'"
            SQ = SQ & "  , a_adsbas = 0"
            SQ = SQ & " WHERE (a_id = " & MASA_ID & ")"
            Call SQL_KOS(SQ, False)




            'Bağlantılı Masaları Çöz.
            SQ = ""
            SQ = "SELECT a_amid, a_ymid"
            SQ = SQ & " FROM " & KUL.tfrm & "men_masbir"
            SQ = SQ & " WHERE a_amid = " & MAS_ID & ""
            SQ = SQ & " AND a_ymid <> " & MAS_ID & ""
            DT = SQL_TABLO(SQ, "T", False, False, False)
            For X = 0 To DT.Rows.Count - 1
                SQ = "Update " & KUL.tfrm & "men_masa"
                SQ = SQ & " SET a_durum = 'BOŞ'"
                SQ = SQ & " , a_adsbas = 0"
                SQ = SQ & " , a_acsaat = '" & TTAR.TR2UK(TTAR.TARIH) & " " & Now.ToShortTimeString & "'"
                SQ = SQ & " , a_adsnosu = " & 0 & ""
                SQ = SQ & " WHERE(a_id = " & NULN(DT, X, "a_ymid") & ")"
                Call SQL_KOS(SQ, False)
            Next

            SQ = ""
            SQ = "DELETE "
            SQ = SQ & " FROM " & KUL.tfrm & "men_masbir"
            SQ = SQ & " WHERE a_amid = " & MAS_ID & ""
            Call SQL_KOS(SQ, False)
        End If



    End Sub


    Public Sub belge_yansit(ByVal kesin As Long, ByVal ISLNO As Long, ByVal TIP As String, ByVal Ktur As String, ByVal USER_ID As Long, ByVal MASA_ID As Long, ByVal ADISYON_ID As Long, ByVal ADID As Long, ByVal ADBOL As Long, ByVal TUTAR_TOPLAM As Double, ByVal VARDIYA_ID As Long)

        'TIP = FATURANIN TİPİNİ GÖSTERİR.
        'Açık veya Kapalı olabilir.


        '10	SCFAT	PSFAT	Parakende Satış Faturası	C	9
        '11	SCFAT	TSFAT	Toptan Satış Faturası	C	0
        '14	SCFAT	VHFAT	Verilen Hizmet Faturası	C	0
        '16	SCFAT	VFFAT	Verilen Fiyat Farkı Faturası	C	0

        '12	SCFAT	PSIFAT	Parakende Satış İade Faturası	C	0
        '13	SCFAT	TSIFAT	Toptan Satış İade Faturası	C	0

        '15	SCFAT	VPFAT	Verilen Proforma Faturası	C	0

        '70	SCFAT	SFFAT	Satış Fişi              	C	0


        Dim TERS As New TERS_ISLEM
        Dim DT As New DataTable
        Dim SQ As String = ""

        TERS.T03_T_ID = 1
        TERS.T04_ISLEM_NO = ISLNO
        TERS.T05_BELGE_NO = ADID & "-" & ADBOL
        TERS.T06_REF_ID = 1
        TERS.T07_TARIH = TTAR.TARIH
        TERS.T08_ACK = MASA_ID & " Nolu masanın " & ADID & " Nolu adisyonudur."
        TERS.T09_MEBLA = TUTAR_TOPLAM
        TERS.T10_YER = "FATURA"
        TERS.T11_YER_ID = ISLNO
        TERS.T12_YER_TUR = 10
        TERS.T13_TIP = "FATURA"
        TERS.T14_VTARIH = TTAR.TARIH


        'Satış 
        If kesin = 1 Then
            If TERS.EKLE(10) = True Then
                BG.cari_guncelle(1, TUTAR_TOPLAM, "B+")
                If TIP = "Kapalı" Then
                    If Ktur = "NKAPAT" Then
                        KASAYA_TAHSIL(kesin, KASA_ID, TUTAR_TOPLAM, ISLNO, USER_ID, MASA_ID, ADISYON_ID, ADID, ADBOL, VARDIYA_ID)
                    End If

                    If Ktur = "KKAPAT" Then
                        Call BANKAYA_TAHSIL(kesin, BANKA_ID, 1, TUTAR_TOPLAM, ISLNO, USER_ID, MASA_ID, ADISYON_ID, ADID, ADBOL, VARDIYA_ID)
                    End If

                    If Ktur = "YKAPAT" Then
                        ' Adisyona girilen tahsilatlar listeleniyor.
                        SQ = "SELECT a_turad AS islemYeri, a_id AS islemID, a_tutar AS islemTutar "
                        SQ = SQ & " FROM " & KUL.tfrm & "men_masa_tahsil"
                        SQ = SQ & " WHERE(a_masid = " & MASA_ID & ") AND (a_master_id = " & ADISYON_ID & ")"
                        SQL_KOS(SQ, False)
                        Dim DT_MEN_TAHSILAT As New DataTable
                        DT_MEN_TAHSILAT = SQL_TABLO(SQ, "Men_Tahsilat", False, True, False)

                        Dim ISLEM_YER As String = ""
                        Dim ISLEM_ID As Long = 0
                        Dim ISLEM_TUTAR As Double = 0
                        For X = 0 To DT_MEN_TAHSILAT.Rows.Count - 1
                            ISLEM_YER = NULA(DT_MEN_TAHSILAT, X, "islemYeri")
                            ISLEM_ID = NULN(DT_MEN_TAHSILAT, X, "islemID")
                            ISLEM_TUTAR = NULD(DT_MEN_TAHSILAT, X, "islemTutar")

                            If ISLEM_YER = "KASA" Then
                                Call KASAYA_TAHSIL(kesin, ISLEM_ID, ISLEM_TUTAR, ISLNO, USER_ID, MASA_ID, ADISYON_ID, ADID, ADBOL, VARDIYA_ID)
                            ElseIf ISLEM_YER = "BANKA" Then
                                Call BANKAYA_TAHSIL(kesin, ISLEM_ID, 1, ISLEM_TUTAR, ISLNO, USER_ID, MASA_ID, ADISYON_ID, ADID, ADBOL, VARDIYA_ID)
                            End If

                        Next

                    End If
                End If
            End If
        End If


    End Sub

    Public Sub stok_yansit(ByVal kesin As Long, ByVal ISLNO As Long)

        Dim AMB As Double
        Dim STK As Double
        Dim MIK As Double

        Dim GC As String
        Dim SQ As String
        Dim DT As New DataTable
        Dim X As Long = 0




        '10	SCFAT	PSFAT	Parakende Satış Faturası	C	9
        '11	SCFAT	TSFAT	Toptan Satış Faturası	C	0

        '14	SCFAT	VHFAT	Verilen Hizmet Faturası	C	0
        '16	SCFAT	VFFAT	Verilen Fiyat Farkı Faturası	C	0


        '12	SCFAT	PSIFAT	Parakende Satış İade Faturası	C	0
        '13	SCFAT	TSIFAT	Toptan Satış İade Faturası	C	0

        '15	SCFAT	VPFAT	Verilen Proforma Faturası	C	0

        '70	SCFAT	SFFAT	Satış Fişi              	C	0



        GC = "C"




        If kesin = 1 Then
            SQ = ""
            SQ = "INSERT INTO " & KUL.tper & "stkhmas"
            SQ = SQ & " (a_gc, a_tur, a_id, a_kod, a_bno, a_ack, a_tarih, a_cari_id, a_ambar_id, a_oplan_id, a_giskonto, a_isktop, a_kdvtop, a_tuttop, a_cuser, a_cdate, "
            SQ = SQ & " a_ctime, a_muser, a_mdate, a_mtime, a_bloke, a_kesin, a_fcari, a_fadres1, a_fadres2, a_fvno, a_fvda, a_ka, a_irsaliye, a_irsaliyetar, a_personel, a_fattur, a_katur, a_kayer, a_taksay, a_dkatur, a_dkayer, a_dtaksay, a_harpuan, a_kdvtevora, a_kdvtevtut, a_otvtop, a_vadetarihi)"
            SQ = SQ & " SELECT '" & GC & "' AS gc, a_tur, a_id, a_kod, a_bno, a_ack, a_tarih, a_cari_id, a_ambar_id, a_oplan_id, a_giskonto, a_isktop, a_kdvtop, a_tuttop, a_cuser, a_cdate, "
            SQ = SQ & " a_ctime, a_muser, a_mdate, a_mtime, a_bloke, a_kesin, a_fcari, a_fadres1, a_fadres2, a_fvno, a_fvda, a_ka, a_irsaliye, a_irsaliyetar, a_personel, a_fattur, a_katur, a_kayer, a_taksay, a_dkatur, a_dkayer, a_dtaksay, a_harpuan, a_kdvtevora, a_kdvtevtut, a_otvtop, a_vadetarihi "
            SQ = SQ & " FROM " & KUL.tper & "stkcmas"
            SQ = SQ & " WHERE (a_tur = 10) AND (a_id = " & ISLNO & ")"

            If SQL_KOS(SQ) = True Then
                '1	a_otv	int	4	1
                '0	a_otvtut	float	8	1
                SQ = ""
                SQ = "INSERT INTO " & KUL.tper & "stkhdet"
                SQ = SQ & " (a_gc, a_tur, a_id, a_sira, a_tarih, a_cari_id, a_stok_id, a_ambar_id, a_mik, a_brmfiy, a_kdv, a_kdvtut, a_isk, a_isk1, a_isk2, a_isktut, a_tutar, a_ack, a_bloke, "
                SQ = SQ & " a_kesin, a_did, a_dtut, a_dkur, a_durum, a_atar, a_perid, a_otv, a_otvtut, a_masmer, a_masyer, a_fattur, a_fatid ,"
                SQ = SQ & " a_sbrm, a_sbrmcrp, a_sbrmmik, a_sbrmfiy )"
                SQ = SQ & " SELECT '" & GC & "' AS gc, a_tur, a_id, a_sira, a_tarih, a_cari_id, a_stok_id, a_ambar_id, a_mik, a_brmfiy, a_kdv, a_kdvtut, a_isk, a_isk1, a_isk2, a_isktut, a_tutar, a_ack, a_bloke, "
                SQ = SQ & " a_kesin, a_did, a_dtut, a_dkur, a_durum, a_atar, a_perid, a_otv, a_otvtut, a_masmer, a_masyer, a_fattur, a_fatid, "
                SQ = SQ & " a_sbrm, a_sbrmcrp, a_sbrmmik, a_sbrmfiy "
                SQ = SQ & " FROM " & KUL.tper & "stkcdet"
                SQ = SQ & " WHERE (a_tur = 10) AND (a_id = " & ISLNO & ")"
                If SQL_KOS(SQ) = True Then
                    SQ = ""
                    SQ = "SELECT a_tur, a_id, a_sira, a_stok_id, a_ambar_id, a_mik"
                    SQ = SQ & " FROM " & KUL.tper & "stkcdet"
                    SQ = SQ & " WHERE (a_tur = 10) AND (a_id = " & ISLNO & ")"
                    DT = SQL_TABLO(B_SQL, SQ, "T", False, False, False)

                    If DT.Rows.Count > 0 Then
                        For X = 0 To DT.Rows.Count - 1

                            STK = NULN(DT, X, "a_stok_id")
                            AMB = NULN(DT, X, "a_ambar_id")
                            MIK = Val(NULD(DT, X, "a_mik"))

                            If GC = "G" Then
                                BG.stok_miktar(STK, MIK, "G+")
                                BG.depo_stok_miktar(AMB, STK, MIK, "G+")
                            End If

                            If GC = "C" Then
                                BG.stok_miktar(STK, MIK, "C+")
                                BG.depo_stok_miktar(AMB, STK, MIK, "C+")
                            End If

                        Next

                    End If
                End If
            End If
        End If

        If kesin = 0 Then
            SQ = ""
            SQ = "DELETE FROM " & KUL.tper & "stkhmas"
            SQ = SQ & " WHERE (a_gc = '" & GC & "') AND (a_tur = 10) AND (a_id = " & ISLNO & ")"
            SQL_KOS(SQ)

            SQ = ""
            SQ = "DELETE FROM " & KUL.tper & "stkhdet"
            SQ = SQ & " WHERE (a_gc = '" & GC & "') AND (a_tur = 10) AND (a_id = " & ISLNO & ")"
            SQL_KOS(SQ)

            SQ = ""
            SQ = "SELECT a_tur, a_id, a_sira, a_stok_id, a_ambar_id, a_mik"
            SQ = SQ & " FROM " & KUL.tper & "stkcdet"
            SQ = SQ & " WHERE (a_tur = 10) AND (a_id = " & ISLNO & ")"
            DT = SQL_TABLO(B_SQL, SQ, "T", False, False, False)
            If DT.Rows.Count > 0 Then
                For X = 0 To DT.Rows.Count - 1

                    STK = NULN(DT, X, "a_stok_id")
                    AMB = NULN(DT, X, "a_ambar_id")
                    MIK = Val(NULD(DT, X, "a_mik"))

                    If GC = "G" Then
                        BG.stok_miktar(STK, MIK, "G-")
                        BG.depo_stok_miktar(AMB, STK, MIK, "G-")
                    End If

                    If GC = "C" Then
                        BG.stok_miktar(STK, MIK, "C-")
                        BG.depo_stok_miktar(AMB, STK, MIK, "C-")
                    End If

                Next
            End If
        End If
    End Sub

    ''' <summary>
    ''' Gönderilen Adisyon Bilgisine Göre R1'e Perakende Satış Faturası Ekliyor
    ''' </summary>
    ''' <param name="kesin"></param>
    ''' <param name="Ktur"></param>
    ''' <param name="KUN"></param>
    ''' <param name="TIP"></param>
    ''' <param name="DT2"></param>
    ''' <param name="MAID"></param>
    ''' <param name="FIS_NO"></param>
    ''' <param name="ADID"></param>
    ''' <param name="ADBOL"></param>
    ''' <param name="Kasa_Id"></param>
    ''' <param name="Banka_Id"></param>
    Public Sub EVRAK_EKLE(ByVal kesin As Long, ByVal Ktur As String, ByVal USER_ID As Long, ByVal TIP As String, ByVal DT2 As DataTable, ByVal MASA_ID As Long, ByVal ADISYON_ID As Long, ByVal VARDIYA_ID As Long, ByVal ADID As Long, ByVal ADBOL As Long, ByVal KASA_ID As Long, ByVal BANKA_ID As Long, SUBEID As Long)
        ' Değişkenlar
#Region "Değişken"
        'TIP = FATURANIN TİPİNİ GÖSTERİR.
        'Açık veya Kapalı olabilir.

        Dim SQ As String = ""
        Dim DT As New DataTable
        Dim ID As Long = 0
        Dim X As Integer = 0

        Dim DTX As New DataTable
        Dim EBN As Long = 0


        Dim ISKOR As Double = 0
        Dim KDVOR As Double = 0

        Dim MIK As Double = 0
        Dim BRFIY As Double = 0


        Dim ISKTUT As Double = 0
        Dim ISKTOP As Double = 0

        Dim KDVTUT As Double = 0
        Dim KDVTOP As Double = 0

        Dim TUTAR As Double = 0
        Dim TUTTOP As Double = 0



        ID = TSAY.MAX_FIS_SCFAT(10, 1)


        KDVTOP = 0
        ISKOR = 0
#End Region


        ' Fatura Detayları Ekleniyor (Stkcdet)
#Region "Detaylar"

        For X = 0 To DT2.Rows.Count - 1

            ' Adisyon İçindeki Kullanıcı ID'den Kullanıcıya bağlı olan Personel ID Bulunuyor.
            'SQ = ""
            'SQ = "SELECT a_id, a_perid"
            'SQ = SQ & " FROM tbl_user"
            'SQ = SQ & " WHERE (a_id = " & NULN(DT2, X, "a_userid") & ")"
            'DT = SQL_TABLO(SQ, "T", False, False, False)


            SQ = "SELECT a_perid "
            SQ = SQ & " FROM tbl_001_personel"
            SQ = SQ & " WHERE(a_id = " & NULN(DT2, X, "a_userid") & ")"
            DT = SQL_TABLO(SQ, "T", False, False, False)


            ' Stok Bilgileri Bulunuyor.
            KDVOR = VT_BILGI("" & KUL.tfrm & "stok", "a_id", "a_sakdvor", NULN(DT2, X, "a_sid"))
            MIK = Val(NULD(DT2, X, "a_mik"))
            BRFIY = Val(NULD(DT2, X, "a_brf"))

            ' İskonto Hesaplanıyor
            ISKTUT = ((((MIK * BRFIY) / (1 + (KDVOR / 100))) / 100)) * ISKOR
            ISKTUT = kurus(ISKTUT, 2)
            ISKTOP = ISKTOP + ISKTUT

            ' KDV Hesaplanıyor
            KDVTUT = ((MIK * BRFIY) + ISKTUT) - (((MIK * BRFIY) + ISKTUT) / (1 + (KDVOR / 100)))
            KDVTUT = kurus(KDVTUT, 2)
            KDVTOP = KDVTOP + KDVTUT

            ' Tutar Hesaplanıyor
            TUTAR = (MIK * BRFIY) - ((MIK * BRFIY / 100) * ISKOR)
            TUTTOP = TUTTOP + TUTAR

            ' R1 İçerisine Belge Detayı Ekleniyor.
            SQ = ""
            SQ = "INSERT INTO " & KUL.tper & "stkcdet"
            SQ = SQ & " (a_tur, a_id, a_fattur, a_fatid, a_sira, a_tarih, a_cari_id, a_stok_id, a_ambar_id, a_mik, a_brmfiy, a_kdv, a_kdvtut, a_isk, a_isk1, a_isk2, a_isktut, a_tutar, a_ack, a_otv, a_otvtut, a_bloke, a_kesin, a_perid, "
            SQ = SQ & " a_sbrm, a_sbrmcrp, a_sbrmmik, a_sbrmfiy, a_sube_id)"

            SQ = SQ & " Select "
            SQ = SQ & " 10 as a_tur,"
            SQ = SQ & " " & ID & " as a_id,"

            SQ = SQ & " 10 as a_fattur,"
            SQ = SQ & " " & ID & " as a_fatid,"

            SQ = SQ & " " & NULN(DT2, X, "a_sira") & " as a_sira,"
            SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & "' as a_tarih,"
            SQ = SQ & " " & 1 & " as a_cari_id,"
            SQ = SQ & " " & NULN(DT2, X, "a_sid") & " as a_stok_id,"
            SQ = SQ & " " & 1 & " as a_ambar_id,"
            SQ = SQ & " " & S2D(MIK) & " as a_mik,"
            SQ = SQ & " " & S2D(BRFIY) & " as a_brmfiy,"
            SQ = SQ & " " & S2D(KDVOR) & " as a_kdv,"
            SQ = SQ & " " & S2D(KDVTUT) & " as a_kdvtut,"

            SQ = SQ & " " & S2D(ISKOR) & " as a_isk,"
            SQ = SQ & " 0 as a_isk1,"
            SQ = SQ & " 0 as a_isk2,"
            SQ = SQ & " " & S2D(ISKTUT) & " as a_isktut,"
            SQ = SQ & " " & S2D(TUTAR) & " as a_tutar,"
            SQ = SQ & " '" & VT_BILGI("" & KUL.tfrm & "stok", "a_id", "a_adi", NULN(DT2, X, "a_sid")) & "' as a_ack,"
            SQ = SQ & " 0 as a_otv,"
            SQ = SQ & " 0 as a_otvtut,"
            SQ = SQ & " 0 as a_bloke,"
            SQ = SQ & " " & kesin & " as a_kesin,"
            SQ = SQ & " " & NULN(DT, 0, "a_perid") & " as a_perid,"

            'a_sbrm, a_sbrmcrp, a_sbrmmik, a_sbrmfiy
            SQ = SQ & " '" & VT_BILGI("" & KUL.tfrm & "stok", "a_id", "a_birim", NULN(DT2, X, "a_sid")) & "' as a_sbrm,"
            SQ = SQ & " 1 as a_sbrmcrp,"
            SQ = SQ & " " & S2D(MIK) & " as a_sbrmmik,"
            SQ = SQ & " " & S2D(BRFIY) & " as a_sbrmfiy,"
            SQ = SQ & " " & SUBEID & " as a_sube_id;"

            Call SQL_KOS(SQ, False)

        Next

#End Region


        ' Fatura Başlığı Ekleniyor (Stkcmas)
#Region "Fatura"
        ' R1 İçerisine Belge Başlık Ekleniyor
        SQ = ""
        SQ = "INSERT INTO " & KUL.tper & "stkcmas"
        SQ = SQ & " (a_tur, a_id, a_kod, a_bno, a_ack, a_tarih, a_cari_id, a_ambar_id, a_giskonto, a_isktop, a_kdvtop, a_tuttop, a_cuser, a_cdate, a_ctime, a_muser, a_mdate, a_mtime, a_bloke, a_kesin, a_fcari, a_fadres1, a_fadres2, a_fvno, a_fvda, a_ka, a_irsaliye, a_irsaliyetar, a_vadetarihi, a_fattur, a_katur, a_kayer, a_taksay, a_dkatur, a_dkayer, a_dtaksay, a_harpuan, a_kdvtevora, a_kdvtevtut, a_otvtop, a_sube_id, a_var_id) "
        SQ = SQ & " Select "
        SQ = SQ & " 10 as a_tur,"  ' PERAKENDE SATIŞ FATURASI
        SQ = SQ & " " & ID & " as a_id,"
        SQ = SQ & " 'M:" & ADISYON_ID & "' as a_kod,"

        If ADISYON_ID = 0 Then
            SQ = SQ & " 'M:" & MASA_ID & "' as a_bno,"
        Else
            SQ = SQ & " 'M:" & ADISYON_ID & "' as a_bno,"
        End If

        SQ = SQ & " '" & MASA_ID & " Nolu masanın " & ADISYON_ID & " Nolu adisyonudur.' as a_ack,"
        SQ = SQ & " '" & TTAR.TR2UK(Date.Now.ToShortDateString) & "' as a_tarih,"
        SQ = SQ & " " & 1 & " as a_cari_id,"
        SQ = SQ & " " & 1 & " as a_ambar_id,"

        SQ = SQ & " 0 as a_giskonto,"
        SQ = SQ & " " & S2D(ISKTOP) & " as a_isktop,"
        SQ = SQ & " " & S2D(KDVTOP) & " as a_kdvtop,"
        SQ = SQ & " " & S2D(TUTTOP) & " as a_tuttop,"

        ' pda personel tablosu ile çalıştığı için user ide 0 olacak
        SQ = SQ & " " & 0 & " as a_cuser,"
        SQ = SQ & " '" & TTAR.TR2UK(Date.Now.ToShortDateString) & "' as a_cdate,"
        SQ = SQ & " '" & Date.Now.ToShortTimeString & "' as a_ctime,"
        SQ = SQ & " " & 0 & " as a_muser,"
        SQ = SQ & " '" & TTAR.TR2UK(Date.Now.ToShortDateString) & "' as a_mdate,"
        SQ = SQ & " '" & Date.Now.ToShortTimeString & "' as a_mtime,"
        SQ = SQ & " 0 as a_bloke,"
        SQ = SQ & " " & kesin & " as a_kesin,"
        SQ = SQ & " '' as a_fcari,"
        SQ = SQ & " ' ' as a_fadres1,"
        SQ = SQ & " ' ' as a_fadres2,"
        SQ = SQ & " ' ' as a_fvno,"
        SQ = SQ & " ' ' as a_fvda,"

        If TIP = "Açık" Then
            SQ = SQ & " 'A' as a_ka,"
        End If
        If TIP = "Kapalı" Then
            SQ = SQ & " 'K' as a_ka,"
        End If

        SQ = SQ & " ' ' as a_irsaliye,"
        SQ = SQ & " '" & TTAR.TR2UK(Date.Now.ToShortDateString) & "' as a_irsaliyetar,"
        SQ = SQ & " '" & TTAR.TR2UK(Date.Now.ToShortDateString) & "' as a_vadetarihi,"
        SQ = SQ & " 0 as a_fattur,"


        If Ktur = "NKAPAT" Then
            SQ = SQ & " " & 1 & " as a_katur,"
            SQ = SQ & " " & KASA_ID & " as a_kayer,"
        End If

        If Ktur = "KKAPAT" Then
            SQ = SQ & " " & 2 & " as a_katur,"
            SQ = SQ & " " & BANKA_ID & " as a_kayer,"
        End If

        If Ktur = "YKAPAT" Then
            SQ = SQ & " " & 3 & " as a_katur,"
            SQ = SQ & " " & 0 & " as a_kayer,"

        End If

        SQ = SQ & " " & 1 & " as a_taksay,"

        SQ = SQ & " " & 0 & " as a_dkatur,"
        SQ = SQ & " " & 0 & " as a_dkayer,"
        SQ = SQ & " " & 0 & " as a_dtaksay,"
        SQ = SQ & " " & 0 & " as a_harpuan,"

        SQ = SQ & " 0 as a_kdvtevora,"
        SQ = SQ & " 0 as a_kdvtevtut,"
        SQ = SQ & " 0 as a_otvtop,"
        SQ = SQ & " " & SUBEID & " as a_sube_id,"
        SQ = SQ & " " & VARDIYA_ID & " as a_var_id;"
#End Region


        If SQL_KOS(SQ, False) = True Then

            ' Adisyon Kapat Güncelleniyor
            SQ = ""
            SQ = "UPDATE " & KUL.tfrm & "men_adskapat"
            SQ = SQ & " SET "
            SQ = SQ & " a_fatid = " & ID & ", "
            SQ = SQ & " a_fattur = 10, "
            SQ = SQ & " a_adsnosu = " & ADISYON_ID & ", "
            SQ = SQ & " a_var_id = " & VARDIYA_ID & " "
            SQ = SQ & " WHERE (a_adid = " & ADID & ")"
            SQL_KOS(SQ, False)

            ' Eklenen Belgenin Tahsilatları Ekleniyor.
            belge_yansit(kesin, ID, TIP, Ktur, 0, MASA_ID, ADISYON_ID, ADID, ADBOL, TUTTOP, VARDIYA_ID)

            ' Eklenen Belge Onaylanıyor
            stok_yansit(kesin, ID)

            ' Tahsilatlar Aktarıldıktan Sonra Adisyon Tablosundan Siliniyor.
            SQ = "DELETE "
            SQ = SQ & " FROM " & KUL.tfrm & "men_masa_tahsil"
            SQ = SQ & " WHERE(a_masid = " & MASA_ID & ") AND (a_master_id = " & ADISYON_ID & ")"
            SQL_KOS(SQ, False)

            ' Adisyon Kapat Tablosundaki Boş Satır Siliniyor.
            SQ = ""
            SQ = "DELETE "
            SQ = SQ & " FROM " & KUL.tfrm & "men_adskapat "
            SQ = SQ & " WHERE (a_adid = " & ADID & ") AND (a_adsira = 0)"
            SQL_KOS(SQ, False)

        End If

    End Sub




    Public Sub STOK_ISLE(ByRef UG_51 As Long, ByRef UC_52 As Long, ByVal DEPO As Long, ByVal Akod As String, ByVal UMik As Double)


        Dim SQ As String
        Dim DT As New DataTable
        Dim IDD As Long
        Dim X As Integer


        'Üretime Çıkış
        UC_52 = TSAY.MAX_FIS_SC(52, 1)
        IDD = UC_52

        SQ = ""
        SQ = "INSERT INTO " & KUL.tper & "stkhmas"
        SQ = SQ & " (a_gc, a_tur, a_id, a_kod, a_bno, a_ack, a_tarih, a_cari_id, a_ambar_id, a_isktop, a_kdvtop, a_tuttop, a_cuser, a_cdate, a_ctime, a_muser, a_mdate, a_mtime, a_bloke, a_kesin) "
        SQ = SQ & " Select "
        SQ = SQ & " 'C' as a_gc,"
        SQ = SQ & " " & 52 & " as a_tur,"
        SQ = SQ & " " & IDD & " as a_id,"
        SQ = SQ & " N'UCF-" & IDD & "' as a_kod,"
        SQ = SQ & " N'UCF-" & IDD & "' as a_bno,"
        SQ = SQ & " N'Üretim İçin Mal Sarfiyatı' as a_ack,"
        SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & "' as a_tarih,"
        SQ = SQ & " '' as a_cari_id,"
        SQ = SQ & " " & DEPO & " as a_ambar_id,"
        SQ = SQ & " 0 as a_isktop,"
        SQ = SQ & " 0 as a_kdvtop,"
        SQ = SQ & " 0 as a_tuttop,"
        SQ = SQ & " " & 1 & " as a_cuser,"
        SQ = SQ & " '" & TTAR.TR2UK(Date.Now.ToShortDateString) & "' as a_cdate,"
        SQ = SQ & " '" & Date.Now.ToShortTimeString & "' as a_ctime,"
        SQ = SQ & " " & 1 & " as a_muser,"
        SQ = SQ & " '" & TTAR.TR2UK(Date.Now.ToShortDateString) & "' as a_mdate,"
        SQ = SQ & " '" & Date.Now.ToShortTimeString & "' as a_mtime,"
        SQ = SQ & " 0 as a_bloke,"
        SQ = SQ & " 0 as a_kesin;"

        If SQL_KOS(SQ) = True Then

            SQ = ""
            SQ = "SELECT " & KUL.tper & "uagacmas.a_tur, " & KUL.tper & "uagacmas.a_id, " & KUL.tper & "uagacmas.a_kod, " & KUL.tper & "uagacmas.a_sipadi, " & KUL.tper & "uagacmas.a_adet, " & KUL.tper & "uagacmas.a_cari_id, "
            SQ = SQ & " " & KUL.tper & "uagacdet.a_stok_id , " & KUL.tper & "uagacdet.a_mik"
            SQ = SQ & " FROM " & KUL.tper & "uagacmas INNER JOIN"
            SQ = SQ & " " & KUL.tper & "uagacdet ON " & KUL.tper & "uagacmas.a_tur = " & KUL.tper & "uagacdet.a_tur AND "
            SQ = SQ & " " & KUL.tper & "uagacmas.a_id = " & KUL.tper & "uagacdet.a_id"
            SQ = SQ & " WHERE     (" & KUL.tper & "uagacmas.a_kod = '" & Akod & "')"
            DT = SQL_TABLO(SQ)

            For X = 0 To DT.Rows.Count - 1
                SQ = ""
                SQ = "INSERT INTO " & KUL.tper & "stkhdet"
                SQ = SQ & " (a_gc, a_tur, a_id, a_sira, a_tarih, a_cari_id, a_stok_id, a_ambar_id, a_mik, a_brmfiy, a_kdv, a_kdvtut, a_isk, a_isktut, a_tutar, a_ack, a_bloke, a_kesin) "
                SQ = SQ & " Select "
                SQ = SQ & " 'C' as a_gc,"
                SQ = SQ & " " & 52 & " as a_tur,"
                SQ = SQ & " " & IDD & " as a_id,"
                SQ = SQ & " " & X & " as a_sira,"
                SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & "' as a_tarih,"
                SQ = SQ & " 0 as a_cari_id,"
                SQ = SQ & " " & NULN(DT, X, "a_stok_id") & " as a_stok_id,"
                SQ = SQ & " " & DEPO & " as a_ambar_id,"
                SQ = SQ & " " & S2D(Val(NULD(DT, X, "a_mik")) * UMik) & " as a_mik,"
                SQ = SQ & " 0 as a_brmfiy,"
                SQ = SQ & " 0 as a_kdv,"
                SQ = SQ & " 0 as a_kdvtut,"
                SQ = SQ & " 0 as a_isk,"
                SQ = SQ & " 0 as a_isktut,"
                SQ = SQ & " 0 as a_tutar,"
                SQ = SQ & " '' as a_ack,"
                SQ = SQ & " 0 as a_bloke,"
                SQ = SQ & " 0 as a_kesin;"

                If SQL_KOS(SQ) = True Then
                    BG.stok_miktar(NULN(DT, X, "a_stok_id"), Val(NULD(DT, X, "a_mik")) * UMik, "C+")
                    BG.depo_stok_miktar(DEPO, NULN(DT, X, "a_stok_id"), Val(NULD(DT, X, "a_mik")) * UMik, "C+")
                End If
            Next


            'Üretimden Giriş
            UG_51 = TSAY.MAX_FIS_SG(51, 1)
            IDD = UG_51

            SQ = ""
            SQ = "INSERT INTO " & KUL.tper & "stkhmas"
            SQ = SQ & " (a_gc, a_tur, a_id, a_kod, a_bno, a_ack, a_tarih, a_cari_id, a_ambar_id, a_isktop, a_kdvtop, a_tuttop, a_cuser, a_cdate, a_ctime, a_muser, a_mdate, a_mtime, a_bloke, a_kesin) "
            SQ = SQ & " Select "
            SQ = SQ & " 'G' as a_gc,"
            SQ = SQ & " " & 51 & " as a_tur,"
            SQ = SQ & " " & IDD & " as a_id,"
            SQ = SQ & " N'UGF-" & IDD & "' as a_kod,"
            SQ = SQ & " N'UGF-" & IDD & "' as a_bno,"
            SQ = SQ & " N'Üretimden Giriş' as a_ack,"
            SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & "' as a_tarih,"
            SQ = SQ & " '' as a_cari_id,"
            SQ = SQ & " " & DEPO & " as a_ambar_id,"
            SQ = SQ & " 0 as a_isktop,"
            SQ = SQ & " 0 as a_kdvtop,"
            SQ = SQ & " 0 as a_tuttop,"
            SQ = SQ & " " & 1 & " as a_cuser,"
            SQ = SQ & " '" & TTAR.TR2UK(Date.Now.ToShortDateString) & "' as a_cdate,"
            SQ = SQ & " '" & Date.Now.ToShortTimeString & "' as a_ctime,"
            SQ = SQ & " " & 1 & " as a_muser,"
            SQ = SQ & " '" & TTAR.TR2UK(Date.Now.ToShortDateString) & "' as a_mdate,"
            SQ = SQ & " '" & Date.Now.ToShortTimeString & "' as a_mtime,"
            SQ = SQ & " 0 as a_bloke,"
            SQ = SQ & " 0 as a_kesin;"
            If SQL_KOS(SQ) = True Then
                SQ = ""
                SQ = "INSERT INTO " & KUL.tper & "stkhdet"
                SQ = SQ & " (a_gc, a_tur, a_id, a_sira, a_tarih, a_cari_id, a_stok_id, a_ambar_id, a_mik, a_brmfiy, a_kdv, a_kdvtut, a_isk, a_isktut, a_tutar, a_ack, a_bloke, a_kesin) "
                SQ = SQ & " Select "
                SQ = SQ & " 'G' as a_gc,"
                SQ = SQ & " " & 51 & " as a_tur,"
                SQ = SQ & " " & IDD & " as a_id,"
                SQ = SQ & " " & 1 & " as a_sira,"
                SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & "' as a_tarih,"
                SQ = SQ & " 0 as a_cari_id,"
                SQ = SQ & " " & NULN(DT, 0, "a_cari_id") & " as a_stok_id,"
                SQ = SQ & " " & DEPO & " as a_ambar_id,"
                SQ = SQ & " " & S2D(Val(NULD(DT, 0, "a_adet")) * UMik) & " as a_mik,"
                SQ = SQ & " 0 as a_brmfiy,"
                SQ = SQ & " 0 as a_kdv,"
                SQ = SQ & " 0 as a_kdvtut,"
                SQ = SQ & " 0 as a_isk,"
                SQ = SQ & " 0 as a_isktut,"
                SQ = SQ & " 0 as a_tutar,"
                SQ = SQ & " '" & "" & "' as a_ack,"
                SQ = SQ & " 0 as a_bloke,"
                SQ = SQ & " 0 as a_kesin;"

                If SQL_KOS(SQ) = True Then
                    BG.stok_miktar(NULN(DT, 0, "a_cari_id"), Val(NULD(DT, 0, "a_adet")) * UMik, "G+")
                    BG.depo_stok_miktar(DEPO, NULN(DT, 0, "a_cari_id"), Val(NULD(DT, 0, "a_adet")) * UMik, "G+")
                End If
            End If
        End If

    End Sub

    Public Sub uretim_emri(ByVal DT2 As DataTable, ByVal D_Id As Long)

        Dim SQ As String = ""

        Dim DEPO As Long = 0
        Dim TUR As Long = 64
        Dim TGC As String = ""
        Dim C_ID As Long = 0

        Dim IDD As Long = 0
        Dim UG_51 As Long = 0
        Dim UC_52 As Long = 0
        Dim K_ID As Long = 0

        Dim X As Integer = 0
        Dim DT As New DataTable


        DEPO = D_Id
        TUR = 64
        TGC = Trim(VT_BILGI(KUL.tper & "maxid", "a_id", "a_gc", TUR))



        For X = 0 To DT2.Rows.Count - 1

            SQ = ""
            SQ = "SELECT a_tur, a_id, a_kod, a_cari_id"
            SQ = SQ & " FROM " & KUL.tper & "uagacmas"
            SQ = SQ & " WHERE a_cari_id = " & NULN(DT2, X, "a_sid") & ""
            DT = SQL_TABLO(SQ, "UA", False, False, False)
            If DT.Rows.Count <> 0 Then

                '------------------------------------------
                UG_51 = 0   ' Üretim Giriş
                UC_52 = 0   ' Üretim Çıkış

                C_ID = NULN(DT, 0, "a_id")

                STOK_ISLE(UG_51, UC_52, DEPO, NULA(DT, 0, "a_kod"), Val(NULD(DT2, X, "a_mik")))


                '------------------------------------------
                'Üretim Giriş / Çıkış Fişlerini Kes ID leri bul
                '------------------------------------------

                K_ID = TSAY.MAX_FIS_URETGC(64, 1)

                SQ = ""
                SQ = "INSERT INTO " & KUL.tper & "uretimgc ( a_tur, a_id, a_gc, a_cari_id, a_kasa_id, a_banka_id, a_islemno, a_belgeno, a_ref_id, a_tarih, a_ack, a_mebla, a_yer, a_yerid, a_cuser, a_cdate, a_ctime, a_muser, a_mdate, a_mtime )"
                SQ = SQ & " SELECT "
                SQ = SQ & " " & TUR & " AS a_tur, "
                SQ = SQ & " " & K_ID & " AS a_id, "
                SQ = SQ & " '" & TGC & "' AS a_gc, "
                SQ = SQ & " " & C_ID & " AS a_cari_id, "
                SQ = SQ & " " & UG_51 & " AS a_kasa_id, "
                SQ = SQ & " " & UC_52 & " AS a_banka_id, "
                SQ = SQ & " N'" & "ÜRT-" & K_ID & "' AS a_islemno, "
                SQ = SQ & " N'" & "ÜRT-" & K_ID & "' AS a_belgeno, "
                SQ = SQ & "  " & C_ID & "  AS a_ref_id, "
                SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & "' AS a_tarih, "
                SQ = SQ & " '" & "" & "' AS a_ack, "
                SQ = SQ & " " & S2D(Val(NULD(DT2, X, "a_mik"))) & " AS a_mebla, "
                SQ = SQ & " N'URETIM' AS a_yer, "
                SQ = SQ & " " & IDD & " AS a_yerid, "
                SQ = SQ & " '" & 1 & "' AS a_cuser, "
                SQ = SQ & " '" & TTAR.TR2UK(Date.Now.ToShortDateString).ToString & "' AS a_cdate, "
                SQ = SQ & " '" & Date.Now.ToShortTimeString & "' AS a_ctime, "
                SQ = SQ & " '" & 1 & "' AS a_muser, "
                SQ = SQ & " '" & TTAR.TR2UK(Date.Now.ToShortDateString).ToString & "' AS a_mdate, "
                SQ = SQ & " '" & Date.Now.ToShortTimeString & "' AS a_mtime;"

                If SQL_KOS(SQ, False) = True Then
                End If

            End If


        Next


    End Sub

    Public Sub Masaya_Sil(ByVal VERI As String)

        Dim SQ As String = ""

        Dim DT As New DataTable
        'Dim DR As New DataTable
        Dim DT2 As New DataTable
        Dim TDT As New DataTable
        Dim KDOS As String = ""
        Dim KID As Long = 0
        Dim EB_SIL As Long = 0
        Dim AdsYazdir As Integer = 0

        Dim X_MASA As Long = 0
        Dim X_SIRA As Long = 0
        Dim X_URUN As Long = 0
        Dim X_PDA As String = ""
        Dim X_GARSON As Long = 0
        Dim X_MIKTAR As Double = 0
        Dim X_ADS_NO As Long = 0
        Dim X_NEDEN As Long = 0
        Dim X_KALAN As Double = 0
        Dim X_PORSIYON As Double = 0

        Dim P1 As Long = 0
        Dim P2 As Long = 0
        Dim P3 As Long = 0
        Dim P4 As Long = 0
        Dim P5 As Long = 0
        Dim P6 As Long = 0
        Dim P7 As Long = 0
        Dim P8 As Long = 0
        Dim P9 As Long = 0
        Dim P10 As Long = 0


        'sq = "MASADANSIL "
        'sq = sq & " MASA=" & Val(Label10.Text)
        'sq = sq & " SIRA=" & Val(Label11.Text)
        'sq = sq & " URUN=" & Val(Label5.Tag)
        'sq = sq & " PDA=" & PDA_AD
        'sq = sq & " GARSON=" & PERSON_ID
        'sq = sq & " MIKTAR=" & Miktar
        'sq = sq & " ADS_NO=" & Val(L_AdsNo.Text)
        'sq = sq & " NEDEN=" & ComboBox1.SelectedValue
        'SQ = SQ & " KALAN=" & Val(Label6.Tag) - Miktar
        'SQ = SQ & " PORSI=" & Val(Label6.Tag) 

        P1 = InStr(VERI, "MASA=")
        P2 = InStr(VERI, "SIRA=")
        P3 = InStr(VERI, "URUN=")
        P4 = InStr(VERI, "PDA=")
        P5 = InStr(VERI, "GARSON=")
        P6 = InStr(VERI, "MIKTAR=")
        P7 = InStr(VERI, "ADS_NO=")
        P8 = InStr(VERI, "NEDEN=")
        P9 = InStr(VERI, "KALAN=")
        P10 = InStr(VERI, "PORSI=")


        X_MASA = Val(Trim(Mid(VERI, P1 + 5, P2 - (P1 + 5))))
        X_SIRA = Val(Trim(Mid(VERI, P2 + 5, P3 - (P2 + 5))))
        X_URUN = Val(Trim(Mid(VERI, P3 + 5, P4 - (P3 + 5))))
        X_PDA = Trim(Mid(VERI, P4 + 4, P5 - (P4 + 4)))
        X_GARSON = Val(Trim(Mid(VERI, P5 + 7, P6 - (P5 + 7))))
        X_MIKTAR = Val(Trim(Mid(VERI, P6 + 7, P7 - (P6 + 7))))
        X_ADS_NO = Val(Trim(Mid(VERI, P7 + 7, P8 - (P7 + 7))))
        X_NEDEN = Val(Trim(Mid(VERI, P8 + 6, P9 - (P8 + 6))))
        X_KALAN = Val(Trim(Mid(VERI, P9 + 6, P10 - (P9 + 6))))
        X_PORSIYON = Val(Trim(Mid(VERI, P10 + 6, Len(VERI))))

        If X_PORSIYON <= 0 Then X_PORSIYON = 1

        EB_SIL = Max_Bul(2)

        'ADISYONS_NOSU = NULN(DR, X, "a_adsnosu")
        'AdsYazdir = NULN(DR, X, "a_adsbas")

        SQ = "SELECT a_mid, a_sira, a_sid, a_mik, a_brf, a_tut, a_cid, a_subeid, a_mutfakid, a_adet, a_porsiyon, a_master_id"
        SQ = SQ & " FROM " & KUL.tfrm & "men_adisyon"
        SQ = SQ & " WHERE (a_mid = " & X_MASA & ") AND (a_sira = " & X_SIRA & ")"
        DT = SQL_TABLO(SQ, "T", False, False, False)


        SQ = ""
        SQ = "SELECT a_kid, a_tur, a_sql, a_aciklama, a_ondeger, a_yazdosya"
        SQ = SQ & " FROM " & KUL.tfrm & "fisdizmas"
        SQ = SQ & " WHERE(a_tur =92) AND a_aciklama=N'IPTAL'"
        TDT = SQL_TABLO(SQ, "T", False, False, False)

        SQ = "INSERT INTO " & KUL.tfrm & "men_silinen (a_id, a_sirax, a_adno, a_cid, a_mid, a_sira, a_sid, a_mik, "
        SQ = SQ & "  a_tarih, a_saat, a_userid, a_sebepid, a_adsyaz, a_master_id, a_subeid, a_personel)"
        SQ = SQ & " VALUES "
        SQ = SQ & " ( "
        SQ = SQ & " " & EB_SIL & ", "
        SQ = SQ & " " & X_SIRA & ", "
        SQ = SQ & " '" & X_ADS_NO & "', "
        SQ = SQ & " " & 1 & ", "
        SQ = SQ & " " & X_MASA & ", "
        SQ = SQ & " " & X_SIRA & ", "
        SQ = SQ & " " & X_URUN & ", "
        SQ = SQ & " " & X_MIKTAR & ", "
        SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & "', "
        SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & " " & TTAR.SAAT & "', "
        SQ = SQ & " " & -X_GARSON & ", "
        SQ = SQ & " " & X_NEDEN & " ,"
        SQ = SQ & " " & AdsYazdir & ","
        SQ = SQ & " " & X_ADS_NO & ","
        SQ = SQ & " " & NULN(DT, 0, "a_subeid") & ","
        SQ = SQ & " " & X_GARSON & ""
        SQ = SQ & " ) "


        If SQL_KOS(SQ, False) = True Then
            If TDT.Rows.Count = 0 Then
                Call Mutfak_Yazdir_Iptal(X_MASA, X_URUN, X_MIKTAR, X_GARSON, X_ADS_NO)
            Else
                Mutfak_Yazdir_Iptal_Matbu(NULN(TDT, 0, "a_kid"), NULA(TDT, 0, "a_sql"), X_MASA, EB_SIL, X_SIRA, X_ADS_NO)
            End If
        End If

        If X_KALAN = 0 Then
            SQ = ""
            SQ = "DELETE FROM " & KUL.tfrm & "men_adisyon"
            SQ = SQ & " WHERE (a_mid = " & X_MASA & ") AND (a_sira = " & X_SIRA & ")"
            Call SQL_KOS(SQ, False)
        Else
            SQ = ""
            SQ = "UPDATE " & KUL.tfrm & "men_adisyon "
            SQ = SQ & " SET "
            SQ = SQ & " a_mik = " & X_KALAN & ", "
            SQ = SQ & " a_adet = " & X_KALAN / X_PORSIYON & ", "


            SQ = SQ & " a_tut = a_brf * " & X_KALAN
            SQ = SQ & " WHERE "
            SQ = SQ & " a_mid  = " & X_MASA & " AND "
            SQ = SQ & " a_sira = " & X_SIRA & " "
            Call SQL_KOS(SQ, False)
        End If

        SQ = "DELETE "
        SQ = SQ & " FROM " & KUL.tfrm & "men_pdasiparissil"
        SQ = SQ & " WHERE "
        SQ = SQ & " (a_pda    = '" & X_PDA & "') AND "
        SQ = SQ & " (a_mid    = " & X_MASA & ")  "

        SQL_KOS(SQ, False)

        SQ = ""
        SQ = "SELECT a_mid,a_sid "
        SQ = SQ & " FROM " & KUL.tfrm & "men_adisyon "
        SQ = SQ & " WHERE "
        SQ = SQ & " a_mid  = " & X_MASA & " AND a_master_id =" & X_ADS_NO
        DT2 = SQL_TABLO(SQ, "T", False, False, False)

        If DT2.Rows.Count = 0 Then

            SQ = ""
            SQ = "UPDATE " & KUL.tfrm & "men_adisyonmas"
            SQ = SQ & " SET "
            SQ = SQ & " a_durum=1"
            SQ = SQ & " WHERE a_id=" & X_ADS_NO
            Call SQL_KOS(SQ, False)


            SQ = ""
            SQ = "SELECT a_mid,a_sid "
            SQ = SQ & " FROM " & KUL.tfrm & "men_adisyon "
            SQ = SQ & " WHERE "
            SQ = SQ & " a_mid  = " & X_MASA & " AND a_master_id <>" & X_ADS_NO
            DT2 = SQL_TABLO(SQ, "T", False, False, False)

            If DT2.Rows.Count = 0 Then
                SQ = ""
                SQ = "UPDATE " & KUL.tfrm & "men_masa "
                SQ = SQ & " SET "
                SQ = SQ & " a_kisisay = 0, "
                SQ = SQ & " a_durum = 'BOŞ'"
                SQ = SQ & " WHERE "
                SQ = SQ & " a_id  = " & X_MASA
                Call SQL_KOS(SQ, False)
            End If
        End If







    End Sub

    Public Sub Mutfak_Yazdir_Iptal_Matbu(ByVal KID As Integer, ByVal KDOS As String, ByVal Masa_ID As Long, ByVal Sil_ID As Long, ByVal Sira As Long, ByVal Ads_ID As Long)
        Dim PS As New sinifmatbu
        Dim YAZICIYOLU As String = ""
        Dim B As String = ""
        Dim SQ As String = ""
        Dim DM As DataTable
        Dim MUTFAKID As Long

        SQ = ""
        SQ = "SELECT     a_mid, a_sid, a_subeid"
        SQ = SQ & " FROM  " & KUL.tfrm & "men_silinen"
        SQ = SQ & " WHERE a_id=" & Sil_ID
        DM = SQL_TABLO(SQ, "T", False, False, False)


        MUTFAKID = MUTFAK_BUL(NULN(DM, 0, "a_sid"), NULN(DM, 0, "a_subeid"), NULN(DM, 0, "a_mid"))

        SQ = ""
        SQ = "SELECT     Adisyon.a_mutfakid, Mutfak.a_yazyol , Adisyon.a_subeid"
        SQ = SQ & " FROM  " & KUL.tfrm & "men_adisyon AS Adisyon INNER JOIN"
        SQ = SQ & " " & KUL.tfrm & "men_mutfak AS Mutfak ON Adisyon.a_mutfakid = Mutfak.a_id"
        SQ = SQ & " WHERE     (Adisyon.a_mutfakid = " & MUTFAKID & ") "
        SQ = SQ & " GROUP BY Adisyon.a_mutfakid, Mutfak.a_yazyol , Adisyon.a_subeid"
        DM = SQL_TABLO(SQ, "T", False, False, False)

        If DM.Rows.Count <> 0 Then
            B = PS.MUTFAK_YAZDIR(KID, KDOS, Masa_ID, Sil_ID, Ads_ID)

            B = TURK(B, 0)


            Dim K As String = ""
            K = KUL.KOD & "-" & Now.Year & "-" & Now.Month & "-" & Now.Day & " " & Now.Hour & ":" & Now.Minute & ":" & Now.Second & ":" & sifre_uret(8)

            SQ = ""
            SQ = "INSERT INTO " & KUL.tfrm & "men_pserver (a_id, a_tur, a_pda, a_user, a_tarih, a_saat, a_komut, a_veri, a_durum, a_sube, a_masa)"
            SQ = SQ & " VALUES "
            SQ = SQ & " ( "
            SQ = SQ & " '" & K & "', "
            SQ = SQ & " '" & "SİPARİŞ" & "', "
            SQ = SQ & " '', "
            SQ = SQ & " 0 , "
            SQ = SQ & " N'" & TTAR.TR2UK(TTAR.TARIH) & "', "
            SQ = SQ & " N'" & TTAR.TR2UK(TTAR.TARIH) & " " & TTAR.SAAT & "', "
            SQ = SQ & " N'" & NULA(DM, 0, "a_yazyol") & "', "
            SQ = SQ & " N'" & B & "', "
            SQ = SQ & " N'" & "H" & "', "
            SQ = SQ & "  " & NULA(DM, 0, "a_subeid") & " , "
            SQ = SQ & "  " & Masa_ID & " "
            SQ = SQ & " ) "

            Call SQL_KOS(SQ, False)
        End If

    End Sub

    Public Sub Mutfak_Yazdir_Iptal(ByVal Masa_Id As Long, ByVal SID As Long, ByVal MIK As Double, ByVal UserIDD As Long, ByVal ADS_Id As Long)

        Dim SQ As String = ""

        Dim DM As New DataTable
        Dim DMU As New DataTable
        Dim DT As New DataTable

        Dim A As Integer = 0
        Dim Z As Integer = 0
        Dim W As Integer = 0

        Dim B As String = ""
        Dim MUTFAKID As Long
        Dim YAZ As String = ""
        Dim DSY As String = ""
        Dim SBID As Long = 0
        Dim SADI As String = ""

        SADI = VT_BILGI(KUL.tfrm & "stok", "a_id", "a_adi", SID)


        SQ = "SELECT a_id, a_kod, a_adi, a_sube "
        SQ = SQ & " FROM " & KUL.tfrm & "men_masa"
        SQ = SQ & " WHERE(a_id = " & Masa_Id & ")"
        DT = SQL_TABLO(SQ, "VERI", False, False, False)

        If NULN(DT, 0, "a_sube") <> 0 Then
            SBID = NULN(DT, 0, "a_sube")
        Else
            SQ = "SELECT a_yildiz, a_mid, a_sira, a_sid, a_mik, a_brf, a_tut, a_tarih, a_saat, a_userid, a_not, a_durum, a_yer, a_cid, a_subeid, a_mutfakid"
            SQ = SQ & " FROM " & KUL.tfrm & "men_adisyon"
            SQ = SQ & " WHERE(a_mid = " & Masa_Id & ") AND (a_sid = " & SID & ")"
            SQ = SQ & " AND (a_master_id = " & ADS_Id & ")"
            DT = SQL_TABLO(SQ, "VERI", False, False, False)
            SBID = NULN(DT, 0, "a_subeid")
        End If


        'SQ = "SELECT M.a_id, M.a_sube, M.a_yazyol, U.a_sid, M.a_adi"
        'SQ = SQ & " FROM " & KUL.tfrm & "men_mutfak AS M INNER JOIN"
        'SQ = SQ & " " & KUL.tfrm & "men_urunmutfak AS U ON M.a_id = U.a_mid"
        'SQ = SQ & " WHERE (M.a_sube = " & SBID & ") AND (U.a_sid = " & SID & ")"
        'DMU = SQL_TABLO(SQ, "T", False, False, False)

        MUTFAKID = MUTFAK_BUL(SID, SBID, Masa_Id)

        SQ = ""
        SQ = "SELECT     Adisyon.a_mutfakid, Mutfak.a_yazyol , Adisyon.a_subeid, Mutfak.a_adi"
        SQ = SQ & " FROM  " & KUL.tfrm & "men_adisyon AS Adisyon INNER JOIN"
        SQ = SQ & " " & KUL.tfrm & "men_mutfak AS Mutfak ON Adisyon.a_mutfakid = Mutfak.a_id"
        SQ = SQ & " WHERE     (Adisyon.a_mutfakid = " & MUTFAKID & ") "
        SQ = SQ & " GROUP BY Adisyon.a_mutfakid, Mutfak.a_yazyol , Adisyon.a_subeid, Mutfak.a_adi"
        DM = SQL_TABLO(SQ, "T", False, False, False)


        YAZ = NULA(DM, 0, "a_yazyol")



        B = ""
        'B = B & Yasla(NULA(DMU, 0, "a_ftel"), 40, "SOL") & vbCrLf
        'B = B & Yasla(NULA(DMU, 0, "a_adi"), 40, "SOL") & vbCrLf
        'B = B & Yasla(NULA(DMU, 0, "a_fadres1"), 40, "SOL") & vbCrLf
        'B = B & Yasla(NULA(DMU, 0, "a_fadres2"), 40, "SOL") & vbCrLf
        'B = B & Yasla(NULA(DMU, 0, "a_fadres3"), 40, "SOL") & vbCrLf
        B = B & "----------------------------------------- " & vbCrLf
        B = B & "!! ÜRÜN İPTAL FİŞİ !! " & vbCrLf
        B = B & "İPTAL Mutfak : " & NULA(DM, 0, "a_adi") & vbCrLf
        B = B & "İPTAL Masa No: " & NULA(DT, 0, "a_kod") & vbCrLf
        B = B & "Tarih          : " & Now.ToShortDateString & vbCrLf
        B = B & "Saat           : " & Now.ToLongTimeString & vbCrLf
        B = B & "Garson         : " & VT_BILGI("tbl_001_personel", "a_id", "a_adi", UserIDD) & vbCrLf
        B = B & "--------------------------- ---------- " & vbCrLf
        B = B & "İPTAL Edilen Ürün           İPTAL Adet " & vbCrLf
        B = B & "--------------------------- ---------- " & vbCrLf

        B = B & Yasla(SADI, 26, "SOL") & " "
        B = B & Yasla(Str(MIK), 10, "SAG") & vbCrLf

        B = B & "--------------------------- ---------- " & vbCrLf
        For W = 1 To 10
            B = B & "" & vbCrLf
        Next

        'Kesme Komutu
        If PRNcm0 <> 0 Then
            B = B & Chr(PRNcm0) & PRNcm1 & vbCrLf
        End If


        B = TURK(B, 0)

        Dim K As String = ""
        K = KUL.KOD & "-" & Now.Year & "-" & Now.Month & "-" & Now.Day & " " & Now.Hour & ":" & Now.Minute & ":" & Now.Second & ":" & sifre_uret(8)



        SQ = ""
        SQ = "INSERT INTO " & KUL.tfrm & "men_pserver (a_id, a_tur, a_pda, a_user, a_tarih, a_saat, a_komut, a_veri, a_durum, a_sube, a_masa)"
        SQ = SQ & " VALUES "
        SQ = SQ & " ( "
        SQ = SQ & " '" & K & "', "
        SQ = SQ & " '" & "SİPARİŞ" & "', "
        SQ = SQ & " '" & NULA(DM, 0, "a_pda") & "', "
        SQ = SQ & "  " & NULN(DM, 0, "Garson_Id") & " , "
        SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & "', "
        SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & " " & TTAR.SAAT & "', "
        SQ = SQ & " '" & YAZ & "', "
        SQ = SQ & " '" & B & "', "
        SQ = SQ & " '" & "H" & "', "
        SQ = SQ & "  " & SBID & " , "
        SQ = SQ & "  " & Masa_Id & " "
        SQ = SQ & " ) "
        Call SQL_KOS(SQ, False)

    End Sub

    Public Sub Masaya_Isle(ByVal VERI As String)


        Dim PDA_Adi As String
        Dim Masa_Id As Long
        Dim Ads_Id As Long
        Dim Ads_Not As String

        Dim P1 As Long = 0
        Dim P2 As Long = 0
        Dim P3 As Long = 0
        Dim P4 As Long = 0
        'MASAYAISLE MASA=" & Val(Label5.Tag) & " PDA=" & PDA_AD & " ADS_NO=" & PERSON_ID

        P1 = InStr(VERI, "MASA=")
        P2 = InStr(VERI, "PDA=")
        P3 = InStr(VERI, "ADS_NO=")
        p4 = InStr(VERI, "ADS_NOT=")
        '************************************** p5 gelcek veri içinde CARIID= 

        Masa_Id = Val(Trim(Mid(VERI, P1 + 5, P2 - (P1 + 5))))
        PDA_Adi = Trim(Mid(VERI, P2 + 4, P3 - (P2 + 4)))
        Ads_Id = Val(Trim(Mid(VERI, P3 + 7, P4 - (P3 + 7))))
        Ads_Not = Trim(Mid(VERI, P4 + 8, Len(VERI)))

        Dim SQ As String = ""

        Dim DT As New DataTable
        'Dim DR As New DataTable
        Dim DU As New DataTable
        Dim DM As New DataTable
        Dim DMU As New DataTable
        Dim DTM As New DataTable


        Dim EB As Long = 0
        Dim X As Integer = 0
        Dim Y As Integer = 0
        Dim Z As Integer = 0
        Dim A As Integer = 0
        Dim W As Integer = 0

        Dim B As String = ""
        Dim DSY As String = ""
        Dim YAZ As String = ""

        Dim SUBE As Integer = 0
        Dim ID_MUTFAK As Integer = 0
        Dim N As String = ""
        Dim DTS As New DataTable

        Dim ADSNO As Long = 0

        Dim KS As Long = 0


        Dim DTZ As New DataTable
        Dim YILDIZ As String = ""


        SQ = ""
        SQ = "SELECT a_pda, a_sira, a_mid, a_sid, a_adet, a_porsiyon, a_mik, a_brf, a_tut, a_not, a_tarih, a_saat, a_userid, a_yildiz,a_kisisay"
        SQ = SQ & " FROM " & KUL.tfrm & "men_pdasiparis"
        SQ = SQ & " WHERE "
        SQ = SQ & " (a_pda    = '" & PDA_Adi & "') AND "
        SQ = SQ & " (a_mid    = " & Masa_Id & ") AND "
        SQ = SQ & " (a_master_id=" & Ads_Id & ") AND"
        SQ = SQ & " (a_yildiz = '*') "
        'SQ = SQ & " ORDER BY a_sira"
        SQ = SQ & " ORDER BY a_not DESC, a_sira DESC"
        DU = SQL_TABLO(SQ, "T", False, False, False)
        If DU.Rows.Count = 0 Then
            Exit Sub
        End If



        SQ = "SELECT a_id, a_veri1, a_veri2, a_veri3, a_veri4, a_yazici, a_esc1, a_esc2, a_zaman, a_bekle"
        SQ = SQ & " FROM " & KUL.tfrm & "men_tanim "
        SQ = SQ & " WHERE a_id = 1"
        DT = SQL_TABLO(SQ, "T", False, False, False)
        PRNADI = NULA(DT, 0, "a_yazici")
        PRNcm0 = NULN(DT, 0, "a_esc1")
        PRNcm1 = NULA(DT, 0, "a_esc2")
        PRNbek = NULN(DT, 0, "a_bekle")




        SQ = ""
        SQ = "SELECT a_id, a_kod, a_adi, a_durum, a_adsnosu, a_sube, a_kisisay"
        SQ = SQ & " FROM " & KUL.tfrm & "men_masa"
        SQ = SQ & " WHERE(a_id = " & Masa_Id & ")"
        DTS = SQL_TABLO(SQ, "VERI", False, False, False)


        KS = 1


        If Ads_Id = 0 Then
            Dim AF As New Adisyon_Fisleri
            'AF.Kayit_ID_Nosu = 0
            AF.Kayit_Tarihi = TTAR.TARIH
            AF.Kayit_Saati = TTAR.SAAT
            AF.Kayit_Masa_Id = NULN(DU, 0, "a_mid")
            AF.Kayit_Sube_Id = NULN(DTS, 0, "a_sube")
            AF.Kayit_Notu = ""
            AF.Kayit_Cari_Id = 1 ' ***************************** cariid geleck
            'AF.Kayit_Gun_Sira = 0
            AF.Kayit_Notu = Ads_Not
            AF.Gunluk_Sayac_Hesapla()
            AF.Kayit_Fis_Nosu = ""
            If NULN(DU, 0, "a_kisisay") <> 0 Then
                AF.Kayit_Kisi_Sayisi = NULN(DU, 0, "a_kisisay")
                KS = NULN(DU, 0, "a_kisisay")
            Else
                AF.Kayit_Kisi_Sayisi = 1
                KS = 1
            End If
            AF.Kayit_Durum_Bilgisi = 0
            AF.Kayit_Kullanici_Bilgisi = -NULN(DU, 0, "a_userid")
            AF.Personel_Id = NULN(DU, 0, "a_userid")
            ADSNO = AF.Ekle()
        Else
            ADSNO = Ads_Id
        End If


        For Y = 0 To DU.Rows.Count - 1

            If Y = 0 Then
                SQ = ""
                SQ = "SELECT a_id, a_durum, a_adsnosu "
                SQ = SQ & " FROM " & KUL.tfrm & "men_masa"
                SQ = SQ & " WHERE(a_id = " & NULN(DU, 0, "a_mid") & ")"
                DT = SQL_TABLO(SQ, "VERI", False, False, False)

                If NULA(DT, 0, "a_durum") <> "DOLU" Then
                    SQ = "Update " & KUL.tfrm & "men_masa"
                    SQ = SQ & " SET a_durum = 'DOLU'"
                    SQ = SQ & " , a_acsaat = '" & TTAR.TR2UK(TTAR.TARIH) & " " & Now.ToShortTimeString & "'"
                    SQ = SQ & " , a_adsnosu = " & Max_Bul(1) & ""
                    SQ = SQ & " , a_kisisay = " & KS & " "
                    SQ = SQ & " WHERE(a_id = " & NULN(DU, 0, "a_mid") & ")"
                    Call SQL_KOS(SQ, False)
                End If

            End If








            SQ = ""
            SQ = "SELECT  MAX(a_sira) AS EB"
            SQ = SQ & " FROM " & KUL.tfrm & "men_adisyon"
            SQ = SQ & " WHERE(a_mid = " & NULN(DU, 0, "a_mid") & ")"
            DT = SQL_TABLO(SQ, "V", False, False, False)
            EB = NULN(DT, 0, "EB") + 1

            '------------------------
            'Durum Flagları.
            '------------------------
            '0-Bekleyen
            '1-Mutfakta
            '2-Tamamlanan
            '3-Sevkiyatta
            '4-Kapanan
            '------------------------


            ID_MUTFAK = MUTFAK_BUL(NULN(DU, Y, "a_sid"), NULN(DTS, 0, "a_sube"), NULN(DU, 0, "a_mid"))




            YILDIZ = "*"

            'Mutfakta Yazdırılsın mı ? 
            SQ = "SELECT a_mid, a_sid, a_noprn"
            SQ = SQ & " FROM " & KUL.tfrm & "men_urunmutfak"
            SQ = SQ & " WHERE (a_mid = " & ID_MUTFAK & ") AND (a_sid = " & NULN(DU, Y, "a_sid") & ")"
            DTZ = SQL_TABLO(SQ, "T", False, False, False)
            If NULN(DTZ, 0, "a_noprn") <> 0 Then
                YILDIZ = ""
            Else
                YILDIZ = "*"
            End If

            Dim CARI As New XCari.Cari_Kartlar
            Dim STOK As New STOK_BILGI

            CARI.Kayit_Bul_ID(1)

            Dim XF As New X_Fiyat_Bilgi
            Dim FIY As Double = 0
            Dim FIYISK As Double = 0
            Dim FIY2 As Double
            STOK.BUL(NULN(DU, Y, "a_sid"))



            XF.Fil_AL_SAT = "SAT"
            XF.Fil_Tur = Islem_Turu.a010_Perakende_Satis_Faturasi
            XF.Fil_F_List_Id = 0
            XF.Fil_Cari_Id = CARI.ID
            XF.Fil_Cari_Kod = CARI.Cari_Hesap_Kodu
            XF.Fil_Sube_Id = NULN(DTS, 0, "a_sube")
            XF.Fil_Isl_Tarih = TTAR.TARIH
            XF.Fil_STB = STOK
            XF.X_Fiyat_Getir()
            FIY = XF.RET_Fiyat
            FIY2 = XF.RET_Fiyat_Isk

            SQ = ""
            SQ = "INSERT INTO " & KUL.tfrm & "men_adisyon"
            SQ = SQ & " (a_yildiz, a_mid, a_sira, a_cid, a_sid, a_adet, a_porsiyon, a_mik, a_brf, a_tut, a_tarih, a_saat, a_userid, a_not, "
            SQ = SQ & " a_durum, a_yer, a_subeid, a_mutfakid, a_satdurum, a_iskoran, a_isktutar, a_master_id)"
            SQ = SQ & " VALUES ("
            SQ = SQ & " '" & YILDIZ & "',"
            SQ = SQ & " " & NULN(DU, 0, "a_mid") & ", "
            SQ = SQ & " " & EB & ", "
            SQ = SQ & " " & 1 & ", " ' CARI ID 
            SQ = SQ & " " & NULN(DU, Y, "a_sid") & ", "

            SQ = SQ & " " & S2D(NULD(DU, Y, "a_adet")) & ", "
            SQ = SQ & " " & S2D(NULD(DU, Y, "a_porsiyon")) & ", "

            SQ = SQ & " " & S2D(NULD(DU, Y, "a_mik")) & ", "
            SQ = SQ & " " & S2D(FIY) & ", "
            SQ = SQ & " " & S2D(FIY) * S2D(NULD(DU, Y, "a_mik")) & ", "

            SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & "', "
            SQ = SQ & " '" & Now.ToLongTimeString & "', "
            SQ = SQ & " " & NULN(DU, Y, "a_userid") & ","
            SQ = SQ & " '" & NULA(DU, Y, "a_not") & "',"
            SQ = SQ & " N'0-Bekleyen',"
            SQ = SQ & " N'Masa Adisyon PDA',"
            SQ = SQ & " " & NULN(DTS, 0, "a_sube") & ","
            SQ = SQ & " " & ID_MUTFAK & ","
            SQ = SQ & " " & 0 & " ," ' Satış Türü
            SQ = SQ & " " & ((FIY - FIY2) / FIY) * 100 & " ,"  ' iskoran
            SQ = SQ & " " & S2D(FIY - FIY2) * S2D(NULD(DU, Y, "a_mik")) & "   ,"   ' isktutar
            SQ = SQ & " " & ADSNO & " " ' Master ID
            SQ = SQ & " )"
            Call SQL_KOS(SQ, False)
        Next


        Dim TDT As New DataTable

        SQ = ""
        SQ = "SELECT a_kid, a_tur, a_sql, a_aciklama, a_ondeger, a_yazdosya"
        SQ = SQ & " FROM " & KUL.tfrm & "fisdizmas"
        SQ = SQ & " WHERE(a_tur =92) AND a_aciklama='MUTFAK'"
        TDT = SQL_TABLO(SQ, "T", False, False, False)

        If TDT.Rows.Count = 0 Then
            Call Mutfak_Yazdir(Masa_Id, ADSNO)
        Else
            Dim KID As Integer
            Dim KDOS As String
            Dim PS As New sinifmatbu

            KID = NULN(TDT, 0, "a_kid")
            KDOS = NULA(TDT, 0, "a_sql")

            SQ = ""
            SQ = "SELECT     Adisyon.a_mutfakid, Mutfak.a_yazyol ,  Adisyon.a_subeid"
            SQ = SQ & " FROM  " & KUL.tfrm & "men_adisyon AS Adisyon INNER JOIN"
            SQ = SQ & " " & KUL.tfrm & "men_mutfak AS Mutfak ON Adisyon.a_mutfakid = Mutfak.a_id"
            SQ = SQ & " WHERE     (Adisyon.a_mid = " & Masa_Id & ") AND (Adisyon.a_yildiz = '*') AND (Adisyon.a_master_id=" & ADSNO & ")"
            SQ = SQ & " GROUP BY Adisyon.a_mutfakid, Mutfak.a_yazyol, Adisyon.a_subeid"
            DM = SQL_TABLO(SQ, "T", False, False, False)


            For Z = 0 To DM.Rows.Count - 1


                B = PS.MUTFAK_YAZDIR(KID, KDOS, Masa_Id, NULD(DM, Z, "a_mutfakid"), ADSNO)

                B = TURK(B, 0)


                Dim K As String = ""
                K = KUL.KOD & "-" & Now.Year & "-" & Now.Month & "-" & Now.Day & " " & Now.Hour & ":" & Now.Minute & ":" & Now.Second & ":" & sifre_uret(8)

                SQ = ""
                SQ = "INSERT INTO " & KUL.tfrm & "men_pserver (a_id, a_tur, a_pda, a_user, a_tarih, a_saat, a_komut, a_veri, a_durum, a_sube, a_masa)"
                SQ = SQ & " VALUES "
                SQ = SQ & " ( "
                SQ = SQ & " '" & K & "', "
                SQ = SQ & " N'" & "SİPARİŞ" & "', "
                SQ = SQ & " N'', "
                SQ = SQ & " 0 , "
                SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & "', "
                SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & " " & TTAR.SAAT & "', "
                SQ = SQ & " N'" & NULA(DM, Z, "a_yazyol") & "', "
                SQ = SQ & " '" & B & "', "
                SQ = SQ & " '" & "H" & "', "
                SQ = SQ & "  " & NULN(DM, X, "a_subeid") & " , "
                SQ = SQ & "  " & Masa_Id & " "
                SQ = SQ & " ) "

                Call SQL_KOS(SQ, False)
            Next
        End If

        '******************************** YAZDIR BITTI *****************************************************
        'For Y = 0 To DU.Rows.Count - 1
        'Fiyat_Güncelleme(B_SQL, ADSNO, NULN(DU, Y, "a_sid"), NULN(DTS, 0, "a_sube"), 1, NULN(DU, Y, "a_sira"))
        'Next


        SQ = ""
        SQ = "UPDATE " & KUL.tfrm & "men_adisyon"
        SQ = SQ & " SET "
        SQ = SQ & " a_yildiz = ''"
        SQ = SQ & " WHERE (a_mid = " & Masa_Id & ")"
        Call SQL_KOS(SQ, False)


        SQ = "DELETE "
        SQ = SQ & " FROM " & KUL.tfrm & "men_pdasiparis"
        SQ = SQ & " WHERE "
        SQ = SQ & " (a_pda        = '" & PDA_Adi & "') AND "
        SQ = SQ & " (a_mid        = " & Masa_Id & ") AND "
        SQ = SQ & " (a_master_id  = " & Ads_Id & ") AND "
        SQ = SQ & " (a_yildiz = '" & "*" & "') "
        SQL_KOS(SQ, False)


    End Sub

    Public Function MUTFAK_BUL(ByVal STK As Long, ByVal Sube As Long, ByVal Masa_Id As Long) As Integer
        Dim SQ As String = ""
        Dim X As Integer = 0
        Dim DT As New DataTable
        Dim DTZ As New DataTable

        Dim DT1 As New DataTable
        Dim DT2 As New DataTable
        Dim DSQL As String = ""


        SQ = ""
        SQ = "SELECT a_id, a_menusirala, a_veri1, a_panfilt"
        SQ = SQ & " FROM " & KUL.tfrm & "men_tanim"
        DTZ = SQL_TABLO(SQ, "DF", False, False, False)


        'Ürünün Mutfağini Bul   Seçilen Şubedeki.
        '--------------------------------------------------------
        'SQ = ""
        'SQ = "Select a_mid"
        'SQ = SQ & " FROM men_urunmutfak"
        'SQ = SQ & " WHERE(a_sid = " & STK & ")"
        'SQ = SQ & " GROUP BY a_mid"

        SQ = "Select UMUT.a_mid"
        SQ = SQ & " FROM " & KUL.tfrm & "men_urunmutfak AS UMUT INNER JOIN"
        SQ = SQ & " " & KUL.tfrm & "men_mutfak AS MUT ON UMUT.a_mid = MUT.a_id"
        SQ = SQ & " WHERE (UMUT.a_sid = " & STK & ") "
        If NULN(DTZ, 0, "a_panfilt") <> 1 Then
            SQ = SQ & " AND (MUT.a_sube = " & Sube & ") "
        End If
        SQ = SQ & " GROUP BY UMUT.a_mid"
        DT1 = SQL_TABLO(SQ, "T", False, False, False)



        If DT1.Rows.Count = 0 Then
            MUTFAK_BUL = 0
            Exit Function
        End If

        If DT1.Rows.Count = 1 Then
            MUTFAK_BUL = NULA(DT1, 0, "a_mid")
            Exit Function
        End If

        'Müsait Mutfağı Bul
        If DT1.Rows.Count > 1 Then

            '----------------------------------------------------------------------------------------
            '17.12.2011 Adapazarı Elmas Otel Restorantları
            '----------------------------------------------------------------------------------------
            'Masanın Mekanının Geçerli Mutfagı var mı ? 
            '----------------------------------------------------------------------------------------
            SQ = ""
            SQ = "SELECT     masa.a_id, masa.a_yerid, masayer.a_gec_mut"
            SQ = SQ & " FROM "
            SQ = SQ & " " & KUL.tfrm & "men_masa AS masa INNER JOIN"
            SQ = SQ & " " & KUL.tfrm & "men_masayer AS masayer ON masa.a_yerid = masayer.a_id"
            SQ = SQ & " WHERE(Masa.a_id = " & Masa_Id & ")"
            DT = SQL_TABLO(SQ, "T", False, False, False)
            If NULN(DT, 0, "a_gec_mut") <> 0 Then
                MUTFAK_BUL = NULN(DT, 0, "a_gec_mut")
                Exit Function
            End If
            '----------------------------------------------------------------------------------------


            DSQL = ""
            For X = 0 To DT1.Rows.Count - 1
                DSQL = DSQL & " OR (A.a_mutfakid = " & NULN(DT1, X, "a_mid") & ")"
            Next

            'İşi En Az Olan Mutfağı Bul
            SQ = "SELECT A.a_mutfakid, SUM(S.a_xursur * A.a_mik) AS TOP_SURE"
            SQ = SQ & " FROM "
            SQ = SQ & " " & KUL.tfrm & "men_adisyon AS A "
            SQ = SQ & " INNER JOIN " & KUL.tfrm & "stok AS S ON A.a_sid = S.a_id"
            SQ = SQ & " WHERE (A.a_durum = '0-Bekleyen') OR (A.a_durum = '1-Mutfakta')"
            SQ = SQ & " GROUP BY A.a_mutfakid"
            SQ = SQ & " HAVING (A.a_mutfakid = -1) " & DSQL
            SQ = SQ & " ORDER BY SUM(S.a_xursur * A.a_mik)"
            DT2 = SQL_TABLO(SQ, "T", False, False, False)
            '-------------------------------------------------------------------------------------
            If DT2.Rows.Count = 0 Then
                MUTFAK_BUL = NULA(DT1, 0, "a_mid")
                Exit Function
            Else
                MUTFAK_BUL = NULA(DT2, 0, "a_mutfakid")
                Exit Function
            End If


        Else

            MUTFAK_BUL = NULA(DT1, 0, "a_mid")

        End If

    End Function


    Public Sub BASLA()

        Dim FIRMA As String = ""
        Dim MDONEM As String = ""
        Dim X As Integer = 0
        Dim S As Integer = 0

        'KUL.FIRMA = 1
        'FIRMA = 1
        'KUL.DONEM = 1
        'MDONEM = 1

        'If FIRMA.Length < 3 Then FIRMA = New String("0", 3 - FIRMA.Length) & FIRMA
        'If MDONEM.Length < 2 Then MDONEM = New String("0", 2 - MDONEM.Length) & MDONEM

        'KUL.tfrm = "tbl_" & FIRMA & "_"
        'KUL.tper = "tbl_" & FIRMA & "_" & MDONEM & "_"

        'FRM = "tbl_" & FIRMA & "_"
        'PER = "tbl_" & FIRMA & "_" & MDONEM & "_"

        DEPO_ID = 1
        USER_ID = 1
        PERS_ID = 1
        KASA_ID = 1


        If DGV.Rows.Count > 30 Then
            S = 30
        Else
            S = DGV.Rows.Count - 1
        End If

        For X = 0 To S
            TPoint = X
            Try
                DINLE(X).Start()
                Threading.Thread.Sleep(TSaniye * 1000)

            Catch ex As Exception
                DINLE(X) = New System.Threading.Thread(AddressOf Komut_Bekle)
                DINLE(X).Start()
                Threading.Thread.Sleep(TSaniye * 1000)

            End Try
        Next

    End Sub

    Public Sub Komut_Bekle()

        Dim SQ As String = ""
        Dim DT As New DataTable
        Dim S As String = ""
        Dim L As String = ""
        Dim X As Integer = 0
        Dim Y As Integer = 0
        Dim Data As String = ""
        Dim KOMUT As String = ""
        Dim SYOL As String = ""
        Dim SON As String = ""
        Dim SNCID As String = ""

        SetText("Bağlantı İçin Bekleniyor")
        SYOL = NULA(DGV, TPoint, "a_yol")
        SetText("Bağlandı")

        While True

            Try

                If File.Exists(SYOL & "komut.txt") = True Then

                    Dim sr As StreamReader = New StreamReader(SYOL & "komut.txt", False)
                    L = sr.ReadLine()
                    L = Trim(L)
                    sr.Close()
                    SetText("Gelen Veri : " & L)

                    If InStr(L, " ") > 0 Then
                        KOMUT = Trim(Mid(L, 1, InStr(L, " ")))
                        SNCID = Mid(KOMUT, 1, 10)
                        KOMUT = Mid(KOMUT, 11, Len(KOMUT))
                        KOMUT = UCase(KOMUT)
                    End If

                    L = L.Replace(SNCID, "")

                    If KOMUT = "SELECT" Then

                        SQ = L
                        SQ = SQ.Replace("tbl_001_01_", KUL.tper)
                        SQ = SQ.Replace("tbl_001_", KUL.tfrm)

                        DT = SQL_TABLO(SQ, "TABLO", False, False, False)
                        Dim sw As StreamWriter = New StreamWriter(SYOL & "tmp.txt", False)
                        sw.WriteLine(SNCID)

                        Data = ""
                        For Y = 0 To DT.Columns.Count - 1
                            Data = Data & DT.Columns(Y).ColumnName & Chr(9)
                        Next

                        sw.WriteLine(Data)
                        SetText("Gönderilen : " & Data)

                        For X = 0 To DT.Rows.Count - 1
                            Data = ""
                            For Y = 0 To DT.Columns.Count - 1
                                Data = Data & NULA(DT, X, DT.Columns(Y).ColumnName) & Chr(9)
                            Next
                            'Data = TURK(Data, 0)
                            'data = data

                            sw.WriteLine(Data)
                            SetText(Data)

                        Next
                        sw.Close()
                        Try
                            File.Delete(SYOL & "sonuc.txt")
                        Catch ex As Exception
                            'MsgBox(ex.Message)
                        End Try

                        Rename(SYOL & "tmp.txt", SYOL & "sonuc.txt")
                    End If






                    If KOMUT = "INSERT" Or KOMUT = "UPDATE" Or KOMUT = "DELETE" Then
                        SQ = L

                        SQ = SQ.Replace("tbl_001_01_", KUL.tper)
                        SQ = SQ.Replace("tbl_001_", KUL.tfrm)


                        If SQL_KOS(SQ, False) = True Then
                            Dim swi As StreamWriter = New StreamWriter(SYOL & "tmp.txt", False)
                            swi.WriteLine(SNCID & "True")
                            swi.Close()
                        Else
                            Dim swi As StreamWriter = New StreamWriter(SYOL & "tmp.txt", False)
                            swi.WriteLine(SNCID & "False")
                            swi.Close()
                        End If


                        Try
                            File.Delete(SYOL & "sonuc.txt")
                        Catch ex As Exception
                            'MsgBox(ex.Message)
                        End Try

                        Rename(SYOL & "tmp.txt", SYOL & "sonuc.txt")

                    End If



                    If KOMUT = "ANKAPAT" Then
                        '--------------------------------------------------------------------------------
                        'Adisyonlu Nakita Kasadan Kapat
                        '--------------------------------------------------------------------------------
                        'data = "HATA"
                        '"ANKAPAT PER=" & PERSON_ID & " MASA=" & Val(Label5.Tag) & " PDA=" & "PDA1"

                        SQ = L
                        Try
                            Call Masa_Kapat("ANKAPAT", SQ)
                        Catch ex As Exception
                        End Try


                        Dim swi As StreamWriter = New StreamWriter(SYOL & "tmp.txt", False)
                        swi.WriteLine(SNCID & "True")
                        swi.Close()
                        Try
                            File.Delete(SYOL & "sonuc.txt")
                        Catch ex As Exception
                            'MsgBox(ex.Message)
                        End Try
                        Rename(SYOL & "tmp.txt", SYOL & "sonuc.txt")


                    End If

                    If KOMUT = "AKKAPAT" Then
                        '--------------------------------------------------------------------------------
                        'Adisyonlu Kredi Bankadan Kapat
                        '--------------------------------------------------------------------------------

                        'data = "HATA"
                        '"AKKAPAT PER=" & PERSON_ID & " MASA=" & Val(Label5.Tag) & " PDA=" & "PDA1"

                        SQ = L
                        Try
                            Call Masa_Kapat("AKKAPAT", SQ)
                        Catch ex As Exception
                        End Try


                        Dim swi As StreamWriter = New StreamWriter(SYOL & "tmp.txt", False)
                        swi.WriteLine(SNCID & "True")
                        swi.Close()
                        Try
                            File.Delete(SYOL & "sonuc.txt")
                        Catch ex As Exception
                            'MsgBox(ex.Message)
                        End Try
                        Rename(SYOL & "tmp.txt", SYOL & "sonuc.txt")

                    End If

                    If KOMUT = "YKAPAT" Then
                        SQ = L
                        Try
                            Call Masa_Kapat("YKAPAT", SQ)
                        Catch ex As Exception
                        End Try


                        Dim swi As StreamWriter = New StreamWriter(SYOL & "tmp.txt", False)
                        swi.WriteLine(SNCID & "True")
                        swi.Close()
                        Try
                            File.Delete(SYOL & "sonuc.txt")
                        Catch ex As Exception
                            'MsgBox(ex.Message)
                        End Try
                        Rename(SYOL & "tmp.txt", SYOL & "sonuc.txt")


                    End If
                    If KOMUT = "NKAPAT" Then
                        '--------------------------------------------------------------------------------
                        'Nakita Kasadan Kapat
                        '--------------------------------------------------------------------------------
                        'data = "HATA"
                        '"NKAPAT PER=" & PERSON_ID & " MASA=" & Val(Label5.Tag)

                        SQ = L
                        Try
                            Call Masa_Kapat("NKAPAT", SQ)
                        Catch ex As Exception
                        End Try

                        Dim swi As StreamWriter = New StreamWriter(SYOL & "tmp.txt", False)
                        swi.WriteLine(SNCID & "True")
                        swi.Close()
                        Try
                            File.Delete(SYOL & "sonuc.txt")
                        Catch ex As Exception
                            'MsgBox(ex.Message)
                        End Try
                        Rename(SYOL & "tmp.txt", SYOL & "sonuc.txt")

                    End If


                    If KOMUT = "KKAPAT" Then
                        '--------------------------------------------------------------------------------
                        'Kredi Bankadan Kapat
                        '--------------------------------------------------------------------------------

                        'data = "HATA"
                        '"KKAPAT PER=" & PERSON_ID & " MASA=" & Val(Label5.Tag)

                        SQ = L
                        Try
                            Call Masa_Kapat("KKAPAT", SQ)
                        Catch ex As Exception
                        End Try


                        Dim swi As StreamWriter = New StreamWriter(SYOL & "tmp.txt", False)
                        swi.WriteLine(SNCID & "True")
                        swi.Close()
                        Try
                            File.Delete(SYOL & "sonuc.txt")
                        Catch ex As Exception
                            'MsgBox(ex.Message)
                        End Try
                        Rename(SYOL & "tmp.txt", SYOL & "sonuc.txt")

                    End If

                    If KOMUT = "ADISYON" Then
                        '"ADISYON MASA=" & Val(Label5.Tag)
                        '"ADISYON MASA=" & Val(Label5.Tag) & " PDA=" & PDA_AD & " GARSON=" & PERSON_ID

                        SQ = L
                        Try
                            Call Masa_Adisyon(SQ)
                        Catch ex As Exception
                        End Try


                        Dim swi As StreamWriter = New StreamWriter(SYOL & "tmp.txt", False)
                        swi.WriteLine(SNCID & "True")
                        swi.Close()
                        Try
                            File.Delete(SYOL & "sonuc.txt")
                        Catch ex As Exception
                            'MsgBox(ex.Message)
                        End Try
                        Rename(SYOL & "tmp.txt", SYOL & "sonuc.txt")

                    End If


                    If KOMUT = "MTASI" Then
                        '"MTASI M1=" & 1 & " M2=" & 2
                        SQ = L
                        Try
                            Call Masa_Tasi(SQ)
                        Catch ex As Exception
                        End Try

                        Dim swi As StreamWriter = New StreamWriter(SYOL & "tmp.txt", False)
                        swi.WriteLine(SNCID & "True")
                        swi.Close()
                        Try
                            File.Delete(SYOL & "sonuc.txt")
                        Catch ex As Exception
                            'MsgBox(ex.Message)
                        End Try
                        Rename(SYOL & "tmp.txt", SYOL & "sonuc.txt")

                    End If
                    If KOMUT = "ADSEKLE" Then
                        '"MTASI M1=" & 1 & " M2=" & 2
                        SQ = L
                        Try
                            Call ADS_EKLE(SQ)
                        Catch ex As Exception
                        End Try

                        Dim swi As StreamWriter = New StreamWriter(SYOL & "tmp.txt", False)
                        swi.WriteLine(SNCID & "True")
                        swi.Close()
                        Try
                            File.Delete(SYOL & "sonuc.txt")
                        Catch ex As Exception
                            'MsgBox(ex.Message)
                        End Try
                        Rename(SYOL & "tmp.txt", SYOL & "sonuc.txt")

                    End If

                    If KOMUT = "ADSGUNCELLE" Then
                        '"MTASI M1=" & 1 & " M2=" & 2
                        SQ = L
                        Try
                            Call ADS_GUNCELLE(SQ)
                        Catch ex As Exception
                        End Try

                        Dim swi As StreamWriter = New StreamWriter(SYOL & "tmp.txt", False)
                        swi.WriteLine(SNCID & "True")
                        swi.Close()
                        Try
                            File.Delete(SYOL & "sonuc.txt")
                        Catch ex As Exception
                            'MsgBox(ex.Message)
                        End Try
                        Rename(SYOL & "tmp.txt", SYOL & "sonuc.txt")

                    End If


                    Try
                        File.Delete(SYOL & "komut.txt")
                    Catch ex As Exception
                        'MsgBox(ex.Message)
                    End Try

                End If

            Catch ex As Exception
                SetText("Hata: " & ex.ToString)

                Dim swi As StreamWriter = New StreamWriter(SYOL & "tmp.txt", False)
                swi.WriteLine(SNCID & "False")
                swi.Close()
                Try
                    File.Delete(SYOL & "sonuc.txt")
                Catch ex2 As Exception
                    'MsgBox(ex.Message)
                End Try
                Rename(SYOL & "tmp.txt", SYOL & "sonuc.txt")


                Try
                    File.Delete(SYOL & "komut.txt")
                Catch ex3 As Exception
                    'MsgBox(ex.Message)
                End Try

            End Try


            Threading.Thread.Sleep(50)

        End While



    End Sub

    Public Sub ADS_EKLE(ByVal Veri As String)

        Dim MasaIDD As Long
        Dim SubeIDD As Long
        Dim AdsNotu As String
        Dim ID As Long
        Dim AdsFisnosu As String
        Dim AdsKisiSay As Long
        Dim AdsUser As Long

        Dim P1 As Integer = 0
        Dim P2 As Integer = 0
        Dim P3 As Integer = 0
        Dim P4 As Integer = 0
        Dim P5 As Integer = 0

        P1 = InStr(Veri, "M1=")
        P2 = InStr(Veri, "N1=")
        P3 = InStr(Veri, "F1=")
        P4 = InStr(Veri, "K1=")
        P5 = InStr(Veri, "U1=")




        MasaIDD = Val(Trim(Mid(Veri, P1 + 3, P2 - (P1 + 3))))
        AdsNotu = Trim(Mid(Veri, P2 + 3, P3 - (P2 + 3)))
        AdsFisnosu = Trim(Mid(Veri, P3 + 3, P4 - (P3 + 3)))
        AdsKisiSay = Val(Trim(Mid(Veri, P4 + 3, P4 - (P3 + 3))))
        AdsUser = Val(Trim(Mid(Veri, P5 + 3, Len(Veri))))



        SubeIDD = Val(VT_BILGI(KUL.tfrm & "men_masa", "a_id", "a_sube", MasaIDD))


        Dim AF As New Adisyon_Fisleri

        AF.Kayit_Tarihi = TTAR.TARIH
        AF.Kayit_Saati = TTAR.SAAT
        AF.Kayit_Masa_Id = MasaIDD
        AF.Kayit_Sube_Id = SubeIDD
        AF.Kayit_Notu = AdsNotu
        AF.Kayit_Cari_Id = 1
        'AF.Kayit_Gun_Sira = 0
        AF.Gunluk_Sayac_Hesapla()
        AF.Kayit_Fis_Nosu = AdsFisnosu
        AF.Kayit_Kisi_Sayisi = AdsKisiSay
        AF.Kayit_Durum_Bilgisi = 0
        AF.Kayit_Kullanici_Bilgisi = -AdsUser
        AF.Personel_Id = AdsUser
        ID = AF.Ekle()
    End Sub

    Public Sub ADS_GUNCELLE(ByVal Veri As String)
        Dim AdsIDD As Long
        Dim AdsNotu As String
        Dim AdsFisnosu As String
        Dim AdsKisiSay As Long


        Dim P1 As Integer = 0
        Dim P2 As Integer = 0
        Dim P3 As Integer = 0
        Dim P4 As Integer = 0
        Dim P5 As Integer = 0

        P1 = InStr(Veri, "A1=")
        P2 = InStr(Veri, "N1=")
        P3 = InStr(Veri, "F1=")
        P4 = InStr(Veri, "K1=")

        AdsIDD = Val(Trim(Mid(Veri, P1 + 3, P2 - (P1 + 3))))
        AdsNotu = Trim(Mid(Veri, P2 + 3, P3 - (P2 + 3)))
        AdsFisnosu = Trim(Mid(Veri, P3 + 3, P4 - (P3 + 3)))
        AdsKisiSay = Val(Trim(Mid(Veri, P4 + 3, P4 - (P3 + 3))))

        Dim AF As New Adisyon_Fisleri

        'AF.Kayit_ID_Nosu = 0
        AF.Kayit_Notu = AdsNotu
        AF.Kayit_Cari_Id = 1
        AF.Kayit_Durum_Bilgisi = 0
        AF.Kayit_Fis_Nosu = AdsFisnosu
        AF.Kayit_Kisi_Sayisi = AdsKisiSay
        Call AF.Duzelt(AdsIDD)

    End Sub

    Public Sub xxAna_Govde()

        Dim PRTNO As Int32

        PRTNO = NULN(DGV, TPoint, "a_port")

        Dim DT As New DataTable
        Dim SQ As String = ""
        Dim X As Integer = 0
        Dim Y As Integer = 0
        Dim V As String = ""
        Dim port As Int32 = PRTNO
        Dim localAddr As IPAddress = IPAddress.Parse(IP)
        Dim server As TcpListener
        Dim KOMUT As String = ""
        Dim SON As Boolean = False

        Dim R(Buf512) As String
        Dim oku(Buf512) As Byte
        Dim keysonuc As String = ""
        Dim L As Integer = 0

        server = Nothing

        Try

tekrar:

            server = New TcpListener(localAddr, port)

            server.Start()

            Dim bytes(BufLen) As Byte
            Dim data As String = Nothing


            While True


                SetText("Bağlantı İçin Bekleniyor")
                '-----------------------------------------------------------------------
                'Bekleme Rütini
                '-----------------------------------------------------------------------
                Dim client As TcpClient = server.AcceptTcpClient()
                '-----------------------------------------------------------------------
                SetText("Bağlandı")
                data = Nothing
                Dim stream As NetworkStream = client.GetStream()

BAS:

                Dim i As Int32

                data = ""

                Try
                    i = stream.Read(bytes, 0, bytes.Length)
                Catch ex As Exception
                    SetText("Hata - 3 ")
                    GoTo tekrar
                End Try



                While (i <> 0)

                    data = ""
                    data = System.Text.Encoding.ASCII.GetString(bytes, 0, i)

                    SetText("Gelen Veri : " & data)

                    If data = "STOP" Then
                        client.Close()
                        server.Stop()
                        Exit Sub
                    End If


                    '--------------------------------------------
                    'Veri İşleniyor
                    '--------------------------------------------

                    SQ = Trim(data)


                    Try
                        KOMUT = Mid(SQ, 1, InStr(SQ, " ") - 1)
                        KOMUT = Trim(UCase(KOMUT))
                    Catch ex As Exception
                        KOMUT = Trim(UCase(Trim(SQ)))
                    End Try


                    Select Case KOMUT

                        Case "#END"
                            SetText("İş Parçacığı Durduruldu")
                            stream.Close()
                            client.Close()
                            server.Stop()
                            Exit Sub


                        Case "SELECT"
                            SQ = SQ.Replace("tbl_001_01_", KUL.tper)
                            SQ = SQ.Replace("tbl_001_", KUL.tfrm)
                            DT = SQL_TABLO(B_SQL, SQ, "T", False, False, False)

                            data = ""
                            For Y = 0 To DT.Columns.Count - 1
                                data = data & DT.Columns(Y).ColumnName & Chr(9)
                            Next

                            If data = "" Then
                                Dim Zmsg1 As Byte() = System.Text.Encoding.ASCII.GetBytes("STOP")
                                stream.Write(Zmsg1, 0, Zmsg1.Length)
                                client.Close()
                                server.Stop()
                                GoTo tekrar
                            End If


                            Dim msg1 As Byte() = System.Text.Encoding.ASCII.GetBytes(data)
                            ' Veriyi Yolla
                            Try
                                stream.Write(msg1, 0, msg1.Length)
                                SetText("Gönderilen : " & data)
                                i = stream.Read(bytes, 0, bytes.Length)
                            Catch ex As Exception
                                client.Close()
                                server.Stop()
                                GoTo tekrar
                            End Try



                            For X = 0 To DT.Rows.Count - 1
                                data = ""
                                For Y = 0 To DT.Columns.Count - 1
                                    data = data & NULA(DT, X, DT.Columns(Y).ColumnName) & Chr(9)
                                Next

                                data = TURK(data, 0)
                                'data = data


                                Dim msg As Byte() = System.Text.Encoding.ASCII.GetBytes(data)
                                ' Veriyi Yolla
                                Try
                                    stream.Write(msg, 0, msg.Length)
                                    SetText("Gönderilen : " & data)
                                    i = stream.Read(bytes, 0, bytes.Length)
                                Catch ex As Exception
                                    client.Close()
                                    server.Stop()
                                    GoTo tekrar
                                End Try
                            Next

                            Dim Zmsg As Byte() = System.Text.Encoding.ASCII.GetBytes("STOP")
                            stream.Write(Zmsg, 0, Zmsg.Length)
                            client.Close()
                            server.Stop()
                            GoTo tekrar



                        Case "###"

                            'Console.Write(SQ & vbCrLf)
                            'TextBox3.Text = SQ & vbCrLf

                            'Call SetText(SQ)

                            ' Process the data sent by the client.
                            Dim msg As Byte() = System.Text.Encoding.ASCII.GetBytes("OK")
                            stream.Write(msg, 0, msg.Length)
                            i = stream.Read(bytes, 0, bytes.Length)
                            If i <> 0 Then
                            End If

                        Case "ANKAPAT"
                            '--------------------------------------------------------------------------------
                            'Adisyonlu Nakita Kasadan Kapat
                            '--------------------------------------------------------------------------------
                            'data = "HATA"
                            '"ANKAPAT PER=" & PERSON_ID & " MASA=" & Val(Label5.Tag) & " PDA=" & "PDA1"

                            data = "OK"
                            Dim Zmsg As Byte() = System.Text.Encoding.ASCII.GetBytes(data)
                            stream.Write(Zmsg, 0, Zmsg.Length)
                            Call Masa_Kapat("ANKAPAT", SQ)
                            GoTo BAS
                        Case "AKKAPAT"
                            '--------------------------------------------------------------------------------
                            'Adisyonlu Kredi Bankadan Kapat
                            '--------------------------------------------------------------------------------

                            'data = "HATA"
                            '"AKKAPAT PER=" & PERSON_ID & " MASA=" & Val(Label5.Tag) & " PDA=" & "PDA1"
                            data = "OK"
                            Dim Zmsg As Byte() = System.Text.Encoding.ASCII.GetBytes(data)
                            stream.Write(Zmsg, 0, Zmsg.Length)
                            Call Masa_Kapat("AKKAPAT", SQ)
                            GoTo BAS

                        Case "NKAPAT"
                            '--------------------------------------------------------------------------------
                            'Nakita Kasadan Kapat
                            '--------------------------------------------------------------------------------
                            'data = "HATA"
                            '"NKAPAT PER=" & PERSON_ID & " MASA=" & Val(Label5.Tag)

                            data = "OK"
                            Dim Zmsg As Byte() = System.Text.Encoding.ASCII.GetBytes(data)
                            stream.Write(Zmsg, 0, Zmsg.Length)
                            Call Masa_Kapat("NKAPAT", SQ)
                            GoTo BAS
                        Case "KKAPAT"
                            '--------------------------------------------------------------------------------
                            'Kredi Bankadan Kapat
                            '--------------------------------------------------------------------------------

                            'data = "HATA"
                            '"KKAPAT PER=" & PERSON_ID & " MASA=" & Val(Label5.Tag)
                            data = "OK"
                            Dim Zmsg As Byte() = System.Text.Encoding.ASCII.GetBytes(data)
                            stream.Write(Zmsg, 0, Zmsg.Length)
                            Call Masa_Kapat("KKAPAT", SQ)
                            GoTo BAS

                        Case "ADISYON"
                            '"ADISYON MASA=" & Val(Label5.Tag)
                            '"ADISYON MASA=" & Val(Label5.Tag) & " PDA=" & PDA_AD & " GARSON=" & PERSON_ID
                            data = "OK"
                            Dim Zmsg As Byte() = System.Text.Encoding.ASCII.GetBytes(data)
                            stream.Write(Zmsg, 0, Zmsg.Length)
                            Call Masa_Adisyon(SQ)
                            GoTo BAS

                        Case "MTASI"
                            '"MTASI M1=" & 1 & " M2=" & 2
                            data = "OK"
                            Dim Zmsg As Byte() = System.Text.Encoding.ASCII.GetBytes(data)
                            stream.Write(Zmsg, 0, Zmsg.Length)
                            Call Masa_Tasi(SQ)
                            GoTo BAS
                            '

                        Case Else
                            'INSERT
                            'UPDATE
                            'DELETE
                            'ALTER
                            SON = SQL_KOS(SQ, False)
                            If SON = True Then
                                data = "OK"
                            Else
                                data = "HATA"
                            End If

                            Dim Zmsg As Byte() = System.Text.Encoding.ASCII.GetBytes(data)
                            stream.Write(Zmsg, 0, Zmsg.Length)

                            'i = stream.Read(bytes, 0, bytes.Length)
                            'If i <> 0 Then
                            'End If

                            'client.Close()
                            'server.Stop()
                            'GoTo tekrar

                            GoTo BAS

                    End Select



                End While
                ' Bağlantıyı Kes
                client.Close()
            End While

        Catch e As SocketException
            SetText("Soket Hatası : {0}" & e.ToString & " ")
            server.Stop()
            GoTo tekrar
        Finally
            server.Stop()
        End Try

    End Sub

    Private Sub Mutfak_Yazdir(ByVal MasaID As Integer, ByVal Ads_Id As Long)

        'Dim SQ As String
        'Dim DM As New DataTable
        'Dim DMU As New DataTable
        'Dim DTM As New DataTable
        'Dim SUBE As Integer = 0
        'Dim YAZ As String = ""
        'Dim B As String = ""
        'Dim N As String = ""

        'SQ = ""
        'SQ = "SELECT a_mutfakid AS mutfak "
        'SQ = SQ & " FROM " & KUL.tfrm & "men_adisyon"
        'SQ = SQ & " WHERE (a_mid = " & MasaID & ") AND (a_yildiz = '*')"
        'SQ = SQ & " GROUP BY a_mutfakid"
        'DM = SQL_TABLO(SQ, "T", False, False, False)

        'For Z = 0 To DM.Rows.Count - 1


        '    'Eski
        '    'SQ = "SELECT "
        '    'SQ = SQ & " men_pdasiparis.a_pda, "
        '    'SQ = SQ & " men_pdasiparis.a_mid, "
        '    'SQ = SQ & " men_urunmutfak.a_mid AS mutfak, "
        '    'SQ = SQ & " men_pdasiparis.a_sira, "
        '    'SQ = SQ & " men_pdasiparis.a_sid, "
        '    'SQ = SQ & " men_pdasiparis.a_mik, "
        '    'SQ = SQ & " men_pdasiparis.a_brf, "
        '    'SQ = SQ & " men_pdasiparis.a_tut, "
        '    'SQ = SQ & " men_pdasiparis.a_not, "
        '    'SQ = SQ & " " & KUL.tfrm & "stok.a_kod, "
        '    'SQ = SQ & " " & KUL.tfrm & "stok.a_adi, "
        '    'SQ = SQ & " " & KUL.tfrm & "stok.a_birim, "
        '    'SQ = SQ & " tbl_user.a_id AS Garson_Id, "
        '    'SQ = SQ & " tbl_user.a_adi AS Garson_Adi, "
        '    'SQ = SQ & " men_mutfak.a_adi AS Mutfak_Adi, "
        '    'SQ = SQ & " men_mutfak.a_yazyol "
        '    'SQ = SQ & " FROM men_pdasiparis INNER JOIN"
        '    'SQ = SQ & " men_urunmutfak ON men_pdasiparis.a_sid = men_urunmutfak.a_sid INNER JOIN"
        '    'SQ = SQ & " " & KUL.tfrm & "stok ON men_urunmutfak.a_sid = " & KUL.tfrm & "stok.a_id INNER JOIN"
        '    'SQ = SQ & " tbl_user ON men_pdasiparis.a_userid = tbl_user.a_id INNER JOIN"
        '    'SQ = SQ & " men_mutfak ON men_urunmutfak.a_mid = men_mutfak.a_id"
        '    'SQ = SQ & " WHERE "
        '    'SQ = SQ & " (men_pdasiparis.a_pda    = '" & NULA(DR, X, "a_pda") & "') AND "
        '    'SQ = SQ & " (men_pdasiparis.a_mid    = " & NULN(DR, X, "a_mid") & ") AND "
        '    'SQ = SQ & " (men_pdasiparis.a_yildiz = '" & "*" & "') AND "
        '    'SQ = SQ & " (men_urunmutfak.a_mid    = " & NULN(DM, Z, "mutfak") & ")"
        '    'SQ = SQ & " ORDER BY men_pdasiparis.a_sira"

        '    'Yeni
        '    SQ = ""
        '    SQ = "SELECT "
        '    SQ = SQ & " " & KUL.tfrm & "men_adisyon.a_mid, "
        '    SQ = SQ & " " & KUL.tfrm & "men_adisyon.a_yildiz, "
        '    SQ = SQ & " " & KUL.tfrm & "men_adisyon.a_sira, "
        '    SQ = SQ & " " & KUL.tfrm & "men_adisyon.a_mik, "
        '    SQ = SQ & " " & KUL.tfrm & "men_adisyon.a_brf, "
        '    SQ = SQ & " " & KUL.tfrm & "men_adisyon.a_tut, "
        '    SQ = SQ & " " & KUL.tfrm & "men_adisyon.a_not, "
        '    SQ = SQ & " " & KUL.tfrm & "men_adisyon.a_subeid, "
        '    SQ = SQ & " " & KUL.tfrm & "men_adisyon.a_userid As Garson_Id, "
        '    SQ = SQ & " " & KUL.tfrm & "stok.a_kod, "
        '    SQ = SQ & " " & KUL.tfrm & "stok.a_adi, "
        '    SQ = SQ & " " & KUL.tfrm & "stok.a_birim, "
        '    SQ = SQ & " " & KUL.tfrm & "men_mutfak.a_kod AS Mutfak_Kod, "
        '    SQ = SQ & " " & KUL.tfrm & "men_mutfak.a_adi AS Mutfak_Adi, "
        '    SQ = SQ & " " & KUL.tfrm & "men_mutfak.a_yazyol "
        '    SQ = SQ & " FROM  " & KUL.tfrm & "men_adisyon INNER JOIN"
        '    SQ = SQ & " " & KUL.tfrm & "stok ON " & KUL.tfrm & "men_adisyon.a_sid = " & KUL.tfrm & "stok.a_id INNER JOIN"
        '    SQ = SQ & " " & KUL.tfrm & "men_mutfak ON " & KUL.tfrm & "men_adisyon.a_mutfakid = " & KUL.tfrm & "men_mutfak.a_id"
        '    SQ = SQ & " WHERE "
        '    SQ = SQ & " (" & KUL.tfrm & "men_adisyon.a_mid = " & MasaID & ") AND "
        '    SQ = SQ & " (" & KUL.tfrm & "men_adisyon.a_yildiz = '*') AND "
        '    SQ = SQ & " (" & KUL.tfrm & "men_adisyon.a_mutfakid = " & NULN(DM, Z, "mutfak") & ")"
        '    'SQ = SQ & " ORDER BY " & KUL.tfrm & "men_adisyon.a_sira DESC"
        '    SQ = SQ & " ORDER BY " & KUL.tfrm & "men_adisyon.a_not DESC"
        '    DMU = SQL_TABLO(SQ, "T", False, False, False)
        '    YAZ = NULA(DMU, 0, "a_yazyol")


        '    SQ = ""
        '    SQ = "SELECT a_id, a_kod, a_adi, a_durum, a_adsnosu, a_sube, a_kisisay"
        '    SQ = SQ & " FROM " & KUL.tfrm & "men_masa"
        '    SQ = SQ & " WHERE(a_id = " & MasaID & ")"
        '    DTM = SQL_TABLO(SQ, "VERI", False, False, False)
        '    SUBE = NULN(DTM, 0, "a_sube")

        '    '-----------------------------------------------------------------------------
        '    'ismail burada
        '    '-----------------------------------------------------------------------------
        '    B = ""
        '    B = B & Yasla("", 40, "SOL") & vbCrLf
        '    B = B & Yasla("", 40, "SOL") & vbCrLf
        '    B = B & Yasla("", 40, "SOL") & vbCrLf
        '    B = B & Yasla("", 40, "SOL") & vbCrLf
        '    B = B & Yasla("", 40, "SOL") & vbCrLf
        '    B = B & "----------------------------------------- " & vbCrLf
        '    B = B & "Sipariş Masa No: " & NULA(DTM, 0, "a_adi") & vbCrLf
        '    B = B & "Garson         : " & VT_BILGI("tbl_user", "a_id", "a_adi", NULN(DMU, 0, "Garson_Id")) & vbCrLf
        '    B = B & "Tarih          : " & Now.ToShortDateString & vbCrLf
        '    B = B & "Saat           : " & Now.ToLongTimeString & vbCrLf
        '    B = B & "Kişi Sayısı    : " & NULA(DTM, 0, "a_kisisay") & vbCrLf
        '    B = B & "Sipariş Mutfak : " & NULA(DMU, 0, "Mutfak_Adi") & vbCrLf
        '    'B = B & "PDA            : " & NULA(DR, X, "a_pda") & vbCrLf
        '    B = B & "------------------------------ -------- " & vbCrLf
        '    B = B & "Ürün                               Adet " & vbCrLf
        '    B = B & "------------------------------ -------- " & vbCrLf
        '    For A = 0 To DMU.Rows.Count - 1

        '        'B = B & Yasla(NULA(DMU, A, "a_kod"), 5, "SOL") & " "
        '        'Aperatiflere XXX İle Başla  CHR(251) = ¹

        '        If Mid(Trim(NULA(DMU, A, "a_not")), 1, 1) = Chr(251) Then
        '            B = B & "XXX " & Yasla(NULA(DMU, A, "a_adi"), 25, "SOL") & " "
        '        Else
        '            B = B & Yasla(NULA(DMU, A, "a_adi"), 29, "SOL") & " "
        '        End If

        '        B = B & Yasla(NULA(DMU, A, "a_mik"), 8, "SAG") & vbCrLf

        '        If Trim(NULA(DMU, A, "a_not")) <> "" And Trim(NULA(DMU, A, "a_not")) <> Chr(251) Then
        '            N = NULA(DMU, A, "a_not")
        '            N = N.Replace(Chr(251), "")
        '            B = B & "(" & Yasla(N, 38, "SOL") & ")" & vbCrLf
        '        End If
        '    Next
        '    B = B & "------------------------------ -------- " & vbCrLf
        '    For W = 1 To 10
        '        B = B & "" & vbCrLf
        '    Next

        '    'Kesme Komutu
        '    If PRNcm0 <> 0 Then
        '        B = B & Chr(PRNcm0) & PRNcm1 & vbCrLf
        '    End If

        '    B = TURK(B, 0)


        '    Dim K As String = ""
        '    K = KUL.KOD & "-" & Now.Year & "-" & Now.Month & "-" & Now.Day & " " & Now.Hour & ":" & Now.Minute & ":" & Now.Second & ":" & sifre_uret(8)

        '    SQ = ""
        '    SQ = "INSERT INTO " & KUL.tfrm & "men_pserver (a_id, a_tur, a_pda, a_user, a_tarih, a_saat, a_komut, a_veri, a_durum, a_sube, a_masa)"
        '    SQ = SQ & " VALUES "
        '    SQ = SQ & " ( "
        '    SQ = SQ & " '" & K & "', "
        '    SQ = SQ & " '" & "SİPARİŞ" & "', "
        '    SQ = SQ & " '" & MasaID & "', "
        '    SQ = SQ & "  " & NULN(DMU, 0, "Garson_Id") & " , "
        '    SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & "', "
        '    SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & " " & TTAR.SAAT & "', "
        '    SQ = SQ & " '" & YAZ & "', "
        '    SQ = SQ & " '" & B & "', "
        '    SQ = SQ & " '" & "H" & "', "
        '    SQ = SQ & "  " & SUBE & ", "
        '    SQ = SQ & "  " & NULN(DMU, 0, "a_mid") & " "
        '    SQ = SQ & " ) "
        '    Call SQL_KOS(SQ, False)

        'Next





        '' YENİLENDİ
        Dim SQ As String = ""

        Dim DM As New DataTable
        Dim DMU As New DataTable
        Dim DT As New DataTable
        Dim DTC As New DataTable

        Dim A As Integer = 0
        Dim Z As Integer = 0
        Dim W As Integer = 0

        Dim B As String = ""
        Dim S As String = ""

        Dim YAZ As String = ""
        Dim DSY As String = ""

        Dim KISI As Long = 0



        SQ = "SELECT a_id, a_kod, a_adi, a_kisisay"
        SQ = SQ & " FROM " & KUL.tfrm & "men_masa"
        SQ = SQ & " WHERE(a_id = " & MasaID & ")"
        DT = SQL_TABLO(SQ, "T", False, False, False)



        KISI = Val(VT_BILGI(KUL.tfrm & "men_adisyonmas", "a_id", "a_kisi_sayisi", Ads_Id))





        SQ = ""
        SQ = "SELECT a_mutfakid AS Mutfak "
        SQ = SQ & " FROM " & KUL.tfrm & "men_adisyon"
        SQ = SQ & " WHERE (a_mid = " & MasaID & ") AND (a_yildiz = '*')"
        SQ = SQ & " GROUP BY a_mutfakid"
        DM = SQL_TABLO(SQ, "T", False, False, False)


        For Z = 0 To DM.Rows.Count - 1

            SQ = ""
            SQ = "SELECT "
            SQ = SQ & " " & KUL.tfrm & "men_adisyon.a_mid, "
            SQ = SQ & " " & KUL.tfrm & "men_adisyon.a_yildiz, "

            SQ = SQ & " " & KUL.tfrm & "men_adisyon.a_adet, "
            SQ = SQ & " " & KUL.tfrm & "men_adisyon.a_porsiyon, "

            SQ = SQ & " " & KUL.tfrm & "men_adisyon.a_mik, "
            SQ = SQ & " " & KUL.tfrm & "men_adisyon.a_brf, "
            SQ = SQ & " " & KUL.tfrm & "men_adisyon.a_tut, "
            SQ = SQ & " " & KUL.tfrm & "men_adisyon.a_not, "
            SQ = SQ & " " & KUL.tfrm & "men_adisyon.a_subeid, "
            SQ = SQ & " " & KUL.tfrm & "stok.a_kod, "
            SQ = SQ & " " & KUL.tfrm & "stok.a_adi, "
            SQ = SQ & " " & KUL.tfrm & "stok.a_birim, "
            SQ = SQ & " " & KUL.tfrm & "men_adisyon.a_personel As Garson_Id, "
            SQ = SQ & " " & KUL.tfrm & "men_mutfak.a_kod AS Mutfak_Kod, "
            SQ = SQ & " " & KUL.tfrm & "men_mutfak.a_adi AS Mutfak_Adi, "
            SQ = SQ & " " & KUL.tfrm & "men_mutfak.a_yazyol "
            SQ = SQ & " FROM  " & KUL.tfrm & "men_adisyon INNER JOIN"
            SQ = SQ & " " & KUL.tfrm & "stok ON " & KUL.tfrm & "men_adisyon.a_sid = " & KUL.tfrm & "stok.a_id INNER JOIN"
            SQ = SQ & " " & KUL.tfrm & "men_mutfak ON " & KUL.tfrm & "men_adisyon.a_mutfakid = " & KUL.tfrm & "men_mutfak.a_id"
            SQ = SQ & " WHERE "
            SQ = SQ & " (" & KUL.tfrm & "men_adisyon.a_mid = " & MasaID & ") AND "
            SQ = SQ & " (" & KUL.tfrm & "men_adisyon.a_master_id = " & Ads_Id & ") AND "
            SQ = SQ & " (" & KUL.tfrm & "men_adisyon.a_yildiz = '*') AND "
            SQ = SQ & " (" & KUL.tfrm & "men_adisyon.a_mutfakid = " & NULN(DM, Z, "Mutfak") & ")"
            SQ = SQ & " ORDER BY " & KUL.tfrm & "men_adisyon.a_sira DESC"

            'SQ = SQ & " (" & KUL.tfrm & "men_adisyon.a_mutfakid = " & NULN(DM, Z, "Mutfak") & ")"
            'SQ = SQ & " GROUP BY " & KUL.tfrm & "men_adisyon.a_mid, " & KUL.tfrm & "men_adisyon.a_yildiz, " & KUL.tfrm & "men_adisyon.a_brf, "
            'SQ = SQ & " " & KUL.tfrm & "men_adisyon.a_not, " & KUL.tfrm & "men_adisyon.a_subeid, "
            'SQ = SQ & " " & KUL.tfrm & "stok.a_kod, " & KUL.tfrm & "stok.a_adi, " & KUL.tfrm & "stok.a_birim, " & KUL.tfrm & "men_mutfak.a_kod,"
            'SQ = SQ & " " & KUL.tfrm & "men_mutfak.a_adi, " & KUL.tfrm & "men_mutfak.a_yazyol"
            'SQ = SQ & " HAVING   (" & KUL.tfrm & "men_adisyon.a_mid = " & Masa_Id & ") AND "
            'SQ = SQ & " (" & KUL.tfrm & "men_adisyon.a_yildiz = '*')"

            DMU = SQL_TABLO(SQ, "T", False, False, False)
            YAZ = NULA(DMU, 0, "a_yazyol")

            'DSY = sifre_uret(8) & ".TEX"
            'Try
            '    If VAR_MI(DSY) = True Then
            '        Try
            '            Kill(DSY)
            '        Catch ex2 As Exception
            '        End Try
            '    End If
            'Catch ex As Exception
            'End Try

            SQ = ""
            SQ = "SELECT a_id, a_ftel, a_fistel1, a_fistel2, a_fgsm, a_fgsm2, a_kod, a_adi, a_fadres1, a_fadres2, a_fadres3 "
            SQ = SQ & " FROM " & KUL.tfrm & "cari"
            SQ = SQ & " WHERE  a_id = " & 0
            DTC = SQL_TABLO(SQ, "cari", False, False, False)

            B = ""
            'B = B & Yasla(NULA(DTC, 0, "a_ftel"), 40, "SOL") & vbCrLf
            'B = B & Yasla(NULA(DTC, 0, "a_adi"), 40, "SOL") & vbCrLf
            'B = B & Yasla(NULA(DTC, 0, "a_fadres1"), 40, "SOL") & vbCrLf
            'B = B & Yasla(NULA(DTC, 0, "a_ftel"), 40, "SOL") & vbCrLf
            'B = B & Yasla(NULA(DTC, 0, "a_adi"), 40, "SOL") & vbCrLf
            'B = B & Yasla(NULA(DTC, 0, "a_fadres1"), 40, "SOL") & vbCrLf
            'B = B & Yasla(NULA(DTC, 0, "a_fadres2"), 40, "SOL") & vbCrLf
            'B = B & Yasla(NULA(DTC, 0, "a_fadres3"), 40, "SOL") & vbCrLf
            B = B & "--------------------------------------- " & vbCrLf
            B = B & "Kişi Sayısı    : " & KISI & vbCrLf
            B = B & "Sipariş Mutfak : " & NULA(DMU, 0, "Mutfak_Adi") & vbCrLf
            B = B & "Sipariş Masa No: " & NULA(DT, 0, "a_adi") & vbCrLf
            B = B & "Tarih          : " & Now.ToShortDateString & vbCrLf
            B = B & "Saat           : " & Now.ToLongTimeString & vbCrLf
            B = B & "Personel       : " & VT_BILGI("tbl_001_personel", "a_id", "a_adi", NULN(DMU, 0, "Garson_Id")) & vbCrLf
            ' B = B & "PDA            : " & NULA(DR, 0, "a_pda") & vbCrLf
            B = B & "------------------------------ -------- " & vbCrLf
            B = B & "Ürün                               Adet " & vbCrLf
            B = B & "------------------------------ -------- " & vbCrLf
            For A = 0 To DMU.Rows.Count - 1

                'B = B & Yasla(NULA(DMU, A, "a_adi"), 30, "SOL") & " "
                'B = B & Yasla(NULA(DMU, A, "a_mik"), 8, "SAG") & vbCrLf

                B = B & Yasla(NULA(DMU, A, "a_adi"), 30, "SOL") & " "
                B = B & Yasla(NULA(DMU, A, "a_adet"), 3, "SOL")
                B = B & "X"
                B = B & Yasla(NULA(DMU, A, "a_porsiyon"), 4, "SAG") & vbCrLf

                If Trim(NULA(DMU, A, "a_not")) <> "" Then
                    S = ""
                    S = "( " & Yasla(NULA(DMU, A, "a_not"), 35, "SOL") & " )"
                    B = B & S & vbCrLf
                End If

            Next
            B = B & "------------------------------ -------- " & vbCrLf
            For W = 1 To 10
                B = B & "" & vbCrLf
            Next

            'Kesme Komutu
            If PRNcm0 <> 0 Then
                B = B & Chr(PRNcm0) & PRNcm1 & vbCrLf
            End If


            B = TURK(B, 0)

            'Try
            '    File.WriteAllText(DSY, B)
            '    Call Shell(YAZ & " " & DSY, AppWinStyle.Hide)
            'Catch ex As Exception
            'End Try


            Dim K As String = ""
            K = KUL.KOD & "-" & Now.Year & "-" & Now.Month & "-" & Now.Day & " " & Now.Hour & ":" & Now.Minute & ":" & Now.Second & ":" & sifre_uret(8)

            SQ = "SELECT a_id, a_kod, a_adi, a_sube "
            SQ = SQ & " FROM " & KUL.tfrm & "men_masa"
            SQ = SQ & " WHERE(a_id = " & MasaID & ")"
            DT = SQL_TABLO(SQ, "VERI", False, False, False)

            SQ = ""
            SQ = "INSERT INTO " & KUL.tfrm & "men_pserver (a_id, a_tur, a_pda, a_user, a_tarih, a_saat, a_komut, a_veri, a_durum, a_sube, a_masa)"
            SQ = SQ & " VALUES "
            SQ = SQ & " ( "
            SQ = SQ & " '" & K & "', "
            SQ = SQ & " '" & "SİPARİŞ" & "', "
            SQ = SQ & " '" & NULA(DMU, 0, "a_pda") & "', "
            SQ = SQ & "  " & NULN(DMU, 0, "Garson_Id") & " , "
            SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & "', "
            SQ = SQ & " '" & TTAR.TR2UK(TTAR.TARIH) & " " & TTAR.SAAT & "', "
            SQ = SQ & " '" & YAZ & "', "
            SQ = SQ & " '" & B & "', "
            SQ = SQ & " '" & "H" & "', "

            If NULN(DT, 0, "a_sube") <> 0 Then
                SQ = SQ & "  " & NULN(DT, 0, "a_sube") & " , "
            Else
                SQ = SQ & "  " & NULN(DMU, 0, "a_subeid") & " , "
            End If

            SQ = SQ & "  " & MasaID & " "
            SQ = SQ & " ) "
            Call SQL_KOS(SQ, False)

        Next
    End Sub

    Public Class Adisyon_Fisleri

        Private a_id As Long
        Private a_tarih As String
        Private a_saat As String
        Private a_mid As Long
        Private a_subeid As Long
        Private a_not As String
        Private a_cari_id As Long
        Private a_gunsira As Long
        Private a_fisno As String
        Private a_kisi_sayisi As Long
        Private a_durum As Long
        Private a_userid As Long
        Private a_personel As Long

        Private x_masakod As String = ""
        Private x_masaadi As String = ""
        Private x_subekod As String = ""
        Private x_subeadi As String = ""
        Private x_carikod As String = ""
        Private x_cariadi As String = ""
        Private x_userkod As String = ""
        Private x_useradi As String = ""

        '
#Region "Ozellikler"
        Public ReadOnly Property Kayit_ID_Nosu() As Long
            Get
                Return a_id
            End Get
            'Set(ByVal value As String)
            'a_gc = Mid(value, 1, 1)
            'End Set
        End Property
        Public Property Kayit_Tarihi() As String
            Get
                Return a_tarih
            End Get
            Set(ByVal value As String)
                a_tarih = value
            End Set
        End Property
        Public Property Kayit_Saati() As String
            Get
                Return a_saat
            End Get
            Set(ByVal value As String)
                a_saat = value
            End Set
        End Property
        Public Property Kayit_Masa_Id() As Long
            Get
                Return a_mid
            End Get
            Set(ByVal value As Long)
                a_mid = value
            End Set
        End Property
        Public Property Kayit_Sube_Id() As Long
            Get
                Return a_subeid
            End Get
            Set(ByVal value As Long)
                a_subeid = value
            End Set
        End Property
        Public Property Kayit_Notu() As String
            Get
                Return a_not
            End Get
            Set(ByVal value As String)
                a_not = value
            End Set
        End Property
        Public Property Kayit_Cari_Id() As Long
            Get
                Return a_cari_id
            End Get
            Set(ByVal value As Long)
                a_cari_id = value
            End Set
        End Property
        Public ReadOnly Property Kayit_Gun_Sira() As Long
            Get
                Return a_gunsira
            End Get
            'Set(ByVal value As String)
            'a_gc = Mid(value, 1, 1)
            'End Set
        End Property
        Public Property Kayit_Fis_Nosu() As String
            Get
                Return a_fisno
            End Get
            Set(ByVal value As String)
                a_fisno = value
            End Set
        End Property
        Public Property Kayit_Kisi_Sayisi() As Long
            Get
                Return a_kisi_sayisi
            End Get
            Set(ByVal value As Long)
                a_kisi_sayisi = value
            End Set
        End Property
        Public Property Kayit_Durum_Bilgisi() As Long
            Get
                Return a_durum
            End Get
            Set(ByVal value As Long)
                a_durum = value
            End Set
        End Property

        Public Property Kayit_Kullanici_Bilgisi() As Long
            Get
                Return a_userid
            End Get
            Set(ByVal value As Long)
                a_userid = value
            End Set
        End Property
        Public Property Personel_Id() As Long
            Get
                Return a_personel
            End Get
            Set(ByVal value As Long)
                a_personel = value
            End Set
        End Property


        Public ReadOnly Property XMasa_Kodu() As String
            Get
                Return x_masakod
            End Get
        End Property
        Public ReadOnly Property XMasa_Adi() As String
            Get
                Return x_masaadi
            End Get
        End Property
        Public ReadOnly Property XSube_Kodu() As String
            Get
                Return x_subekod
            End Get
        End Property
        Public ReadOnly Property XSube_Adi() As String
            Get
                Return x_subeadi
            End Get
        End Property

        Public ReadOnly Property XCari_Kodu() As String
            Get
                Return x_carikod
            End Get
        End Property
        Public ReadOnly Property XCari_Adi() As String
            Get
                Return x_cariadi
            End Get
        End Property

        Public ReadOnly Property XUser_Kodu() As String
            Get
                Return x_userkod
            End Get
        End Property
        Public ReadOnly Property XUser_Adi() As String
            Get
                Return x_useradi
            End Get
        End Property



#End Region


        Public Function Kontrol() As Boolean
            If a_tarih = "" Then a_tarih = TTAR.TARIH
            If a_saat = "" Then a_saat = TTAR.SAAT

            If a_userid = 0 Then a_userid = Val(KUL.KOD)

            If a_mid = 0 Then
                MKUTU("Masa Seçilmeden Adisyon Fişi Açılamaz.", "T", "Uyarı")
                Return False
            End If
            If a_subeid = 0 Then
                MKUTU("Şube Seçilmeden Adisyon Fişi Açılamaz.", "T", "Uyarı")
                Return False
            End If

            Return True

        End Function

        Public Sub Gunluk_Sayac_Hesapla()
            Dim SQ As String = ""
            Dim DT1 As New DataTable

            SQ = "SELECT MAX(a_gunsira) AS EB"
            SQ = SQ & " FROM " & KUL.tfrm & "men_adisyonmas"
            SQ = SQ & " WHERE(a_tarih = N'" & TTAR.TR2UK(TTAR.TARIH) & "')"
            DT1 = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            a_gunsira = NULN(DT1, 0, "EB") + 1

        End Sub

        Public Function Ekle() As Long
            If Kontrol() = False Then
                Return 0
            End If
            Dim SQ As String = ""
            Dim DT1 As New DataTable
            Dim ID As Long = 0
            SQ = ""
            SQ = "INSERT INTO " & KUL.tfrm & "men_adisyonmas "
            SQ = SQ & " ("
            SQ = SQ & " a_tarih, a_saat, a_mid, a_subeid, a_not, a_cari_id, a_gunsira, a_fisno, a_kisi_sayisi, a_durum, a_userid, a_personel "
            SQ = SQ & " )"
            SQ = SQ & " VALUES "
            SQ = SQ & " ("
            SQ = SQ & " N'" & TTAR.TR2UK(a_tarih) & "', "
            SQ = SQ & " N'" & a_saat & "', "
            SQ = SQ & "  " & a_mid & " , "
            SQ = SQ & "  " & a_subeid & " , "
            SQ = SQ & " N'" & a_not & "' , "
            SQ = SQ & "  " & a_cari_id & " , "
            SQ = SQ & "  " & a_gunsira & " , "
            SQ = SQ & "  N'" & a_fisno & "' , "
            SQ = SQ & "  " & a_kisi_sayisi & " , "
            SQ = SQ & "  " & a_durum & ",  "
            SQ = SQ & "  " & a_userid & ",  "
            SQ = SQ & "  " & a_personel & "  "
            SQ = SQ & " )"
            SQ = SQ & " SELECT SCOPE_IDENTITY() AS DEGER "
            DT1 = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            ID = NULN(DT1, 0, "DEGER")
            Return ID
        End Function



        Public Function Bul_Masa(ByVal Masa_Id As Long) As DataTable
            Dim SQ As String = ""
            Dim DT As New DataTable

            SQ = ""
            SQ = "SELECT a_id,a_personel, a_tarih, a_saat, a_mid, a_subeid, a_not, a_cari_id, a_gunsira, a_fisno, a_kisi_sayisi, a_durum, a_userid"
            SQ = SQ & " FROM " & KUL.tfrm & "men_adisyonmas "
            SQ = SQ & " WHERE a_mid = " & Masa_Id & " and a_durum = 0 "
            DT = SQL_TABLO(SQ, "T", False, False, False)

            Return DT


        End Function



        Public Function Bul_Id(ByVal Id As Long) As Boolean

            Dim SQ As String = ""
            Dim DT As New DataTable


            SQ = ""
            SQ = "SELECT "
            SQ = SQ & " master.a_id, "
            SQ = SQ & " master.a_tarih, "
            SQ = SQ & " master.a_saat, "
            SQ = SQ & " master.a_mid, "
            SQ = SQ & " master.a_subeid, "
            SQ = SQ & " master.a_not, "
            SQ = SQ & " master.a_cari_id, "
            SQ = SQ & " master.a_gunsira, "
            SQ = SQ & " master.a_fisno, "
            SQ = SQ & " master.a_kisi_sayisi, "
            SQ = SQ & " master.a_durum, "
            SQ = SQ & " master.a_userid, "
            SQ = SQ & " master.a_personel, "
            SQ = SQ & " tbl_user.a_kod AS Kul_Kodu, "
            SQ = SQ & " tbl_user.a_adi AS Kul_Adi, "
            SQ = SQ & " masa.a_kod AS Masa_Kod, "
            SQ = SQ & " masa.a_adi AS Masa_Adi, "
            SQ = SQ & " sube.a_kod AS Sube_Kodu, "
            SQ = SQ & " sube.a_adi AS Sube_Adi, "
            SQ = SQ & " cari.a_kod AS Cari_Kod, "
            SQ = SQ & " cari.a_adi AS Cari_Ad "
            SQ = SQ & " FROM "
            SQ = SQ & " " & KUL.tfrm & "men_adisyonmas AS master LEFT OUTER JOIN"
            SQ = SQ & " " & KUL.tfrm & "cari AS cari ON master.a_cari_id = cari.a_id LEFT OUTER JOIN"
            SQ = SQ & " " & KUL.tfrm & "sube AS sube ON master.a_subeid = sube.a_id LEFT OUTER JOIN"
            SQ = SQ & " " & KUL.tfrm & "men_masa AS masa ON master.a_mid = masa.a_id LEFT OUTER JOIN"
            SQ = SQ & " tbl_user ON master.a_userid = tbl_user.a_id"
            SQ = SQ & " WHERE master.a_id = " & Id & ""
            DT = SQL_TABLO(SQ, "T", False, False, False)


            If DT.Rows.Count <> 0 Then
                a_id = NULN(DT, 0, "a_id")
                a_tarih = NULA(DT, 0, "a_tarih")
                a_saat = NULA(DT, 0, "a_saat")
                a_mid = NULN(DT, 0, "a_mid")
                a_subeid = NULN(DT, 0, "a_subeid")
                a_not = NULA(DT, 0, "a_not")
                a_cari_id = NULN(DT, 0, "a_cari_id")
                a_gunsira = NULN(DT, 0, "a_gunsira")
                a_fisno = NULA(DT, 0, "a_fisno")
                a_kisi_sayisi = NULN(DT, 0, "a_kisi_sayisi")
                a_durum = NULN(DT, 0, "a_durum")
                a_userid = NULN(DT, 0, "a_userid")
                a_personel = NULN(DT, 0, "a_personel")

                x_masakod = NULA(DT, 0, "Masa_Kod")
                x_masaadi = NULA(DT, 0, "Masa_Adi")
                x_subekod = NULA(DT, 0, "Sube_Kodu")
                x_subeadi = NULA(DT, 0, "Sube_Adi")
                x_carikod = NULA(DT, 0, "Cari_Kod")
                x_cariadi = NULA(DT, 0, "Cari_Ad")
                x_userkod = NULA(DT, 0, "Kul_Kodu")
                x_useradi = NULA(DT, 0, "Kul_Adi")

                Return True
            Else
                a_id = 0
                a_tarih = ""
                a_saat = ""
                a_mid = 0
                a_subeid = 0
                a_not = ""
                a_cari_id = 0
                a_gunsira = 0
                a_fisno = ""
                a_kisi_sayisi = 0
                a_durum = 0
                a_userid = 0

                x_masakod = ""
                x_masaadi = ""
                x_subekod = ""
                x_subeadi = ""
                x_carikod = ""
                x_cariadi = ""
                x_userkod = ""
                x_useradi = ""

                Return False
            End If


        End Function


        Public Function Sil(ByVal Id As Long) As Boolean
            Dim SQ As String = ""
            SQ = "DELETE "
            SQ = SQ & " FROM " & KUL.tfrm & "men_adisyonmas "
            SQ = SQ & " WHERE a_id = " & Id & ""
            Return SQL_KOS(SQ, True)
        End Function

        Public Function Duzelt(ByVal Id As Long) As Boolean
            Dim SQ As String = ""
            Dim DT As New DataTable
            'a_tarih, a_saat, a_mid, a_subeid, a_not, a_cari_id, a_gunsira, a_fisno, a_kisi_sayisi, a_durum, a_userid 
            SQ = "UPDATE " & KUL.tfrm & "men_adisyonmas "
            SQ = SQ & " SET "
            SQ = SQ & " a_not         = N'" & a_not & "', "
            SQ = SQ & " a_cari_id     = " & a_cari_id & ", "
            SQ = SQ & " a_fisno       = N'" & a_fisno & "', "
            SQ = SQ & " a_kisi_sayisi = " & a_kisi_sayisi & ", "
            SQ = SQ & " a_durum       = " & a_durum & " "
            SQ = SQ & " WHERE a_id = " & Id & ""
            Return SQL_KOS(SQ, True)
        End Function

    End Class

    Public Function SayimBelgeDetayGetir(BELGEID As Long) As Service.WS_BELGEDETAY()
        Dim SQ As String = ""
        Dim DT As New DataTable
        Dim TOPLAM_MIKTAR As Double
        'Dim TOPLAMTUTAR As Double

        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()

        SQ = "SELECT  stk.a_adi AS AD,STK.a_kod AS KOD,STK.a_barkod AS BARKOD, "
        SQ = SQ & " STHD.a_sira as SIRA,STK.a_id as STOKID,STHD.a_sayilan as MIKTAR "
        SQ = SQ & " FROM " & KUL.tfrm & "stok as STK INNER JOIN " & KUL.tper & "stsaydet as STHD "
        SQ = SQ & " ON STK.a_id=STHD.a_stok_id "
        SQ = SQ & " where STHD.a_id=" & BELGEID.ToString
        SQ = SQ & " ORDER BY a_sira DESC"
        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)



        If DT.Rows.Count > 0 Then
            TOPLAM_MIKTAR = DT.Compute("SUM(MIKTAR)", "")
            Dim VERI(DT.Rows.Count - 1) As Service.WS_BELGEDETAY
            For X = 0 To DT.Rows.Count - 1
                VERI(X) = New Service.WS_BELGEDETAY
                VERI(X).DURUM = Service.NUMARATOR.BAŞARILI
                VERI(X).SATIRSAYISI = DT.Rows.Count
                VERI(X).URUNSIRA = NULN(DT, X, "SIRA")
                VERI(X).URUNADI = NULA(DT, X, "AD")
                VERI(X).URUNID = NULN(DT, X, "STOKID")
                VERI(X).URUNBARKODU = NULA(DT, X, "BARKOD")
                If VERI(X).URUNBARKODU = "" Then
                    VERI(X).URUNBARKODU = " "
                End If
                VERI(X).URUNKODU = NULA(DT, X, "KOD")
                VERI(X).URUNMİKTARI = NULD(DT, X, "MIKTAR")
                VERI(X).BIRIMFIYAT = 0 'NULD(DT, X, "BIRIMFIYAT")
                VERI(X).TUTARTOPLAM = 0
                VERI(X).TOPLAMMIKTAR = TOPLAM_MIKTAR 'TOPLAM ÜRÜN ADEDİ
            Next
            Return VERI
        Else
            SQ = " SELECT a_id FROM " & KUL.tper & "stsaymas WHERE a_id=" & BELGEID
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)

            If DT.Rows.Count = 0 Then
                Dim VERI(0) As Service.WS_BELGEDETAY
                VERI(0) = New Service.WS_BELGEDETAY
                VERI(0).DURUM = Service.NUMARATOR.HATA
                VERI(0).SATIRSAYISI = 0
                VERI(0).URUNSIRA = 0
                VERI(0).URUNADI = " "
                VERI(0).URUNID = 0
                VERI(0).URUNBARKODU = " "
                VERI(0).URUNKODU = 0
                VERI(0).URUNMİKTARI = 0
                VERI(0).BIRIMFIYAT = 0 'NULD(DT, X, "BIRIMFIYAT")
                VERI(0).TUTARTOPLAM = 0
                VERI(0).TOPLAMMIKTAR = 0
                Return VERI

            Else
                Dim VERI(0) As Service.WS_BELGEDETAY
                VERI(0) = New Service.WS_BELGEDETAY
                VERI(0).DURUM = Service.NUMARATOR.BOSBELGE
                VERI(0).SATIRSAYISI = 0
                VERI(0).URUNSIRA = 0
                VERI(0).URUNADI = " "
                VERI(0).URUNID = 0
                VERI(0).URUNBARKODU = " "
                VERI(0).URUNKODU = 0
                VERI(0).URUNMİKTARI = 0
                VERI(0).BIRIMFIYAT = 0 'NULD(DT, X, "BIRIMFIYAT")
                VERI(0).TUTARTOPLAM = 0
                VERI(0).TOPLAMMIKTAR = 0
                Return VERI
            End If

            'Dim VERI2(0) As Service.WS_BELGEDETAY
            'VERI2(0) = New Service.WS_BELGEDETAY
            'VERI2(0).DURUM = Service.NUMARATOR.BOSBELGE
            'VERI2(0).MIKTAR = 0
            'VERI2(0).URUNSIRA = 0
            'VERI2(0).URUNADI = " "
            'VERI2(0).URUNID = 0
            'VERI2(0).URUNBARKODU = " "
            'VERI2(0).URUNKODU = 0
            'VERI2(0).URUNMİKTARI = 0
            'VERI2(0).BIRIMFIYAT = 0 'NULD(DT, X, "BIRIMFIYAT")
            'VERI2(0).TUTARTOPLAM = 0
            'VERI2(0).TOPLAMMIKTAR = 0
            'Return
        End If

    End Function

    Public Function SatisKabulBelgeDetayGetir(TUR As Long, ID As Long) As Service.WS_BELGEDETAY()
        Dim SQ As String = ""
        Dim DT As New DataTable
        Dim TOPLAM_MIKTAR As Double = 0
        Dim TABLO, TABLO1 As String
        Dim URUNAD As String = ""
        Dim URUNKOD As String = ""
        Dim URUNBARKOD As String = ""
        Dim URUNMIKTAR As Double = 0
        Dim SATIRSAYISI As Long = 0
        Dim TOPLAMTUTAR As Double = 0
        Dim DT2, DT3, DT4 As DataTable
        'Dim TOPLAMTUTAR As Double

        B_SQL = ConfigurationManager.ConnectionStrings("connstr").ConnectionString

        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()

        Select Case TUR
            Case 10, 11, 12, 13, 14, 16, 70, 75, 40, 41, 42, 95, 96
                TABLO = "stkcdet"
                TABLO1 = "stkcmas"
            Case 17, 18, 19, 20, 21, 22, 54, 71, 87, 88, 43, 44, 69
                TABLO = "stkgdet"
                TABLO1 = "stkgmas"

            Case Else
                Dim VERI(0) As Service.WS_BELGEDETAY
                VERI(0) = New Service.WS_BELGEDETAY

                VERI(0).DURUM = Service.NUMARATOR.HATA
                VERI(0).SATIRSAYISI = 0
                VERI(0).URUNSIRA = 0
                VERI(0).URUNADI = " "
                VERI(0).URUNID = 0
                VERI(0).URUNBARKODU = " "
                VERI(0).URUNKODU = 0
                VERI(0).URUNMİKTARI = 0
                VERI(0).TUTARTOPLAM = 0

        End Select
        If TUR <> 49 And TUR <> 4 And TUR <> 33 And TUR <> 34 Then

            SQ = "SELECT  stk.a_adi AS AD,STK.a_kod AS KOD,STK.a_barkod AS BARKOD,STHD.a_mik AS MIKTAR,STHD.a_sira as SIRA,STK.a_id as STOKID,CAST(STHD.a_brmfiy as DECIMAL(25,4)) as BIRIMFIYAT FROM " & KUL.tfrm & "stok as STK INNER JOIN " & KUL.tper & TABLO & " as STHD "
            SQ = SQ & " ON STK.a_id=STHD.a_stok_id "
            SQ = SQ & " where STHD.a_tur=" & TUR & " and STHD.a_id=" & ID
            SQ = SQ & " ORDER BY a_sira DESC"
            DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
            URUNAD = NULA(DT, 0, "AD")
            URUNKOD = NULA(DT, 0, "KOD")
            URUNBARKOD = NULA(DT, 0, "BARKOD")
            URUNMIKTAR = NULD(DT, 0, "MIKTAR")
            SQ = ""
            SQ = "Select count(*) As MIKTAR FROM " & KUL.tper & TABLO & " WHERE a_id=" & ID & " And a_tur=" & TUR
            DT2 = SQL_TABLO(SQ, "T", False, False, False)
            SATIRSAYISI = NULN(DT2, 0, "MIKTAR")
            'BELGENİN TOPLAM TUTARI BULUNUYOR
            SQ = ""
            SQ = "Select CAST(a_tuttop as DECIMAL(25,4))  As TUTAR FROM " & KUL.tper & TABLO1 & " WHERE a_id=" & ID & " And a_tur=" & TUR
            DT3 = SQL_TABLO(SQ, "T", False, False, False)
            TOPLAMTUTAR = NULD(DT3, 0, "TUTAR")
            SQ = ""
            SQ = "Select SUM(a_mik) As MIKTAR FROM " & KUL.tper & TABLO & " WHERE a_id=" & ID & " And a_tur=" & TUR
            DT4 = SQL_TABLO(SQ, "T", False, False, False)
            TOPLAM_MIKTAR = NULD(DT4, 0, "MIKTAR")
            If DT.Rows.Count > 0 Then
                Dim VERI(DT.Rows.Count - 1) As Service.WS_BELGEDETAY
                For X = 0 To DT.Rows.Count - 1
                    VERI(X) = New Service.WS_BELGEDETAY
                    VERI(X).DURUM = Service.NUMARATOR.BAŞARILI
                    VERI(X).SATIRSAYISI = SATIRSAYISI
                    VERI(X).URUNSIRA = NULN(DT, X, "SIRA")
                    VERI(X).URUNADI = NULA(DT, X, "AD")
                    VERI(X).URUNID = NULN(DT, X, "STOKID")
                    VERI(X).URUNBARKODU = NULA(DT, X, "BARKOD")
                    If VERI(X).URUNBARKODU = "" Then
                        VERI(X).URUNBARKODU = " "
                    Else
                        VERI(X).URUNBARKODU = NULA(DT, X, "BARKOD")
                    End If
                    VERI(X).URUNKODU = NULA(DT, X, "KOD")
                    VERI(X).URUNMİKTARI = NULD(DT, X, "MIKTAR")
                    VERI(X).BIRIMFIYAT = NULD(DT, X, "BIRIMFIYAT")
                    VERI(X).TUTARTOPLAM = TOPLAMTUTAR
                    VERI(X).TOPLAMMIKTAR = TOPLAM_MIKTAR
                Next
                Return VERI
            Else
                Dim VERI(0) As Service.WS_BELGEDETAY
                VERI(0) = New Service.WS_BELGEDETAY
                VERI(0).DURUM = Service.NUMARATOR.HATA
                VERI(0).SATIRSAYISI = 0
                VERI(0).URUNSIRA = 0
                VERI(0).URUNADI = " "
                VERI(0).URUNID = 0
                VERI(0).URUNBARKODU = " "
                VERI(0).URUNKODU = 0
                VERI(0).URUNMİKTARI = 0
                VERI(0).TUTARTOPLAM = 0
                VERI(0).TOPLAMMIKTAR = 0
                Return VERI
            End If
        End If

    End Function
    Function Stok_Gir_Mik(ByVal gtar As String, ByVal gdepo As Long, ByVal gstok As Long, ByVal gstip As String) As Double

        Dim SQ As String = ""
        Dim DT As New DataTable
        Dim MK As Double = 0

        SQ = ""
        SQ = "SELECT SUM(a_mik) AS mik"
        SQ = SQ & " FROM " & KUL.tper & "stkhdet"
        SQ = SQ & " WHERE "
        SQ = SQ & " (a_tarih <= N'" & TTAR.TR2UK(gtar) & "') "
        SQ = SQ & " AND (a_ambar_id = " & gdepo & ") "
        SQ = SQ & " AND (a_gc = N'G')"
        If gstip = "A" Then
            SQ = SQ & " AND (a_stok_id IN ((SELECT a_id FROM " & KUL.tfrm & "stok WHERE (a_reftip = N'T') AND (a_refstokid = " & gstok & ")) " & ")) "
        Else
            SQ = SQ & " AND (a_stok_id = " & gstok & ")"
        End If
        DT = SQL_TABLO(SQ, "T", False, False, False)
        MK = NULD(DT, 0, "mik")

        Return MK

    End Function

    Function Stok_Cik_Mik(ByVal ctar As String, ByVal cdepo As Long, ByVal cstok As Long, ByVal cstip As String) As Double

        Dim SQ As String = ""
        Dim DT As New DataTable
        Dim MK As Double = 0

        SQ = ""
        SQ = "SELECT SUM(a_mik) AS mik"
        SQ = SQ & " FROM " & KUL.tper & "stkhdet"
        SQ = SQ & " WHERE "
        SQ = SQ & " (a_tarih <= N'" & TTAR.TR2UK(ctar) & "') "
        SQ = SQ & " AND (a_ambar_id = " & cdepo & ") "
        SQ = SQ & " AND (a_gc = N'C')"
        If cstip = "A" Then
            SQ = SQ & " AND (a_stok_id IN ((SELECT a_id FROM " & KUL.tfrm & "stok WHERE (a_reftip = N'T') AND (a_refstokid = " & cstok & ")) " & ")) "
        Else
            SQ = SQ & " AND (a_stok_id = " & cstok & ")"
        End If
        DT = SQL_TABLO(SQ, "T", False, False, False)
        MK = NULD(DT, 0, "mik")

        Return MK

    End Function

    Public Function FirmaIdGetir(ByVal a_uid As Long)
        Dim FIRMAID As Long
        Dim SQ As String
        Dim DT As New DataTable

        SQ = ""
        SQ = "Select KUL.a_firma_id "
        SQ = SQ & " FROM  tbl_user as KUL "
        SQ = SQ & " INNER JOIN tbl_userfrm AS KULYETKI ON KUL.a_id=KULYETKI.a_userid AND KUL.a_firma_id = KULYETKI.a_firma "
        SQ = SQ & " INNER Join tbl_user_grp as KULGRUP ON KUL.a_grpid=KULGRUP.a_id "
        SQ = SQ & " WHERE KUL.a_id=" & a_uid & " "
        DT = SQL_TABLO_2005(B_SQL, SQ, "T", False, False, False, False)
        If DT.Rows.Count > 0 Then
            FIRMAID = NULN(DT, 0, "a_firma_id")
            Return FIRMAID
        Else
            Return 0
        End If
    End Function
    Public Function Fiyat_Güncelleme(ByVal CNN As String, ByVal ADID As Long, ByVal STOKID As Long, ByVal SUBEID As Long, ByVal CARIID As Long, ByVal SIRA As Long)
        B_SQL = CNN
        SQL_TUR = "2005"
        KUL.tper = ConfigurationManager.AppSettings("KULtper").ToString()
        KUL.tfrm = ConfigurationManager.AppSettings("KULtfrm").ToString()
        Dim DT As New DataTable
        Dim SQ As String


        SQ = "SELECT a_master_id FROM " & KUL.tfrm & "men_adisyon where a_master_id=" & ADID & " AND a_sid=" & STOKID & " AND a_sira=" & SIRA & ""
        DT = SQL_TABLO(SQ, "T", False, False, False)

        If DT.Rows.Count = 0 Then

            'hata mesajı


        End If

        Dim CARI As New XCari.Cari_Kartlar
        Dim STOK As New STOK_BILGI

        CARI.Kayit_Bul_ID(CARIID)

        Dim XF As New X_Fiyat_Bilgi
        Dim FIY As Double = 0
        Dim FIYISK As Double = 0
        Dim FIY2 As Double
        STOK.BUL(STOKID)



        XF.Fil_AL_SAT = "SAT"
        XF.Fil_Tur = Islem_Turu.a010_Perakende_Satis_Faturasi
        XF.Fil_F_List_Id = 0
        XF.Fil_Cari_Id = CARI.ID
        XF.Fil_Cari_Kod = CARI.Cari_Hesap_Kodu
        XF.Fil_Sube_Id = SUBEID
        XF.Fil_Isl_Tarih = TTAR.TARIH
        XF.Fil_STB = STOK
        XF.X_Fiyat_Getir()
        FIY = XF.RET_Fiyat
        FIY2 = XF.RET_Fiyat_Isk


        If FIY > 0 Then
            SQ = "UPDATE " & KUL.tfrm & "men_adisyon "
            SQ = SQ & "SET a_tut=" & FIY & "*a_mik , "
            SQ = SQ & "a_isktutar=(" & FIY - FIY2 & ")*a_mik, "
            SQ = SQ & "a_iskoran=" & ((FIY - FIY2) / FIY) * 100 & " "
            SQ = SQ & "WHERE a_master_id=" & ADID & " AND a_sid=" & STOKID & " AND a_sira=" & SIRA & ""
            If SQL_KOS(SQ, False) = True Then
                ' başarılı 
            End If
        End If
    End Function
End Module
